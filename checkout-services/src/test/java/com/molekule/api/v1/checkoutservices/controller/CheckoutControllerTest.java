package com.molekule.api.v1.checkoutservices.controller;


import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.checkoutservices.service.CheckoutService;
import com.molekule.api.v1.checkoutservices.service.FedExApiService;
import com.molekule.api.v1.checkoutservices.util.PaymentTypeNotFound;
import com.molekule.api.v1.commonframework.model.checkout.OrderRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.service.sns.NotificationHelperService;

@ContextConfiguration
public class CheckoutControllerTest {

	@InjectMocks
	CheckoutController checkoutController;

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Mock
	Logger logger;

	@Mock
	CheckoutService checkoutService;

	@Mock
	FedExApiService fedExApiService;

	@Mock
	NotificationHelperService notificationHelperService;

	@Mock
	HttpServletRequest serverHttpRequest;

	@Mock
	HttpServletResponse serverHttpResponse;

	@Mock
	PaymentRequest paymentmRequestBean;

	@Test
	public void taxCalculate() {
		checkoutController.taxCalculate(ORDER_NUMBER);
	}

	@Test
	public void applyPromoCode() {
		checkoutController.applyPromoCode("cartId", "promoCode");
	}

	@Test
	public void removePromoCode() {
		checkoutController.removePromoCode("cartId", "promoCode", null);
	}

	/*
	 * @Test public void paymentRequest() throws PaymentTypeNotFound {
	 * checkoutController.submitPayment(paymentmRequestBean); }
	 */

//	@Test
	public void createOrder() throws PaymentTypeNotFound {
		OrderRequestBean orderRequestBean = Mockito.mock(OrderRequestBean.class);
		checkoutController.createOrder(orderRequestBean, "");
	}

	@Test
	public void getAccountDetails() {

		checkoutController.getAccountDetails("accountId", "customerId");
	}

	@Test
	public void makeFedExApiCall() {
		checkoutController.makeFedExApiCall("cartId", "");
	}

	@Test
	public void purchaseEvent() throws IOException {
		checkoutController.purchaseEvent(serverHttpRequest, serverHttpResponse);

	}

}
