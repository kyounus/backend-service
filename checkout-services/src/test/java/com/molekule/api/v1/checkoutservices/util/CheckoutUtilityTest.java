package com.molekule.api.v1.checkoutservices.util;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
public class CheckoutUtilityTest {

	@Test
	public void checkoutUtility() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CheckoutUtility.class);
	}

}