package com.molekule.api.v1.checkoutservices.service;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
public class FedExApiServiceTest {

	@Test
	public void fedExApiService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(FedExApiService.class);
	}

}