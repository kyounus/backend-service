package com.molekule.api.v1.checkoutservices.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
public class PaymentTypeNotFoundTest {

	@Test
	public void paymentTypeNotFound() throws Exception {
		PaymentTypeNotFound paymentTypeNotFound = new PaymentTypeNotFound("test message", 1000);
		assertEquals(1000, paymentTypeNotFound.errorCode);
		assertEquals("test message", paymentTypeNotFound.getMessage());

	}

}