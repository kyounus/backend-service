package com.molekule.api.v1.checkoutservices.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
public class ErrorCodeTest {

	@Test
	public void errorCodeTest() throws Exception {

		assertEquals("Payment Type not found.", ErrorCode.PAYMENT_TYPE.getDescription());
		assertEquals(1000, ErrorCode.PAYMENT_TYPE.getCode());
		assertEquals("1000: Payment Type not found.", ErrorCode.PAYMENT_TYPE.toString());

	}

}