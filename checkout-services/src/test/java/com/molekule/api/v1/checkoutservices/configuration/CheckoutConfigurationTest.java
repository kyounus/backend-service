package com.molekule.api.v1.checkoutservices.configuration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class CheckoutConfigurationTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CheckoutConfiguration checkoutConfiguration;

	@Test
	public void getDocket() {
		checkoutConfiguration.getDocket();
	}

	@Test
	public void crosConfigure() {
		checkoutConfiguration.crosConfigure();

	}

	public void getApiInfo() {
		checkoutConfiguration.getApiInfo();
	}

	@Test
	public void cTEnvProperties() {
		checkoutConfiguration.cTEnvProperties();
	}

	@Test
	public void netConnectionHelper() {
		checkoutConfiguration.netConnectionHelper();
	}

	@Test
	public void ctServerHelperService() {
		checkoutConfiguration.ctServerHelperService();
	}

	@Test
	public void registrationHelperService() {
		checkoutConfiguration.registrationHelperService();
	}

	@Test
	public void checkoutHelperService() {
		checkoutConfiguration.checkoutHelperService();
	}

	@Test
	public void orderHelperService() {
		checkoutConfiguration.orderHelperService();
	}

	@Test
	public void cartHelperService() {
		checkoutConfiguration.cartHelperService();
	}

	@Test
	public void taxService() {
		checkoutConfiguration.taxService();
	}

	@Test
	public void handlingCostHelperService() {
		checkoutConfiguration.handlingCostHelperService();
	}

	@Test
	public void parcelHandlingCostCalculationServieImpl() {
		checkoutConfiguration.parcelHandlingCostCalculationServieImpl();
	}

	@Test
	public void freightHandlingCostCalculationServiceImpl() {
		checkoutConfiguration.freightHandlingCostCalculationServiceImpl();
	}

	@Test
	public void tealiumHelperService() {
		checkoutConfiguration.tealiumHelperService();
	}

	@Test
	public void notificationHelperService() {
		checkoutConfiguration.notificationHelperService();
	}

	@Test
	public void paymentReceivedConfirmationSender() {
		checkoutConfiguration.paymentReceivedConfirmationSender();
	}

	@Test
	public void orderConfirmMailSender() {
		checkoutConfiguration.orderConfirmMailSender();
	}

	@Test
	public void orderPendingMailSender() {
		checkoutConfiguration.orderPendingMailSender();
	}

	@Test
	public void orderPendingSalesRepMailSender() {
		checkoutConfiguration.orderPendingSalesRepMailSender();
	}

	@Test
	public void freightInfoPendingMailSender() {
		checkoutConfiguration.freightInfoPendingMailSender();
	}

	@Test
	public void paymentPendingMailSender() {
		checkoutConfiguration.paymentPendingMailSender();
	}

	@Test
	public void auroraHelperService() {
		checkoutConfiguration.auroraHelperService();
	}

	@Test
	public void csrHelperService() {
		checkoutConfiguration.csrHelperService();
	}

	@Test
	public void quoteRequestConfirmationMailSender() {
		checkoutConfiguration.quoteRequestConfirmationMailSender();
	}

	@Test
	public void quoteRequestPendingApprovalMailSender() {
		checkoutConfiguration.quoteRequestPendingApprovalMailSender();
	}

	@Test
	public void subscriptionSignupConfirmationSender() {
		checkoutConfiguration.subscriptionSignupConfirmationSender();
	}

	@Test
	public void orderShippedMailSender() {
		checkoutConfiguration.orderShippedMailSender();
	}

}