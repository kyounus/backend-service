package com.molekule.api.v1.checkoutservices.util;

public class PaymentTypeNotFound extends Exception{
   
	private static final long serialVersionUID = 13074934209253835L;
	final int errorCode;
	public PaymentTypeNotFound(String message,int errorCode) {
	   super(message);
	   this.errorCode = errorCode;
	}

}
