package com.molekule.api.v1.checkoutservices.service;

import org.springframework.stereotype.Service;

import com.molekule.api.v1.commonframework.model.checkout.PaymentIntentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
@Service
public interface PaymentService {

	String authorizePayment(PaymentOrderDTO paymentOrderDTO,String currency);
	String capturePayment(PaymentOrderDTO paymentOrderDTO,String authToken,String currency);
	String cancelPayment(PaymentOrderDTO paymentOrderDTO,String authToken);
	String createPaymentToken(PaymentIntentRequest paymentmRequestBean, String action, String paymentIntentId);
	String captureOrCancelPayment(String action, String paymentIntentId);
	String createPaymentToken(PaymentRequest paymentRequest,String currency);
	String refundPayment(Object refundObject);
}
