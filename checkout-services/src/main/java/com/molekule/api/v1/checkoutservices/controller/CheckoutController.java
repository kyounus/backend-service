package com.molekule.api.v1.checkoutservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.molekule.api.v1.checkoutservices.service.CheckoutService;
import com.molekule.api.v1.checkoutservices.service.FedExApiService;
import com.molekule.api.v1.checkoutservices.util.CheckoutUtility;
import com.molekule.api.v1.checkoutservices.util.PaymentTypeNotFound;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.model.checkout.AffirmCheckoutBean;
import com.molekule.api.v1.commonframework.model.checkout.CheckoutSurveyResponse;
import com.molekule.api.v1.commonframework.model.checkout.OrderRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.model.checkout.SubscriptionShippingMethodsRequestBean;
import com.molekule.api.v1.commonframework.service.sns.NotificationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The class CheckoutController is used to write checkout related apis.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/users")
public class CheckoutController {

	Logger logger = LoggerFactory.getLogger(CheckoutController.class);
	@Autowired
	@Qualifier("checkoutService")
	CheckoutService checkoutService;

	@Autowired
	@Qualifier("fedExApiService")
	FedExApiService fedExApiService;

	@Autowired
	@Qualifier("notificationHelperService")
	NotificationHelperService notificationHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	/**
	 * taxCalculate method is used to calculate the tax.
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(value = "/checkout/tax/{orderNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String taxCalculate(
			@ApiParam(value = "The order number", required = true) @PathVariable(ORDER_NUMBER) String orderNumber) {
		return checkoutService.taxCalculate(orderNumber);
	}

	/**
	 * applyPromoCode method is used to add promocode for cart products.
	 * 
	 * @param cartId,promocode
	 * @return
	 */
	@PostMapping(value = "/checkout/{cartId}/discounts/{promocode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String applyPromoCode(
			@ApiParam(value = "The cart id", required = true) @PathVariable("cartId") String cartId,
			@ApiParam(value = "promo code", required = true) @PathVariable("promocode") String promoCode) {
		return checkoutService.applyPromoCode(cartId, promoCode);
	}

	/**
	 * removePromoCode method is used to remove promocode for cart products.
	 * 
	 * @param cartId
	 * @return
	 */
	@DeleteMapping(value = "/checkout/{cartId}/discounts/{promocode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String removePromoCode(
			@ApiParam(value = "The cart id", required = true) @PathVariable("cartId") String cartId,
			@ApiParam(value = "promo code", required = true) @PathVariable("promocode") String promoCode,
			@RequestHeader("country") String country) {
		return checkoutService.removePromoCode(cartId, promoCode,country);
	}

	
	@PostMapping(value = "/checkout/payment", produces = MediaType.APPLICATION_JSON_VALUE)

	@ApiOperation(value = "Create Payment")
	public String submitPayment(

			@ApiParam(value = "Payment", required = true) @RequestBody PaymentRequest paymentRequest,
			@RequestHeader("country") String country) throws PaymentTypeNotFound {
		return checkoutService.createPayment(paymentRequest, country);
	}

	@PostMapping(value = "/checkout/payment/token", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Payment Token")
	public String getPaymentToken(@RequestHeader("country") String country,
			@ApiParam(value = "Order Payment", required = true) @RequestBody PaymentRequest paymentRequest)
			{
		return checkoutService.getPaymentToken(paymentRequest, country);
	}

	/*
	 * @PostMapping(value = "/checkout/payment", produces =
	 * MediaType.APPLICATION_JSON_VALUE)
	 * 
	 * @ApiOperation(value = "Create Payment Token") public String
	 * paymentToken(@RequestHeader("country") String country,
	 * 
	 * @ApiParam(value = "Order Payment", required = true) @RequestBody
	 * PaymentRequest paymentRequest) { return
	 * checkoutService.getPaymentToken(paymentRequest, "US"); }
	 */

	/**
	 * createOrder method is used to create the order and capture the payment
	 * intent.
	 * 
	 * @param OrderRequestBean
	 * @return
	 * @throws PaymentTypeNotFound
	 */
	@PostMapping(value = "/checkout/order", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Order")
	public String createOrder(
			@ApiParam(value = "Create Order", required = true) @RequestBody OrderRequestBean orderRequestBean,
			@RequestHeader("country") String country)
			throws PaymentTypeNotFound {
		logger.debug("Checkout Controller Create Order Method Start -"+new Timestamp(System.currentTimeMillis()));
		String orderResponse = checkoutService.captureOrCancelPayment(CheckoutUtility.createOrderDto(orderRequestBean),
				true, false, country, orderRequestBean);
		JsonObject orderJson =  MolekuleUtility.parseJsonObject(orderResponse);
		if(orderJson.has("id")) {
		tealiumHelperService.setPurchaseSubData(orderResponse);
		}
		logger.debug("Checkout Controller Create Order Method End -"+new Timestamp(System.currentTimeMillis()));
		return orderResponse;
	}

	/**
	 * getAccountDetails method is used to get Account Custom Object details.
	 * 
	 * @param accountId
	 * @return ResponseEntity<AccountCustomObject>
	 */
	@GetMapping(value = "/checkout/account/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Account Details", response = AccountCustomObject.class)
	public ResponseEntity<AccountCustomObject> getAccountDetails(
			@ApiParam(value = "The account id", required = true) @PathVariable("accountId") String accountId,
			@ApiParam(value = "The customer id", required = false) @RequestParam(defaultValue = "") String customerId) {
		return checkoutService.getAccountCustomObjectApi(accountId, customerId);
	}

	/**
	 * makeFedExApiCall method is used to get to make a fedEx api call.
	 * 
	 * @param accountId
	 * @return Map<String, String>
	 */
	@GetMapping(value = "/checkout/{cartId}/shipping-methods")
	@ApiOperation(value = "Make FedEx Api call to get shipping methods", response = AccountCustomObject.class)
	public ResponseEntity<Object> makeFedExApiCall(
			@ApiParam(value = "The Cart Id", required = true) @PathVariable("cartId") String cartId,
			@ApiParam(value = "Country", example = "US", required = false) @RequestParam("country") String country) {
		return fedExApiService.prepareFedExApi(cartId, country);
	}

	/**
	 * getSmartPostService method is used to get to make a fedEx api call for
	 * smart-post.
	 * 
	 * @param cartId
	 * @return Map<String, String>
	 */
	@GetMapping(value = "/checkout/{cartId}/smart-post")
	@ApiOperation(value = "Get Smart Post Shipping Method")
	public ResponseEntity<Object> getSmartPostService(
			@ApiParam(value = "The Cart Id", required = true) @PathVariable("cartId") String cartId) {
		return fedExApiService.getSmartPostService(cartId);
	}

	/**
	 * getSubscriptionShippingMethods method is used to get to shipping methods when
	 * creating subscription.
	 * 
	 * @param orderDate
	 * @return Map<String, String>
	 */
	@PostMapping(value = "/checkout/subscription/shipping-methods")
	@ApiOperation(value = "Get Subscription Shipping Methods")
	public ResponseEntity<Object> getSubscriptionShippingMethods(
			@ApiParam(value = "Order Date", required = true) @RequestBody SubscriptionShippingMethodsRequestBean subscriptionShippingMethodsRequestBean) {
		return fedExApiService.getSubscriptionShippingMethods(subscriptionShippingMethodsRequestBean);
	}

	@PostMapping(value = "/checkout/orders/event-notification", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Simple Notification Service")
	public String purchaseEvent(HttpServletRequest serverHttpRequest, HttpServletResponse serverHttpResponse) {
		return notificationHelperService.getSNSResponse(serverHttpRequest);

	}

	/**
	 * 
	 * @param orderId
	 * @return
	 */

	@GetMapping(value = "/checkout/sendOrderConfirmationEmail/{orderNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Send Order Confirmation Email to Customer")
	public String sendOrderConfirmationEmail(
			@ApiParam(value = "Order Number", required = true) @PathVariable(ORDER_NUMBER) String orderNumber) {
		return checkoutService.sendOrderConfEmail(orderNumber);
	}

	@GetMapping(value = "/checkout/sendOrderInvoiceEmail/{orderNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Send Order Invoices Email to Customer")
	public String sendOrderInvoiceEmail(
			@ApiParam(value = "Order Number", required = true) @PathVariable(ORDER_NUMBER) String orderNumber) {
		return checkoutService.sendOrderInvoiceEmail(orderNumber);
	}

	@PostMapping(value = "/afirm/charges", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Affirm charges")

	public String affirmCharges(@RequestBody AffirmCheckoutBean afirmCheckoutBean) {
		return checkoutService.affirmCharges(afirmCheckoutBean);
	}

	/**
	 * getAllSurveysResponse method issue to get all Survey questions .
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/checkout/surveys", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all Checkout Survey Questions and Answers")
	public String getCheckoutSurveys() {
		return checkoutService.getCheckoutSurveys();
	}

	/**
	 * getSurveyResponses method issue to get all Customer Surveys .
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/checkout/surveys/response", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all Customer Survey Response")
	public CheckoutSurveyResponse getSurveyResponses(
			@ApiParam(value = "The limit is", example = "20", required = true) @RequestParam int limit,
			@ApiParam(value = "The offset is", example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "The email is", required = false) @RequestParam(defaultValue = "") String email,
			@ApiParam(value = "The Customer Number is", required = false) @RequestParam(defaultValue = "") String customerNumber,
			@ApiParam(value = "Survey Answer", required = false) @RequestParam(defaultValue = "") String answer) {
		return checkoutService.getSurveyResponses(limit, offset, email, answer, customerNumber);
	}

	/**
	 * createSurveyResponse method is to add a Checkout Survey Response .
	 * 
	 * @return ResponseEntity<String>
	 * @throws Exception
	 */
	@PostMapping(value = "/checkout/surveys/response", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Add A Checkout Survey Response")
	public ResponseEntity<Object> createSurveyResponse(
			@ApiParam(value = "The Customer ID is", required = true) @RequestParam("customerId") String customerId,
			@ApiParam(value = "The Customer Number is", required = true) @RequestParam("customerNumber") String customerNumber,
			@ApiParam(value = "The Customer Email is", required = true) @RequestParam("email") String email,
			@ApiParam(value = "The Survey Response ", required = true) @RequestParam("surveyResponse") String surveyResponse,
			@ApiParam(value = "Other response", required = true) @RequestParam("other") Boolean other) {
		return checkoutService.createSurveyResponse(customerId, customerNumber, email, surveyResponse, other);
	}

	/**
	 * updateSurveyResponse method is to update a Checkout Survey Response .
	 * 
	 * @return ResponseEntity<String>
	 * @throws Exception
	 */
	@PostMapping(value = "/checkout/surveys/{surveyId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update A Checkout Survey Response")
	public ResponseEntity<Object> updateSurveyResponse(
			@ApiParam(value = "The Survey ID is", required = true) @PathVariable("surveyId") String surveyId,
			@ApiParam(value = "The Survey Response ", required = true) @RequestParam("surveyResponse") String surveyResponse,
			@ApiParam(value = "Other response", required = true) @RequestParam("other") Boolean other) {
		return checkoutService.updateSurveyResponse(surveyId, surveyResponse, other);
	}

	/**
	 * deleteSurveyResponse method is to delete a Checkout Survey Response .
	 * 
	 * @return ResponseEntity<String>
	 */
	@DeleteMapping(value = "/checkout/surveys/{surveyId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete A Checkout Survey Response")
	public ResponseEntity<Object> deleteSurveyResponse(
			@ApiParam(value = "The Survey ID is", required = true) @PathVariable("surveyId") String surveyId) {
		return checkoutService.deleteSurveyResponse(surveyId);
	}

	/**
	 * getCheckoutSurveyResponse method is to get a Checkout Survey Response .
	 * 
	 * @return ResponseEntity<String>
	 * @throws Exception
	 */
	@GetMapping(value = "/checkout/surveys/{surveyId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get A Checkout Survey Response")
	public ResponseEntity<Object> getCheckoutSurveyResponse(
			@ApiParam(value = "The Survey ID is", required = true) @PathVariable("surveyId") String surveyId) {
		return checkoutService.getCheckoutSurveyResponse(surveyId);
	}

	@PostMapping(value = "/checkout/reorder/{orderId}/loadShippingAddress/{loadShippingAddress}/loadShippingMethod/{loadShippingMethod}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reorder by order Id", response = String.class)
	public String addLineItemIntoCart(
			@ApiParam(value = "The order id", required = true) @PathVariable("orderId") String orderId,
			@ApiParam(value = "Load Shipping Address", required = true) @PathVariable("loadShippingAddress") Boolean loadShippingAddress,
			@ApiParam(value = "Load Shipping Method", required = true) @PathVariable("loadShippingMethod") Boolean loadShippingMethod,
			@ApiParam(value = "Country", required = true) @RequestParam String country) {
		return checkoutService.addLineItemIntoCart(orderId, loadShippingAddress, loadShippingMethod, country);
	}

}
