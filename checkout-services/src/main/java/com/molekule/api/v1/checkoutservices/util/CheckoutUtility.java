package com.molekule.api.v1.checkoutservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CURRENCY_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIRST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FREIGHT_HOLD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.HOLDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_INTENT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_TYPE_CREDIT_CARD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PENDING;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.POSTAL_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SET_CUSTOM_FIELD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.model.checkout.OrderRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.Payment;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart;
import com.molekule.api.v1.commonframework.model.checkout.UpdatePayment;
import com.molekule.api.v1.commonframework.model.registration.AdditionalFreightInfo;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;

public class CheckoutUtility {

	public static final Logger LOGGER = LoggerFactory.getLogger(CheckoutUtility.class);

	public enum CartActions {
		SET_SHIPPING_ADDRESS, ADD_LINE_ITEM, REMOVE_LINE_ITEM, CHANGE_LINE_ITEM_QTY, SET_PO_NUMBER, SET_CART_AS_QUOTE,
		SET_CUSTOMER_FOR_QUOTE, SET_QUOTE_AS_CART, SET_SHIPPING_METHOD
	}

	public static Map<String, Object> getCartDat(JsonObject jsonObject) {
		Map<String, Object> cartData = new HashMap<>();
		if (jsonObject == null) {
			return null;
		}
		long cartVersion = jsonObject.get(VERSION).getAsLong();
		String totalAmount = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(ORDER_TOTAL)
				.getAsString();
		String currencyCode = jsonObject.get(TOTAL_PRICE).getAsJsonObject().get(CURRENCY_CODE).getAsString();
		cartData.put(VERSION, cartVersion);
		cartData.put(TOTAL_AMOUNT, totalAmount);
		cartData.put(CURRENCY_CODE, currencyCode);
		return cartData;
	}

	public static String convertObjToString(Object obj) {
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonStr = null;
		try {
			jsonStr = objectMapper.writeValueAsString(obj);
		} catch (Exception e) {
			LOGGER.error("Error occured when converting object to String ", e);
		}
		return jsonStr;
	}

	public static Payment createPaymentRequest(Map<String, Object> paymentData) {
		Payment payment = new Payment();
		if (paymentData.get(PAYMENT_INTENT_ID) != null)
			payment.setInterfaceId((String) paymentData.get(PAYMENT_INTENT_ID));
		Payment.AmountPlanned amountPlanned = new Payment.AmountPlanned();
		amountPlanned.setCurrencyCode((String) paymentData.get(CURRENCY_CODE));
		amountPlanned.setCentAmount(Long.parseLong((String) paymentData.get(TOTAL_AMOUNT)));
		payment.setAmountPlanned(amountPlanned);
		Payment.PaymentMethodInfo paymentMethodInfo = new Payment.PaymentMethodInfo();
		String paymentType = (String) paymentData.get("PaymentType");
		if (paymentType != null) {
			if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentType) || "ACH".equals(paymentType) || PaymentRequestBean.Payment.AFFIRM.name().equals(paymentType) )
				paymentMethodInfo.setMethod(paymentType);

		} else {
			if (paymentData.get(PAYMENT_INTENT_ID) != null)
				paymentMethodInfo.setMethod("CREDIT_CARD");
		}
		paymentMethodInfo.setPaymentInterface("STRIPE");
		Payment.PaymentMethodInfo.Name name = new Payment.PaymentMethodInfo.Name();
		if (paymentType != null) {
			
			if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentType)) {
				name.setEn("PAYMENT TERMS");
			}
			else if ("ACH".equals(paymentType)) {
				name.setEn("ACH");
			} else if (PaymentRequestBean.Payment.AFFIRM.name().equals(paymentType)) {
				name.setEn(PaymentRequestBean.Payment.AFFIRM.name());
			}

		} else {
			if (paymentData.get(PAYMENT_INTENT_ID) != null)
				name.setEn("Credit Card");
		}

		paymentMethodInfo.setName(name);
		payment.setPaymentMethodInfo(paymentMethodInfo);
		List<Payment.Transaction> transactions = new ArrayList<>();
		Payment.Transaction transaction = new Payment.Transaction();
		Payment.Transaction.Amount amount = new Payment.Transaction.Amount();
		amount.setCentAmount(Long.parseLong((String) paymentData.get(TOTAL_AMOUNT)));
		amount.setCurrencyCode((String) paymentData.get(CURRENCY_CODE));
		transaction.setAmount(amount);
		transaction.setType("Charge");
		transaction.setState(PENDING);
		transactions.add(transaction);
		payment.setTransactions(transactions);
		return payment;
	}

	public static UpdateActionsOnCart.Action getAddPaymentRequestObj(String paymentId) {
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		action.setActionName("addPayment");
		UpdateActionsOnCart.Action.Payment payment = new UpdateActionsOnCart.Action.Payment();
		payment.setId(paymentId);
		payment.setTypeId("payment");
		action.setPayment(payment);
		return action;
	}

	public static UpdatePayment getUpdatedCTPaymentObj(String paymentType, String actionStr, long version,
			String transactionId) {
		UpdatePayment updatePayment = new UpdatePayment();
		updatePayment.setVersion(version);
		List<UpdatePayment.Action> actions = new ArrayList<>();
		UpdatePayment.Action action = new UpdatePayment.Action();
		action.setActionName(actionStr);
		if (paymentType != null && "ACH".equals(paymentType)) {
			action.setInterfaceText(PENDING);
		} else {
			action.setInterfaceText("Success");
		}
		actions.add(action);
		UpdatePayment.Action stateAction = new UpdatePayment.Action();
		stateAction.setActionName("changeTransactionState");
		stateAction.setTransactionId(transactionId);
		if (paymentType != null && "ACH".equals(paymentType)) {
			stateAction.setState(PENDING);
		} else {
			stateAction.setState("Success");
		}
		actions.add(stateAction);
		updatePayment.setActions(actions);
		return updatePayment;

	}

	

	public static boolean checkIsFreightAvailable(AccountCustomObject accountCustomObject, JsonObject cartJsonObject) {
		Value value = accountCustomObject.getValue();
		String shippingAddressId = cartJsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
		List<String> shippingaddressIds = null;
		if (accountCustomObject.getValue().getAdditionalFreightInfos() != null) {
			shippingaddressIds = accountCustomObject.getValue().getAdditionalFreightInfos().stream()
					.map(AdditionalFreightInfo::getShippingAddressId).collect(Collectors.toList());
		}
		return (value.isFreightFlag() && shippingaddressIds != null && shippingaddressIds.contains(shippingAddressId));

	}

	public static UpdateActionsOnCart.Action updateHoldCustomAttributesToOrder(String cartResponseById,
			boolean isFreightAvalible, boolean isOrderOnHold, boolean isTaxExempted) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartResponseById);
		if (jsonObject.get(STATUS_CODE) != null) {
			return null;
		}
		Set<String> holds = new HashSet<>();
		boolean isJsonObject = jsonObject.has(CUSTOM);
		if (isJsonObject) {
			UpdateActionsOnCart.Action customAction = new UpdateActionsOnCart.Action();
			customAction.setActionName(SET_CUSTOM_FIELD);
			customAction.setName(HOLDS);
			if (!isFreightAvalible) {
				holds.add(FREIGHT_HOLD);
			}
			if (isOrderOnHold) {
				holds.add("CreditHold");
			}
			if (!isTaxExempted) {
				holds.add("TaxHold");
			}
			customAction.setValue(holds);
			return customAction;
		} else {
			UpdateActionsOnCart.Action customAction = new UpdateActionsOnCart.Action();
			UpdateActionsOnCart.Action.Type type = new UpdateActionsOnCart.Action.Type();
			customAction.setActionName("setCustomType");
			type.setId("937cb0c5-55a8-4c88-af52-3bcb8c53a04c");
			type.setTypeId("type");
			customAction.setType(type);
			UpdateActionsOnCart.Action.Fields fields = new UpdateActionsOnCart.Action.Fields();
			fields.setFreightHold("Freight Hold Pending");
			customAction.setFields(fields);
			return customAction;
		}
	}

	public static UpdateActionsOnCart.Action getBillingAddressAction(String paymentType,
			AccountCustomObject accountCustomObject, String setUpIntentId) {
		Payments payment = null;
		if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentType)) {
			payment = processPaymentTerms(accountCustomObject, payment);
		} else if ("ACH".equals(paymentType)) {
			payment = processACHValue(accountCustomObject, payment);
		} else if (PAYMENT_TYPE_CREDIT_CARD.equals(paymentType)) {
			payment = processCreditCardPayment(accountCustomObject, setUpIntentId, payment);
		} else if (PaymentRequestBean.Payment.AFFIRM.name().equals(paymentType)) {
			payment = processAffirmPayment(accountCustomObject, setUpIntentId, payment);
		}
		if (payment == null) {
			return null;
		}
		if (payment.getBillingAddressId() == null) {
			return null;
		}
		BillingAddresses billingAddress = null;
		List<BillingAddresses> billingAddressList = accountCustomObject.getValue().getAddresses().getBillingAddresses();
		Iterator<BillingAddresses> billingAddressItr = billingAddressList.listIterator();
		while (billingAddressItr.hasNext()) {
			BillingAddresses billingAddress2 = billingAddressItr.next();
			if (billingAddress2.getBillingAddressId().equals(payment.getBillingAddressId())) {
				billingAddress = billingAddress2;
				break;
			}
		}
		if (billingAddress == null) {
			return null;
		}
		UpdateActionsOnCart.Action.Address address = getAddressAction(billingAddress);
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		action.setActionName("setBillingAddress");
		action.setAddress(address);
		return action;
	}
	
	public static UpdateActionsOnCart.Action getBillingAddressAction(String paymentType,
			PaymentCustomObject customPaymentResponse, String setUpIntentId, String customerResponse) {
		Payments payment = null;
		
		if(customPaymentResponse == null || customPaymentResponse.getValue() == null || customPaymentResponse.getValue().getPayments() == null) {
			return null;
		}
		List<Payments> payments = customPaymentResponse.getValue().getPayments();
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if ("CREDITCARD".equals(payments2.getPaymentType())
						&& setUpIntentId.equals(payments2.getPaymentToken())) {
					payment = payments2;
					break;
				}
			}
		}
		if (payment == null) {
			return null;
		}
		if (payment.getBillingAddressId() == null) {
			return null;
		}
		BillingAddresses billingAddress = null;
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		if(customerJsonObject.has("addresses")) {
			JsonArray addresssesJsonArray = customerJsonObject.get("addresses").getAsJsonArray();
			for(int i=0;i<addresssesJsonArray.size();i++) {
				JsonObject address = addresssesJsonArray.get(i).getAsJsonObject();
				if(address.get("id").getAsString().equals(payment.getBillingAddressId())) {
					BillingAddresses billingAddress2 = new BillingAddresses();
					billingAddress2.setBillingAddressId(address.get(ID).getAsString());
					billingAddress2.setFirstName(address.get(FIRST_NAME).getAsString());
					billingAddress2.setLastName(address.get(LAST_NAME).getAsString());
					billingAddress2.setStreetAddress1(address.get("streetName").getAsString());
					if(address.has("streetNumber")) {
						billingAddress2.setStreetAddress2(address.get("streetNumber").getAsString());					
					}
					billingAddress2.setCity(address.get("city").getAsString());
					if(address.has(STATE)) {
						billingAddress2.setState(address.get(STATE).getAsString());						
					}
					billingAddress2.setPostalCode(address.get(POSTAL_CODE).getAsString());
					billingAddress2.setPhoneNumber(address.get("phone").getAsString());
					billingAddress2.setCountry(address.get(COUNTRY).getAsString());
					billingAddress = billingAddress2;
					break;
				}
			}
		}
		if (billingAddress == null) {
			return null;
		}
		UpdateActionsOnCart.Action.Address address = getAddressAction(billingAddress);
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		action.setActionName("setBillingAddress");
		action.setAddress(address);
		return action;
	}

	private static Payments processCreditCardPayment(AccountCustomObject accountCustomObject, String setUpIntentId,
			Payments payment) {
		List<Payments> payments = accountCustomObject.getValue().getPayments();
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if ("CREDITCARD".equals(payments2.getPaymentType())
						&& setUpIntentId.equals(payments2.getPaymentToken())) {
					payment = payments2;
					break;
				}
			}
		}
		return payment;
	}
	
	
	private static Payments processAffirmPayment(AccountCustomObject accountCustomObject, String setUpIntentId,
			Payments payment) {
		List<Payments> payments = accountCustomObject.getValue().getPayments();
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (PaymentRequestBean.Payment.AFFIRM.name().equals(payments2.getPaymentType())
						&& setUpIntentId.equals(payments2.getPaymentToken())) {
					payment = payments2;
					break;
				}
			}
		}
		return payment;
	}

	private static Payments processACHValue(AccountCustomObject accountCustomObject, Payments payment) {
		List<Payments> payments = accountCustomObject.getValue().getPayments();
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if ("ACH".equals(payments2.getPaymentType())) {
					payment = payments2;
					break;
				}
			}
		}
		return payment;
	}

	private static Payments processPaymentTerms(AccountCustomObject accountCustomObject, Payments payment) {
		List<Payments> payments = accountCustomObject.getValue().getPayments();
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(payments2.getPaymentType())) {
					payment = payments2;
					break;
				}
			}
		}
		return payment;
	}

	public static UpdateActionsOnCart.Action.Address getAddressAction(BillingAddresses billingAddresses) {
		UpdateActionsOnCart.Action.Address address = new UpdateActionsOnCart.Action.Address();
		address.setFirstName(billingAddresses.getFirstName());
		address.setLastName(billingAddresses.getLastName());
		address.setStreetName(billingAddresses.getStreetAddress1());
		address.setCity(billingAddresses.getCity());
		address.setState(billingAddresses.getState());
		address.setPostalCode(billingAddresses.getPostalCode());
		address.setPhone(billingAddresses.getPhoneNumber());
		address.setCountry(billingAddresses.getCountry());
		return address;
	}

	public static PaymentOrderDTO createPaymentDto(PaymentRequest paymentRequestBean) {
		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setAmount(paymentRequestBean.getAmount());
		paymentOrderDTO.setSetupPaymentIntentId(paymentRequestBean.getPaymentToken());
		paymentOrderDTO.setCustomerId(paymentRequestBean.getCustomerId());
		paymentOrderDTO.setCartId(paymentRequestBean.getCartId());
		return paymentOrderDTO;
	}

	public static PaymentOrderDTO createOrderDto(OrderRequestBean orderRequestBean) {
		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setCartId(orderRequestBean.getCartId());
		paymentOrderDTO.setSetupPaymentIntentId(orderRequestBean.getPaymentToken());
		paymentOrderDTO.setPaymentType(orderRequestBean.getPaymentType());
		if(null != orderRequestBean.getBillingAddress()) {
			paymentOrderDTO.setBillingAddress(orderRequestBean.getBillingAddress());
		}
		paymentOrderDTO.setShopperReference(orderRequestBean.getShopperReference());
		paymentOrderDTO.setEncryptedCardNumber(orderRequestBean.getEncryptedCardNumber());
		paymentOrderDTO.setEncryptedExpiryMonth(orderRequestBean.getEncryptedExpiryMonth());
		paymentOrderDTO.setEncryptedExpiryYear(orderRequestBean.getEncryptedExpiryYear());
		paymentOrderDTO.setEncryptedSecurityCode(orderRequestBean.getEncryptedSecurityCode());
		return paymentOrderDTO;
	}

	public static boolean isTaxExempted(AccountCustomObject accountCustomObject) {
		return accountCustomObject.getValue().isTaxExemptFlag();
	}

	public static void displayRequestHeaders(HttpServletRequest serverHttpRequest) {
		Enumeration<String> headerNames = serverHttpRequest.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerValue = serverHttpRequest.getHeader(headerName);
			LOGGER.info("Header name - {} Header value - {} \n", headerName, headerValue);
		}

	}

	public static void displayResponseHeaders(HttpServletResponse serverHttpResponse) {
		Collection<String> headerNames = serverHttpResponse.getHeaderNames();
		headerNames.forEach(header -> {
			String headerValue = serverHttpResponse.getHeader(header);
			LOGGER.info("Response Header name - {} Header value - {} \n", header, headerValue);
		});
	}

}