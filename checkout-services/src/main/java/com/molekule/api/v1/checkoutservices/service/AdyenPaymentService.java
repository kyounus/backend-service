package com.molekule.api.v1.checkoutservices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.molekule.api.v1.checkoutservices.util.CheckoutUtility;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.model.checkout.AdyenCancelRequest;
import com.molekule.api.v1.commonframework.model.checkout.AdyenCaptureRequest;
import com.molekule.api.v1.commonframework.model.checkout.AdyenPaymentAuthRequest;
import com.molekule.api.v1.commonframework.model.checkout.AdyenRefundRequest;
import com.molekule.api.v1.commonframework.model.checkout.AdyenTokenRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentIntentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.model.checkout.ReccurringDetails;
import com.molekule.api.v1.commonframework.model.checkout.RefundDTO;
import com.molekule.api.v1.commonframework.service.order.AdyenPaymentHeplerService;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("adyenPaymentService")
public class AdyenPaymentService implements PaymentService {

	@Autowired
	CTEnvProperties ctEnvProperties;
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;
	
	@Autowired
	@Qualifier("adyenPaymentHeplerService")
	AdyenPaymentHeplerService adyenPaymentHeplerService;

	@Override
	public String createPaymentToken(PaymentIntentRequest paymentmRequestBean, String action, String paymentIntentId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public String authorizePayment(PaymentOrderDTO paymentOrderDTO, String currency) {
		String adyenPaymentAuthUrl = ctEnvProperties.getAdyenHost()+"/checkout/"+ctEnvProperties.getAdyenVersion()+"/payments";
		AdyenPaymentAuthRequest adyenPaymentAuthRequest = preparePaymentAuthRequest(paymentOrderDTO,currency);
		String paymentAuthResponse = (String) netConnectionHelper.sendPostWithXApiKey(adyenPaymentAuthUrl, ctEnvProperties.getXAPIKey(), adyenPaymentAuthRequest);
		JsonObject authJson = MolekuleUtility.parseJsonObject(paymentAuthResponse);
		return authJson.get("pspReference").getAsString();
	}

	@Override
	public String capturePayment(PaymentOrderDTO paymentOrderDTO,String paymentAuthToken,String currency) {
		String adyenPaymentCaptureUrl = ctEnvProperties.getAdyenHost()+"/checkout/"+ctEnvProperties.getAdyenVersion()+"/payments/"+paymentAuthToken+"/captures";
		AdyenCaptureRequest adyenCaptureRequest = prepareAdyenCaptureRequest(paymentOrderDTO,currency);
		return (String)netConnectionHelper.sendPostWithXApiKey(adyenPaymentCaptureUrl, ctEnvProperties.getXAPIKey(), adyenCaptureRequest);
	}


	@Override
	public String cancelPayment(PaymentOrderDTO paymentOrderDTO,String paymentAuthToken) {
		String adyenPaymentCancelUrl = ctEnvProperties.getAdyenHost()+"/checkout/"+ctEnvProperties.getAdyenVersion()+"/payments/"+paymentAuthToken+"/cancels";
		AdyenCancelRequest adyenCancelRequest = prepareAdyenCancelRequest(paymentOrderDTO);
		return (String)netConnectionHelper.sendPostWithXApiKey(adyenPaymentCancelUrl, ctEnvProperties.getXAPIKey(), adyenCancelRequest);
	}
	

	@Override
	public String refundPayment(Object refundObject) {
		return adyenPaymentHeplerService.refundPayment(refundObject);
	}


	private AdyenCancelRequest prepareAdyenCancelRequest(PaymentOrderDTO paymentOrderDTO) {
		AdyenCancelRequest adyenCancelRequest = new AdyenCancelRequest();
		adyenCancelRequest.setReference(paymentOrderDTO.getCartId());
		adyenCancelRequest.setMerchantAccount(ctEnvProperties.getMerchantAccount());
		return adyenCancelRequest;
	}


	private AdyenCaptureRequest prepareAdyenCaptureRequest(PaymentOrderDTO paymentOrderDTO,String currency) {
		AdyenCaptureRequest adyenCaptureRequest = new AdyenCaptureRequest();
		AdyenCaptureRequest.Amount amount = new AdyenCaptureRequest.Amount();
		amount.setCurrency(currency);
		amount.setValue(Long.parseLong(paymentOrderDTO.getAmount()));
		adyenCaptureRequest.setAmount(amount);
		adyenCaptureRequest.setReference(paymentOrderDTO.getCartId());
		adyenCaptureRequest.setMerchantAccount(ctEnvProperties.getMerchantAccount());
		return adyenCaptureRequest;
	}

	public AdyenPaymentAuthRequest preparePaymentAuthRequest(PaymentOrderDTO paymentOrderDTO,String currency) {
		AdyenPaymentAuthRequest adyenPaymentAuthRequest = new AdyenPaymentAuthRequest();
		AdyenPaymentAuthRequest.Amount amount = new AdyenPaymentAuthRequest.Amount();
		amount.setCurrency(currency);
		amount.setValue(Long.parseLong(paymentOrderDTO.getAmount()));
		adyenPaymentAuthRequest.setAmount(amount);
		AdyenPaymentAuthRequest.PaymentMethod paymentMethod = new AdyenPaymentAuthRequest.PaymentMethod();
		paymentMethod.setStoredPaymentMethodId(paymentOrderDTO.getSetupPaymentIntentId());
		adyenPaymentAuthRequest.setPaymentMethod(paymentMethod);
		adyenPaymentAuthRequest.setMerchantAccount(ctEnvProperties.getMerchantAccount());
		adyenPaymentAuthRequest.setReference(paymentOrderDTO.getCartId());
		adyenPaymentAuthRequest.setShopperInteraction(ctEnvProperties.getAdyenShopperInteraction());
		adyenPaymentAuthRequest.setRecurringProcessingModel("Subscription");
		adyenPaymentAuthRequest.setShopperReference(paymentOrderDTO.getCustomerId());
		return adyenPaymentAuthRequest;
	}


	@Override
	public String createPaymentToken(PaymentRequest paymentRequest,String currency) {
		AdyenTokenRequest adyenTokenRequest = prepareRequest(paymentRequest,currency);
		if(adyenTokenRequest == null) {
	        ReccurringDetails reccurringDetails = new ReccurringDetails();
	        reccurringDetails.setRecurringDetailReference(MolekuleUtility.createUuidValue());
	        return CheckoutUtility.convertObjToString(reccurringDetails);
		}
		String adyenPaymentsUrl =ctEnvProperties.getAdyenHost()+"/checkout/"+ctEnvProperties.getAdyenVersion()+"/payments"; 
	    String payentTokenResponse = (String) netConnectionHelper.sendPostWithXApiKey(adyenPaymentsUrl, ctEnvProperties.getXAPIKey(), adyenTokenRequest);
	    JsonObject paymentTokenJson = MolekuleUtility.parseJsonObject(payentTokenResponse);
	    if(paymentTokenJson.get("status") != null || !"Authorised".equals(paymentTokenJson.get("resultCode").getAsString())) {
	    	return payentTokenResponse;
	    }
	    ReccurringDetails reccurringDetails = new ReccurringDetails();
        reccurringDetails.setRecurringDetailReference(paymentTokenJson.get("additionalData").getAsJsonObject().get("recurring.recurringDetailReference").getAsString());
        return CheckoutUtility.convertObjToString(reccurringDetails);
	}

	@Override
	public String captureOrCancelPayment(String action, String paymentIntentId) {
		// TODO Auto-generated method stub
		return null;
	}



	private AdyenTokenRequest prepareRequest(PaymentRequest paymentRequest,String currency) {
		AdyenTokenRequest adyenTokenRequest = new AdyenTokenRequest();
		AdyenTokenRequest.Amount amount = new AdyenTokenRequest.Amount();
		amount.setCurrency(currency);
		amount.setValue(Long.parseLong(paymentRequest.getAmount()));
		adyenTokenRequest.setAmount(amount);
		adyenTokenRequest.setReference(MolekuleUtility.createUuidValue());
		AdyenTokenRequest.PaymentMethod paymentMethod = new AdyenTokenRequest.PaymentMethod();
		paymentMethod.setType(ctEnvProperties.getAdyenPaymentType());
		paymentMethod.setEncryptedCardNumber(paymentRequest.getEncryptedCardNumber());
		paymentMethod.setEncryptedExpiryMonth(paymentRequest.getEncryptedExpiryMonth());
		paymentMethod.setEncryptedExpiryYear(paymentRequest.getEncryptedExpiryYear());
		paymentMethod.setEncryptedSecurityCode(paymentRequest.getEncryptedSecurityCode());
		adyenTokenRequest.setPaymentMethod(paymentMethod);
		String shopperReferenceId = paymentRequest.getCustomerId();
		if(shopperReferenceId == null) {
			return null;
		}
		adyenTokenRequest.setShopperReference(shopperReferenceId);
		adyenTokenRequest.setShopperInteraction("Ecommerce");
		adyenTokenRequest.setStorePaymentMethod(true);
		adyenTokenRequest.setRecurringProcessingModel("Subscription");
		adyenTokenRequest.setMerchantAccount(ctEnvProperties.getMerchantAccount());
		adyenTokenRequest.setReturnUrl(null);
		return adyenTokenRequest;
	}
	
}
