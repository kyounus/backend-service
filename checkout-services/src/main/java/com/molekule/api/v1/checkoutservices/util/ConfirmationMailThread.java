package com.molekule.api.v1.checkoutservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BILLING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CENT_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIRST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.HANDLING_COST;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.IS_A_GIFT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LINE_ITEMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_TYPE_CREDIT_CARD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.POSTAL_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SERIAL_NUMBERS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_COST;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_INFO;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SUB_TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_DISCOUNT_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_TAX;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccessTokenDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerAction;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.customerprofile.DeviceRequestBean;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.model.sendgrid.Product;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.CustomerProfileHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.CustomerStatusEnum;
import com.molekule.api.v1.commonframework.util.LifeTimeSalesEnum;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.Data;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Data
public class ConfirmationMailThread implements Runnable {

	Logger logger = LoggerFactory.getLogger(ConfirmationMailThread.class);
	private PaymentOrderDTO paymentOrderdto;
	private String orderResponse;
	private String token;
	private String customerByIdUrl;
	private String customerId;
	private NetConnectionHelper netConnectionHelper;
	private TemplateMailRequestHelper orderConfirmMailSender;
	private TemplateMailRequestHelper orderConfirmD2CMailSender;
	private CtServerHelperService ctServerHelperService;
	private String ctHost;
	private String ctProjectKey;
	private String countryCode;
	private String authUrl;
	private String accessTokenUrl;
	private String grantType;
	private String basicAuthValue;
	private String prismicApiUrl;
	private String prismicApiD2CUrl;
	private String prismicApiB2BUrl;
	private boolean httpclientEnableFlag;
	private int httpClientResponseTimeout;
	private CTEnvProperties ctEnvProperties;

	@Override
	public void run() {
		String customerUrlById = ctHost + "/" + ctProjectKey
				+ MolekuleConstant.SLASH_CUSTOMERS_SLASH + customerId;
		String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerUrlById);
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		String channel = "D2C";
		if (customerObject.has(CUSTOM)) {
			JsonObject fieldObject = customerObject.get(CUSTOM).getAsJsonObject().get("fields").getAsJsonObject();
			channel = fieldObject.get(CHANNEL).getAsString();
			if (fieldObject.has(MolekuleConstant.CUSTOMER_STATUS) && fieldObject.get(MolekuleConstant.CUSTOMER_STATUS)
					.getAsString().equals(CustomerStatusEnum.REGISTERED.toString())) {
				CustomerAction customerAction = new CustomerAction();
				customerAction.setVersion(customerObject.get(VERSION).getAsLong());
				List<CustomerAction.Actions> list = new ArrayList<>();
				CustomerAction.Actions actionsObj = new CustomerAction.Actions();
				actionsObj.setAction("setCustomField");
				actionsObj.setName(MolekuleConstant.CUSTOMER_STATUS);
				actionsObj.setValue(CustomerStatusEnum.ACTIVE.toString());
				list.add(actionsObj);
				customerAction.setActions(list);
				netConnectionHelper.sendPostRequest(customerUrlById, token, customerAction);
			}
		}
		SendGridModel sendGridModel = getSendGridRequestData(orderResponse);
		if(customerObject.has(FIRST_NAME)) {
			sendGridModel.setFirstName(customerObject.get(FIRST_NAME).getAsString());			
		}
		if (paymentOrderdto.getPaymentType() == null) {
			sendGridModel.setOrderPaymentMethod(PAYMENT_TYPE_CREDIT_CARD);
		} else {
			sendGridModel.setOrderPaymentMethod(paymentOrderdto.getPaymentType());
		}
		if(channel.equals("B2B"))
			orderConfirmMailSender.sendMail(sendGridModel);
		if(channel.equals("D2C"))
			orderConfirmD2CMailSender.sendMail(sendGridModel);
		CustomerProfileHelperService.setLifeTimeSales(netConnectionHelper, customerResponse, orderResponse, 
				LifeTimeSalesEnum.ADD_ORDER_TOTAL, customerUrlById, token, "0");
		registerDevice(orderResponse);

	}

	private void registerDevice(String response) {
		JsonObject orderObject = MolekuleUtility.parseJsonObject(response);
		String purchaseDate = orderObject.get("createdAt").getAsString().substring(0, 10);
		JsonObject fieldsObject = new JsonObject();
		if(orderObject.has(CUSTOM) && orderObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			fieldsObject = orderObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}
		boolean isGift = false;
		if(fieldsObject.has("gift")) {
			isGift = fieldsObject.get("gift").getAsBoolean();
		}
		String channel = null;
		if(fieldsObject.has(CHANNEL)) {
			channel = fieldsObject.get(CHANNEL).getAsString();
		}
		String retailSeller = null;
		if(fieldsObject.has("retailSeller")) {
			retailSeller = fieldsObject.get("retailSeller").getAsString();
		}

		JsonArray lineItemArray = orderObject.get("lineItems").getAsJsonArray();
		for(int i=0; i<lineItemArray.size(); i++) {
			JsonObject lineItemObject = lineItemArray.get(i).getAsJsonObject();
			createDevice(purchaseDate, isGift, channel, retailSeller, lineItemObject);
		}
	}

	public void createDevice(String purchaseDate, boolean isGift, String channel, String retailSeller,
			JsonObject lineItemObject) {
		String sku = null;
		String serialNumber = null;
		int count = 0;
		if(lineItemObject.has("variant")) {
			JsonObject variantObject = lineItemObject.get("variant").getAsJsonObject();
			sku = variantObject.has("sku") ? variantObject.get("sku").getAsString() : null;
			if(variantObject.has("attributes")) {
				JsonArray attributeArray = variantObject.get("attributes").getAsJsonArray();
				count = getCount(count, attributeArray);
			}
		}
		serialNumber = getSerialNumber(lineItemObject, serialNumber);
		if(count == 0) {
			DeviceRequestBean deviceRequestBean = new DeviceRequestBean();
			deviceRequestBean.setChannel(channel);
			deviceRequestBean.setCustomerId(customerId);
			deviceRequestBean.setGift(isGift);
			deviceRequestBean.setPurchaseDate(purchaseDate);
			deviceRequestBean.setRetailSeller(retailSeller);
			deviceRequestBean.setSerialNumber(serialNumber);
			deviceRequestBean.setSku(sku);
			CustomerProfileHelperService.registerDevice(deviceRequestBean, token,ctHost,ctProjectKey, netConnectionHelper);	
		}
	}

	public String getSerialNumber(JsonObject lineItemObject, String serialNumber) {
		if(lineItemObject.has(CUSTOM) && lineItemObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			JsonObject customField = lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(customField.has(SERIAL_NUMBERS)) {
				serialNumber = customField.get(SERIAL_NUMBERS).getAsJsonArray().size()>0 ? customField.get(SERIAL_NUMBERS).getAsJsonArray().get(0).getAsString() : null;
			}
		}
		return serialNumber;
	}

	public int getCount(int count, JsonArray attributeArray) {
		for(int j=0; j<attributeArray.size(); j++) {
			JsonObject attributeObject = attributeArray.get(j).getAsJsonObject();
			if(attributeObject.has("name") && attributeObject.get("name").getAsString().equals("filterProduct")) {
				count ++;
			}
		}
		return count;
	}

	public SendGridModel getSendGridRequestData(String orderResponse) {
		SendGridModel sendGridModel = new SendGridModel();
		try {
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
			String channel = jsonObject.getAsJsonObject(CUSTOM).getAsJsonObject("fields").get(CHANNEL).getAsString();
			sendGridModel.setEmail(jsonObject.get("customerEmail").getAsString());
			sendGridModel.setOrderNumber(jsonObject.get(ORDER_NUMBER).getAsString());
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(IS_A_GIFT)) {
				sendGridModel.setGift(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(IS_A_GIFT).getAsBoolean());
			}else {
				sendGridModel.setGift(false);
			}
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(SUB_TOTAL)) {
				sendGridModel.setOrderSummarySubtotal(centToDollarConversion(
						jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(SUB_TOTAL).getAsLong()));
			}
			if(countryCode.equals("US") && jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(TOTAL_TAX)) {
				sendGridModel.setOrderSummaryTax(centToDollarConversion(
						jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(TOTAL_TAX).getAsLong()));
			}
			if(countryCode.equals("GB")) {
				sendGridModel.setTaxNote("Including VAT");
			}
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(SHIPPING_COST)) {
				sendGridModel.setOrderSummaryShipping(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject().get(SHIPPING_COST).getAsLong()));
			}
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(HANDLING_COST)) {
				sendGridModel.setOrderSummaryHandling(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject().get(HANDLING_COST).getAsLong()));
			}
			if (jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
					.get(TOTAL_DISCOUNT_PRICE) != null) {
				sendGridModel.setOrderSummaryDiscount(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject().get(TOTAL_DISCOUNT_PRICE).getAsLong()));
			}
			sendGridModel.setOrderSummaryTotal(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
					.get(FIELDS).getAsJsonObject().get(ORDER_TOTAL).getAsLong()));
			JsonArray lineItemsArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
			List<Map<String,Object>> productlist = new ArrayList<Map<String,Object>>();
			if(channel.equals("B2B")) {
				getProductDetail(lineItemsArray,channel);
			}else if(channel.equals("D2C")) {
				for (int i = 0; i < lineItemsArray.size(); i++) {
					Map<String,Object> productMap = new HashMap<String,Object>();
					JsonObject lineitemJson = lineItemsArray.get(i).getAsJsonObject();
					if(lineitemJson.has(CUSTOM)&&lineitemJson.get(CUSTOM).getAsJsonObject().has(FIELDS)&&
							lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has("subscriptionEnabled")){
						productMap.put("subscription", lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().
								get("subscriptionEnabled").getAsBoolean());
						if(countryCode.equals("US")) {
							productMap.put("subscriptionHeader", lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());
						}else {
							productMap.put("subscriptionHeader", lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());
						}
						productMap.put("SubscriptionPrice", centToDollarConversion(
								lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong()));
						productMap.put("orderImageUrl",getProductImage(lineitemJson.get("productId").getAsString(),channel));
						productlist.add(productMap);
					}else {
						if(countryCode.equals("US")) {
							productMap.put("orderProductName", lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());	
						}else{
							productMap.put("orderProductName", lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());	
						}
						productMap.put("orderProductQty",lineitemJson.get(QUANTITY).getAsLong());
						productMap.put("orderProductPrice", centToDollarConversion(
								lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong()));
						productMap.put("orderImageUrl",getProductImage(lineitemJson.get("productId").getAsString(),channel));
						productlist.add(productMap);
					}
				}
			}
			sendGridModel.setProductlist(productlist);
			if (jsonObject.get(SHIPPING_INFO) != null) {
				sendGridModel.setOrderShippingMethod(
						jsonObject.get(SHIPPING_INFO).getAsJsonObject().get("shippingMethodName").getAsString());
			}
			if (jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("company") != null) {
				sendGridModel.setOrderShippingAddressCompanyName(
						jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("company").getAsString());
			}
			sendGridModel.setOrderShippingAddressContactFirstName(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(FIRST_NAME).getAsString());
			sendGridModel.setOrderShippingAddressContactLastName(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("lastName").getAsString());
			sendGridModel.setOrderShippingAddressStreet1(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("streetName").getAsString());
			sendGridModel.setOrderShippingAddressCity(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("city").getAsString());
			if (countryCode.equals("US") && jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has(STATE)){
				sendGridModel.setOrderShippingAddressState(
						jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(STATE).getAsString());
			}
			sendGridModel.setOrderShippingAddressZip(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(POSTAL_CODE).getAsString());
			if(jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has(PHONE))
				sendGridModel.setOrderShippingAddressPhoneNumber(
						jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(PHONE).getAsString());

			sendGridModel.setOrderBillingAddressFirstName(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(FIRST_NAME).getAsString());
			sendGridModel.setOrderBillingAddressLastName(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get("lastName").getAsString());
			sendGridModel.setOrderBillingAddressStreet1(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get("streetName").getAsString());
			sendGridModel.setOrderBillingAddressCity(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get("city").getAsString());
			if(countryCode.equals("US") && jsonObject.get(BILLING_ADDRESS).getAsJsonObject().has(STATE)) {
				sendGridModel.setOrderBillingAddressState(
						jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(STATE).getAsString());
			}
			sendGridModel.setOrderBillingAddressZip(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(POSTAL_CODE).getAsString());
			sendGridModel.setOrderBillingAddressPhoneNumber(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(PHONE).getAsString());

		} catch (Exception e) {
			logger.error("Error occured in getSendGridRequestData - ", e);
		}
		return sendGridModel;
	}

	public String centToDollarConversion(long centPayment) {
		NumberFormat numberFormat = null;
		if(countryCode.equals("US")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		}else if(countryCode.equals("GB")){
			numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		}
		return numberFormat.format(centPayment / 100.0);
	}

	private List<Product> getProductDetail(JsonArray lineItemsArray,String channel){
		List<Product> products = new ArrayList<>();
		for (int i = 0; i < lineItemsArray.size(); i++) {
			Product product = new Product();
			JsonObject lineitemJson = lineItemsArray.get(i).getAsJsonObject();
			if(lineitemJson.has(CUSTOM)&&lineitemJson.get(CUSTOM).getAsJsonObject().has(FIELDS)&&
					lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has("subscriptionEnabled")){
				product.setSubscription(lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().
						get("subscriptionEnabled").getAsBoolean());
				if(countryCode.equals("US")) {
					product.setSubscriptionHeader(lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());
				}else {
					product.setSubscriptionHeader(lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());
				}
				product.setSubscriptionPrice(centToDollarConversion(
						lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong()));
				product.setOrderImageUrl(getProductImage(lineitemJson.get("productId").getAsString(),channel));
				products.add(product);
			}else {
				if(countryCode.equals("US")) {
					product.setOrderProductName(lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());
				}else {
					product.setOrderProductName(lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());
				}
				product.setOrderProductQty(lineitemJson.get(QUANTITY).getAsLong());
				product.setOrderProductPrice(centToDollarConversion(
						lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong()));
				product.setOrderImageUrl(getProductImage(lineitemJson.get("productId").getAsString(),channel));
				products.add(product);
			}
		}
		return products;
	}

	private String getProductImage(String productId,String channel) {
		String sku = null;
		String prismicImageResponse = null;
		String prismicImageUrl = null;
		try {
			String token = getStringAccessToken();
			String url = new StringBuilder().append(ctHost).append("/")
					.append(ctProjectKey).append("/products/").append(productId)
					.append("?country=").append(countryCode).toString();
			ProductBean productBeanResponseData = netConnectionHelper.sendGetProductDataWithoutBody(token, url);
			ProductData current = productBeanResponseData.getMasterData().getCurrent();
			/**slug = current.getSlug().get(EN_US);*/
			sku = current.getMasterVariant().getSku();
			if(countryCode.equals("GB")) {
				prismicImageUrl = prismicApiUrl+sku+"?country="+countryCode;
			}else if(countryCode.equals("US")) {
				if(channel.equals("D2C")) {
					prismicImageUrl = prismicApiD2CUrl+sku+"?country="+countryCode;
				}else if(channel.equals("B2B")) {
					prismicImageUrl = prismicApiB2BUrl+sku+"?country="+countryCode;
				}
			}
			prismicImageResponse = netConnectionHelper.sendGetWithoutBody(null, prismicImageUrl);
			prismicImageResponse = prismicImageResponse.replace("prismic-images", "images.prismic.io");
			prismicImageResponse = "https:/"+prismicImageResponse;
		}catch(Exception e) {
			logger.error("Error occured in getSendGridRequestData - ", e);
		}
		return prismicImageResponse;
	}

	private String getStringAccessToken() {
		String accessToken = getAccessToken();
		return new StringBuilder().append(BEARER).append(accessToken).toString();
	}

	private String getAccessToken() {
		String baseUrl = authUrl + "/" + accessTokenUrl + "?grant_type="
				+ grantType;
		logger.trace("Url...{}", baseUrl);
		String token = new StringBuilder().append("Basic ").append(basicAuthValue).toString();

		WebClient webClient = fetchWebclient(baseUrl);
		Mono<Object> accessToken = webClient.post().accept(MediaType.APPLICATION_JSON)
				.header("Authorization", token).exchangeToMono(clientResponse -> {
					logger.trace("Access Token Api Status Code - {}", clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						throw new CustomRunTimeException("Unable to get Access Token From the CT Server");
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
						return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty()));
					}
					return clientResponse.bodyToMono(AccessTokenDTO.class);
				});
		AccessTokenDTO accessTokenResponse = (AccessTokenDTO) accessToken.block();
		if(accessTokenResponse != null) {
			return accessTokenResponse.getAccessToken();
		}
		return null;
	}

	public WebClient fetchWebclient(String url) {
		if(isHttpclientEnableFlag()) {
			HttpClient client = HttpClient.create().doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(httpClientResponseTimeout)).addHandlerLast(new WriteTimeoutHandler(httpClientResponseTimeout)));
			client.disableRetry(true);
			ClientHttpConnector connector = new ReactorClientHttpConnector(client.wiretap(true));
			return WebClient.builder().clientConnector(connector).baseUrl(url).build();
		}
		return WebClient.builder().baseUrl(url).build();
	}

}
