package com.molekule.api.v1.checkoutservices.util;

public enum ErrorCode {
	  PAYMENT_TYPE(1000, "Payment Type not found.");

	  private final int code;
	  private final String description;

	  private ErrorCode(int code, String description) {
	    this.code = code;
	    this.description = description;
	  }

	  public String getDescription() {
	     return description;
	  }

	  public int getCode() {
	     return code;
	  }

	  @Override
	  public String toString() {
	    return code + ": " + description;
	  }
	}
