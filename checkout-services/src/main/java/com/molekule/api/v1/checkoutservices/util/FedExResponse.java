package com.molekule.api.v1.checkoutservices.util;

import lombok.Data;

/**
 * THe class FedExResponse is used to hold the response data for fedEx shipping methods.
 *
 */
@Data
public class FedExResponse {
	String serviceType;
	String serviceName;
	String cost;
	String duration;
	String displayName;
}
