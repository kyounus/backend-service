package com.molekule.api.v1.checkoutservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.checkoutservices.util.CheckoutUtility;
import com.molekule.api.v1.checkoutservices.util.ConfirmationMailThread;
import com.molekule.api.v1.checkoutservices.util.ErrorCode;
import com.molekule.api.v1.checkoutservices.util.FedExResponse;
import com.molekule.api.v1.checkoutservices.util.PaymentTypeNotFound;
import com.molekule.api.v1.checkoutservices.util.PendingMailThread;
import com.molekule.api.v1.checkoutservices.util.UpdatePaymentThread;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean.ShippingMethod;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartCustomServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCustomerToCart;
import com.molekule.api.v1.commonframework.dto.cart.WarrentyCustomFieldServiceBean;
import com.molekule.api.v1.commonframework.dto.order.WarrentyOrderServiceBean;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerAction;
import com.molekule.api.v1.commonframework.dto.registration.CustomerAction.Actions;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Payments.TermsStatus;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.cart.AvalaraRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.AdyenPaymentAuthRequest;
import com.molekule.api.v1.commonframework.model.checkout.AffirmCaptureRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.AffirmCheckoutBean;
import com.molekule.api.v1.commonframework.model.checkout.CheckoutSurveyResponse;
import com.molekule.api.v1.commonframework.model.checkout.CheckoutSurveyResponse.CheckoutSurvey;
import com.molekule.api.v1.commonframework.model.checkout.ClientSecret;
import com.molekule.api.v1.commonframework.model.checkout.DiscountCodeRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.OrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.OrderEmailResponse;
import com.molekule.api.v1.commonframework.model.checkout.OrderRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.Payment;
import com.molekule.api.v1.commonframework.model.checkout.PaymentIdUpdatePayment;
import com.molekule.api.v1.commonframework.model.checkout.PaymentIntentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentTypeServiceBean;
import com.molekule.api.v1.commonframework.model.checkout.PaymentTypeServiceBean.Action;
import com.molekule.api.v1.commonframework.model.checkout.SetTaxAmount;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;
import com.molekule.api.v1.commonframework.model.registration.CustomerRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ForgotPasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.sendgrid.Product;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.cart.TaxService;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.CustomerProfileHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.CartServiceUtility;
import com.molekule.api.v1.commonframework.util.CountryCodeEnum;
import com.molekule.api.v1.commonframework.util.CustomerChannelEnum;
import com.molekule.api.v1.commonframework.util.CustomerServiceUtility;
import com.molekule.api.v1.commonframework.util.CustomerSourceEnum;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

/**
 * The class CheckoutService is used to write the api calls for the checkout.
 * 
 * @version 1.0
 */
@Service("checkoutService")
public class CheckoutService {

	Logger logger = LoggerFactory.getLogger(CheckoutService.class);

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("cartHelperService")
	CartHelperService cartHelperService;

	@Autowired
	@Qualifier("checkoutHelperService")
	CheckoutHelperService checkoutHelperService;

	@Autowired
	@Qualifier("orderHelperService")
	OrderHelperService orderHelperService;

	@Autowired
	@Qualifier("adyenPaymentService")
	AdyenPaymentService adyenPaymentService;
	@Autowired
	@Qualifier("fedExApiService")
	FedExApiService fedExApiService;

	//	@org.springframework.beans.factory.annotation.Value("${currencyValue}")
	//	private String currencyValue;

	@Value("${companyCode}")
	private String companyCode;
	@Value("${type}")
	private String type;

	enum PaymentIntentOptions {
		PRE_AUTH, CAPTURE, CANCEL
	}

	@org.springframework.beans.factory.annotation.Value("${paymentIntentHost}")
	private String paymentIntentUrl;
	@org.springframework.beans.factory.annotation.Value("${stripeApiKey}")
	private String stripeAuthKey;
	@org.springframework.beans.factory.annotation.Value("${paymentMethodUrl}")
	private String paymentMethodUrl;
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;
	@Autowired
	@Qualifier("taxService")
	TaxService taxService;
	@org.springframework.beans.factory.annotation.Value("${setupPaymentIntentUrl}")
	private String setupPaymentIntentUrl;
	@org.springframework.beans.factory.annotation.Value("${createCustomerUrl}")
	private String createCustomerUrl;

	@Autowired
	@Qualifier("orderConfirmMailSender")
	TemplateMailRequestHelper orderConfirmMailSender;
	@Autowired
	@Qualifier("orderPendingMailSender")
	TemplateMailRequestHelper orderPendingMailSender;
	@Autowired
	@Qualifier(value = "orderPendingSalesRepMailSender")
	TemplateMailRequestHelper orderPendingSalesRepMailSender;
	@Autowired
	@Qualifier(value = "freightInfoPendingMailSender")
	TemplateMailRequestHelper freightInfoPendingMailSender;
	@Autowired
	@Qualifier(value = "paymentPendingMailSender")
	TemplateMailRequestHelper paymentPendingMailSender;

	@Autowired
	@Qualifier("orderInvoiceMailSender")
	TemplateMailRequestHelper orderInvoiceMailSender;

	@Autowired
	@Qualifier("orderConfirmD2CMailSender")
	TemplateMailRequestHelper orderConfirmD2CMailSender;

	@org.springframework.beans.factory.annotation.Value("${holdStateId}")
	private String holdStateId;

	@Autowired
	@Qualifier("stripePaymentService")
	PaymentService stripePaymentService;

	@Autowired
	@Qualifier("customerProfileHelperService")
	private CustomerProfileHelperService customerProfileHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	@Value("#{${counrty.alpha.codes}}") 
	private Map<String,String> countryAlphaCodes;

	public String taxCalculate(String orderNumber) {
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResponse = orderHelperService.getOrderByNumber(orderNumber, token);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		if (orderResponse == null || orderJsonObject.get("statusCode") != null) {
			ErrorResponse errorResponse = new ErrorResponse(400, "Invalid Order Number");
			return errorResponse.toString();
		}
		AvalaraRequestBean avalaraRequestBean = CartServiceUtility.getAvalaraRequestBean(orderResponse);
		avalaraRequestBean.setCompanyCode(companyCode);
		avalaraRequestBean.setType(type);
		String taxResponse = null;
		try {
			taxResponse = taxService.calculateTaxByAvalara(avalaraRequestBean);
		} catch (Exception e) {
			return sendTaxCalculatedOrderEditResponse(orderNumber, orderJsonObject, token);
		}
		SetTaxAmount setTaxAmount = CartServiceUtility.updateTaxToCart(taxResponse, orderResponse, true);
		if (setTaxAmount == null) {
			return sendTaxCalculatedOrderEditResponse(orderNumber, orderJsonObject, token);
		}
		setTaxAmount.setStagedActions(setTaxAmount.getActions());
		setTaxAmount.setActions(null);
		setTaxAmount.setKey(ORDER + orderNumber);
		SetTaxAmount.Resource resource = new SetTaxAmount.Resource();
		resource.setTypeId(ORDER);
		resource.setId(orderJsonObject.get("id").getAsString());
		setTaxAmount.setResource(resource);
		String createOrderEditUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders/edits";
		return (String) netConnectionHelper.sendPostRequest(createOrderEditUrl, token, setTaxAmount);
	}

	private String sendTaxCalculatedOrderEditResponse(String orderNumber, JsonObject orderJsonObject, String token) {
		SetTaxAmount setTaxAmount = new SetTaxAmount();
		List<SetTaxAmount.Action> actions = new ArrayList<>();
		SetTaxAmount.Action action = new SetTaxAmount.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName("isTaxCalculated");
		action.setValue(Boolean.FALSE.toString());
		actions.add(action);
		setTaxAmount.setStagedActions(actions);
		setTaxAmount.setKey(ORDER + orderNumber);
		SetTaxAmount.Resource resource = new SetTaxAmount.Resource();
		resource.setTypeId(ORDER);
		resource.setId(orderJsonObject.get("id").getAsString());
		setTaxAmount.setResource(resource);
		String createOrderEditUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders/edits";
		return (String) netConnectionHelper.sendPostRequest(createOrderEditUrl, token, setTaxAmount);

	}

	public String applyPromoCode(String cartId, String promoCode) {
		return checkoutHelperService.applyPromoCode(cartId, promoCode);

	}

	public String removePromoCode(String cartId, String promoCode, String country) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		String url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId +"?expand=discountCodes[*].discountCode.typeId";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String cartResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		if(cartJsonObject.has(STATUS_CODE) || cartJsonObject.get(DISCOUNT_CODES).getAsJsonArray().size() == 0) {
			logger.trace("Remove Discount to Cart Response - {}", cartResponse);
			return cartResponse;
		}
		String discountCodeId = getDiscountCodeId(promoCode);
		long cartVersion = cartHelperService.getCartCurrentVersionByCartId(cartId);
		DiscountCodeRequestBean disCodeRequestBean = new DiscountCodeRequestBean();
		disCodeRequestBean.setVersion(cartVersion);
		DiscountCodeRequestBean.Action action = new DiscountCodeRequestBean.Action();
		action.setActionName("removeDiscountCode");
		DiscountCodeRequestBean.Action.DiscountCode discountCode = new DiscountCodeRequestBean.Action.DiscountCode();
		discountCode.setTypeId("discount-code");
		discountCode.setId(discountCodeId);
		action.setDiscountCode(discountCode);
		List<DiscountCodeRequestBean.Action> actions = new ArrayList<>();
		actions.add(action);
		disCodeRequestBean.setActions(actions);
		String response = cartHelperService.calculationsAfterPromoCode(url, token, disCodeRequestBean, cartId,currency);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		if(jsonObject.get(STATUS_CODE) != null) {
			return response;
		}
		JsonObject fieldsJsonObject = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		if(fieldsJsonObject.has(DISCOUNT_MESSAGE)) {
			response = checkoutHelperService.setDiscountMessage(jsonObject, url, token, DISCOUNT_MESSAGE, "");
		}
		logger.trace("Remove Discount to Cart Response - {}", response);
		return response;
	}

	public String getDiscountCodeId(String promoCode) {

		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/discount-codes?where=code=\"").append(promoCode)
				.append("\"").toString();
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String response = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject cartObject = MolekuleUtility.parseJsonObject(response);
		JsonArray jsonArray = cartObject.getAsJsonArray(RESULTS).getAsJsonArray();
		JsonObject cart = jsonArray.get(0).getAsJsonObject();
		String discountCode = null;
		if (cart != null) {
			discountCode = cart.get("id").getAsString();
		}
		return discountCode;
	}

	/**
	 * createPaymentIntent method is to create payment intent,capture the payment or
	 * cancel the payment based on action
	 * 
	 * 
	 * @param paymentmRequestBean,action,paymentIntentId
	 * @return String
	 */
	public String createPaymentIntent(PaymentIntentRequest paymentmRequestBean, String action, String paymentIntentId) {
		String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
		JsonObject jsonObject = null;
		// create payment intent
		if (PaymentIntentOptions.PRE_AUTH.name().equals(action)) {
			// get customer and payment method by Retrieve setup payment intent by id
			String retrievePaymentIntentBaseUrl = setupPaymentIntentUrl + "/"
					+ paymentmRequestBean.getSetUpPaymentIntentId();
			String setupPaymentIntentResponse = netConnectionHelper.sendGetWithoutBody(authToken,
					retrievePaymentIntentBaseUrl);
			jsonObject = MolekuleUtility.parseJsonObject(setupPaymentIntentResponse);
			String paymentMethodId = jsonObject.get("payment_method").getAsString();
			String customerId = jsonObject.get(CUSTOMER).getAsString();
			MultiValueMap<String, String> intentFormData = new LinkedMultiValueMap<>();
			intentFormData.add("amount", paymentmRequestBean.getAmount());
			intentFormData.add("currency", paymentmRequestBean.getCurrency());
			intentFormData.add("capture_method", "manual");
			intentFormData.add("payment_method", paymentMethodId);
			intentFormData.add("confirm", "true");
			intentFormData.add(CUSTOMER, customerId);
			return (String) netConnectionHelper.sendFormUrlEncoded(paymentIntentUrl, authToken, intentFormData);
		}
		if (PaymentIntentOptions.CAPTURE.name().equals(action)) {

			// Capture the payment intent
			String captureIntentUrl = paymentIntentUrl + "/" + paymentIntentId + "/capture";
			MultiValueMap<String, String> captureIntentFormData = new LinkedMultiValueMap<>();
			return (String) netConnectionHelper.sendFormUrlEncoded(captureIntentUrl, authToken, captureIntentFormData);
		}
		if (PaymentIntentOptions.CANCEL.name().equals(action)) {
			// Cancel the payment intent
			String cancelIntentUrl = paymentIntentUrl + "/" + paymentIntentId + "/cancel";
			MultiValueMap<String, String> cancelIntentFormData = new LinkedMultiValueMap<>();
			return (String) netConnectionHelper.sendFormUrlEncoded(cancelIntentUrl, authToken, cancelIntentFormData);
		}

		return null;
	}

	public String createPayment(PaymentRequest paymentRequest, String country) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		PaymentIntentRequest paymentIntentRequest = null;
		JsonObject jsonObject = null;
		String captureResponse = null;
		String paymentToken = null;
		if ("US".equals(country) || "CA".equals(country)) {
			paymentIntentRequest = getPaymentIntentBean(paymentRequest,currency);
			String paymentIntentResponse = stripePaymentService.createPaymentToken(paymentIntentRequest,
					PaymentIntentOptions.PRE_AUTH.name(), null);
			jsonObject = MolekuleUtility.parseJsonObject(paymentIntentResponse);
			if (jsonObject.get("error") != null) {
				return paymentIntentResponse;
			}
			paymentToken = jsonObject.has("id") ? jsonObject.get("id").getAsString() : null;
			captureResponse = stripePaymentService.captureOrCancelPayment(PaymentIntentOptions.CAPTURE.name(),
					paymentToken);
		} else {
			PaymentOrderDTO paymentOrderDTO = preparePaymentDto(paymentRequest);
			String pspReferenceId = adyenPaymentService.authorizePayment(paymentOrderDTO, CountryCodeEnum.valueOf(country).getCurrency());
			captureResponse = adyenPaymentService.capturePayment(paymentOrderDTO, pspReferenceId, currency);
		}
		return captureResponse;
	}

	private PaymentOrderDTO preparePaymentDto(PaymentRequest paymentRequest) {
		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setAmount(paymentRequest.getAmount());
		paymentOrderDTO.setCartId(paymentRequest.getCartId());
		paymentOrderDTO.setCustomerId(paymentRequest.getCustomerId());
		paymentOrderDTO.setSetupPaymentIntentId(paymentRequest.getPaymentToken());
		return paymentOrderDTO;
	}

	public String getPaymentToken(PaymentRequest paymentRequest,String country) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		String customerId = null;
		if ("US".equals(country) || "CA".equals(country)){
			if (paymentRequest.getCartId() != null) {
				String cartResponse = cartHelperService.getCartById(paymentRequest.getCartId());
				JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
				customerId =  jsonObject.get(CUSTOMER_ID).getAsString();
			}else {
				customerId = paymentRequest.getCustomerId();
			}
			return createPaymentAuth(customerId);
		} else   {
			return adyenPaymentService.createPaymentToken(paymentRequest,currency);
		}
	}
	private PaymentIntentRequest getPaymentIntentBean(PaymentRequest paymentRequest, String currency) {
		PaymentIntentRequest createPaymentIntentBean= new PaymentIntentRequest();
		createPaymentIntentBean.setAmount(paymentRequest.getAmount());
		createPaymentIntentBean.setSetUpPaymentIntentId(paymentRequest.getPaymentToken());
		createPaymentIntentBean.setCurrency(currency);
		return createPaymentIntentBean;
	}


	private void updatePaymentCardToCustomer(String customerId, JsonObject cartJson,PaymentOrderDTO paymentOrderDTO,String ctToken) {
		String country = cartJson.get("country").getAsString();
		CustomerRequestBean customerRequestBean = new CustomerRequestBean();
		customerRequestBean.setAction(CustomerServiceUtility.CustomerActions.ADD_PAYMENT.name());
		customerRequestBean.setPaymentType("CREDITCARD");
		if("US".equals(country) || "CA".equals(country)) {
			customerRequestBean.setPaymentToken(paymentOrderDTO.getSetupPaymentIntentId());
		}else {
			customerRequestBean.setPaymentToken(paymentOrderDTO.getSetupPaymentIntentId());
		}
		customerRequestBean.setBillingAddress(paymentOrderDTO.getBillingAddress());
		customerProfileHelperService.updateCustomerById(customerId, customerRequestBean,country,ctToken,paymentOrderDTO.getShopperReference());
	}

	private Map<String, String> createUserAccount(JsonObject cartJson,String ctToken, String country, OrderRequestBean orderRequestBean) {
		String customerId = null;
		String customerNumber = null;
		Map<String, String> responseMap = new HashMap<>();
		String email = cartJson.get("customerEmail").getAsString();
		String firstName = cartJson.get(SHIPPING_ADDRESS).getAsJsonObject().get("firstName").getAsString().toUpperCase();
		String lastName = cartJson.get(SHIPPING_ADDRESS).getAsJsonObject().get("lastName").getAsString().toUpperCase();
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/customers?where=";
		String emailStr = "email=\"" + email + "\"";
		customerBaseUrl = customerBaseUrl + MolekuleUtility.convertToEncodeUri(emailStr);
		String customerResponse = null;
		try {
			customerResponse = netConnectionHelper.httpConnectionHelper(customerBaseUrl, ctToken);
		} catch (IOException e) {
			logger.error("IOException Occured", e);
		}
		JsonObject customerJson = MolekuleUtility.parseJsonObject(customerResponse);
		JsonArray customerArray = customerJson.get(RESULTS).getAsJsonArray();
		if (customerArray.size() <= 0) {
			String registrationUrl = ctEnvProperties.getUserAccountUrl() + "/api/v1/user/signup?country="+country;		
			String storeId;
			RegistrationBean registrationBean = new RegistrationBean();
			registrationBean.setEmail(email);
			registrationBean.setFirstName(firstName);
			registrationBean.setLastName(lastName);
			registrationBean.setMobileNumber("");
			registrationBean.setChannel(CustomerChannelEnum.D2C);
			registrationBean.setRegisteredChannel("MO");
			registrationBean.setCustomerSource(CustomerSourceEnum.D2C_WEB);
			registrationBean.setTermsOfService(orderRequestBean.isTermsOfService());
			registrationBean.setMarketingOffers(orderRequestBean.isMarketingOffers());
			if("GB".equals(cartJson.get("country").getAsString())) {
				storeId = ctEnvProperties.getUkStoreId();
			} else if("US".equals(cartJson.get("country").getAsString())) {
				storeId = ctEnvProperties.getUsStoreId();
			} else if("CA".equals(cartJson.get("country").getAsString())){
				storeId = ctEnvProperties.getCaStoreId();
			}else {
				storeId = ctEnvProperties.getEuStoreId();
			}
			String storeUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/stores/" + storeId;
			String storeResponse = netConnectionHelper.sendGetWithoutBody(ctToken, storeUrl);
			JsonObject storeJsonObject = MolekuleUtility.parseJsonObject(storeResponse);
			registrationBean.setStore(storeJsonObject.get("key").getAsString());
			customerResponse = (String) netConnectionHelper.sendPostRequest(registrationUrl, null, registrationBean);
			customerJson= MolekuleUtility.parseJsonObject(customerResponse);
			customerId = customerJson.get("customerId").getAsString();		
			customerNumber = customerJson.get(CUSTOMER_NUMBER).getAsString();
			// create password call
			ForgotPasswordRequestBean forgotPasswordRequestBean = new ForgotPasswordRequestBean();
			forgotPasswordRequestBean.setEmail(email);
			registrationHelperService.forgotPassword(forgotPasswordRequestBean, "createPassword");
		}else {
			customerJson = customerArray.get(0).getAsJsonObject();
			customerId = customerJson.get("id").getAsString();
			customerNumber = customerJson.get(CUSTOMER_NUMBER).getAsString();
		}
		responseMap.put(CUSTOMER_ID , customerId);
		responseMap.put(CUSTOMER_NUMBER, customerNumber);
		return responseMap;

	}

	private boolean isCreateUser(JsonObject cartJson,String ctToken) {
		JsonObject cartCustomFields = cartJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		boolean gift = false;
		if(cartCustomFields.has("gift")) {
			gift = cartCustomFields.get("gift").getAsBoolean();
		}
		boolean hasAnonymousId = cartJson.has(ANONYMOUSID);
		boolean purifierExist = checkCartProducts(cartJson, ctToken);
		return (hasAnonymousId && !gift && purifierExist);
	}

	public String createOrder(PaymentOrderDTO paymentOrderdto, String ctToken,String country, OrderRequestBean orderRequestBean) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		String customerId = null;
		String cartResponse = cartHelperService.getCartById(paymentOrderdto.getCartId(),ctToken);
		JsonObject cartJson = MolekuleUtility.parseJsonObject(cartResponse);
		Map<String, String> customerMap = new HashMap<>();
		paymentOrderdto.setAmount(
				cartJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(ORDER_TOTAL).getAsString());
		if (isCreateUser(cartJson, ctToken)) {
			logger.debug(" UK Guest User Create OrderFlow -"+new Timestamp(System.currentTimeMillis()));
			customerMap = createUserAccount(cartJson, ctToken,country, orderRequestBean);
			customerId = customerMap.get(CUSTOMER_ID); //  createUserAccount(cartJson, ctToken,country, orderRequestBean);
			paymentOrderdto.setCustomerId(customerId);
			String storePaymentIdResponse  = createAdyenStoreIdForGuestUser(paymentOrderdto,country);
			JsonObject adyenJson = MolekuleUtility.parseJsonObject(storePaymentIdResponse);
			if(adyenJson.get("status")!=null || (adyenJson.has("resultCode") &&!"Authorised".equals(adyenJson.get("resultCode").getAsString())) ) {
				return storePaymentIdResponse;
			}
			String storePaymentId = adyenJson.get("recurringDetailReference").getAsString();
			paymentOrderdto.setShopperReference(customerId);
			paymentOrderdto.setSetupPaymentIntentId(storePaymentId);
			cartResponse = updateCustomerIdToCart(customerId, cartJson.get("id").getAsString(),
					cartJson.get("version").getAsLong(), ctToken);
			cartJson = MolekuleUtility.parseJsonObject(cartResponse);
		} else {
			logger.debug(" UK SignedIn User Create OrderFlow -"+new Timestamp(System.currentTimeMillis()));
			customerId = cartJson.get(CUSTOMER_ID).getAsString();
			paymentOrderdto.setShopperReference(customerId);
			String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ SLASH_CUSTOMERS_SLASH + customerId;
			String customerObjectResponse = netConnectionHelper.sendGetWithoutBody(ctToken, customerBaseUrl);
			JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerObjectResponse);
			if (!customerJsonObject.has(STATUS_CODE)) {
				customerMap.put(CUSTOMER_NUMBER, customerJsonObject.get(CUSTOMER_NUMBER).getAsString());
			}
		}
		paymentOrderdto.setCustomerId(customerId);
		paymentOrderdto.setAmount(
				cartJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(ORDER_TOTAL).getAsString());
		// if paymentToken present for customer skip adding paymentCardToCustomer
		String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + D2C_PAYMENT
				+ customerId;
		PaymentCustomObject customPaymentResponse = netConnectionHelper.getPaymentCustomObject(ctToken, customPaymentBaseUrl);
		if(customPaymentResponse != null && customPaymentResponse.getValue() != null && customPaymentResponse.getValue().getPayments() != null) {
			PaymentCustomObject.Value value = customPaymentResponse.getValue();
			List<Payments> payments = value.getPayments();
			Optional<Payments> paymentsOptional = payments.stream().filter(payment -> paymentOrderdto.getSetupPaymentIntentId().equals(payment.getPaymentToken())).findAny();
			if(!paymentsOptional.isPresent()) {
				updatePaymentCardToCustomer(customerId, cartJson, paymentOrderdto, ctToken);	
			}
		} else {
			updatePaymentCardToCustomer(customerId, cartJson, paymentOrderdto, ctToken);			
		}
		String customerNumber = customerMap.get(CUSTOMER_NUMBER);
		String paymentAuthToken = adyenPaymentService.authorizePayment(paymentOrderdto,currency);
		String orderResponse = placeOrder(cartJson,paymentAuthToken,ctToken,paymentOrderdto,country, customerNumber);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		if (!jsonObject.has(STATUS_CODE)) {
			orderHelperService.updateReadFlag(jsonObject.get("id").getAsString(), false, ctToken);
			String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
					+ SLASH_CUSTOMERS_SLASH + customerId;
			String customerResponse = netConnectionHelper.sendGetWithoutBody(ctToken, customerByIdUrl);
			JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(null != customerObject && jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has("id")) {
				setRecentlyUsedAddress(jsonObject, customerObject, customerByIdUrl, ctToken);
				if(jsonObject.has(PAYMENT_INFO)) {
					setRecentlyUsedPayment(jsonObject, customerObject, customerByIdUrl, ctToken, customPaymentBaseUrl);
				}
			}
			// warrent order operation
			isWarrentyOrderOperation(orderResponse,ctToken);
		}
		logger.debug("UK Create Order Method Ends -"+new Timestamp(System.currentTimeMillis()));
		return orderResponse;
	}


	private void setRecentlyUsedPayment(JsonObject jsonObject, JsonObject customerObject, String customerByIdUrl,
			String ctToken, String customPaymentBaseUrl) {
		JsonObject objJsonobject = jsonObject.get(PAYMENT_INFO).getAsJsonObject().get("payments").getAsJsonArray().get(0).getAsJsonObject().get("obj").getAsJsonObject();
		JsonObject fieldsObject = objJsonobject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		String customObjectPaymentId = fieldsObject.get("paymentId").getAsString();
		String customObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		PaymentCustomObject customPaymentResponse = netConnectionHelper.getPaymentCustomObject(ctToken, customPaymentBaseUrl);
		customPaymentResponse.getValue().setRecentlyUsedPayment(customObjectPaymentId);
		netConnectionHelper.sendPostRequest(customObjectUrl, ctToken, customPaymentResponse);
		
	}

	private void setRecentlyUsedAddress(JsonObject jsonObject, JsonObject customerObject, String customerByIdUrl, String ctToken) {
		String shippingAddressId = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
		List<CustomerAction.Actions> list = new ArrayList<>();
		CustomerAction customerAction = new CustomerAction();
		customerAction.setVersion(customerObject.has(VERSION) ? customerObject.get(VERSION).getAsLong() : 0);
		list.add(setCustomAttributes("recentlyUsedAddress", shippingAddressId));
		customerAction.setActions(list);
		netConnectionHelper.sendPostRequest(customerByIdUrl, ctToken, customerAction);		
		
	}

	private String createAdyenStoreIdForGuestUser(PaymentOrderDTO paymentOrderdto, String country) {
		PaymentRequest paymentRequest = preparePaymentRequest(paymentOrderdto);
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		String storePaymentIdResponse = adyenPaymentService.createPaymentToken(paymentRequest, currency);
		return storePaymentIdResponse;
	}
	private PaymentRequest preparePaymentRequest(PaymentOrderDTO paymentOrderdto) {
		PaymentRequest paymentRequest = new PaymentRequest();
		paymentRequest.setAmount(paymentOrderdto.getAmount());
		paymentRequest.setCustomerId(paymentOrderdto.getCustomerId());
		paymentRequest.setEncryptedCardNumber(paymentOrderdto.getEncryptedCardNumber());
		paymentRequest.setEncryptedExpiryMonth(paymentOrderdto.getEncryptedExpiryMonth());
		paymentRequest.setEncryptedExpiryYear(paymentOrderdto.getEncryptedExpiryYear());
		paymentRequest.setEncryptedSecurityCode(paymentOrderdto.getEncryptedSecurityCode());
		return paymentRequest;
	}

	private String placeOrder(JsonObject cartJson, String paymentAuthToken, String ctToken,
			PaymentOrderDTO paymentOrderdto, String country, String customerNumber) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		String orderResponse = null;
		String customerId = null;
		String ctPaymentId = null;
		String cartResponse = null;
		String transactionId = null;
		PaymentCustomObject customPaymentResponse = null;
		String customerResponse = null;
		try {	
			customerId = cartJson.get(CUSTOMER_ID).getAsString();
			Payment payment = prepareCTPaymentRequest(cartJson, paymentAuthToken,currency);
			String paymentCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments";
			String paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentCtUrl, ctToken, payment);
			JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
			ctPaymentId= paymentJson.get("id").getAsString();
			
			String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + D2C_PAYMENT
					+ customerId;
			customPaymentResponse = netConnectionHelper.getPaymentCustomObject(ctToken, customPaymentBaseUrl);
			String customPaymentId = "";
			if(customPaymentResponse != null && customPaymentResponse.getValue() != null && customPaymentResponse.getValue().getPayments() != null) {
				PaymentCustomObject.Value value = customPaymentResponse.getValue();
				List<Payments> payments = value.getPayments();
				Optional<Payments> paymentsOptional = payments.stream().filter(customPayment -> paymentOrderdto.getSetupPaymentIntentId().equals(customPayment.getPaymentToken())).findAny();
				if(paymentsOptional.isPresent()) {
					customPaymentId = paymentsOptional.get().getPaymentId();
				}
			}
			// add payment custom fields to paymentObject
			addAdyenCardDataToPayment(paymentOrderdto, paymentCtUrl, ctToken, ctPaymentId, cartJson, currency, customPaymentId);
			JsonArray transactionsArray = paymentJson.has("transactions") ? paymentJson.get("transactions").getAsJsonArray()
					: new JsonArray();
			if (transactionsArray.size() > 0) {
				transactionId = transactionsArray.get(0).getAsJsonObject().get("id").getAsString();
			}
			Long currentPaymentVersion = 0l;
			currentPaymentVersion = paymentJson.has(VERSION) ? paymentJson.get(VERSION).getAsLong() : currentPaymentVersion;
			UpdateActionsOnCart addPaymentToCart = new UpdateActionsOnCart();
			addPaymentToCart.setVersion(cartJson.get(VERSION).getAsLong());
			List<UpdateActionsOnCart.Action> actions = new ArrayList<>();
			if (StringUtils.hasText(ctPaymentId)) {
				UpdateActionsOnCart.Action addPaymentAction = CheckoutUtility.getAddPaymentRequestObj(ctPaymentId);
				if (actions != null) {
					actions.add(addPaymentAction);
				}
			}
			String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ SLASH_CUSTOMERS_SLASH + customerId;
			customerResponse = netConnectionHelper.sendGetWithoutBody(ctToken, customerByIdUrl);
			com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart.Action setBillingAddressAction = CheckoutUtility.getBillingAddressAction("CREDITCARD",
					customPaymentResponse, paymentOrderdto.getSetupPaymentIntentId(), customerResponse);
			if (setBillingAddressAction != null) {
				actions.add(setBillingAddressAction);
			} else if (paymentOrderdto.getBillingAddress() != null) {
				AddressRequestBean requestBillingAddress = paymentOrderdto.getBillingAddress();
				UpdateActionsOnCart.Action.Address address = new UpdateActionsOnCart.Action.Address();
				address.setFirstName(requestBillingAddress.getFirstName());
				address.setLastName(requestBillingAddress.getLastName());
				address.setStreetName(requestBillingAddress.getStreetName());
				address.setCity(requestBillingAddress.getCity());
				address.setState(requestBillingAddress.getState());
				address.setPostalCode(requestBillingAddress.getPostalCode());
				address.setPhone(requestBillingAddress.getPhone());
				address.setCountry(requestBillingAddress.getCountry());

				UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
				action.setActionName("setBillingAddress");
				action.setAddress(address);
				actions.add(action);
			}
			addPaymentToCart.setVersion(cartJson.get(VERSION).getAsLong());
			addPaymentToCart.setActions(actions);
			String updateCartCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
					+ paymentOrderdto.getCartId();
			cartResponse = (String) netConnectionHelper.sendPostRequest(updateCartCtUrl, ctToken, addPaymentToCart);
			JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if (cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartJson.get("id").getAsString()
					+ "?expand=lineItems[*].variant.attributes[*].value.typeId";
			JsonObject fieldsObject = cartJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			JsonArray lineItemsArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
			UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
			updateCartCustomServiceBean.setVersion(cartJsonObject.get(VERSION).getAsLong());
			List<UpdateCartCustomServiceBean.Action> actionList = new ArrayList<>();
			if (!fieldsObject.has("totalTax")) {
				//			set total tax
				Long taxAmount = 0L;
				for (int i = 0; i < lineItemsArray.size(); i++) {
					JsonObject lineItemObject = lineItemsArray.get(i).getAsJsonObject();
					JsonObject taxedPriceObject = lineItemObject.get("taxedPrice").getAsJsonObject();
					JsonObject totalNetObject = taxedPriceObject.get("totalNet").getAsJsonObject();
					JsonObject totalGrossObject = taxedPriceObject.get("totalGross").getAsJsonObject();
					Long totalNetPrice = totalNetObject.get(CENT_AMOUNT).getAsLong();
					Long totalGrossPrice = totalGrossObject.get(CENT_AMOUNT).getAsLong();
					taxAmount = taxAmount + (totalGrossPrice - totalNetPrice);
				}
				UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
				action.setActionName(SET_CUSTOM_FIELD);
				action.setName("totalTax");
				action.setValue(taxAmount.toString());
				actionList.add(action);
			}
			updateCustomerNumberAction(customerNumber, actionList);
			updateCartCustomServiceBean.setActions(actionList);			
			cartResponse =  (String)netConnectionHelper.sendPostRequest(cartUrl, ctToken, updateCartCustomServiceBean);
			cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if (cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			
			OrderDTO orderDTO = createOrderRequestBean(cartJsonObject, paymentOrderdto,country);
			String orderBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ "/orders?expand=state.id&expand=paymentInfo.payments[*].typeId";
			/** Create order */
			orderResponse = (String) netConnectionHelper.sendPostRequest(orderBaseUrl, ctToken, orderDTO);
			JsonObject orderJson = MolekuleUtility.parseJsonObject(orderResponse);

			if (orderJson.get("statusCode") != null) {
				adyenPaymentService.cancelPayment(paymentOrderdto, paymentAuthToken);
			} else {
				adyenPaymentService.capturePayment(paymentOrderdto, paymentAuthToken,currency);
				processCustomerStatus(paymentOrderdto, orderResponse, ctToken, customerByIdUrl, orderJson.get("customerId").getAsString(),country);
				ExecutorService executorService = Executors.newFixedThreadPool(15);
				UpdatePaymentThread updatePaymentThread = new UpdatePaymentThread();
				updatePaymentThread.setPaymentOrderdto(paymentOrderdto);
				updatePaymentThread.setCurrentPaymentVersion(currentPaymentVersion);
				updatePaymentThread.setTransactionId(transactionId);
				updatePaymentThread.setPaymentId(ctPaymentId);
				updatePaymentThread.setNetConnectionHelper(netConnectionHelper);
				updatePaymentThread.setToken(ctToken);
				updatePaymentThread.setCtEnvProperties(ctEnvProperties);
				executorService.execute(updatePaymentThread);
				executorService.shutdown();
			}
		}catch(Exception ex) {
			logger.error("Error In Creating Order - ",ex);
			adyenPaymentService.cancelPayment(paymentOrderdto, paymentAuthToken);
		}
		return orderResponse;
	}

	public void updateCustomerNumberAction(String customerNumber, List<UpdateCartCustomServiceBean.Action> actionList) {
		UpdateCartCustomServiceBean.Action actionTwo = new UpdateCartCustomServiceBean.Action();
		actionTwo.setActionName(SET_CUSTOM_FIELD);
		actionTwo.setName(CUSTOMER_NUMBER);
		actionTwo.setValue(customerNumber);
		actionList.add(actionTwo);
	}

	private List<com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart.Action> updateBillingAddressRequest(
			PaymentCustomObject customPaymentResponse, String setupPaymentIntentId, String customerResponse,
			List<com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart.Action> actions,
			PaymentOrderDTO paymentOrderdto) {
	
			AddressRequestBean requestBillingAddress = paymentOrderdto.getBillingAddress();
			UpdateActionsOnCart.Action.Address address = new UpdateActionsOnCart.Action.Address();
			address.setFirstName(requestBillingAddress.getFirstName());
			address.setLastName(requestBillingAddress.getLastName());
			address.setStreetName(requestBillingAddress.getStreetName());
			address.setCity(requestBillingAddress.getCity());
			address.setState(requestBillingAddress.getState());
			address.setPostalCode(requestBillingAddress.getPostalCode());
			address.setPhone(requestBillingAddress.getPhone());
			address.setCountry(requestBillingAddress.getCountry());

			UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
			action.setActionName("setBillingAddress");
			action.setAddress(address);
			actions.add(action);
		return actions;
	}

	/**
	 * captureOrCancelPayment method is to create order and payment
	 * @param country 
	 * @param orderRequestBean 
	 * 
	 * 
	 * @param paymentOrderdto,isOrderCreationRequired
	 * @return String
	 * @throws PaymentTypeNotFound
	 */
	public String captureOrCancelPayment(PaymentOrderDTO paymentOrderdto, boolean isOrderCreationRequired,
			boolean setupIntentOnly, String country, OrderRequestBean orderRequestBean) throws PaymentTypeNotFound {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		String ctToken = ctServerHelperService.getStringAccessToken();
		if (!"US".equals(country) && !"CA".equals(country)) {
			logger.debug("UK Create Order Method Start -"+new Timestamp(System.currentTimeMillis()));
			return createOrder(paymentOrderdto, ctToken, country, orderRequestBean);
		}

		if (PaymentRequestBean.Payment.AFFIRM.name().equals(paymentOrderdto.getPaymentType())) {
			return captureOrCancelAffirmPayment(paymentOrderdto, isOrderCreationRequired,currency,country);
		} 
		logger.debug("Create Order flow Debug");
		logger.info("Create Order flow Info");
		logger.trace("Create Order flow Trace");
		String paymentIntentId = null;
		String paymentId = null;
		String transactionId = null;
		Map<String, Object> cartData = null;
		String orderResponse = null;
		AccountCustomObject accountCustomObject = null;
		boolean isOrderOnHold = false;
		boolean isFreightAvalible = true;
		boolean isACHPayment = PaymentRequestBean.Payment.ACH.name().equals(paymentOrderdto.getPaymentType());
		String orderBaseUrl = null;
		String paymentType = null;
		String paymentTypeId = null;
		String customerId = null;
		String channel = null;
		String customerResponse = null;
		JsonObject customerObject = null;
		PaymentCustomObject customPaymentResponse = null;
		String cartResponse = null;
		JsonObject jsonObject = null;
		PaymentIntentRequest paymentRequestBean = CartServiceUtility.createPaymentRequest(paymentOrderdto);
		paymentRequestBean.setCurrency(currency);
		long currentPaymentVersion = 0;

		String customerByIdUrl = null;
		String customerNumber = null;
		if (isOrderCreationRequired) {
			cartResponse = cartHelperService.getCartById(paymentOrderdto.getCartId());
			jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + jsonObject.get("id").getAsString();
			//Check Guest user
			JsonObject cartFieldObject = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			boolean gift = false;
			if(cartFieldObject.has("gift")) {
				gift = cartFieldObject.get("gift").getAsBoolean();
			}
			channel = cartFieldObject.get(CHANNEL).getAsString();
			boolean hasAnonymousId = jsonObject.has(ANONYMOUSID);
			String paymentCustomerId = null;
			boolean createAccount = checkCartProducts(jsonObject, ctToken);
			if (hasAnonymousId && !gift && createAccount) {

				String email = jsonObject.get("customerEmail").getAsString();
				String firstName = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("firstName").getAsString().toUpperCase();
				String lastName = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("lastName").getAsString().toUpperCase();
				String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/customers?where=";
				String emailStr = "email=\"" + email + "\"";
				customerBaseUrl = customerBaseUrl + MolekuleUtility.convertToEncodeUri(emailStr);
				String response = null;
				try {
					response = netConnectionHelper.httpConnectionHelper(customerBaseUrl, ctToken);
				} catch (IOException e) {
					logger.error("IOException Occured", e);
				}
				JsonObject custoerJsonObject = MolekuleUtility.parseJsonObject(response);
				JsonArray resultsJsonArray = custoerJsonObject.get(RESULTS).getAsJsonArray();
				if (resultsJsonArray.size() <= 0) {
					String registrationUrl = ctEnvProperties.getUserAccountUrl() + "/api/v1/user/signup?country="+country;		
					String storeId;
					RegistrationBean registrationBean = new RegistrationBean();
					registrationBean.setEmail(email);
					registrationBean.setFirstName(firstName);
					registrationBean.setLastName(lastName);
					registrationBean.setMobileNumber("");
					registrationBean.setChannel(CustomerChannelEnum.D2C);
					registrationBean.setRegisteredChannel("MO");
					registrationBean.setCustomerSource(CustomerSourceEnum.D2C_WEB);
					registrationBean.setTermsOfService(orderRequestBean.isTermsOfService());
					registrationBean.setMarketingOffers(orderRequestBean.isMarketingOffers());
					if("GB".equals(country)) {
						storeId = ctEnvProperties.getUkStoreId();
					} else if("EU".equals(country)) {
						storeId = ctEnvProperties.getEuStoreId();
					} else {
						storeId = ctEnvProperties.getUsStoreId();
					}
					String storeUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/stores/" + storeId;
					String storeResponse = netConnectionHelper.sendGetWithoutBody(ctToken, storeUrl);
					JsonObject storeJsonObject = MolekuleUtility.parseJsonObject(storeResponse);
					registrationBean.setStore(storeJsonObject.get("key").getAsString());
					customerResponse = (String) netConnectionHelper.sendPostRequest(registrationUrl, null, registrationBean);
					customerObject = MolekuleUtility.parseJsonObject(customerResponse);
					customerId = customerObject.get("customerId").getAsString();		

					// create password call
					ForgotPasswordRequestBean forgotPasswordRequestBean = new ForgotPasswordRequestBean();
					forgotPasswordRequestBean.setEmail(email);
					registrationHelperService.forgotPassword(forgotPasswordRequestBean, "createPassword");
				} else {
					for (int i = 0; i < resultsJsonArray.size(); i++) {
						JsonObject resultJsonObject = resultsJsonArray.get(i).getAsJsonObject();
						customerId = resultJsonObject.get("id").getAsString();
					}
				}
				cartResponse = updateCustomerIdToCart(customerId,jsonObject.get("id").getAsString(),jsonObject.get(VERSION).getAsLong(),ctToken);
				if("US".equals(country)) {
					Object taxResponse = cartHelperService.calculateTax(cartResponse);
					if (taxResponse instanceof String) {
						return (String) taxResponse;
					}
					SetTaxAmount setTaxAmount = (SetTaxAmount) taxResponse;
					cartResponse = (String) netConnectionHelper.sendPostRequest(cartUrl, ctToken, setTaxAmount);
					jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
				}
				paymentCustomerId = customerId;

			} else if(hasAnonymousId) {
				paymentCustomerId = jsonObject.get(ANONYMOUSID).getAsString();
				customerId = paymentCustomerId;
			} else {
				customerId = jsonObject.get(CUSTOMER_ID).getAsString();
			}
			//Create Customer Payment Custom Object
			customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ SLASH_CUSTOMERS_SLASH + customerId;
			if(null != paymentCustomerId) {
				CustomerRequestBean customerRequestBean = new CustomerRequestBean();
				customerRequestBean.setAction(CustomerServiceUtility.CustomerActions.ADD_PAYMENT.name());
				customerRequestBean.setPaymentType("CREDITCARD");
				customerRequestBean.setPaymentToken(paymentOrderdto.getSetupPaymentIntentId());
				customerRequestBean.setBillingAddress(paymentOrderdto.getBillingAddress());
				customerProfileHelperService.updateCustomerById(paymentCustomerId, customerRequestBean,country,ctToken,null);
				customerResponse = netConnectionHelper.sendGetWithoutBody(ctToken, customerByIdUrl);
				customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			}
			if(jsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			if(D2C.equals(channel)) {
				String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + D2C_PAYMENT
						+ customerId;
				customPaymentResponse = netConnectionHelper.getPaymentCustomObject(ctToken, customPaymentBaseUrl);
			}
			if(null == customerResponse) {
				customerResponse = netConnectionHelper.sendGetWithoutBody(ctToken, customerByIdUrl);
				customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			}
			customerNumber = customerObject.get(CUSTOMER_NUMBER).getAsString();
			ctToken = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
			// Get cart version,total amount,currency... from cart by Id
			cartData = CheckoutUtility.getCartDat(jsonObject);
			if (cartData == null) {
				return "Invalid Cart Id";
			}
			paymentType = paymentOrderdto.getPaymentType();
			if(B2B.equals(channel)) {
				accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,ctToken);				
			}
			paymentRequestBean.setAmount((String) cartData.get(TOTAL_AMOUNT));
			paymentRequestBean.setCurrency((String) cartData.get(CURRENCY_CODE));
			cartData.put("PaymentType", paymentOrderdto.getPaymentType());
		}

		if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentOrderdto.getPaymentType())) {
			isOrderOnHold = checkIsPaymentTermsApproved(accountCustomObject, cartData);
		} else if (paymentOrderdto.getPaymentType() == null && paymentOrderdto.getSetupPaymentIntentId() != null) {
			paymentType = PAYMENT_TYPE_CREDIT_CARD;
			// Create the payment intent
			JsonObject fieldObject = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(!(paymentRequestBean.getAmount().equals("0") && fieldObject.has("orderTag"))) {
				String paymentIntentResponse = stripePaymentService.createPaymentToken(paymentRequestBean,
						PaymentIntentOptions.PRE_AUTH.name(), null);
				jsonObject = MolekuleUtility.parseJsonObject(paymentIntentResponse);
				if(jsonObject.get("error") != null) {
					return paymentIntentResponse;
				}
				paymentIntentId = jsonObject.has("id") ? jsonObject.get("id").getAsString() : null;
			}
			if(paymentOrderdto.getCartId() != null ) {
				if(B2B.equals(channel)) {
					paymentTypeId = getPaymentIdBySetupIntentId(accountCustomObject, paymentOrderdto);					
				} else {
					paymentTypeId = getPaymentIdBySetupIntentId(customPaymentResponse, paymentOrderdto);
				}
			}
		} else if (!isACHPayment) {
			throw new PaymentTypeNotFound(ErrorCode.PAYMENT_TYPE.getDescription(), ErrorCode.PAYMENT_TYPE.getCode());
		}
		if (isOrderCreationRequired) {
			// Create Payment
			cartData.put(PAYMENT_INTENT_ID, paymentIntentId);
			Payment payment = CheckoutUtility.createPaymentRequest(cartData);
			payment.setInterfaceId(paymentIntentId);
			String paymentCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments";
			String paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentCtUrl, ctToken, payment);
			// Update PaymentType Id If is is CreditCard Type
			JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
			if (paymentTypeId != null && !paymentJson.has(STATUS_CODE)) {
				currentPaymentVersion = paymentJson.get(VERSION).getAsLong();
				paymentId = paymentJson.get("id").getAsString();
				PaymentIdUpdatePayment updatePayment = createUpdatePaymentCtRequest(currentPaymentVersion,
						paymentTypeId);
				String updatePaymentUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/payments/" + paymentId;
				paymentResponse = (String) netConnectionHelper.sendPostRequest(updatePaymentUrl, ctToken, updatePayment);
				paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
				paymentId = paymentJson.get("id").getAsString();
			}
			// set credit card data if paymentIntentId != null
			if (paymentIntentId != null && jsonObject != null && paymentId != null) {
				paymentResponse = addCardDataToPayment(jsonObject, paymentCtUrl, ctToken, paymentId);
				paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
				if (paymentJson.get(STATUS_CODE) != null) {
					return paymentResponse;
				}
			}
			JsonArray transactionsArray =  paymentJson.has("transactions") ? paymentJson.get("transactions").getAsJsonArray() : new JsonArray();
			if (transactionsArray.size() > 0) {
				transactionId = transactionsArray.get(0).getAsJsonObject().get("id").getAsString();
			}
			currentPaymentVersion = paymentJson.has(VERSION) ? paymentJson.get(VERSION).getAsLong() : currentPaymentVersion;
			// Update payment to cart
			UpdateActionsOnCart addPaymentToCart = new UpdateActionsOnCart();
			addPaymentToCart.setVersion((long) cartData.get(VERSION));
			List<UpdateActionsOnCart.Action> actions = new ArrayList<>();
			// Add payment to cart
			if(StringUtils.hasText(paymentId)) {
				UpdateActionsOnCart.Action addPaymentAction = CheckoutUtility.getAddPaymentRequestObj(paymentId);
				if (actions != null) {
					actions.add(addPaymentAction);
				}
			}
			// Set Billing Address into Cart
			UpdateActionsOnCart.Action setBillingAddressAction;
			if(B2B.equals(channel)) {
				setBillingAddressAction = CheckoutUtility.getBillingAddressAction(paymentType,
						accountCustomObject, paymentOrderdto.getSetupPaymentIntentId());
			} else {
				setBillingAddressAction = CheckoutUtility.getBillingAddressAction(paymentType,
						customPaymentResponse, paymentOrderdto.getSetupPaymentIntentId(), customerResponse);
			}
			if (setBillingAddressAction != null) {
				actions.add(setBillingAddressAction);
			} else if(paymentOrderdto.getBillingAddress() != null) {
				AddressRequestBean requestBillingAddress = paymentOrderdto.getBillingAddress();
				UpdateActionsOnCart.Action.Address address = new UpdateActionsOnCart.Action.Address();
				address.setFirstName(requestBillingAddress.getFirstName());
				address.setLastName(requestBillingAddress.getLastName());
				address.setStreetName(requestBillingAddress.getStreetName());
				address.setCity(requestBillingAddress.getCity());
				address.setState(requestBillingAddress.getState());
				address.setPostalCode(requestBillingAddress.getPostalCode());
				address.setPhone(requestBillingAddress.getPhone());
				address.setCountry(requestBillingAddress.getCountry());

				UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
				action.setActionName("setBillingAddress");
				action.setAddress(address);
				actions.add(action);
			}

			// Set Freight Hold to cart
			String cartStringResponse = cartHelperService.getCartById(paymentOrderdto.getCartId());
			JsonObject cartStringJsonObject = MolekuleUtility.parseJsonObject(cartStringResponse);
			String shippingMethodName = cartStringJsonObject.get(SHIPPING_INFO).getAsJsonObject()
					.get(SHIPPING_METHOD_NAME).getAsString();
			if ("Contact Carrier".equals(shippingMethodName)) {
				String ownCarrier = cartStringJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(OWN_CARRIER).getAsString();
				Map<String,String> ownCarrierData = null;
				if(ownCarrier != null) {
					ownCarrierData = convertStringToMap(ownCarrier);
				}		
				String deliveryType = (ownCarrierData != null)? ownCarrierData.get("deliveryType"):"";
				if ("FREIGHT".equals(deliveryType) || "BOTH".equals(deliveryType)) {
					isFreightAvalible = CheckoutUtility.checkIsFreightAvailable(accountCustomObject,
							cartStringJsonObject);
				} else {
					isFreightAvalible = true;
				}
			} else if (!("FEDEX_FREIGHT_ECONOMY".equals(shippingMethodName)
					|| "FedEx Freight - Priority".equals(shippingMethodName))) {
				isFreightAvalible = true;
			} else if(B2B.equals(channel)){
				isFreightAvalible = CheckoutUtility.checkIsFreightAvailable(accountCustomObject, cartStringJsonObject);
			}
			// check is tax exempted
			//			TaxExempt hold check is removed for release 1.0, so the taxExemptFlag is set as true always. 
			if (!isFreightAvalible || isOrderOnHold) {
				String cartResponseById = cartHelperService.getCartById(paymentOrderdto.getCartId());
				UpdateActionsOnCart.Action holdAction = CheckoutUtility
						.updateHoldCustomAttributesToOrder(cartResponseById, isFreightAvalible, isOrderOnHold, true);
				if (holdAction != null) {
					actions.add(holdAction);
				}
			}
			addPaymentToCart.setVersion(cartStringJsonObject.get(VERSION).getAsLong());
			addPaymentToCart.setActions(actions);
			String updateCartCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
					+ paymentOrderdto.getCartId();
			cartResponse = (String) netConnectionHelper.sendPostRequest(updateCartCtUrl, ctToken, addPaymentToCart);
			JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if(cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			// Remove subscriptions if giftCard is enabled.
			String cartId = cartJsonObject.get("id").getAsString();
			String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId
					+ "?expand=lineItems[*].variant.attributes[*].value.typeId";
			cartResponse = removeSubscriptionProducts(cartResponse, cartJsonObject, cartUrl, ctToken);
			cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if (cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			JsonObject fieldsObject = cartJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			JsonArray lineItemsArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
			UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
			updateCartCustomServiceBean.setVersion(cartJsonObject.get(VERSION).getAsLong());
			List<UpdateCartCustomServiceBean.Action> actionList = new ArrayList<>();
			if (!fieldsObject.has("totalTax")) {
				//				set total tax
				Long taxAmount = 0L;
				for (int i = 0; i < lineItemsArray.size(); i++) {
					JsonObject lineItemObject = lineItemsArray.get(i).getAsJsonObject();
					JsonObject taxedPriceObject = lineItemObject.get("taxedPrice").getAsJsonObject();
					JsonObject totalNetObject = taxedPriceObject.get("totalNet").getAsJsonObject();
					JsonObject totalGrossObject = taxedPriceObject.get("totalGross").getAsJsonObject();
					Long totalNetPrice = totalNetObject.get(CENT_AMOUNT).getAsLong();
					Long totalGrossPrice = totalGrossObject.get(CENT_AMOUNT).getAsLong();
					taxAmount = taxAmount + (totalGrossPrice - totalNetPrice);
				}
				UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
				action.setActionName(SET_CUSTOM_FIELD);
				action.setName("totalTax");
				action.setValue(taxAmount.toString());
				actionList.add(action);
			}
			updateCustomerNumberAction(customerNumber, actionList);
			updateCartCustomServiceBean.setActions(actionList);			
			cartResponse =  (String)netConnectionHelper.sendPostRequest(cartUrl, ctToken, updateCartCustomServiceBean);
			cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if (cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}

			OrderDTO orderDTO = createOrderRequestBean(cartJsonObject, paymentOrderdto,country);
			orderBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ "/orders?expand=state.id&expand=paymentInfo.payments[*].typeId";
			/** Create order */

			orderResponse = (String) netConnectionHelper.sendPostRequest(orderBaseUrl, ctToken, orderDTO);
			jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		}
		if (orderResponse != null) {
			jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
			orderHelperService.updateReadFlag(jsonObject.get("id").getAsString(), false, ctToken);
			if(D2C.equals(channel) && null != customerObject && jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has("id")) {
				String shippingAddressId = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
				List<CustomerAction.Actions> list = new ArrayList<>();
				CustomerAction customerAction = new CustomerAction();
				customerAction.setVersion(customerObject.has(VERSION) ? customerObject.get(VERSION).getAsLong() : 0);
				list.add(setCustomAttributes("recentlyUsedAddress", shippingAddressId));
				customerAction.setActions(list);
				netConnectionHelper.sendPostRequest(customerByIdUrl, ctToken, customerAction);		
				if(jsonObject.has(PAYMENT_INFO)) {
					JsonObject objJsonobject = jsonObject.get(PAYMENT_INFO).getAsJsonObject().get("payments").getAsJsonArray().get(0).getAsJsonObject().get("obj").getAsJsonObject();
					JsonObject fieldsObject = objJsonobject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
					String customObjectPaymentId = fieldsObject.get("paymentId").getAsString();
					String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + D2C_PAYMENT
							+ customerId;
					String customObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
					customPaymentResponse = netConnectionHelper.getPaymentCustomObject(ctToken, customPaymentBaseUrl);
					customPaymentResponse.getValue().setRecentlyUsedPayment(customObjectPaymentId);
					netConnectionHelper.sendPostRequest(customObjectUrl, ctToken, customPaymentResponse);
				}
			}
		}
		if (isOrderCreationRequired && jsonObject.get(STATUS_CODE) != null) {
			if (paymentType != null && PAYMENT_TYPE_CREDIT_CARD.equals(paymentType)) {
				// If order is not created then cancel the payment intent
				stripePaymentService.createPaymentToken(null, PaymentIntentOptions.CANCEL.name(), paymentIntentId);
			}
			return orderResponse;
		}
		String capturedResponse = null;
		if (paymentOrderdto.getPaymentType() == null) {
			// Capture the Payment Intent
			capturedResponse = stripePaymentService.createPaymentToken(null, PaymentIntentOptions.CAPTURE.name(),
					paymentIntentId);
		}
		// warrent order operation
		isWarrentyOrderOperation(orderResponse,ctToken);
		if (!isOrderCreationRequired) {
			return capturedResponse;
		}
		if (isOrderOnHold || !isFreightAvalible || isACHPayment) {
			sendGridModelProcess(paymentOrderdto, orderResponse, accountCustomObject, isFreightAvalible, isACHPayment,
					customerId, channel,country);
		} else {
			processCustomerStatus(paymentOrderdto, orderResponse, ctToken, customerByIdUrl, customerId,country);

		}

		if (isACHPayment) {
			processACHPayment(orderResponse, accountCustomObject, customerId);
		}
		if (!PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentOrderdto.getPaymentType())) {
			ExecutorService executorService = Executors.newFixedThreadPool(15);
			UpdatePaymentThread updatePaymentThread = new UpdatePaymentThread();
			updatePaymentThread.setPaymentOrderdto(paymentOrderdto);
			updatePaymentThread.setCurrentPaymentVersion(currentPaymentVersion);
			updatePaymentThread.setTransactionId(transactionId);
			updatePaymentThread.setPaymentId(paymentId);
			updatePaymentThread.setNetConnectionHelper(netConnectionHelper);
			updatePaymentThread.setToken(ctToken);
			updatePaymentThread.setCtEnvProperties(ctEnvProperties);
			executorService.execute(updatePaymentThread);
			executorService.shutdown();
		}
		return orderResponse;
	}

	private void isWarrentyOrderOperation(String warrentyOrderResponse, String token) {
		JsonObject warrentyOrderJsonObject = MolekuleUtility.parseJsonObject(warrentyOrderResponse);
		String warrentyOrderId = warrentyOrderJsonObject.get("id").getAsString();
		JsonObject fieldJsonObject = warrentyOrderJsonObject.get(CUSTOM).getAsJsonObject().get("fields").getAsJsonObject();
		if(fieldJsonObject.has("orderTag")) {
			String originalOrderNumber = fieldJsonObject.get("previousOrderNumber").getAsString();
			String originalNumberOrderBaseUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey() +"/orders/order-number="+ originalOrderNumber;
			String orginalOrderResponse = netConnectionHelper.sendGetWithoutBody(token, originalNumberOrderBaseUrl);
			JsonObject originalOrderJsonObject = MolekuleUtility.parseJsonObject(orginalOrderResponse);
			String originalOrderId = originalOrderJsonObject.get("id").getAsString();
			String orderUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey() + "/orders/" + originalOrderId;
			String lineItemId = getWarrentyLineItemId(warrentyOrderJsonObject,originalOrderJsonObject);
			WarrentyOrderServiceBean warrentyOrderServiceBean = new WarrentyOrderServiceBean();
			warrentyOrderServiceBean.setVersion(originalOrderJsonObject.get(VERSION).getAsLong());
			List<WarrentyOrderServiceBean.Action> actionList = new ArrayList<>();
			WarrentyOrderServiceBean.Action action = new WarrentyOrderServiceBean.Action();
			boolean isCustomField = checkForCustomField(originalOrderJsonObject,lineItemId);
			if(isCustomField) {
				action.setActionName("setLineItemCustomField");
				action.setLineItemId(lineItemId);
				action.setName("warrantyOrderNumbers");
				action.setValue(Arrays.asList(warrentyOrderJsonObject.get("orderNumber").getAsString()));
			}else {
				action.setActionName("setLineItemCustomType");
				action.setLineItemId(lineItemId);
				WarrentyOrderServiceBean.Action.Type type = new WarrentyOrderServiceBean.Action.Type();
				type.setTypeId("type");
				type.setId(ctEnvProperties.getLineItemTypeId());
				action.setType(type);
				WarrentyOrderServiceBean.Action.Fields fields = new WarrentyOrderServiceBean.Action.Fields();
				fields.setWarrantyOrderNumbers(Arrays.asList(warrentyOrderJsonObject.get("orderNumber").getAsString()));
				action.setFields(fields);
			}
			actionList.add(action);
			warrentyOrderServiceBean.setActions(actionList);
			netConnectionHelper.sendPostRequest(orderUrl, token, warrentyOrderServiceBean);

			// update subscription object
			String customerId = originalOrderJsonObject.get("customerId").getAsString();
			List<JsonObject> subscriptionCustom = customerProfileHelperService.getSubscriptionContainerAndKey(customerId, originalOrderId);
			subscriptionCustom.stream().forEach(resultObject->{
				if(resultObject.has("value")) {
					JsonObject valueObject = resultObject.get("value").getAsJsonObject();
					if(valueObject.has("cart")) {
						JsonObject cartObject = valueObject.get("cart").getAsJsonObject();
						if(cartObject.has("id")) {
							String subscriptionCartId = cartObject.get("id").getAsString();
							if(subscriptionCartId != null) {
								String cartBaseUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey() + CART + subscriptionCartId;
								WarrentyCustomFieldServiceBean updateSubscriptionCartServiceBean = new WarrentyCustomFieldServiceBean();
								updateSubscriptionCartServiceBean.setVersion(cartHelperService.getCartCurrentVersionByCartId(subscriptionCartId));
								List<WarrentyCustomFieldServiceBean.Action> subscriptionActionList = new ArrayList<>();
								WarrentyCustomFieldServiceBean.Action subcrptionAction = new WarrentyCustomFieldServiceBean.Action();
								subcrptionAction.setActionName("setCustomField");
								subcrptionAction.setName("parentOrderReferenceId");
								subcrptionAction.setValue(String.valueOf(warrentyOrderId));
								subscriptionActionList.add(subcrptionAction);
								updateSubscriptionCartServiceBean.setActions(subscriptionActionList);
								netConnectionHelper.sendPostRequest(cartBaseUrl, token, updateSubscriptionCartServiceBean);
							}
						}
					}
				}
			});
		}
	}
	private boolean checkForCustomField(JsonObject originalOrderJsonObject, String lineItemId) {
		boolean iscustom = false;
		JsonArray originalOrderJsonArray =originalOrderJsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		for(int i=0;i<originalOrderJsonArray.size();i++) {
			if(originalOrderJsonArray.get(i).getAsJsonObject().get("id").getAsString().equals(lineItemId)) {
				iscustom = originalOrderJsonArray.get(i).getAsJsonObject().has(CUSTOM);
			}
		}
		return iscustom;

	}
	private String getWarrentyLineItemId(JsonObject warrentyOrderJsonObject, JsonObject originalOrderJsonObject) {
		String lineItemId = null;
		JsonArray warrentyOrderLineItemJsonArray =warrentyOrderJsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		JsonArray originalOrderJsonArray =originalOrderJsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		String warrentyProductId = null;
		for(int i=0;i<warrentyOrderLineItemJsonArray.size();i++) {
			JsonArray attributeJsonArray = warrentyOrderLineItemJsonArray.get(i).getAsJsonObject().get("variant").getAsJsonObject().get("attributes").getAsJsonArray();
			for(int j=0;j<attributeJsonArray.size();j++) {
				String name = attributeJsonArray.get(j).getAsJsonObject().get("name").getAsString();
				if("SubscriptionIdentifier".equals(name)) {
					warrentyProductId = warrentyOrderLineItemJsonArray.get(i).getAsJsonObject().get("productId").getAsString();
				}
			}
		}
		if(warrentyProductId == null) {
			for(int i=0;i<warrentyOrderLineItemJsonArray.size();i++) {
				for(int j=0;j<originalOrderJsonArray.size();j++) {
					if(originalOrderJsonArray.get(j).getAsJsonObject().get("productId").getAsString().equals(warrentyOrderLineItemJsonArray.get(i).getAsJsonObject().get("productId").getAsString())) {
						lineItemId = originalOrderJsonArray.get(j).getAsJsonObject().get("id").getAsString();
					}
				}
			}
		} else {
			for(int i=0;i<warrentyOrderLineItemJsonArray.size();i++) {
				for(int j=0;j<originalOrderJsonArray.size();j++) {
					if(originalOrderJsonArray.get(j).getAsJsonObject().get("productId").getAsString().equals(warrentyProductId)) {
						lineItemId = originalOrderJsonArray.get(j).getAsJsonObject().get("id").getAsString();
					}
				}
			}
		}
		return lineItemId;
	}

	private Boolean checkCartProducts(JsonObject cartJson, String token) {
		String queryLineItemPurifier = "";
		List<String> lineItemsPurifiers = new ArrayList<>();
		JsonArray lineItemsArray = cartJson.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		for(int i =0;i<lineItemsArray.size();i++) {
			JsonObject lineItemObject = lineItemsArray.get(i).getAsJsonObject();
			String key = lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("obj").getAsJsonObject().get("key").getAsString();
			if(key.equals("purifier")) {
				String productId = lineItemObject.get("productId").getAsString();
				lineItemsPurifiers.add(productId);
				queryLineItemPurifier = queryLineItemPurifier + "\"" + productId + "\",";
			}
		}
		if(lineItemsPurifiers.isEmpty()) {
			return false;
		}
		queryLineItemPurifier = queryLineItemPurifier.substring(0, queryLineItemPurifier.length() - 1);
		String querProductProjectionsUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey() 
		+ "/product-projections?where=categories(id=\"" + ctEnvProperties.getReplacementPartsCategoryId() +"\") and id IN (" + queryLineItemPurifier + ")";
		int lineItemsPurifiersSize = lineItemsPurifiers.size();
		String productProjectsResponse = netConnectionHelper.sendGetWithoutBody(token, querProductProjectionsUrl);
		JsonObject productProjectsObject = MolekuleUtility.parseJsonObject(productProjectsResponse);
		return lineItemsPurifiersSize != productProjectsObject.get(RESULTS).getAsJsonArray().size();
	}

	private String updateCustomerIdToCart(String customerID,String cartId,long version,String ctToken) {
		String updateUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey()+"/carts/"+cartId;
		UpdateCustomerToCart updateCustomerToCart = new UpdateCustomerToCart();
		updateCustomerToCart.setVersion(version);		List<UpdateCustomerToCart.Action> actions = new ArrayList<>();
		UpdateCustomerToCart.Action action = new UpdateCustomerToCart.Action();
		action.setActionName("setAnonymousId");
		action.setAnonymousId(null);
		actions.add(action);
		UpdateCustomerToCart.Action action1 = new UpdateCustomerToCart.Action();
		action1.setActionName("setCustomerId");
		action1.setCustomerId(customerID);
		actions.add(action1);
		updateCustomerToCart.setActions(actions);
		return (String)netConnectionHelper.sendPostRequest(updateUrl, ctToken, updateCustomerToCart);
	}

	private Actions setCustomAttributes(String name, Object value) {
		CustomerAction.Actions actions = new CustomerAction.Actions();
		actions.setAction(SET_CUSTOM_FIELD);
		actions.setName(name);
		actions.setValue(value);
		return actions;
	}

	public String captureOrCancelAffirmPayment(PaymentOrderDTO paymentOrderdto, boolean isOrderCreationRequired, String currency, String country) {

		String paymentIntentId = null;
		String paymentId = null;
		Map<String, Object> cartData = null;
		String orderResponse = null;
		AccountCustomObject accountCustomObject = null;
		boolean isOrderOnHold = false;
		boolean isFreightAvalible = true;
		boolean isACHPayment = PaymentRequestBean.Payment.ACH.name().equals(paymentOrderdto.getPaymentType());
		String orderBaseUrl = null;
		String paymentType = null;
		String customerId = null;
		JsonObject jsonObject = null;
		String customerResponse = null;
		JsonObject customerObject = null;
		PaymentIntentRequest paymentRequestBean = CartServiceUtility.createPaymentRequest(paymentOrderdto);
		paymentRequestBean.setCurrency(currency);
		String token = ctServerHelperService.getStringAccessToken();
		String customerByIdUrl = null;
		String cartResponse = cartHelperService.getCartById(paymentOrderdto.getCartId());
		jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		if(jsonObject.has(STATUS_CODE)) {
			return cartResponse;
		}
		jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + jsonObject.get("id").getAsString();
		//Check Guest user
		JsonObject cartFieldObject = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		boolean gift = false;
		if(cartFieldObject.has("gift")) {
			gift = cartFieldObject.get("gift").getAsBoolean();
		}
		String channel = cartFieldObject.get(CHANNEL).getAsString();
		boolean hasAnonymousId = jsonObject.has(ANONYMOUSID);
		String paymentCustomerId = null;
		boolean createAccount = checkCartProducts(jsonObject, token);
		if (hasAnonymousId && !gift && createAccount) {

			String email = jsonObject.get("customerEmail").getAsString();
			String firstName = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("firstName").getAsString();
			String lastName = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("lastName").getAsString();
			String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ "/customers?where=";
			String emailStr = "email=\"" + email + "\"";
			customerBaseUrl = customerBaseUrl + MolekuleUtility.convertToEncodeUri(emailStr);
			String response = null;
			try {
				response = netConnectionHelper.httpConnectionHelper(customerBaseUrl, token);
			} catch (IOException e) {
				logger.error("IOException Occured", e);
			}
			JsonObject custoerJsonObject = MolekuleUtility.parseJsonObject(response);
			JsonArray resultsJsonArray = custoerJsonObject.get(RESULTS).getAsJsonArray();
			if (resultsJsonArray.size() <= 0) {
				String registrationUrl = ctEnvProperties.getUserAccountUrl() + "/api/v1/user/signup/";
				RegistrationBean registrationBean = new RegistrationBean();
				registrationBean.setEmail(email);
				registrationBean.setFirstName(firstName);
				registrationBean.setLastName(lastName);
				registrationBean.setCompanyName("Photon");
				registrationBean.setMobileNumber("");
				registrationBean.setComments("Test");
				registrationBean.setChannel(CustomerChannelEnum.D2C);
				registrationBean.setRegisteredChannel("MO");
				registrationBean.setCustomerSource(CustomerSourceEnum.D2C_WEB);
				customerResponse = (String) netConnectionHelper.sendPostRequest(registrationUrl, null, registrationBean);
				customerObject = MolekuleUtility.parseJsonObject(customerResponse);
				customerId = customerObject.get("customerId").getAsString();	

				// create password call
				ForgotPasswordRequestBean forgotPasswordRequestBean = new ForgotPasswordRequestBean();
				forgotPasswordRequestBean.setEmail(email);
				registrationHelperService.forgotPassword(forgotPasswordRequestBean, "createPassword");
			} else {
				for (int i = 0; i < resultsJsonArray.size(); i++) {
					JsonObject resultJsonObject = resultsJsonArray.get(i).getAsJsonObject();
					customerId = resultJsonObject.get("id").getAsString();
				}
			}
			cartResponse = updateCustomerIdToCart(customerId,jsonObject.get("id").getAsString(),jsonObject.get(VERSION).getAsLong(),token);
			Object taxResponse = cartHelperService.calculateTax(cartResponse);
			if (taxResponse instanceof String) {
				return (String) taxResponse;
			}
			SetTaxAmount setTaxAmount = (SetTaxAmount) taxResponse;
			cartResponse = (String) netConnectionHelper.sendPostRequest(cartUrl, token, setTaxAmount);
			jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			paymentCustomerId = customerId;
		} else if(hasAnonymousId) {
			paymentCustomerId = jsonObject.get(ANONYMOUSID).getAsString();
			customerId = paymentCustomerId;
		} else {
			customerId = jsonObject.get(CUSTOMER_ID).getAsString();
		}
		if(!gift) {
			customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ SLASH_CUSTOMERS_SLASH + customerId;
			customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
			customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(customerObject.has(STATUS_CODE)) {
				return customerResponse;
			}
		}

		PaymentCustomObject customPaymentResponse = null;
		if(D2C.equals(channel)) {
			String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + D2C_PAYMENT
					+ customerId;
			customPaymentResponse = netConnectionHelper.getPaymentCustomObject(token, customPaymentBaseUrl);
		}
		if (isOrderCreationRequired) {
			token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
			// Get cart version,total amount,currency... from cart by Id
			cartData = CheckoutUtility.getCartDat(jsonObject);
			if (cartData == null) {
				return "Invalid Cart Id";
			}
			paymentType = paymentOrderdto.getPaymentType();
			if(B2B.equals(channel)) {
				accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,token);				
			}
			paymentRequestBean.setAmount((String) cartData.get(TOTAL_AMOUNT));
			paymentRequestBean.setCurrency((String) cartData.get(CURRENCY_CODE));
			cartData.put("PaymentType", paymentOrderdto.getPaymentType());
			cartResponse = null;
			// Create Payment
			cartData.put(PAYMENT_INTENT_ID, paymentIntentId);
			Payment payment = CheckoutUtility.createPaymentRequest(cartData);
			payment.setInterfaceId(paymentIntentId);
			String paymentCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments";
			String paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentCtUrl, token, payment);

			JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
			paymentId = paymentJson.get("id").getAsString();
			paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
			if (paymentJson.get(STATUS_CODE) != null) {
				return paymentResponse;
			}
			// Update payment to cart
			UpdateActionsOnCart addPaymentToCart = new UpdateActionsOnCart();
			addPaymentToCart.setVersion((long) cartData.get(VERSION));
			List<UpdateActionsOnCart.Action> actions = new ArrayList<>();
			// Add payment to cart
			UpdateActionsOnCart.Action addPaymentAction = CheckoutUtility.getAddPaymentRequestObj(paymentId);
			actions.add(addPaymentAction);
			// Set Billing Address into Cart
			UpdateActionsOnCart.Action setBillingAddressAction;
			if(B2B.equals(channel)) {
				setBillingAddressAction = CheckoutUtility.getBillingAddressAction(paymentType,
						accountCustomObject, paymentOrderdto.getSetupPaymentIntentId());
			} else {
				setBillingAddressAction = CheckoutUtility.getBillingAddressAction(paymentType,
						customPaymentResponse, paymentOrderdto.getSetupPaymentIntentId(), customerResponse);
			}
			if (setBillingAddressAction != null) {
				actions.add(setBillingAddressAction);
			}

			// Set Freight Hold to cart
			String cartStringResponse = cartHelperService.getCartById(paymentOrderdto.getCartId());
			JsonObject cartStringJsonObject = MolekuleUtility.parseJsonObject(cartStringResponse);
			String shippingMethodName = cartStringJsonObject.get(SHIPPING_INFO).getAsJsonObject()
					.get(SHIPPING_METHOD_NAME).getAsString();
			if ("Contact Carrier".equals(shippingMethodName)) {
				String ownCarrier = cartStringJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(OWN_CARRIER).getAsString();
				Map<String,String> ownCarrierData = null;
				if(ownCarrier != null) {
					ownCarrierData = convertStringToMap(ownCarrier);
				}		
				String deliveryType = (ownCarrierData != null)? ownCarrierData.get("deliveryType"):"";
				if ("FREIGHT".equals(deliveryType) || "BOTH".equals(deliveryType)) {
					isFreightAvalible = CheckoutUtility.checkIsFreightAvailable(accountCustomObject,
							cartStringJsonObject);
				} else {
					isFreightAvalible = true;
				}
			} else if (!("FEDEX_FREIGHT_ECONOMY".equals(shippingMethodName)
					|| "FedEx Freight - Priority".equals(shippingMethodName))) {
				isFreightAvalible = true;
			} else if(B2B.equals(channel)){
				isFreightAvalible = CheckoutUtility.checkIsFreightAvailable(accountCustomObject, cartStringJsonObject);
			}
			// check is tax exempted
			//			TaxExempt hold check is removed for release 1.0, so the taxExemptFlag is set as true always. 
			if (!isFreightAvalible || isOrderOnHold) {
				String cartResponseById = cartHelperService.getCartById(paymentOrderdto.getCartId());
				UpdateActionsOnCart.Action holdAction = CheckoutUtility
						.updateHoldCustomAttributesToOrder(cartResponseById, isFreightAvalible, isOrderOnHold, true);
				if (holdAction != null) {
					actions.add(holdAction);
				}
			}
			addPaymentToCart.setActions(actions);
			String updateCartCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
					+ paymentOrderdto.getCartId();
			cartResponse = (String) netConnectionHelper.sendPostRequest(updateCartCtUrl, token, addPaymentToCart);
			JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if(cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			// Remove subscriptions if giftCard is enabled.
			String cartId = cartJsonObject.get("id").getAsString();
			cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId
					+ "?expand=lineItems[*].variant.attributes[*].value.typeId";
			cartResponse = removeSubscriptionProducts(cartResponse, cartJsonObject, cartUrl, token);
			cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
			if (cartJsonObject.has(STATUS_CODE)) {
				return cartResponse;
			}
			OrderDTO orderDTO = createOrderRequestBean(cartJsonObject, paymentOrderdto,country);
			orderBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ "/orders?expand=state.id";
			/** Create order */

			orderResponse = (String) netConnectionHelper.sendPostRequest(orderBaseUrl, token, orderDTO);
		}
		// warrent order operation
		isWarrentyOrderOperation(orderResponse,token);
		if (orderResponse != null) {
			jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
			String orderId = jsonObject.get("id").getAsString();
			orderHelperService.updateReadFlag(orderId, false, token);
			if(D2C.equals(channel) && null != customerObject && jsonObject.get(SHIPPING_ADDRESS) !=null && jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has("id")) {
				String shippingAddressId = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
				List<CustomerAction.Actions> list = new ArrayList<>();
				CustomerAction customerAction = new CustomerAction();
				customerAction.setVersion(customerObject.get(VERSION).getAsLong());
				list.add(setCustomAttributes("recentlyUsedAddress", shippingAddressId));
				customerAction.setActions(list);
				netConnectionHelper.sendPostRequest(customerByIdUrl, token, customerAction);
			}
			String affirmResponse =  affirmCharges(new AffirmCheckoutBean(paymentOrderdto.getSetupPaymentIntentId(), orderId));
			JsonObject affirmResponseJSON = MolekuleUtility.parseJsonObject(affirmResponse);

			if (affirmResponseJSON.get("status_code") != null || affirmResponseJSON.get("id") == null) {
				return affirmResponseJSON.toString();
			}
			UpdateActionsOnCart addBillingAddressToOrder = new UpdateActionsOnCart();
			addBillingAddressToOrder.setVersion(orderHelperService.getCurrentVersionByOrderId(orderId, token));
			List<UpdateActionsOnCart.Action> actions = new ArrayList<>();
			UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
			action.setActionName("setBillingAddress");
			action.setAddress(getAffirmBillingAddress(affirmResponseJSON));
			actions.add(action);
			addBillingAddressToOrder.setActions(actions);
			String updateCartCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders/"+ orderId;
			netConnectionHelper.sendPostRequest(updateCartCtUrl, token, addBillingAddressToOrder);
			affirmCaptureUrl(orderId, affirmResponseJSON.get("id").getAsString());
		}

		if (isOrderOnHold || !isFreightAvalible || isACHPayment) {
			sendGridModelProcess(paymentOrderdto, orderResponse, accountCustomObject, isFreightAvalible, isACHPayment,
					customerId, channel,country);
		} else {
			processCustomerStatus(paymentOrderdto, orderResponse, token, customerByIdUrl, customerId,country);

		}
		tealiumHelperService.setAffirmPaymentCapture(orderResponse);
		return orderResponse;
	}

	private String removeSubscriptionProducts(String cartResponse, JsonObject jsonObject, String cartUrl, String token) {
		UpdateCartServiceBean updateCartServiceBean = new UpdateCartServiceBean();
		List<UpdateCartServiceBean.Action> actionList = new ArrayList<>();
		JsonObject fieldsJsonObject = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		if (fieldsJsonObject.has(IS_A_GIFT) && fieldsJsonObject.get(IS_A_GIFT).getAsBoolean()) {
			JsonArray lineItemsArray = jsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
			for (int i = 0; i < lineItemsArray.size(); i++) {
				JsonObject lineItemObject = lineItemsArray.get(i).getAsJsonObject();
				if (lineItemObject.has(CUSTOM)) {
					JsonObject fieldsObject = lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS)
							.getAsJsonObject();
					if (fieldsObject.has(SUBSCRIPTION_ENABLED)
							&& fieldsObject.get(SUBSCRIPTION_ENABLED).getAsBoolean()) {
						actionList.add(setRemoveLineItemCustomField(lineItemObject));
					}
				}
			}
			if(!CollectionUtils.isEmpty(actionList)) {
				updateCartServiceBean.setVersion(jsonObject.get(VERSION).getAsLong());
				updateCartServiceBean.setActions(actionList);
				cartResponse = (String) netConnectionHelper.sendPostRequest(cartUrl, token, updateCartServiceBean);
				cartResponse = taxCalculation(cartResponse, token);
			}
		}
		return cartResponse;
	}

	private String taxCalculation(String cartResponse, String token) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		if(jsonObject.has(STATUS_CODE)) {
			return cartResponse;
		}
		String cartId = jsonObject.get("id").getAsString();
		String url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId;
		boolean isShippingAddressAvailable = jsonObject.has(SHIPPING_ADDRESS);
		if (isShippingAddressAvailable) {
			Object taxResponse = cartHelperService.calculateTax(cartResponse);
			if (taxResponse instanceof String) {
				return (String) taxResponse;
			}
			SetTaxAmount setTaxAmount = (SetTaxAmount) taxResponse;
			cartResponse = (String) netConnectionHelper.sendPostRequest(url, token, setTaxAmount);
		}
		return cartHelperService.updateCustomField(cartResponse, cartId, url, token);
	}

	private com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean.Action setRemoveLineItemCustomField(
			JsonObject lineItemObject) {
		UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
		action.setActionName(REMOVE_LINE_ITEM);
		action.setLineItemId(lineItemObject.get("id").getAsString());
		action.setQuantity(lineItemObject.get(QUANTITY).getAsInt());
		return action;
	}

	private OrderDTO createOrderRequestBean(JsonObject cartJsonObject, PaymentOrderDTO paymentOrderdto, String countryCode) {
		long currentCartVersion = cartJsonObject.get(VERSION).getAsLong();
		// create unique order number
		String storeLocation = cartJsonObject.get("store").getAsJsonObject().get("key").getAsString();
		String orderNumber = null;
		OrderDTO orderDTO = new OrderDTO();
		if("EU".equals(countryCode)) {
			String country =  cartJsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("country").getAsString();
			String alphaCode = countryAlphaCodes.get(country);
			orderNumber = getUKOrderNumber(alphaCode);
			orderDTO.setOrderNumber(orderNumber); 
		}else if("GB".equals(countryCode)) {
			String country =  cartJsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("country").getAsString();
			String alphaCode = countryAlphaCodes.get(country);
			orderNumber = getUKOrderNumber(alphaCode);
			orderDTO.setOrderNumber(orderNumber); 
		}else {
			orderNumber = orderHelperService.getOrderNumber(storeLocation);
			JsonObject orderNumberJsonObject = MolekuleUtility.parseJsonObject(orderNumber);
			if (orderNumberJsonObject.has("orderId")) {
				orderDTO.setOrderNumber(orderNumberJsonObject.get("orderId").getAsString());
			}
		}
		orderDTO.setId(paymentOrderdto.getCartId());
		orderDTO.setVersion(currentCartVersion);

		OrderDTO.State state = new OrderDTO.State();
		state.setId(ctEnvProperties.getOrderSubmittedId());
		state.setTypeId(STATE);
		orderDTO.setState(state);
		return orderDTO;
	}


	private String getUKOrderNumber(String alphaCode) {
		int randomNumber = getRandomNumber();
		return alphaCode+randomNumber;
	}

	private int getRandomNumber() {
		return (int) ((Math.random() * 9000000) + 1000000);
	}

	private PaymentIdUpdatePayment createUpdatePaymentCtRequest(long currentPaymentVersion, String paymentTypeId) {
		PaymentIdUpdatePayment updatePayment = new PaymentIdUpdatePayment();
		updatePayment.setVersion(currentPaymentVersion);
		List<PaymentIdUpdatePayment.Action> actions = new ArrayList<>();
		PaymentIdUpdatePayment.Action action = new PaymentIdUpdatePayment.Action();
		action.setActionName("setCustomType");
		PaymentIdUpdatePayment.Action.Type paymentIdUpdatePaymentActionType = new PaymentIdUpdatePayment.Action.Type();
		paymentIdUpdatePaymentActionType.setId(ctEnvProperties.getPaymentTypeId());
		paymentIdUpdatePaymentActionType.setTypeId("type");
		action.setType(paymentIdUpdatePaymentActionType);
		PaymentIdUpdatePayment.Action.Fields fields = new PaymentIdUpdatePayment.Action.Fields();
		fields.setPaymentId(paymentTypeId);
		action.setFields(fields);
		actions.add(action);
		updatePayment.setActions(actions);
		return updatePayment;
	}

	private String getPaymentIdBySetupIntentId(AccountCustomObject accountCustomObject,
			PaymentOrderDTO paymentOrderdto) {
		String paymentTypeId = null;
		List<Payments> payments = accountCustomObject.getValue().getPayments();
		Payments payment = null;
		if (payments != null) {

			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (paymentOrderdto.getSetupPaymentIntentId().equals(payments2.getPaymentToken())) {
					payment = payments2;
					break;
				}
			}
		}
		if (payment != null) {
			paymentTypeId = payment.getPaymentId();
		}
		return paymentTypeId;
	}

	private String getPaymentIdBySetupIntentId(PaymentCustomObject customPaymentResponse, PaymentOrderDTO paymentOrderdto) {
		String paymentTypeId = null;
		if(customPaymentResponse ==null || customPaymentResponse.getValue() ==null) {
			return paymentTypeId;
		}
		List<Payments> payments = customPaymentResponse.getValue().getPayments();
		Payments payment = null;
		if (payments != null) {

			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (paymentOrderdto.getSetupPaymentIntentId().equals(payments2.getPaymentToken())) {
					payment = payments2;
					break;
				}
			}
		}
		if (payment != null) {
			paymentTypeId = payment.getPaymentId();
		}
		return paymentTypeId;
	}

	private boolean checkIsPaymentTermsApproved(AccountCustomObject accountCustomObject, Map<String, Object> cartData) {
		boolean isOrderOnHold = false;
		List<Payments> payments = accountCustomObject.getValue().getPayments();
		Payments payment = null;
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(payments2.getPaymentType())) {
					payment = payments2;
					break;
				}
			}
		}

		long totalApprovedCredit = 0;
		if(payment == null) {
			return true;
		}
		if (payment.getTotalApprovedCredit() != null)
			totalApprovedCredit = Long.parseLong(payment.getTotalApprovedCredit());
		long totalUsedCredit = 0;
		if (payment.getTotalUsedCredit() != null)
			totalUsedCredit = Long.parseLong(payment.getTotalUsedCredit());
		long orderAmount = Long.parseLong((String) cartData.get(TOTAL_AMOUNT));
		if ((!TermsStatus.APPROVED.name().equals(payment.getTermsStatus().name())) && (orderAmount > (totalApprovedCredit - totalUsedCredit))) {
			isOrderOnHold = true;
		}
		return isOrderOnHold;
	}

	private void sendGridModelProcess(PaymentOrderDTO paymentOrderdto, String orderResponse,
			AccountCustomObject accountCustomObject, boolean isFreightAvalible, boolean isACHPayment,
			String customerId, String channel, String country) {
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		ExecutorService executorService = Executors.newFixedThreadPool(15);
		PendingMailThread pendingMailThread = new PendingMailThread();
		pendingMailThread.setPaymentOrderdto(paymentOrderdto);
		pendingMailThread.setOrderResponse(orderResponse);
		if(B2B.equals(channel)) {
			pendingMailThread.setAccountCustomObject(accountCustomObject);			
		}
		pendingMailThread.setFreightAvalible(isFreightAvalible);
		pendingMailThread.setACHPayment(isACHPayment);
		pendingMailThread.setCustomerId(customerId);
		pendingMailThread.setOrderPendingMailSender(orderPendingMailSender);
		pendingMailThread.setOrderPendingSalesRepMailSender(orderPendingSalesRepMailSender);
		pendingMailThread.setFreightInfoPendingMailSender(freightInfoPendingMailSender);
		pendingMailThread.setCtServerHelperService(ctServerHelperService);
		pendingMailThread.setCtEnvProperties(ctEnvProperties);
		pendingMailThread.setNetConnectionHelper(netConnectionHelper);
		pendingMailThread.setPrismicApiUrl(ctEnvProperties.getPrismicApiUrl());
		pendingMailThread.setPrismicApiB2BUrl(ctEnvProperties.getPrismicApiB2BUrl());
		pendingMailThread.setPrismicApiD2CUrl(ctEnvProperties.getPrismicApiD2CUrl());
		pendingMailThread.setAuthUrl(ctEnvProperties.getAuthUrl());
		pendingMailThread.setAccessTokenUrl(ctEnvProperties.getAccessTokenUrl());
		pendingMailThread.setGrantType(ctEnvProperties.getGranttype());
		pendingMailThread.setHttpclientEnableFlag(ctEnvProperties.isHttpclientEnableFlag());
		pendingMailThread.setBasicAuthValue(ctEnvProperties.getBasicAuthValue());
		pendingMailThread.setHttpClientResponseTimeout(ctEnvProperties.getHttpClientResponseTimeout());
		pendingMailThread.setToken(token);
		pendingMailThread.setCountry(country);
		executorService.execute(pendingMailThread);
		executorService.shutdown();
	}

	private void processCustomerStatus(PaymentOrderDTO paymentOrderdto, String orderResponse, String token,
			String customerByIdUrl, String  customerId, String country) {
		ExecutorService executorService = Executors.newFixedThreadPool(15);
		ConfirmationMailThread confirmationMailThread = new ConfirmationMailThread();
		confirmationMailThread.setPaymentOrderdto(paymentOrderdto);
		confirmationMailThread.setOrderResponse(orderResponse);
		confirmationMailThread.setToken(token);
		confirmationMailThread.setCustomerByIdUrl(customerByIdUrl);
		confirmationMailThread.setCustomerId(customerId);
		confirmationMailThread.setNetConnectionHelper(netConnectionHelper);
		confirmationMailThread.setOrderConfirmMailSender(orderConfirmMailSender);
		confirmationMailThread.setOrderConfirmD2CMailSender(orderConfirmD2CMailSender);
		confirmationMailThread.setCtServerHelperService(ctServerHelperService);
		confirmationMailThread.setCtEnvProperties(ctEnvProperties);
		confirmationMailThread.setCtHost(ctEnvProperties.getHost());
		confirmationMailThread.setCtProjectKey(ctEnvProperties.getProjectKey());
		confirmationMailThread.setPrismicApiUrl(ctEnvProperties.getPrismicApiUrl());
		confirmationMailThread.setPrismicApiB2BUrl(ctEnvProperties.getPrismicApiB2BUrl());
		confirmationMailThread.setPrismicApiD2CUrl(ctEnvProperties.getPrismicApiD2CUrl());
		confirmationMailThread.setCountryCode(country);
		confirmationMailThread.setAuthUrl(ctEnvProperties.getAuthUrl());
		confirmationMailThread.setAccessTokenUrl(ctEnvProperties.getAccessTokenUrl());
		confirmationMailThread.setGrantType(ctEnvProperties.getGranttype());
		confirmationMailThread.setHttpclientEnableFlag(ctEnvProperties.isHttpclientEnableFlag());
		confirmationMailThread.setBasicAuthValue(ctEnvProperties.getBasicAuthValue());
		confirmationMailThread.setHttpClientResponseTimeout(ctEnvProperties.getHttpClientResponseTimeout());
		executorService.execute(confirmationMailThread);
		executorService.shutdown();
	}

	private void processACHPayment(String orderResponse, AccountCustomObject accountCustomObject,
			String customerId) {
		ExecutorService executorService = Executors.newFixedThreadPool(15);
		executorService.execute(new Runnable() {
			public void run() {
				String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
				String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ MolekuleConstant.SLASH_CUSTOMERS_SLASH + customerId;
				String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
				JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
				SendGridModel sendGridModel = new SendGridModel();
				JsonObject orderObject = MolekuleUtility.parseJsonObject(orderResponse);
				sendGridModel.setOrderNumber(orderObject.get(ORDER_NUMBER).getAsString());
				sendGridModel.setFirstName(customerObject.get(FIRST_NAME).getAsString());
				sendGridModel.setEmail(customerObject.get("email").getAsString());
				sendGridModel
				.setSalesRepPhoneNumber(accountCustomObject.getValue().getSalesRepresentative().getPhone());
				sendGridModel.setSalesRepEmail(accountCustomObject.getValue().getSalesRepresentative().getEmail());
				sendGridModel.setSalesRepCalendyLink(
						accountCustomObject.getValue().getSalesRepresentative().getCalendlyLink());
				paymentPendingMailSender.sendMail(sendGridModel);
			}
		});
		executorService.shutdown();

	}

	private Map<String, String> convertStringToMap(String ownCarrierStr) {
		ownCarrierStr = ownCarrierStr.substring(1, ownCarrierStr.length() - 1);
		String[] keyValuePairs = ownCarrierStr.split(",");
		Map<String, String> map = new HashMap<>();
		for (String pair : keyValuePairs) {
			String[] entry = pair.split("=");
			map.put(entry[0].trim(), entry[1].trim());
		}
		return map;
	}
	
	public String addAdyenCardDataToPayment(PaymentOrderDTO paymentOrderdto, String paymentCtUrl, String token,
			String paymentId, JsonObject cartJson, String currency, String customPaymentId) {
		CreditCard creditCard = customerProfileHelperService.getPaymentCardDetails(
				paymentOrderdto.getSetupPaymentIntentId(), cartJson.get(CUSTOMER_ID).getAsString());
		String paymentUrl = paymentCtUrl + "/" + paymentId;
		String paymentResponse = null;
		String adyenPaymentAuthUrl = ctEnvProperties.getAdyenHost() + "/checkout/" + ctEnvProperties.getAdyenVersion()
				+ "/payments";
		AdyenPaymentAuthRequest adyenPaymentAuthRequest = adyenPaymentService.preparePaymentAuthRequest(paymentOrderdto,
				currency);
		String paymentAuthResponse = (String) netConnectionHelper.sendPostWithXApiKey(adyenPaymentAuthUrl,
				ctEnvProperties.getXAPIKey(), adyenPaymentAuthRequest);
		JsonObject authJson = MolekuleUtility.parseJsonObject(paymentAuthResponse);
		String streetCheck = "";
		String zipCheck = "";
		String cvcCheck = "";
		paymentResponse = netConnectionHelper.sendGetWithoutBody(token, paymentUrl);
		JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
		if (paymentJson.get(STATUS_CODE) == null) {

			String expMonth = creditCard.getExpMonth();
			String expYear = creditCard.getExpYear();
			String last4 = creditCard.getLast4();
			String brand = creditCard.getBrand();
			String charge = "";
			Boolean captured = false;
			Boolean refunded = false;
			String radarRisk = "";
			String paymentViewInStripeLink = "";
			String customerViewInStripeLink = "";
			Long version = paymentJson.get(VERSION).getAsLong();
			PaymentTypeServiceBean paymentTypeServiceBean = new PaymentTypeServiceBean();
			if (!paymentJson.has(CUSTOM)) {
				List<PaymentTypeServiceBean.Action> paymentActionsList = new ArrayList<>();
				PaymentTypeServiceBean.Action paymentAction = new PaymentTypeServiceBean.Action();
				PaymentTypeServiceBean.Action.Type paymentTypeServiceBeanActionType = new PaymentTypeServiceBean.Action.Type();
				PaymentTypeServiceBean.Action.Fields field = new PaymentTypeServiceBean.Action.Fields();
				paymentTypeServiceBeanActionType.setId(ctEnvProperties.getPaymentTypeId());
				paymentTypeServiceBeanActionType.setTypeId("type");
				paymentAction.setType(paymentTypeServiceBeanActionType);
				paymentAction.setActionName(SET_CUSTOM_TYPE);
				field.setExpMonth(expMonth);
				field.setExpYear(expYear);
				field.setLast4(last4);
				field.setBrand(brand);
				field.setCharge(charge);
				field.setStreetCheck(streetCheck);
				field.setZipCheck(zipCheck);
				field.setCvcCheck(cvcCheck);
				field.setCaptured(captured);
				field.setRefunded(refunded);
				field.setRadarRisk(radarRisk);
				field.setPaymentView(paymentViewInStripeLink);
				field.setCustomerView(customerViewInStripeLink);
				paymentAction.setFields(field);
				paymentActionsList.add(paymentAction);
				paymentTypeServiceBean.setActions(paymentActionsList);
				paymentTypeServiceBean.setVersion(version);
				paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentUrl, token,
						paymentTypeServiceBean);
				JsonObject paymentJsonObject = MolekuleUtility.parseJsonObject(paymentResponse);
				if(!paymentJsonObject.has(STATUS_CODE)) {
					PaymentTypeServiceBean paymentTypeServiceBean1 = new PaymentTypeServiceBean();
					List<PaymentTypeServiceBean.Action> paymentActionsList1 = new ArrayList<>();
					paymentTypeServiceBean1.setVersion(paymentJsonObject.get(VERSION).getAsLong());
					paymentActionsList1.add(updateCustomFieldsInPayment(customPaymentId, "paymentId"));
					paymentTypeServiceBean1.setActions(paymentActionsList1);
					paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentUrl, token,
							paymentTypeServiceBean1);
				}
				return paymentResponse;
			} else {
				List<PaymentTypeServiceBean.Action> paymentActionsList = new ArrayList<>();
				paymentTypeServiceBean.setVersion(version);
				paymentActionsList.add(updateCustomFieldsInPayment(expMonth, "expMonth"));
				paymentActionsList.add(updateCustomFieldsInPayment(expYear, "expYear"));
				paymentActionsList.add(updateCustomFieldsInPayment(last4, "last4"));
				paymentActionsList.add(updateCustomFieldsInPayment(brand, "brand"));
				paymentActionsList.add(updateCustomFieldsInPayment(charge, "charge"));
				paymentActionsList.add(updateCustomFieldsInPayment(streetCheck, "streetCheck"));
				paymentActionsList.add(updateCustomFieldsInPayment(zipCheck, "zipCheck"));
				paymentActionsList.add(updateCustomFieldsInPayment(cvcCheck, "cvcCheck"));
				paymentActionsList.add(updateCustomFieldsInPayment(captured, "captured"));
				paymentActionsList.add(updateCustomFieldsInPayment(refunded, "refunded"));
				paymentActionsList.add(updateCustomFieldsInPayment(radarRisk, "radarRisk"));
				paymentActionsList.add(updateCustomFieldsInPayment(paymentViewInStripeLink, "paymentView"));
				paymentActionsList.add(updateCustomFieldsInPayment(customerViewInStripeLink, "customerView"));
				paymentTypeServiceBean.setActions(paymentActionsList);
				return (String) netConnectionHelper.sendPostRequest(paymentUrl, token, paymentTypeServiceBean);
			}
		}
		return paymentResponse;
	}


	public String addCardDataToPayment(JsonObject jsonObject, String paymentCtUrl, String token,
			String paymentId) {
		String paymentUrl = paymentCtUrl + "/" + paymentId;
		String paymentResponse = null;
		String streetCheck = "";
		String zipCheck = "";
		String cvcCheck = "";
		if (jsonObject.has(CHARGES) && jsonObject.get(CHARGES).getAsJsonObject().has(DATA)) {
			JsonArray dataJsonArray = jsonObject.get(CHARGES).getAsJsonObject().get(DATA).getAsJsonArray();
			JsonObject dataJsonObject = dataJsonArray.get(0).getAsJsonObject();
			JsonObject cardJsonObject = dataJsonObject.get("payment_method_details").getAsJsonObject().get("card")
					.getAsJsonObject();
			JsonObject checksJsonObject = cardJsonObject.get("checks").getAsJsonObject();
			String expMonth = cardJsonObject.get("exp_month").getAsString();
			String expYear = cardJsonObject.get("exp_year").getAsString();
			String last4 = cardJsonObject.get("last4").getAsString();
			String brand = cardJsonObject.get("brand").getAsString();
			String charge = dataJsonObject.get("status").getAsString();
			if (!checksJsonObject.get("address_line1_check").isJsonNull()) {
				streetCheck = checksJsonObject.get("address_line1_check").getAsString();
			}
			if (!checksJsonObject.get("address_postal_code_check").isJsonNull()) {
				zipCheck = checksJsonObject.get("address_postal_code_check").getAsString();
			}
			if (!checksJsonObject.get("cvc_check").isJsonNull()) {
				cvcCheck = checksJsonObject.get("cvc_check").getAsString();
			}
			Boolean captured = dataJsonObject.get("captured").getAsBoolean();
			Boolean refunded = dataJsonObject.get("refunded").getAsBoolean();
			String radarRisk = dataJsonObject.get("outcome").getAsJsonObject().get("risk_level").getAsString();
			String viewInStripeHost = "https://dashboard.stripe.com/";

			String stripePaymentId = dataJsonObject.get("id").getAsString();
			String stripeCustomerId = dataJsonObject.get("customer").getAsString();
			String paymentViewInStripeLink = viewInStripeHost+"payments/"+stripePaymentId;
			String customerViewInStripeLink = viewInStripeHost+"customers/"+stripeCustomerId;
			paymentResponse = netConnectionHelper.sendGetWithoutBody(token, paymentUrl);
			JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
			if (paymentJson.get(STATUS_CODE) == null) {
				Long version = paymentJson.get(VERSION).getAsLong();
				PaymentTypeServiceBean paymentTypeServiceBean = new PaymentTypeServiceBean();

				if (!paymentJson.has(CUSTOM)) {
					List<PaymentTypeServiceBean.Action> paymentActionsList = new ArrayList<>();
					PaymentTypeServiceBean.Action paymentAction = new PaymentTypeServiceBean.Action();
					PaymentTypeServiceBean.Action.Type paymentTypeServiceBeanActionType = new PaymentTypeServiceBean.Action.Type();
					PaymentTypeServiceBean.Action.Fields field = new PaymentTypeServiceBean.Action.Fields();
					paymentTypeServiceBeanActionType.setId(ctEnvProperties.getPaymentTypeId());
					paymentTypeServiceBeanActionType.setTypeId("type");
					paymentAction.setType(paymentTypeServiceBeanActionType);
					paymentAction.setActionName(SET_CUSTOM_TYPE);
					field.setExpMonth(expMonth);
					field.setExpYear(expYear);
					field.setLast4(last4);
					field.setBrand(brand);
					field.setCharge(charge);
					field.setStreetCheck(streetCheck);
					field.setZipCheck(zipCheck);
					field.setCvcCheck(cvcCheck);
					field.setCaptured(captured);
					field.setRefunded(refunded);
					field.setRadarRisk(radarRisk);
					field.setPaymentView(paymentViewInStripeLink);
					field.setCustomerView(customerViewInStripeLink);
					paymentAction.setFields(field);
					paymentActionsList.add(paymentAction);
					paymentTypeServiceBean.setActions(paymentActionsList);
					paymentTypeServiceBean.setVersion(version);
					paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentUrl, token,
							paymentTypeServiceBean);
					return paymentResponse;
				} else {
					List<PaymentTypeServiceBean.Action> paymentActionsList = new ArrayList<>();
					paymentTypeServiceBean.setActions(paymentActionsList);
					paymentTypeServiceBean.setVersion(version);
					paymentActionsList.add(updateCustomFieldsInPayment(expMonth, "expMonth"));
					paymentActionsList.add(updateCustomFieldsInPayment(expYear, "expYear"));
					paymentActionsList.add(updateCustomFieldsInPayment(last4, "last4"));
					paymentActionsList.add(updateCustomFieldsInPayment(brand, "brand"));
					paymentActionsList.add(updateCustomFieldsInPayment(charge, "charge"));
					paymentActionsList.add(updateCustomFieldsInPayment(streetCheck, "streetCheck"));
					paymentActionsList.add(updateCustomFieldsInPayment(zipCheck, "zipCheck"));
					paymentActionsList.add(updateCustomFieldsInPayment(cvcCheck, "cvcCheck"));
					paymentActionsList.add(updateCustomFieldsInPayment(captured, "captured"));
					paymentActionsList.add(updateCustomFieldsInPayment(refunded, "refunded"));
					paymentActionsList.add(updateCustomFieldsInPayment(radarRisk, "radarRisk"));
					paymentActionsList.add(updateCustomFieldsInPayment(paymentViewInStripeLink, "paymentView"));
					paymentActionsList.add(updateCustomFieldsInPayment(customerViewInStripeLink, "customerView"));
					return (String) netConnectionHelper.sendPostRequest(paymentUrl, token, paymentTypeServiceBean);
				}
			}
		}
		return paymentResponse;
	}

	private Action updateCustomFieldsInPayment(Object value, String name) {
		PaymentTypeServiceBean.Action paymentAction = new PaymentTypeServiceBean.Action();
		paymentAction.setActionName(SET_CUSTOM_FIELD);
		paymentAction.setName(name);
		paymentAction.setValue(value);
		return paymentAction;
	}

	private String createPaymentAuth(String customerId) {
		String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
		JsonObject jsonObject = null;
		CustomerResponseBean customerResponseBean = registrationHelperService.getCustomerById(customerId);
		String name = customerResponseBean.getFirstName();
		String email = customerResponseBean.getEmail();

		// create stripe customer with email and name
		MultiValueMap<String, String> customerFormData = new LinkedMultiValueMap<>();
		customerFormData.add("name", name);
		customerFormData.add(EMAIL, email);
		String customerResponse = (String) netConnectionHelper.sendFormUrlEncoded(createCustomerUrl, authToken,
				customerFormData);
		jsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		String stripeCustomerId = jsonObject.get("id").getAsString();

		// create setup intent by stripe customer and return the client secret
		MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
		formData.add(CUSTOMER, stripeCustomerId);
		String response = (String) netConnectionHelper.sendFormUrlEncoded(setupPaymentIntentUrl, authToken, formData);
		jsonObject = MolekuleUtility.parseJsonObject(response);
		String clientSecretStr = jsonObject.get("client_secret").getAsString();
		ClientSecret clientSecret = new ClientSecret();
		clientSecret.setClientSecretValue(clientSecretStr);
		return CheckoutUtility.convertObjToString(clientSecret);
	}

	public ResponseEntity<AccountCustomObject> getAccountCustomObjectApi(String accountId, String customerId) {
		return checkoutHelperService.getAccountCustomObjectApi(accountId, customerId);
	}

	public String sendOrderConfEmail(String orderNumber) {
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResponse = orderHelperService.getOrderByNumber(orderNumber, token);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		if(jsonObject.get(STATUS_CODE) != null) {
			ErrorResponse errorResponse = new ErrorResponse(400, "Invalid Order Number..");
			return errorResponse.toString();
		}
		SendGridModel sendGridModel = getSendGridRequestData(orderResponse);
		String paymentId = jsonObject.get(PAYMENT_INFO).getAsJsonObject().get("payments").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString();
		String paymentIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+"/payments/"+paymentId;
		String paymentResponse = netConnectionHelper.sendGetWithoutBody(token, paymentIdUrl);
		JsonObject paymentJsonObject = MolekuleUtility.parseJsonObject(paymentResponse);
		sendGridModel.setOrderPaymentMethod(paymentJsonObject.get("paymentMethodInfo").getAsJsonObject().get("method").getAsString());
		String customerId = jsonObject.get("customerId").getAsString();
		String channel = "";
		if(customerId != null) {
			String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ SLASH_CUSTOMERS_SLASH + customerId;
			String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
			JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
			sendGridModel.setFirstName(customerJsonObject.get("firstName").getAsString());
			if (customerJsonObject.has(CUSTOM)) {
				JsonObject fieldObject = customerJsonObject.get(CUSTOM).getAsJsonObject().get("fields").getAsJsonObject();
				channel = fieldObject.get(CHANNEL).getAsString();
				if(channel.equals("B2B"))
					orderConfirmMailSender.sendMail(sendGridModel);
				if(channel.equals("D2C"))
					orderConfirmD2CMailSender.sendMail(sendGridModel);
			}
		}
		OrderEmailResponse emailResponse = new OrderEmailResponse();
		emailResponse.setStatus("Email sent");
		return CheckoutUtility.convertObjToString(emailResponse);
	}

	public SendGridModel getSendGridRequestData(String orderResponse) {
		SendGridModel sendGridModel = new SendGridModel();
		try {
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
			String country = "US";
			if(jsonObject.has("country")) {
				country = jsonObject.get("country").getAsString();
			}
			String channel = jsonObject.getAsJsonObject(CUSTOM).getAsJsonObject("fields").get(CHANNEL).getAsString();
			sendGridModel.setEmail(jsonObject.get("customerEmail").getAsString());
			sendGridModel.setOrderNumber(jsonObject.get(ORDER_NUMBER).getAsString());
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(IS_A_GIFT)) {
				sendGridModel.setGift(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(IS_A_GIFT).getAsBoolean());
			}else {
				sendGridModel.setGift(false);
			}
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(SUB_TOTAL)) {
				sendGridModel.setOrderSummarySubtotal(centToDollarConversion(
						jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(SUB_TOTAL).getAsLong(),country));
			}
			if(country.equals("US") && jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(TOTAL_TAX)) {
				sendGridModel.setOrderSummaryTax(centToDollarConversion(
						jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(TOTAL_TAX).getAsLong(),country));
			}
			if(country.equals("GB")) {
				sendGridModel.setTaxNote("Including VAT");
			}
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(SHIPPING_COST)) {
				sendGridModel.setOrderSummaryShipping(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject().get(SHIPPING_COST).getAsLong(),country));
			}
			if(jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(HANDLING_COST)) {
				sendGridModel.setOrderSummaryHandling(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject().get(HANDLING_COST).getAsLong(),country));
			}
			if (jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
					.get(TOTAL_DISCOUNT_PRICE) != null) {
				sendGridModel.setOrderSummaryDiscount(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject().get(TOTAL_DISCOUNT_PRICE).getAsLong(),country));
			}
			sendGridModel.setOrderSummaryTotal(centToDollarConversion(jsonObject.get(CUSTOM).getAsJsonObject()
					.get(FIELDS).getAsJsonObject().get(ORDER_TOTAL).getAsLong(),country));
			JsonArray lineItemsArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
			List<Map<String,Object>> productlist = new ArrayList<Map<String,Object>>();
			if(channel.equals("B2B")) {
				getProductDetail(lineItemsArray,channel,country);
			}else if(channel.equals("D2C")) {
				for (int i = 0; i < lineItemsArray.size(); i++) {
					Map<String,Object> productMap = new HashMap<String,Object>();
					JsonObject lineitemJson = lineItemsArray.get(i).getAsJsonObject();
					if(lineitemJson.has(CUSTOM)&&lineitemJson.get(CUSTOM).getAsJsonObject().has(FIELDS)&&
							lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has("subscriptionEnabled")){
						productMap.put("subscription", lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().
								get("subscriptionEnabled").getAsBoolean());
						if(country.equals("US")) {
							productMap.put("subscriptionHeader", lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());
						}else {
							productMap.put("subscriptionHeader", lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());
						}
						productMap.put("SubscriptionPrice", centToDollarConversion(
								lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong(),country));
						productMap.put("orderImageUrl",getProductImage(lineitemJson.get("productId").getAsString(),country,channel));
						productlist.add(productMap);
					}else {
						if(country.equals("US")) {
							productMap.put("orderProductName", lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());	
						}else{
							productMap.put("orderProductName", lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());	
						}
						productMap.put("orderProductQty",lineitemJson.get(QUANTITY).getAsLong());
						productMap.put("orderProductPrice", centToDollarConversion(
								lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong(),country));
						productMap.put("orderImageUrl",getProductImage(lineitemJson.get("productId").getAsString(),country,channel));
						productlist.add(productMap);
					}
				}
			}
			sendGridModel.setProductlist(productlist);
			if (jsonObject.get(SHIPPING_INFO) != null) {
				sendGridModel.setOrderShippingMethod(
						jsonObject.get(SHIPPING_INFO).getAsJsonObject().get("shippingMethodName").getAsString());
			}
			if (jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("company") != null) {
				sendGridModel.setOrderShippingAddressCompanyName(
						jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("company").getAsString());
			}
			sendGridModel.setOrderShippingAddressContactFirstName(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(FIRST_NAME).getAsString());
			sendGridModel.setOrderShippingAddressContactLastName(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("lastName").getAsString());
			sendGridModel.setOrderShippingAddressStreet1(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("streetName").getAsString());
			sendGridModel.setOrderShippingAddressCity(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("city").getAsString());
			if (country.equals("US") && jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has(STATE)){
				sendGridModel.setOrderShippingAddressState(
						jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(STATE).getAsString());
			}
			sendGridModel.setOrderShippingAddressZip(
					jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(POSTAL_CODE).getAsString());
			if(jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().has(PHONE))
				sendGridModel.setOrderShippingAddressPhoneNumber(
						jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get(PHONE).getAsString());

			sendGridModel.setOrderBillingAddressFirstName(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(FIRST_NAME).getAsString());
			sendGridModel.setOrderBillingAddressLastName(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get("lastName").getAsString());
			sendGridModel.setOrderBillingAddressStreet1(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get("streetName").getAsString());
			sendGridModel.setOrderBillingAddressCity(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get("city").getAsString());
			if(country.equals("US") && jsonObject.get(BILLING_ADDRESS).getAsJsonObject().has(STATE)) {
				sendGridModel.setOrderBillingAddressState(
						jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(STATE).getAsString());
			}
			sendGridModel.setOrderBillingAddressZip(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(POSTAL_CODE).getAsString());
			sendGridModel.setOrderBillingAddressPhoneNumber(
					jsonObject.get(BILLING_ADDRESS).getAsJsonObject().get(PHONE).getAsString());

		} catch (Exception e) {
			logger.error("Error occured in getSendGridRequestData - ", e);
		}
		return sendGridModel;
	}

	private String getProductImage(String productId,String country,String channel) {
		String sku = null;
		String prismicImageResponse = null;
		String prismicImageUrl = null;
		try {
			String token = ctServerHelperService.getStringAccessToken();
			String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
					.append(ctEnvProperties.getProjectKey()).append("/products/").append(productId)
					.append("?country=").append(country).toString();
			ProductBean productBeanResponseData = netConnectionHelper.sendGetProductDataWithoutBody(token, url);
			ProductData current = productBeanResponseData.getMasterData().getCurrent();
			/**slug = current.getSlug().get(EN_US);*/
			sku = current.getMasterVariant().getSku();
			if(country.equals("GB")) {
				prismicImageUrl = ctEnvProperties.getPrismicApiUrl()+sku+"?country="+country;
			}else if(country.equals("US")) {
				if(channel.equals("D2C")) {
					prismicImageUrl = ctEnvProperties.getPrismicApiD2CUrl()+sku+"?country="+country;
				}else if(channel.equals("B2B")) {
					prismicImageUrl = ctEnvProperties.getPrismicApiB2BUrl()+sku+"?country="+country;
				}
			}
			prismicImageResponse = netConnectionHelper.sendGetWithoutBody(null, prismicImageUrl);
			prismicImageResponse = prismicImageResponse.replace("prismic-images", "images.prismic.io");
			prismicImageResponse = "https:/"+prismicImageResponse;
		}catch (Exception e) {
			logger.error("Error occured in getSendGridRequestData - ", e);
		}
		return prismicImageResponse;
	}



	public String centToDollarConversion(long centPayment,String country) {
		NumberFormat numberFormat = null;
		if(country.equals("US")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		}else if(country.equals("GB")){
			numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		}
		return numberFormat.format(centPayment / 100.0);
	}
	
	private List<Product> getProductDetail(JsonArray lineItemsArray,String channel,String country){
		List<Product> products = new ArrayList<>();
		for (int i = 0; i < lineItemsArray.size(); i++) {
			Product product = new Product();
			JsonObject lineitemJson = lineItemsArray.get(i).getAsJsonObject();
			if(lineitemJson.has(CUSTOM)&&lineitemJson.get(CUSTOM).getAsJsonObject().has(FIELDS)&&
					lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has("subscriptionEnabled")){
				product.setSubscription(lineitemJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().
						get("subscriptionEnabled").getAsBoolean());
				if(country.equals("US")) {
					product.setSubscriptionHeader(lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());
				}else {
					product.setSubscriptionHeader(lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());
				}
				product.setSubscriptionPrice(centToDollarConversion(
						lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong(),country));
				product.setOrderImageUrl(getProductImage(lineitemJson.get("productId").getAsString(),channel,country));
				products.add(product);
			}else {
				if(country.equals("US")) {
					product.setOrderProductName(lineitemJson.get("name").getAsJsonObject().get("en-US").getAsString());
				}else {
					product.setOrderProductName(lineitemJson.get("name").getAsJsonObject().get("en-GB").getAsString());
				}
				product.setOrderProductQty(lineitemJson.get(QUANTITY).getAsLong());
				product.setOrderProductPrice(centToDollarConversion(
						lineitemJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong(),country));
				product.setOrderImageUrl(getProductImage(lineitemJson.get("productId").getAsString(),channel,country));
				products.add(product);
			}
		}
		return products;
	}

	public String sendOrderInvoiceEmail(String orderNumber) {
		String customerResponse = null;
		List<String> invoiceemailAddress = new ArrayList<>();
		SendGridModel sendGridModel = new SendGridModel();
		sendGridModel.setOrderNumber(orderNumber);
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResponse = orderHelperService.getOrderByNumber(orderNumber, token);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		if(jsonObject.get(STATUS_CODE) != null) {
			ErrorResponse errorResponse = new ErrorResponse(400, "Invalid Order Number..");
			return errorResponse.toString();
		}
		String customerId = jsonObject.get("customerId").getAsString();
		if(customerId != null) {
			String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ SLASH_CUSTOMERS_SLASH + customerId;
			customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
		}
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		JsonArray invoicearray = customerJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).
				getAsJsonObject().get("invoiceEmailAddress").getAsJsonArray();
		for(int i =0;i<invoicearray.size();i++) {
			invoiceemailAddress.add(invoicearray.get(i).getAsString().replace(" ", "+"));
		}
		sendGridModel.setInvoiceEmailAddresses(invoiceemailAddress);
		if(customerJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(CHANNEL).getAsString().equals("B2B")){
			String accountId = customerJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get("accountId")
					.getAsString();
			String accountKey = registrationHelperService.getAccountKeyById(accountId);
			AccountCustomObject accountCustomObject = (AccountCustomObject) registrationHelperService.getAccountCustomObject(accountKey);
			sendGridModel.setSalesRepEmail(accountCustomObject.getValue().getSalesRepresentative().getEmail());
			sendGridModel
			.setSalesRepPhoneNumber(accountCustomObject.getValue().getSalesRepresentative().getPhone());
		}
		JsonArray lineItemsArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i=0 ; i<lineItemsArray.size();i++) {
			if((lineItemsArray.get(i).getAsJsonObject().has(CUSTOM)) && (lineItemsArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has("invoiceUrl"))) {
				sendGridModel.setInvoiceUrl(lineItemsArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS)
						.getAsJsonObject().get("invoiceUrl").getAsString());
			}
		}
		if (!jsonObject.get(MolekuleConstant.SHIPPING_INFO).getAsJsonObject().get(SHIPPING_METHOD_NAME)
				.getAsString().equals("Contact Carrier") && jsonObject.has(MolekuleConstant.SHIPPING_INFO)) {
			sendGridModel.setOrderFedExTrackingInfo(jsonObject.get(MolekuleConstant.SHIPPING_INFO)
					.getAsJsonObject().get("deliveries").getAsJsonArray().get(0).getAsJsonObject()
					.get("parcels").getAsJsonArray().get(0).getAsJsonObject().get("trackingData")
					.getAsJsonObject().get("trackingId").getAsString());
		} else if (jsonObject.get(MolekuleConstant.CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS)
				.getAsJsonObject().has(MolekuleConstant.OWN_CARRIER)) {
			JsonObject ownCarrierObject = MolekuleUtility.parseJsonObject(
					jsonObject.get(MolekuleConstant.CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS)
					.getAsJsonObject().get(MolekuleConstant.OWN_CARRIER).getAsString());
			if (ownCarrierObject.has("carrierName") && ownCarrierObject.has("carrierAccountNumber")) {
				sendGridModel.setCustomerCarrierName(ownCarrierObject.get("carrierName").getAsString());
				sendGridModel.setCustomerCarrierAccountNumber(
						ownCarrierObject.get("carrierAccountNumber").getAsString());
			}
		}
		orderInvoiceMailSender.sendMail(sendGridModel);
		OrderEmailResponse emailResponse = new OrderEmailResponse();
		emailResponse.setStatus("Email sent");
		return CheckoutUtility.convertObjToString(emailResponse);
	}

	public String affirmCharges(AffirmCheckoutBean affirmCheckoutBean) {

		String affirmAuthorizeToken = getAffirmAuthorizeToken();
		return (String) netConnectionHelper.sendPostRequest(ctEnvProperties.getAffirmServiceBaseUrl() + ctEnvProperties.getAffirmChargesUrl(), affirmAuthorizeToken, affirmCheckoutBean);

	}

	public String affirmCaptureUrl(String orderId, String chargeId) {

		String affirmCaptureUrl = String
				.format(new StringBuilder(ctEnvProperties.getAffirmServiceBaseUrl()).append( ctEnvProperties.getAffirmCaptureUrl()).toString(), chargeId);

		AffirmCaptureRequestBean affirmCaptureRequestBean = new AffirmCaptureRequestBean();
		affirmCaptureRequestBean.setOrderId(orderId);

		return (String) netConnectionHelper.sendPostRequest(affirmCaptureUrl, getAffirmAuthorizeToken(), affirmCaptureRequestBean);

	}



	private UpdateActionsOnCart.Action.Address getAffirmBillingAddress (JsonObject affirmChargesResponse) {
		BillingAddresses billingAddresses = new BillingAddresses();
		JsonObject detailsJSONObject = affirmChargesResponse.get("details").getAsJsonObject();
		JsonObject billingJSONObject = detailsJSONObject.get("billing").getAsJsonObject();

		if (billingJSONObject.has("name")) {
			JsonObject nameJSONObject = billingJSONObject.get("name").getAsJsonObject();
			billingAddresses
			.setFirstName((nameJSONObject.has("first") ? nameJSONObject.get("first").getAsString() : null));
			billingAddresses
			.setLastName((nameJSONObject.has("last") ? nameJSONObject.get("last").getAsString() : null));

		}
		if (billingJSONObject.has("address")) {

			JsonObject addressJSONObject = billingJSONObject.get("address").getAsJsonObject();
			billingAddresses.setStreetAddress1(
					(addressJSONObject.has("line1") ? addressJSONObject.get("line1").getAsString() : null));
			billingAddresses.setStreetAddress2(
					(addressJSONObject.has("line2") ? addressJSONObject.get("line2").getAsString() : null));
			billingAddresses
			.setCity((addressJSONObject.has("city") ? addressJSONObject.get("city").getAsString() : null));
			billingAddresses
			.setState((addressJSONObject.has("state") ? addressJSONObject.get("state").getAsString() : null));
			billingAddresses.setPostalCode(
					(addressJSONObject.has("zipcode") ? addressJSONObject.get("zipcode").getAsString() : null));

			if(addressJSONObject.has("country") && "USA".equals(addressJSONObject.get("country").getAsString())) {
				billingAddresses.setCountry("US");	
			}

		}
		billingAddresses.setPhoneNumber(
				(billingJSONObject.has("phone_number") ? billingJSONObject.get("phone_number").getAsString() : null));

		return CheckoutUtility.getAddressAction(billingAddresses);
	}

	public String getAffirmAuthorizeToken() {
		return "basic " + Base64Utils.encodeToString(
				(ctEnvProperties.getAffirmPublicKey() + ":" + ctEnvProperties.getAffirmPrivateKey()).getBytes());

	}


	/**
	 * getCheckoutSurveys is to get checkout survey question and answers
	 * 
	 * @return String
	 */
	public String getCheckoutSurveys() {
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + MolekuleConstant.CUSTOM_OBJECTS + "/"+ctEnvProperties.getCheckoutSurveyKey()+"/"
				+ctEnvProperties.getCheckoutSurveyContainer();
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		return netConnectionHelper.sendGetWithoutBody(token, baseUrl);
	}

	/**
	 * getSurveyResponses is to get Customer Survey Response from checkout survey object
	 * @param offset 
	 * @param limit 
	 * 
	 * @return CheckoutSurveyResponse
	 */
	public CheckoutSurveyResponse getSurveyResponses(int limit, int offset, String email, String answer, String customerNumber) {
		CheckoutSurveyResponse checkoutSurveyResponse = new CheckoutSurveyResponse();
		List<CheckoutSurvey> totalCheckoutSurveyList =getCheckoutSurveyList(getCustomerSurveyResponse());
		if(StringUtils.hasText(email)) {
			totalCheckoutSurveyList = totalCheckoutSurveyList.stream().filter(survey -> email.equals(survey.getEmail())).collect(Collectors.toList());
		}
		if(StringUtils.hasText(customerNumber)) {
			totalCheckoutSurveyList = totalCheckoutSurveyList.stream().filter(survey -> customerNumber.equals(survey.getCustomerNumber())).collect(Collectors.toList());
		}
		if(StringUtils.hasText(answer)&&(answer.equals("Others"))) {
			totalCheckoutSurveyList = totalCheckoutSurveyList.stream().filter(survey -> Boolean.TRUE.equals(survey.isOther())).collect(Collectors.toList());
		}else if(StringUtils.hasText(answer)) {
			totalCheckoutSurveyList = totalCheckoutSurveyList.stream().filter(survey -> answer.equals(survey.getSurveyResponse())).collect(Collectors.toList());
		}
		Collections.sort(totalCheckoutSurveyList, (o1, o2) -> o2.getLastModifiedDate().compareTo(o1.getLastModifiedDate()));
		List<CheckoutSurvey> checkoutSurveyList = totalCheckoutSurveyList.stream().skip(offset).limit(limit)
				.collect(Collectors.toList());
		checkoutSurveyResponse.setLimit(limit);
		checkoutSurveyResponse.setOffset(offset);
		checkoutSurveyResponse.setCount(checkoutSurveyList.size());
		checkoutSurveyResponse.setTotal(totalCheckoutSurveyList.size());
		checkoutSurveyResponse.setCheckoutSurveyList(checkoutSurveyList);
		return checkoutSurveyResponse;

	}

	public String getCustomerSurveyResponse() {
		String token = ctServerHelperService.getStringAccessToken();
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + MolekuleConstant.CUSTOM_OBJECTS + "/d2c/checkout_survey_response";
		return netConnectionHelper.sendGetWithLimitSize(token, baseUrl);

	}

	/**
	 * getCheckoutSurveyList is to map the response to checkout survey object
	 * 
	 * @param response
	 * @return
	 */
	public List<CheckoutSurvey> getCheckoutSurveyList(String response){
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonObject valueObject = jsonObject.get(MolekuleConstant.VALUE).getAsJsonObject();
		JsonArray resultArrays = valueObject.has("checkout_surveys")?valueObject.get("checkout_surveys").getAsJsonArray():new JsonArray();
		List<CheckoutSurvey> checkoutSurveyList = new ArrayList<>();
		for(int i=0;i<resultArrays.size();i++) {
			CheckoutSurveyResponse.CheckoutSurvey checkoutSurvey= new CheckoutSurveyResponse.CheckoutSurvey();
			checkoutSurvey.setId(resultArrays.get(i).getAsJsonObject().get("id").getAsString());
			checkoutSurvey.setCustomerId(resultArrays.get(i).getAsJsonObject().get("customerId").getAsString());
			if(resultArrays.get(i).getAsJsonObject().has(CUSTOMER_NUMBER)) {
				checkoutSurvey.setCustomerNumber(resultArrays.get(i).getAsJsonObject().get(CUSTOMER_NUMBER).getAsString());				
			}
			checkoutSurvey.setEmail(resultArrays.get(i).getAsJsonObject().get("email").getAsString());
			checkoutSurvey.setOther(resultArrays.get(i).getAsJsonObject().get("other").getAsBoolean());
			checkoutSurvey.setSurveyResponse(resultArrays.get(i).getAsJsonObject().get("surveyResponse").getAsString());
			checkoutSurvey.setCreatedDate(resultArrays.get(i).getAsJsonObject().get("createdDate").getAsString());
			checkoutSurvey.setLastModifiedDate(resultArrays.get(i).getAsJsonObject().get("lastModifiedDate").getAsString());
			checkoutSurveyList.add(checkoutSurvey);
		}
		return checkoutSurveyList;

	}

	/**
	 * createSurveyResponse is to create a new customer response in custom object
	 * 
	 * @param customerId
	 * @param email
	 * @param surveyResponse
	 * @return ResponseEntity<Object>
	 */
	public ResponseEntity<Object> createSurveyResponse(String customerId,String customerNumber, String email, String surveyResponse, boolean other) {
		String response =getCustomerSurveyResponse();
		List<CheckoutSurvey> checkoutSurveyList = getCheckoutSurveyList(response);

		CheckoutSurveyResponse.CheckoutSurvey checkoutSurvey= new CheckoutSurveyResponse.CheckoutSurvey();
		checkoutSurvey.setId(MolekuleUtility.createUuidValue());
		checkoutSurvey.setCustomerId(customerId);
		checkoutSurvey.setCustomerNumber(customerNumber);
		checkoutSurvey.setEmail(email);
		checkoutSurvey.setOther(other);
		checkoutSurvey.setSurveyResponse(surveyResponse);
		LocalDateTime currentDate = LocalDateTime.now();
		checkoutSurvey.setCreatedDate(currentDate.toString());	
		checkoutSurvey.setLastModifiedDate(currentDate.toString());	
		checkoutSurveyList.add(checkoutSurvey);
		String surveys = updateCustomerSurveyObject(response, checkoutSurveyList);
		List<CheckoutSurvey> surveyList = getCheckoutSurveyList(surveys);
		CheckoutSurveyResponse checkoutSurveyResponse = new CheckoutSurveyResponse(); 
		checkoutSurveyResponse.setTotal(surveyList.size());
		checkoutSurveyResponse.setCount(surveyList.size());
		checkoutSurveyResponse.setCheckoutSurveyList(surveyList);
		return new ResponseEntity<>(checkoutSurveyResponse, HttpStatus.OK);
	}

	/**
	 * deleteSurveyResponse is to delete a given survey from checkoutsurvey object
	 * 
	 * @param id
	 * @return ResponseEntity<Object>
	 */
	public ResponseEntity<Object> deleteSurveyResponse(String id) {
		String response =getCustomerSurveyResponse();
		List<CheckoutSurvey> checkoutSurveyList = getCheckoutSurveyList(response);
		CheckoutSurvey checkoutSurvey = checkoutSurveyList.stream().filter(survey -> id.equals(survey.getId()))
				.findAny().orElse(null);
		if(checkoutSurvey == null) {
			return new ResponseEntity<>(new ErrorResponse(400, "Resources does not have a valid SurveyResponse Id"),HttpStatus.BAD_REQUEST);
		}
		checkoutSurveyList.removeIf(survey -> survey.getId().equals(id));
		String surveys = updateCustomerSurveyObject(response, checkoutSurveyList);
		List<CheckoutSurvey> surveyList = getCheckoutSurveyList(surveys);
		CheckoutSurveyResponse checkoutSurveyResponse = new CheckoutSurveyResponse(); 
		checkoutSurveyResponse.setTotal(surveyList.size());
		checkoutSurveyResponse.setCount(surveyList.size());
		checkoutSurveyResponse.setCheckoutSurveyList(surveyList);
		return new ResponseEntity<>(checkoutSurveyResponse, HttpStatus.OK);
	}

	/**
	 * updateSurveyResponse is to update an given survey object
	 * 
	 * @param id
	 * @param surveyResponse
	 * @return ResponseEntity<Object>
	 */
	public ResponseEntity<Object> updateSurveyResponse(String id,String surveyResponse,boolean other) {
		String response =getCustomerSurveyResponse();
		List<CheckoutSurvey> checkoutSurveyList = getCheckoutSurveyList(response);
		//To check whether it is a valid survey id
		CheckoutSurvey checkoutSurvey = checkoutSurveyList.stream().filter(survey -> id.equals(survey.getId()))
				.findAny().orElse(null);
		if(checkoutSurvey == null) {
			return new ResponseEntity<>(new ErrorResponse(400, "Resources does not have a valid SurveyResponse Id"),HttpStatus.BAD_REQUEST);
		}
		checkoutSurveyList.stream().filter(survey -> id.equals(survey.getId())).forEach(survey->{
			survey.setSurveyResponse(surveyResponse);
			LocalDateTime currentDate = LocalDateTime.now();
			survey.setLastModifiedDate(currentDate.toString());
			survey.setOther(other);

		});
		String surveys = updateCustomerSurveyObject(response, checkoutSurveyList);
		List<CheckoutSurvey> surveyList = getCheckoutSurveyList(surveys);
		CheckoutSurveyResponse checkoutSurveyResponse = new CheckoutSurveyResponse(); 
		checkoutSurveyResponse.setCheckoutSurveyList(surveyList);
		checkoutSurveyResponse.setTotal(surveyList.size());
		checkoutSurveyResponse.setCount(surveyList.size());
		return new ResponseEntity<>(checkoutSurveyResponse, HttpStatus.OK);
	}

	/**
	 * updateCustomerSurveyObject method is to update customerSurvey in
	 * accountcustomobject
	 * 
	 * @return String
	 */
	public String updateCustomerSurveyObject(String response, List<CheckoutSurvey> checkoutSurveyList) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonObject valueObject = jsonObject.get(MolekuleConstant.VALUE).getAsJsonObject();
		com.molekule.api.v1.commonframework.dto.registration.Value value = new com.molekule.api.v1.commonframework.dto.registration.Value();
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		try {
			value = mapper.readValue(valueObject.toString(), com.molekule.api.v1.commonframework.dto.registration.Value.class);
			value.setCheckoutSurveyList(checkoutSurveyList);
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured in updateCustomerSurveyObject ", e);
		}
		accountCustomObject.setContainer(jsonObject.get(MolekuleConstant.CONTAINER).getAsString());
		accountCustomObject.setKey(jsonObject.get("key").getAsString());
		accountCustomObject.setVersion(jsonObject.get(VERSION).getAsInt());
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + MolekuleConstant.CUSTOM_OBJECTS;
		// updating survey custom object in CT server.
		return (String) netConnectionHelper.sendPostRequestWithLimitSize(accountBaseUrl,
				ctServerHelperService.getStringAccessToken(), accountCustomObject);

	}

	/**
	 * getCheckoutSurveyResponse is to get checkout survey response by surveyId
	 * 
	 * @param surveyId
	 * @return
	 */
	public ResponseEntity<Object> getCheckoutSurveyResponse(String surveyId) {
		String response =getCustomerSurveyResponse();
		List<CheckoutSurvey> checkoutSurveyList = getCheckoutSurveyList(response);
		CheckoutSurvey checkoutSurvey = checkoutSurveyList.stream().filter(survey -> surveyId.equals(survey.getId()))
				.findAny().orElse(null);
		if(checkoutSurvey == null) {
			return new ResponseEntity<>(new ErrorResponse(400, "Resources does not have a valid SurveyResponse Id"),HttpStatus.BAD_REQUEST);
		}	
		return new ResponseEntity<>(checkoutSurvey, HttpStatus.OK);
	}


	public String addLineItemIntoCart(String orderId, Boolean loadShippingAddress, Boolean loadShippingMethod, String country) {
		String response = "";

		ResponseEntity<Object> orderResponse = orderHelperService.getOrderByOrderId(orderId);
		String invalidRequest = new ErrorResponse(HttpStatus.BAD_REQUEST.value(),
				"Resources does not have a valid Order Id").toString();

		if (orderResponse == null || orderResponse.getStatusCode().equals(HttpStatus.BAD_REQUEST) || orderResponse.getBody() == null) {
			return invalidRequest;
		}

		JsonObject orderResponseJsonObject = MolekuleUtility.parseJsonObject(orderResponse.getBody().toString());

		if (!orderResponseJsonObject.has(CUSTOMER_ID) || !orderResponseJsonObject.has(LINE_ITEMS)) {
			return invalidRequest;
		}

		String customerId = orderResponseJsonObject.get(CUSTOMER_ID).getAsString();

		String cartId = cartHelperService.createCart(customerId, country);

		response = cartHelperService.processLineItem(orderResponseJsonObject, cartId, country);

		if (Boolean.TRUE.equals(loadShippingAddress)) {
			response = cartHelperService.loadShippingAddress(orderResponseJsonObject, cartId, country);
		}

		if (Boolean.TRUE.equals(loadShippingMethod)) {
			//			TODO: Add country in the second Param instead of empty
			ResponseEntity<Object> fedexResponse = fedExApiService.prepareFedExApi(cartId, country);

			List<FedExResponse> fedExResponses = (List<FedExResponse>)fedexResponse.getBody();

			JsonObject shippingInfoJSONObject = orderResponseJsonObject.get("shippingInfo").getAsJsonObject();
			String shippingMethodName = shippingInfoJSONObject.get("shippingMethodName").getAsString();
			String cost = "";
			for (FedExResponse eachFedexResponse : fedExResponses) {
				if(shippingMethodName.equals(eachFedexResponse.getServiceName())) {
					cost= eachFedexResponse.getCost();
				}
			}
			ShippingMethod shippingMethod = new ShippingMethod();
			shippingMethod.setName(shippingMethodName);
			shippingMethod.setCost(cost);
			CartRequestBean cartRequestBean = new CartRequestBean();
			cartRequestBean.setShippingMethod(shippingMethod);
			cartRequestBean.setAction(CartServiceUtility.CartActions.SET_SHIPPING_METHOD.name());
			cartRequestBean.setCountry(country);
			response = cartHelperService.updateCartById(cartId, cartRequestBean);
		}
		return response;
	}
	public Payment prepareCTPaymentRequest(JsonObject cartJson,String paymentAuthToken, String currency) {
		Payment payment = new Payment();
		payment.setInterfaceId(paymentAuthToken);
		Payment.AmountPlanned amountPlanned = new Payment.AmountPlanned();
		amountPlanned.setCurrencyCode(currency);
		amountPlanned.setCentAmount(Long.parseLong(cartJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(ORDER_TOTAL)
				.getAsString()));
		payment.setAmountPlanned(amountPlanned);
		Payment.PaymentMethodInfo paymentMethodInfo = new Payment.PaymentMethodInfo();
		paymentMethodInfo.setMethod("CREDIT_CARD");
		paymentMethodInfo.setPaymentInterface("ADYEN");
		Payment.PaymentMethodInfo.Name name = new Payment.PaymentMethodInfo.Name();
		name.setEn("Credit Card");
		paymentMethodInfo.setName(name);
		payment.setPaymentMethodInfo(paymentMethodInfo);
		List<Payment.Transaction> transactions = new ArrayList<>();
		Payment.Transaction transaction = new Payment.Transaction();
		Payment.Transaction.Amount amount = new Payment.Transaction.Amount();
		amount.setCentAmount(Long.parseLong((String)cartJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(ORDER_TOTAL)
				.getAsString()));
		amount.setCurrencyCode(currency);
		transaction.setAmount(amount);
		transaction.setType("Charge");
		transaction.setState(PENDING);
		transactions.add(transaction);
		payment.setTransactions(transactions);
		return payment;		
	}


}
