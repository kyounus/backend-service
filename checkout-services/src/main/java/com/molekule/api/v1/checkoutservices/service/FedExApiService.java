package com.molekule.api.v1.checkoutservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ADDITIONAL_FREIGHT_INFOS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ATTRIBUTES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.B2B;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CA;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CENT_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.D2C;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DELIVERY_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMPTY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FEDEX_GROUND;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FORK_LIFT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FREIGHT_DELIVERY_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.GROUND;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LINE_ITEMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LOADING_DOCK;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.POSTAL_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SUBSCRIPTION_ENABLED;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_NET_CHARGE_WITH_DUTIES_AND_TAXES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VARIANT;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.checkoutservices.util.FedExResponse;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable;
import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable.ShippingTableValue;
import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable.ShippingTableValue.SourceAddress;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.SubscriptionShippingMethodsRequestBean;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostCalculationService;
import com.molekule.api.v1.commonframework.service.cart.ParcelHandlingCostCalculationServieImpl;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import reactor.core.publisher.Mono;
/**
 * The class FedExApiService is used to make fedEx api related calls.
 *
 */
@Service("fedExApiService")
public class FedExApiService {

	public static final String XML_STREET_LINES = "                        <StreetLines>";
	
	public static final String XML_ACCOUNT_NUMBER = "</AccountNumber>\r\n";
	
	public static final String XML_CITY = "</City>\r\n";
	
	public static final String XML_COUNTRY_CODE = "</CountryCode>\r\n";
	
	public static final String XML_POSTAL_CODE = "</PostalCode>\r\n";
	
	public static final String XML_STATE_OR_PROVINCE_CODE = "</StateOrProvinceCode>\r\n";
	
	public static final String XML_STREET_LINES_WITH_NEW_LINE = "</StreetLines>\r\n";
	
	public static final String XML_UNITS = "</Units>\r\n";
	
	public static final String XML_VALUE_WITH_NEW_LINE = "</Value>\r\n";	
	
	@Autowired
	CTEnvProperties ctEnvProperties;
	
	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;
	
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;
	
	@Autowired
	@Qualifier("auroraHelperService")
	AuroraHelperService auroraHelperService;
	
	@Autowired
	@Qualifier("cartHelperService")
	CartHelperService cartHelperService;
	
	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;
	
	@Value("#{${shipping.methods.map}}") 
	private Map<String,String> shippingMethodDisplayCodes;
	
	@Autowired
	HandlingCostCalculationService handlingCostCalculationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FedExApiService.class);
	
	/**
	 * prepareFedExApi method is used to get data to call FedExApi.
	 * 
	 * @param cartId 
	 * @param country 
	 * @return Map<String, String>.
	 */
	public ResponseEntity<Object> prepareFedExApi(String cartId, String country) {
		List<FedExResponse> fedExApiResponseList = new ArrayList<>();
		String token = ctServerHelperService.getStringAccessToken();
		if("GB".equals(country) || "EU".equals(country)) {
			String shippingMethodsUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/shipping-methods?where=name=\"Standard\"";
			String response = netConnectionHelper.sendGetWithoutBody(token, shippingMethodsUrl);
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
			JsonArray resultsArray = jsonObject.get(RESULTS).getAsJsonArray();
			if(resultsArray.size() <= 0) {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			JsonArray zoneRatesArray = resultsArray.get(0).getAsJsonObject().get("zoneRates").getAsJsonArray();
			for(int i=0;i<zoneRatesArray.size();i++) {
				JsonObject zoneRateObject = zoneRatesArray.get(i).getAsJsonObject();
				JsonObject priceObject = zoneRateObject.get("shippingRates").getAsJsonArray().get(0).getAsJsonObject().get(PRICE).getAsJsonObject();
				String currency = priceObject.get("currencyCode").getAsString();
				if("GB".equals(country) && "GBP".equals(currency)) {
					getCTshippingMethod(priceObject, fedExApiResponseList);
				} else if ("EU".equals(country) && "EUR".equals(currency)) {
					getCTshippingMethod(priceObject, fedExApiResponseList);
				}
				
			}
			return new ResponseEntity<>(fedExApiResponseList, HttpStatus.OK);
		}
		Integer packageCount = 0;
		StringBuilder freightLineItems = new StringBuilder();
		StringBuilder freightRequestedPackageLineItems = new StringBuilder();
		StringBuilder parcelRequestedPackageLineItems = new StringBuilder();
		Boolean showShipOvernight = true;

		String cartReponse = cartHelperService.getExpandCartDataById(cartId);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartReponse);
		String channel = getCustomerChannel(jsonObject);
		ShippingTable shippingTable = getShippingTable(channel);
		ShippingTableValue value = shippingTable.getValue();
		validateSourceAddress(value);
		BigDecimal orderTotal = new BigDecimal(0);
		
		JsonArray lineItemsArray = jsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		JsonObject shippingAddress = jsonObject.getAsJsonObject().get(SHIPPING_ADDRESS).getAsJsonObject();
		if(CA.equals(shippingAddress.get(COUNTRY).getAsString())) {
			List<FedExResponse> canadaResponseList = new ArrayList<>();
			FedExResponse fedExResponse = new FedExResponse();
			fedExResponse.setCost("0");
			fedExResponse.setDisplayName(GROUND);
			fedExResponse.setDuration("3-7");
			fedExResponse.setServiceType(FEDEX_GROUND);
			String newServiceTypeName = shippingMethodDisplayCodes.get(FEDEX_GROUND);
			fedExResponse.setServiceName(newServiceTypeName);
			canadaResponseList.add(fedExResponse);
			return new ResponseEntity<>(canadaResponseList, HttpStatus.OK);
		}
		for (int i = 0; i < lineItemsArray.size(); i++) {
			Boolean isSubscriptionEnabled = false;
			if (lineItemsArray.get(i).getAsJsonObject().has(CUSTOM)) {
				JsonObject customFields = lineItemsArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject()
						.get(FIELDS).getAsJsonObject();
				if(customFields.has(SUBSCRIPTION_ENABLED)) {
					isSubscriptionEnabled = customFields.get(SUBSCRIPTION_ENABLED).getAsBoolean();
				}
			}
			JsonObject cartCustomFields = jsonObject.get(CUSTOM).getAsJsonObject()
					.get(FIELDS).getAsJsonObject();
			if(cartCustomFields.has("orderTotal")) {
				Long orderTotalLong = cartCustomFields.get("orderTotal").getAsLong();
				orderTotal = new BigDecimal(orderTotalLong).divide(new BigDecimal(100));
			}
			packageCount = packageCount + lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
			showShipOvernight = setQuantityValue(parcelRequestedPackageLineItems, showShipOvernight, lineItemsArray, i,
					isSubscriptionEnabled);
		}
		if(B2B.equals(channel)) {
			Map<String, String> map = cartHelperService.getDeliveryType(lineItemsArray, value);
			if (map == null || map.get(DELIVERY_TYPE) == null) {
				return new ResponseEntity<>("There is no deliveryType FREIGTH or PARCEL", HttpStatus.BAD_REQUEST);
			}
			String deliveryType = map.get(DELIVERY_TYPE);
			String palletCount = map.get("palletCount");
			String rxPalletCount = map.get("rxPalletCount");
			if (FREIGHT_DELIVERY_TYPE.equals(deliveryType)) {
				String cartShippingAddressId = shippingAddress.get("id").getAsString();
				String specialServiceTag = getSpecialServiceTag(jsonObject, cartShippingAddressId);
				Integer integerPalletCount = Integer.parseInt(palletCount);
				Integer integerRxPalletCount = Integer.parseInt(rxPalletCount);
				Integer totalPalletCount = integerPalletCount + integerRxPalletCount;
				for (int i = 0; i < totalPalletCount; i++) {
					freightLineItems.append(getDynamicLineItemsTag(value, i));
					freightRequestedPackageLineItems.append(getDynamicRequestedPackageLineItemsTag(value, i));
				}
				String freightExRequest = getFreightFedExAPiRequest(value, freightLineItems.toString(),
						freightRequestedPackageLineItems.toString(), shippingAddress, totalPalletCount, specialServiceTag);
				ResponseEntity<Object> freightResponse = getFreightDetails(freightExRequest, value);
				if (freightResponse.getStatusCode().is2xxSuccessful()) {
					List<FedExResponse> freightResponseList = (List<FedExResponse>) freightResponse.getBody();
					fedExApiResponseList.addAll(freightResponseList);
				} else {
					return freightResponse;
				}
			}
			if (!Boolean.valueOf(map.get("rxProduct"))) {
				String parcelRequest = getParcelFedExAPiRequest(value, parcelRequestedPackageLineItems.toString(),
						shippingAddress, packageCount, channel, false);
				ResponseEntity<Object> parcelResponse = getParcelDetails(parcelRequest, value, showShipOvernight, 
						0L, false, channel, orderTotal);
				if (parcelResponse.getStatusCode().is2xxSuccessful()) {
					List<FedExResponse> parcelResponseList = (List<FedExResponse>) parcelResponse.getBody();
					fedExApiResponseList.addAll(parcelResponseList);
				} else {
					return parcelResponse;
				}
			}
		}
		if (D2C.equals(channel)) {
			Long calculatedHandlingCharge = getHandlingCost(lineItemsArray, shippingTable);
			String parcelRequest = getParcelFedExAPiRequest(value, parcelRequestedPackageLineItems.toString(),
					shippingAddress, packageCount, channel, false);
			ResponseEntity<Object> parcelResponse = getParcelDetails(parcelRequest, value, showShipOvernight, 
					calculatedHandlingCharge, false, channel, orderTotal);
			if (parcelResponse.getStatusCode().is2xxSuccessful()) {
				List<FedExResponse> parcelResponseList = (List<FedExResponse>) parcelResponse.getBody();
				fedExApiResponseList.addAll(parcelResponseList);
				fedExApiResponseList.sort(Comparator.comparingDouble(data -> Double.parseDouble(data.getCost())));
			} else {
				return parcelResponse;
			}
		}
		return new ResponseEntity<>(fedExApiResponseList, HttpStatus.OK);

	}

	private void getCTshippingMethod(JsonObject priceObject, List<FedExResponse> fedExApiResponseList) {
		FedExResponse fedExResponse = new FedExResponse();
		BigDecimal centAmount = new BigDecimal(0);
		Long fractionDigits = 0L;
		centAmount = priceObject.get(CENT_AMOUNT).getAsBigDecimal();
		fractionDigits = priceObject.get("fractionDigits").getAsLong();
		while(fractionDigits != 0){
			centAmount = centAmount.divide(new BigDecimal(10));
			fractionDigits--;
		}
		fedExResponse.setCost(centAmount.toString());
		fedExResponse.setDisplayName("Standard");
		fedExResponse.setServiceName("Standard");
		fedExResponse.setServiceType("Standard");
		fedExResponse.setDuration("3-7");
		fedExApiResponseList.add(fedExResponse);
		
	}

	private String getCustomerChannel(JsonObject jsonObject) {
		if(jsonObject.has("customerId")) {
			String customerId = jsonObject.get("customerId").getAsString();
			String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
					+ MolekuleConstant.SLASH_CUSTOMERS_SLASH + customerId;
			String token = ctServerHelperService.getStringAccessToken();
			String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
			JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			JsonObject fieldsJsonObject = customerObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			return fieldsJsonObject.get(CHANNEL).getAsString();
		} else {
			return "D2C";
		}
		
	}

	private Long getHandlingCost(JsonArray lineItemsArray, ShippingTable shippingTable) {
		int deviceCount = 0;
		int filterCount = 0;
		int quantity;
		for(int i =0;i<lineItemsArray.size();i++) {
			String key = lineItemsArray.get(i).getAsJsonObject().get(PRODUCT_TYPE).getAsJsonObject().get("obj").getAsJsonObject().get("key").getAsString();
			if(key.equals("purifier")) {
				quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
				deviceCount = deviceCount + quantity;
			} else if(key.equals("filter")) {
				quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
				filterCount = filterCount + quantity;
			}
		}
		handlingCostCalculationService = new ParcelHandlingCostCalculationServieImpl();
		JsonObject shippingTableJsonObject = MolekuleUtility.parseJsonObject(new Gson().toJson(shippingTable));
		String calculatedHandlingCharge = handlingCostCalculationService.calculateHandlingCharge(shippingTableJsonObject, deviceCount, filterCount);
		return Long.parseLong(calculatedHandlingCharge);
	}

	private ShippingTable getShippingTable(String channel) {
		ShippingTable shippingTable;
		if(B2B.equals(channel)) {
			shippingTable = auroraHelperService.getShippingTables(ctEnvProperties.getShippingTableContainer(),
					ctEnvProperties.getShippingTableKey());			
		} else {
			shippingTable = auroraHelperService.getShippingTables(ctEnvProperties.getShippingTableContainer(),
					ctEnvProperties.getD2cShippingTableKey());
		}
		return shippingTable;
	}

	private void validateSourceAddress(ShippingTableValue value) {
		if (value.getSourceAddress().getCountry() == null) {
			value.getSourceAddress().setCountry("US");
		}
		if(null != value.getSourceAddress2() && null == value.getSourceAddress2().getCountry()) {
			value.getSourceAddress2().setCountry("US");
		}
	}

	private Boolean setQuantityValue(StringBuilder parcelRequestedPackageLineItems, Boolean showShipOvernight,
			JsonArray lineItemsArray, int i, Boolean isSubscriptionEnabled) {
		Integer quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
		JsonArray attributes = lineItemsArray.get(i).getAsJsonObject().get(VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		String productTypeKey = lineItemsArray.get(i).getAsJsonObject().get("productType").getAsJsonObject().get("obj").getAsJsonObject().get("key").getAsString();
		if(!(isSubscriptionEnabled || "warranty".equals(productTypeKey))) {
			parcelRequestedPackageLineItems.append(getDynamicRequestedPackageLineItemsTag(attributes, quantity, i));
		}
		showShipOvernight = validateShowShipOverNightValue(showShipOvernight, quantity, attributes);
		return showShipOvernight;
	}

	private Boolean validateShowShipOverNightValue(Boolean showShipOvernight, Integer quantity, JsonArray attributes) {
		for(int j = 0;j<attributes.size();j++) {
			if(attributes.get(j) != null &&
					"lowStockThreshold".equals(attributes.get(j).getAsJsonObject().get("name").getAsString())) {
				Integer lowStockThreshold = attributes.get(j).getAsJsonObject().get(VALUE).getAsInt();
				if(quantity > lowStockThreshold) {
					showShipOvernight = false;
					break;
				}
			}
		}
		return showShipOvernight;
	}

	/**
	 * getSpecialServiceTag method is used to get special service tag by condition check on additional freight info.
	 * 
	 * @param jsonObject
	 * @param cartShippingAddressId 
	 * @return String
	 */
	private String getSpecialServiceTag(JsonObject jsonObject, String cartShippingAddressId) {
		String specialServiceTag = " ";
		if(jsonObject.getAsJsonObject().get(CUSTOMER_ID) != null ) {
			String customerId = jsonObject.getAsJsonObject().get(CUSTOMER_ID).getAsString();
			String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
					.append("/custom-objects?where=value(employeeIds in (\"").append(customerId).append("\") )").toString();
			String response = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(), url);
			JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(response);
			if(accountJsonObject.get(RESULTS) != null) {
				specialServiceTag = getSpecialServiceTag(cartShippingAddressId, specialServiceTag, accountJsonObject);
			}
			
		}
		return specialServiceTag;
	}

	private String getSpecialServiceTag(String cartShippingAddressId, String specialServiceTag,
			JsonObject accountJsonObject) {
		JsonObject value = accountJsonObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject().get(VALUE).getAsJsonObject();
		if(value.get(ADDITIONAL_FREIGHT_INFOS) != null ) {
			JsonArray additionalFreightInfos = value.get(ADDITIONAL_FREIGHT_INFOS).getAsJsonArray();
			for(int i = 0; i < additionalFreightInfos.size(); i++) {
				if(cartShippingAddressId.equals(additionalFreightInfos.get(i).getAsJsonObject().get("shippingAddressId").getAsString())) {
					JsonObject freightInfoJsonObject = value.get(ADDITIONAL_FREIGHT_INFOS).getAsJsonArray().get(0).getAsJsonObject();
					Boolean dock = freightInfoJsonObject.get(LOADING_DOCK).getAsBoolean();
					Boolean forkLift = freightInfoJsonObject.get(FORK_LIFT).getAsBoolean();
					if(dock || forkLift) {
						specialServiceTag = "<SpecialServicesRequested><SpecialServiceTypes>LIFTGATE_DELIVERY</SpecialServiceTypes></SpecialServicesRequested>";
					}
				}
			}
		}
		return specialServiceTag;
	}

	/**
	 * getDynamicLineItemsTag method is used to get line items.
	 * 
	 * @param value
	 * @param i
	 * @return String
	 */
	private String getDynamicLineItemsTag(ShippingTableValue value, int i) {
		return "                    <LineItems>\r\n"
				+ "                        <Id>"+i+"</Id>\r\n"
				+ "                        <FreightClass>CLASS_"+value.getFreightClass()+"</FreightClass>\r\n"
				+ "                        <Packaging>PALLET</Packaging>\r\n"
				+ "                        <Description>PalletNumber: "+i+"</Description>\r\n"
				+ "                        <Weight>\r\n"
				+ "                            <Units>"+value.getPallet().getWeight().getUnit()+XML_UNITS
				+ "                            <Value>"+value.getPallet().getWeight().getValue()+XML_VALUE_WITH_NEW_LINE
				+ "                        </Weight>\r\n"
				+ "                    </LineItems>\r\n";
	}
	
	/**
	 * getDynamicRequestedPackageLineItemsTag method is used to get requested package line items for freight based on number of line items.
	 * 
	 * @param value
	 * @param i
	 * @return String
	 */
	private String getDynamicRequestedPackageLineItemsTag(ShippingTableValue value, int i) {
		return "                <RequestedPackageLineItems>\r\n"
				+ "                    <SequenceNumber>"+(i+1)+"</SequenceNumber>\r\n"
				+ "                    <Weight>\r\n"
				+ "                        <Units>"+value.getPallet().getWeight().getUnit()+XML_UNITS
				+ "                        <Value>"+value.getPallet().getWeight().getValue()+XML_VALUE_WITH_NEW_LINE
				+ "                    </Weight>\r\n"
				+ "                    <PhysicalPackaging>PALLET</PhysicalPackaging>\r\n"
				+ "                    <AssociatedFreightLineItems>\r\n"
				+ "                        <Id>"+i+"</Id>\r\n"
				+ "                    </AssociatedFreightLineItems>\r\n"
				+ "                </RequestedPackageLineItems>\r\n";
	}

	/**
	 * getDynamicRequestedPackageLineItemsTag method is used to get requested package line items for parcel based on line items.
	 * 
	 * @param attributes
	 * @param quantity
	 * @param i 
	 * @return String
	 */
	private String getDynamicRequestedPackageLineItemsTag(JsonArray attributes, Integer quantity, int i) {
		String weight = "0";
		String length = "0";
		String width = "0";
		String height = "0";
		for(int j = 0;j<attributes.size();j++) {
			if(attributes.get(j) != null &&
					"weight".equals(attributes.get(j).getAsJsonObject().get("name").getAsString())) {
				weight = attributes.get(j).getAsJsonObject().get(VALUE).getAsString();
			}
			if(attributes.get(j) != null &&
					"length".equals(attributes.get(j).getAsJsonObject().get("name").getAsString())) {
				length = attributes.get(j).getAsJsonObject().get(VALUE).getAsString();
				Double doubleLength = Double.valueOf(length);
				Double ceil = Math.ceil(doubleLength);
				length = String.valueOf(ceil.intValue());
			}
			if(attributes.get(j) != null &&
					"width".equals(attributes.get(j).getAsJsonObject().get("name").getAsString())) {
				width = attributes.get(j).getAsJsonObject().get(VALUE).getAsString();
				Double doubleWidth = Double.valueOf(width);
				Double ceil = Math.ceil(doubleWidth);
				width = String.valueOf(ceil.intValue());
			}
			if(attributes.get(j) != null &&
					"height".equals(attributes.get(j).getAsJsonObject().get("name").getAsString())) {
				height = attributes.get(j).getAsJsonObject().get(VALUE).getAsString();
				Double doubleHeight = Double.valueOf(height);
				Double ceil = Math.ceil(doubleHeight);
				height = String.valueOf(ceil.intValue());
			}
        }
		
		return "                <RequestedPackageLineItems>\r\n"
				+ "                    <SequenceNumber>"+(i+1)+"</SequenceNumber>\r\n"
				+ "                    <GroupPackageCount>"+quantity+"</GroupPackageCount>\r\n"
				+ "                    <Weight>\r\n"
				+ "                        <Units>LB</Units>\r\n"
				+ "                        <Value>"+weight+XML_VALUE_WITH_NEW_LINE
				+ "                    </Weight>\r\n"
				+ "                    <Dimensions>\r\n"
				+ "                        <Length>"+length+"</Length>\r\n"
				+ "                        <Width>"+width+"</Width>\r\n"
				+ "                        <Height>"+height+"</Height>\r\n"
				+ "                        <Units>IN</Units>\r\n"
				+ "                    </Dimensions>\r\n"
				+ "                </RequestedPackageLineItems>\r\n";
	}
	
	/**
	 * getFreightFedExAPiRequest method is used to make fedEx Api request for freight.
	 * 
	 * @param shippingTable
	 * @param freightLineItems
	 * @param freightRequestedPackageLineItems 
	 * @param shippingAddress 
	 * @param totalPalletCount 
	 * @param specialServiceTag 
	 * @return String
	 */
	public String getFreightFedExAPiRequest(ShippingTableValue value, String freightLineItems, 
			String freightRequestedPackageLineItems, JsonObject shippingAddress, Integer totalPalletCount, String specialServiceTag) {
		SourceAddress sourceAddress = value.getSourceAddress();
		String serviceType = "FEDEX_FREIGHT_ECONOMY";
		String length = value.getPallet().getLength().getValue();
		String width = value.getPallet().getWidth().getValue();
		String height = value.getPallet().getHeight().getValue();
		
		Double doubleLength = Double.valueOf(length);
		Double ceil = Math.ceil(doubleLength);
		length = String.valueOf(ceil.intValue());
		
		Double doubleWidth = Double.valueOf(width);
		ceil = Math.ceil(doubleWidth);
		width = String.valueOf(ceil.intValue());
		
		Double doubleHeight = Double.valueOf(height);
		ceil = Math.ceil(doubleHeight);
		height = String.valueOf(ceil.intValue());
		
		return "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n"
				+ "    <SOAP-ENV:Body>\r\n"
				+ "        <RateRequest  xmlns=\"http://fedex.com/ws/rate/v28\">\r\n"
				+ getWebAuthenticationDetail()
				+ getClientDetail()
				+ "            <TransactionDetail>\r\n"
				+ "                <CustomerTransactionId>CustTranID: FreightRate</CustomerTransactionId>\r\n"
				+ "            </TransactionDetail>\r\n"
				+ getVersion()
				+ "            <RequestedShipment>\r\n"
				+ getShipmentTimestamp()
				+ "                <ServiceType>"+serviceType+"</ServiceType>\r\n"
				+ getShipperAddresss(sourceAddress)
				+ getRecipientAddresss(shippingAddress)
				+ "                <ShippingChargesPayment>\r\n"
				+ "					   <PaymentType>SENDER</PaymentType>"
				+ "                    <Payor>\r\n"
				+ "                        <ResponsibleParty>\r\n"
				+ "                            <AccountNumber>"+sourceAddress.getAlomAccount()+XML_ACCOUNT_NUMBER
				+ getShippingChargesPayment(sourceAddress)
				+ specialServiceTag
				+ getFreightShipmentDetail(sourceAddress)
				+ "                    <Role>SHIPPER</Role>\r\n"
				+ "                    <CollectTermsType>STANDARD</CollectTermsType>\r\n"
				+ "                    <TotalHandlingUnits>"+totalPalletCount+"</TotalHandlingUnits>\r\n"
				+ "                    <ShipmentDimensions>\r\n"
				+ "                        <Length>"+length+"</Length>\r\n"
				+ "                        <Width>"+width+"</Width>\r\n"
				+ "                        <Height>"+height+"</Height>\r\n"
				+ "                        <Units>"+value.getPallet().getLength().getUnit()+XML_UNITS
				+ "                    </ShipmentDimensions>"
				+ freightLineItems
				+ "                </FreightShipmentDetail>\r\n"
				+ "                <LabelSpecification>\r\n"
				+ "                    <ImageType>PNG</ImageType>\r\n"
				+ "                    <LabelStockType>PAPER_4X6</LabelStockType>\r\n"
				+ "                    <LabelPrintingOrientation>BOTTOM_EDGE_OF_TEXT_FIRST</LabelPrintingOrientation>\r\n"
				+ "                </LabelSpecification>\r\n"
				+ freightRequestedPackageLineItems
				+ "            </RequestedShipment>\r\n"
				+ "        </RateRequest>\r\n"
				+ "    </SOAP-ENV:Body>\r\n"
				+ "</SOAP-ENV:Envelope>";
	}
	
	/**
	 * getParcelFedExAPiRequest method is used to make fedEx Api request for parcel.
	 * 
	 * @param shippingTable
	 * @param parcelRequestedPackageLineItems
	 * @param packageCount 
	 * @param subscriptionCart 
	 * @param accountObjectContainer 
	 * @return String
	 */
	public String getParcelFedExAPiRequest(ShippingTableValue value, String parcelRequestedPackageLineItems, JsonObject shippingAddress, 
			Integer packageCount, String channel, boolean subscriptionCart) {
		SourceAddress sourceAddress;
		if(B2B.equals(channel)) {
			sourceAddress = value.getSourceAddress();
		} else {
			List<String> zone1StatesList = Arrays.asList("AK", "AZ", "CA", "CO", "HI", "ID", "MT", "NM", "NV", "OR",
					"UT", "WA", "WY");
			if (zone1StatesList.contains(shippingAddress.get("state").getAsString())) {
				sourceAddress = value.getSourceAddress();
			} else {
				sourceAddress = value.getSourceAddress2();
			}
		}

		return "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n"
				+ "    <SOAP-ENV:Body>\r\n"
				+ "        <RateRequest  xmlns=\"http://fedex.com/ws/rate/v28\">\r\n"
				+ getWebAuthenticationDetail()
				+ getClientDetail()
				+ "            <TransactionDetail>\r\n"
				+ "                <CustomerTransactionId>RateRequest_v28</CustomerTransactionId>\r\n"
				+ "            </TransactionDetail>\r\n"
				+ getVersion()
				+ (!subscriptionCart ? "<ReturnTransitAndCommit>true</ReturnTransitAndCommit>" : "")
				+ "            <RequestedShipment>\r\n"
				+ getShipmentTimestamp()
				+ (subscriptionCart ? "<ServiceType>SMART_POST</ServiceType>" : "")
				+ "<PackagingType>YOUR_PACKAGING</PackagingType>"
				+ getShipperAddresss(sourceAddress)
				+ getRecipientAddresss(shippingAddress)
				+ "                <ShippingChargesPayment>\r\n"
				+ "					   <PaymentType>SENDER</PaymentType>"
				+ "                    <Payor>\r\n"
				+ "                        <ResponsibleParty>\r\n"
				+ "                            <AccountNumber>"+ctEnvProperties.getFedExClientAccountNumber()+XML_ACCOUNT_NUMBER
				+ getShippingChargesPayment(sourceAddress)
				+ (subscriptionCart ? getSmartPostRequest() : "")
				+ "                <RateRequestTypes>NONE</RateRequestTypes>\r\n"
				+ "                <PackageCount>"+packageCount+"</PackageCount>\r\n"
				+ parcelRequestedPackageLineItems
				+ "            </RequestedShipment>\r\n"
				+ "        </RateRequest>\r\n"
				+ "    </SOAP-ENV:Body>\r\n"
				+ "</SOAP-ENV:Envelope>";
	}
	
	/**
	 * getWebAuthenticationDetail method is used to get web authentication details.
	 * 
	 * @return String
	 */
	private String getWebAuthenticationDetail() {
		return "            <WebAuthenticationDetail>\r\n"
				+ "                <UserCredential>\r\n"
				+ "                    <Key>"+ctEnvProperties.getFedExUser()+"</Key>\r\n"
				+ "                    <Password>"+ctEnvProperties.getFedExPassword()+"</Password>\r\n"
				+ "                </UserCredential>\r\n"
				+ "            </WebAuthenticationDetail>\r\n";
	}
	
	/**
	 * getClientDetail method is used to get the client details.
	 * 
	 * @return String
	 */
	private String getClientDetail() {
		return "            <ClientDetail>\r\n"
				+ "                <AccountNumber>"+ctEnvProperties.getFedExClientAccountNumber()+XML_ACCOUNT_NUMBER
				+ "                <MeterNumber>"+ctEnvProperties.getFedExClientMeterNumber()+"</MeterNumber>\r\n"
				+ "            </ClientDetail>\r\n";
	}
	
	/**
	 * getVersion method is used to get the version.
	 * 
	 * @return String
	 */
	private String getVersion() {
		return "            <Version>\r\n"
				+ "                <ServiceId>crs</ServiceId>\r\n"
				+ "                <Major>28</Major>\r\n"
				+ "                <Intermediate>0</Intermediate>\r\n"
				+ "                <Minor>0</Minor>\r\n"
				+ "            </Version>\r\n";
	}
	
	/**
	 * getShipmentTimestamp method is used to get the shipment timestamp.
	 * 
	 * @return String
	 */
	private String getShipmentTimestamp() {
		return "                <ShipTimestamp>"+LocalDateTime.now()+"</ShipTimestamp>\r\n";
	}
	
	/**
	 * getShipperAddresss method is used to get shipper address
	 * 
	 * @param sourceAddress
	 * @return String
	 */
	private String getShipperAddresss(SourceAddress sourceAddress) {
		return "                <Shipper>\r\n"
				+ "                    <Address>\r\n"
				+ XML_STREET_LINES+sourceAddress.getStreetAddress1()+XML_STREET_LINES_WITH_NEW_LINE
				+ XML_STREET_LINES+sourceAddress.getStreetAddress2()+XML_STREET_LINES_WITH_NEW_LINE
				+ "                        <City>"+sourceAddress.getCity()+XML_CITY
				+ "                        <StateOrProvinceCode>"+sourceAddress.getState()+XML_STATE_OR_PROVINCE_CODE
				+ "                        <PostalCode>"+sourceAddress.getPostalCode()+XML_POSTAL_CODE
				+ "                        <CountryCode>"+sourceAddress.getCountry()+XML_COUNTRY_CODE
				+ "                    </Address>\r\n"
				+ "                </Shipper>\r\n";
	}
	
	/**
	 * getRecipientAddresss method is used to get recipient address.
	 * 
	 * @param shippingAddress
	 * @return String
	 */
	private String getRecipientAddresss(JsonObject shippingAddress) {
		String region;
		String streetName = shippingAddress.get("streetName").getAsString();
		String postalCode = shippingAddress.get(POSTAL_CODE).getAsString();
		String city = shippingAddress.get("city").getAsString();
		if(shippingAddress.get(STATE) != null) {
			region = shippingAddress.get(STATE).getAsString();
		} else {
			region = shippingAddress.get("region").getAsString();
		}
		String country = shippingAddress.get("country").getAsString();
		
		return "                <Recipient>\r\n"
		+ "                    <Address>\r\n"
		+ XML_STREET_LINES+streetName+XML_STREET_LINES_WITH_NEW_LINE
		+ "                        <StreetLines> </StreetLines>\r\n"
		+ "                        <City>"+city+XML_CITY
		+ "                        <StateOrProvinceCode>"+region+XML_STATE_OR_PROVINCE_CODE
		+ "                        <PostalCode>"+postalCode+XML_POSTAL_CODE
		+ "                        <CountryCode>"+country+XML_COUNTRY_CODE
		+ "                    </Address>\r\n"
		+ "                </Recipient>\r\n";
	}
	
	/**
	 * getShippingChargesPayment method is used to get shipping charges payment.
	 * 
	 * @param sourceAddress
	 * @return String
	 */
	private String getShippingChargesPayment(SourceAddress sourceAddress) {
		return "                            <Address>\r\n"
				+ "                        	   		<StreetLines>"+sourceAddress.getStreetAddress1()+XML_STREET_LINES_WITH_NEW_LINE
				+ "                        			<StreetLines>"+sourceAddress.getStreetAddress2()+XML_STREET_LINES_WITH_NEW_LINE
				+ "                        			<City>"+sourceAddress.getCity()+XML_CITY
				+ "                        			<StateOrProvinceCode>"+sourceAddress.getState()+XML_STATE_OR_PROVINCE_CODE
				+ "                        			<PostalCode>"+sourceAddress.getPostalCode()+XML_POSTAL_CODE
				+ "                        			<CountryCode>"+sourceAddress.getCountry()+XML_COUNTRY_CODE
				+ "                            </Address>\r\n"
				+ "                        </ResponsibleParty>\r\n"
				+ "                    </Payor>\r\n"
				+ "                </ShippingChargesPayment>\r\n";
	}	
	
	/**
	 * getFreightShipmentDetail method is used to get freight shipment detail.
	 * 
	 * @param sourceAddress
	 * @return String
	 */
	private String getFreightShipmentDetail(SourceAddress sourceAddress) {
		return "                <FreightShipmentDetail>\r\n"
				+ "                    <FedExFreightAccountNumber>"+sourceAddress.getAlomAccount()+"</FedExFreightAccountNumber>\r\n"
				+ "                    <FedExFreightBillingContactAndAddress>\r\n"
				+ "                        <Address>\r\n"
				+ "                        	   	<StreetLines>"+sourceAddress.getStreetAddress1()+XML_STREET_LINES_WITH_NEW_LINE
				+ "                        		<StreetLines>"+sourceAddress.getStreetAddress2()+XML_STREET_LINES_WITH_NEW_LINE
				+ "                        		<City>"+sourceAddress.getCity()+XML_CITY
				+ "                        		<StateOrProvinceCode>"+sourceAddress.getState()+XML_STATE_OR_PROVINCE_CODE
				+ "                        		<PostalCode>"+sourceAddress.getPostalCode()+XML_POSTAL_CODE
				+ "                        		<CountryCode>US</CountryCode>\r\n"
				+ "                        </Address>\r\n"
				+ "                    </FedExFreightBillingContactAndAddress>\r\n";
	}
	
	public String getSmartPostRequest() {
		return "                <SmartPostDetail>\r\n"
				+ "                    <Indicia>PARCEL_SELECT</Indicia>\r\n"
				+ "                    <AncillaryEndorsement>CARRIER_LEAVE_IF_NO_RESPONSE</AncillaryEndorsement>\r\n"
				+ "                </SmartPostDetail>";
	}
	
	/**
	 * getFreightDetails method is used to get freight total net charge with duties and taxes.
	 * 
	 * @param freightExRequest
	 * @param value 
	 */
	private ResponseEntity<Object> getFreightDetails(String freightExRequest, ShippingTableValue value) {
		List<FedExResponse> fedExResponseList = new ArrayList<>();
		String responseBlock = getFedExApiCall(freightExRequest);
		try {
			Document doc = loadXMLString(responseBlock);
			List<String> highestSeverityList = getTagFromXml(doc, "HighestSeverity");
			if("SUCCESS".equals(highestSeverityList.get(0))) {
				List<String> serviceTypeList = getTagFromXml(doc, "ServiceType");
				List<String> totalNetChargeWithDutiesAndTaxesList = getTagFromXml(doc, TOTAL_NET_CHARGE_WITH_DUTIES_AND_TAXES);
				String totalNetChargeString = totalNetChargeWithDutiesAndTaxesList.get(0);
				BigDecimal totalNetCharge =  addMarkupPercent(totalNetChargeString, value);
				String serviceType = serviceTypeList.get(0);
				FedExResponse fedExResponse = new FedExResponse();
				fedExResponse.setServiceType(serviceType);
				String newServiceTypeName = shippingMethodDisplayCodes.get(serviceType);
				if(newServiceTypeName == null) {
					newServiceTypeName  = serviceType;
				}
				fedExResponse.setServiceName(newServiceTypeName);
				fedExResponse.setCost(totalNetCharge.toString());
				fedExResponse.setDuration("3-7");
				fedExResponse.setDisplayName("Freight Economy");
				fedExResponseList.add(fedExResponse);
			} else {
				List<String> errorMessageList = getTagFromXml(doc, "Message");
				List<String> errorCodes = getTagFromXml(doc, "Code");
				if(!CollectionUtils.isEmpty(errorMessageList) && !CollectionUtils.isEmpty(errorCodes) ) {
					return new ResponseEntity<>(
							ErrorResponse.builder()
							.errorMessage(errorMessageList.get(0))
							.statusCode(Integer.valueOf(errorCodes.get(0)))
							.build(), HttpStatus.BAD_REQUEST);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured getFreightDetails", e);
		}
		return new ResponseEntity<>(fedExResponseList, HttpStatus.OK);
	}
	
	/**
	 * getParcelDetails method is used to get parcel total net charge with duties and taxes.
	 * 
	 * @param request
	 * @param totalNetChargeWithDutiesAndTaxesMap
	 * @param value 
	 * @param showShipOvernight 
	 * @param calculatedHandlingCharge 
	 * @param subscriptionCart 
	 * @param channel 
	 * @param orderTotal 
	 * @return ResponseEntity<Object>
	 */
	private ResponseEntity<Object> getParcelDetails(String request, ShippingTableValue value, Boolean showShipOvernight, Long calculatedHandlingCharge,
			boolean subscriptionCart, String channel, BigDecimal orderTotal) {
		List<FedExResponse> fedExResponseList = new ArrayList<>();
		String responseBlock = getFedExApiCall(request);
		try {
			Document doc = loadXMLString(responseBlock);
			List<String> highestSeverityList = getTagFromXml(doc, "HighestSeverity");
			if ("SUCCESS".equals(highestSeverityList.get(0)) || "NOTE".equals(highestSeverityList.get(0))
					|| "WARNING".equals(highestSeverityList.get(0))) {
				List<String> allServiceTypeList = getTagFromXml(doc, "ServiceType");
				List<String> allDeliveryDatesList = getTagFromXml(doc, "DeliveryTimestamp");
				List<String> distinctServiceTypeList = allServiceTypeList.stream().distinct().collect(Collectors.toList());
				List<String> allTotalNetChargeWithDutiesAndTaxesList = getTagFromXml(doc, TOTAL_NET_CHARGE_WITH_DUTIES_AND_TAXES);
				List<String> distinctTotalNetChargeWithDutiesAndTaxesList = allTotalNetChargeWithDutiesAndTaxesList
						.stream().distinct().collect(Collectors.toList());
				processTaxesList(value, showShipOvernight, fedExResponseList, distinctServiceTypeList,
						distinctTotalNetChargeWithDutiesAndTaxesList, calculatedHandlingCharge, allDeliveryDatesList,
						subscriptionCart, channel, orderTotal);
			} else {
				List<String> errorMessageList = getTagFromXml(doc, "Message");
				List<String> errorCodes = getTagFromXml(doc, "Code");
				if(!CollectionUtils.isEmpty(errorMessageList) && !CollectionUtils.isEmpty(errorCodes) ) {
					return new ResponseEntity<>(
							ErrorResponse.builder()
							.errorMessage(errorMessageList.get(0))
							.statusCode(Integer.valueOf(errorCodes.get(0)))
							.build(), HttpStatus.BAD_REQUEST);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured getParcelDetails", e);
		}
		return new ResponseEntity<>(fedExResponseList, HttpStatus.OK);
	}

	private void processTaxesList(ShippingTableValue value, Boolean showShipOvernight,
			List<FedExResponse> fedExResponseList, List<String> distinctServiceTypeList,
			List<String> distinctTotalNetChargeWithDutiesAndTaxesList, Long calculatedHandlingCharge, 
			List<String> allDeliveryDatesList, boolean subscriptionCart, String channel, BigDecimal orderTotal) {
		boolean freeGround = false;
		boolean freePriorityOvernight = false;
		boolean free2Day = false;
		boolean freeSmartPost = false;
		if(D2C.equals(channel)) {
			String token = ctServerHelperService.getStringAccessToken();
			String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
					.append(ctEnvProperties.getProjectKey())
					.append("/cart-discounts?where=(target(type=\"shipping\") and isActive=true and requiresDiscountCode=false)").toString();
			String cartDiscountsResponse = netConnectionHelper.sendGetWithoutBody(token, url);
			JsonObject cartDiscountObject = MolekuleUtility.parseJsonObject(cartDiscountsResponse);
			JsonArray cartDiscountsResultsArray = cartDiscountObject.get(RESULTS).getAsJsonArray();
			if(cartDiscountsResultsArray.size() != 0) {
				for(int i=0; i<cartDiscountsResultsArray.size();i++) {
					JsonObject cartDiscountsResultsObject = cartDiscountsResultsArray.get(i).getAsJsonObject();
					String cartPredicate = cartDiscountsResultsObject.get("cartPredicate").getAsString();
					if(!cartPredicate.contains("and")) {
						continue;
					}
					String predicate1 = cartPredicate.split("and")[0].trim();
					String predicate2 = cartPredicate.split("and")[1].trim();
					String shippingName = EMPTY;
					String totalPrice = EMPTY; 
					String conditionalSymbol = EMPTY;
					String totalPriceWithCode = EMPTY;
					if(predicate1.contains("shippingMethodName") && predicate2.contains("totalPrice")) {
						shippingName = predicate1.split("\"")[1].trim();
						totalPriceWithCode = predicate2.split("\"")[1].trim();
						totalPrice = totalPriceWithCode.split("\\s")[0].trim();
						conditionalSymbol = predicate2.split("\\s")[1].trim();
					} else if (predicate2.contains("shippingMethodName") && predicate1.contains("totalPrice")) {
						shippingName = predicate2.split("\"")[1].trim();
						totalPriceWithCode = predicate1.split("\"")[1].trim();
						totalPrice = totalPriceWithCode.split("\\s")[0].trim();
						conditionalSymbol = predicate1.split("\\s")[1].trim();
					} else {
						continue;
					}
					BigDecimal totalPriceBigDecimal = new BigDecimal(totalPrice);
					boolean cartTotalCondtion = getCartTotalPredicateCondition(totalPriceBigDecimal.longValue(), conditionalSymbol, orderTotal.longValue());
					if(cartTotalCondtion) {
						if(shippingName.contains("Ground")) {
							freeGround = true;
						} else if(shippingName.contains("Overnight")) {
							freePriorityOvernight = true;
						} else if(shippingName.contains("Day")) {
							free2Day = true;
						} else if(shippingName.contains("Post")) {
							freeSmartPost = true;
						}
					}
				}
			}
		}
		for (int i = 0; i < distinctTotalNetChargeWithDutiesAndTaxesList.size(); i++) {
			FedExResponse fedExResponse = new FedExResponse();
			String totalNetChargeString = distinctTotalNetChargeWithDutiesAndTaxesList.get(i);
			BigDecimal totalNetCharge = addMarkupPercent(totalNetChargeString, value);
			String serviceType = distinctServiceTypeList.get(i);
			String newServiceTypeName = shippingMethodDisplayCodes.get(serviceType);
			if(newServiceTypeName == null) {
				newServiceTypeName  = serviceType;
			}
			if(subscriptionCart) {
				smartPost(fedExResponseList, fedExResponse, totalNetCharge, serviceType,
						newServiceTypeName, calculatedHandlingCharge, "3-7", freeSmartPost);
			} else {
				LocalDateTime currentDate = LocalDateTime.now();
				LocalDateTime deliveryDate = LocalDateTime.parse(allDeliveryDatesList.get(i));
				Duration duration = Duration.between(currentDate, deliveryDate);
				Long days = duration.toDays();
				
				if (showShipOvernight && "PRIORITY_OVERNIGHT".equals(serviceType) ) {
					priotityOvernight(fedExResponseList, fedExResponse, totalNetCharge, serviceType,
							newServiceTypeName, calculatedHandlingCharge, days, freePriorityOvernight);
				}
				if ("FEDEX_2_DAY".equals(serviceType)) {
					fedEx2Day(fedExResponseList, fedExResponse, totalNetCharge, serviceType,
							newServiceTypeName, calculatedHandlingCharge, days, free2Day);
				} else if ("FEDEX_GROUND".equals(serviceType)) {
					if (freeGround) {
						fedExResponse.setCost("0");
					} else if (0L == calculatedHandlingCharge) {
						fedExResponse.setCost(String.valueOf(totalNetCharge));
					} else {
						fedExResponse.setCost(String.valueOf(totalNetCharge.add(new BigDecimal(calculatedHandlingCharge))));
					}
					setFedExResponseValue(fedExResponseList, fedExResponse, serviceType, newServiceTypeName, "3-7", "Ground");
				}				
			}
		}
	}

	private boolean getCartTotalPredicateCondition(Long totalPrice, String conditionalSymbol, Long orderTotal) {
		if(conditionalSymbol.equals(">")) {
			return orderTotal > totalPrice;
		}
		if(conditionalSymbol.equals(">=")) {
			return orderTotal >= totalPrice;
		}
		if(conditionalSymbol.equals("<")) {
			return orderTotal < totalPrice;
		}
		if(conditionalSymbol.equals("<=")) {
			return orderTotal <= totalPrice;
		}
		if(conditionalSymbol.equals("==")) {
			return orderTotal == totalPrice;
		}
		if(conditionalSymbol.equals("!=")) {
			return orderTotal != totalPrice;
		}
		return false;
	}

	private void smartPost(List<FedExResponse> fedExResponseList, FedExResponse fedExResponse,
			BigDecimal totalNetCharge, String serviceType, String newServiceTypeName, Long calculatedHandlingCharge,
			String days, boolean freeSmartPost) {
		if (freeSmartPost) {
			fedExResponse.setCost("0");
		} else if (0L == calculatedHandlingCharge) {
			fedExResponse.setCost(String.valueOf(totalNetCharge));
		} else {
			fedExResponse.setCost(String.valueOf(totalNetCharge.add(new BigDecimal(calculatedHandlingCharge))));
		}
		setFedExResponseValue(fedExResponseList, fedExResponse, serviceType, newServiceTypeName, days, "Smart Post");
	}

	private void fedEx2Day(List<FedExResponse> fedExResponseList, FedExResponse fedExResponse,
			BigDecimal totalNetCharge, String serviceType, String newServiceTypeName, Long calculatedHandlingCharge,
			Long days, boolean free2Day) {
			if ((days <= 2) && free2Day) {
				fedExResponse.setCost("0");
			} else if (0L == calculatedHandlingCharge) {
				fedExResponse.setCost(String.valueOf(totalNetCharge));
			} else {
				fedExResponse.setCost(String.valueOf(totalNetCharge.add(new BigDecimal(calculatedHandlingCharge))));
			}
			setFedExResponseValue(fedExResponseList, fedExResponse, serviceType, newServiceTypeName, "2", "2-Day");
	}

	private void priotityOvernight(List<FedExResponse> fedExResponseList, FedExResponse fedExResponse,
			BigDecimal totalNetCharge, String serviceType, String newServiceTypeName, Long calculatedHandlingCharge,
			Long days, boolean freePriorityOvernight) {
			if ((days <= 1) && freePriorityOvernight) {
				fedExResponse.setCost("0");
			} else if (0L == calculatedHandlingCharge) {
				fedExResponse.setCost(String.valueOf(totalNetCharge));
			} else {
				fedExResponse.setCost(String.valueOf(totalNetCharge.add(new BigDecimal(calculatedHandlingCharge))));
			}
			setFedExResponseValue(fedExResponseList, fedExResponse, serviceType, newServiceTypeName, "1", "Overnight");
	}

	private void setFedExResponseValue(List<FedExResponse> fedExResponseList, FedExResponse fedExResponse,
			String serviceType, String newServiceTypeName, String duration, String displayName) {
		fedExResponse.setDuration(duration);
		fedExResponse.setServiceType(serviceType);
		fedExResponse.setServiceName(newServiceTypeName);
		fedExResponse.setDisplayName(displayName);
		fedExResponseList.add(fedExResponse);
	}

	/**
	 * getFedExApiCall method is used to make a fedEx Api call.
	 * 
	 * @param request
	 * @return String
	 */
	public String getFedExApiCall(String request) {
		ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(262144 * 10)).build();
		WebClient webClient = WebClient.builder().baseUrl(ctEnvProperties.getFedExApiUrl())
				.exchangeStrategies(exchangeStrategies).build();
		Mono<String> response = webClient.post().accept(MediaType.APPLICATION_XML)
				.body(Mono.just(request), String.class).exchangeToMono(clientResponse -> {
					if (clientResponse.statusCode().value() != 200) {
						throw new CustomRunTimeException( "Unable to get Access Token From the CT Server");
					}
					return clientResponse.bodyToMono(String.class);
				});

		return response.block();
	}
	
	/**
	 * loadXMLString method is used to parse string to Document.
	 * 
	 * @param response
	 * @return Document
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws Exception
	 */
	public Document loadXMLString(String response) throws ParserConfigurationException, SAXException, IOException  {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
	
		DocumentBuilder db = dbf.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(response));
		return db.parse(is);
	}

	/**
	 * getTagFromXml method is used to get all values of the mentioned tagName from fedEx Api response.
	 * 
	 * @param xmlDoc
	 * @param tagName
	 * @return List<String>
	 * @throws Exception
	 */
	public List<String> getTagFromXml(Document xmlDoc, String tagName) {
		NodeList nodeList = xmlDoc.getElementsByTagName(tagName);
		List<String> ids = new ArrayList<>(nodeList.getLength());
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node x = nodeList.item(i);
			if(TOTAL_NET_CHARGE_WITH_DUTIES_AND_TAXES.equals(tagName)) {
				ids.add(x.getFirstChild().getNextSibling().getFirstChild().getNodeValue());
			} else {
				ids.add(x.getFirstChild().getNodeValue());
			}
		}
		return ids;
	}
	
	/**
	 * addMarkupPercent method is used to add the markup percentage to the totalNetChargeWithDutiesAndTaxes.
	 * 
	 * @param totalNetChargeString
	 * @param value
	 * @return BigDecimal
	 */
	private BigDecimal addMarkupPercent(String totalNetChargeString, ShippingTableValue value) {
		BigDecimal totalNetChargeBigDecimal = new BigDecimal(totalNetChargeString);
		String markupPercentageString = value.getMarkUpPercentage();
		BigDecimal markupPercentage;
		if(markupPercentageString == null || markupPercentageString.equals("")) {
			markupPercentage = new BigDecimal(0); 
		} else {
			markupPercentage = new BigDecimal(markupPercentageString);
		}
		BigDecimal markupPercentageValue = totalNetChargeBigDecimal.multiply(markupPercentage.divide(BigDecimal.valueOf(100)));
		BigDecimal roundoffMarkUp = markupPercentageValue.setScale(2, RoundingMode.HALF_EVEN);
		return totalNetChargeBigDecimal.add(roundoffMarkUp);
	}

	public ResponseEntity<Object> getSmartPostService(String cartId) {
		List<FedExResponse> fedExApiResponseList = new ArrayList<>();
		Integer packageCount = 0;
		StringBuilder parcelRequestedPackageLineItems = new StringBuilder();
		Boolean showShipOvernight = true;
		BigDecimal orderTotal = new BigDecimal(0);
		
		String cartReponse = cartHelperService.getExpandCartDataById(cartId);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartReponse);
		String channel = getCustomerChannel(jsonObject);
		ShippingTable shippingTable = getShippingTable(channel);
		ShippingTableValue value = shippingTable.getValue();
		validateSourceAddress(value);
		
		JsonArray lineItemsArray = jsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		JsonObject shippingAddress = jsonObject.getAsJsonObject().get(SHIPPING_ADDRESS).getAsJsonObject();
		for (int i = 0; i < lineItemsArray.size(); i++) {
			packageCount = packageCount + lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
			showShipOvernight = setQuantityValue(parcelRequestedPackageLineItems, showShipOvernight, lineItemsArray, i,
					false);
		}
		
		JsonObject cartCustomFields = jsonObject.get(CUSTOM).getAsJsonObject()
				.get(FIELDS).getAsJsonObject();
		if(cartCustomFields.has("orderTotal")) {
			Long orderTotalLong = cartCustomFields.get("orderTotal").getAsLong();
			orderTotal = new BigDecimal(orderTotalLong).divide(new BigDecimal(100));
		}
		
		Long calculatedHandlingCharge = getHandlingCost(lineItemsArray, shippingTable);
		String parcelRequest = getParcelFedExAPiRequest(value, parcelRequestedPackageLineItems.toString(),
				shippingAddress, packageCount, channel, true);
		ResponseEntity<Object> parcelResponse = getParcelDetails(parcelRequest, value, showShipOvernight,
				calculatedHandlingCharge, true, channel, orderTotal);
		if (parcelResponse.getStatusCode().is2xxSuccessful()) {
			List<FedExResponse> parcelResponseList = (List<FedExResponse>) parcelResponse.getBody();
			fedExApiResponseList.addAll(parcelResponseList);
			fedExApiResponseList.sort(Comparator.comparingDouble(data -> Double.parseDouble(data.getCost())));
		} else {
			return parcelResponse;
		}
		return new ResponseEntity<>(fedExApiResponseList, HttpStatus.OK);
	}

	public String getSubscriptionShippingMehodsRequest(SubscriptionShippingMethodsRequestBean subscriptionShippingMethodsRequestBean, String carrierCode) {
		AddressRequestBean shippingAddress = subscriptionShippingMethodsRequestBean.getShippingAddress();
		return "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n"
				+ "    <SOAP-ENV:Body>\r\n"
				+ "        <ServiceAvailabilityRequest xmlns=\"http://fedex.com/ws/vacs/v8\">\r\n"
				+ getWebAuthenticationDetail()
				+ getClientDetail()
				+ "            <TransactionDetail>\r\n"
				+ "                <CustomerTransactionId>ServiceAvailabilityRequest</CustomerTransactionId>\r\n"
				+ "            </TransactionDetail>\r\n"
				+ "            <Version>\r\n"
				+ "                <ServiceId>vacs</ServiceId>\r\n"
				+ "                <Major>8</Major>\r\n"
				+ "                <Intermediate>0</Intermediate>\r\n"
				+ "                <Minor>0</Minor>\r\n"
				+ "            </Version>\r\n"
				+ "            <Origin>\r\n"
				+ "                <PostalCode>"+shippingAddress.getPostalCode()+"</PostalCode>\r\n"
				+ "                <CountryCode>"+shippingAddress.getCountry()+"</CountryCode>\r\n"
				+ "            </Origin>\r\n"
				+ "            <Destination>\r\n"
				+ "                <PostalCode>94103</PostalCode>\r\n"
				+ "                <CountryCode>US</CountryCode>\r\n"
				+ "            </Destination>\r\n"
				+ "            <ShipDate>"+subscriptionShippingMethodsRequestBean.getOrderDate()+"</ShipDate>\r\n"
				+ "            <CarrierCode>"+carrierCode+"</CarrierCode>\r\n"
				+ "        </ServiceAvailabilityRequest>\r\n"
				+ "    </SOAP-ENV:Body>\r\n"
				+ "</SOAP-ENV:Envelope>";
	}
	
	public ResponseEntity<Object> getSubscriptionShippingMethods(SubscriptionShippingMethodsRequestBean subscriptionShippingMethodsRequestBean) {
		List<String> availableServicesList = new ArrayList<>();
		availableServicesList.add("FedEx - Smart Post");
		availableServicesList.add(shippingMethodDisplayCodes.get("FEDEX_GROUND"));
		String request = getSubscriptionShippingMehodsRequest(subscriptionShippingMethodsRequestBean, "FDXE");
		String responseBlock = getFedExApiCall(request);
		try {
			Document doc = loadXMLString(responseBlock);
			List<String> highestSeverityList = getTagFromXml(doc, "HighestSeverity");
			if ("SUCCESS".equals(highestSeverityList.get(0)) || "NOTE".equals(highestSeverityList.get(0))
					|| "WARNING".equals(highestSeverityList.get(0))) {
				List<String> allServiceTypeList = getTagFromXml(doc, "Service");
				List<String> allDeliveryDatesList = getTagFromXml(doc, "DeliveryDate");

				for (int j = 0; j < allDeliveryDatesList.size(); j++) {
					String serviceType = allServiceTypeList.get(j);
					String newServiceTypeName = shippingMethodDisplayCodes.get(serviceType);
					if (newServiceTypeName == null) {
						newServiceTypeName = serviceType;
					}
					LocalDate currentDate = LocalDate.now();
					LocalDate deliveryDate = LocalDate.parse(allDeliveryDatesList.get(j));
					Period duration = Period.between(currentDate, deliveryDate);
					int days = duration.getDays();
					if (days <= 1 && "PRIORITY_OVERNIGHT".equals(serviceType)) {
						availableServicesList.add(newServiceTypeName);
					}
					if (days <= 2 && "FEDEX_2_DAY".equals(serviceType)) {
						availableServicesList.add(newServiceTypeName);
					}
				}
			} else {
				List<String> errorMessageList = getTagFromXml(doc, "Message");
				List<String> errorCodes = getTagFromXml(doc, "Code");
				if(!CollectionUtils.isEmpty(errorMessageList) && !CollectionUtils.isEmpty(errorCodes) ) {
					return new ResponseEntity<>(
							ErrorResponse.builder()
							.errorMessage(errorMessageList.get(0))
							.statusCode(Integer.valueOf(errorCodes.get(0)))
							.build(), HttpStatus.BAD_REQUEST);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured getSubscriptionShippingMethods", e);
		}
	
		return new ResponseEntity<>(availableServicesList, HttpStatus.OK);
	}
}
