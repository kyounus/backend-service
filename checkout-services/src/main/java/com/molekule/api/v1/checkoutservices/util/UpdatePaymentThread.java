package com.molekule.api.v1.checkoutservices.util;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.UpdatePayment;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import lombok.Data;
@Data
public class UpdatePaymentThread implements Runnable {

	private PaymentOrderDTO paymentOrderdto;
	private long currentPaymentVersion;
	private String transactionId;
	private CTEnvProperties ctEnvProperties;
	private NetConnectionHelper netConnectionHelper;
	private String paymentId;
	private String token;
	
	@Override
	public void run() {

		UpdatePayment updatePayment = CheckoutUtility.getUpdatedCTPaymentObj(paymentOrderdto.getPaymentType(),
				"setStatusInterfaceText", currentPaymentVersion, transactionId);
		String updatePaymentUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/payments/" + paymentId;
		netConnectionHelper.sendPostRequest(updatePaymentUrl, token, updatePayment);
	}

	
}
