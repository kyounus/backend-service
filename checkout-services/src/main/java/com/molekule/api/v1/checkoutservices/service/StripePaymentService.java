package com.molekule.api.v1.checkoutservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.JsonObject;
import com.molekule.api.v1.checkoutservices.service.CheckoutService.PaymentIntentOptions;
import com.molekule.api.v1.commonframework.model.checkout.PaymentIntentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("stripePaymentService")
public class StripePaymentService implements PaymentService {

	@Autowired
	@Qualifier("netConnectionHelper")
	private NetConnectionHelper netConnectionHelper;
	@Value("${stripeApiKey}")
	private String stripeAuthKey;
	@Value("${setupPaymentIntentUrl}")
	private String setupPaymentIntentUrl;
	@Value("${paymentIntentHost}")
	private String paymentIntentUrl;

	/**
	 * createPaymentIntent method is to create payment intent,capture the payment or
	 * cancel the payment based on action
	 * 
	 * 
	 * @param paymentmRequestBean,action,paymentIntentId
	 * @return String
	 */
	@Override
	public String createPaymentToken(PaymentIntentRequest paymentmRequestBean, String action, String paymentIntentId) {
		String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
		JsonObject jsonObject = null;
		// create payment intent
		if (PaymentIntentOptions.PRE_AUTH.name().equals(action)) {
			// get customer and payment method by Retrieve setup payment intent by id
			String retrievePaymentIntentBaseUrl = setupPaymentIntentUrl + "/"
					+ paymentmRequestBean.getSetUpPaymentIntentId();
			String setupPaymentIntentResponse = netConnectionHelper.sendGetWithoutBody(authToken,
					retrievePaymentIntentBaseUrl);
			jsonObject = MolekuleUtility.parseJsonObject(setupPaymentIntentResponse);
			if (jsonObject.get("error") != null) {
				return setupPaymentIntentResponse;
			}
			String paymentMethodId = jsonObject.get("payment_method").getAsString();
			String customerId = jsonObject.get(CUSTOMER).getAsString();
			MultiValueMap<String, String> intentFormData = new LinkedMultiValueMap<>();
			float amount = Float.parseFloat(paymentmRequestBean.getAmount());
			long amountVal = Math.round(amount);
			intentFormData.add("amount", String.valueOf(amountVal));
			intentFormData.add("currency", paymentmRequestBean.getCurrency());
			intentFormData.add("capture_method", "manual");
			intentFormData.add("payment_method", paymentMethodId);
			intentFormData.add("confirm", "true");
			intentFormData.add(CUSTOMER, customerId);
			return (String) netConnectionHelper.sendFormUrlEncoded(paymentIntentUrl, authToken, intentFormData);
		}
		if (PaymentIntentOptions.CAPTURE.name().equals(action)) {

			// Capture the payment intent
			String captureIntentUrl = paymentIntentUrl + "/" + paymentIntentId + "/capture";
			MultiValueMap<String, String> captureIntentFormData = new LinkedMultiValueMap<>();
			return (String) netConnectionHelper.sendFormUrlEncoded(captureIntentUrl, authToken, captureIntentFormData);
		}
		if (PaymentIntentOptions.CANCEL.name().equals(action)) {
			// Cancel the payment intent
			String cancelIntentUrl = paymentIntentUrl + "/" + paymentIntentId + "/cancel";
			MultiValueMap<String, String> cancelIntentFormData = new LinkedMultiValueMap<>();
			return (String) netConnectionHelper.sendFormUrlEncoded(cancelIntentUrl, authToken, cancelIntentFormData);
		}

		return null;
	}
	

	
	@Override
	public String authorizePayment(PaymentOrderDTO paymentOrderDTO,String currency) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String capturePayment(PaymentOrderDTO paymentOrderDTO,String authToken,String currency) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String cancelPayment(PaymentOrderDTO paymentOrderDTO,String authToken) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String captureOrCancelPayment(String action, String paymentIntentId) {
		String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
		if (PaymentIntentOptions.CAPTURE.name().equals(action)) {

			// Capture the payment intent
			String captureIntentUrl = paymentIntentUrl + "/" + paymentIntentId + "/capture";
			MultiValueMap<String, String> captureIntentFormData = new LinkedMultiValueMap<>();
			return (String) netConnectionHelper.sendFormUrlEncoded(captureIntentUrl, authToken, captureIntentFormData);
		}
		if (PaymentIntentOptions.CANCEL.name().equals(action)) {
			// Cancel the payment intent
			String cancelIntentUrl = paymentIntentUrl + "/" + paymentIntentId + "/cancel";
			MultiValueMap<String, String> cancelIntentFormData = new LinkedMultiValueMap<>();
			return (String) netConnectionHelper.sendFormUrlEncoded(cancelIntentUrl, authToken, cancelIntentFormData);
		}
		return null;
	}

	

	@Override
	public String createPaymentToken(PaymentRequest paymentRequest,String currency) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String refundPayment(Object refundObject) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
