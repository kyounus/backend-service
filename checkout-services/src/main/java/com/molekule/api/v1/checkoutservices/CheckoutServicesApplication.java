package com.molekule.api.v1.checkoutservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckoutServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckoutServicesApplication.class, args);
	}

}
