package com.molekule.api.v1.orderservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ATTRIBUTES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CENT_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_PRICE_PER_QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ERROR;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.GBR;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.INCLUDED_DISCOUNTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ITEMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_MODIFIED_AT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LINE_ITEMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LINE_ITEM_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDERS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_INFO;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_METHOD_INFO;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RETURN_INFO;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SERIAL_NUMBERS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_COST;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SLASH_CUSTOMERS_SLASH;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SUB_TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_TAX;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VARIANT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.util.StringUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.order.LineItems;
import com.molekule.api.v1.commonframework.dto.order.OrderRefundRequestBean;
import com.molekule.api.v1.commonframework.dto.order.OrderRefundServiceBean;
import com.molekule.api.v1.commonframework.dto.order.OrderResponseServiceBean;
import com.molekule.api.v1.commonframework.dto.order.OrderReturnServiceBean;
import com.molekule.api.v1.commonframework.dto.order.OrderReturnServiceBean.Action;
import com.molekule.api.v1.commonframework.dto.order.RefundCustomObject;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.checkout.RefundDTO;
import com.molekule.api.v1.commonframework.model.customerprofile.BulkUpdateRequestBean;
import com.molekule.api.v1.commonframework.model.order.OrderCancelResponse;
import com.molekule.api.v1.commonframework.model.order.OrderReturnRequestBean;
import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean;
import com.molekule.api.v1.commonframework.model.order.ReprocessOrderDTO;
import com.molekule.api.v1.commonframework.model.order.SerialNumberValidationBean;
import com.molekule.api.v1.commonframework.model.order.SortActionEnum;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.model.sendgrid.Product;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.cart.TaxService;
import com.molekule.api.v1.commonframework.service.order.AdyenPaymentHeplerService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.CustomerProfileHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.LifeTimeSalesEnum;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.commonframework.util.OrderServiceUtility.RMASTATUS;
/**
 * The class OrderService is used to write the api calls for the Order Api.
 * 
 * @version 1.0
 */
@Service("orderService")
public class OrderService {

	Logger logger = LoggerFactory.getLogger(OrderService.class);
	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("orderHelperService")
	OrderHelperService orderHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	@Autowired
	@Qualifier("amazonDynamoDB")
	AmazonDynamoDB amazonDynamoDB;

	@Autowired
	@Qualifier(value="orderReturnConfirmationMailSender")
	TemplateMailRequestHelper orderReturnConfirmationMailSender;

	@Autowired
	@Qualifier(value="orderRefundMailSender")
	TemplateMailRequestHelper orderRefundMailSender;

	@Autowired
	@Qualifier(value="orderReturnedItemMailSender")
	TemplateMailRequestHelper orderReturnedItemMailSender;

	@Qualifier("taxService")
	TaxService taxService;

	@Autowired
	@Qualifier("customerProfileHelperService")
	CustomerProfileHelperService customerProfileHelperService;

	@Value("${stripeApiKey}")
	private String stripeAuthKey;

	@Autowired
	@Qualifier("adyenPaymentHeplerService")
	AdyenPaymentHeplerService adyenPaymentHeplerService;
	
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * getOrderByOrderId method is used to display the Order Data by orderId.
	 * @param orderId
	 * 
	 * @return String - it contain order data.
	 */
	public ResponseEntity<Object> getOrderByOrderId(String orderId) {
		return orderHelperService.getOrderByOrderId(orderId);
	}

	public OrderSummaryResponseBean getAllOrders(String customerId, Integer limit, int offset, String email, String orderNumber, Boolean isGift, String channel, SortActionEnum sortAction, SortOrderEnum sortOrder,String giftReceiptMail) {
		return orderHelperService.getAllOrders(customerId, limit, offset, email, orderNumber, isGift, channel, sortAction, sortOrder, giftReceiptMail);
	}

	public String getOrderNumber(String locationType) {
		return orderHelperService.getOrderNumber(locationType);
	}

	public String getOrderReadStatus(String orderId) {
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String response =  orderHelperService.getOrderObj(orderId, token);
		return orderHelperService.getReadFlagResponseBean(response);
	}

	public String updateOrderReadStatus(String orderId, Boolean readFlagValue) {
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String response =  orderHelperService.updateReadFlag(orderId, readFlagValue, token);
		return orderHelperService.getReadFlagResponseBean(response);
	}

	public List<ReprocessOrderDTO> getReprocessOrderList(String status, String createdDateRange, String orderNumberRange) {
		DynamoDBMapperConfig mapperConfig = new DynamoDBMapperConfig.Builder().withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(ctEnvProperties.getNetsuiteOrderStatusTableName())).build();
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDB, mapperConfig);
		List<ReprocessOrderDTO> reprocessOrderDTOList = mapper.scan(ReprocessOrderDTO.class, new DynamoDBScanExpression());
		List<ReprocessOrderDTO> filteredReprocessDtoList = new ArrayList<>();

		if(StringUtils.hasValue(status)) {
			reprocessOrderDTOList.stream().forEach(dto->{
				if(dto.getStatus()!=null && dto.getStatus().equals(status)) {
					filteredReprocessDtoList.add(dto);
				}
			});
		} 

		processCreatedDateRange(createdDateRange, reprocessOrderDTOList, filteredReprocessDtoList);
		
		processOrderNumberRange(orderNumberRange, reprocessOrderDTOList,filteredReprocessDtoList);
		
		if(!filteredReprocessDtoList.isEmpty()) {
			return filteredReprocessDtoList;
		}
		return reprocessOrderDTOList;
	}

	private void processCreatedDateRange(String createdDateRange, List<ReprocessOrderDTO> reprocessOrderDTOList,
			List<ReprocessOrderDTO> filteredReprocessDtoList) {
		if (!StringUtils.hasValue(createdDateRange)) {
			return;
		}

		String dates[] = createdDateRange.split(":");
		try {
			Date createdDateFrom = dateFormat.parse(dates[0]);
			Date createdDateTo = (dates.length == 2) ? dateFormat.parse(dates[1]) : createdDateFrom;
			for (ReprocessOrderDTO dto : reprocessOrderDTOList) {
				if (dto.getCreatedAt() != null) {
					Date dtoCreatedDateFormatted = dateFormat.parse(dto.getCreatedAt());
					if (dtoCreatedDateFormatted.equals(createdDateFrom) || dtoCreatedDateFormatted.equals(createdDateTo)
							|| (dtoCreatedDateFormatted.after(createdDateFrom)
									&& dtoCreatedDateFormatted.before(createdDateTo))) {
						filteredReprocessDtoList.add(dto);
					}
				}
			}
		} catch (ParseException parseException) {
			logger.error("Parse exception occured when we processed the Data", parseException);
		}

	}

	private void processOrderNumberRange(String orderNumberRange, List<ReprocessOrderDTO> reprocessOrderDTOList,
			List<ReprocessOrderDTO> filteredReprocessDtoList) {
		if (orderNumberRange == null) {
			return;
		}
		String orderNumbers[] = orderNumberRange.split(":");

		Long orderNumberFrom = orderNumbers[0].contains(GBR) ? Long.parseLong(orderNumbers[0].substring(GBR.length()))
				: Long.parseLong(orderNumbers[0]);

		Long orderNumberTo;
		if (orderNumbers.length == 2) {
			orderNumberTo = orderNumbers[1].contains(GBR) ? Long.parseLong(orderNumbers[1].substring(GBR.length()))
					: Long.parseLong(orderNumbers[1]);
		} else {
			orderNumberTo = orderNumberFrom;
		}

		reprocessOrderDTOList.stream().forEach(dto -> {
			try {
			if (dto.getOrderNumber() != null) {
				String orderNumber = dto.getOrderNumber();
				if ((orderNumber.contains(GBR))) {
					orderNumber = orderNumber.substring(GBR.length());
				}
				long eachOrderNumber = Long.parseLong(orderNumber);
				if (eachOrderNumber >= orderNumberFrom && eachOrderNumber <= orderNumberTo) {
					filteredReprocessDtoList.add(dto);
				}
			}
			} catch(NumberFormatException ex) {
				logger.error("Getting Invalid Order Number", ex);
			}
		});

	}

	public List<ReprocessOrderDTO> updateReprocessOrder(List<String> orderNumberList, String status) {
		List<ReprocessOrderDTO> updatedReprocessOrderList = new ArrayList<>();
		orderNumberList.stream().forEach(orderNumber->{
			UpdateItemRequest request = new UpdateItemRequest();
			request.setTableName(ctEnvProperties.getNetsuiteOrderStatusTableName());
			request.setReturnValues(ReturnValue.UPDATED_NEW);
			Map<String, AttributeValue> keysMap = new HashMap<>();
			keysMap.put(ORDER_NUMBER, new AttributeValue(orderNumber));
			request.setKey(keysMap);
			Map<String, AttributeValueUpdate> map = new HashMap<>();
			map.put(STATUS, new AttributeValueUpdate(new AttributeValue(status),"PUT"));
			map.put(LAST_MODIFIED_AT, new AttributeValueUpdate(new AttributeValue(String.valueOf(new Timestamp(System.currentTimeMillis()).toInstant())),"PUT"));
			request.setAttributeUpdates(map);
			try {
				amazonDynamoDB.updateItem(request);
				GetItemResult getItemResult = amazonDynamoDB.getItem(ctEnvProperties.getNetsuiteOrderStatusTableName(), keysMap);
				Map<String, AttributeValue> itemResultMap = getItemResult.getItem();
				ReprocessOrderDTO reprocessOrderDTO = new ReprocessOrderDTO();
				reprocessOrderDTO.setOrderNumber(itemResultMap.get(ORDER_NUMBER)!=null ? itemResultMap.get(ORDER_NUMBER).getS():"");
				reprocessOrderDTO.setPayload(itemResultMap.get("payload")!=null ? itemResultMap.get("payload").getS():"");
				reprocessOrderDTO.setStatus(itemResultMap.get(STATUS)!=null ? itemResultMap.get(STATUS).getS():"");
				reprocessOrderDTO.setCreatedAt(itemResultMap.get("createdAt")!=null ? itemResultMap.get("createdAt").getS():"");
				reprocessOrderDTO.setLastModifiedAt(itemResultMap.get(LAST_MODIFIED_AT)!=null ? itemResultMap.get(LAST_MODIFIED_AT).getS():"");
				reprocessOrderDTO.setError(itemResultMap.get("error")!=null ? itemResultMap.get("error").getS():"");
				updatedReprocessOrderList.add(reprocessOrderDTO);
			} catch (AmazonServiceException exception) {
				logger.error("exception occured when updating the table", exception);
			}
		});
		return updatedReprocessOrderList;
	}

	public String getOrderTags() {
		String orderTagUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey()+"/custom-objects/order_tag/csr_order_create";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		return netConnectionHelper.sendGetWithoutBody(token, orderTagUrl);
	}

	public String getRMAInitiationValues() {
		String rmaBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects/csr_order_return_rma_initiation/d2c_return_warranty_replacement";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		return netConnectionHelper.sendGetWithoutBody(token, rmaBaseUrl);
	}

	public String processOrderReturn(String orderId, List<OrderReturnRequestBean> orderReturnRequestBeanList) {
		OrderReturnServiceBean orderReturnServiceBean = new OrderReturnServiceBean();
		String ordeBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + ORDERS+ orderId ;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String ordeResponse = netConnectionHelper.sendGetWithoutBody(token, ordeBaseUrl);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(ordeResponse);
		orderReturnServiceBean.setVersion(orderJsonObject.get(VERSION).getAsLong());
		List<OrderReturnServiceBean.Action> actionList = new ArrayList<>();
		setOrderReturnInfoAction(orderReturnRequestBeanList, actionList);
		setOrderReturnCustomFields(orderReturnRequestBeanList, actionList,orderJsonObject);
		orderReturnServiceBean.setActions(actionList);
		logger.info("Order Number : {}",orderJsonObject.get(ORDER_NUMBER).getAsString());
		logger.info("AddReturnInfo Payload : {}",orderReturnServiceBean);
		String orderReturnResponse = (String) netConnectionHelper.sendPostRequest(ordeBaseUrl, token, orderReturnServiceBean);
		JsonObject orderReturnJsonObject = MolekuleUtility.parseJsonObject(orderReturnResponse);
		String customerId = orderReturnJsonObject.get("customerId").getAsString();
		//SendGrid Implementation
		returnMailSender(customerId,orderReturnJsonObject);
		BulkUpdateRequestBean bulkUpdateRequestBean = new BulkUpdateRequestBean();
		List<String> subscriptionIdList = new ArrayList<>();
		List<String> serialNumbersList = new ArrayList<>();
		bulkUpdateRequestBean.setLimit(20);
		bulkUpdateRequestBean.setOffset(0);
		List<JsonObject> subscriptionJsonObjectList = customerProfileHelperService.getSubscriptionContainerAndKey(customerId, orderId);

		JsonArray orderLineItemsJsonArray = orderJsonObject.get(LINE_ITEMS).getAsJsonArray();
		orderReturnRequestBeanList.stream().forEach(orderReturnRequestBean -> {
			for (int j = 0; j < orderLineItemsJsonArray.size(); j++) {
				JsonObject orderLineItemsJsonObject = orderLineItemsJsonArray.get(j).getAsJsonObject();
				if(orderLineItemsJsonObject.get(ID).getAsString().equals(orderReturnRequestBean.getLineItemId())) {
					JsonArray orderAttributesJsonArray = orderLineItemsJsonObject.get(VARIANT).getAsJsonObject()
							.get(ATTRIBUTES).getAsJsonArray();
					for (int z = 0; z < orderAttributesJsonArray.size(); z++) {
						JsonObject subscriptionIdentifierObject = orderAttributesJsonArray.get(z).getAsJsonObject();
						String name = subscriptionIdentifierObject.get("name").getAsString();
						if ("filterProduct".equals(name)) {
							String orderSubscriptionAttributeValue = subscriptionIdentifierObject.get("value").getAsJsonObject().get(ID).getAsString();
							subscriptionJsonObjectList.stream().forEach(subscriptionJsonObject -> {
								JsonObject subCartJsonObject = subscriptionJsonObject.get(VALUE).getAsJsonObject().get("cart")
										.getAsJsonObject();
								JsonObject subCartObject = subCartJsonObject.get("obj").getAsJsonObject();
								JsonArray subCartLineItems = subCartObject.get(LINE_ITEMS).getAsJsonArray();
								String subscriptionProductId = subCartLineItems.get(0).getAsJsonObject().get(PRODUCT_ID).getAsString();
								if(subscriptionProductId.equals(orderSubscriptionAttributeValue)) {
									subscriptionIdList.add(subscriptionJsonObject.get(ID).getAsString());
								}
							});
						}
					}
					if(orderLineItemsJsonObject.has(CUSTOM)) {
						JsonObject orderLineItemCustomFields = orderLineItemsJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
						if(orderLineItemCustomFields.has(SERIAL_NUMBERS)) {
							serialNumbersList.add(orderLineItemCustomFields.get(SERIAL_NUMBERS).getAsJsonArray().get(0).getAsString());
						}
					}
				}
			}
		});
		if(!subscriptionIdList.isEmpty()) {
			bulkUpdateRequestBean.setSubscriptionIdList(subscriptionIdList);
			customerProfileHelperService.bulkSubscriptionTurnOff(bulkUpdateRequestBean, token);
		}
		if(!serialNumbersList.isEmpty()) {
			customerProfileHelperService.bulkDeleteDevice(serialNumbersList);
		}
		return orderReturnResponse;
	}

	private void returnMailSender(String customerId,JsonObject orderReturnJsonObject) {
		SendGridModel sendGridModel = new SendGridModel();
		JsonObject customerJsonObject = null;
		if(customerId != null) {
			String customerUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
					+ customerId;
			String customerResponse = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(),
					customerUrl);
			customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
			sendGridModel.setEmail(customerJsonObject.get("email").getAsString());
			sendGridModel.setFirstName(customerJsonObject.get("firstName").getAsString());
			sendGridModel.setLastName(customerJsonObject.get("lastName").getAsString());
			sendGridModel.setChannel(customerJsonObject.getAsJsonObject(CUSTOM).getAsJsonObject(FIELDS).get(CHANNEL).getAsString());
		}
		sendGridModel.setOrderId(orderReturnJsonObject.get("id").getAsString());
		sendGridModel.setOrderNumber(orderReturnJsonObject.get(ORDER_NUMBER).getAsString());
		JsonArray returnJsonArray = orderReturnJsonObject.getAsJsonArray(RETURN_INFO);
		JsonArray lineItemArray = orderReturnJsonObject.getAsJsonArray(LINE_ITEMS);
		sendGridModel.setReturnNumber(returnJsonArray.get(0).getAsJsonObject().get("returnTrackingId").getAsString());
		for(int k=0;k<lineItemArray.size();k++) {
			if(!lineItemArray.get(k).getAsJsonObject().getAsJsonObject(CUSTOM).getAsJsonObject(FIELDS).has("subscriptionEnabled")) {
				for(int i=0;i<returnJsonArray.size();i++) {
					JsonArray itemsJsonArray = returnJsonArray.get(i).getAsJsonObject().getAsJsonArray(ITEMS);
					String lineItemId = itemsJsonArray.get(0).getAsJsonObject().get(LINE_ITEM_ID).getAsString();
					if(lineItemArray.get(k).getAsJsonObject().get("id").getAsString().equals(lineItemId)) {
						sendGridModel.setReturnLabelUrl(lineItemArray.get(k).getAsJsonObject().getAsJsonObject(CUSTOM).
								getAsJsonObject(FIELDS).get("fedexReturnLable").getAsString());
					}
				}
				orderReturnConfirmationMailSender.sendMail(sendGridModel);
			}
		}
	}

	private void setOrderReturnCustomFields(List<OrderReturnRequestBean> orderReturnRequestBeanList, List<Action> actionList, JsonObject orderJsonObject) {
		orderReturnRequestBeanList.stream().forEach(orderReturnRequestBean -> {
			String resolution = orderReturnRequestBean.getResolution() !=null ? orderReturnRequestBean.getResolution():"";
			String packageCondition = orderReturnRequestBean.getPackageCondition() !=null ? orderReturnRequestBean.getPackageCondition():"";
			String functionalIssue = orderReturnRequestBean.getFunctionalIssue() !=null ? orderReturnRequestBean.getFunctionalIssue():"";
			String returnLocation = orderReturnRequestBean.getReturnLocation() !=null ? orderReturnRequestBean.getReturnLocation():"";
			String returnReason = orderReturnRequestBean.getReturnReasons() !=null ? orderReturnRequestBean.getReturnReasons():"";
			String fedexReturnLabel = "FedexReturnLabel";
			String serialNumber =orderReturnRequestBean.getSerialNumber() !=null ? orderReturnRequestBean.getSerialNumber():"";
			String failureSymptoms = orderReturnRequestBean.getFailureSymptoms() !=null ? orderReturnRequestBean.getFailureSymptoms():"";
			String returnQuantity = orderReturnRequestBean.getQuantity() !=null ? orderReturnRequestBean.getQuantity():"";
			String requestLineItemId = orderReturnRequestBean.getLineItemId();
			boolean isCustomFieldAvailable = checkCustomFieldInOrderLineItem(orderJsonObject,orderReturnRequestBean.getLineItemId());
			if(!isCustomFieldAvailable) {
				OrderReturnServiceBean.Action action = new OrderReturnServiceBean.Action();
				OrderReturnServiceBean.Action.Type orderReturnType = new OrderReturnServiceBean.Action.Type();
				OrderReturnServiceBean.Action.Fields field = new OrderReturnServiceBean.Action.Fields();
				action.setActionName("setLineItemCustomType");
				orderReturnType.setId(ctEnvProperties.getLineItemTypeId());
				orderReturnType.setTypeId("type");
				field.setFunctionalIssue(functionalIssue);
				field.setPackageCondition(packageCondition);
				field.setResolution(resolution);
				field.setReturnLocation(returnLocation);
				field.setReturnReasons(returnReason);
				field.setFedexReturnLable(fedexReturnLabel);
				field.setSerialNumbers(Arrays.asList(serialNumber));
				field.setReturnQuantity(returnQuantity);
				field.setFailureSymptoms(failureSymptoms);
				action.setType(orderReturnType);
				action.setFields(field);
				action.setLineItemId(orderReturnRequestBean.getLineItemId());
				actionList.add(action);
			}else {
				// update resolution
				actionList.add(setOrderReturnCustomFieldsAction(resolution, "resolution", requestLineItemId));
				// update functionalIssue
				actionList.add(setOrderReturnCustomFieldsAction(functionalIssue, "functionalIssue", requestLineItemId));
				// update packageCondition
				actionList.add(setOrderReturnCustomFieldsAction(packageCondition, "packageCondition", requestLineItemId));
				// update returnReasons
				actionList.add(setOrderReturnCustomFieldsAction(returnReason, "returnReasons", requestLineItemId));
				// update returnLocation
				actionList.add(setOrderReturnCustomFieldsAction(returnLocation, "returnLocation", requestLineItemId));
				// update fedexReturnLable
				actionList.add(setOrderReturnCustomFieldsAction(fedexReturnLabel, "fedexReturnLable", requestLineItemId));
				// update serialNumber
				actionList.add(setOrderReturnCustomFieldsAction(Arrays.asList(serialNumber),SERIAL_NUMBERS, requestLineItemId));
				// update failure Symptoms
				actionList.add(setOrderReturnCustomFieldsAction(failureSymptoms, "failureSymptoms", requestLineItemId));
				//update return quantity
				actionList.add(setOrderReturnCustomFieldsAction(returnQuantity, "returnQuantity", requestLineItemId));
			}
		});
	}

	private boolean checkCustomFieldInOrderLineItem(JsonObject orderJsonObject, String lineItemId) {
		boolean iscustom = false;
		JsonArray lineItemArray = orderJsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i=0;i<lineItemArray.size();i++) {
			JsonObject lineItemObject = lineItemArray.get(i).getAsJsonObject();
			if(lineItemObject.get("id").getAsString().equals(lineItemId)&&lineItemObject.has(CUSTOM)){
				iscustom = true;
			}
		}
		return iscustom;
	}

	private OrderReturnServiceBean.Action setOrderReturnCustomFieldsAction(Object value, String filedName, String requestLineItemId) {
		OrderReturnServiceBean.Action action = new OrderReturnServiceBean.Action();
		action.setActionName("setLineItemCustomField");
		action.setName(filedName);
		action.setValue(value);
		action.setLineItemId(requestLineItemId);
		return action;
	}

	private void setOrderReturnInfoAction(List<OrderReturnRequestBean> orderReturnRequestBeanList,
			List<OrderReturnServiceBean.Action> actionList) {
		orderReturnRequestBeanList.stream().forEach(orderReturnRequestBean -> {
			OrderReturnServiceBean.Action action = new OrderReturnServiceBean.Action();
			action.setActionName("addReturnInfo");
			action.setReturnDate(String.valueOf(new Timestamp(System.currentTimeMillis()).toInstant()));
			action.setReturnTrackingId(orderHelperService.getTrackingNumber());
			List<OrderReturnServiceBean.Action.Items> itemsList = new ArrayList<>();
			OrderReturnServiceBean.Action.Items items = new OrderReturnServiceBean.Action.Items();
			items.setQuantity(Integer.parseInt(orderReturnRequestBean.getQuantity()));
			items.setLineItemId(orderReturnRequestBean.getLineItemId());
			items.setComment(orderReturnRequestBean.getComment());
			items.setShipmentState("Returned");
			itemsList.add(items);
			action.setItems(itemsList);
			actionList.add(action);
		});
	}

	public ResponseEntity<String> processOrderRefund(String orderId, OrderRefundRequestBean orderRefundRequestBean) {
		OrderRefundServiceBean orderRefundServiceBean = new OrderRefundServiceBean();
		Map<String, String> responseMap = new HashMap<>();
		String ordeBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + ORDERS+ orderId + "?expand=paymentInfo.payments[*].typeId" ;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResponse = netConnectionHelper.sendGetWithoutBody(token, ordeBaseUrl);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		if(orderJsonObject.has(STATUS_CODE)) {
			responseMap.put(ERROR, orderResponse);
			return new ResponseEntity<>(MolekuleUtility.parseJsonObject(responseMap.toString()).toString(), HttpStatus.BAD_REQUEST);
		}
		
		//cartId
		String cartId = orderJsonObject.get("cart").getAsJsonObject().get(ID).getAsString();

		responseMap.put("orderResponse", orderResponse);
		JsonArray lineItemsArray = orderJsonObject.get(LINE_ITEMS).getAsJsonArray();
		orderRefundServiceBean.setVersion(orderJsonObject.get(VERSION).getAsLong());
		List<LineItems> lineItems = orderRefundRequestBean.getLineItemsList();
		Long discountedAmount = 0L;
		Long taxAmount = 0L;
		for(int i=0;i<lineItemsArray.size();i++) {
			JsonObject lineItemObject = lineItemsArray.get(i).getAsJsonObject();
			for(int j=0;j<lineItems.size();j++) {
				if(lineItemObject.get(ID).getAsString().equals(lineItems.get(j).getLineItemId())) {
					Integer quantity = lineItems.get(j).getQuantityToRefund();

					JsonArray discountedPricePerQuantityArray = lineItemObject.get(DISCOUNTED_PRICE_PER_QUANTITY).getAsJsonArray();
					if(discountedPricePerQuantityArray.size()!=0) {
						for (int z = 0; z < discountedPricePerQuantityArray.size(); z++) {
							JsonObject discountedPricePerQuantityJsonObject = discountedPricePerQuantityArray.get(z).getAsJsonObject();
							JsonArray includedDiscountsArray = discountedPricePerQuantityJsonObject.get(DISCOUNTED_PRICE).getAsJsonObject().get(INCLUDED_DISCOUNTS).getAsJsonArray();
							for (int k = 0; k < includedDiscountsArray.size(); k++) {
								JsonObject includedDiscountsJson = includedDiscountsArray.get(k).getAsJsonObject();
								long lineItemDiscount = includedDiscountsJson.get(DISCOUNTED_AMOUNT).getAsJsonObject().get(CENT_AMOUNT).getAsLong();
								long totalDiscountLineItems = lineItemDiscount * quantity;
								discountedAmount = discountedAmount + totalDiscountLineItems;
							}
						}
					}
					JsonObject taxedPriceObject = lineItemObject.get("taxedPrice").getAsJsonObject();
					JsonObject totalNetObject = taxedPriceObject.get("totalNet").getAsJsonObject();
					JsonObject totalGrossObject = taxedPriceObject.get("totalGross").getAsJsonObject();
					Long totalNetPrice = totalNetObject.get(CENT_AMOUNT).getAsLong();
					Long totalGrossPrice = totalGrossObject.get(CENT_AMOUNT).getAsLong();
					taxAmount = taxAmount + ((totalGrossPrice - totalNetPrice)/quantity);
				}
			}
		}

		BigDecimal totalDiscountAmount = setBecimalvalue(discountedAmount);
		BigDecimal totalTax = setBecimalvalue(taxAmount);
		BigDecimal refundTotal = totalDiscountAmount.add(totalTax);
		RefundCustomObject refundCustomObject = new RefundCustomObject();
		RefundCustomObject.Value value = new RefundCustomObject.Value();
		String container = "d2c_refund_"+ orderJsonObject.get(ORDER_NUMBER).getAsString();

		value.setAdjustmentFee(orderRefundRequestBean.getAdjustmentFee());		
		value.setAdjustmentRefund(orderRefundRequestBean.getAdjustmentRefund());
		value.setDiscountTotal(totalDiscountAmount.toString());
		value.setLineItemsList(lineItems);
		value.setRefundShipping(orderRefundRequestBean.getRefundShipping());
		value.setTax(totalTax.toString());
		value.setRefundTotal(refundTotal.toString());
		value.setComments(orderRefundRequestBean.getComments());

		refundCustomObject.setContainer(container);
		refundCustomObject.setKey(orderRefundRequestBean.getInvoiceNumber());
		refundCustomObject.setValue(value);	
		String createCustomObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/custom-objects";
		String refundCustomObjectResponse = (String)netConnectionHelper.sendPostRequest(createCustomObjectUrl, token, refundCustomObject);
		JsonObject refundCustomJsonObject = MolekuleUtility.parseJsonObject(refundCustomObjectResponse);
		if(refundCustomJsonObject.has(STATUS_CODE)) {
			responseMap.put(ERROR, refundCustomObjectResponse);
			return new ResponseEntity<>(MolekuleUtility.parseJsonObject(responseMap.toString()).toString(), HttpStatus.BAD_REQUEST);
		}

		JsonObject paymentInfoObject = orderJsonObject.get(PAYMENT_INFO).getAsJsonObject();
		JsonArray paymentsArray = paymentInfoObject.get(PAYMENTS).getAsJsonArray();
		JsonObject paymentsObject = paymentsArray.get(0).getAsJsonObject().get("obj").getAsJsonObject();
		String paymentInterfaceId = paymentsObject.get("interfaceId").getAsString();
		String currencyCode = paymentsObject.get("amountPlanned").getAsJsonObject().get("currencyCode").getAsString();
		
		RefundDTO refundObject = new RefundDTO();
		Long refundLongAmount = refundTotal.multiply(new BigDecimal(100)).longValue();
		refundObject.setAmount(refundLongAmount.toString());
		refundObject.setCurrency(currencyCode);
		refundObject.setPspReference(paymentInterfaceId);
		refundObject.setReference(cartId);
		
		String paymentResponse = adyenPaymentHeplerService.refundPayment(refundObject);
		JsonObject paymentObject = MolekuleUtility.parseJsonObject(paymentResponse);
		String channel = orderJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(CHANNEL).getAsString();
		if (paymentObject.has(STATUS) && "received".equals(paymentObject.get(STATUS).getAsString())) {
			List<OrderRefundServiceBean.Action> actionList = new ArrayList<>();
			Map<String, Object> refundNetsuiteMap = new HashMap<>();
			List<Map<String, String>> nsItems = new ArrayList<>();

			JsonArray returnInfoArray = orderJsonObject.get(RETURN_INFO).getAsJsonArray();
			for (int i = 0; i < returnInfoArray.size(); i++) {
				Map<String, String> nsItemMap = new HashMap<>();
				JsonObject returnInfoObject = returnInfoArray.get(i).getAsJsonObject();
				JsonArray itemsArray = returnInfoObject.get(ITEMS).getAsJsonArray();
				for (int j = 0; j < itemsArray.size(); j++) {
					JsonObject itemsObject = itemsArray.get(j).getAsJsonObject();
					for (int z = 0; z < lineItems.size(); z++) {
						if (itemsObject.get(LINE_ITEM_ID).getAsString().equals(lineItems.get(z).getLineItemId())) {
							actionList.add(setReturnPaymentState(itemsObject.get(ID).getAsString()));
							nsItemMap.put("id", itemsObject.get(ID).getAsString());
							nsItemMap.put("paymentState", "Refunded");
							nsItems.add(nsItemMap);
						}
					}
				}
			}
			orderRefundServiceBean.setActions(actionList);
			orderResponse = (String) netConnectionHelper.sendPostRequest(ordeBaseUrl, token, orderRefundServiceBean);
			orderJsonObject = MolekuleUtility.parseJsonObject(orderResponse);

			if(!orderJsonObject.has(STATUS_CODE)) {
				responseMap.put("orderResponse", orderResponse);

				String customerUrlById = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() 
				+ MolekuleConstant.SLASH_CUSTOMERS_SLASH + orderJsonObject.get("customerId").getAsString();
				String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerUrlById);
				Long refundAmount = refundTotal.multiply(new BigDecimal(100)).longValue();
				CustomerProfileHelperService.setLifeTimeSales(netConnectionHelper, customerResponse, orderResponse, 
						LifeTimeSalesEnum.SUBTRACT_ORDER_TOTAL, customerUrlById, token, refundAmount.toString());

				Map<String, Object> updatedReturnMap = new HashMap<>();
				updatedReturnMap.put(ITEMS, nsItems);

				refundNetsuiteMap.put("entity", "order");
				refundNetsuiteMap.put("action","update");
				refundNetsuiteMap.put("actionType","ReturnPaymentStateChanged");
				refundNetsuiteMap.put(CHANNEL, channel);
				refundNetsuiteMap.put("object", orderResponse);
				refundNetsuiteMap.put("updatedReturnInfo", updatedReturnMap);
				orderHelperService.sendAmazonSqsToNS(refundNetsuiteMap);
			}
		}
		responseMap.put("refundCustom", refundCustomObjectResponse);
		tealiumHelperService.triggerRefundEvent(responseMap);
		refundMailSender(orderJsonObject,lineItems);
		return new ResponseEntity<>(MolekuleUtility.parseJsonObject(responseMap.toString()).toString(), HttpStatus.OK);
	}

	private void refundMailSender(JsonObject orderJsonObject,List<LineItems> lineItems) {
		SendGridModel sendGridModel = new SendGridModel();
		sendGridModel.setOrderNumber(orderJsonObject.get(ORDER_NUMBER).getAsString());
		sendGridModel.setOrderId(orderJsonObject.get("id").getAsString());
		sendGridModel.setEmail(orderJsonObject.get("customerEmail").getAsString());
		JsonArray lineItemArray = orderJsonObject.getAsJsonArray(LINE_ITEMS);
		String channel = orderJsonObject.getAsJsonObject(CUSTOM).getAsJsonObject(FIELDS).get(CHANNEL).getAsString();
		String country = orderJsonObject.get(COUNTRY).getAsString();
		List<Product> products = new ArrayList<>();
		for(int i=0;i<lineItemArray.size();i++) {
			if(!lineItemArray.get(i).getAsJsonObject().getAsJsonObject(CUSTOM).getAsJsonObject(FIELDS).has("subscriptionEnabled")) {
				for(int j=0;j<lineItems.size();j++) {
					Product product = new Product();				
					if(lineItemArray.get(i).getAsJsonObject().get("id").getAsString().equals(lineItems.get(j).getLineItemId())){
						if(orderJsonObject.has(COUNTRY) && country.equals("US")) {
							product.setOrderProductName(lineItemArray.get(i).getAsJsonObject().getAsJsonObject("name").get("en-US").getAsString());
						}else{
							product.setOrderProductName(lineItemArray.get(i).getAsJsonObject().getAsJsonObject("name").get("en-GB").getAsString());
						}
						product.setOrderProductQty(lineItems.get(j).getQuantityToRefund());
						List<String> productDetails = orderHelperService.getProductImage(lineItemArray.get(i).getAsJsonObject().get("productId").getAsString(),country,channel);
						if(productDetails.size() > 1){
							product.setOrderProductDesc(productDetails.get(1));
							product.setOrderImageUrl(productDetails.get(0));
						}
						products.add(product);
						sendGridModel.setProducts(products);
						JsonObject paymentJson = orderJsonObject.getAsJsonObject(PAYMENT_INFO).getAsJsonArray(PAYMENTS).get(0).getAsJsonObject();
						if(paymentJson.getAsJsonObject("obj").getAsJsonObject(PAYMENT_METHOD_INFO).get("method").getAsString().equals("CREDIT_CARD")) {
							sendGridModel.setCreditToCC(true);
						}else if(paymentJson.getAsJsonObject("obj").getAsJsonObject(PAYMENT_METHOD_INFO).get("method").getAsString().equals("AFFIRM")) {
							sendGridModel.setCreditToCC(false);
						}
						JsonObject fieldJson = orderJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
						sendGridModel.setOrderSummarySubtotal(centToDollarConversion(
								fieldJson.get(SUB_TOTAL).getAsLong(),country));
						if(country.equals("US") && fieldJson.has(TOTAL_TAX)) {
							sendGridModel.setOrderSummaryTax(centToDollarConversion(
									fieldJson.get(TOTAL_TAX).getAsLong(),country));
						}
						sendGridModel.setOrderSummaryShipping(centToDollarConversion(
								fieldJson.get(SHIPPING_COST).getAsLong(),country));
						if(country.equals("US") && fieldJson.has(TOTAL_TAX)) {
							sendGridModel.setReturnTotal(centToDollarConversion(
									fieldJson.get(SUB_TOTAL).getAsLong()+fieldJson.get(TOTAL_TAX).getAsLong()+fieldJson.get(SHIPPING_COST).getAsLong(),country));
						}else {
							sendGridModel.setReturnTotal(centToDollarConversion(
									fieldJson.get(SUB_TOTAL).getAsLong()+fieldJson.get(SHIPPING_COST).getAsLong(),country));
						}
						sendGridModel.setChannel(orderJsonObject.getAsJsonObject(CUSTOM).getAsJsonObject(FIELDS).get(CHANNEL).getAsString());
						orderRefundMailSender.sendMail(sendGridModel);
					}
				}
			}
		}
	}

	private com.molekule.api.v1.commonframework.dto.order.OrderRefundServiceBean.Action setReturnPaymentState(String returnItemId) {
		OrderRefundServiceBean.Action action = new OrderRefundServiceBean.Action();	
		action.setActionName("setReturnPaymentState");
		action.setReturnItemId(returnItemId);
		action.setPaymentState("Refunded");
		return action;
	}

	private BigDecimal setBecimalvalue(Long discountedAmount) {
		BigDecimal discountedAmountBigDecimal = new BigDecimal(discountedAmount).divide(BigDecimal.valueOf(100));
		return discountedAmountBigDecimal.setScale(2,RoundingMode.HALF_EVEN);
	}

	public ResponseEntity<Object> orderCancel(String orderId) {
		String ordeBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + ORDERS+ orderId+"?expand=lineItems[*].state[*].state.typeId&expand=state.typeId&expand=paymentInfo.payments[*].typeId" ;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResponse = netConnectionHelper.sendGetWithoutBody(token, ordeBaseUrl);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		// Netsuite Call
		String netsuiteResponseBody = doNetsuiteCancelApiCall(orderJsonObject);
		JsonObject netsuiteJsonObject = MolekuleUtility.parseJsonObject(netsuiteResponseBody);
		if(netsuiteJsonObject.has(ERROR)) {
			ErrorResponse errorResponse = new ErrorResponse(400, netsuiteResponseBody);
			return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
		}
		OrderCancelResponse orderCancelResponse = new OrderCancelResponse();
		orderCancelResponse.setOrderId(orderId);
		if(netsuiteJsonObject.get("ctOrderState").getAsString().equals("Cancelled")&&netsuiteJsonObject.get(STATUS).getAsString().equals("success") ) {
			orderCancelResponse.setCancelled(true);
			return new ResponseEntity<>(orderCancelResponse, HttpStatus.OK);
		} else {
			orderCancelResponse.setCancelled(false);
			return new ResponseEntity<>(orderCancelResponse, HttpStatus.OK);
		}
	}

	private String doNetsuiteCancelApiCall(JsonObject orderJsonObject) {
		String orderNumber = orderJsonObject.get("orderNumber").getAsString();
		String nsUrl = ctEnvProperties.getOAuthBaseUrl()+ "?script=" + ctEnvProperties.getOAuthCancellationApiValidationScriptId() 
		+"&deploy=" + ctEnvProperties.getOAuthCancellationApiValidationDeploymentId()+"&orderid="+ orderNumber;
		return netConnectionHelper.getNetSuitAPI(nsUrl,orderNumber);
	}

	public String updateRMAStatusToOrder(String orderId, List<String> lineItemIdList, RMASTATUS rmaStatus) {
		String response = null;
		String ordeBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + ORDERS+ orderId+"?expand=paymentInfo.payments[*].typeId";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String ordeResponse = netConnectionHelper.sendGetWithoutBody(token, ordeBaseUrl);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(ordeResponse);
		OrderResponseServiceBean orderResponseServiceBean = new OrderResponseServiceBean();
		orderResponseServiceBean.setVersion(orderJsonObject.get(VERSION).getAsLong());
		List<OrderResponseServiceBean.Action> actionList = new ArrayList<>();
		lineItemIdList.stream().forEach(lineItemId->{
			boolean isCustomFieldAvailable = checkCustomFieldInOrderLineItem(orderJsonObject,lineItemId);
			OrderResponseServiceBean.Action action = new OrderResponseServiceBean.Action();
			if(!isCustomFieldAvailable) {
				OrderResponseServiceBean.Action.Type orderResponseType = new OrderResponseServiceBean.Action.Type();
				OrderResponseServiceBean.Action.Fields field = new OrderResponseServiceBean.Action.Fields();
				action.setActionName("setLineItemCustomType");
				orderResponseType.setId(ctEnvProperties.getLineItemTypeId());
				orderResponseType.setTypeId("type");
				field.setRmaStatus(rmaStatus.getValue());
				action.setType(orderResponseType);
				action.setFields(field);
			}else {
				action.setActionName("setLineItemCustomField");
				action.setName("rmaStatus");
				action.setValue(rmaStatus.getValue());
			}
			action.setLineItemId(lineItemId);
			actionList.add(action);
			orderResponseServiceBean.setActions(actionList);
		});
		response = (String)netConnectionHelper.sendPostRequest(ordeBaseUrl, token, orderResponseServiceBean);
		returnedItemMailSender(response,orderJsonObject);
		return response;
	}

	private void returnedItemMailSender(String response,JsonObject orderJsonObject) {
		JsonObject responseJsonObject = MolekuleUtility.parseJsonObject(response);
		JsonArray responseLineItem = responseJsonObject.getAsJsonArray("lineItems");
		SendGridModel sendGridModel = new SendGridModel();
		for(int i=0;i<responseLineItem.size();i++) {
			if(responseLineItem.get(i).getAsJsonObject().getAsJsonObject(CUSTOM).
					getAsJsonObject(FIELDS).get("rmaStatus").getAsString().equals("Closed")) {
				sendGridModel.setOrderNumber(responseJsonObject.get("orderNumber").getAsString());
				sendGridModel.setOrderId(responseJsonObject.get("id").getAsString());
				sendGridModel.setEmail(responseJsonObject.get("customerEmail").getAsString());
				String country = responseJsonObject.get(COUNTRY).getAsString();
				JsonArray returnInfoArray = responseJsonObject.getAsJsonArray(RETURN_INFO);
				List<Product> products = new ArrayList<>();
				for(int k=0;k<returnInfoArray.size();k++) {
					Product product = new Product();
					List<String> productDetails = new ArrayList<String>();
					if(responseLineItem.get(i).getAsJsonObject().get("id").getAsString().equals(
							returnInfoArray.get(k).getAsJsonObject().getAsJsonArray("items").get(0).
							getAsJsonObject().get(LINE_ITEM_ID).getAsString())){
						if(orderJsonObject.has(COUNTRY) && country.equals("GB")) {
							product.setOrderProductName(responseLineItem.get(i).getAsJsonObject().getAsJsonObject("name").get("en-GB").getAsString());
						}else {
							product.setOrderProductName(responseLineItem.get(i).getAsJsonObject().getAsJsonObject("name").get("en-US").getAsString());
						}
						product.setOrderProductQty(returnInfoArray.get(k).getAsJsonObject().getAsJsonArray("items").get(0).
								getAsJsonObject().get("quantity").getAsLong());
						productDetails = orderHelperService.getProductImage(responseLineItem.get(i).getAsJsonObject().get("productId").getAsString(),country,
								orderJsonObject.getAsJsonObject(CUSTOM).getAsJsonObject("fields").get(CHANNEL).getAsString());
						if(productDetails.size() > 1) {
							product.setOrderImageUrl(productDetails.get(0));
							product.setOrderProductDesc(productDetails.get(1));
						}
						products.add(product);
						sendGridModel.setProducts(products);
						JsonObject paymentJson = orderJsonObject.getAsJsonObject(PAYMENT_INFO).getAsJsonArray(PAYMENTS).get(0).getAsJsonObject();
						if(paymentJson.getAsJsonObject("obj").getAsJsonObject(PAYMENT_METHOD_INFO).get("method").getAsString().equals("CREDIT_CARD")) {
							sendGridModel.setCreditToCC(true);
						}else if(paymentJson.getAsJsonObject("obj").getAsJsonObject(PAYMENT_METHOD_INFO).get("method").getAsString().equals("AFFIRM")) {
							sendGridModel.setCreditToCC(false);
						}
						JsonObject fieldJson = orderJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
						sendGridModel.setOrderSummarySubtotal(centToDollarConversion(
								fieldJson.get(SUB_TOTAL).getAsLong(),country));
						if(country.equals("US") && fieldJson.has(TOTAL_TAX)) {
							sendGridModel.setOrderSummaryTax(centToDollarConversion(
									fieldJson.get(TOTAL_TAX).getAsLong(),country));
						}
						sendGridModel.setOrderSummaryShipping(centToDollarConversion(
								fieldJson.get(SHIPPING_COST).getAsLong(),country));
						if(country.equals("US") && fieldJson.has(TOTAL_TAX)) {
							sendGridModel.setReturnTotal(centToDollarConversion(
									fieldJson.get(SUB_TOTAL).getAsLong()+fieldJson.get(TOTAL_TAX).getAsLong()+fieldJson.get(SHIPPING_COST).getAsLong(),country));
						}else {
							sendGridModel.setReturnTotal(centToDollarConversion(
									fieldJson.get(SUB_TOTAL).getAsLong()+fieldJson.get(SHIPPING_COST).getAsLong(),country));

						}
						sendGridModel.setChannel(orderJsonObject.getAsJsonObject(CUSTOM).getAsJsonObject(FIELDS).get(CHANNEL).getAsString());
						orderReturnedItemMailSender.sendMail(sendGridModel);
					}
				}
			}
		}
	}


	public String validateSerialNumber(String serialNumber) {
		String nsUrl = ctEnvProperties.getOAuthBaseUrl()+ "?script=" + ctEnvProperties.getOAuthSerialNumberValidationScriptId() 
		+"&deploy=" + ctEnvProperties.getOAuthSerialNumberValidationDeploymentId();
		SerialNumberValidationBean serialNumberValidationBean = new SerialNumberValidationBean();
		serialNumberValidationBean.setSerialNumber(serialNumber);		
		return netConnectionHelper.postNetSuitAPI(nsUrl, serialNumberValidationBean);
	}

	public String centToDollarConversion(long centPayment,String countryCode ) {
		NumberFormat numberFormat = null;
		if(countryCode.equals("US")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		}else if(countryCode.equals("GB")){
			numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		}
		return numberFormat.format(centPayment / 100.0);
	}
}
