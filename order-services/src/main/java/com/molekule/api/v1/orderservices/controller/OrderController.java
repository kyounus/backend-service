package com.molekule.api.v1.orderservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.dto.order.OrderRefundRequestBean;
import com.molekule.api.v1.commonframework.model.order.OrderReturnRequestBean;
import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean;
import com.molekule.api.v1.commonframework.model.order.ReprocessOrderDTO;
import com.molekule.api.v1.commonframework.model.order.SortActionEnum;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.service.sns.NotificationHelperService;
import com.molekule.api.v1.commonframework.util.OrderServiceUtility.RMASTATUS;
import com.molekule.api.v1.orderservices.service.OrderService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/v1")
public class OrderController {

	Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	@Qualifier("orderService")
	OrderService orderService;

	@Autowired
	@Qualifier("notificationHelperService")
	NotificationHelperService notificationHelperService;

	/**
	 * getOrderByOrderId method is used to display the Order Data by orderId.
	 * 
	 * @param orderId
	 * 
	 * @return String - it contain order data.
	 */
	@GetMapping(value = "/users/orders/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Order Details")
	public ResponseEntity<Object> getOrderByOrderId(
			@ApiParam(value = "The Order id", required = true) @PathVariable("orderId") String orderId) {
		return orderService.getOrderByOrderId(orderId);
	}

	/**
	 * getAllOrdersByCustomerId method is used to get the list of all orders by
	 * customerId.
	 * 
	 * @param customerId
	 * @param limit
	 * @param offset
	 * @return OrderSummaryResponseBean.
	 */
	@GetMapping(value = "/users/{customerId}/orders", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get All Orders By Customer Id")
	public OrderSummaryResponseBean getAllOrdersByCustomerId(
			@ApiParam(value = "The Customer Id is", required = false) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "The limit is", required = false) @RequestParam(defaultValue = "5") String limit,
			@ApiParam(value = "The offset is", example = "0", required = true) @RequestParam int offset) {
		return orderService.getAllOrders(customerId, Integer.valueOf(limit), offset, null, null, null, null, null,null, null);
	}

	/**
	 * getAllOrders method is used to get the list of all orders.
	 * 
	 * @param limit
	 * @param offset
	 * @param email
	 * @param orderNumber
	 * @return
	 */
	@GetMapping(value = "/users/orders", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get All Orders")
	public OrderSummaryResponseBean getAllOrders(
			@ApiParam(value = "The limit is", required = false) @RequestParam(defaultValue = "5") String limit,
			@ApiParam(value = "The offset is", example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "The email is", required = false) @RequestParam(defaultValue = "") String email,
			@ApiParam(value = "Order number", required = false) @RequestParam(defaultValue = "") String orderNumber,
			@ApiParam(value = "Channel:D2C or B2B", required = false) @RequestParam(defaultValue = "") String channel,
			@ApiParam(value = "Is Gift Order", required = false) @RequestParam(defaultValue = "") Boolean isGift,
			@ApiParam(value = "Sort Action", required = false) @RequestParam(defaultValue = "") SortActionEnum sortAction,
			@ApiParam(value = "Sort Order", required = false) @RequestParam(defaultValue = "") SortOrderEnum sortOrder,
			@ApiParam(value = "Gift Receipt mail", required = false) @RequestParam(defaultValue = "") String giftReceiptMail) {
		return orderService.getAllOrders(null, Integer.valueOf(limit), offset, email, orderNumber,isGift, channel, sortAction, sortOrder, giftReceiptMail);
	}

	/**
	 * 
	 * @param storeLocation
	 * @return
	 */
	@GetMapping(value = "/users/ordernumber/{store_location}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Order Number Based on the Store Location")
	public String getOrderNumber(
			@ApiParam(value = "Store location", required = true) @PathVariable("store_location") String storeLocation) {
		return orderService.getOrderNumber(storeLocation);
	}

	/**
	 * 
	 * @param orderId
	 * @return
	 */

	@GetMapping(value = "/users/orderId/readStatus/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get read status of the Order")
	public String getOrderReadStatus(
			@ApiParam(value = "Order ID", required = true) @PathVariable("orderId") String orderId) {
		return orderService.getOrderReadStatus(orderId);
	}

	/**
	 * 
	 * @param orderId
	 * @param readFlag
	 * @return
	 */

	@PostMapping(value = "/users/orderId/readStatus/{orderId}/{readFlag}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update read status of the Order")
	public String updateOrderReadStatus(
			@ApiParam(value = "Order ID", required = true) @PathVariable("orderId") String orderId,
			@ApiParam(value = "Read Flag", required = true) @PathVariable("readFlag") Boolean readFlag) {
		return orderService.updateOrderReadStatus(orderId, readFlag);
	}

	@PostMapping(value = "/orders/event-notification", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Order Shipped Mail notification")
	public String purchaseEvent(HttpServletRequest serverHttpRequest, HttpServletResponse serverHttpResponse) {
		String response = notificationHelperService.getSNSResponse(serverHttpRequest);
		if (StringUtils.hasText(response))
			logger.info("Response Body : {}", response);
		else
			logger.info("NULL RESPONSE BODY");
		return response;
	}
	/**
	 * reprocessOrderList method is used to get all the reprocessed list from DynamoDb.
	 * @return List<ReprocessOrderDTO>
	 */
	@GetMapping(value = "/orders/reprocess", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get All ReprocessOrderList")
	public List<ReprocessOrderDTO> reprocessOrderList(@ApiParam(value = "The Status is", required = false) @RequestParam(required = false) String status,
			@RequestParam(required = false, name = "createdDateRange")  String createdDateRange,
			@RequestParam(required = false, name = "orderNumberRange")  String orderNumberRange) {
		return orderService.getReprocessOrderList(status,createdDateRange,orderNumberRange );
	}
	/**
	 * reprocessOrders method is used to update the status in reprocess order list.
	 * @param reprocessOrderDTO
	 * @param status
	 * @return List<ReprocessOrderDTO>
	 */
	@PostMapping(value = "/orders/reprocess", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update ReprocessOrderList")
	public List<ReprocessOrderDTO> reprocessOrders(@ApiParam(value = "Order Number", required = true) @RequestParam List<String> orderNumberList,
			@ApiParam(value = "The Status is", required = true) @RequestParam String status) {
		return orderService.updateReprocessOrder(orderNumberList,status);
	}

	/**
	 * getOrderTags method is used to display the Order tags on MC.
	 * 
	 * 
	 * @return String - it contain order tags custom response object.
	 */
	@GetMapping(value = "/orders/tags", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Order Tags")
	public String getOrderTags() {
		return orderService.getOrderTags();
	}
	/**
	 * getRMAInitiationValues method is used to get the RMA Initiation value from CT.
	 * 
	 * @return String
	 */
	@GetMapping(value = "/users/orders/rma-initiation", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get RMAInitiation Value")
	public String getRMAInitiationValues() {
		return orderService.getRMAInitiationValues();
	}

	@PostMapping(value = "/users/orders/{orderId}/return", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Order Return Request")
	public String processOrderReturn(
			@ApiParam(value = "Order ID", required = true) @PathVariable("orderId") String orderId,
			@ApiParam(value = "Order Return RequestBean", required = true) @RequestBody List<OrderReturnRequestBean> orderReturnRequestBeanList) {
		return orderService.processOrderReturn(orderId, orderReturnRequestBeanList);
	}

	/**
	 * processOrderReturn method is used to process the order return.
	 * 
	 * @return String
	 */
	@PostMapping(value = "/users/orders/{orderId}/refund")
	@ApiOperation(value = "Process Order Refund")
	public ResponseEntity<String> processOrderRefund(
			@ApiParam(value = "Order ID", required = true) @PathVariable("orderId") String orderId,
			@ApiParam(value = "Order Refund Request Bean", required = true) @RequestBody OrderRefundRequestBean orderRefundRequestBean) {
		return orderService.processOrderRefund(orderId, orderRefundRequestBean);
	}

	@PostMapping(value = "/users/orders/{orderId}/cancel")
	@ApiOperation(value = "Order cancel Request")
	public ResponseEntity<Object> orderCancel(
			@ApiParam(value = "Order ID", required = true) @PathVariable("orderId") String orderId) {
		return orderService.orderCancel(orderId);
	}

	@PostMapping(value = "/users/orders/{orderId}/rma-status")
	@ApiOperation(value = "Update RMA Status")
	public String updateRMAStatusToOrder (
			@ApiParam(value = "Order ID", required = true) @PathVariable("orderId") String orderId,
			@ApiParam(value = "RMA Status", required = true) @RequestParam RMASTATUS rmaStatus,
			@ApiParam(value = "LineItemId is", required = true) @RequestParam List<String> lineItemIdList) {
		return orderService.updateRMAStatusToOrder (orderId,lineItemIdList,rmaStatus);
	}
	
	@PostMapping(value = "/users/orders/serialNumber/{serialNumber}")
	@ApiOperation(value = "Validate Serial Number")
	public String validateSerialNumber (
			@ApiParam(value = "Serial Number", required = true) @PathVariable("serialNumber") String serialNumber) {
		return orderService.validateSerialNumber (serialNumber);
	}
}
