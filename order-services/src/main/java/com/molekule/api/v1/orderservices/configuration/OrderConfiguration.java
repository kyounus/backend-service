package com.molekule.api.v1.orderservices.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.mail.sendgrid.AutoRefillConfirmationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.CustomerEmailUpdateMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.FreightInformationConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderCancelMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderProcessedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderRefundMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderReturnConfirmationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderReturnedItemMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderShippedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderUpdatedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentReceivedConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.QuoteRequestConfirmationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.QuoteRequestPendingApprovalMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.SubscriptionPaymentFailedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.SubscriptionSignupConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.cart.FreightHandlingCostCalculationServiceImpl;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostCalculationService;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostHelperService;
import com.molekule.api.v1.commonframework.service.cart.ParcelHandlingCostCalculationServieImpl;
import com.molekule.api.v1.commonframework.service.cart.TaxService;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.order.AdyenPaymentHeplerService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.AuthenticationHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.CustomerProfileHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.sns.NotificationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.EncryptUtil;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The class MolekuleBeanConfiguration is used to configure the beans for the
 * application.
 */
@Configuration
@EnableSwagger2
//@PropertySource(value = ("${common.application.properties}"))
public class OrderConfiguration {

	@Value("${ct.aws.access.key.id}")
	private String awsAccessKeyId;

	@Value("${ct.aws.access.secret.key}")
	private String awsSecretKey;

	@Value("${ct.aws.region}")
	private String awsRegion;
	
    @Value("${devHost}")
    private String devHost;
    
    @Value("${qaHost}")
    private String qaHost;
    @Value("${ukHost}")
    private String ukHost;
    @Value("${profile}")
    private String profile;
    
    @Value("${ct.cognitoUserPool}")
	private String cognitoUserPool;
    
    @Value("${ct.cognitoClientId}")
	private String cognitoClientId;
    
    @Value("${ct.cognitoClientSecret}")
	private String cognitoClientSecret;

	@Bean(name = "awsCredentialsProvider")
	public AWSCredentialsProvider getAWSCredentials() {
		return new AWSStaticCredentialsProvider(new BasicAWSCredentials(EncryptUtil.decrypt(this.awsAccessKeyId), EncryptUtil.decrypt(this.awsSecretKey)));
	}

	@Bean(name = "dynamoDB")
	public DynamoDB getDynamoDB() {
		return new DynamoDB(getAmazonDynamoDB());

	}
	
	@Bean(name = "amazonDynamoDB")
	public AmazonDynamoDB getAmazonDynamoDB() {
		return AmazonDynamoDBClientBuilder.standard().withRegion(awsRegion)
				.withCredentials(getAWSCredentials()).build();
	}
	
	/**
	 * getDocket bean is written to get all apis in the swagger ui.
	 * 
	 * @return Docket
	 */
	@Bean
	public Docket getDocket() {
		  String host = null;	  
		   if("dev".equals(profile)) {
			   host = devHost;
		   }else if("qa".equals(profile)){
			   host = qaHost;
		   }else if("uk".equals(profile)){
			   host = ukHost;
		   }
		Docket swaggerDoc = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).build();
		if(host!= null) {
			swaggerDoc.host(host);
		}
		return swaggerDoc.apiInfo(getApiInfo());
	}

	/**
	 * crosConfigure bean is written to configure the cross platform applications.
	 * 
	 * @return WebMvcConfigurer
	 */
	@Bean
	public WebMvcConfigurer crosConfigure() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST").allowedOrigins("*").allowedHeaders("*");
				WebMvcConfigurer.super.addCorsMappings(registry);
			}

		};
	}

	/**
	 * getApiInfo method is used to get the api information.
	 * 
	 * @return ApiInfo
	 */
	public ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("Molekule Integration Api's For Order Services")
				.description("exposed integration api's from molekule to ct").build();
	}

	@Bean
	public CTEnvProperties cTEnvProperties() {
		return new CTEnvProperties();
	}

	@Bean
	public NetConnectionHelper netConnectionHelper() {
		return new NetConnectionHelper();
	}

	@Bean
	public CtServerHelperService ctServerHelperService() {
		return new CtServerHelperService();
	}

	@Bean
	public RegistrationHelperService registrationHelperService() {
		return new RegistrationHelperService();
	}

	@Bean
	public CheckoutHelperService checkoutHelperService() {
		return new CheckoutHelperService();
	}

	@Bean
	public OrderHelperService orderHelperService() {
		return new OrderHelperService();
	}

	@Bean
	public CartHelperService cartHelperService() {
		return new CartHelperService();
	}

	@Bean
	public TaxService taxService() {
		return new TaxService();
	}

	@Bean
	public HandlingCostHelperService handlingCostHelperService() {
		return new HandlingCostHelperService();
	}

	@Bean("handlingCostCalculationService")
	public HandlingCostCalculationService parcelHandlingCostCalculationServieImpl() {
		return new ParcelHandlingCostCalculationServieImpl();
	}

	@Bean("handlingCostCalculationService")
	public HandlingCostCalculationService freightHandlingCostCalculationServiceImpl() {
		return new FreightHandlingCostCalculationServiceImpl();
	}

	@Bean
	public TealiumHelperService tealiumHelperService() {
		return new TealiumHelperService();
	}

	@Bean
	public TemplateMailRequestHelper quoteRequestConfirmationMailSender() {
		return new QuoteRequestConfirmationMailSender();
	}

	@Bean
	public TemplateMailRequestHelper quoteRequestPendingApprovalMailSender() {
		return new QuoteRequestPendingApprovalMailSender();
	}

	@Bean
	public TemplateMailRequestHelper paymentReceivedConfirmationSender() {
		return new PaymentReceivedConfirmationSender();
	}

	@Bean
	public NotificationHelperService notificationHelperService() {
		return new NotificationHelperService();
	}

	@Bean
	public TemplateMailRequestHelper orderShippedMailSender() {
		return new OrderShippedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper subscriptionSignupConfirmationSender() {
		return new SubscriptionSignupConfirmationSender();
	}
	
	@Bean
	public TemplateMailRequestHelper autoRefillConfirmationMailSender() {
		return new AutoRefillConfirmationMailSender();
	}
	
	@Bean
	public AuroraHelperService auroraHelperService() {
		return new AuroraHelperService();
	}
	
	@Bean
	public TemplateMailRequestHelper orderUpdatedMailSender() {
		return new OrderUpdatedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper subscriptionPaymentFailedMailSender() {
		return new SubscriptionPaymentFailedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderProcessedMailSender() {
		return new OrderProcessedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper freightInformationConfirmationSender() {
		return new FreightInformationConfirmationSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderCancelMailSender() {
		return new OrderCancelMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderReturnConfirmationMailSender() {
		return new OrderReturnConfirmationMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderRefundMailSender() {
		return new OrderRefundMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderReturnedItemMailSender() {
		return new OrderReturnedItemMailSender();
	}
	
	@Bean
	public AuthenticationHelperService authenticationHelperService() {
		if(com.amazonaws.util.StringUtils.isNullOrEmpty(cognitoClientSecret)) {
			return new AuthenticationHelperService(cognitoUserPool, cognitoClientId, null);			
		} else {
			return new AuthenticationHelperService(cognitoUserPool, cognitoClientId, cognitoClientSecret);
		}
	}
	
	@Bean
	public CustomerProfileHelperService customerProfileHelperService() {
		return new CustomerProfileHelperService();
	}
	
	@Bean
	public CustomerEmailUpdateMailSender customerEmailUpdateMailSender(){
		return new CustomerEmailUpdateMailSender();
	}
	
	@Bean
	public AdyenPaymentHeplerService adyenPaymentHeplerService() {
		return new AdyenPaymentHeplerService();
	}
}
