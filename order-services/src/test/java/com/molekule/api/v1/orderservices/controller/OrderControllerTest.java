package com.molekule.api.v1.orderservices.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.molekule.api.v1.orderservices.service.OrderService;

public class OrderControllerTest {
	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	OrderController orderController;

	@Mock
	OrderService orderSerive;

	@Test
	public void getOrderByOrderId() {
		Assert.assertNull(orderController.getOrderByOrderId("orderId"));
	}

	@Test
	public void getAllOrdersByCustomerId() {
		Assert.assertNull(orderController.getAllOrdersByCustomerId("test_consumer_id", "10", 10));
	}

	@Test
	public void getAllOrders() {
		Assert.assertNull(
				orderController.getAllOrders("10", 10, "testEmail", "orderNumber", null, null, null, null, null));
	}

	@Test
	public void getOrderNumber() {
		Assert.assertNull(orderController.getOrderNumber("storeLocation"));
	}

	@Test
	public void getOrderReadStatus() {
		Assert.assertNull(orderController.getOrderReadStatus("orderId"));
	}

	@Test
	public void updateOrderReadStatus() {
		Assert.assertNull(orderController.updateOrderReadStatus("orderId", true));
	}

}