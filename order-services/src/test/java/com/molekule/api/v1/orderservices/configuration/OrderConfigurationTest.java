package com.molekule.api.v1.orderservices.configuration;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class OrderConfigurationTest {
	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	OrderConfiguration orderConfiguration;

	@Test
	public void getDocket() {
		Assert.assertNotNull(orderConfiguration.getDocket());
	}

	@Test
	public void crosConfigure() {
		Assert.assertNotNull(orderConfiguration.crosConfigure());
	}

	public void getApiInfo() {
		Assert.assertNotNull(orderConfiguration.getApiInfo());
	}

	@Test
	public void cTEnvProperties() {
		Assert.assertNotNull(orderConfiguration.cTEnvProperties());
	}

	@Test
	public void netConnectionHelper() {
		Assert.assertNotNull(orderConfiguration.netConnectionHelper());
	}

	@Test
	public void ctServerHelperService() {
		Assert.assertNotNull(orderConfiguration.ctServerHelperService());
	}

	@Test
	public void registrationHelperService() {
		Assert.assertNotNull(orderConfiguration.registrationHelperService());
	}

	@Test
	public void checkoutHelperService() {
		Assert.assertNotNull(orderConfiguration.checkoutHelperService());
	}

	@Test
	public void orderHelperService() {
		Assert.assertNotNull(orderConfiguration.orderHelperService());
	}

	@Test
	public void cartHelperService() {
		Assert.assertNotNull(orderConfiguration.cartHelperService());
	}

	@Test
	public void taxService() {
		Assert.assertNotNull(orderConfiguration.taxService());
	}

	@Test
	public void handlingCostHelperService() {
		Assert.assertNotNull(orderConfiguration.handlingCostHelperService());
	}

	@Test
	public void parcelHandlingCostCalculationServieImpl() {
		Assert.assertNotNull(orderConfiguration.parcelHandlingCostCalculationServieImpl());
	}

	@Test
	public void freightHandlingCostCalculationServiceImpl() {
		Assert.assertNotNull(orderConfiguration.freightHandlingCostCalculationServiceImpl());
	}

	@Test
	public void tealiumHelperService() {
		Assert.assertNotNull(orderConfiguration.tealiumHelperService());
	}

	@Test
	public void quoteRequestConfirmationMailSender() {
		Assert.assertNotNull(orderConfiguration.quoteRequestConfirmationMailSender());
	}

	@Test
	public void quoteRequestPendingApprovalMailSender() {
		Assert.assertNotNull(orderConfiguration.quoteRequestPendingApprovalMailSender());
	}

	@Test
	public void paymentReceivedConfirmationSender() {
		Assert.assertNotNull(orderConfiguration.paymentReceivedConfirmationSender());
	}
}