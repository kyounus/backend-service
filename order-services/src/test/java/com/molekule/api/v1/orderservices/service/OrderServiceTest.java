package com.molekule.api.v1.orderservices.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.commonframework.util.StoreLocationType;

public class OrderServiceTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	OrderService orderService;

	@Mock
	Logger logger;

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	NetConnectionHelper netConnectionHelper;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	OrderHelperService orderHelperService;

	private final String ORDER_RESPONSE = "{\"type\":\"Order\",\"id\":\"3d473370-d51b-482f-bdc9-9f88913ff7ce\",\"version\":3,\"lastMessageSequenceNumber\":3,\"createdAt\":\"2021-05-25T11:32:14.771Z\",\"lastModifiedAt\":\"2021-05-25T11:32:18.674Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"orderNumber\":\"3000001021\",\"customerId\":\"ae919c0d-89f3-4d7b-a2b5-d20815e41275\",\"customerEmail\":\"sudalai.manickam20@gmail.com\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":104810,\"fractionDigits\":2},\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":104810,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":112932,\"fractionDigits\":2},\"taxPortions\":[{\"rate\":0.06,\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":6288,\"fractionDigits\":2},\"name\":\"CA STATE TAX\"},{\"rate\":0.0025,\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":262,\"fractionDigits\":2},\"name\":\"CA COUNTY TAX\"},{\"rate\":0.01,\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":1048,\"fractionDigits\":2},\"name\":\"CA SPECIAL TAX\"},{\"rate\":0.005,\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":524,\"fractionDigits\":2},\"name\":\"CA SPECIAL TAX\"}]},\"country\":\"US\",\"orderState\":\"Confirmed\",\"syncInfo\":[],\"returnInfo\":[],\"state\":{\"typeId\":\"state\",\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\",\"obj\":{\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\",\"version\":3,\"createdAt\":\"2021-05-04T13:34:52.825Z\",\"lastModifiedAt\":\"2021-05-18T14:01:55.561Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-processing\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Processing\"},\"description\":{\"en\":\"Processing\"},\"builtIn\":false,\"transitions\":[{\"typeId\":\"state\",\"id\":\"c4da8c79-a751-429f-bb93-0332e7d8b5dd\"},{\"typeId\":\"state\",\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\"},{\"typeId\":\"state\",\"id\":\"c66111c7-92f3-4145-9d3d-4b6a0c6e00e9\"}],\"initial\":false}},\"taxMode\":\"ExternalAmount\",\"inventoryMode\":\"None\",\"taxRoundingMode\":\"HalfEven\",\"taxCalculationMode\":\"LineItemLevel\",\"origin\":\"Customer\",\"lineItems\":[{\"id\":\"de79a36b-3d93-495d-8205-4940a96f1fbf\",\"productId\":\"27355a9d-f2ab-4c2b-990a-a9486f58d972\",\"name\":{\"en-US\":\"Molekule Air Pro\"},\"productType\":{\"typeId\":\"product-type\",\"id\":\"f62e5a83-fbf1-4f9d-aced-b3169aa99757\"},\"productSlug\":{\"en-US\":\"molekule-air-pro\"},\"variant\":{\"id\":1,\"sku\":\"SQ2P-US\",\"key\":\"1620236\",\"prices\":[{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":119900,\"fractionDigits\":2},\"id\":\"3524cce3-4c0b-45b5-94f7-6fee4abf47ee\",\"discounted\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":114900,\"fractionDigits\":2},\"discount\":{\"typeId\":\"product-discount\",\"id\":\"0a60d6dc-2773-47e3-a28a-f83986a01798\"}}},{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":119900,\"fractionDigits\":2},\"id\":\"eaec2983-6ab3-437a-a79a-7210fc88cfe7\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"tiers\":[{\"minimumQuantity\":5,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":111000,\"fractionDigits\":2}},{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":99000,\"fractionDigits\":2}}],\"discounted\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":114900,\"fractionDigits\":2},\"discount\":{\"typeId\":\"product-discount\",\"id\":\"0a60d6dc-2773-47e3-a28a-f83986a01798\"}}}],\"images\":[],\"attributes\":[{\"name\":\"b2bView\",\"value\":true},{\"name\":\"b2cView\",\"value\":false},{\"name\":\"sellable\",\"value\":true},{\"name\":\"standardCost\",\"value\":[{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":50000,\"fractionDigits\":2}]},{\"name\":\"upcCode\",\"value\":818701020353},{\"name\":\"length\",\"value\":13.6},{\"name\":\"width\",\"value\":14},{\"name\":\"height\",\"value\":25.75},{\"name\":\"weight\",\"value\":37},{\"name\":\"b2bRequired\",\"value\":true},{\"name\":\"SubscriptionIdentifier\",\"value\":{\"typeId\":\"product\",\"id\":\"ab858042-d452-4e99-9d78-06afd6ee00b1\"}}],\"assets\":[],\"availability\":{\"isOnStock\":true,\"availableQuantity\":1074}},\"price\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":119900,\"fractionDigits\":2},\"id\":\"eaec2983-6ab3-437a-a79a-7210fc88cfe7\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"tiers\":[{\"minimumQuantity\":5,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":111000,\"fractionDigits\":2}},{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":99000,\"fractionDigits\":2}}],\"discounted\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":114900,\"fractionDigits\":2},\"discount\":{\"typeId\":\"product-discount\",\"id\":\"0a60d6dc-2773-47e3-a28a-f83986a01798\"}}},\"quantity\":1,\"discountedPrice\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":103410,\"fractionDigits\":2},\"includedDiscounts\":[{\"discount\":{\"typeId\":\"cart-discount\",\"id\":\"a3b3ae29-79ea-4680-ac85-3c94fad48ba4\"},\"discountedAmount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11490,\"fractionDigits\":2}}]},\"discountedPricePerQuantity\":[{\"quantity\":1,\"discountedPrice\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":103410,\"fractionDigits\":2},\"includedDiscounts\":[{\"discount\":{\"typeId\":\"cart-discount\",\"id\":\"a3b3ae29-79ea-4680-ac85-3c94fad48ba4\"},\"discountedAmount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11490,\"fractionDigits\":2}}]}}],\"taxRate\":{\"name\":\"tax\",\"amount\":0.0775,\"includedInPrice\":false,\"country\":\"US\",\"subRates\":[]},\"addedAt\":\"2021-05-25T10:42:03.584Z\",\"lastModifiedAt\":\"2021-05-25T10:42:03.584Z\",\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}],\"priceMode\":\"Platform\",\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":103410,\"fractionDigits\":2},\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":103410,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":111424,\"fractionDigits\":2}},\"lineItemMode\":\"Standard\"},{\"id\":\"4d8d6b1c-0e9f-4525-b27a-5912105299f7\",\"productId\":\"ab858042-d452-4e99-9d78-06afd6ee00b1\",\"name\":{\"en-US\":\"AirSubscription\"},\"productType\":{\"typeId\":\"product-type\",\"id\":\"9160d8e4-9cc1-422f-9b00-1bd688de51c3\"},\"productSlug\":{\"en-US\":\"airsubscription\"},\"variant\":{\"id\":1,\"sku\":\"MAS-123\",\"prices\":[{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":29900,\"fractionDigits\":2},\"id\":\"72aa3f7f-a649-413e-b0aa-40e1800ac05f\"}],\"images\":[],\"attributes\":[{\"name\":\"purifierReferenceSku\",\"value\":{\"typeId\":\"product\",\"id\":\"27355a9d-f2ab-4c2b-990a-a9486f58d972\"}},{\"name\":\"filterProduct\",\"value\":{\"typeId\":\"product\",\"id\":\"8a4b28c5-a0e3-4afe-a547-c1d4680282ba\"}},{\"name\":\"b2cView\",\"value\":false},{\"name\":\"b2bView\",\"value\":true},{\"name\":\"sellable\",\"value\":true},{\"name\":\"period\",\"value\":6},{\"name\":\"firstDeliveryDays\",\"value\":180},{\"name\":\"StoreID\",\"value\":{\"key\":\"MLK US\",\"label\":\"MLK US\"}}],\"assets\":[]},\"price\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2},\"id\":\"28028a13-baf5-417f-8f09-e9250484dfe3\"},\"quantity\":1,\"discountedPricePerQuantity\":[],\"taxRate\":{\"name\":\"myTaxRate\",\"amount\":0.0,\"includedInPrice\":false,\"country\":\"US\",\"subRates\":[]},\"addedAt\":\"2021-05-25T10:42:05.215Z\",\"lastModifiedAt\":\"2021-05-25T10:42:05.215Z\",\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}],\"priceMode\":\"ExternalTotal\",\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2},\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2}},\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"6d2ca957-ce13-46e5-8d52-3f8267b07263\"},\"fields\":{\"subscriptionEnabled\":true}},\"lineItemMode\":\"Standard\"}],\"customLineItems\":[{\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":1400,\"fractionDigits\":2},\"id\":\"aeef3d73-4da7-4e8d-9cfa-bf438ed2d8c2\",\"name\":{\"en\":\"Handling Cost\"},\"money\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":1400,\"fractionDigits\":2},\"slug\":\"handling_cost\",\"quantity\":1,\"discountedPricePerQuantity\":[],\"taxRate\":{\"name\":\"tax\",\"amount\":0.0775,\"includedInPrice\":false,\"country\":\"US\",\"subRates\":[]},\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}],\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":1400,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":1508,\"fractionDigits\":2}}}],\"transactionFee\":true,\"discountCodes\":[],\"cart\":{\"typeId\":\"cart\",\"id\":\"cbc205bc-63d7-45c7-9b51-0adbd5c7779f\"},\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"937cb0c5-55a8-4c88-af52-3bcb8c53a04c\"},\"fields\":{\"subTotal\":\"103410\",\"shippingCost\":\"0\",\"accountId\":\"da671e83-ff59-498f-bdf3-bc07ce4cc62c\",\"handlingCost\":\"1400\",\"totalTax\":\"8122\",\"orderTotal\":\"112932\",\"holds\":[\"FreightHold\"]}},\"paymentInfo\":{\"payments\":[{\"typeId\":\"payment\",\"id\":\"eb1f3589-08cf-4735-bf8f-9d6929f7731c\",\"obj\":{\"id\":\"eb1f3589-08cf-4735-bf8f-9d6929f7731c\",\"version\":3,\"lastMessageSequenceNumber\":2,\"createdAt\":\"2021-05-25T11:32:08.986Z\",\"lastModifiedAt\":\"2021-05-25T11:32:16.323Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"interfaceId\":\"pi_1IuyUV2eZvKYlo2CaSkG3Juz\",\"amountPlanned\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":112932,\"fractionDigits\":2},\"paymentMethodInfo\":{\"paymentInterface\":\"STRIPE\",\"method\":\"CREDIT_CARD\",\"name\":{\"en\":\"Credit Card\"}},\"paymentStatus\":{\"interfaceText\":\"Success\"},\"transactions\":[{\"id\":\"c2287d0f-3e70-4b0e-b6ef-5a7df36e003e\",\"type\":\"Charge\",\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":112932,\"fractionDigits\":2},\"state\":\"Success\"}],\"interfaceInteractions\":[]}}]},\"shippingAddress\":{\"id\":\"e621caed-ede1-489d-b343-3b3c3b3ac95f\",\"firstName\":\"string\",\"lastName\":\"string\",\"streetName\":\"2000 Main Street\",\"streetNumber\":\"string\",\"postalCode\":\"92614\",\"city\":\"Ivrine\",\"region\":\"CA\",\"state\":\"CA\",\"country\":\"US\",\"company\":\"shipping address company name\",\"phone\":\"string\"},\"itemShippingAddresses\":[],\"refusedGifts\":[],\"store\":{\"typeId\":\"store\",\"key\":\"MLK_US\"}}\r\n";

	private final String CUSTOMER_RESPONSE = "{\"id\":\"ae919c0d-89f3-4d7b-a2b5-d20815e41275\",\"version\":3,\"lastMessageSequenceNumber\":2,\"createdAt\":\"2021-05-25T10:36:58.889Z\",\"lastModifiedAt\":\"2021-05-25T10:36:59.287Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"customerNumber\":\"168812\",\"email\":\"sudalai.manickam20@gmail.com\",\"firstName\":\"Sudalai\",\"lastName\":\"manickam\",\"companyName\":\"Test Company\",\"password\":\"vVJrPbICc+p2nWQ9A+/uKcVDgq9ank+j+neDO4HeR30=$87ZJ/3m9qsGfqtDeaoZqmk+zx0CCLNsnugZnIDwdb1I=\",\"addresses\":[],\"shippingAddressIds\":[],\"billingAddressIds\":[],\"isEmailVerified\":false,\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"key\":\"ae919c0d-89f3-4d7b-a2b5-d20815e41275\",\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"de4181c9-24f4-4877-a4be-8b2e04b3f2e1\"},\"fields\":{\"admin\":false,\"accountLocked\":false,\"customerGroupName\":\"MO_Registered\",\"invoiceEmailAddress\":[\"sudalai.manickam20@gmail.com\"],\"accountId\":\"da671e83-ff59-498f-bdf3-bc07ce4cc62c\",\"salutation\":\"\",\"phone\":\"123456789\"}},\"stores\":[]}\r\n";

	private final String ORDER_URL = "null/null/orders/orderId?expand=paymentInfo.payments[*].id&expand=state.id";

	private final String CUSTOMER_URL = "null/null/customers/ae919c0d-89f3-4d7b-a2b5-d20815e41275";

	private final String ACCOUNT_RESPONSE = "{\"limit\":20,\"offset\":0,\"count\":1,\"total\":1,\"results\":[{\"id\":\"da671e83-ff59-498f-bdf3-bc07ce4cc62c\",\"version\":4,\"createdAt\":\"2021-05-25T10:36:57.772Z\",\"lastModifiedAt\":\"2021-06-01T09:56:20.430Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"TestCompany395Sudalai\",\"value\":{\"companyName\":\"Test Company\",\"parcelFlag\":true,\"freightFlag\":true,\"employeeIds\":[\"ae919c0d-89f3-4d7b-a2b5-d20815e41275\"],\"taxExemptFlag\":false,\"myOwnCarrier\":true,\"companyCategory\":{\"id\":\"5cb62872-fa6e-422e-80ef-dadd35ec3490\",\"name\":\"Other Healthcare\",\"sales-rep-id\":\"b779377d-0a92-48c6-a7e7-016da03f5922\"},\"salesRepresentative\":{\"id\":\"b779377d-0a92-48c6-a7e7-016da03f5922\",\"name\":\"Pavithra test\",\"phone\":\"123456789\",\"email\":\"test@gmail.com\",\"firstName\":\"Pavithra\",\"lastName\":\"test\",\"calendlyLink\":\"string\"},\"addresses\":{\"billingAddresses\":[{\"billingAddressId\":\"169ebd4a-1c62-40be-81f9-712e74da966e\",\"firstName\":\"billing Address firstNAme57\",\"lastName\":\"billing Address lastname 57\",\"streetAddress1\":\"billing Address streetAddress1 57\",\"streetAddress2\":\"billing Address streetAddress2 57\",\"city\":\"billing Address city 57\",\"state\":\"billing Address state 57\",\"postalCode\":\"billing Address postal 57\",\"phoneNumber\":\"billing Address phone 57\",\"country\":\"billing Address country 57\"}],\"shippingAddresses\":[{\"shippingAddressId\":\"78c8ae8e-0c6e-4c1c-b1cf-43dd69a329f5\",\"firstName\":\"shipping Address first Name 201\",\"lastName\":\"shipping Address last Name 201\",\"streetAddress1\":\"shipping Address streetAddress1 201\",\"streetAddress2\":\"shipping Address streetAddress2 201\",\"city\":\"shipping Address City 201\",\"state\":\"shipping Address state 201\",\"postalCode\":\"shipping Address Postal Code 201\"}]},\"carriers\":[{\"loadingDock\":true,\"forkLift\":true,\"deliveryAppointmentRequired\":true,\"receivingHours\":{\"from\":\"10 AM\",\"to\":\"12 AM\"},\"instructions\":\"instructions\",\"carrierId\":\"6f6892eb-2034-46fc-be69-7667a5635610\",\"type\":\"PARCEL\",\"carrierName\":\"carrierName 57\",\"billingAddressId\":\"169ebd4a-1c62-40be-81f9-712e74da966e\"}],\"defaultCarrierId\":\"6f6892eb-2034-46fc-be69-7667a5635610\",\"additionalFreightInfos\":[{\"loadingDock\":true,\"forkLift\":true,\"deliveryAppointmentRequired\":true,\"receivingHours\":{\"from\":\"10 AM\",\"to\":\"01 PM\"},\"instructions\":\"string\",\"shippingAddressId\":\"78c8ae8e-0c6e-4c1c-b1cf-43dd69a329f5\"}],\"defaultShippingAddressId\":\"78c8ae8e-0c6e-4c1c-b1cf-43dd69a329f5\"}}]}\r\n";

	private final String ACCOUNT_URL = "null/null/custom-objects?where=id=\"da671e83-ff59-498f-bdf3-bc07ce4cc62c\"";

	private final String ORDER_RESPONSE_URL = "null/null/orders/orderId?expand=paymentInfo.payments[*].id&expand=state.id&expand=lineItems[*].variant.attributes[*].value.typeId";

	private final String ORDER_BASE_URL = "null/null/orders/orderId?expand=paymentInfo.payments[*].id&expand=state.id&expand=customerGroup.id&expand=lineItems[*].variant.attributes[*].value.typeId";

	// @Test
	public void getOrderByOrderId() {
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", ORDER_URL)).thenReturn(ORDER_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", ACCOUNT_URL))
				.thenReturn(ACCOUNT_RESPONSE);

		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, CUSTOMER_URL)).thenReturn(CUSTOMER_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", ORDER_RESPONSE_URL))
				.thenReturn(ORDER_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", ORDER_BASE_URL))
				.thenReturn(ORDER_RESPONSE);

		orderService.getOrderByOrderId("orderId");
	}

	@Test
	public void getAllOrders() {
		Assert.assertNull(orderService.getAllOrders("customerId", 1, 1, "testEmail", "OrderNumber", null, null, null,
				null, null));
	}

	@Test
	public void getOrderNumber() {
		Assert.assertNull(orderService.getOrderNumber(StoreLocationType.MLK_CA.name()));
	}

	@Test
	public void getOrderReadStatus() {
		Assert.assertNull(orderService.getOrderReadStatus("orderId"));
	}

	@Test
	public void updateOrderReadStatus() {
		Assert.assertNull(orderService.updateOrderReadStatus("orderId", true));
	}

}
