package com.molekule.api.v1.cartservices.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.HandlerMethod;

@SuppressWarnings({ "unchecked", "rawtypes" })
@ControllerAdvice
public class CustomExceptionsHandler {

	@ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, HandlerMethod handlerMethod, HttpServletRequest request) {
		Class controllerName = handlerMethod.getMethod().getDeclaringClass();
        String methodName = handlerMethod.getMethod().getName();
        String message = "Error Got In "+controllerName+" method "+methodName+". Exception is "+ex.getMessage();
        com.molekule.api.v1.commonframework.util.ErrorResponse errorResponse = new com.molekule.api.v1.commonframework.util.ErrorResponse(400, message);
        return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }

}
