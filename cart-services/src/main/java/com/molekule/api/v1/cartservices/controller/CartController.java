package com.molekule.api.v1.cartservices.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.cartservices.service.CartService;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The class CartController is used to write cart related apis.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1")
public class CartController {

	@Autowired
	@Qualifier("cartService")
	CartService cartService;
	
	Logger logger = LoggerFactory.getLogger(CartController.class);

	/**
	 * getCartById method is used to get the cart information by passing
	 * CartId.
	 * 
	 * @param cartId
	 * @return String - it contains all product related data.
	 */
	@GetMapping(value = "/carts/{cartId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getCartById(@ApiParam(value = "The cart id", required = true) @PathVariable("cartId") String cartId) {
		return cartService.getCartById(cartId);
	}

	@PostMapping(value = "/carts",produces = MediaType.APPLICATION_JSON_VALUE)
    public String createCart(@RequestBody CreateCartRequestBean createCartRequestBean){
		return cartService.createCart(createCartRequestBean);
    }
	
	@PostMapping(value = "/carts/{cartId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update Cart by Id", response = String.class)
	public String updateCartById(@ApiParam(value = "The cart id", required = true) @PathVariable("cartId") String cartId,@RequestBody CartRequestBean cartRequestBean,
			@RequestHeader("country") String country) {
		cartRequestBean.setCountry(country);
		return cartService.updateCartById(cartId, cartRequestBean);
	}
		
	/**
	 * getAllQuotes method is used to get all the quotes object from CT.
	 * 
	 * @param limit
	 * @param offset
	 * @param quoteNumber
	 * @param email
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/quotes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getAllQuotes(
			@ApiParam(value = "The limit is",example = "20", required = true) @RequestParam int limit, 
			@ApiParam(value = "The offset is",example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "Quote number", required = false) @RequestParam(defaultValue = "") String quoteNumber,
			@ApiParam(value = "email", required = false) @RequestParam(defaultValue = "") String email) {
		return cartService.getAllQuotes(limit, offset, quoteNumber, email);
	}
	
	/**
	 * getQuotesById method is used to get the specific quotes object from CT.
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/quotes/{quoteId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getQuotesById(
			@ApiParam(value = "The quote id", required = true) @PathVariable("quoteId") String quoteId) {
		return cartService.getQuotesById(quoteId);
	}

}
