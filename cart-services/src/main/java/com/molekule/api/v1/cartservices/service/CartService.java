package com.molekule.api.v1.cartservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.API_STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.AUTHORIZATION;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.Cart;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.products.ProductVariant;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import reactor.core.publisher.Mono;
/**
 * The class CartService is used to write the api calls for the cart.
 * 
 * @version 1.0
 */
@Service("cartService")
public class CartService {

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("netConnectionHelper")
	private NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("cartHelperService")
	CartHelperService cartHelperService;

	Logger logger = LoggerFactory.getLogger(CartService.class);
	
	@Value("${taxMode}")
	private String taxMode;

	/**
	 * createCart method is used to create a by customer id.
	 * 
	 * @param createCartRequestBean
	 * @return String
	 */
	public String createCart(CreateCartRequestBean createCartRequestBean) {
		return cartHelperService.createCart(createCartRequestBean);
	}

	public TypedMoney getPricesByProductId(String productId) {
		String token = ctServerHelperService.getStringAccessToken();
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/products/").append(productId).toString();
		ProductBean productBeanResponseData = netConnectionHelper.sendGetProductDataWithoutBody(token, url);
		ProductData current = productBeanResponseData.getMasterData().getCurrent();
		ProductVariant masterData = current.getMasterVariant();
		return masterData.getPrices().get(0).getValue();

	}
	
	/**
	 * sendPostCreateCartRequest method is used to send the post request through
	 * webclient to the CT api calls.
	 * 
	 * @param baseUrl
	 * @param token
	 * @param requestData
	 * @return Cart
	 */
	public Cart sendPostCreateCartRequest(String baseUrl, String token, Object requestData) {
		WebClient webClient = WebClient.builder().baseUrl(baseUrl).build();
		Mono<Cart> response = webClient.post().header(AUTHORIZATION, token).accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(requestData), Object.class).exchangeToMono(clientResponse -> {
					logger.trace(String.format("%s%s", API_STATUS_CODE,clientResponse.statusCode()));
					return clientResponse.bodyToMono(Cart.class);
				});
		return response.block();
	}

	public String getCartById(String cartId) {
		return cartHelperService.getCartById(cartId);
	}


	public String updateCartById(String cartId, CartRequestBean cartRequestBean) {
		return cartHelperService.updateCartById(cartId, cartRequestBean);
	}

	/**
	 * getAllQuotes method is used to get all the quotes object from CT.
	 * @param offset 
	 * @param limit 
	 * @param email 
	 * @param quoteNumber 
	 * @return String
	 */
	
	public ResponseEntity<String> getAllQuotes(int limit, int offset, String quoteNumber, String email) {
		return cartHelperService.getAllQuotes(limit, offset, quoteNumber, email);
	}

	/**
	 * getQuotesById method is used to get the specific quotes object from CT.
	 * 
	 * @return ResponseEntity<String>
	 */
	public ResponseEntity<String> getQuotesById(String quoteId) {
		return cartHelperService.getAllQuotes(quoteId);
	}
}
