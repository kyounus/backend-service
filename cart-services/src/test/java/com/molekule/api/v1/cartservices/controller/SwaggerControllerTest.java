package com.molekule.api.v1.cartservices.controller;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SwaggerControllerTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	SwaggerController swaggerController;

	@Mock
	HttpServletRequest request;

	@Test
	public void swagger() {
		Assert.assertEquals("", swaggerController.swagger());
	}

}
