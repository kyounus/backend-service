package com.molekule.api.v1.cartservices.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.ResponseEntity;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartDTO;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.model.products.Price;
import com.molekule.api.v1.commonframework.model.products.ProductCatalogData;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.products.ProductVariant;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

public class CartServiceTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CartService cartService;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	RegistrationHelperService registrationHelperService;

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	NetConnectionHelper netConnectionHelper;

	@Mock
	CartHelperService cartHelperService;

	@Mock
	CreateCartRequestBean createCartRequestBean;

	@Mock
	CartRequestBean updateCartRequestBean;

	@Mock
	CustomerResponseBean customerResponseBean;

	@Mock
	CustomerGroupRequestBean.Action.CustomerGroup customerGroup;

	@Spy
	CreateCartDTO createCartDTO;

	@Mock
	ProductBean productBeanResponseData;

	@Mock
	ProductCatalogData productCatalogData;

	@Mock
	ProductData productData;

	@Mock
	ProductVariant productVariant;

	@Test
	public void createCart() {
		Assert.assertNull(cartService.createCart(createCartRequestBean));

	}

	@Test
	public void createCartCustomerIDNotNull() {
		String jsonResponse = "{\r\n" + "  \"id\": \"1234\",\r\n" + "  \"version\": \"1\"\r\n" + "}";

		BDDMockito.when(registrationHelperService.getCustomerById("1234")).thenReturn(customerResponseBean);
		BDDMockito.when(customerResponseBean.getCustomerGroup()).thenReturn(customerGroup);

		BDDMockito.when(createCartRequestBean.getCustomerId()).thenReturn("1234");

		BDDMockito.when(netConnectionHelper.sendPostRequest(Mockito.eq("null/null/carts"), Mockito.eq("Bearer null"),
				Mockito.any(CreateCartDTO.class))).thenReturn(jsonResponse);

		Assert.assertNull(cartService.createCart(createCartRequestBean));

	}

	@Test
	public void getPricesByProductId() {

		BDDMockito.when(netConnectionHelper.sendGetProductDataWithoutBody(null, "null/null/products/testProductID"))
				.thenReturn(productBeanResponseData);
		BDDMockito.when(productBeanResponseData.getMasterData()).thenReturn(productCatalogData);
		BDDMockito.when(productCatalogData.getCurrent()).thenReturn(productData);

		BDDMockito.when(productData.getMasterVariant()).thenReturn(productVariant);
		List<Price> prices = new ArrayList<>();
		prices.add(new Price());
		BDDMockito.when(productVariant.getPrices()).thenReturn(prices);

		Assert.assertNull(cartService.getPricesByProductId("testProductID"));
	}

	@Test
	public void getCartById() {
		Assert.assertNull(cartService.getCartById("carId"));
	}

	@Test
	public void updateCartById() {
		Assert.assertNull(cartService.updateCartById("cardId", updateCartRequestBean));
	}

	@Test
	public void getAllQuotes() {
		Assert.assertNull(cartService.getAllQuotes(1, 1, "quoteNumber", "email"));
	}

	@Test
	public void getQuotesById() {
		Assert.assertNull(cartService.getQuotesById("1"));
	}
}