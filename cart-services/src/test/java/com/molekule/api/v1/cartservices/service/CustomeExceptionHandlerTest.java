package com.molekule.api.v1.cartservices.service;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.method.HandlerMethod;

public class CustomeExceptionHandlerTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CustomExceptionsHandler customExceptionsHandler;

	@Mock
	Exception ex;

	@Mock
	HttpServletRequest request;

	private class TestController {
		public void testMethod() {
		}

	}

	@Test
	public void testMethod() throws Exception {
		Method method = TestController.class.getMethod("testMethod");
		TestController controller = new TestController();
		HandlerMethod handlerMethod = new HandlerMethod(controller, method);

		Assert.assertNotNull(customExceptionsHandler.handleAllExceptions(ex, handlerMethod, request));
	}

}