package com.molekule.api.v1.cartservices.configuration;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

@ContextConfiguration
public class CartConfigurationTest {

	AutoCloseable closeable;

	@InjectMocks
	CartConfiguration cartConfiguration;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Test
	public void getAWSCredentials() {
		ReflectionTestUtils.setField(cartConfiguration, "awsAccessKeyId", "Az9/vTv4f08Bachy1CrwAQ==");
		ReflectionTestUtils.setField(cartConfiguration, "awsSecretKey", "Az9/vTv4f08Bachy1CrwAQ==");
		Assert.assertNotNull(cartConfiguration.getAWSCredentials());
	}

	@Test
	public void getDynamoDB() {
		ReflectionTestUtils.setField(cartConfiguration, "awsAccessKeyId", "Az9/vTv4f08Bachy1CrwAQ==");
		ReflectionTestUtils.setField(cartConfiguration, "awsSecretKey", "Az9/vTv4f08Bachy1CrwAQ==");
		ReflectionTestUtils.setField(cartConfiguration, "awsRegion", "eu-central-1");
		Assert.assertNotNull(cartConfiguration.getDynamoDB());
	}

	@Test
	public void getDocket() {
		ReflectionTestUtils.setField(cartConfiguration, "profile", "dev");
		ReflectionTestUtils.setField(cartConfiguration, "devHost", "dev");
		Assert.assertNotNull(cartConfiguration.getDocket());
	}

	@Test
	public void getDocketQA() {
		ReflectionTestUtils.setField(cartConfiguration, "profile", "qa");
		ReflectionTestUtils.setField(cartConfiguration, "qaHost", "qa");
		Assert.assertNotNull(cartConfiguration.getDocket());
	}

	@Test
	public void getDocketUk() {
		ReflectionTestUtils.setField(cartConfiguration, "profile", "uk");
		ReflectionTestUtils.setField(cartConfiguration, "ukHost", "uk");
		Assert.assertNotNull(cartConfiguration.getDocket());
	}

	@Test
	public void crosConfigure() {
		Assert.assertNotNull(cartConfiguration.crosConfigure());

	}

	@Test
	public void getApiInfo() {
		Assert.assertNotNull(cartConfiguration.getApiInfo());
	}

	@Test
	public void cTEnvProperties() {
		Assert.assertNotNull(cartConfiguration.cTEnvProperties());
	}

	@Test
	public void netConnectionHelper() {
		Assert.assertNotNull(cartConfiguration.netConnectionHelper());
	}

	@Test
	public void ctServerHelperService() {
		Assert.assertNotNull(cartConfiguration.ctServerHelperService());
	}

	@Test
	public void registrationHelperService() {
		Assert.assertNotNull(cartConfiguration.registrationHelperService());
	}

	@Test
	public void checkoutHelperService() {
		Assert.assertNotNull(cartConfiguration.checkoutHelperService());
	}

	@Test
	public void orderHelperService() {
		Assert.assertNotNull(cartConfiguration.orderHelperService());
	}

	@Test
	public void cartHelperService() {
		Assert.assertNotNull(cartConfiguration.cartHelperService());
	}

	@Test
	public void taxService() {
		Assert.assertNotNull(cartConfiguration.taxService());
	}

	@Test
	public void handlingCostHelperService() {
		Assert.assertNotNull(cartConfiguration.handlingCostHelperService());
	}

	@Test
	public void parcelHandlingCostCalculationServieImpl() {
		Assert.assertNotNull(cartConfiguration.parcelHandlingCostCalculationServieImpl());
	}

	@Test
	public void freightHandlingCostCalculationServiceImpl() {
		Assert.assertNotNull(cartConfiguration.freightHandlingCostCalculationServiceImpl());
	}

	@Test
	public void tealiumHelperService() {
		Assert.assertNotNull(cartConfiguration.tealiumHelperService());
	}

	@Test
	public void csrHelperService() {
		Assert.assertNotNull(cartConfiguration.csrHelperService());
	}

	@Test
	public void quoteRequestConfirmationMailSender() {
		Assert.assertNotNull(cartConfiguration.quoteRequestConfirmationMailSender());
	}

	@Test
	public void quoteRequestPendingApprovalMailSender() {
		Assert.assertNotNull(cartConfiguration.quoteRequestPendingApprovalMailSender());
	}

	@Test
	public void auroraHelperService() {
		Assert.assertNotNull(cartConfiguration.auroraHelperService());
	}

	@Test
	public void orderUpdatedMailSender() {
		Assert.assertNotNull(cartConfiguration.orderUpdatedMailSender());
	}

	@Test
	public void freightInformationConfirmationSender() {
		Assert.assertNotNull(cartConfiguration.freightInformationConfirmationSender());
	}

	@Test
	public void authenticationHelperService() {
		ReflectionTestUtils.setField(cartConfiguration, "cognitoUserPool", "30");
		ReflectionTestUtils.setField(cartConfiguration, "cognitoClientId", "30");
		ReflectionTestUtils.setField(cartConfiguration, "cognitoClientSecret", "30");
		Assert.assertNotNull(cartConfiguration.authenticationHelperService());
	}

	@Test
	public void authenticationSecretAsNull() {
		ReflectionTestUtils.setField(cartConfiguration, "cognitoUserPool", "30");
		ReflectionTestUtils.setField(cartConfiguration, "cognitoClientId", "30");
		ReflectionTestUtils.setField(cartConfiguration, "cognitoClientSecret", null);
		Assert.assertNotNull(cartConfiguration.authenticationHelperService());
	}

	@Test
	public void getAsyncExecutor() {
		ReflectionTestUtils.setField(cartConfiguration, "threadPoolSize", 30);
		Assert.assertNotNull(cartConfiguration.getAsyncExecutor());
	}
}