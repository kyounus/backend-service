package com.molekule.api.v1.cartservices.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.amazonaws.services.route53domains.model.CountryCode;
import com.molekule.api.v1.cartservices.service.CartService;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

public class CartControllerTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CartController cartController;

	@Mock
	CartService cartService;

	@Mock
	CreateCartRequestBean createCartRequestBean;

	@Mock
	CartRequestBean cartRequestBean;

	@Test
	public void getCartById() {
		Assert.assertNull(cartController.getCartById("testID"));
	}

	@Test
	public void getAllQuotes() {
		Assert.assertNull(cartController.getAllQuotes(1, 1, "quoteNumber", "email"));
	}

	@Test
	public void getQuotesById() {
		Assert.assertNull(cartController.getQuotesById("quoteId"));
	}

	@Test
	public void createCart() {
		Assert.assertNull(cartController.createCart(createCartRequestBean));
	}

	@Test
	public void updateCartById() {
		Assert.assertNull(cartController.updateCartById("1234", cartRequestBean, CountryCode.GB.toString()));
	}

}
