package com.molekule.api.v1.productservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import com.amazonaws.services.route53domains.model.CountryCode;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean.Action.CustomerGroup;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.model.products.ProductCatalogData;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

public class ProductServiceTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	ProductService productService;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	RegistrationHelperService registrationHelperService;

	@Mock
	NetConnectionHelper netConnectionHelper;

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	CustomerResponseBean customerResponseBean;

	@Mock
	Logger logger;

	private final String CUSOMTER_ID = "1234";

	private final String url = "null/null/products/productId?priceCurrency=USD&expand=masterData.current.masterVariant.attributes[*].value.id";

	private final String PRODUCT_URL = "null/null/products?limit=10&offset=10&priceCurrency=USD&where=masterData(published=true)";
	private final String url1 = "null/null/products/productId?priceCurrency=USD&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id";

	private final String SKU_URL = "null/null/products?where=masterData(current(masterVariant(sku=\"testSKU\")))&priceCurrency=USD&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id&priceCustomerGroup=null";
	private final String json = "{\r\n" + "    \"masterData\":{\r\n" + "      \"published\":\"false\"\r\n" + "    }\r\n"
			+ "}";

	private final String PRODUCT_JSON = "{\"limit\":1,\"offset\":1,\"count\":1,\"total\":34,\"results\":[{\"id\":\"316642a0-3ef9-4ec4-b385-772de95a0541\",\"version\":37,\"lastMessageSequenceNumber\":17,\"createdAt\":\"2021-02-24T21:57:35.278Z\",\"lastModifiedAt\":\"2021-05-29T12:39:34.436Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"692b6979-70d8-4720-b446-902e754e3b08\"}},\"productType\":{\"typeId\":\"product-type\",\"id\":\"b0da5e03-5749-451c-ad2f-51d6c6855d7b\"},\"masterData\":{\"current\":{\"name\":{\"en-US\":\"Stand Alone PECO Filter - Sequoia\"},\"description\":{\"en-US\":\"SPARE/FRU, Stand Alone PECO Filter - Sequoia\"},\"categories\":[{\"typeId\":\"category\",\"id\":\"5a7d7456-a4ba-469a-a8ce-98b14c6c118e\"}],\"categoryOrderHints\":{},\"slug\":{\"en-US\":\"stand-alone-peco-filter-sequoia\"},\"metaTitle\":{\"en-US\":\"\",\"fr-CA\":\"\"},\"metaDescription\":{\"en-US\":\"\",\"fr-CA\":\"\"},\"masterVariant\":{\"id\":1,\"sku\":\"SQ1-PECFL-US\",\"key\":\"1920654\",\"prices\":[{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":14900,\"fractionDigits\":2},\"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\",\"tiers\":[{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":12516,\"fractionDigits\":2}},{\"minimumQuantity\":21,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":12218,\"fractionDigits\":2}},{\"minimumQuantity\":51,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11920,\"fractionDigits\":2}},{\"minimumQuantity\":101,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11548,\"fractionDigits\":2}},{\"minimumQuantity\":501,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11175,\"fractionDigits\":2}},{\"minimumQuantity\":1001,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":10803,\"fractionDigits\":2}}]}],\"images\":[],\"attributes\":[{\"name\":\"b2bView\",\"value\":true},{\"name\":\"b2cView\",\"value\":false},{\"name\":\"sellable\",\"value\":true},{\"name\":\"b2cSubscriptionRequired\",\"value\":false},{\"name\":\"b2bSubscriptionRequired\",\"value\":false},{\"name\":\"weight\",\"value\":2.6},{\"name\":\"length\",\"value\":8.23},{\"name\":\"width\",\"value\":8.23},{\"name\":\"height\",\"value\":11.85}],\"price\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":14900,\"fractionDigits\":2},\"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\",\"tiers\":[{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":12516,\"fractionDigits\":2}},{\"minimumQuantity\":21,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":12218,\"fractionDigits\":2}},{\"minimumQuantity\":51,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11920,\"fractionDigits\":2}},{\"minimumQuantity\":101,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11548,\"fractionDigits\":2}},{\"minimumQuantity\":501,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":11175,\"fractionDigits\":2}},{\"minimumQuantity\":1001,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":10803,\"fractionDigits\":2}}]},\"assets\":[]},\"variants\":[],\"searchKeywords\":{}},\"staged\":{\"name\":{\"en-US\":\"Stand Alone PECO Filter - Sequoia\"},\"description\":{\"en-US\":\"SPARE/FRU, Stand Alone PECO Filter - Sequoia\"},\"categories\":[{\"typeId\":\"category\",\"id\":\"5a7d7456-a4ba-469a-a8ce-98b14c6c118e\"}],\"categoryOrderHints\":{},\"slug\":{\"en-US\":\"stand-alone-peco-filter-sequoia\"},\"metaTitle\":{\"en-US\":\"\",\"fr-CA\":\"\"},\"metaDescription\":{\"en-US\":\"\",\"fr-CA\":\"\"},\"masterVariant\":{\"id\":1,\"sku\":\"SQ1-PECFL-US\",\"key\":\"1920654\",\"prices\":[{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":15000,\"fractionDigits\":2},\"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\"}],\"images\":[],\"attributes\":[{\"name\":\"b2bView\",\"value\":true},{\"name\":\"b2cView\",\"value\":true},{\"name\":\"sellable\",\"value\":true},{\"name\":\"b2cSubscriptionRequired\",\"value\":false},{\"name\":\"b2bSubscriptionRequired\",\"value\":false},{\"name\":\"weight\",\"value\":5},{\"name\":\"length\",\"value\":8.23},{\"name\":\"width\",\"value\":8.23},{\"name\":\"height\",\"value\":11.85},{\"name\":\"status\",\"value\":\"\"},{\"name\":\"unitOfMeasure\",\"value\":\"Each\"},{\"name\":\"upcCode\",\"value\":0}],\"price\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":15000,\"fractionDigits\":2},\"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\"},\"assets\":[]},\"variants\":[],\"searchKeywords\":{}},\"published\":true,\"hasStagedChanges\":true},\"key\":\"1920654\",\"taxCategory\":{\"typeId\":\"tax-category\",\"id\":\"8ec0e9ef-35a5-47a9-91c1-24a4e8ad5861\"},\"lastVariantId\":1}]}\r\n";

	private final String PRODUCT_JSON_1 = "{ \"limit\":1, \"offset\":1, \"count\":1, \"total\":34, \"masterData\":{ \"current\":{ \"name\":{ \"en-US\":\"StandAlonePECOFilter-Sequoia\" }, \"description\":{ \"en-US\":\"SPARE/FRU,StandAlonePECOFilter-Sequoia\" }, \"categories\":[ { \"typeId\":\"category\", \"id\":\"5a7d7456-a4ba-469a-a8ce-98b14c6c118e\" } ], \"categoryOrderHints\":{}, \"slug\":{ \"en-US\":\"stand-alone-peco-filter-sequoia\" }, \"metaTitle\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"metaDescription\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"masterVariant\":{ \"id\":1, \"sku\":\"SQ1-PECFL-US\", \"key\":\"1920654\", \"prices\":[ { \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":14900, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\", \"tiers\":[ { \"minimumQuantity\":10, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12516, \"fractionDigits\":2 } }, { \"minimumQuantity\":21, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12218, \"fractionDigits\":2 } }, { \"minimumQuantity\":51, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11920, \"fractionDigits\":2 } }, { \"minimumQuantity\":101, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11548, \"fractionDigits\":2 } }, { \"minimumQuantity\":501, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11175, \"fractionDigits\":2 } }, { \"minimumQuantity\":1001, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":10803, \"fractionDigits\":2 } } ] } ], \"images\":[], \"attributes\":[ { \"name\":\"b2bView\", \"value\":true }, { \"name\":\"b2cView\", \"value\":false }, { \"name\":\"sellable\", \"value\":true }, { \"name\":\"b2cSubscriptionRequired\", \"value\":false }, { \"name\":\"b2bSubscriptionRequired\", \"value\":false }, { \"name\":\"weight\", \"value\":2.6 }, { \"name\":\"length\", \"value\":8.23 }, { \"name\":\"width\", \"value\":8.23 }, { \"name\":\"height\", \"value\":11.85 } ], \"price\":{ \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":14900, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\", \"tiers\":[ { \"minimumQuantity\":10, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12516, \"fractionDigits\":2 } }, { \"minimumQuantity\":21, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12218, \"fractionDigits\":2 } }, { \"minimumQuantity\":51, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11920, \"fractionDigits\":2 } }, { \"minimumQuantity\":101, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11548, \"fractionDigits\":2 } }, { \"minimumQuantity\":501, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11175, \"fractionDigits\":2 } }, { \"minimumQuantity\":1001, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":10803, \"fractionDigits\":2 } } ] }, \"assets\":[] }, \"variants\":[], \"searchKeywords\":{} }, \"staged\":{ \"name\":{ \"en-US\":\"StandAlonePECOFilter-Sequoia\" }, \"description\":{ \"en-US\":\"SPARE/FRU,StandAlonePECOFilter-Sequoia\" }, \"categories\":[ { \"typeId\":\"category\", \"id\":\"5a7d7456-a4ba-469a-a8ce-98b14c6c118e\" } ], \"categoryOrderHints\":{}, \"slug\":{ \"en-US\":\"stand-alone-peco-filter-sequoia\" }, \"metaTitle\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"metaDescription\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"masterVariant\":{ \"id\":1, \"sku\":\"SQ1-PECFL-US\", \"key\":\"1920654\", \"prices\":[ { \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":15000, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\" } ], \"images\":[], \"attributes\":[ { \"name\":\"b2bView\", \"value\":true }, { \"name\":\"b2cView\", \"value\":true }, { \"name\":\"sellable\", \"value\":true }, { \"name\":\"b2cSubscriptionRequired\", \"value\":false }, { \"name\":\"b2bSubscriptionRequired\", \"value\":false }, { \"name\":\"weight\", \"value\":5 }, { \"name\":\"length\", \"value\":8.23 }, { \"name\":\"width\", \"value\":8.23 }, { \"name\":\"height\", \"value\":11.85 }, { \"name\":\"status\", \"value\":\"\" }, { \"name\":\"unitOfMeasure\", \"value\":\"Each\" }, { \"name\":\"upcCode\", \"value\":0 } ], \"price\":{ \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":15000, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\" }, \"assets\":[] }, \"variants\":[], \"searchKeywords\":{} }, \"published\":true, \"hasStagedChanges\":true }, \"results\":[ { \"id\":\"316642a0-3ef9-4ec4-b385-772de95a0541\", \"version\":37, \"lastMessageSequenceNumber\":17, \"createdAt\":\"2021-02-24T21:57:35.278Z\", \"lastModifiedAt\":\"2021-05-29T12:39:34.436Z\", \"lastModifiedBy\":{ \"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\", \"isPlatformClient\":false }, \"createdBy\":{ \"isPlatformClient\":true, \"user\":{ \"typeId\":\"user\", \"id\":\"692b6979-70d8-4720-b446-902e754e3b08\" } }, \"productType\":{ \"typeId\":\"product-type\", \"id\":\"b0da5e03-5749-451c-ad2f-51d6c6855d7b\" }, \"masterData\":{ \"current\":{ \"name\":{ \"en-US\":\"StandAlonePECOFilter-Sequoia\" }, \"description\":{ \"en-US\":\"SPARE/FRU,StandAlonePECOFilter-Sequoia\" }, \"categories\":[ { \"typeId\":\"category\", \"id\":\"5a7d7456-a4ba-469a-a8ce-98b14c6c118e\" } ], \"categoryOrderHints\":{}, \"slug\":{ \"en-US\":\"stand-alone-peco-filter-sequoia\" }, \"metaTitle\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"metaDescription\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"masterVariant\":{ \"id\":1, \"sku\":\"SQ1-PECFL-US\", \"key\":\"1920654\", \"prices\":[ { \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":14900, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\", \"tiers\":[ { \"minimumQuantity\":10, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12516, \"fractionDigits\":2 } }, { \"minimumQuantity\":21, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12218, \"fractionDigits\":2 } }, { \"minimumQuantity\":51, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11920, \"fractionDigits\":2 } }, { \"minimumQuantity\":101, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11548, \"fractionDigits\":2 } }, { \"minimumQuantity\":501, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11175, \"fractionDigits\":2 } }, { \"minimumQuantity\":1001, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":10803, \"fractionDigits\":2 } } ] } ], \"images\":[], \"attributes\":[ { \"name\":\"b2bView\", \"value\":true }, { \"name\":\"b2cView\", \"value\":false }, { \"name\":\"sellable\", \"value\":true }, { \"name\":\"b2cSubscriptionRequired\", \"value\":false }, { \"name\":\"b2bSubscriptionRequired\", \"value\":false }, { \"name\":\"weight\", \"value\":2.6 }, { \"name\":\"length\", \"value\":8.23 }, { \"name\":\"width\", \"value\":8.23 }, { \"name\":\"height\", \"value\":11.85 } ], \"price\":{ \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":14900, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\", \"tiers\":[ { \"minimumQuantity\":10, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12516, \"fractionDigits\":2 } }, { \"minimumQuantity\":21, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":12218, \"fractionDigits\":2 } }, { \"minimumQuantity\":51, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11920, \"fractionDigits\":2 } }, { \"minimumQuantity\":101, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11548, \"fractionDigits\":2 } }, { \"minimumQuantity\":501, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":11175, \"fractionDigits\":2 } }, { \"minimumQuantity\":1001, \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":10803, \"fractionDigits\":2 } } ] }, \"assets\":[] }, \"variants\":[], \"searchKeywords\":{} }, \"staged\":{ \"name\":{ \"en-US\":\"StandAlonePECOFilter-Sequoia\" }, \"description\":{ \"en-US\":\"SPARE/FRU,StandAlonePECOFilter-Sequoia\" }, \"categories\":[ { \"typeId\":\"category\", \"id\":\"5a7d7456-a4ba-469a-a8ce-98b14c6c118e\" } ], \"categoryOrderHints\":{}, \"slug\":{ \"en-US\":\"stand-alone-peco-filter-sequoia\" }, \"metaTitle\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"metaDescription\":{ \"en-US\":\"\", \"fr-CA\":\"\" }, \"masterVariant\":{ \"id\":1, \"sku\":\"SQ1-PECFL-US\", \"key\":\"1920654\", \"prices\":[ { \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":15000, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\" } ], \"images\":[], \"attributes\":[ { \"name\":\"b2bView\", \"value\":true }, { \"name\":\"b2cView\", \"value\":true }, { \"name\":\"sellable\", \"value\":true }, { \"name\":\"b2cSubscriptionRequired\", \"value\":false }, { \"name\":\"b2bSubscriptionRequired\", \"value\":false }, { \"name\":\"weight\", \"value\":5 }, { \"name\":\"length\", \"value\":8.23 }, { \"name\":\"width\", \"value\":8.23 }, { \"name\":\"height\", \"value\":11.85 }, { \"name\":\"status\", \"value\":\"\" }, { \"name\":\"unitOfMeasure\", \"value\":\"Each\" }, { \"name\":\"upcCode\", \"value\":0 } ], \"price\":{ \"value\":{ \"type\":\"centPrecision\", \"currencyCode\":\"USD\", \"centAmount\":15000, \"fractionDigits\":2 }, \"id\":\"5bcbc278-cf0e-49fb-b9dc-be44d663909e\" }, \"assets\":[] }, \"variants\":[], \"searchKeywords\":{} }, \"published\":true, \"hasStagedChanges\":true }, \"key\":\"1920654\", \"taxCategory\":{ \"typeId\":\"tax-category\", \"id\":\"8ec0e9ef-35a5-47a9-91c1-24a4e8ad5861\" }, \"lastVariantId\":1 } ] }";

	private final String TEST_SKU = "testSKU";

	private final String ALL_PRODUCT_URL = "null/null/products/316642a0-3ef9-4ec4-b385-772de95a0541?priceCurrency=USD&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id&priceCustomerGroup=null";

	private final String PRODUCT_TYPE_URL = "null/null/products/316642a0-3ef9-4ec4-b385-772de95a0541?priceCurrency=USD&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id&priceCustomerGroup=null";

	private final String RATINGS_URL = "null/m/null/l/en_US/product/null/reviews?sort=HighestRating&apikey=null";

	private final String RATING_BAD_RESPONSE = "{   \"statusCode\": \"400\" }";
	@Mock
	CustomerGroup customerGroup;

	@Mock
	ProductBean productBean;

	@Mock
	ProductCatalogData productCatalogData;

	@Test(expected = NullPointerException.class)
	public void getProductByIdSKUIsNull() {

		BDDMockito.when(registrationHelperService.getCustomerById(CUSOMTER_ID)).thenReturn(customerResponseBean);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, url)).thenReturn(json);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, url1)).thenReturn(json);
		Assert.assertNull(productService.getProductById(PRODUCT_ID, "", null, null, null, CountryCode.GB.toString()));
	}

	@Test(expected = NullPointerException.class)
	public void getProductBySku() {
		BDDMockito.when(registrationHelperService.getCustomerById(CUSOMTER_ID)).thenReturn(customerResponseBean);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, PRODUCT_URL)).thenReturn(PRODUCT_JSON);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, SKU_URL)).thenReturn(PRODUCT_JSON);
		BDDMockito.when(customerResponseBean.getCustomerGroup()).thenReturn(customerGroup);
		Assert.assertNotNull(productService.getProductBySku(TEST_SKU, CUSOMTER_ID, CountryCode.GB.toString()));
	}

	@Test(expected = NullPointerException.class)
	public void getAllProducts() {
		BDDMockito.when(registrationHelperService.getCustomerById(CUSOMTER_ID)).thenReturn(customerResponseBean);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, PRODUCT_URL)).thenReturn(PRODUCT_JSON);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, SKU_URL)).thenReturn(PRODUCT_JSON);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, ALL_PRODUCT_URL)).thenReturn(PRODUCT_JSON_1);
		BDDMockito.when(netConnectionHelper.sendGetProductDataWithoutBody(null, PRODUCT_TYPE_URL))
				.thenReturn(productBean);
		BDDMockito.when(productBean.getMasterData()).thenReturn(productCatalogData);

		ProductData current = Mockito.mock(ProductData.class);
		BDDMockito.when(productCatalogData.getCurrent()).thenReturn(current);
		Map<String, String> slug = new HashMap<>();
		slug.put("EN_US", "1");
		BDDMockito.when(current.getSlug()).thenReturn(slug);

		BDDMockito.when(customerResponseBean.getCustomerGroup()).thenReturn(customerGroup);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(MolekuleConstant.EMPTY, RATINGS_URL))
				.thenReturn(RATING_BAD_RESPONSE);
		Assert.assertNull(productService.getAllProducts(CUSOMTER_ID, 10, 10, CountryCode.GB.toString()));

	}

}