package com.molekule.api.v1.productservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.productservices.service.ProductService;

public class ProductsControllerTest {
	@InjectMocks
	ProductsController productsController;

	AutoCloseable closeable;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Mock
	ProductService productService;

	@Mock
	Logger logger;

	@Test
	public void getProductById() {
		Assert.assertNull(productsController.getProductById(PRODUCT_ID, "customerId", null));
	}

	@Test
	public void getProductBySku() {
		Assert.assertNull(productsController.getProductBySku("sku", "customerId", null));
	}

	@Test
	public void getAllProducts() {
		Assert.assertNull(productsController.getAllProducts("customerId", 10, 20, null));
	}
}