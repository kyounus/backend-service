package com.molekule.api.v1.productservices.configuration;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class ProductConfigurationTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	ProductConfiguration productConfiguration;

	// @Test
	public void getAWSCredentials() {
		Assert.assertNotNull(productConfiguration.getAWSCredentials());
	}

	// @Test
	public void getDynamoDB() {
		Assert.assertNotNull(productConfiguration.getDynamoDB());

	}

	@Test
	public void getDocket() {
		Assert.assertNotNull(productConfiguration.getDocket());
	}

	@Test
	public void crosConfigure() {
		Assert.assertNotNull(productConfiguration.crosConfigure());
	}

	@Test
	public void getApiInfo() {
		Assert.assertNotNull(productConfiguration.getApiInfo());
	}

	@Test
	public void cTEnvProperties() {
		Assert.assertNotNull(productConfiguration.cTEnvProperties());

	}

	@Test
	public void netConnectionHelper() {
		Assert.assertNotNull(productConfiguration.netConnectionHelper());

	}

	@Test
	public void ctServerHelperService() {
		Assert.assertNotNull(productConfiguration.ctServerHelperService());
	}

	@Test
	public void registrationHelperService() {
		Assert.assertNotNull(productConfiguration.registrationHelperService());

	}

	@Test
	public void checkoutHelperService() {
		Assert.assertNotNull(productConfiguration.checkoutHelperService());

	}

	@Test
	public void orderHelperService() {
		Assert.assertNotNull(productConfiguration.orderHelperService());

	}

	@Test
	public void cartHelperService() {
		Assert.assertNotNull(productConfiguration.cartHelperService());
	}

	@Test
	public void taxService() {
		Assert.assertNotNull(productConfiguration.taxService());

	}

	@Test
	public void handlingCostHelperService() {
		Assert.assertNotNull(productConfiguration.handlingCostHelperService());

	}

	@Test
	public void parcelHandlingCostCalculationServieImpl() {
		Assert.assertNotNull(productConfiguration.parcelHandlingCostCalculationServieImpl());
	}

	@Test
	public void freightHandlingCostCalculationServiceImpl() {
		Assert.assertNotNull(productConfiguration.freightHandlingCostCalculationServiceImpl());

	}

	@Test
	public void csrHelperService() {
		Assert.assertNotNull(productConfiguration.csrHelperService());

	}

	@Test
	public void tealiumHelperService() {
		Assert.assertNotNull(productConfiguration.tealiumHelperService());

	}

	@Test
	public void quoteRequestConfirmationMailSender() {
		Assert.assertNotNull(productConfiguration.quoteRequestConfirmationMailSender());
	}

	@Test
	public void quoteRequestPendingApprovalMailSender() {
		Assert.assertNotNull(productConfiguration.quoteRequestPendingApprovalMailSender());
	}

	@Test
	public void paymentReceivedConfirmationSender() {
		Assert.assertNotNull(productConfiguration.paymentReceivedConfirmationSender());
	}

}