package com.molekule.api.v1.productservices.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.mail.sendgrid.FreightInformationConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderUpdatedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentReceivedConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.QuoteRequestConfirmationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.QuoteRequestPendingApprovalMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.cart.FreightHandlingCostCalculationServiceImpl;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostCalculationService;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostHelperService;
import com.molekule.api.v1.commonframework.service.cart.ParcelHandlingCostCalculationServieImpl;
import com.molekule.api.v1.commonframework.service.cart.TaxService;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.AuthenticationHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.EncryptUtil;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The class MolekuleBeanConfiguration is used to configure the beans for the
 * application.
 */
@Configuration
@EnableSwagger2
//@PropertySource(value = ("${common.application.properties}"))
public class ProductConfiguration {
	
    @Value("${ct.aws.access.key.id}")
    private String awsAccessKeyId;

    @Value("${ct.aws.access.secret.key}")
    private String awsSecretKey;

    @Value("${ct.aws.region}")
    private String awsRegion;
    
    @Value("${devHost}")
    private String devHost;
    
    @Value("${qaHost}")
    private String qaHost;
    @Value("${ukHost}")
    private String ukHost;
    @Value("${profile}")
    private String profile;
    
    @Value("${ct.cognitoUserPool}")
	private String cognitoUserPool;
    
    @Value("${ct.cognitoClientId}")
	private String cognitoClientId;
    
    @Value("${ct.cognitoClientSecret}")
	private String cognitoClientSecret;

	@Bean(name = "awsCredentialsProvider")
	public AWSCredentialsProvider getAWSCredentials() {
		return new AWSStaticCredentialsProvider(new BasicAWSCredentials(EncryptUtil.decrypt(this.awsAccessKeyId), EncryptUtil.decrypt(this.awsSecretKey)));
	}

	@Bean(name = "dynamoDB")
	public DynamoDB getDynamoDB() {
		return new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(awsRegion)
				.withCredentials(getAWSCredentials()).build());

	}
	
	/**
	 * getDocket bean is written to get all apis in the swagger ui.
	 * 
	 * @return Docket
	 */
	@Bean
	public Docket getDocket() {
		  String host = null;	  
		   if("dev".equals(profile)) {
			   host = devHost;
		   }else if("qa".equals(profile)){
			   host = qaHost;
		   }else if("uk".equals(profile)){
			   host = ukHost;
		   }
		Docket swaggerDoc = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).build();
		if(host!= null) {
			swaggerDoc.host(host);
		}
		return swaggerDoc.apiInfo(getApiInfo());
	}

	/**
	 * crosConfigure bean is written to configure the cross platform applications.
	 * 
	 * @return WebMvcConfigurer
	 */
	@Bean
	public WebMvcConfigurer crosConfigure() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST").allowedOrigins("*").allowedHeaders("*");
				WebMvcConfigurer.super.addCorsMappings(registry);
			}

		};
	}

	/**
	 * getApiInfo method is used to get the api information.
	 * 
	 * @return ApiInfo
	 */
	public ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("Molekule Integration Api's For Product Services")
				.description("exposed integration api's from molekule to ct").build();
	}

	@Bean
	public CTEnvProperties cTEnvProperties() {
		return new CTEnvProperties();
	}
	
	@Bean
	public NetConnectionHelper netConnectionHelper() {
		return new NetConnectionHelper();
	}
	
	@Bean
	public CtServerHelperService ctServerHelperService() {
		return new CtServerHelperService();
	}
	
	@Bean
	public RegistrationHelperService registrationHelperService() {
		return new RegistrationHelperService();
	}
	@Bean
	public CheckoutHelperService checkoutHelperService() {
		return new CheckoutHelperService();
	}
	
	@Bean
	public OrderHelperService orderHelperService() {
		return new OrderHelperService();
	}
	
	@Bean
	public CartHelperService cartHelperService() {
		return new CartHelperService();
	}
	@Bean
	public TaxService taxService() {
		return new TaxService();
	}
	
	@Bean
	public HandlingCostHelperService handlingCostHelperService() {
		 return new HandlingCostHelperService();
	}
	
	@Bean("handlingCostCalculationService")
	public HandlingCostCalculationService parcelHandlingCostCalculationServieImpl() {
		return new ParcelHandlingCostCalculationServieImpl();
	}
	
	@Bean("handlingCostCalculationService")
	public HandlingCostCalculationService freightHandlingCostCalculationServiceImpl() {
		return new FreightHandlingCostCalculationServiceImpl();
	}
	
	@Bean
	public CsrHelperService csrHelperService() {
		return new CsrHelperService();
	}
	
	@Bean
	public TealiumHelperService tealiumHelperService() {
		return new TealiumHelperService();
	}
	
	@Bean
	public TemplateMailRequestHelper quoteRequestConfirmationMailSender() {
		return new QuoteRequestConfirmationMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper quoteRequestPendingApprovalMailSender() {
		return new QuoteRequestPendingApprovalMailSender();
	}

	@Bean
	public TemplateMailRequestHelper paymentReceivedConfirmationSender() {
		return new PaymentReceivedConfirmationSender();
	}

	@Bean
	public AuroraHelperService auroraHelperService() {
		return new AuroraHelperService();
	}
	@Bean
	public TemplateMailRequestHelper orderUpdatedMailSender() {
		return new OrderUpdatedMailSender();
	}
	@Bean
	public TemplateMailRequestHelper freightInformationConfirmationSender() {
		return new FreightInformationConfirmationSender();
	}
	
	@Bean
	public AuthenticationHelperService authenticationHelperService() {
		if(com.amazonaws.util.StringUtils.isNullOrEmpty(cognitoClientSecret)) {
			return new AuthenticationHelperService(cognitoUserPool, cognitoClientId, null);			
		} else {
			return new AuthenticationHelperService(cognitoUserPool, cognitoClientId, cognitoClientSecret);
		}
	}
}
