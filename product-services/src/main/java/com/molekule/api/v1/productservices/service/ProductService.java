package com.molekule.api.v1.productservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.model.products.Price;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.products.ProductListResponse;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.model.registration.ProductResponse;
import com.molekule.api.v1.commonframework.model.registration.ProductResponse.UpSellSku;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.CountryCodeEnum;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

/**
 * The class ProductService is used to write the api calls for the product.
 * 
 * @version 1.0
 */
@Service("productService")
public class ProductService {

	private static final String SLASH_PRODUCTS_SLASH ="/products/";
	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	CTEnvProperties ctEnvProperties;

	Logger logger = LoggerFactory.getLogger(ProductService.class);

	/**
	 * getProductById method is used to get entire product data by productId.
	 * 
	 * @param productId
	 * @param customerId
	 * @param sku
	 * @param country 
	 * @return ResponseEntity<ProductResponse>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<ProductResponse> getProductById(String productId, String customerId, String sku, String token,CustomerResponseBean customerResponseBean, String country) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		ProductResponse responseBean = new ProductResponse();
		String customerGroupId = null;
		String url = null;
		if (StringUtils.hasText(productId)) {
			url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
					.append(ctEnvProperties.getProjectKey()).append(SLASH_PRODUCTS_SLASH).append(productId).append("?")
					.append(PRICE_CURRENCY).append(currency)
					.append("&expand=masterData.current.masterVariant.attributes[*].value.id").append("&expand=productType.id")
					.toString();
		} else if (StringUtils.hasText(sku)) {
			url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ "/products?where=masterData(current(masterVariant(sku=\"" + sku + "\")))&"
			+ PRICE_CURRENCY + currency 
			+ "&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id";
		}
		if (StringUtils.hasText(customerId)) {
			if(customerResponseBean == null) {
				customerResponseBean = registrationHelperService.getCustomerById(customerId.trim());
			}
			
			if ( customerResponseBean != null && customerResponseBean.getCustomerGroup() != null) {
				customerGroupId = customerResponseBean.getCustomerGroup().getId();
			 if (StringUtils.hasText(productId)) {
				url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
						.append(ctEnvProperties.getProjectKey()).append(SLASH_PRODUCTS_SLASH).append(productId).append("?")
						.append(PRICE_CURRENCY).append(currency)
						.append("&expand=masterData.current.masterVariant.attributes[*].value.id").append("&expand=productType.id&priceCustomerGroup=")
						.append(customerGroupId).toString();
			} else if (StringUtils.hasText(sku)) {
				url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/products?where=masterData(current(masterVariant(sku=\"" + sku + "\")))&"
				+ PRICE_CURRENCY + currency 
				+ "&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id&priceCustomerGroup="
				+ customerGroupId;
			}
			}
		}
		String subscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, url);

		JsonObject responseObject = MolekuleUtility.parseJsonObject(subscriptionResponse);
		if(StringUtils.hasText(sku)) {
			responseObject = responseObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
			productId = responseObject.get("id").getAsString();
			url = skuGenerateUrl(productId, customerId, customerGroupId,currency);

		}
		boolean published = responseObject.get(MASTER_DATA).getAsJsonObject().get("published").getAsBoolean();
		if (!published) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "Product not available");
			return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		}
		Map<String, String> subscriptionData = getSubscriptionPeriod(responseObject);
		ProductBean productBeanResponseData = netConnectionHelper.sendGetProductDataWithoutBody(token, url);

		if (productBeanResponseData == null || productBeanResponseData.getMasterData() == null) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "Product not available");
			return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		}

		ProductData current = productBeanResponseData.getMasterData().getCurrent();
		String pageId = current.getSlug().get(EN_US);

		String ratingsResponseData = getRatingsandReviews(pageId);
		JsonObject ratingsResponseJsonObject = MolekuleUtility.parseJsonObject(ratingsResponseData);
		if (ratingsResponseJsonObject.get(STATUS_CODE) != null) {
			return new ResponseEntity(HttpStatus.valueOf(ratingsResponseJsonObject.get(STATUS_CODE).getAsInt()));
		}
		setResultsArray(customerId, responseBean, customerGroupId, subscriptionData, productBeanResponseData,
				current, ratingsResponseJsonObject);
		//Clyde implementation
		populateUpsellSKU(productId, responseBean, token);

		return new ResponseEntity<>(responseBean, HttpStatus.OK);
	}

	private void populateUpsellSKU(String productId, ProductResponse responseBean, String token) {
		UpSellSku upSellSku = new UpSellSku();
		//setUpSellSku
		String productProjectionUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() 
		+"/product-projections/search?expand=masterVariant.attributes[*].value.id&filter=variants.attributes.sku.id:\"" + productId + "\"" ; 
		String productProjectionsResponse = netConnectionHelper.sendGetWithoutBody(token, productProjectionUrl);
		JsonObject responseObject = MolekuleUtility.parseJsonObject(productProjectionsResponse);
		JsonObject resultObject = new JsonObject();
		if(responseObject.has(RESULTS) && responseObject.get(RESULTS).getAsJsonArray().size() > 0) {
			resultObject = responseObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
		}
		JsonArray attributeArray = new JsonArray();
		if(resultObject.has(MASTER_VARIANT)) {
			attributeArray = resultObject.get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		}
		JsonObject valueJsonObj = new JsonObject();
		for(int i=0; i<attributeArray.size(); i++) {
			JsonObject attributeObject = attributeArray.get(i).getAsJsonObject();
			String name = attributeObject.get("name").getAsString();
			if(name.equals("upsellSKU") && attributeObject.has(VALUE) && attributeObject.get(VALUE).getAsJsonObject().has("obj")) {
				valueJsonObj = attributeObject.get(VALUE).getAsJsonObject().get("obj").getAsJsonObject();
				upSellSku.setId(attributeObject.get(VALUE).getAsJsonObject().get("id").getAsString());
			}
		}
		if(valueJsonObj.has(MASTER_DATA)) {
			JsonObject currentObject = valueJsonObj.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject();
			if(currentObject.has("name")) {
				upSellSku.setName(currentObject.get("name").getAsJsonObject().get(EN_US).getAsString());
			}
			if(currentObject.has(MASTER_VARIANT)) {
				upSellSku.setSku(currentObject.get(MASTER_VARIANT).getAsJsonObject().get("sku").getAsString());
			}
		}
		responseBean.setUpSellSku(upSellSku);
	}

	private String skuGenerateUrl(String productId, String customerId, String customerGroupId, String currency) {
		if (StringUtils.hasText(customerId)) {
			return new StringBuilder().append(ctEnvProperties.getHost()).append("/")
					.append(ctEnvProperties.getProjectKey()).append(SLASH_PRODUCTS_SLASH).append(productId).append("?")
					.append(PRICE_CURRENCY).append(currency)
					.append("&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id&priceCustomerGroup=")
					.append(customerGroupId).toString();
		}
		return new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
				.append(SLASH_PRODUCTS_SLASH).append(productId).append("?")
				.append(PRICE_CURRENCY).append(currency)
				.append("&expand=masterData.current.masterVariant.attributes[*].value.id&expand=productType.id")
				.toString();
	}

	private void setResultsArray(String customerId, ProductResponse responseBean, String customerGroupId,
			Map<String, String> subscriptionData, ProductBean productBeanResponseData, ProductData current,
			JsonObject ratingsResponseJsonObject) {
		JsonArray resultsArray = ratingsResponseJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		if (resultsObject.getAsJsonObject("rollup") != null) {
			JsonObject rollUpObject = resultsObject.getAsJsonObject("rollup").getAsJsonObject();
			String averageRating = rollUpObject.get("average_rating").getAsString();
			String reviewCount = rollUpObject.get("review_count").getAsString();
			responseBean.setAverageRating(averageRating);
			responseBean.setReviewCount(reviewCount);
		}

		// Updating prices with customer group price in response
		if (StringUtils.hasText(customerId) && StringUtils.hasText(customerGroupId)) {
			updateCustomerGroupPricesResponse(customerGroupId, productBeanResponseData, current);

		}
		responseBean.setProductBean(productBeanResponseData);
		if (subscriptionData != null && subscriptionData.size() > 0) {
			responseBean.setHasSubscription(Boolean.parseBoolean(subscriptionData.get("hasSubscription")));
			responseBean.setSubscriptionPeriod(subscriptionData.get("subscriptionPeriod"));
		}
	}

	/**
	 * updateCustomerGroupPricesResponse method is used to update the response
	 * object with customer group prices.
	 * 
	 * @param customerGroupId
	 * @param productBeanResponseData
	 * @param current
	 */
	private void updateCustomerGroupPricesResponse(String customerGroupId, ProductBean productBeanResponseData,
			ProductData current) {
		List<Price> currentPriceList = new ArrayList<>();
		int currentPriceListSize = current.getMasterVariant().getPrices().size();

		for (int i = 0; i < currentPriceListSize; i++) {
			Price price = current.getMasterVariant().getPrices().get(i);
			if (Objects.nonNull(price.getCustomerGroup())
					&& price.getCustomerGroup().getCustomerGroupId().equals(customerGroupId)) {
				currentPriceList.add(price);
			}
		}
		productBeanResponseData.getMasterData().getCurrent().getMasterVariant().setPrices(currentPriceList);

		List<Price> stagedPriceList = new ArrayList<>();
		int stagedPriceListSize = productBeanResponseData.getMasterData().getStaged().getMasterVariant().getPrices()
				.size();
		for (int i = 0; i < stagedPriceListSize; i++) {
			Price price = productBeanResponseData.getMasterData().getStaged().getMasterVariant().getPrices().get(i);
			if (Objects.nonNull(price.getCustomerGroup())
					&& price.getCustomerGroup().getCustomerGroupId().equals(customerGroupId)) {
				stagedPriceList.add(price);
			}
		}
		productBeanResponseData.getMasterData().getStaged().getMasterVariant().setPrices(stagedPriceList);
	}

	/**
	 * getRatingsandReviews method is used to call power review api for ratings and
	 * reviews.
	 * 
	 * @param pageId
	 * @return String
	 */
	private String getRatingsandReviews(String pageId) {
		String ratingsUrl = new StringBuilder().append(ctEnvProperties.getPowerReviewHost()).append("/m/")
				.append(ctEnvProperties.getPowerReviewMerchantIdTestUS()).append("/l/en_US/product/").append(pageId)
				.append("/reviews?sort=HighestRating&apikey=").append(ctEnvProperties.getPowerReviewApiKey())
				.toString();
		return netConnectionHelper.sendGetWithoutBody(EMPTY, ratingsUrl);
	}

	private Map<String, String> getSubscriptionPeriod(JsonObject responseObject) {
		Map<String, String> subscriptionData = new HashMap<>();
		JsonArray attributes = responseObject.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject()
				.get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		if (attributes == null || attributes.size() <= 0) {
			return null;
		}
		return processAttributesValue(subscriptionData, attributes);
	}

	private Map<String, String> processAttributesValue(Map<String, String> subscriptionData, JsonArray attributes) {
		for (int i = 0; i < attributes.size(); i++) {
			String name = attributes.get(i).getAsJsonObject().get("name").getAsString();
			if ("SubscriptionIdentifier".equals(name)) {
				logger.debug("Subscription True....Value - {}", i);
				subscriptionData.put("hasSubscription", "true");
				JsonArray jsonArray = null;
				JsonObject productValueJson = attributes.get(i).getAsJsonObject().get(VALUE).getAsJsonObject();
				if (productValueJson.has("obj")) {
					jsonArray = productValueJson.get("obj").getAsJsonObject().get(MASTER_DATA).getAsJsonObject()
							.get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES)
							.getAsJsonArray();
				}
				if (jsonArray == null || jsonArray.size() <= 0) {
					return null;
				}
				processPeriodNameValues(subscriptionData, attributes, jsonArray);
				break;
			}
		}
		return subscriptionData;
	}

	private void processPeriodNameValues(Map<String, String> subscriptionData, JsonArray attributes,
			JsonArray jsonArray) {
		int period;
		for (int j = 0; j < attributes.size(); j++) {
			String periodName = jsonArray.get(j).getAsJsonObject().get("name").getAsString();
			if (PERIOD.equals(periodName)) {
				period = jsonArray.get(j).getAsJsonObject().get(VALUE).getAsInt();
				subscriptionData.put("subscriptionPeriod", String.valueOf(period));
				break;
			}
		}
	}

	/**
	 * getProductBySku method is used to get the product information by passing sku.
	 * 
	 * @param sku
	 * @param customerId
	 * @param country 
	 * @return ResponseEntity<ProductResponse>
	 */
	public ResponseEntity<ProductResponse> getProductBySku(String sku, String customerId, String country) {
		String token = ctServerHelperService.getStringAccessToken();
		return getProductById(null, customerId, sku, token,null,country);
	}

	/**
	 * getAllProducts method is used to get all the products.
	 * 
	 * @param customerId
	 * @param limit
	 * @param offset
	 * @param country 
	 * @return ResponseEntity<ProductListResponse>
	 */
	public ResponseEntity<ProductListResponse> getAllProducts(String customerId, int limit, int offset, String country) {
		String currency = CountryCodeEnum.valueOf(country).getCurrency();
		ProductListResponse productListResponse = new ProductListResponse();
		List<ProductResponse> responseList = new ArrayList<>();
		String currencyCode = currency;
		String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/products?limit="
				+ limit + "&offset=" + offset + "&where=masterData(published=true)&"
				+ PRICE_CURRENCY + currencyCode + "&where=masterData(current(masterVariant(prices(country=\""+country+"\"))))";
		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, productUrl);
		JsonObject responseObject = MolekuleUtility.parseJsonObject(response);
		productListResponse.setLimit(responseObject.get("limit").getAsInt());
		productListResponse.setOffset(responseObject.get("offset").getAsInt());
		productListResponse.setCount(responseObject.get("count").getAsInt());
		productListResponse.setTotal(responseObject.get("total").getAsInt());
		JsonArray	responseArray = responseObject.get(RESULTS).getAsJsonArray();
		
		CustomerResponseBean customerResponseBean = null;
		if(StringUtils.hasText(customerId)) {
			customerResponseBean = registrationHelperService.getCustomerById(customerId.trim());
		}
		for(int i=0; i<responseArray.size(); i++) {
			JsonObject jsonObject = responseArray.get(i).getAsJsonObject();
			String productId = jsonObject.get("id").getAsString();
			ProductResponse productResponse = getProductById(productId, customerId, null, token, customerResponseBean,country).getBody();
			responseList.add(productResponse);
		}
		productListResponse.setProductResponseList(responseList);
		return new ResponseEntity<>(productListResponse, HttpStatus.OK);
	}

	public ResponseEntity<String> getCategories() {
		String categoryUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/categories";
		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, categoryUrl);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public ResponseEntity<String> getProductsByCategories(List<String> categoryKeys) {
		String token = ctServerHelperService.getStringAccessToken();
		StringBuilder categoryUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/categories?where=key in(");
		appendUrl(categoryKeys, categoryUrl);
		categoryUrl.append(")");
		String categoryResponse = netConnectionHelper.sendGetWithoutBody(token, categoryUrl.toString());
		JsonObject categoryJsonObject = MolekuleUtility.parseJsonObject(categoryResponse);
		JsonArray resultArray = categoryJsonObject.get("results").getAsJsonArray();
		List<String> categoryList = new ArrayList<>();
		for(int i=0;i<resultArray.size();i++) {
			categoryList.add(resultArray.get(i).getAsJsonObject().get("id").getAsString());
		}
		StringBuilder productProjectionUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/product-projections/search?filter=categories.id :");
		appendUrl(categoryList, productProjectionUrl);
		productProjectionUrl.append("&expand=categories[*].typeId");
		String productProjectionResponse = netConnectionHelper.sendGetWithoutBody(token, productProjectionUrl.toString());
		return new ResponseEntity<>(productProjectionResponse, HttpStatus.OK);
	}

	private void appendUrl(List<String> valueList, StringBuilder baseUrl) {
		for(int i=0;i<valueList.size();i++) {
			if(valueList.size()==1 || i==valueList.size()-1) {
				baseUrl.append("\"").append(valueList.get(i)).append("\"");
			}else {
				baseUrl.append("\"").append(valueList.get(i)).append("\"").append(",");
			}
		}
	}
}
