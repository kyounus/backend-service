package com.molekule.api.v1.productservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.model.products.ProductListResponse;
import com.molekule.api.v1.commonframework.model.registration.ProductResponse;
import com.molekule.api.v1.commonframework.model.registration.ProductResponseBean;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.productservices.service.ProductService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * The class ProductsController is used to write products related apis.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1")
public class ProductsController {

	@Autowired
	@Qualifier("productService")
	ProductService productService;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;
	
	Logger logger = LoggerFactory.getLogger(ProductsController.class);

	/**
	 * getProductById method is used to get the product information by passing productId.
	 * 
	 * @param productId
	 * @param customerId
	 * @return ResponseEntity<ProductResponse>
	 */
	@GetMapping(value = "/products/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get product information by product id", response = ProductResponseBean.class)
	public ResponseEntity<ProductResponse> getProductById(
			@ApiParam(value = "The product id", required = true) @PathVariable(PRODUCT_ID) String productId,
			@ApiParam(value = "The customer id", required=false) @RequestParam(value =CUSTOMER_ID, defaultValue=" ") String customerId,
			@RequestHeader("country") String country) {
		String token = ctServerHelperService.getStringAccessToken();
		return productService.getProductById(productId, customerId, null, token, null,country);
	} 
	
	/**
	 * getProductBySku method is used to get the product information by passing sku.
	 * 
	 * @param sku
	 * @param customerId 
	 * @return ResponseEntity<ProductResponse>
	 */
	@GetMapping(value = "/products/sku", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get product information by sku", response = ProductResponseBean.class)
	public ResponseEntity<ProductResponse> getProductBySku(
			@ApiParam(value = "Sku", required = true) @RequestParam("sku") String sku,
			@ApiParam(value = "The customer id", required=false) @RequestParam(value =CUSTOMER_ID, defaultValue=" ") String customerId,
			@RequestHeader("country") String country) {
		return productService.getProductBySku(sku, customerId,country);
	} 
	
	/**
	 * getAllProducts method is used to get all the products.
	 * 
	 * @param customerId
	 * @param limit
	 * @param offset
	 * @return ResponseEntity<ProductListResponse>
	 */
	@GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all products", response = ProductListResponse.class)
	public ResponseEntity<ProductListResponse> getAllProducts(
			@ApiParam(value = "The customer id", required=false) @RequestParam(value =CUSTOMER_ID, defaultValue=" ") String customerId,
			@ApiParam(value = "The limit is",example = "20", required = false) @RequestParam int limit,
			@ApiParam(value = "The offset is",example = "0", required = false) @RequestParam int offset,
			@RequestHeader("country") String country) {
		return productService.getAllProducts(customerId, limit, offset,country);
	} 
	
	/**
	 * getCategories method is used to get the product categories from CT.
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all categories")
	public ResponseEntity<String> getCategories() {
		return productService.getCategories();
	} 
	
	/**
	 * getProductsByCategories method is used to get the products based on category from CT.
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/categories/products", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all product categories")
	public ResponseEntity<String> getProductsByCategories(@RequestParam List<String> categoryKeys) {
		return productService.getProductsByCategories(categoryKeys);
	} 
}
