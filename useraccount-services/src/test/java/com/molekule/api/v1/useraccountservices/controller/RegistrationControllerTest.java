package com.molekule.api.v1.useraccountservices.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.molekule.api.v1.commonframework.model.registration.CompanyInfoBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.useraccountservices.service.AmazonS3Service;
import com.molekule.api.v1.useraccountservices.service.RegistrationService;

public class RegistrationControllerTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	RegistrationController registrationController;

	@Mock
	Logger logger;

	@Mock
	RegistrationService registrationService;

	@Mock
	private AmazonS3Service amazonS3Service;

	@Test
	public void getRegistrationData() {
		RegistrationBean registrationBean = Mockito.mock(RegistrationBean.class);
		Assert.assertNull(registrationController.getRegistrationData(registrationBean));
	}

	@Test
	public void getCompanyInfo() {
		CompanyInfoBean companyInfoBean = Mockito.mock(CompanyInfoBean.class);
		Assert.assertNull(registrationController.getCompanyInfo(companyInfoBean));
	}

	@Test
	public void saveShippingAddress() {
		ShippingAddressRequestBean shippingAddressRequestBean = Mockito.mock(ShippingAddressRequestBean.class);
		logger.trace("execuing shipping address...");
		Assert.assertNull(registrationController.saveShippingAddress(shippingAddressRequestBean));
	}

	@Test
	public void uploadFile() {
		MultipartFile file = Mockito.mock(MultipartFile.class);
		Assert.assertNull(registrationController.uploadFile(file, "customerId"));
	}

	@Test
	public void deleteFile() {
		Assert.assertNull(registrationController.deleteFile("fileName"));
	}

	@Test
	public void getUploadedFilesByCustomerId() {
		Assert.assertNotNull(registrationController.getUploadedFilesByCustomerId("customerId"));
	}

	@Test
	public void createPaymentOption() {
		PaymentRequestBean paymentRequestBean = Mockito.mock(PaymentRequestBean.class);
		Assert.assertNull(registrationController.createPaymentOption("customerId", paymentRequestBean));
	}

	@Test
	public void updatePaymentOption() {
		UpdatePaymentRequestBean updatePaymentRequestBean = Mockito.mock(UpdatePaymentRequestBean.class);
		Assert.assertNull(
				registrationController.updatePaymentOption("customerId", "paymentId", updatePaymentRequestBean));
	}

	@Test
	public void createShippingOptions() {
		ShippingCarrierOptionsBean shippingCarrierOptionsBean = Mockito.mock(ShippingCarrierOptionsBean.class);
		Assert.assertNull(registrationController.createShippingOptions("customerId", shippingCarrierOptionsBean));
	}

	@Test
	public void updateShippingOptions() {
		ShippingCarrierOptionsBean shippingCarrierOptionsBean = Mockito.mock(ShippingCarrierOptionsBean.class);
		Assert.assertNull(registrationController.updateShippingOptions("customerId", "shippingAddressId",
				shippingCarrierOptionsBean));
	}

	@Test
	public void createCarrierOptions() {
		ShippingCarrierOptionsBean shippingCarrierOptionsBean = Mockito.mock(ShippingCarrierOptionsBean.class);
		Assert.assertNull(registrationController.createCarrierOptions("customerId", shippingCarrierOptionsBean));
	}

	@Test
	public void updateCarrierOptions() {
		ShippingCarrierOptionsBean shippingCarrierOptionsBean = Mockito.mock(ShippingCarrierOptionsBean.class);
		Assert.assertNull(
				registrationController.updateCarrierOptions("customerId", "carrierId", shippingCarrierOptionsBean));
	}

	@Test
	public void deleteCarrier() {
		Assert.assertNull(registrationController.deleteCarrier("customerId", "carrierId"));
	}

	@Test
	public void deleteShippingOptions() {
		Assert.assertNull(registrationController.deleteShippingOptions("customerId", "shippingAddressId"));
	}

	@Test
	public void updateInvoiceEmail() {
		Assert.assertNull(registrationController.updateInvoiceEmail("customerId", "invoiceEmail"));
	}

	@Test
	public void getAvalaraCertcaptureeCommerceToken() {
		Assert.assertNull(registrationService.getAvalaraCertcaptureeCommerceToken("customerId"));
	}

}