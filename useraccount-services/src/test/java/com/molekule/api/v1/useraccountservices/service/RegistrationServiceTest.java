package com.molekule.api.v1.useraccountservices.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.registration.CarrierOption;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@ContextConfiguration
public class RegistrationServiceTest {
	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	RegistrationService registrationService;

	@Mock
	Logger logger;

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	TemplateMailRequestHelper registartionConfirmationSender;

	@Mock
	TemplateMailRequestHelper paymentTermsPendingApprovalSender;

	@Mock
	TemplateMailRequestHelper paymentTermsRequestConfirmationSender;

	@Mock
	TemplateMailRequestHelper paymentTermsSuccessConfirmationSender;

	@Mock
	TemplateMailRequestHelper taxExemptionPendingApprovalSender;

	@Mock
	TemplateMailRequestHelper taxExemptionSuccessConfirmationSender;

	@Mock
	TemplateMailRequestHelper freightInformationConfirmationSender;

	@Mock
	NetConnectionHelper netConnectionHelper;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	RegistrationHelperService registrationHelperService;

	@Mock
	CheckoutHelperService checkoutHelperService;

	@Mock
	CustomerDTO customerDTO;

	@Mock
	CustomerGroupRequestBean customerGroupRequestBean;
	
	@Mock
	RegistrationBean registrationBean;

	private final String CUSTOMER_ID = "1234";
	private final String ACCOUNT_ID = "5678";
	private final String TEST_EMAIL = "abcd@test.com";

	public void createCustomer() {
		BDDMockito.when(customerDTO.getCompanyName()).thenReturn("test company");
		BDDMockito.when(customerDTO.getFirstName()).thenReturn("test");
		registrationService.createCustomer(customerDTO, registrationBean);
	}

	public void updateCustomerId() {
		registrationService.updateCustomerId("customerId", customerDTO, "token");
	}

	public void assignCustomerGroup() {
		registrationService.assignCustomerGroup("customerId", customerGroupRequestBean, "token");
	}

	public void updateCompanyInfo() {
		CompanyInfoBean companyInfoBean = Mockito.mock(CompanyInfoBean.class);
		registrationService.updateCompanyInfo(companyInfoBean);

	}

	public void getAccountID() {
		AccountCustomObject accountCustomObject = Mockito.mock(AccountCustomObject.class);
		registrationService.getAccountID(accountCustomObject, "token");
	}

	public void addShippingAddress() {
		ShippingAddressRequestBean shippingAddressRequestBean = Mockito.mock(ShippingAddressRequestBean.class);
		registrationService.addShippingAddress(shippingAddressRequestBean);
	}

	public void sendPostRequest() {
		registrationService.sendPostRequest("https://test.com", "token", new Object());

	}

	public void createPaymentOption() {
		PaymentRequestBean paymentRequestBean = Mockito.mock(PaymentRequestBean.class);
		registrationService.createPaymentOption("customerId", paymentRequestBean);
	}

	public void updatePaymentOption() {
		UpdatePaymentRequestBean updatePaymentRequestBean = Mockito.mock(UpdatePaymentRequestBean.class);
		registrationService.updatePaymentOption("customerId", "paymentID", updatePaymentRequestBean, "accountId");

	}

	//@Test
	public void createShippingOptions() {
		ShippingCarrierOptionsBean shippingCarrierOptionsBean = Mockito.mock(ShippingCarrierOptionsBean.class);
		registrationService.createShippingOptions("testCustomerId", shippingCarrierOptionsBean);
	}

	@Test
	public void updateShippingOptions() {
		ShippingCarrierOptionsBean updateShippingOptions = Mockito.mock(ShippingCarrierOptionsBean.class);
		Assert.assertNotNull(registrationService.updateShippingOptions("testCustomerId", "shippingAddressId", updateShippingOptions));
	}

	//@Test
	public void createCarrierOptions() {
		List<CarrierOption> carriers = new ArrayList<>();
		registrationService.createCarrierOptions("testCustomerId", carriers);

	}

	@Test
	public void updateCarrierOptions() {
		List<CarrierOption> carriers = new ArrayList<>();
		Assert.assertNotNull(registrationService.updateCarrierOptions("testCustomerId", "carrierId", carriers));

	}

	@Test
	public void deleteCarrierOptions() {
		Assert.assertNotNull(registrationService.deleteCarrierOptions("customerId", "carrierId"));

	}

	@Test
	public void deleteShippingOptions() {
		Assert.assertNotNull(registrationService.deleteShippingOptions("customerId", "shippingAddressId"));

	}

	// @Test
	public void updateInvoiceEmail() {
		final String emailRepsonse = "{ \"version\":\"1\", \"custom\":{ \"type\":{ \"id\":\"1234\" }, \"fields\":{ \"admin\":\"true\", \"accountId\":\"true\", \"salutation\":\"salutation\", \"customerGroupName\":\"testGroup\", \"phone\":\"123445\" } } }";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", "null/null/customers/1234"))
				.thenReturn(emailRepsonse);
		registrationService.updateInvoiceEmail(CUSTOMER_ID, TEST_EMAIL);
	}

	@Test
	public void updateInvoiceEmailASNull() {
		Assert.assertNotNull(registrationService.updateInvoiceEmail(null, null));
	}

	@Test
	public void createEmailDynamicInfoInvalid() {
		CustomerResponseBean customer = Mockito.mock(CustomerResponseBean.class);
		BDDMockito.when(registrationHelperService.getCustomerById(CUSTOMER_ID)).thenReturn(customer);
		BDDMockito.when(registrationHelperService.getAccountIdWithCustomerId(CUSTOMER_ID)).thenReturn(ACCOUNT_ID);
		BDDMockito.when(registrationHelperService.getCTCustomObjectsByAccountIdUrl(ACCOUNT_ID)).thenReturn(ACCOUNT_ID);
		final String BAD_RESPONSE = "{   \"statusCode\": \"400\" }";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, ACCOUNT_ID)).thenReturn(BAD_RESPONSE);
		Assert.assertNull(registrationHelperService.createEmailDynamicInfo(CUSTOMER_ID));

	}

	@Test
	public void createEmailDynamicInfo() {
		CustomerResponseBean customer = Mockito.mock(CustomerResponseBean.class);
		BDDMockito.when(registrationHelperService.getCustomerById(CUSTOMER_ID)).thenReturn(customer);
		BDDMockito.when(registrationHelperService.getAccountIdWithCustomerId(CUSTOMER_ID)).thenReturn(ACCOUNT_ID);
		BDDMockito.when(registrationHelperService.getCTCustomObjectsByAccountIdUrl(ACCOUNT_ID)).thenReturn(ACCOUNT_ID);
		final String actualResponse = "{ \"container\":\"b2b\", \"key\":\"TestCompany395Sudalai\", \"version\":2, \"results\":[ { \"value\":{ \"id\":null, \"companyName\":\"TestCompany\", \"salesRepresentative\":{ \"id\":\"b779377d-0a92-48c6-a7e7-016da03f5922\", \"name\":\"Pavithratest\", \"phone\":\"123456789\", \"email\":\"test@gmail.com\", \"firstName\":\"Pavithra\", \"lastName\":\"test\", \"calendlyLink\":\"string\" }, \"companyCategory\":{ \"id\":\"5cb62872-fa6e-422e-80ef-dadd35ec3490\", \"name\":\"OtherHealthcare\", \"sales-rep-id\":\"b779377d-0a92-48c6-a7e7-016da03f5922\" } } } ] }";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, ACCOUNT_ID)).thenReturn(actualResponse);

		Assert.assertNull(registrationHelperService.createEmailDynamicInfo(CUSTOMER_ID));

	}

	public void getAvalaraCertcaptureeCommerceToken() {
		registrationService.getAvalaraCertcaptureeCommerceToken("customerId");
	}

	@Test
	public void isAccountLocked() {
		String customerBaseURL = "null/null/customers/customerId";
		String customerResponse = "{\"version\":\"1\",\"custom\":{\"fields\":{\"accountLocked\":\"true\"}}}";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", customerBaseURL))
				.thenReturn(customerResponse);
		Assert.assertNotNull(registrationService.isAccountLocked("customerId"));
	}
}