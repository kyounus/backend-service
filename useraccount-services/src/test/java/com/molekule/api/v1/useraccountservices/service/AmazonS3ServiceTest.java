package com.molekule.api.v1.useraccountservices.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.useraccountservices.configuration.UserAccountConfiguration;

@ContextConfiguration
public class AmazonS3ServiceTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	AmazonS3Service amazonS3Service;

	@Mock
	private AmazonS3 amazonS3;

	@Mock
	@Qualifier("userAccountConfiguration")
	UserAccountConfiguration userAccountConfiguration;

	@Mock
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Mock
	TemplateMailRequestHelper taxExemptionConfirmationSender;

	@Mock
	Logger logger;

	@Mock
	MultipartFile multipartFile;

	public void uploadFileToS3Bucket() {
		amazonS3Service.uploadFileToS3Bucket(multipartFile, true, "testCustomerId");
	}

//	@Test
	public void deleteFileFromS3Bucket() {
		amazonS3Service.deleteFileFromS3Bucket("testFileName");
	}

	public void getUploadedFilesByCustomerId() {
		amazonS3Service.getUploadedFilesByCustomerId("customerId");
	}

	public void downloadFile() {
		amazonS3Service.downloadFile("customerId");
	}

	@Test
	public void fileNameTest() {
		Assertions.assertEquals(true, AmazonS3Service.isValidFileName("test1.pdf"));
		Assertions.assertEquals(false, AmazonS3Service.isValidFileName("test1..pdf"));
		Assertions.assertEquals(false, AmazonS3Service.isValidFileName("test@*1.pdf"));
		Assertions.assertEquals(false, AmazonS3Service.isValidFileName("#%test1.pdf"));
		Assertions.assertEquals(false, AmazonS3Service.isValidFileName("#%test1.pdf.pdf"));
		Assertions.assertEquals(false, AmazonS3Service.isValidFileName("#%test1...pdf"));
		Assertions.assertEquals(true, AmazonS3Service.isValidFileName("te_st_1.pdf"));
		Assertions.assertEquals(true, AmazonS3Service.isValidFileName("test-2.pdf"));
		Assertions.assertEquals(true, AmazonS3Service.isValidFileName("t-_0est-2.pdf"));

	}

}