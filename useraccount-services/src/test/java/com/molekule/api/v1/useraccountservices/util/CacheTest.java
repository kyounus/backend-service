package com.molekule.api.v1.useraccountservices.util;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
class CacheTest {

	Cache cache = Cache.getInstance();

	private static final String TEST_KEY = "testKey";
	private static final String TEST_VALUE = "testValue";
	private static final String TEST_INVALID_VALUE = "testValue1";

	@Test
	void getCacheTest() {
		cache.addCache(TEST_KEY, TEST_VALUE);
		Assert.assertEquals(TEST_VALUE, cache.get(TEST_KEY));
		Assert.assertNotEquals(TEST_INVALID_VALUE, cache.get(TEST_KEY));
		Assert.assertEquals(true, cache.containsKey(TEST_KEY));
	}

}
