package com.molekule.api.v1.useraccountservices.configuration;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
public class UserAccountConfigurationTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	UserAccountConfiguration userAccountConfiguration;

	@Test
	public void getDocket() {
		Assert.assertNotNull(userAccountConfiguration.getDocket());
	}

	@Test
	public void crosConfigure() {
		Assert.assertNotNull(userAccountConfiguration.crosConfigure());
	}

	@Test
	public void getApiInfo() {
		Assert.assertNotNull(userAccountConfiguration.getApiInfo());
	}

	@Test
	public void cTEnvProperties() {
		Assert.assertNotNull(userAccountConfiguration.cTEnvProperties());

	}

	@Test
	public void netConnectionHelper() {
		Assert.assertNotNull(userAccountConfiguration.netConnectionHelper());

	}

	@Test
	public void ctServerHelperService() {
		Assert.assertNotNull(userAccountConfiguration.ctServerHelperService());
	}

	@Test
	public void registrationHelperService() {
		Assert.assertNotNull(userAccountConfiguration.registrationHelperService());

	}

	@Test
	public void checkoutHelperService() {
		Assert.assertNotNull(userAccountConfiguration.checkoutHelperService());

	}

	@Test
	public void orderHelperService() {
		Assert.assertNotNull(userAccountConfiguration.orderHelperService());

	}

	@Test
	public void cartHelperService() {
		Assert.assertNotNull(userAccountConfiguration.cartHelperService());
	}

	@Test
	public void taxService() {
		Assert.assertNotNull(userAccountConfiguration.taxService());

	}

	@Test
	public void handlingCostHelperService() {
		Assert.assertNotNull(userAccountConfiguration.handlingCostHelperService());

	}

	@Test
	public void parcelHandlingCostCalculationServieImpl() {
		Assert.assertNotNull(userAccountConfiguration.parcelHandlingCostCalculationServieImpl());
	}

	@Test
	public void freightHandlingCostCalculationServiceImpl() {
		Assert.assertNotNull(userAccountConfiguration.freightHandlingCostCalculationServiceImpl());

	}

	@Test
	public void csrHelperService() {
		Assert.assertNotNull(userAccountConfiguration.csrHelperService());

	}

	@Test
	public void tealiumHelperService() {
		Assert.assertNotNull(userAccountConfiguration.tealiumHelperService());

	}

	// @Test
	public void getAWSKeyId() {
		Assert.assertNotNull(userAccountConfiguration.getAWSKeyId());
	}

	// @Test
	public void getAWSKeySecret() {
		Assert.assertNotNull(userAccountConfiguration.getAWSKeySecret());
	}

	@Test
	public void getAWSS3BucketName() {
		Assert.assertNull(userAccountConfiguration.getAWSS3BucketName());
	}

	@Test
	public void registartionConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.registartionConfirmationSender());
	}

	@Test
	public void paymentTermsPendingApprovalSender() {
		Assert.assertNotNull(userAccountConfiguration.paymentTermsPendingApprovalSender());
	}

	@Test
	public void paymentTermsRequestConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.paymentTermsRequestConfirmationSender());
	}

	@Test
	public void paymentTermsSuccessConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.paymentTermsSuccessConfirmationSender());
	}

	@Test
	public void taxExemptionPendingApprovalSender() {
		Assert.assertNotNull(userAccountConfiguration.taxExemptionPendingApprovalSender());
	}

	@Test
	public void taxExemptionSuccessConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.taxExemptionSuccessConfirmationSender());
	}

	@Test
	public void registrationBySalesRepConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.registrationBySalesRepConfirmationSender());
	}

	@Test
	public void freightInformationConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.freightInformationConfirmationSender());
	}

	@Test
	public void subscriptionSignupConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.subscriptionSignupConfirmationSender());
	}

	@Test
	public void subScriptionRenewalDateUpdationSender() {
		Assert.assertNotNull(userAccountConfiguration.subScriptionRenewalDateUpdationSender());
	}

	@Test
	public void customerProfileUtility() {
		Assert.assertNotNull(userAccountConfiguration.customerProfileUtility());
	}

	@Test
	public void notificationHelperService() {
		Assert.assertNotNull(userAccountConfiguration.notificationHelperService());
	}

	@Test
	public void quoteRequestConfirmationMailSender() {
		Assert.assertNotNull(userAccountConfiguration.quoteRequestConfirmationMailSender());
	}

	@Test
	public void quoteRequestPendingApprovalMailSender() {
		Assert.assertNotNull(userAccountConfiguration.quoteRequestPendingApprovalMailSender());
	}

	@Test
	public void paymentReceivedConfirmationSender() {
		Assert.assertNotNull(userAccountConfiguration.paymentReceivedConfirmationSender());
	}

	@Test
	public void orderShippedMailSender() {
		Assert.assertNotNull(userAccountConfiguration.orderShippedMailSender());
	}

}