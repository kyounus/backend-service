package com.molekule.api.v1.useraccountservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.molekule.api.v1.commonframework.model.customerprofile.UpdateAutoRefillsRequestBean;
import com.molekule.api.v1.useraccountservices.service.AuroraService;
import com.molekule.api.v1.useraccountservices.service.CustomerProfileService;

public class CustomerProfileControllerTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CustomerProfileController customerProfileController;

	@Mock
	AuroraService auroraService;

	@Mock
	CustomerProfileService customerProfileService;

//	@Test
	public void getCustomerProfile() {
		Assert.assertNull(customerProfileController.getCustomerProfile("customerId", "10", 10));
	}

	@Test
	public void getSubscriptions() {
		Assert.assertNull(
				customerProfileController.getSubscriptions(20, 0, "customerId", "shippingAddressId", "paymentId"));
	}

	@Test
	public void getAllSubscriptions() {
		Assert.assertNull(customerProfileController.getAllSubscriptions(10, 10, "", "testemail@test.com", null, null,
				null, null, null));
	}

	@Test
	public void updateAutoRefills() {
		UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean = Mockito.mock(UpdateAutoRefillsRequestBean.class);
		Assert.assertNull(customerProfileController.updateAutoRefills("customerId", CONTAINER, "key",
				updateAutoRefillsRequestBean));
	}

	@Test
	public void getAllSurveysResponse() {
		Assert.assertNull(customerProfileController.getAllSurveysResponse(null));
	}

	@Test
	public void getSurveysResponseById() {

		Assert.assertNull(customerProfileController.getSurveysResponseById("id"));
	}

	@Test
	public void addSurveyResponse() {
		Assert.assertNull(customerProfileController.addSurveyResponse("question", null));
	}

	@Test
	public void updateSurveyResponse() {
		Assert.assertNull(customerProfileController.updateSurveyResponse("id", "question"));
	}

}