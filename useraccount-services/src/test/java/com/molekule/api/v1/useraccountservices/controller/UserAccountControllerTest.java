package com.molekule.api.v1.useraccountservices.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.molekule.api.v1.commonframework.model.aurora.AuroraCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.AuroraUpdateCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepRequestBean;
import com.molekule.api.v1.commonframework.model.registration.AccountCustomResponseBean;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReviewsRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.useraccountservices.service.AuroraService;
import com.molekule.api.v1.useraccountservices.service.RegistrationService;

public class UserAccountControllerTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	UserAccountController userAccountController;
	@Mock
	AuroraService auroraService;

	@Mock
	CsrHelperService csrHelperService;

	@Mock
	RegistrationService registrationService;

	@Test
	public void getListOfCustomers() {
		Assert.assertNull(userAccountController.getListOfCustomers(10, 20, "test", "testEmail.com", null, null, null,
				null, null));
	}

	@Test
	public void getListOfAccounts() {
		Assert.assertNull(userAccountController.getListOfAccounts(10, 20, "test", "testEmail.com"));
	}

	// @Test
	public void getCompanyCategories() {
		Assert.assertNull(userAccountController.getCompanyCategories());
	}

	@Test
	public void getCustomers() {
		Assert.assertNotNull(userAccountController.getCustomers("testAccountId", "testCustomerId"));
	}

	@Test
	public void listCustomersByAccount() {
		Assert.assertNotNull(userAccountController.listCustomersByAccount("testAccountId"));
	}

	@Test
	public void getSalesRep() {
		Assert.assertNull(userAccountController.getSalesRep(10, 5));
	}

	@Test
	public void getListCustomerGroups() {
		Assert.assertNull(userAccountController.getListCustomerGroups());
	}

	@Test
	public void createAccount() {
		AccountCustomResponseBean accountCustomResponseBean = Mockito.mock(AccountCustomResponseBean.class);
		Assert.assertNull(userAccountController.createAccount(accountCustomResponseBean));
	}

	@Test
	public void createTaxExemptReviews() {
		TaxExemptReviewsRequestBean taxExemptReviewsRequestBean = Mockito.mock(TaxExemptReviewsRequestBean.class);
		Assert.assertNull(userAccountController.createTaxExemptReviews("testAccountId", taxExemptReviewsRequestBean));
	}

	@Test
	public void createCustomer() {
		AuroraCustomerRequestBean customerRequestBean = Mockito.mock(AuroraCustomerRequestBean.class);
		Assert.assertNull(userAccountController.createCustomer("accountId", customerRequestBean));
	}

	@Test
	public void updateCustomer() {
		AuroraUpdateCustomerRequestBean customerRequestBean = Mockito.mock(AuroraUpdateCustomerRequestBean.class);
		Assert.assertNull(userAccountController.updateCustomer("accountId", "customerId", customerRequestBean));
	}

	@Test
	public void assignCustomerGroup() {
		Assert.assertNull(userAccountController.assignCustomerGroup("customerId", "customerGroupId"));
	}

	@Test
	public void assignSalesrepToCompany() {
		Assert.assertNull(userAccountController.assignSalesrepToCompany("categoryId", "salesRepId"));
	}

	@Test
	public void removeTaxExemptReviews() {
		Assert.assertNull(userAccountController.removeTaxExemptReviews("accountId", "certificateId"));
	}

	@Test
	public void createCompanyCategory() {
		Assert.assertNull(userAccountController.createCompanyCategory("categoryName", "salesRepId"));
	}

	@Test
	public void removeCompanyCategory() {
		Assert.assertNull(userAccountController.removeCompanyCategory("categoryId"));
	}

	// @Test
	public void getCustomerById() {
		Assert.assertNull(userAccountController.getCustomerData("customerId", "12", "", null));
	}

	@Test
	public void updatePaymentOptionByAccount() {
		UpdatePaymentRequestBean updatePaymentRequestBean = Mockito.mock(UpdatePaymentRequestBean.class);
		Assert.assertNull(
				userAccountController.updatePaymentOptionByAccount("accountId", "paymentId", updatePaymentRequestBean));
	}

	@Test
	public void createSalesRepresentative() {
		SalesRepRequestBean salesRequestBean = Mockito.mock(SalesRepRequestBean.class);
		Assert.assertNull(userAccountController.createSalesRepresentative(salesRequestBean));
	}

	@Test
	public void updateSalesRepresentative() {
		SalesRepRequestBean salesRequestBean = Mockito.mock(SalesRepRequestBean.class);
		Assert.assertNull(auroraService.updateSalesRepresentative("id", salesRequestBean));
	}
}
