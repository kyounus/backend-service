package com.molekule.api.v1.useraccountservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_GROUP_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.ResourceUtils;

import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartDTO;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.CarrierOption;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;

public class RegistrationUtilityTest {

	String testString = null;

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
		try {
			File file = ResourceUtils.getFile("classpath:value.json");
			testString = new String(Files.readAllBytes(file.toPath()));

		} catch (FileNotFoundException e) {
		} catch (IOException e) {

		}

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Mock
	AccountCustomObject accountCustomObject;

	@Mock
	ShippingCarrierOptionsBean shippingCarrierOptionsBean;

	@Mock
	CarrierOption carrierOption;

	@Mock
	UpdatePaymentRequestBean updatePaymentRequestBean;

	@Mock
	Payments payments;

	JsonObject jsonObject = new JsonObject();

	@Mock
	CustomerDTO customerDTO;

	@Test
	public void convertToJsonObject() {
		String jsonValue = "{\r\n" + "  \"customerId\": \"string\",\r\n" + "  \"tealiumVisitorId\": \"string\"\r\n"
				+ "}";
		Assert.assertNotNull(RegistrationUtility.convertToJsonObject(jsonValue, CreateCartRequestBean.class));
	}

	@Test
	public void convertObjectToJsonStr() {
		CreateCartDTO createCartDTO = new CreateCartDTO();
		Assert.assertNotNull(RegistrationUtility.convertObjectToJsonStr(createCartDTO));
	}

	@Test
	public void createCustomObj() {
		Map<String, Object> createCustomObjectParamMap = new HashMap<>();
		createCustomObjectParamMap.put("typeId", "123");
		createCustomObjectParamMap.put("accountId", "123");
		createCustomObjectParamMap.put("salutation", "testsalutaion");
		createCustomObjectParamMap.put("email", "testEmail");
		createCustomObjectParamMap.put("phone", "123456789");
		createCustomObjectParamMap.put(CUSTOMER_GROUP_NAME, "testGroupName");
		createCustomObjectParamMap.put("countryCode", "US");
		createCustomObjectParamMap.put("store", "MLK US - Website");
		createCustomObjectParamMap.put(CHANNEL, "B2B");
		createCustomObjectParamMap.put("termsOfService", true);
		createCustomObjectParamMap.put("marketingOffers", true);
		Assert.assertNotNull(RegistrationUtility.createCustomObj(true, createCustomObjectParamMap));
	}

	@Test
	public void getAccountCustomObject() {

		Assert.assertNotNull(
				RegistrationUtility.getAccountCustomObject(CONTAINER, "key", new Object(), accountCustomObject, "123"));

	}

	@Test
	public void populateShippingOptions() {

		RegistrationUtility.populateShippingOptions(CONTAINER, "key", shippingCarrierOptionsBean, accountCustomObject);
		Assert.assertNotNull(shippingCarrierOptionsBean);
	}

	@Test
	public void populateCarrierOptions() {

		List<CarrierOption> data = new ArrayList<CarrierOption>();

		RegistrationUtility.populateCarrierOptions(CONTAINER, "key", data, accountCustomObject);
		Assert.assertNotNull(accountCustomObject);
	}

	// @Test(expected = NullPointerException.class)
	public void updateShippingOptions() {
		RegistrationUtility.updateShippingOptions("shippingAddressId", shippingCarrierOptionsBean, accountCustomObject);
	}

	@Test(expected = NullPointerException.class)
	public void updateCarrierOptions() {
		RegistrationUtility.updateCarrierOptions("shippingAddressId", carrierOption, accountCustomObject);
		Assert.assertNotNull(carrierOption);
	}

	@Test
	public void getCustomerResponse() {

		Assert.assertNotNull(RegistrationUtility.getCustomerResponse(CUSTOMER_ID, "accountId", customerDTO));
	}

	@Test
	public void generateKey() {
		Assert.assertNotNull(RegistrationUtility.generateKey("testCompanyName", "name"));
	}

	@Test
	public void getCustomerNumber() {
		Assert.assertNotNull(RegistrationUtility.getCustomerNumber());
	}

	@Test(expected = NullPointerException.class)
	public void createCustomerObj() {
		Assert.assertNotNull(RegistrationUtility.createCustomerObj(testString));
	}

	@Test
	public void getCategoryListDto() {
		Assert.assertNotNull(MolekuleUtility.getCategoryListDto(testString));

	}

	// @Test(expected = NullPointerException.class)
	public void getSalesRepresentativeDtoList() {
		MolekuleUtility.getSalesRepresentativeDtoList(testString);
	}

	@Test
	public void createOrderNumber() {
		Assert.assertNotNull(RegistrationUtility.createOrderNumber());
	}

	@Test(expected = NullPointerException.class)
	public void updatePaymentsObject() {
		RegistrationUtility.updatePaymentsObject("billingAddressId", payments, jsonObject, updatePaymentRequestBean);
		Assert.assertNotNull(updatePaymentRequestBean);
	}

	@Test(expected = NullPointerException.class)
	public void updateAddressList() {
		List<BillingAddresses> billingAddressList = new ArrayList<BillingAddresses>();
		List<ShippingAddresses> shippingList = new ArrayList<ShippingAddresses>();
		RegistrationUtility.updateAddressList(updatePaymentRequestBean, "billingAddressId", new JsonObject(),
				billingAddressList, shippingList);
		Assert.assertNotNull(updatePaymentRequestBean);
	}
}
