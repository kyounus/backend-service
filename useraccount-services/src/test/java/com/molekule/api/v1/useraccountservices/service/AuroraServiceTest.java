package com.molekule.api.v1.useraccountservices.service;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.aurora.AuroraCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.AuroraUpdateCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepRequestBean;
import com.molekule.api.v1.commonframework.model.registration.AccountCustomResponseBean;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReviewsRequestBean;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

public class AuroraServiceTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	AuroraService auroraService;

	@Mock
	Logger logger = LoggerFactory.getLogger(AuroraService.class);

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	RegistrationService registrationService;

	@Mock
	TemplateMailRequestHelper registrationBySalesRepConfirmationSender;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	RegistrationHelperService registrationHelperService;

	@Mock
	NetConnectionHelper netConnectionHelper;
	
	@Mock
	AuroraHelperService auroraHelperService;

	@Mock
	SalesRepRequestBean salesRequestBean;

	@Mock
	TaxExemptReviewsRequestBean taxExemptReviewsRequestBean;

	@Mock
	AuroraCustomerRequestBean auroraCustomerRequestBean;

	
	private final String CUSTOMER_RESPONSE = "{\"limit\":1,\"offset\":0,\"count\":1,\"total\":8192,\"results\":[{\"id\":\"1998ff70-9974-49d0-a078-ed5856aafb8d\",\"version\":6,\"lastMessageSequenceNumber\":2,\"createdAt\":\"2021-06-18T10:45:24.508Z\",\"lastModifiedAt\":\"2021-06-18T10:50:30.036Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"customerNumber\":\"548988\",\"email\":\"vasanth123@mailinator.com\",\"firstName\":\"Vasanth\",\"lastName\":\"ravi\",\"companyName\":\"Vasanth\",\"password\":\"p3IZN4aJoBpC4vFGvw+QUQY4ekxw9e8ZPzF1p82cCUk=$uB0Gv3NJy8Z8v2YYWHYdgq1DkW11KZglsRahkfsULzA=\",\"addresses\":[],\"shippingAddressIds\":[],\"billingAddressIds\":[],\"isEmailVerified\":false,\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\",\"obj\":{\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\",\"version\":1,\"createdAt\":\"2021-04-01T14:53:53.533Z\",\"lastModifiedAt\":\"2021-04-01T14:53:53.533Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"4ef0f3cc-3572-47ba-9f67-5eaa3559fbb2\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"4ef0f3cc-3572-47ba-9f67-5eaa3559fbb2\"}},\"name\":\"MO_Registered\",\"key\":\"MO_Registered\"}},\"key\":\"1998ff70-9974-49d0-a078-ed5856aafb8d\",\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"de4181c9-24f4-4877-a4be-8b2e04b3f2e1\"},\"fields\":{\"customerStatus\":\"ACTIVE\",\"admin\":false,\"accountLocked\":false,\"customerGroupName\":\"MO_Registered\",\"invoiceEmailAddress\":[\"vasanth123@mailinator.com\"],\"cognitoId\":\"d2adafb0-779f-46b8-b224-8b6c58f31ad1\",\"accountId\":\"e69f4ee0-fd5c-4d1b-bfcb-73095d95ac7e\",\"salutation\":\"\",\"phone\":\"9876543234\"}},\"stores\":[]}]}";
	 
	private final String CATEGORY_RESPONSE = "{\"id\":\"d192893b-8a83-44dd-8a59-6e145a32b1ab\",\"version\":1184,\"createdAt\":\"2021-05-04T13:38:42.307Z\",\"lastModifiedAt\":\"2021-06-17T19:48:26.001Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"mo_company_cat\",\"value\":{\"companyCategories\":{\"companyCategory\":[{\"id\":\"5cb62872-fa6e-422e-80ef-dadd35ec3490\",\"name\":\"Other Healthcare\",\"sales-rep-id\":\"b1e9fbfc-145c-45e9-b4f5-58fafa6706e5\"},{\"id\":\"9ec9a10f-7ce5-4157-aa82-91c12f1acc12\",\"name\":\"Nursing Home/Assisted Living\",\"sales-rep-id\":\"328d6b58-0957-4c2a-a603-c91104ced24c\"},{\"id\":\"e10c5f79-4d31-4c4a-8585-64c089152e32\",\"name\":\"Office\",\"sales-rep-id\":\"f1496427-aa86-459d-ac6e-33da01692b31\"},{\"id\":\"fbb19970-98f8-4392-bd7a-af10e9be37d4\",\"name\":\"Restaurant\",\"sales-rep-id\":\"9017c7d3-67a8-4358-83c8-622e75cd5a06\"},{\"id\":\"5aaffac9-f23a-4eb3-86a7-ee7585d2d6da\",\"name\":\"Other\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"6a9e6fb7-f0c8-4ea0-b4eb-f010f1fb8efc\",\"name\":\"Education\",\"sales-rep-id\":\"1d0393bf-923a-4136-8717-07012a817897\"},{\"id\":\"a7ca49a4-0d11-455a-af4d-a28b091d2834\",\"name\":\"Spa\",\"sales-rep-id\":\"07fd4002-560b-4acd-b5dd-e2dc99c855c1\"},{\"id\":\"88b12010-3ada-44fa-b4c8-e68af01a2b6c\",\"name\":\"Hotel\",\"sales-rep-id\":\"07fd4002-560b-4acd-b5dd-e2dc99c855c1\"},{\"id\":\"2dca0a4d-c54e-461c-a947-091d48b0656f\",\"name\":\"School\",\"sales-rep-id\":\"a19f667c-be0f-46dc-8ef5-d71be9b5ac63\"},{\"id\":\"65148275-49db-43a2-8578-9837c2e4eff5\",\"name\":\"Test\",\"sales-rep-id\":\"9017c7d3-67a8-4358-83c8-622e75cd5a06\"},{\"id\":\"d95345a0-64e2-4080-8978-f24b4d39ee57\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"a1640cd6-9266-4e08-9841-30b2992ede6f\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"5efac82c-f8e5-4e11-90d6-6fc9c03eb736\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"eadaa279-4a36-47bd-93cb-424be5ec0151\",\"name\":\"Other11\",\"sales-rep-id\":\"328d6b58-0957-4c2a-a603-c91104ced24c\"},{\"id\":\"ac43a93a-90b2-4ec3-b2dd-8c29c67f78ea\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"de5067be-4a53-4d5b-b03e-319b228d6273\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"4b94793b-60b8-4f8d-8a90-a195c65e8f2a\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"02e92444-8e73-48ea-bc5d-467ccda8f536\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"6aa788c3-a043-40b1-872e-aacb0c488e25\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"3978be06-b4c2-47ca-860c-412f556374cf\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"7cbc2019-f627-43dd-bcf4-f37949138de0\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"4a3f5183-57e6-4776-9007-774fef0f3e7b\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"84f91265-0d76-477f-bf16-e4cd8b9a7d2d\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"ebbd9bf8-47c8-4df7-a364-6c2e9ea98c6e\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"b55864e7-06a0-4f09-8269-245224aa6df6\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"3403130f-5fc9-437b-9fc1-716466160141\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"759feb06-f5f2-4e8e-a99b-2bee347e355f\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"43d23783-ea21-4f8c-b71d-e9fa204e5eae\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"653333af-b835-4ad5-872b-18a3a803b545\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"70c1b047-35a7-444e-9225-69f72a934a47\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"a59f91db-78f9-46c6-9cbe-32aee6b0f609\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"47241c2c-5888-4d05-a553-7e64ee77a60c\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"f81298ee-00bb-42b6-87e0-143026cbdd1a\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"951c4b52-abd6-479d-99ab-92131af349ac\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"ffb96338-3e87-4d82-84c5-13a6e26bc5f1\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"bbf5e458-2dc4-4a07-924a-b4333142e467\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"81d864bd-b53f-4bf6-a9eb-b443e15accaa\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"1686988e-a107-4263-8e22-4061f01ea142\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"14c45c50-6df7-4667-af8e-59e2c9341bfb\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"c2c5611c-f8e8-4fb6-92fd-f8d2636a1860\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"0cc632dc-86cb-4400-9a0f-81c6732d59f6\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"9ecf75c4-1416-421a-98aa-6e779d719dd1\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"19aad408-73de-4adf-8e7f-4a02caaf7eeb\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"775ee12e-63f7-43ca-9cce-eb991921908d\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"c69ff182-0378-4f48-ab63-357c73085ed7\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"f4d2227b-2a94-441f-aa81-c48add0f769d\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"93ef9c78-68d0-445d-b7a5-4bf98e3c68f0\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"02affecc-732c-48e3-bfc7-572babd5ed73\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"ac264ae4-6423-49e5-8935-9e0112ee7eab\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"9a00e000-adcc-4d76-8e7d-41b0f9aa2e49\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"e5a484a5-b9b1-47a8-b74b-96d44cd0f483\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"cfcd1255-214e-4461-9efa-5f1fc181ad7e\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"0d54bc49-76ac-495e-b123-e580ccfaf5d5\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"35152823-3c40-423e-9a90-dda262c7e04a\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"f8ee880d-692f-4f26-ad6b-f4b62cf29a3a\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"ebb3f27d-9262-453f-b6b5-9b146c2555d6\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"708e2816-ff87-4406-86a2-5203bb6109d1\",\"name\":\"Other11\",\"sales-rep-id\":\"fa39bbce-19d8-40e4-b0db-e56564ca2392\"},{\"id\":\"0e782aba-8392-446c-b0b4-5b8325666873\",\"name\":\"Testing\",\"sales-rep-id\":\"55e04367-5439-496f-b6e9-76252979e0ef\"}]},\"employeeIds\":[],\"freightFlag\":false,\"myOwnCarrier\":false,\"parcelFlag\":false,\"taxExemptFlag\":false}}";
	
	private final String SALES_RESPONSE = "{\"id\":\"fbf05fd9-693f-46dd-998e-1e75357119ce\",\"version\":657,\"createdAt\":\"2021-05-14T14:19:47.031Z\",\"lastModifiedAt\":\"2021-06-17T19:48:20.822Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"mo_sales_rep\",\"value\":{\"sales-reps\":[{\"id\":\"67e606ee-c0aa-45dd-a866-fc17d3ccafbb\",\"name\":\" string2\",\"firstName\":\"\",\"lastName\":\"string2\",\"phone\":\"1223\",\"email\":\"st@gmail.com\",\"calendlyLink\":\"string\"},{\"id\":\"8e922aad-da30-4e9f-8236-42b5b3b4e24e\",\"name\":\"Scott Smith\",\"firstName\":\"Scott\",\"lastName\":\"Smith\",\"phone\":\"1999767213456\",\"email\":\"scott-rep@mmolek.com\",\"calendlyLink\":\"\"},{\"id\":\"55e04367-5439-496f-b6e9-76252979e0ef\",\"name\":\"Joe Bill\",\"firstName\":\"Joe\",\"lastName\":\"Bill\",\"phone\":\"19997672134\",\"email\":\"joe-rep@molek.com\",\"calendlyLink\":\"\"},{\"id\":\"b62385ed-2a84-49a0-a0c4-fe7b4eb6a92c\",\"name\":\"Rob Wills\",\"firstName\":\"Rob\",\"lastName\":\"Wills\",\"phone\":\"19997672134\",\"email\":\"rob-rep@molek.com\",\"calendlyLink\":\"\"}],\"employeeIds\":[],\"freightFlag\":false,\"myOwnCarrier\":false,\"parcelFlag\":false,\"taxExemptFlag\":false}}";
	
	private final String ACCOUNT_RESPONSE = "{\"limit\":1,\"offset\":0,\"count\":1,\"total\":1,\"results\":[{\"id\":\"3f65f67c-a45c-4c5d-b4cb-ab6152751d26\",\"version\":21,\"createdAt\":\"2021-03-23T10:46:14.673Z\",\"lastModifiedAt\":\"2021-05-26T08:23:13.730Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"bd577ea6-82f0-4857-adf6-fc8ab315c881\",\"value\":{\"parcelFlag\":false,\"freightFlag\":false,\"noOfEmployees\":\"12\",\"noOfOfficeSpaces\":\"12\",\"employeeIds\":[\"78007a27-34f9-4666-ab4d-c3f4390cd2f9\",\"7a2d72c3-f654-437e-a53b-d69de38de447\",\"196e15b1-df44-4d0b-9d3f-9cd15caf42cd\",\"7cd30584-e971-4550-ac9f-038fd084eb0b\",\"727d7769-53a1-4d81-98fa-e2af302f17f5\",\"bcd6a26d-3368-43e0-b754-b826e514640d\",\"d517a8a6-a547-43ac-ba77-8b63dedef8cb\",\"5efc09a3-3406-40f9-9be3-541cda1c2d00\",\"2a4bb145-ff48-423e-90a5-10298c58f6c1\",\"3db21d19-f237-4c83-aa11-93caaeae2017\",\"92589178-bd35-40b3-9e7f-08e84fae430c\",\"9792bcf3-c798-4590-bb18-28294abf6dcd\",\"8b210905-6f54-47a2-88d8-ca62681bce92\",\"5c316189-46d1-461a-aeef-ae4dac6d77f0\",\"1223f6e1-68e0-4b0f-a62d-73dcd7e5f562\",\"a2a951aa-15f7-4f90-8cea-3051146cfbd7\",\"0c84b3d3-9907-4373-a4c3-eac66ec6c58a\",\"e5cc2b1a-4b59-454a-ab93-f74519617e39\",\"fa29a8f2-44a7-4bb3-a57b-7cd03d95ce3a\"],\"myOwnCarrier\":false,\"taxExemptFlag\":false}}]}";


	public void getListOfCustomerByCompany() throws IOException {
		BDDMockito.when(netConnectionHelper.httpConnectionHelper("null/null/customers?limit=1&offset=1&expand=customerGroup.typeId&sort%3D%22createdAt+desc%22&where=companyName%3D%22testCompanyName%22",null ))
		.thenReturn(CUSTOMER_RESPONSE);
		auroraService.getListOfCustomer(null);
	}
	
	public void getListOfCustomerByEmail() throws IOException {
		BDDMockito.when(netConnectionHelper.httpConnectionHelper("null/null/customers?limit=1&offset=1&expand=customerGroup.typeId&sort%3D%22createdAt+desc%22&where=email%3D%22testEmail%22",null ))
		.thenReturn(CUSTOMER_RESPONSE);
		auroraService.getListOfCustomer(null);
	}
	
	public void getListOfCustomer() throws IOException {
		BDDMockito.when(netConnectionHelper.httpConnectionHelper("null/null/customers?limit=1&offset=1&expand=customerGroup.typeId&sort%3D%22createdAt+desc%22",null ))
		.thenReturn(CUSTOMER_RESPONSE);
		auroraService.getListOfCustomer(null);
	}

	@Test
	public void getListOfAccounts() {
		Assert.assertNull(auroraService.getListOfAccounts(1, 1, "testCompanyName", "testEmail"));
	}

	@Test
	public void getCustomersByAccountId() {
		CustomerResponseBean customer = Mockito.mock(CustomerResponseBean.class);
		BDDMockito.when(registrationHelperService.getCustomerById(Mockito.anyString())).thenReturn(customer);
		String cartResponse = "{\"limit\":20,\"offset\":0,\"count\":1,\"total\":1,\"results\":[{\"type\":\"Cart\",\"id\":\"3883127b-88f4-4f62-85aa-e9f5b7c1fb55\",\"version\":11,\"lastMessageSequenceNumber\":1,\"createdAt\":\"2021-05-17T13:01:20.474Z\",\"lastModifiedAt\":\"2021-05-17T13:01:22.864Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"customerId\":\"067b970d-de1c-4c49-980d-643c07fa6eb9\",\"customerEmail\":\"wer@gmail.com\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"lineItems\":[{\"id\":\"bfdd77df-c31f-45b9-a8c7-2035f5869524\",\"productId\":\"89242925-fb19-441d-aca8-a86614346460\",\"name\":{\"en-US\":\"Molekule Air Mini + + + \"},\"productType\":{\"typeId\":\"product-type\",\"id\":\"f62e5a83-fbf1-4f9d-aced-b3169aa99757\",\"version\":34},\"variant\":{\"id\":1,\"sku\":\"MN2P-US\",\"key\":\"611882\",\"prices\":[{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":69900,\"fractionDigits\":2},\"id\":\"a2f2f3e6-85c9-410c-b17a-018df649cec2\",\"tiers\":[{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":64900,\"fractionDigits\":2}}]},{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":49900,\"fractionDigits\":2},\"id\":\"a1824b34-9881-447c-b30c-52df4d642583\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"tiers\":[{\"minimumQuantity\":5,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":45000,\"fractionDigits\":2}},{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":42500,\"fractionDigits\":2}},{\"minimumQuantity\":15,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":40000,\"fractionDigits\":2}}]}],\"images\":[],\"attributes\":[{\"name\":\"b2bView\",\"value\":true},{\"name\":\"b2cView\",\"value\":true},{\"name\":\"sellable\",\"value\":true},{\"name\":\"inventoryThreshold\",\"value\":15},{\"name\":\"upcCode\",\"value\":818701020322},{\"name\":\"length\",\"value\":12},{\"name\":\"width\",\"value\":11.5},{\"name\":\"height\",\"value\":16.25},{\"name\":\"weight\",\"value\":10},{\"name\":\"QuoteThreshold\",\"value\":5}],\"assets\":[]},\"price\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":49900,\"fractionDigits\":2},\"id\":\"a1824b34-9881-447c-b30c-52df4d642583\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"tiers\":[{\"minimumQuantity\":5,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":45000,\"fractionDigits\":2}},{\"minimumQuantity\":10,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":42500,\"fractionDigits\":2}},{\"minimumQuantity\":15,\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":40000,\"fractionDigits\":2}}]},\"quantity\":1,\"discountedPricePerQuantity\":[],\"addedAt\":\"2021-05-17T13:01:22.380Z\",\"lastModifiedAt\":\"2021-05-17T13:01:22.380Z\",\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}],\"priceMode\":\"Platform\",\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":49900,\"fractionDigits\":2},\"lineItemMode\":\"Standard\"}],\"cartState\":\"Active\",\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":50500,\"fractionDigits\":2},\"country\":\"US\",\"customLineItems\":[{\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":600,\"fractionDigits\":2},\"id\":\"515f3bf8-79d1-4869-a869-d3004a681537\",\"name\":{\"en\":\"Handling Cost\"},\"money\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":600,\"fractionDigits\":2},\"slug\":\"handling_cost\",\"quantity\":1,\"discountedPricePerQuantity\":[],\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}]}],\"discountCodes\":[],\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"937cb0c5-55a8-4c88-af52-3bcb8c53a04c\"},\"fields\":{\"handlingCost\":\"600\",\"subTotal\":\"49900\",\"shippingCost\":\"0\",\"orderTotal\":\"50500\"}},\"inventoryMode\":\"None\",\"taxMode\":\"ExternalAmount\",\"taxRoundingMode\":\"HalfEven\",\"taxCalculationMode\":\"LineItemLevel\",\"deleteDaysAfterLastModification\":90,\"refusedGifts\":[],\"origin\":\"Customer\",\"itemShippingAddresses\":[],\"store\":{\"typeId\":\"store\",\"key\":\"MLK_US\"}}]}";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/carts/?where=customerId=\"customerId\"&where=custom(fields(parentOrderReferenceId is not defined))&where=custom(fields(Quote is not defined))&where=cartState=\"Active\""))
		.thenReturn(cartResponse);
		Assert.assertNotNull(auroraService.getCustomersByAccountId( "customerId"));

	}

	@Test
	public void getAllCustomersByAccountId() {
		String accountResponse = "{\"limit\":20,\"offset\":0,\"count\":1,\"total\":1,\"results\":[{\"id\":\"7c54ab03-b69c-41e6-8d07-ce28da2de0de\",\"version\":30,\"createdAt\":\"2021-05-17T07:50:43.151Z\",\"lastModifiedAt\":\"2021-05-22T12:46:38.602Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"ohoton138Hello\",\"value\":{\"companyName\":\"ohoton\",\"parcelFlag\":false,\"freightFlag\":false,\"employeeIds\":[\"067b970d-de1c-4c49-980d-643c07fa6eb9\"],\"taxExemptFlag\":false,\"myOwnCarrier\":true,\"companyCategory\":{\"id\":\"e10c5f79-4d31-4c4a-8585-64c089152e32\",\"name\":\"Office\",\"sales-rep-id\":\"8e922aad-da30-4e9f-8236-42b5b3b4e24e\"},\"salesRepresentative\":{\"id\":\"8e922aad-da30-4e9f-8236-42b5b3b4e24e\",\"name\":\"Scott Smith\",\"phone\":\"19997672134\",\"email\":\"scott-rep@mmolek.com\",\"firstName\":\"Scott\",\"lastName\":\"Smith\",\"calendlyLink\":\"\"},\"noOfEmployees\":\"50-100\",\"noOfOfficeSpaces\":\"5+\",\"taxCertificateList\":[],\"addresses\":{\"shippingAddresses\":[{\"shippingAddressId\":\"6ce1ee8e-bcb4-45e4-b1f8-d907725990d2\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"shippingAddressId\":\"902bb086-0010-468b-8c34-161c6d86f9f8\",\"firstName\":\"Salman\",\"lastName\":\"asd\",\"streetAddress2\":\"ST\",\"city\":\"Cupertino\",\"state\":\"CA\",\"postalCode\":\"95014\",\"phoneNumber\":\"\",\"country\":\"US\"},{\"shippingAddressId\":\"a11805c1-22ac-45ee-a0f8-338841ba5c3d\",\"firstName\":\"Sara\",\"lastName\":\"aru\",\"streetAddress1\":\"Shiva Loop\",\"streetAddress2\":\"\",\"city\":\"Kissimmee\",\"state\":\"FL\",\"postalCode\":\"34746\",\"phoneNumber\":\"7867567586\",\"country\":\"US\"}],\"billingAddresses\":[{\"billingAddressId\":\"d110d13a-d308-4291-b2cb-0bca2c326c65\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"4fd797ea-ba76-435e-8b09-6b53ef3a13fc\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"57328176-db98-4533-bd0d-bbe129b1ca1a\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"2c601f47-b926-4768-adf0-acb7c538e3d9\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"ccf0a200-4cfc-4cfb-949d-9ba1e0f15307\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"46da82ea-b782-406b-ad08-b791c646bf24\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"d213e56e-31a7-4900-9771-96a590ba1ef1\",\"firstName\":\"sar\",\"lastName\":\"asd\",\"streetAddress1\":\"1 Apple Park Way\",\"streetAddress2\":\"\",\"city\":\"Cupertino\",\"state\":\"CA\",\"postalCode\":\"95014\",\"phoneNumber\":\"1432543654\",\"country\":\"US\"},{\"billingAddressId\":\"7368d9b8-1d3a-47d0-82c4-8aa011ecec67\",\"firstName\":\"sar\",\"lastName\":\"asd\",\"streetAddress1\":\"1 Apple Park Way\",\"streetAddress2\":\"\",\"city\":\"Cupertino\",\"state\":\"CA\",\"postalCode\":\"95014\",\"phoneNumber\":\"1432543654\",\"country\":\"US\"},{\"billingAddressId\":\"416f26ba-01c8-456b-9ef5-61a44c62bb7e\",\"firstName\":\"sar\",\"lastName\":\"asd\",\"streetAddress1\":\"1 Apple Park Way\",\"streetAddress2\":\"\",\"city\":\"Cupertino\",\"state\":\"CA\",\"postalCode\":\"95014\",\"phoneNumber\":\"1432543654\",\"country\":\"US\"},{\"billingAddressId\":\"014eb7d1-ba98-4fed-9cfb-a5be04560b82\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"f5a95233-2946-4dfe-9278-1f401a5ab2d3\",\"firstName\":\"Sar\",\"lastName\":\"ar\",\"streetAddress1\":\"12830 Columbia Way\",\"streetAddress2\":\"\",\"city\":\"Downey\",\"state\":\"CA\",\"postalCode\":\"90242\",\"phoneNumber\":\"1242354364\",\"country\":\"US\"},{\"billingAddressId\":\"73b06fc9-30e5-47f2-a956-81312725e53c\",\"firstName\":\"Sar\",\"lastName\":\"Arn\",\"streetAddress1\":\"Livermore Outlets Drive\",\"streetAddress2\":\"\",\"city\":\"Pleasanton\",\"state\":\"CA\",\"postalCode\":\"94551\",\"phoneNumber\":\"9878676754\",\"country\":\"US\"}]},\"defaultShippingAddressId\":\"a11805c1-22ac-45ee-a0f8-338841ba5c3d\",\"defaultBillingAddressId\":\"f5a95233-2946-4dfe-9278-1f401a5ab2d3\",\"defaultPaymentId\":\"de4545f3-a542-4859-b1e9-52ea79809db7\",\"payments\":[{\"paymentId\":\"de4545f3-a542-4859-b1e9-52ea79809db7\",\"paymentType\":\"PAYMENTTERMS\",\"billingAddressId\":\"f5a95233-2946-4dfe-9278-1f401a5ab2d3\",\"business\":{\"businessType\":\"GOVERNMENT\",\"email\":\"kljkjKkjjk@jkj.com\",\"phoneNumber\":\"9886040503\"}},{\"paymentId\":\"24554dc0-5e9c-4073-9c6f-6d869413b8aa\",\"paymentType\":\"ACH\"},{\"paymentId\":\"830b0939-af49-4ad9-861b-fa3f18cacbfa\",\"paymentType\":\"CREDITCARD\",\"paymentToken\":\"seti_1Is1Wt2eZvKYlo2CYvapbEjK\",\"customerId\":\"067b970d-de1c-4c49-980d-643c07fa6eb9\",\"billingAddressId\":\"416f26ba-01c8-456b-9ef5-61a44c62bb7e\"},{\"paymentId\":\"1056520a-7d51-49ab-90d4-fd2f06e7ec6e\",\"paymentType\":\"CREDITCARD\",\"paymentToken\":\"seti_1Is1WE2eZvKYlo2COIe2gvmF\",\"customerId\":\"067b970d-de1c-4c49-980d-643c07fa6eb9\",\"billingAddressId\":\"7368d9b8-1d3a-47d0-82c4-8aa011ecec67\"},{\"paymentId\":\"fb093625-3ce6-476f-969b-876eb73f7c4e\",\"paymentType\":\"CREDITCARD\",\"paymentToken\":\"seti_1Is1Uw2eZvKYlo2CkaOwOeao\",\"customerId\":\"067b970d-de1c-4c49-980d-643c07fa6eb9\",\"billingAddressId\":\"d213e56e-31a7-4900-9771-96a590ba1ef1\"},{\"paymentId\":\"135dd573-ede6-46e8-9641-32db646e82ac\",\"paymentType\":\"CREDITCARD\",\"paymentToken\":\"seti_1Is1Rc2eZvKYlo2Cky8pzfKX\",\"customerId\":\"067b970d-de1c-4c49-980d-643c07fa6eb9\",\"billingAddressId\":\"46da82ea-b782-406b-ad08-b791c646bf24\"},{\"paymentId\":\"e03c197b-69de-4f53-94bf-40090fec6c69\",\"paymentType\":\"CREDITCARD\",\"paymentToken\":\"seti_1Is1RK2eZvKYlo2CLbFWoX88\",\"customerId\":\"067b970d-de1c-4c49-980d-643c07fa6eb9\",\"billingAddressId\":\"ccf0a200-4cfc-4cfb-949d-9ba1e0f15307\"}],\"companyAddress\":{\"streetAddress1\":\"1155 East Oakton Street\",\"streetAddress2\":\"\",\"city\":\"Des Plaines\",\"state\":\"IL\",\"postalCode\":\"60018\",\"phoneNumber\":\"9886040503\",\"country\":\"US\"},\"carriers\":[{\"loadingDock\":false,\"forkLift\":false,\"deliveryAppointmentRequired\":false,\"carrierId\":\"2ebd7990-1be1-48ef-abe6-0c4ef817f22b\",\"type\":\"BOTH\",\"carrierName\":\"hello\",\"carrierAccountNumber\":\"124384y78\",\"billingAddressId\":\"73b06fc9-30e5-47f2-a956-81312725e53c\"}]}}]}";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=id=\"accountId\""))
		.thenReturn(accountResponse);
		
		Assert.assertNotNull(auroraService.getAllCustomersByAccountId("accountId"));
	}

	@Test
	public void getCTCustomObjectsByAccountIdUrl() {
		Assert.assertNotNull(auroraService.getCTCustomObjectsByAccountIdUrl("accountId"));

	}

	@Test
	public void getListCustomerGroups() {
		Assert.assertNull(auroraService.getListCustomerGroups());
	}

	@Mock
	AccountCustomResponseBean accountCustomResponseBean;

	public void createAccount() {
		auroraService.createAccount(accountCustomResponseBean);
	}


	public void addTaxExemptReviews() {
		auroraService.addTaxExemptReviews("test_account_id", taxExemptReviewsRequestBean);

	}

	public void createCustomer() {
		auroraService.createCustomer("test_account_id", auroraCustomerRequestBean);
	}

	@Test
	public void generatePassword() {
		Assert.assertNotNull(auroraService.generatePassword(4));
	}

	@Mock
	AuroraUpdateCustomerRequestBean customerRequest;

	public void updateCustomer() {
		auroraService.updateCustomer("customerId", customerRequest);
	}

	@Test
	public void assignCustomerGroup() {
		String groupResponse = "{\"limit\":20,\"offset\":0,\"count\":7,\"total\":7,\"results\":[{\"id\":\"e32665d7-0c5c-4571-b348-7882eae3ad50\",\"version\":1,\"createdAt\":\"2021-04-01T14:52:23.131Z\",\"lastModifiedAt\":\"2021-04-01T14:52:23.131Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"4ef0f3cc-3572-47ba-9f67-5eaa3559fbb2\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"4ef0f3cc-3572-47ba-9f67-5eaa3559fbb2\"}},\"name\":\"Any\",\"key\":\"Any\"},{\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\",\"version\":1,\"createdAt\":\"2021-04-01T14:53:53.533Z\",\"lastModifiedAt\":\"2021-04-01T14:53:53.533Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"4ef0f3cc-3572-47ba-9f67-5eaa3559fbb2\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"4ef0f3cc-3572-47ba-9f67-5eaa3559fbb2\"}},\"name\":\"MO_Registered\",\"key\":\"MO_Registered\"},{\"id\":\"6a2c342b-9a13-4527-9cf5-f7c2cdd3cbc0\",\"version\":1,\"createdAt\":\"2021-04-16T06:38:54.042Z\",\"lastModifiedAt\":\"2021-04-16T06:38:54.042Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"b9ef6a88-c8ac-47cb-9097-2c221ce23d4a\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"b9ef6a88-c8ac-47cb-9097-2c221ce23d4a\"}},\"name\":\"estest\",\"key\":\"estest\"},{\"id\":\"61e266b9-171f-408d-a380-7d2ff295aeb1\",\"version\":1,\"createdAt\":\"2021-04-16T16:33:12.544Z\",\"lastModifiedAt\":\"2021-04-16T16:33:12.544Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"name\":\"Webshop user\"},{\"id\":\"63b65e1b-3611-447b-8e17-ea67b6d57361\",\"version\":1,\"createdAt\":\"2021-04-29T14:01:28.333Z\",\"lastModifiedAt\":\"2021-04-29T14:01:28.333Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"e059b819-5d0e-4f67-9bbc-00cb8f50894e\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"e059b819-5d0e-4f67-9bbc-00cb8f50894e\"}},\"name\":\"IN - Testing Group\",\"key\":\"3431445\"},{\"id\":\"be4d0d7c-1a4c-4ae1-9ba3-450436642658\",\"version\":1,\"createdAt\":\"2021-04-30T15:47:00.717Z\",\"lastModifiedAt\":\"2021-04-30T15:47:00.717Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"1424cb5d-a01d-46c4-ae7c-3d3100232654\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"1424cb5d-a01d-46c4-ae7c-3d3100232654\"}},\"name\":\"Test_Customer_QA\"},{\"id\":\"b4c47e19-6bef-4f6b-b52f-5b1fd11ed581\",\"version\":1,\"createdAt\":\"2021-05-06T15:15:07.629Z\",\"lastModifiedAt\":\"2021-05-06T15:15:07.629Z\",\"lastModifiedBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"1ee744e6-043d-490e-ba43-d8d4d0e2134c\"}},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"1ee744e6-043d-490e-ba43-d8d4d0e2134c\"}},\"name\":\"test\",\"key\":\"we\"}]}";
		CustomerResponseBean customer = Mockito.mock(CustomerResponseBean.class);
		BDDMockito.when(registrationHelperService.getCustomerById("customerId")).thenReturn(customer);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/customer-groups")).thenReturn(groupResponse);
		Assert.assertNotNull(auroraService.assignCustomerGroup("customerId", "customerGroupId"));
	}

	@Test
	public void assignSalesrepToCompanyCategory() {
		BDDMockito.when(auroraHelperService.getCompanyCategories()).thenReturn(CATEGORY_RESPONSE);
		BDDMockito.when(auroraHelperService.getListOfSalesRep()).thenReturn(SALES_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=value(companyCategory(id=\"accountId\"))&limit=500&offset=0")).thenReturn(ACCOUNT_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=value(companyCategory(id=\"accountId\"))&limit=500&offset=501")).thenReturn(ACCOUNT_RESPONSE);
		
		Assert.assertNotNull(auroraService.assignSalesrepToCompanyCategory("accountId", "8e922aad-da30-4e9f-8236-42b5b3b4e24e"));
	}

	public void removeTaxExemptReviews() {
		auroraService.removeTaxExemptReviews("accountId", "certificateId");

	}

	@Test
	public void createCategory() {
		BDDMockito.when(auroraHelperService.getCompanyCategories()).thenReturn(CATEGORY_RESPONSE);
		BDDMockito.when(auroraHelperService.getListOfSalesRep()).thenReturn(SALES_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=value(companyCategory(id=\"accountId\"))&limit=500&offset=0")).thenReturn(ACCOUNT_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=value(companyCategory(id=\"accountId\"))&limit=500&offset=501")).thenReturn(ACCOUNT_RESPONSE);
		
		Assert.assertNotNull(auroraService.createCategory("categoryName", "8e922aad-da30-4e9f-8236-42b5b3b4e24e"));
	}

	@Test
	public void removeCategoryId() {
		BDDMockito.when(auroraHelperService.getCompanyCategories()).thenReturn(CATEGORY_RESPONSE);
		BDDMockito.when(auroraHelperService.getListOfSalesRep()).thenReturn(SALES_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=value(companyCategory(id=\"5cb62872-fa6e-422e-80ef-dadd35ec3490\"))&limit=500&offset=0")).thenReturn(ACCOUNT_RESPONSE);
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, "null/null/custom-objects?where=value(companyCategory(id=\"5cb62872-fa6e-422e-80ef-dadd35ec3490\"))&limit=500&offset=501")).thenReturn(ACCOUNT_RESPONSE);
		
		Assert.assertNotNull(auroraService.removeCategoryId("5cb62872-fa6e-422e-80ef-dadd35ec3490"));
	}

	@Test
	public void createSalesRepresentative() {
		BDDMockito.when(auroraHelperService.getListOfSalesRep()).thenReturn(SALES_RESPONSE);
		BDDMockito.when(auroraHelperService.getCompanyCategories()).thenReturn(CATEGORY_RESPONSE);
		Assert.assertNotNull(auroraService.createSalesRepresentative(salesRequestBean));
	}


	@Test
	public void updateSalesRepresentative() {
		BDDMockito.when(auroraHelperService.getListOfSalesRep()).thenReturn(SALES_RESPONSE);
		BDDMockito.when(auroraHelperService.getCompanyCategories()).thenReturn(CATEGORY_RESPONSE);
		
		Assert.assertNotNull(auroraService.updateSalesRepresentative("salesRepId", salesRequestBean));
	}

	@Test
	public void getAllSalesReps() {
		BDDMockito.when(auroraHelperService.getCompanyCategories()).thenReturn(CATEGORY_RESPONSE);
		BDDMockito.when(auroraHelperService.getListOfSalesRep()).thenReturn(SALES_RESPONSE);
		Assert.assertNotNull(auroraService.getAllSalesReps(1, 1));
	}

}
