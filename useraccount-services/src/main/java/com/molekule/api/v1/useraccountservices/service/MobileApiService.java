package com.molekule.api.v1.useraccountservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccessTokenDTO;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;
import com.molekule.api.v1.commonframework.model.customerprofile.AutoRefillsResponseBean;
import com.molekule.api.v1.commonframework.model.customerprofile.AutoRefillsResponseBean.SubscriptionsObject;
import com.molekule.api.v1.commonframework.model.customerprofile.BulkUpdateRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.UpdateAutoRefillsRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.UpdateAutoRefillsRequestBean.AutoRefillsActions;
import com.molekule.api.v1.commonframework.model.mobileapp.Address;
import com.molekule.api.v1.commonframework.model.mobileapp.CustomerCardInfo;
import com.molekule.api.v1.commonframework.model.mobileapp.CustomerCards;
import com.molekule.api.v1.commonframework.model.mobileapp.CustomerResponse;
import com.molekule.api.v1.commonframework.model.mobileapp.CustomerStripePayment;
import com.molekule.api.v1.commonframework.model.mobileapp.Subscription;
import com.molekule.api.v1.commonframework.model.mobileapp.Subscription.Device;
import com.molekule.api.v1.commonframework.model.mobileapp.Subscription.Payment;
import com.molekule.api.v1.commonframework.model.mobileapp.Subscription.SubscriptionPlan;
import com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionCustomerEstimateQuery;
import com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionEstimate;
import com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionObject;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MobileErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.useraccountservices.util.MobileAppUtility;

import reactor.core.publisher.Mono;

@Service("mobileService")
public class MobileApiService {

	Logger logger = LoggerFactory.getLogger(MobileApiService.class);
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;
	@Autowired
	CTEnvProperties ctEnvProperties;
	@Autowired
	NetConnectionHelper netConnectionHelper;
	@Autowired
	CustomerProfileService customerProfileService;
	@Autowired
	CtServerHelperService ctServerHelperService;
	@Autowired
	CsrHelperService csrHelperService;
	public String getAccessToken(String username, String password) {
		if (username == null || password == null) {
			ErrorResponse errorResponse = new ErrorResponse(400, "Invalid username or password");
			return errorResponse.toString();
		}
		String baseUrl = ctEnvProperties.getAuthUrl() + "/" + ctEnvProperties.getAccessTokenUrl() + "?grant_type="
				+ ctEnvProperties.getGranttype();
		String userPasswordStr = username + ":" + password;
		String basicAuthValue = Base64.getEncoder().encodeToString(userPasswordStr.getBytes());
		String token = new StringBuilder().append("Basic ").append(basicAuthValue).toString();
		WebClient webClient = fetchWebclient(baseUrl);
		Mono<AccessTokenDTO> accessToken = webClient.post().accept(MediaType.APPLICATION_JSON)
				.header("Authorization", token).exchangeToMono(clientResponse -> {
					logger.trace("Access Token Api Status Code - {}", clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						throw new CustomRunTimeException("Unable to get Access Token From the CT Server");
					}
					return clientResponse.bodyToMono(AccessTokenDTO.class);
				});
		String responseAccessToken = null;
		AccessTokenDTO accessTokenDTO = accessToken.block();
		if(null != accessTokenDTO && accessTokenDTO.getAccessToken() != null) {
			responseAccessToken = accessTokenDTO.getAccessToken();
		}
		return responseAccessToken;
	}

	public ResponseEntity<Object> getCustomerById(String customerId) {
		CustomerResponse customerResponse = null;
		CustomerResponseBean customerResponseBean = null;
		try {
			customerResponseBean =  registrationHelperService.getCustomerById(customerId,null);
		} catch (Exception e) {
			logger.error("Error Occured getCustomerById: ", e);
			return new ResponseEntity<>(getErrorResponse(customerId),HttpStatus.BAD_REQUEST);
		}
		if(customerResponseBean == null||customerResponseBean.getCustomerNumber() == null) {
			return new ResponseEntity<>(getErrorResponse(customerId),HttpStatus.BAD_REQUEST);
		}
		customerResponse = MobileAppUtility.setCustomerResponse(customerResponseBean);
		String accountId = customerResponseBean.getCustom().getFields().getAccountId();
		if(accountId != null) {
			String accountKey = registrationHelperService.getAccountKeyById(accountId);
			AccountCustomObject accountCustomObject = (AccountCustomObject)registrationHelperService.getAccountCustomObject(accountKey);
			customerResponse = MobileAppUtility.setAccountResponse(accountCustomObject, customerResponse);
		}else {
			List<com.molekule.api.v1.commonframework.model.registration.Address> addresses = customerResponseBean.getAddresses();
			getCustomerAddress(addresses,customerResponse);
		}
		customerResponse.setStore_id(customerResponseBean.getCustom().getFields().getStore());
		return new ResponseEntity<>(MobileAppUtility.convertJsonToString(customerResponse),HttpStatus.OK);
	}

	private CustomerResponse getCustomerAddress(
			List<com.molekule.api.v1.commonframework.model.registration.Address> addresses,CustomerResponse customerResponse) {
		List<Address> mobileAppAddressList = new ArrayList<>();   
		Iterator<com.molekule.api.v1.commonframework.model.registration.Address> addressItreator = addresses.iterator();
		while(addressItreator.hasNext()) {
			com.molekule.api.v1.commonframework.model.registration.Address ctAddress = addressItreator.next();
			Address maAddress = new Address();
			maAddress.setId(ctAddress.getId());
			maAddress.setCustomer_id(customerResponse.getId());
			Address.Region region = new Address.Region();
			region.setRegion(ctAddress.getRegion());
			maAddress.setRegion(region);
			maAddress.setCountry_id(ctAddress.getCountry());
			List<String> streets = new ArrayList<>();
			streets.add(ctAddress.getStreetAddress1());
			streets.add(ctAddress.getStreetAddress2());
			maAddress.setStreet(streets);
			maAddress.setTelephone(ctAddress.getPhoneNumber());
			maAddress.setPostcode(ctAddress.getPostalCode());
			maAddress.setCity(ctAddress.getCity());
			maAddress.setFirstname(ctAddress.getFirstName());
			maAddress.setLastname(ctAddress.getLastName());
			mobileAppAddressList.add(maAddress);
		}
		customerResponse.setAddresses(mobileAppAddressList);
		return customerResponse;
	}

	public ResponseEntity<Object> getCurrentLoggedinCustomer(String bearerToken) {
		String customerId = getCustomerIdFromCognito(bearerToken);
		return getCustomerById(customerId);
	}

	public ResponseEntity<Object> getSubscriptionByID(String bearerToken) {
		String customerId = getCustomerIdFromCognito(bearerToken);
		if(customerId == null) {
			MobileErrorResponse mobileErrorResponse = new MobileErrorResponse();
			mobileErrorResponse.setMessage(MolekuleConstant.UNAUTHORIZED_MESSAGE);
			return new ResponseEntity<>(mobileErrorResponse,HttpStatus.UNAUTHORIZED);
		}
		ResponseEntity<AutoRefillsResponseBean> responseAutoRefillResponseBean =  customerProfileService.getAutoRefillsSubscriptions(customerId, null, null, 50, 0);
		AutoRefillsResponseBean autoRefillResponseBean =   responseAutoRefillResponseBean.getBody();
		List<SubscriptionsObject> subscriptionsObject =autoRefillResponseBean != null ? autoRefillResponseBean.getSubscriptionsObject() : new ArrayList<>();
		java.util.Iterator<SubscriptionsObject>  subIterator = subscriptionsObject.iterator();
		List<Subscription> subscriptions = new ArrayList<>();
		while(subIterator.hasNext()) {
			SubscriptionsObject subObject = subIterator.next();
			Subscription subscription = prepareMobileAppSubscriptionObject(subObject);
			subscriptions.add(subscription);
		}
		return new ResponseEntity<>(MobileAppUtility.convertJsonToString(subscriptions),HttpStatus.OK);
	}

	public ResponseEntity<Object> createSubscriptionByCustId(String authKey,SubscriptionObject subscriptionObject, String country) {
		String subscriptionProductId = getProductBySku(subscriptionObject); 
		SubscriptionRequestBean subscriptionRequestBean = new SubscriptionRequestBean();
		subscriptionRequestBean.setCustomerId(subscriptionObject.getSubscriptionCustomer().getCustomer_id());
		subscriptionRequestBean.setProductId(subscriptionProductId);
		subscriptionRequestBean.setPeriod(Integer.parseInt(subscriptionObject.getSubscriptionCustomer().getSubscription_plan().getFrequency()));
		String nextOrderDate = getNextOrderDate(subscriptionRequestBean.getPeriod());
		subscriptionRequestBean.setNextOrderDate(nextOrderDate);
		subscriptionRequestBean.setShippingAddressId(subscriptionObject.getSubscriptionCustomer().getShipping_address());
		customerProfileService.createSubscription(subscriptionRequestBean,country);
		return getSubscriptionByID(authKey);
	}

	public ResponseEntity<Object> updateSubscriptionById(String authKey,SubscriptionObject subscriptionObject) {
		String url = registrationHelperService.getCTCustomObjectsByAccountIdUrl(subscriptionObject.getSubscriptionCustomer().getId());
		String token = ctServerHelperService.getStringAccessToken();
		String subscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject subscriptionJsonObject = MolekuleUtility.parseJsonObject(subscriptionResponse);
		if (subscriptionJsonObject.get(STATUS_CODE) != null) {
			return null;
		}
		JsonArray resultsArray = subscriptionJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		String key = resultsObject.get("key").getAsString();
		String container = resultsObject.get("container").getAsString();
		UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean = new UpdateAutoRefillsRequestBean();
		updateAutoRefillsRequestBean.setAction(AutoRefillsActions.UPDATE_SHIPPING_ADDRESS);
		updateAutoRefillsRequestBean.setShippingAddressId(subscriptionObject.getSubscriptionCustomer().getShipping_address());
		customerProfileService.updateAutoRefills(subscriptionObject.getSubscriptionCustomer().getCustomer_id(), container, key, updateAutoRefillsRequestBean).getBody();
		return getSubscriptionByID(authKey);
	}

	private String getNextOrderDate(int period) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH,period);
		Date currentDate = calendar.getTime();
		return dateFormat.format(currentDate);
	}

	private String getProductBySku(SubscriptionObject subscriptionObject) {
		String subscriptionProductId = null;
		String productSkuUrl = ctEnvProperties.getProductServiceHost()+"api/v1/products/sku?sku="+subscriptionObject.getSubscriptionCustomer().getDevice().getSku();
		String token = ctServerHelperService.getAccessToken();
		String productResponse =  netConnectionHelper.sendGetWithoutBody(token, productSkuUrl);
		JsonObject productsJson = MolekuleUtility.parseJsonObject(productResponse);
		String productId =productsJson.get("productBean").getAsJsonObject().get("id").getAsString();
		String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + PRODUCTS+ productId + "?expand=masterData.current.masterVariant.attributes[*].value.typeId";
		String productRes = netConnectionHelper.sendGetWithoutBody("Bearer "+token, productUrl);
		subscriptionProductId = getSubscriptionProductId(productRes);
		return subscriptionProductId;
	}

	private String getSubscriptionProductId(String productRes) {
		String subscriptionProductId = null;
		JsonObject productJson = MolekuleUtility.parseJsonObject(productRes);
		JsonArray attributes = productJson.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		for(int i=0;i<attributes.size();i++) {
			String name = attributes.get(i).getAsJsonObject().get("name").getAsString();
			if("filterProduct".equals(name)) {
				subscriptionProductId = attributes.get(i).getAsJsonObject().get("value").getAsJsonObject().get("id").getAsString();
			}
		}
		return subscriptionProductId;
	}

	public boolean subscriptionEstimate(String authKey,SubscriptionEstimate subscriptionEstimate) {
		boolean isPromoAvalible = false;
		SubscriptionCustomerEstimateQuery subscriptionCustomerEstimate = subscriptionEstimate.getSubscriptionCustomerEstimateQuery();
		List<String> promos = subscriptionCustomerEstimate.getCoupon_codes();
		Iterator<String> promoItr = promos.iterator();
		while(promoItr.hasNext()) {
			String promo = promoItr.next();
			isPromoAvalible =  getDiscountCodeObj(promo,authKey);
		}
		return isPromoAvalible;
	}

	private boolean getDiscountCodeObj(String promo,String authKey) {
		String discountCodeUrl = getCTDiscountCodes(promo);
		String discoundResponse = netConnectionHelper.sendGetWithoutBody(authKey, discountCodeUrl);
		JsonObject discountCodeJson = MolekuleUtility.parseJsonObject(discoundResponse);
		if (discountCodeJson.get(STATUS_CODE) != null) {
			return false;
		}
		JsonArray resultsArray = discountCodeJson.getAsJsonArray(RESULTS).getAsJsonArray();
		return (resultsArray.size()>0);

	}

	public String getCTDiscountCodes(String promoCode) {
		return new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
				.append("/discount-codes?where=code=\"").append(promoCode).append("\"").toString();
	}

	private Subscription prepareMobileAppSubscriptionObject(SubscriptionsObject subObject) {
		Subscription subscription = new Subscription();
		subscription.setId(subObject.getId());
		subscription.setCustomer_id(subObject.getCustomerId());
		subscription.setStatus(subObject.getSubscriptionState());
		subscription.setNext_order(subObject.getNextOrderDate());
		subscription.setLast_order(subObject.getLastOrderDate());
		subscription.setCreated_at(subObject.getCreatedAt());
		subscription.setUpdated_at(subObject.getLastModifiedAt());
		Address address = new Address();
		if (subObject.getShippingAddress() != null) {
			address.setId(subObject.getShippingAddress().getId());
			address.setCustomer_id(subObject.getCustomerId());
			address.setCountry_id(subObject.getShippingAddress().getCountry());
			List<String> street = new ArrayList<>();
			street.add(subObject.getShippingAddress().getStreetName());
			street.add(subObject.getShippingAddress().getStreetNumber());
			address.setStreet(street);
			address.setTelephone(subObject.getShippingAddress().getPhone());
			address.setCity(subObject.getShippingAddress().getCity());
			address.setFirstname(subObject.getShippingAddress().getFirstName());
			address.setLastname(subObject.getShippingAddress().getLastName());
		}
		subscription.setShipping_address(address);
		SubscriptionPlan subscriptionPlan = new SubscriptionPlan();
		subscriptionPlan.setId(subObject.getId());
		subscriptionPlan.setTitle(subObject.getPlanName());
		subscriptionPlan.setShort_description(subObject.getPlanDescription());
		subscriptionPlan.setFrequency_units("month");
		subscriptionPlan.setFrequency(String.valueOf(subObject.getPeriod()));
		subscriptionPlan.setStatus(subObject.getStatus());
		subscriptionPlan.setCreated_at(subObject.getCreatedAt());
		subscriptionPlan.setUpdated_at(subObject.getLastModifiedAt());
		if(subObject.getTotalPrice() != null)
			subscriptionPlan.setPlan_price(Long.parseLong(subObject.getTotalPrice()));
		subscription.setSubscription_plan(subscriptionPlan);
		Device device = new Device();
		device.setCustomer_id(subObject.getCustomerId());
		subscription.setDevice(device);
		Payment payment = new Payment();
		subscription.setPayment(payment);
		return subscription;
	}

	public String getCustomerIdFromCognito(String bearerToken) {

		ObjectMapper mapper = new ObjectMapper();
		String[] tokenArr = bearerToken.split("\\s");
		String cognitoToken = tokenArr[1];
		String userDetails = null;
		String customerId  = null;
		if (cognitoToken != null) {
			String cognitoBaseUrl = ctEnvProperties.getCognitoDomain();
			Map<String, Object> cognitoSignUpRequest = new HashMap<>();
			cognitoSignUpRequest.put("AccessToken", cognitoToken);
			String cognitoRequest = null;
			try {
				cognitoRequest = mapper.writeValueAsString(cognitoSignUpRequest);
			} catch (Exception e) {
				logger.error("Error -", e);
			}
			userDetails = netConnectionHelper.sendCognitoPostRequest(cognitoBaseUrl, cognitoRequest, ctEnvProperties,ctEnvProperties.getCognitoHeaderAmzGetUserTarget());
			customerId = getCustomerIdFromUserDetails(userDetails);
		}
		return customerId;
	}

	private String getCustomerIdFromUserDetails(String userDetails) {
		String customerId = null;
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(userDetails);
		JsonArray userDetailsArray =  jsonObject.has("UserAttributes")?jsonObject.get("UserAttributes").getAsJsonArray():null;
		if(userDetailsArray != null) {
			for(int i=0;i<userDetailsArray.size();i++) {
				if("custom:customerId".equals(userDetailsArray.get(i).getAsJsonObject().get("Name").getAsString())) {
					customerId = userDetailsArray.get(i).getAsJsonObject().get("Value").getAsString();
					break;
				}
			}
		}
		return customerId;
	}

	/**
	 * fetchWebclient method is used to fetch the web client.
	 * 
	 * @param url
	 * @return WebClient
	 */
	public WebClient fetchWebclient(String url) {
		return WebClient.builder().baseUrl(url).build();
	}

	public ResponseEntity<Object> getCustomerPaymentInfo(String bearerToken) {
		String customerId = getCustomerIdFromCognito(bearerToken);
		if(customerId == null) {
			MobileErrorResponse mobileErrorResponse = new MobileErrorResponse();
			mobileErrorResponse.setMessage(MolekuleConstant.UNAUTHORIZED_MESSAGE);
			return new ResponseEntity<>(mobileErrorResponse,HttpStatus.UNAUTHORIZED);
		}
		JsonObject jsonObject = getCustomerObject(customerId);
		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		List<Payments> creditCardPayments = null;
		if(customObject.has(CHANNEL) && customObject.get(CHANNEL).getAsString().equals("D2C")) {
			CustomerDetailBean customerDetailBean = csrHelperService.getCustomerData(customerId, null, 5 , null);
			creditCardPayments = customerDetailBean.getPayments();
		}else {
			AccountCustomObject accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,null);
			if(accountCustomObject == null) {
			return new ResponseEntity<>(getErrorResponse(customerId),HttpStatus.BAD_REQUEST);
			}
			creditCardPayments = getCreditCardPayments(accountCustomObject.getValue().getPayments());
		}
		if(creditCardPayments.isEmpty()) {
			MobileAppUtility.convertJsonToString(new CustomerStripePayment());
		}
		String setUpIntentId = creditCardPayments.get(0).getPaymentToken();
		String stripeResponse = getRetrieveStripeById(setUpIntentId);
		JsonObject stripeJson = MolekuleUtility.parseJsonObject(stripeResponse);
		CustomerStripePayment customerStripePayment = new CustomerStripePayment();
		customerStripePayment.setCustomer_id(stripeJson.get("customer").getAsString());
		customerStripePayment.setStripe_id(setUpIntentId);
		customerStripePayment.setCustomer_email(jsonObject.get("email").getAsString());
		return new ResponseEntity<>(MobileAppUtility.convertJsonToString(customerStripePayment),HttpStatus.OK);
	}

	private String getRetrieveStripeById(String setUpIntentId) {
		String retrieveStripeUrl = ctEnvProperties.getStripeDomainHost()+"/v1/setup_intents/"+setUpIntentId;
		return netConnectionHelper.sendGetWithoutBody(getStripeToken(), retrieveStripeUrl);
	}

	private List<Payments> getCreditCardPayments(List<Payments> payments) {
		List<Payments> creditCardPayments = new ArrayList<>();
		Payments payment = null;
		if(payments != null) {
			java.util.Iterator<Payments> payIterator = payments.iterator();
			while(payIterator.hasNext()) {
				payment = payIterator.next();
				if("CREDITCARD".equals(payment.getPaymentType())) {
					creditCardPayments.add(payment);
				}
			}
		}
		return creditCardPayments;
	}
	private String getStripeToken() {
		return new StringBuilder("Bearer "+ctEnvProperties.getStripeBearerToken()).toString();
	}
	public ResponseEntity<Object> getCustomerCardInfo(String bearerToken) {
		String customerId = getCustomerIdFromCognito(bearerToken);
		if(customerId == null) {
			MobileErrorResponse mobileErrorResponse = new MobileErrorResponse();
			mobileErrorResponse.setMessage(MolekuleConstant.UNAUTHORIZED_MESSAGE);
			return new ResponseEntity<>(mobileErrorResponse,HttpStatus.UNAUTHORIZED);
		}
		JsonObject jsonObject = getCustomerObject(customerId);
		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		List<Payments> creditCardPayments = null;
		if(customObject.has(CHANNEL) && customObject.get(CHANNEL).getAsString().equals("D2C")) {
			CustomerDetailBean customerDetailBean = csrHelperService.getCustomerData(customerId, null, 5 , null);
			creditCardPayments = customerDetailBean.getPayments();
		}else {
			AccountCustomObject accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,null);
			if(accountCustomObject == null) {
				return new ResponseEntity<>(getErrorResponse(customerId),HttpStatus.BAD_REQUEST);
			}
			creditCardPayments = getCreditCardPayments(accountCustomObject.getValue().getPayments());
		}

		if(creditCardPayments.isEmpty()) {
			MobileAppUtility.convertJsonToString(new CustomerCardInfo());
		}
		List<CustomerCardInfo> customerCardInfos = getCustomerCardInfo(creditCardPayments);
		return new ResponseEntity<>(MobileAppUtility.convertJsonToString(customerCardInfos),HttpStatus.OK);
	}

	private List<CustomerCardInfo> getCustomerCardInfo(List<Payments> creditCardPayments) {
		List<CustomerCardInfo> customerCardInfos = new ArrayList<>();
		Iterator<Payments> payIterator = creditCardPayments.iterator();
		while(payIterator.hasNext()) {
			CustomerCardInfo customerCardInfo = new CustomerCardInfo();
			Payments payment  = payIterator.next();
			String setupIntentId = payment.getPaymentToken();
			String stripeResponse = getRetrieveStripeById(setupIntentId);
			JsonObject stripeJson = MolekuleUtility.parseJsonObject(stripeResponse);
			String paymentMethodId = stripeJson.get("payment_method").getAsString();
			customerCardInfo.setId(paymentMethodId);
			String paymentRetrieveUrl =  ctEnvProperties.getStripeDomainHost()+"/v1/payment_methods/"+paymentMethodId;
			String paymentMethodRes = netConnectionHelper.sendGetWithoutBody(getStripeToken(), paymentRetrieveUrl);
			JsonObject paymentMethodJson = MolekuleUtility.parseJsonObject(paymentMethodRes);
			getCardInfoFromPaymentMethod(paymentMethodJson,customerCardInfo);
			customerCardInfos.add(customerCardInfo);
		}
		return customerCardInfos;
	}

	private CustomerCardInfo getCardInfoFromPaymentMethod(JsonObject paymentMethodJson,
			CustomerCardInfo customerCardInfo) {
		customerCardInfo.setExp_month(paymentMethodJson.get("card").getAsJsonObject().get("exp_month").getAsInt());
		customerCardInfo.setExp_year(paymentMethodJson.get("card").getAsJsonObject().get("exp_year").getAsInt());
		customerCardInfo.setLast4(paymentMethodJson.get("card").getAsJsonObject().get("last4").getAsString());
		customerCardInfo.setBrand(paymentMethodJson.get("card").getAsJsonObject().get("networks").getAsJsonObject().get("available").getAsJsonArray().get(0).getAsString());
		return customerCardInfo;
	}


	public ResponseEntity<Object> getCustomerCards(String bearerToken) {
		String customerId = getCustomerIdFromCognito(bearerToken);
		if(customerId == null) {
			MobileErrorResponse mobileErrorResponse = new MobileErrorResponse();
			mobileErrorResponse.setMessage(MolekuleConstant.UNAUTHORIZED_MESSAGE);
			return new ResponseEntity<>(mobileErrorResponse,HttpStatus.UNAUTHORIZED);
		}
		JsonObject jsonObject = getCustomerObject(customerId);
		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		List<Payments> creditCardPayments = null;
		if(customObject.has(CHANNEL) && customObject.get(CHANNEL).getAsString().equals("D2C")) {
			CustomerDetailBean customerDetailBean = csrHelperService.getCustomerData(customerId, null, 5 , null);
			creditCardPayments = customerDetailBean.getPayments();
		}else {
			AccountCustomObject accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,null);
			if(accountCustomObject == null) {
				return new ResponseEntity<>(getErrorResponse(customerId),HttpStatus.BAD_REQUEST);
			}
			creditCardPayments = getCreditCardPayments(accountCustomObject.getValue().getPayments());
		}
		if(creditCardPayments.isEmpty()) {
			MobileAppUtility.convertJsonToString(new CustomerCardInfo());
		}
		List<CustomerCards> customerCards = getCustomerCards(creditCardPayments);

		return new ResponseEntity<>(MobileAppUtility.convertJsonToString(customerCards),HttpStatus.OK);
	}

	private List<CustomerCards> getCustomerCards(List<Payments> creditCardPayments) {
		List<CustomerCards> customerCardsList = new ArrayList<>();
		Iterator<Payments> payIterator = creditCardPayments.iterator();
		while(payIterator.hasNext()) {
			CustomerCards customerCards = new CustomerCards();
			Payments payment  = payIterator.next();
			String setupIntentId = payment.getPaymentToken();
			String stripeResponse = getRetrieveStripeById(setupIntentId);
			JsonObject stripeJson = MolekuleUtility.parseJsonObject(stripeResponse);
			String paymentMethodId = stripeJson.get("payment_method").getAsString();
			customerCards.setPayment_id(setupIntentId);
			customerCards.setPayment_code(paymentMethodId);
			String paymentRetrieveUrl =  ctEnvProperties.getStripeDomainHost()+"/v1/payment_methods/"+paymentMethodId;
			String paymentMethodRes = netConnectionHelper.sendGetWithoutBody(getStripeToken(), paymentRetrieveUrl);
			JsonObject paymentMethodJson = MolekuleUtility.parseJsonObject(paymentMethodRes);
			getCardFromPaymentMethod(paymentMethodJson,customerCards);
			customerCardsList.add(customerCards);
		}
		return customerCardsList;
	}

	private CustomerCards getCardFromPaymentMethod(JsonObject paymentMethodJson, CustomerCards customerCards) {
		String expiry = paymentMethodJson.get("card").getAsJsonObject().get("exp_month").getAsInt()+"/"+paymentMethodJson.get("card").getAsJsonObject().get("exp_year").getAsInt();
		customerCards.setExpiration_date(expiry);
		return customerCards;
	}

	public ResponseEntity<Object> getSubscriptionPlans() {
		String subscriptionProductTypeId = ctEnvProperties.getSubscriptionProductTypeId();
		String subscriptionProductsUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey()+"/products?where=productType(id=\""+subscriptionProductTypeId+"\")";
		String subscriptionProductRes = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(), subscriptionProductsUrl);
		JsonObject plansJson = MolekuleUtility.parseJsonObject(subscriptionProductRes);
		if(plansJson.has(STATUS_CODE)) {
			MobileErrorResponse mobileErrorResponse = new MobileErrorResponse();
			mobileErrorResponse.setMessage(MolekuleConstant.UNAUTHORIZED_MESSAGE);
			return new ResponseEntity<>(mobileErrorResponse,HttpStatus.UNAUTHORIZED);
		}
		JsonArray plansArray = plansJson.get("results").getAsJsonArray();
		List<com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionPlan> subscriptionPlans = new ArrayList<>();
		AtomicInteger idCount = new AtomicInteger(1);
		if(plansArray.size()>0) {
			for(int i=0;i<plansArray.size();i++) {
				com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionPlan subscriptionPlan = new com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionPlan();
				subscriptionPlan.setId(idCount.getAndIncrement());
				JsonObject subscriptionJson = plansArray.get(i).getAsJsonObject();
				subscriptionPlan.setTitle(subscriptionJson.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get("name").getAsJsonObject().get(EN_US).getAsString());
				subscriptionPlan.setShort_description(subscriptionJson.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get("description").getAsJsonObject().get(EN_US).getAsString());
				subscriptionPlan.setFrequency_units("month");
				subscriptionPlan.setTrigger_sku(subscriptionJson.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get("masterVariant").getAsJsonObject().get("sku").getAsString());
				int period = getSubscriptionPeriod(subscriptionJson);
				subscriptionPlan.setFrequency(String.valueOf(period));
				subscriptionPlan.setCreated_at(subscriptionJson.get("createdAt").getAsString());
				subscriptionPlan.setUpdated_at(subscriptionJson.get("lastModifiedAt").getAsString());
				subscriptionPlan.setIdentifier(subscriptionJson.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get("slug").getAsJsonObject().get(EN_US).getAsString());
				subscriptionPlans.add(subscriptionPlan);
			}
		}
		return new ResponseEntity<>(MobileAppUtility.convertJsonToString(subscriptionPlans),HttpStatus.OK);
	}

	private int getSubscriptionPeriod(JsonObject subscriptionJson) {
		int period = 0;
		JsonArray attributesArray = subscriptionJson.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get("masterVariant").getAsJsonObject().get("attributes").getAsJsonArray();
		for(int i=0;i<attributesArray.size();i++) {
			String name = attributesArray.get(i).getAsJsonObject().get("name").getAsString();
			if("period".equals(name)) {
				period = attributesArray.get(i).getAsJsonObject().get("value").getAsInt();
				break;
			}
		}
		return period;
	}

	public ResponseEntity<Object> subscriptionRenewal(String authkey,SubscriptionObject subscriptionRenewal) {
		BulkUpdateRequestBean bulkUpdateRequestBean = new BulkUpdateRequestBean();
		List<String> subscriptionIds = new ArrayList<>();
		subscriptionIds.add(subscriptionRenewal.getSubscriptionCustomer().getId());
		bulkUpdateRequestBean.setSubscriptionIdList(subscriptionIds);
		bulkUpdateRequestBean.setNextOrderRenewalDate(subscriptionRenewal.getSubscriptionCustomer().getNext_order());
		ResponseEntity<String> renewalUpdateResponse =  customerProfileService.updateBulkRenewalDate(bulkUpdateRequestBean);
		logger.debug("Renewal Update Response -{}",renewalUpdateResponse.getBody());
		return getSubscriptionByID(authkey);
	}

	public JsonObject getCustomerObject(String customerId) {
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customers/"+customerId;
		String token = ctServerHelperService.getStringAccessToken();
		String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerBaseUrl);
		return MolekuleUtility.parseJsonObject(customerResponse);
	}

	private MobileErrorResponse getErrorResponse(String customerId) {
		MobileErrorResponse mobileErrorResponse = new MobileErrorResponse();
		mobileErrorResponse.setMessage("Unable to find customer data for customerId");
		List<String> params = new ArrayList<>();
		params.add(customerId);
		mobileErrorResponse.setParameters(params);
		return mobileErrorResponse;
	}
}
