package com.molekule.api.v1.useraccountservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.Payment;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart.Action;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart.Action.Address;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;

public class CustomerProfileUtility {
	Logger logger = LoggerFactory.getLogger(CustomerProfileUtility.class);

	public Payment createPaymentRequest(Map<String, Object> paymentData) {
		Payment payment = new Payment();
		if(paymentData.get(PAYMENT_INTENT_ID) != null)
			payment.setInterfaceId((String) paymentData.get(PAYMENT_INTENT_ID));
		Payment.AmountPlanned amountPlanned = new Payment.AmountPlanned();
		amountPlanned.setCurrencyCode((String) paymentData.get(CURRENCY_CODE));
		amountPlanned.setCentAmount(Long.parseLong((String) paymentData.get(TOTAL_AMOUNT)));
		payment.setAmountPlanned(amountPlanned);
		Payment.PaymentMethodInfo paymentMethodInfo = new Payment.PaymentMethodInfo();
		String paymentType = (String) paymentData.get("PaymentType");
		if(paymentType != null && (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentType) || "ACH".equals(paymentType))) {
				paymentMethodInfo.setMethod(paymentType);
		}else {
			if(paymentData.get(PAYMENT_INTENT_ID) != null)
				paymentMethodInfo.setMethod("CREDIT_CARD");
		}
		paymentMethodInfo.setPaymentInterface("STRIPE");
		Payment.PaymentMethodInfo.Name name = new Payment.PaymentMethodInfo.Name();
		if(paymentType != null && paymentType.equals(PaymentRequestBean.Payment.PAYMENTTERMS.name())) {
			if(PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(paymentType)) 
				name.setEn("PAYMENT TERMS");
			else if("ACH".equals(paymentType))
				name.setEn("ACH");

		}else {
			if(paymentData.get(PAYMENT_INTENT_ID) != null)
				name.setEn("Credit Card");
		}

		paymentMethodInfo.setName(name);
		payment.setPaymentMethodInfo(paymentMethodInfo);
		List<Payment.Transaction> transactions = new ArrayList<>();
		Payment.Transaction transaction = new Payment.Transaction();
		Payment.Transaction.Amount amount = new Payment.Transaction.Amount();
		amount.setCentAmount(Long.parseLong((String) paymentData.get(TOTAL_AMOUNT)));
		amount.setCurrencyCode((String) paymentData.get(CURRENCY_CODE));
		transaction.setAmount(amount);
		transaction.setType("Charge");
		transaction.setState(PENDING);
		transactions.add(transaction);
		payment.setTransactions(transactions);
		return payment;

	}

	public Action getAddPaymentRequestObj(String paymentId) {
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		action.setActionName("addPayment");
		UpdateActionsOnCart.Action.Payment payment = new UpdateActionsOnCart.Action.Payment();
		payment.setId(paymentId);
		payment.setTypeId("payment");
		action.setPayment(payment);
		return action;
	}

	public Action updateCartBillingAddress(String customObject, String paymentId) {

		JsonObject customJsonObject = MolekuleUtility.parseJsonObject(customObject);
		JsonObject valueObject = customJsonObject.get(VALUE).getAsJsonObject();
		ObjectMapper mapper = new ObjectMapper();
		String billingAddressId = null;
		Value value = new Value();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
		}  catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		List<Payments> payments = value.getPayments();
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (paymentId.equals(payments2.getPaymentId())) {
					billingAddressId = payments2.getBillingAddressId();
					break;
				}
			}
		}
		return getBillingAddressAction(billingAddressId, value);

	}

	public Action getBillingAddressAction(String billingAddressId, Value value) {
		BillingAddresses billingAddress = null;
		List<BillingAddresses> billingAddressList = value.getAddresses().getBillingAddresses();
		Iterator<BillingAddresses> billingAddressItr = billingAddressList.listIterator();
		while (billingAddressItr.hasNext()) {
			BillingAddresses billingAddress2 = billingAddressItr.next();
			if (billingAddress2.getBillingAddressId().equals(billingAddressId)) {
				billingAddress = billingAddress2;
				break;
			}
		}
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		if(Objects.nonNull(billingAddress)) {
			UpdateActionsOnCart.Action.Address address = getAddressAction(billingAddress);
			action.setActionName(SET_BILLING_ADDRESS);
			action.setAddress(address);
		
		}
		return action;
	}
	
	public Action getBillingAddress(BillingAddresses billingAddress) {
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		if(Objects.nonNull(billingAddress)) {
			UpdateActionsOnCart.Action.Address address = getAddressAction(billingAddress);
			action.setActionName(SET_BILLING_ADDRESS);
			action.setAddress(address);
		
		}
		return action;
	}


	public static UpdateActionsOnCart.Action.Address getAddressAction(BillingAddresses billingAddresses) {
		UpdateActionsOnCart.Action.Address address = new UpdateActionsOnCart.Action.Address();
		address.setFirstName(billingAddresses.getFirstName());
		address.setLastName(billingAddresses.getLastName());
		address.setStreetName(billingAddresses.getStreetAddress1());
		address.setCity(billingAddresses.getCity());
		address.setState(billingAddresses.getState());
		address.setPostalCode(billingAddresses.getPostalCode());
		address.setPhone(billingAddresses.getPhoneNumber());
		address.setCountry(billingAddresses.getCountry());
		return address;
	}

	public Action getD2CBillingAddressAction(AddressRequestBean billingAddress) {
		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		if(Objects.nonNull(billingAddress)) {
			UpdateActionsOnCart.Action.Address address = getD2CAddressAction(billingAddress);
			action.setActionName(SET_BILLING_ADDRESS);
			action.setAddress(address);
		}
		return action;
	}

	private Address getD2CAddressAction(AddressRequestBean billingAddress) {
		UpdateActionsOnCart.Action.Address address = new UpdateActionsOnCart.Action.Address();
		address.setId(billingAddress.getId());
		address.setFirstName(billingAddress.getFirstName());
		address.setLastName(billingAddress.getLastName());
		address.setStreetName(billingAddress.getStreetName());
		address.setStreetNumber(billingAddress.getStreetNumber());
		address.setCity(billingAddress.getCity());
		address.setState(billingAddress.getState());
		address.setPostalCode(billingAddress.getPostalCode());
		address.setPhone(billingAddress.getPhone());
		address.setCountry(billingAddress.getCountry());
		address.setCompany(billingAddress.getCompany());
		address.setRegion(billingAddress.getRegion());
		return address;
	}

}
