package com.molekule.api.v1.useraccountservices.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionEstimate;
import com.molekule.api.v1.commonframework.model.mobileapp.SubscriptionObject;
import com.molekule.api.v1.useraccountservices.service.MobileApiService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/go/rest/V1")
public class MobileApiController {

	Logger logger = LoggerFactory.getLogger(MobileApiController.class);

	@Autowired
	@Qualifier(value = "mobileService")
	MobileApiService mobileService;

	/**
	 * getAccessToken method is used to generate CT access token.
	 * 
	 * @param userName and password
	 * @return String - it contains access token.
	 */
	@PostMapping(value = "/integration/customer/token", produces = MediaType.TEXT_PLAIN_VALUE)
	@ApiOperation(value = "access token", response = String.class)
	public String getAccessToken(@RequestParam("username") String userName, @RequestParam("password") String password,@RequestHeader("Authorization") String auth ) {
		return mobileService.getAccessToken(userName, password);
	}

	/**
	 * Get customer By Id
	 * 
	 * @param customerId and authorizationKey
	 * @return String - Customer response
	 */
	@GetMapping(value = "/customers/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get customer by id", response = String.class)
	public ResponseEntity<Object> getCustometById(@PathVariable("customerId") String customerId,@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getCustomerById(customerId);
	}
	
	/**
	 * Get customer
	 * 
	 * @param authorizationKey
	 * @return String - Customer Response
	 */
	@GetMapping(value = "/customers/me", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get current loggedin customer", response = String.class)
	public ResponseEntity<Object> getCustomer(@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getCurrentLoggedinCustomer(authorizationKey);
	}
	/**
	 * get customer subscription
	 * 
	 * @param authorizationKey
	 * @return customer subscription
	 */
	@GetMapping(value = "/vonnda/subscription/me/customer", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Subscription for customer by id", response = String.class)
	public ResponseEntity<Object> getCustomerSubscription(@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getSubscriptionByID(authorizationKey);
	}
	
	/**
	 * Post customer subscription
	 * 
	 * @param authorizationKey
	 * @return customer subscription
	 */
	@PostMapping(value = "/vonnda/subscription/me/customer", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Subscription for customer by id", response = String.class)
	public ResponseEntity<Object> createCustomerSubscription(@RequestHeader("Authorization") String authorizationKey,@ApiParam(value = "Customer Subscription", required = true) @RequestBody SubscriptionObject subscriptionObject,
			@RequestHeader("country") String country) {
		return mobileService.createSubscriptionByCustId(authorizationKey,subscriptionObject,country);
	}
	
	/**
	 * Put customer subscription
	 * 
	 * @param authorizationKey
	 * @return customer subscription
	 */
	@PutMapping(value = "/vonnda/subscription/me/customer", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Subscription for customer by id", response = String.class)
	public ResponseEntity<Object> updateCustomerSubscription(@RequestHeader("Authorization") String authorizationKey,@ApiParam(value = "Customer Subscription", required = true) @RequestBody SubscriptionObject subscriptionCustomer) {
		return mobileService.updateSubscriptionById(authorizationKey,subscriptionCustomer);
	}
	/**
	 * Put customer Renewal
	 * 
	 * @param authorizationKey
	 * @return customer subscription
	 */
	@PutMapping(value = "/vonnda/subscription/me/customer/renewal", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Customer Subscription Renewal", response = String.class)
	public ResponseEntity<Object> subscriptionRenewal(@RequestHeader("Authorization") String authorizationKey,@ApiParam(value = "Subscription Renewal", required = true) @RequestBody SubscriptionObject subscriptionRenewal) {
		return mobileService.subscriptionRenewal(authorizationKey,subscriptionRenewal);
	}
	/**
	 * Post subscription estimate
	 * 
	 * @param authorizationKey
	 * @return customer subscription
	 */
	@PostMapping(value = "/vonnda/subscription/me/customer/estimate", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Subscription estimate", response = String.class)
	public boolean subscriptionEstimate(@RequestHeader("Authorization") String authorizationKey,@ApiParam(value = "Subscription Customer Estimate Query", required = true) @RequestBody SubscriptionEstimate subscriptionEstimate) {
		return mobileService.subscriptionEstimate(authorizationKey, subscriptionEstimate);
	}
	/**
	 * Get Subscription Plans
	 * 
	 * @param - authorizationKey
	 * @return - Subscription plans
	 */
	@GetMapping(value = "/vonnda/subscription/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Subscription Plans", response = String.class)
	public ResponseEntity<Object> getSubscriptionPlans(@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getSubscriptionPlans();
	}
	
	/**
	 * Get Customer Payment Info
	 * 
	 * @param - authorizationKey
	 * @return - customer payment info
	 */
	@GetMapping(value = "/stripe/payments/me/customer", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Customer Payment Info", response = String.class)
	public ResponseEntity<Object> getCustomerPaymentInfo(@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getCustomerPaymentInfo(authorizationKey);
	}
	
	/**
	 * Get Customer card info
	 * 
	 * @param - authorizationKey
	 * @return - Customer card Info
	 */
	@GetMapping(value = "/stripe/payments/me/card-info", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Customer Card Info", response = String.class)
	public ResponseEntity<Object> getCustomerCardInfo(@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getCustomerCardInfo(authorizationKey);
	}
	
	/**
	 * Get customer cards
	 * 
	 * @param - authorizationKey
	 * @return - customer cards
	 */
	@GetMapping(value = "/stripe/payments/me/cards", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Customer Cards", response = String.class)
	public ResponseEntity<Object> getCustomerCards(@RequestHeader("Authorization") String authorizationKey) {
		return mobileService.getCustomerCards(authorizationKey);
	}
	

}
