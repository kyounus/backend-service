package com.molekule.api.v1.useraccountservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountDto;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.Custom;
import com.molekule.api.v1.commonframework.dto.registration.Customer;
import com.molekule.api.v1.commonframework.dto.registration.CustomerAction;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.CustomerSignupResponseDTO;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.products.ProductVariant;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;
import com.molekule.api.v1.commonframework.model.registration.AddressV2;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.CarrierOption;
import com.molekule.api.v1.commonframework.model.registration.CarrierOptionCustomObject;
import com.molekule.api.v1.commonframework.model.registration.ChangePasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoBean;
import com.molekule.api.v1.commonframework.model.registration.ForgotPasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.model.registration.ProductResponseBean;
import com.molekule.api.v1.commonframework.model.registration.RefreshToken;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.registration.ResetPasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressResponse;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.SignRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.registration.AuthenticationHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.CustomerSourceEnum;
import com.molekule.api.v1.commonframework.util.CustomerStatusEnum;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.useraccountservices.util.Cache;
import com.molekule.api.v1.useraccountservices.util.ProcessOrderSendGridThread;
import com.molekule.api.v1.useraccountservices.util.RegistrationUtility;
import com.molekule.api.v1.useraccountservices.util.TealiumSendgridThread;
import com.molekule.api.v1.useraccountservices.util.UpdatePaymentThread;

import reactor.core.publisher.Mono;

/**
 * The class CTClientAccess is used to write the api calls for the Commerce
 * Tools collections.
 * 
 * @version 1.0
 */
@Service("registrationService")
public class RegistrationService {
	Logger logger = LoggerFactory.getLogger(RegistrationService.class);
	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier(value = "registrationConfirmation")
	TemplateMailRequestHelper registartionConfirmationSender;

	@Autowired
	@Qualifier(value = "paymentTermsPendingApprovalSender")
	TemplateMailRequestHelper paymentTermsPendingApprovalSender;

	@Autowired
	@Qualifier(value = "paymentTermsRequestConfirmationSender")
	TemplateMailRequestHelper paymentTermsRequestConfirmationSender;

	@Autowired
	@Qualifier(value = "paymentTermsSuccessConfirmationSender")
	TemplateMailRequestHelper paymentTermsSuccessConfirmationSender;

	@Autowired
	@Qualifier(value = "taxExemptionPendingApprovalSender")
	TemplateMailRequestHelper taxExemptionPendingApprovalSender;

	@Autowired
	@Qualifier(value = "taxExemptionSuccessConfirmationSender")
	TemplateMailRequestHelper taxExemptionSuccessConfirmationSender;

	@Autowired(required = true)
	@Qualifier(value = "freightInformationConfirmationSender")
	TemplateMailRequestHelper freightInformationConfirmationSender;

	@Autowired
	@Qualifier(value = "orderUpdatedMailSender")
	TemplateMailRequestHelper orderUpdatedMailSender;

	@Autowired
	@Qualifier(value = "customerPasswordUpdateMailSender")
	TemplateMailRequestHelper customerPasswordUpdateMailSender;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("checkoutHelperService")
	CheckoutHelperService checkoutHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	@Autowired
	@Qualifier("authenticationHelperService")
	AuthenticationHelperService authenticationHelperService;

	/**
	 * getCustomer method is sued to get the customer information while doing the
	 * quick registration.
	 * 
	 * @param customerDTO
	 * @param registrationBean
	 * @return String - it contains the response of the api call.
	 */
	public String createCustomer(CustomerDTO customerDTO, RegistrationBean registrationBean) {
		String password = customerDTO.getPassword();
		String accountId = null;
		customerDTO.setPassword(ctEnvProperties.getPw0rd());
		customerDTO.setCountryCode(registrationBean.getCountryCode());
		CustomerSignupResponseDTO customerSignupResponseDTO = null;
		String channel = registrationBean.getChannel().name();
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		//Checking company name is already exist or not
		isExistingAccount(customerDTO, registrationBean, channel, token);

		String key = null;
		// step-1 Creating custom Account Object
		JsonObject jsonAccountObj = null;
		if(B2B.equals(channel)) {
			key = RegistrationUtility.generateKey(customerDTO.getCompanyName(), customerDTO.getFirstName());
			if (StringUtils.hasText(registrationBean.getAccountId())) {
				accountId = registrationBean.getAccountId();
			} else {
				AccountCustomObject createdCustomObject = RegistrationUtility.getAccountCustomObject("b2b", key.trim(),
						customerDTO, new AccountCustomObject(), null);
				String accountResponse = getAccountID(createdCustomObject, token);
				jsonAccountObj = MolekuleUtility.parseJsonObject(accountResponse);
				if (jsonAccountObj.get(STATUS_CODE) != null) {
					return accountResponse;
				}
				accountId = jsonAccountObj.get("id").getAsString();
			}
		}
		// Step -2 Creating the Customer Object
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customers?";
		String customerNumber = RegistrationUtility.getCustomerNumber();
		customerDTO.setCustomerNumber(customerNumber);
		String customerGroupId = null;
		String customerGroupName = "";
		Custom custom;
		if(B2B.equals(channel)) {
			customerGroupId = getCustomerGroupId(registrationBean, customerGroupId);
			String customerGroup = (String) registrationHelperService.getCustomerGroup(customerGroupId);
			JsonObject customerGroupJsonObject = MolekuleUtility.parseJsonObject(customerGroup);
			customerGroupName = customerGroupJsonObject.get("name").getAsString();
		}
		Map<String, Object> createCustomObjectParamMap = new HashMap<>();
		createCustomObjectParamMap.put("typeId", ctEnvProperties.getCustomAttributeId());
		createCustomObjectParamMap.put("accountId", accountId);
		createCustomObjectParamMap.put("salutation", EMPTY);
		createCustomObjectParamMap.put("email", customerDTO.getEmail());
		createCustomObjectParamMap.put("phone", customerDTO.getPhone());
		createCustomObjectParamMap.put(CUSTOMER_GROUP_NAME, customerGroupName);
		createCustomObjectParamMap.put("countryCode", customerDTO.getCountryCode());
		createCustomObjectParamMap.put("store", registrationBean.getStore());
		createCustomObjectParamMap.put(CHANNEL, channel);
		createCustomObjectParamMap.put("termsOfService", registrationBean.isTermsOfService());
		createCustomObjectParamMap.put("marketingOffers", registrationBean.isMarketingOffers());
		custom = RegistrationUtility.createCustomObj(false, createCustomObjectParamMap);
		customerDTO.setCustom(custom);

		String responseData = (String) sendPostRequest(customerBaseUrl, token, customerDTO);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(responseData);
		if (jsonObject.get(STATUS_CODE) != null) {
			removeCustomObject(token, jsonAccountObj);
			return RegistrationUtility.removeEmailFromErrorsMessage(jsonObject);
		}

		String customerId = jsonObject.getAsJsonObject(CUSTOMER).get("id").getAsString();
		setCache(channel, key, customerId);
		Customer customer = RegistrationUtility.createCustomerObj(responseData);

		// customer group and key assignment for quick registration
		Map<String, String> updateCustomerFieldsMap = new HashMap<>();
		updateCustomerFieldsMap.put(CHANNEL, channel);
		updateCustomerFieldsMap.put(TOKEN, token);
		updateCustomerFieldsMap.put("customerGroupId", customerGroupId);
		updateCustomerFieldsMap.put(CUSTOMER_GROUP_NAME, customerGroupName);
		updateCustomerFieldsMap.put(CUSTOMER_ID, customerId);
		String customerResponse = updateCustomerGroupAndKey(customerDTO, registrationBean, jsonObject, updateCustomerFieldsMap);

		// Step -3 - update the customer into Custom Account Object
		AccountCustomObject accountCustomObject = null;
		if(B2B.equals(channel)) {
			key = getKey(registrationBean, key);
			Object obj = registrationHelperService.getAccountCustomObject(key);
			if (obj instanceof AccountCustomObject) {
				accountCustomObject = (AccountCustomObject) obj;
			} else if (obj instanceof String) {
				return (String) obj;
			}
			AccountCustomObject customerUpdateObject = getCustomObject(customerDTO, registrationBean, token, key,
					customerId, customer, accountCustomObject);
			String response = getAccountID(customerUpdateObject, token);
			logger.trace("Step-3 Response: {}", response);
		}
		// Cognito signup
		doCognitoSignup(customerDTO, password, accountId, channel, token, customerId, customerResponse);

		triggerWelcomeMail(customerDTO, channel, accountCustomObject);
		customerSignupResponseDTO = RegistrationUtility.getCustomerResponse(customerId, accountId, customerDTO);
		tealiumHelperService.quickRegister(RegistrationUtility.convertObjectToJsonStr(customerSignupResponseDTO),
				customerDTO);
		return RegistrationUtility.convertObjectToJsonStr(customerSignupResponseDTO);

	}

	public String getCustomerGroupId(RegistrationBean registrationBean, String customerGroupId) {
		if (StringUtils.hasText(registrationBean.getCustomerGroupId())) {
			customerGroupId = registrationBean.getCustomerGroupId();
		}else if(registrationBean.getCustomerSource()!= CustomerSourceEnum.MC) {
			customerGroupId = ctEnvProperties.getCustomerGroupId();
		}
		return customerGroupId;
	}

	public void setCache(String channel, String key, String customerId) {
		if(B2B.equals(channel)) {
			Cache.getInstance().addCache(customerId, key);			
		}
	}

	public void triggerWelcomeMail(CustomerDTO customerDTO, String channel, AccountCustomObject accountCustomObject) {
		if(B2B.equals(channel)) {
			// SendGrid Api call for Quick Registration Welcome mail
			SendGridModel sendGridModel = getSendGridModel(customerDTO, accountCustomObject);
			registartionConfirmationSender.sendMail(sendGridModel);

		}
	}

	public void doCognitoSignup(CustomerDTO customerDTO, String password, String accountId, String channel,
			String token, String customerId, String customerResponse) {
		Long version = 0L;
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		if (customerJsonObject.get(STATUS_CODE) == null) {
			version = customerJsonObject.get(VERSION).getAsLong();
		}
		registrationHelperService.setCognitoResponseDetails(customerDTO, channel, password, accountId, token, customerId, version);
	}

	public String getKey(RegistrationBean registrationBean, String key) {
		if(StringUtils.hasText(registrationBean.getAccountId())) {
			key = registrationHelperService.getAccountKeyById(registrationBean.getAccountId());
		}
		return key;
	}

	public void removeCustomObject(String token, JsonObject jsonAccountObj) {
		if(jsonAccountObj != null) {
			String container = jsonAccountObj.get(CONTAINER).getAsString();
			String accountKey = jsonAccountObj.get("key").getAsString();
			String accountDeleteUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects/"
					+ container + "/" + accountKey;
			netConnectionHelper.deleteRequest(accountDeleteUrl, token);
		}
	}

	public String updateCustomerGroupAndKey(CustomerDTO customerDTO, RegistrationBean registrationBean, JsonObject jsonObject, Map<String, String> updateCustomerFieldsMap) {
		String channel = updateCustomerFieldsMap.get(CHANNEL);
		String token = updateCustomerFieldsMap.get(TOKEN);
		String customerGroupId = updateCustomerFieldsMap.get("customerGroupId");
		String customerGroupName = updateCustomerFieldsMap.get(CUSTOMER_GROUP_NAME);
		String customerId = updateCustomerFieldsMap.get(CUSTOMER_ID);
		String customerIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
				+ customerId;

		CustomerAction customerAction = new CustomerAction();
		customerAction.setVersion(jsonObject.getAsJsonObject(CUSTOMER).get(VERSION).getAsLong());
		List<CustomerAction.Actions> list = new ArrayList<>();
		// customer group
		if(B2B.equals(channel)) {
			getCustomerGroupObject(customerDTO, customerGroupId, customerGroupName, customerId, list);			
		}
		//customer Status
		getCustomerSource(registrationBean, list);

		list.add(setCustomAttributes("admin", false));
		list.add(setCustomAttributes("salutation", EMPTY));
		list.add(setCustomAttributes(ACCOUNT_LOCKED, false));
		list.add(setCustomAttributes(CHANNEL, channel));
		if(StringUtils.hasText(customerDTO.getPhone())) {
			list.add(setCustomAttributes(PHONE, customerDTO.getPhone()));			
		}
		list.add(setCustomAttributes("customerSource", registrationBean.getCustomerSource().name()));
		if(!com.amazonaws.util.StringUtils.isNullOrEmpty(registrationBean.getRegisteredChannel())) {
			list.add(setCustomAttributes("registeredChannel", registrationBean.getRegisteredChannel()));
		} 
		if(!com.amazonaws.util.StringUtils.isNullOrEmpty(registrationBean.getComments())) {
			list.add(setCustomAttributes("comments", registrationBean.getComments()));
		}

		List<String> mailList = new ArrayList<>();
		mailList.add(customerDTO.getEmail());
		list.add(setCustomAttributes("invoiceEmailAddress", mailList));

		customerAction.setActions(list);
		// updating customer group and key in CT
		return (String) sendPostRequest(customerIdUrl, token, customerAction);
	}

	public void isExistingAccount(CustomerDTO customerDTO, RegistrationBean registrationBean, String channel,
			String token) {
		if(B2B.equals(channel)) {
			String accountCheckUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
					+ "?where=value(companyName=\"" + customerDTO.getCompanyName() + "\")&sort=createdAt desc";

			String companyResponse = netConnectionHelper.sendGetWithoutBody(token, accountCheckUrl);
			JsonObject companyObject = MolekuleUtility.parseJsonObject(companyResponse);
			if(!StringUtils.hasText(registrationBean.getAccountId()) && companyObject.has(RESULTS) && companyObject.get(RESULTS).getAsJsonArray().size() >0) {
				registrationBean.setAccountId(companyObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString());
			}
		}
	}

	private com.molekule.api.v1.commonframework.dto.registration.CustomerAction.Actions setCustomAttributes(String name, Object value) {
		CustomerAction.Actions actions = new CustomerAction.Actions();
		actions.setAction(SET_CUSTOM_FIELD);
		actions.setName(name);
		actions.setValue(value);
		return actions;
	}

	protected String getChannel(RegistrationBean registrationBean, AccountCustomObject customerUpdateObject) {
		String channel = registrationBean.getChannel().toString();
		if(registrationBean.getCustomerSource() == CustomerSourceEnum.MC 
				|| null==channel || channel.isEmpty()) {
			channel = customerUpdateObject.getContainer().toUpperCase();
		}
		return channel;
	}

	private void getCustomerGroupObject(CustomerDTO customerDTO, String customerGroupId, String customerGroupName,
			String customerId, List<CustomerAction.Actions> list) {
		CustomerAction.Actions.CustomerGroup customerGroupObj = new CustomerAction.Actions.CustomerGroup();
		customerGroupObj.setId(customerGroupId);
		customerGroupObj.setCustomerGroupName(customerGroupName);
		customerGroupObj.setTypeId("customer-group");
		CustomerAction.Actions actionsObj1 = new CustomerAction.Actions();
		actionsObj1.setAction("setCustomerGroup");
		actionsObj1.setCustomerGroup(customerGroupObj);
		list.add(actionsObj1);
		customerDTO.setCustomerGroup(customerGroupObj);
		if(null == customerDTO.getKey()) {
			// customer key
			CustomerAction.Actions actionsObj2 = new CustomerAction.Actions();
			actionsObj2.setAction("setKey");
			actionsObj2.setKey(customerId);
			list.add(actionsObj2);
		}
	}

	private void getCustomerSource(RegistrationBean registrationBean, List<CustomerAction.Actions> list) {
		if(StringUtils.hasText(registrationBean.getCustomerSource().toString())) {
			CustomerAction.Actions actionsObj3 = new CustomerAction.Actions();
			if(registrationBean.getCustomerSource().equals(CustomerSourceEnum.MC)) {
				actionsObj3.setAction("setCustomField");
				actionsObj3.setName("customerStatus");
				actionsObj3.setValue(CustomerStatusEnum.LEAD.toString());
				list.add(actionsObj3);
			}else if(registrationBean.getCustomerSource().equals(CustomerSourceEnum.B2B_WEB)
					|| registrationBean.getCustomerSource().equals(CustomerSourceEnum.D2C_WEB)) {
				actionsObj3.setAction("setCustomField");
				actionsObj3.setName("customerStatus");
				actionsObj3.setValue(CustomerStatusEnum.REGISTERED.toString());
				list.add(actionsObj3);
			}
		}
	}

	private AccountCustomObject getCustomObject(CustomerDTO customerDTO, RegistrationBean registrationBean,
			String token, String key, String customerId, Customer customer, AccountCustomObject accountCustomObject) {
		AccountCustomObject customerUpdateObject;
		if(StringUtils.hasText(registrationBean.getAccountId())) {

			customerUpdateObject = RegistrationUtility.getAccountCustomObject("b2b", key.trim(),
					customer, accountCustomObject, customerId);
		}else {
			RegistrationUtility.getAccountCustomObject("b2b", key.trim(),
					customer, accountCustomObject, customerId);
			customerUpdateObject = setCategoryAndSalesRep(customerDTO.getCompanyCategoryId(), token, key, customerId,
					accountCustomObject);
		}
		return customerUpdateObject;
	}

	private SendGridModel getSendGridModel(CustomerDTO customerDTO, AccountCustomObject accountCustomObject) {
		SendGridModel sendGridModel = new SendGridModel();
		sendGridModel.setEmail(customerDTO.getEmail());
		sendGridModel.setFirstName(customerDTO.getFirstName());
		if (accountCustomObject != null && accountCustomObject.getValue() != null && accountCustomObject.getValue().getSalesRepresentative()!= null) {

			if (accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ").length > 1) {
				sendGridModel.setSalesRepFirstName(
						accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ")[0]);
				sendGridModel.setSalesRepLastName(
						accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ")[1]);
			} else {
				sendGridModel.setSalesRepFirstName(
						accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ")[0]);
				sendGridModel.setSalesRepLastName(EMPTY);
			}

			sendGridModel.setSalesRepPhoneNumber(accountCustomObject.getValue().getSalesRepresentative().getPhone());
			sendGridModel.setSalesRepEmail(accountCustomObject.getValue().getSalesRepresentative().getEmail());
		}
		sendGridModel.setSalesRepCalendyLink("Calendy Link");
		return sendGridModel;
	}

	private AccountCustomObject setCategoryAndSalesRep(String companyCategoryId, String token, String key,
			String customerId, AccountCustomObject accountCustomObject) {
		// Get category Details from CT server.
		String categaoryBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects"
				+ "/b2b" + "/" + "mo_company_cat";
		String ctCategoryResponse = netConnectionHelper.sendGetWithoutBody(token, categaoryBaseUrl);
		List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);
		List<CategoryDto> filterdCategoryDtoList = categoryDtoList.stream()
				.filter(dto -> dto.getId().equals(companyCategoryId)).collect(Collectors.toList());
		CategoryDto categoryDto = filterdCategoryDtoList.get(0);
		RegistrationUtility.getAccountCustomObject("b2b", key.trim(), categoryDto, accountCustomObject, customerId);
		// Get SalesRep Details from CT server.
		String salesRepId = categoryDto.getSalesRepId();
		String salesRepBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects"
				+ "/b2b" + "/" + "mo_sales_rep";
		String ctSalesRepResponse = netConnectionHelper.sendGetWithoutBody(token, salesRepBaseUrl);
		List<SalesRepresentativeDto> salesRepresentativeDtoList = MolekuleUtility
				.getSalesRepresentativeDtoList(ctSalesRepResponse);
		SalesRepresentativeDto salesRespresentativeDto = getSalesRepDetails(salesRepresentativeDtoList, salesRepId);
		return RegistrationUtility.getAccountCustomObject("b2b", key.trim(), salesRespresentativeDto,
				accountCustomObject, customerId);
	}

	/**
	 * getSalesRepDetails method is used to get the sales representative detail
	 * based on sales rep id mentioned in the category.
	 * 
	 * @param salesRespresentativeDtoList
	 * @param salesRepId
	 * @return SalesRespresentativeDto - contains the sales representative details.
	 */
	private SalesRepresentativeDto getSalesRepDetails(List<SalesRepresentativeDto> salesRespresentativeDtoList,
			String salesRepId) {
		if (salesRepId != null) {
			List<SalesRepresentativeDto> filteredList = salesRespresentativeDtoList.stream()
					.filter(dto -> dto.getId().equals(salesRepId)).collect(Collectors.toList());
			return filteredList.get(0);
		} else {
			List<SalesRepresentativeDto> filteredList = salesRespresentativeDtoList.stream()
					.filter(dto -> dto.getName().equals("Other")).collect(Collectors.toList());
			return filteredList.get(0);
		}

	}

	/**
	 * updateCustomerId method is used to get the updated customer id while doing
	 * the registration.
	 * 
	 * @param customerDTO
	 * @param token
	 */
	public String updateCustomerId(String customerId, CustomerDTO customerDTO, String token) {
		String customerUpdateBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
				+ customerId + "/?";
		return (String) sendPostRequest(customerUpdateBaseUrl, token, customerDTO);
	}

	/**
	 * assignCustomerGroup method is used to associate customer group to customer
	 * while doing the registration.
	 * 
	 * @param customerId
	 * @param customerGroupRequestBean
	 * @param token
	 * @return String
	 */
	public String assignCustomerGroup(String customerId,CustomerGroupRequestBean customerGroupRequestBean,String token) {
		String customerGroupurl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
				+ customerId + "/?";
		return (String) sendPostRequest(customerGroupurl, token, customerGroupRequestBean);
	}

	/**
	 * updateCompanyInfo method is used to get the updated company info while doing
	 * the registration of company information.
	 * 
	 * @param companyInfoBean
	 * @return String
	 */
	public String updateCompanyInfo(CompanyInfoBean companyInfoBean) {

		String accountUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=id=\"" + companyInfoBean.getAccountId() + "\"";
		String token = ctServerHelperService.getStringAccessToken();
		AccountDto accountResult = netConnectionHelper.sendGetAccountObject(token, accountUrl);
		if(accountResult.getResults().isEmpty()) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "Invalid AccountId");
			return errorResponse.toString();
		}
		AccountCustomObject accountCustomObject = accountResult.getResults().get(0);
		String response;
		String key = accountCustomObject.getKey();
		AccountCustomObject custom = RegistrationUtility.getAccountCustomObject("b2b", key, companyInfoBean,
				accountCustomObject, null);
		if (StringUtils.hasText(companyInfoBean.getCompanyCategoryId())) {
			custom = setCategoryAndSalesRep(companyInfoBean.getCompanyCategoryId(), token, key, null, custom);
			response = getAccountID(custom, ctServerHelperService.getStringAccessToken());
		} else {
			response = getAccountID(custom, ctServerHelperService.getStringAccessToken());
		}
		JsonObject valueObject = MolekuleUtility.parseJsonObject(response).getAsJsonObject().get(VALUE).getAsJsonObject();
		ObjectMapper mapper = new ObjectMapper();
		Value postResponseValue = new Value();
		try {
			postResponseValue = mapper.readValue(valueObject.toString(), Value.class);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}


		ExecutorService executorService = Executors.newFixedThreadPool(100);
		ProcessOrderSendGridThread processOrderSendGridThread = new ProcessOrderSendGridThread();
		Map<String,String> requestData = new HashMap<>();
		requestData.put(CUSTOMER_ID, companyInfoBean.getCustomerId());
		requestData.put(ACCOUNT_ID,companyInfoBean.getAccountId());
		processOrderSendGridThread.setTaxExemptionPendingApprovalSender(taxExemptionPendingApprovalSender);
		processOrderSendGridThread.setTaxExemptionSuccessConfirmationSender(taxExemptionSuccessConfirmationSender);
		processOrderSendGridThread.setRequestData(requestData);
		processOrderSendGridThread.setPostResponseValue(postResponseValue);
		processOrderSendGridThread.setRegistrationHelperService(registrationHelperService);
		processOrderSendGridThread.setCheckoutHelperService(checkoutHelperService);
		executorService.execute(processOrderSendGridThread);
		executorService.shutdown();

		return response;
	}

	/**
	 * getAccountID method is used to get the account Id information.
	 * 
	 * @param customObject
	 * @param token
	 * @return String
	 */
	public String getAccountID(AccountCustomObject customObject, String token) {
		String accountIdBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/custom-objects?";
		return (String) sendPostRequest(accountIdBaseUrl, token, customObject);

	}

	/**
	 * addShippingAddress method is used to register the shipping information.
	 * 
	 * @param shippingAddressRequestBean
	 * @return String
	 */
	public String addShippingAddress(ShippingAddressRequestBean shippingAddressRequestBean) {
		AccountCustomObject accountCustomObject = registrationHelperService.getAccountByAccountId(shippingAddressRequestBean.getAccountId());
		AccountCustomObject customObject = RegistrationUtility.getAccountCustomObject("b2b", accountCustomObject.getKey(),
				shippingAddressRequestBean, accountCustomObject, null);
		return getAccountID(customObject, ctServerHelperService.getStringAccessToken());
	}

	/**
	 * getProductById method is used to get the product information by passing
	 * productId.
	 * 
	 * @param productId
	 * @return String
	 */
	public String getProductById(String productId) {
		ProductResponseBean responseBean = new ProductResponseBean();
		String token = ctServerHelperService.getStringAccessToken();
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/products/").append(productId).toString();
		ProductBean productBeanResponseData = netConnectionHelper.sendGetProductDataWithoutBody(token, url);
		ProductData current = productBeanResponseData.getMasterData().getCurrent();
		ProductVariant masterData = current.getMasterVariant();
		String pageId = current.getSlug().get(EN_US);

		String ratingsResponseData = getRatingsandReviews(pageId);
		JsonObject ratingsResponseJsonObject = MolekuleUtility.parseJsonObject(ratingsResponseData);
		if (ratingsResponseJsonObject.get(STATUS_CODE) != null) {
			return ratingsResponseData;
		}
		JsonArray resultsArray = ratingsResponseJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		if (resultsObject.getAsJsonObject("rollup") != null) {
			JsonObject rollUpObject = resultsObject.getAsJsonObject("rollup").getAsJsonObject();
			String averageRating = rollUpObject.get("average_rating").getAsString();
			String reviewCount = rollUpObject.get("review_count").getAsString();
			responseBean.setAverageRating(averageRating);
			responseBean.setReviewCount(reviewCount);
		}

		TypedMoney pricesValue = masterData.getPrices().get(0).getValue();
		Long centAmount = pricesValue.getCentAmount();
		Integer fractionDigits = pricesValue.getFractionDigits();
		Integer format = 1;
		for (int i = 0; i < fractionDigits; i++) {
			format = format * 10;
		}
		Long price = centAmount / format;

		responseBean.setProductId(productId);
		responseBean.setProductName(current.getName().get(EN_US));
		responseBean.setProductDescription(current.getDescription().get(EN_US));
		if (masterData.getAvailability() != null) {
			responseBean.setInStock(masterData.getAvailability().getIsOnStock());
			responseBean.setAvailableQuantity(masterData.getAvailability().getAvailableQuantity());
		}
		responseBean.setPrice(Float.parseFloat(price.toString()));
		return RegistrationUtility.convertObjectToJsonStr(responseBean);
	}

	/**
	 * getRatingsandReviews method is used to call power review api for ratings and
	 * reviews.
	 * 
	 * @param pageId
	 * @return String
	 */
	private String getRatingsandReviews(String pageId) {
		String ratingsUrl = new StringBuilder().append(ctEnvProperties.getPowerReviewHost()).append("/m/")
				.append(ctEnvProperties.getPowerReviewMerchantIdTestUS()).append("/l/en_US/product/").append(pageId)
				.append("/reviews?sort=HighestRating&apikey=").append(ctEnvProperties.getPowerReviewApiKey())
				.toString();
		logger.info("ratingsUrl: {}", ratingsUrl);
		return netConnectionHelper.sendGetWithoutBody(EMPTY, ratingsUrl);
	}

	/**
	 * sendPostRequest method is used to send the post request through webclient to
	 * the CT api calls.
	 * 
	 * @param baseUrl
	 * @param token
	 * @param requestData
	 * @return Object
	 */
	public Object sendPostRequest(String baseUrl, String token, Object requestData) {
		WebClient webClient = netConnectionHelper.getWebClient(baseUrl);
		Mono<String> response = webClient.post().header("Authorization", token).accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(requestData), Object.class).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
						return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
					}
					return clientResponse.bodyToMono(String.class);
				});
		return response.block();
	}

	/**
	 * createPaymentOption method is used to create new payment option for a
	 * customer.
	 * 
	 * @param customerId
	 * @param paymentRequestBean
	 * @return String
	 */
	public AccountCustomObject createPaymentOption(String customerId, PaymentRequestBean paymentRequestBean) {
		logger.info("createPaymentOption implementation starts");
		String token = ctServerHelperService.getStringAccessToken();
		AccountCustomObject accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,token);
		List<Payments> oldPaymentList = accountCustomObject.getValue().getPayments();
		AccountCustomObject customObject = RegistrationUtility.getAccountCustomObject("b2b", accountCustomObject.getKey(), paymentRequestBean,
				accountCustomObject, customerId);
		if(PaymentRequestBean.Payment.CREDITCARD.toString().equals(paymentRequestBean.getPaymentPreferences())) {
			setCardObject(customObject, oldPaymentList);
		}
		String response = getAccountID(customObject, token);
		performTealiumAndSendGridOperation(customerId, paymentRequestBean, response);
		logger.info("createPaymentOption implementation ends");
		return customObject;
	}
	private AccountCustomObject setCardObject(AccountCustomObject customObject, List<Payments> oldPaymentList) {
		List<Payments> newPaymentList = customObject.getValue().getPayments();
		if(oldPaymentList != null) {
			newPaymentList.removeAll(oldPaymentList);
		}
		Payments payments = newPaymentList.get(0);
		payments.setCardObject(registrationHelperService.getPaymentCardDetails(payments.getPaymentToken()));
		if(oldPaymentList != null) {
			oldPaymentList.add(payments);
			customObject.getValue().setPayments(oldPaymentList);
		} else {
			List<Payments> updatedPaymentList = new ArrayList<>();
			updatedPaymentList.add(payments);
			customObject.getValue().setPayments(updatedPaymentList);
		}
		return customObject;
	}
	/**
	 * performTealiumAndSendGridOperation method is used to execute thread for sendgrid and tealium.
	 * 
	 * @param customerId
	 * @param paymentRequestBean
	 * @param response
	 */
	private void performTealiumAndSendGridOperation(String customerId, PaymentRequestBean paymentRequestBean, String response) {

		ExecutorService executorService = Executors.newFixedThreadPool(100);
		TealiumSendgridThread telSendgridThread = new TealiumSendgridThread();
		telSendgridThread.setPaymentTermsPendingApprovalSender(paymentTermsPendingApprovalSender);
		telSendgridThread.setPaymentTermsRequestConfirmationSender(paymentTermsRequestConfirmationSender);
		telSendgridThread.setNetConnectionHelper(netConnectionHelper);
		telSendgridThread.setPaymentRequestBean(paymentRequestBean);
		telSendgridThread.setRegistrationHelperService(registrationHelperService);
		telSendgridThread.setTealiumHelperService(tealiumHelperService);
		telSendgridThread.setCtServerHelperService(ctServerHelperService);
		Map<String,String> requestData = new HashMap<>();
		requestData.put(CUSTOMER_ID, customerId);
		requestData.put("response",response);
		telSendgridThread.setRequestData(requestData);
		executorService.execute(telSendgridThread);
		executorService.shutdown();
	}

	/**
	 * updatePaymentOption method is used to payment details.
	 * 
	 * @param customerId
	 * @param paymentId
	 * @param updatePaymentRequestBean
	 * @param accountId
	 * @return
	 */
	public String updatePaymentOption(String customerId, String paymentId,
			UpdatePaymentRequestBean updatePaymentRequestBean, String accountId) {
		if (Objects.isNull(accountId)) {
			accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);
		}
		String billingAddressId = EMPTY;
		String response = EMPTY;
		String accountStringResponse = netConnectionHelper.sendGetWithoutBody(
				ctServerHelperService.getStringAccessToken(),
				registrationHelperService.getCTCustomObjectsByAccountIdUrl(accountId));
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
		if (accountJsonObject.get(STATUS_CODE) != null) {
			return null;
		}
		if (accountJsonObject.getAsJsonArray(RESULTS) != null) {
			JsonArray resultsArray = accountJsonObject.get(RESULTS).getAsJsonArray();
			JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
			if (resultsObject.getAsJsonObject(VALUE) != null) {
				JsonObject valueObject = resultsObject.get(VALUE).getAsJsonObject();
				List<Payments> paymentList = new ArrayList<>();
				billingAddressId = getBillingAddressId(paymentId, updatePaymentRequestBean, billingAddressId,
						valueObject, paymentList);
				AddressV2 address = setBillingAddress(updatePaymentRequestBean, billingAddressId, valueObject);
				AccountCustomObject accountObject = getAcountCustomObject(paymentId, updatePaymentRequestBean,
						resultsObject, valueObject, paymentList, address);

				response = getAccountID(accountObject, ctServerHelperService.getStringAccessToken());

			}
		}

		ExecutorService executorService = Executors.newFixedThreadPool(100);
		UpdatePaymentThread updatePaymentThread = new UpdatePaymentThread();
		Map<String,String> requestData = new HashMap<>();
		requestData.put(CUSTOMER_ID, customerId);
		requestData.put(ACCOUNT_ID, accountId);
		requestData.put("response",response);
		updatePaymentThread.setRequestData(requestData);
		updatePaymentThread.setUpdatePaymentRequestBean(updatePaymentRequestBean);
		updatePaymentThread.setTealiumHelperService(tealiumHelperService);
		updatePaymentThread.setRegistrationHelperService(registrationHelperService);
		updatePaymentThread.setPaymentTermsSuccessConfirmationSender(paymentTermsSuccessConfirmationSender);
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		updatePaymentThread.setAccountCustomObject(accountCustomObject);
		executorService.execute(updatePaymentThread);
		executorService.shutdown();
		return response;
	}

	private String getBillingAddressId(String paymentId, UpdatePaymentRequestBean updatePaymentRequestBean,
			String billingAddressId, JsonObject valueObject, List<Payments> paymentList) {
		if (valueObject.getAsJsonArray(PAYMENTS) != null) {
			JsonArray paymentsArray = valueObject.get(PAYMENTS).getAsJsonArray();
			for (int i = 0; i < paymentsArray.size(); i++) {
				billingAddressId = processPaymentList(paymentId, updatePaymentRequestBean, billingAddressId,
						paymentList, paymentsArray, i);
			}
		}
		return billingAddressId;
	}

	private AddressV2 setBillingAddress(UpdatePaymentRequestBean updatePaymentRequestBean, String billingAddressId,
			JsonObject valueObject) {
		List<BillingAddresses> billingAddressList = new ArrayList<>();
		List<ShippingAddresses> shippingList = new ArrayList<>();
		if (valueObject.get(ADDRESSES) != null) {
			RegistrationUtility.updateAddressList(updatePaymentRequestBean, billingAddressId, valueObject,
					billingAddressList, shippingList);
		}
		AddressV2 address = new AddressV2();
		address.setBillingAddresses(billingAddressList);
		address.setShippingAddresses(shippingList);
		return address;
	}

	private String processPaymentList(String paymentId, UpdatePaymentRequestBean updatePaymentRequestBean,
			String billingAddressId, List<Payments> paymentList, JsonArray paymentsArray, int i) {
		Payments payments = new Payments();
		JsonObject paymentObject = paymentsArray.get(i).getAsJsonObject();
		if (paymentId.equals(paymentObject.get("paymentId").getAsString())) {
			billingAddressId = paymentObject.get(MolekuleConstant.BILLING_ADDRESS_ID).getAsString();
			if (updatePaymentRequestBean.getAction().equals(UpdatePaymentRequestBean.Action.CHANGE)) {
				String paymentType = paymentObject.get(MolekuleConstant.PAYMENT_TYPE).getAsString();
				if (paymentType.equals(PaymentRequestBean.Payment.CREDITCARD.toString()) ||
						paymentType.equals(PaymentRequestBean.Payment.ACH.toString())) {
					RegistrationUtility.updatePaymentsObject(billingAddressId, payments, paymentObject,
							null);
				} else if (paymentType.equals(PaymentRequestBean.Payment.PAYMENTTERMS.toString())) {
					RegistrationUtility.updatePaymentsObject(billingAddressId, payments, paymentObject,
							updatePaymentRequestBean);
				}
				paymentList.add(payments);
			}
		} else {
			String currentBillingAddressId;
			if (!paymentObject.has(MolekuleConstant.BILLING_ADDRESS_ID)
					&& paymentObject.get(MolekuleConstant.PAYMENT_TYPE).getAsString()
					.equals(PaymentRequestBean.Payment.ACH.toString())) {
				currentBillingAddressId = null;
			} else {
				currentBillingAddressId = paymentObject.get(MolekuleConstant.BILLING_ADDRESS_ID)
						.getAsString();
			}
			RegistrationUtility.updatePaymentsObject(currentBillingAddressId, payments, paymentObject,
					null);
			paymentList.add(payments);
		}
		return billingAddressId;
	}

	private AccountCustomObject getAcountCustomObject(String paymentId,
			UpdatePaymentRequestBean updatePaymentRequestBean, JsonObject resultsObject, JsonObject valueObject,
			List<Payments> paymentList, AddressV2 address) {
		AccountCustomObject accountObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		Value value = new Value();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setAddresses(address);
			value.setPayments(paymentList);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		if (updatePaymentRequestBean.isDefaultPayment()) {
			value.setDefaultPaymentId(paymentId);
		}
		accountObject.setValue(value);
		accountObject.setContainer(resultsObject.get(CONTAINER).getAsString());
		accountObject.setKey(resultsObject.get("key").getAsString());
		accountObject.setVersion(resultsObject.get(VERSION).getAsInt());
		return accountObject;
	}

	/**
	 * createShippingOrCarrierOptions method used to create new shipping options.
	 * 
	 * @param customerId
	 * @param shippingCarrierOptionsBean
	 * @return
	 */
	public ResponseEntity<ShippingAddressResponse> createShippingOptions(String customerId,
			ShippingCarrierOptionsBean shippingCarrierOptionsBean) {
		logger.trace("Start createShippingOrCarrierOptions Method");

		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);

		String key = registrationHelperService.getAccountKeyById(accountId);
		AccountCustomObject accountCustomObject = null;
		Object obj = registrationHelperService.getAccountCustomObject(key);
		if (!(obj instanceof AccountCustomObject)) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}
		accountCustomObject = (AccountCustomObject) obj;

		ShippingAddressResponse shippingAddressResponse = RegistrationUtility.populateShippingOptions("b2b", key,
				shippingCarrierOptionsBean, accountCustomObject);
		getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());

		logger.trace("End createShippingOptions Method");
		return new ResponseEntity<>(shippingAddressResponse, HttpStatus.OK);

	}

	/**
	 * updateShippingOptions method used to update the shipping information.
	 * 
	 * @param customerId
	 * @param shippingAddressId
	 * @param shippingCarrierOptionsBean
	 * @return
	 */
	public ResponseEntity<AccountCustomObject> updateShippingOptions(String customerId, String shippingAddressId,
			ShippingCarrierOptionsBean shippingCarrierOptionsBean) {
		logger.trace("Start Update Ship Method");

		if (null == shippingAddressId || null == customerId) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);

		String key = registrationHelperService.getAccountKeyById(accountId);

		AccountCustomObject accountCustomObject = null;

		Object obj = registrationHelperService.getAccountCustomObject(key);
		if (!(obj instanceof AccountCustomObject)) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}

		if (obj instanceof AccountCustomObject) {
			accountCustomObject = (AccountCustomObject) obj;
		}

		RegistrationUtility.updateShippingOptions(shippingAddressId, shippingCarrierOptionsBean, accountCustomObject);

		getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());
		// The below code is for process order after enter the freight information
		if (accountCustomObject != null && accountCustomObject.getValue() != null
				&& accountCustomObject.getValue().isFreightFlag()) {
			registrationHelperService.processOrder(customerId, FREIGHT_HOLD, accountId);
		}
		logger.trace("End updateShippingOrCarrierOptions Method");

		return new ResponseEntity<>(accountCustomObject, HttpStatus.OK);

	}






	/**
	 * createCarrierOptions method used to create new carrier options.
	 * 
	 * @param customerId
	 * @param carriers
	 * @return
	 */

	public ResponseEntity<AccountCustomObject> createCarrierOptions(String customerId, List<CarrierOption> carriers) {
		logger.trace("Start createShippingOrCarrierOptions Method");
		String token = ctServerHelperService.getStringAccessToken();
		AccountCustomObject accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,token);
		RegistrationUtility.populateCarrierOptions(accountCustomObject.getContainer(), accountCustomObject.getKey(), carriers, accountCustomObject);
		getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());

		logger.trace("End createCarrierOptions Method");
		return new ResponseEntity<>(accountCustomObject, HttpStatus.OK);

	}

	/**
	 * updateCarrierOptions method used to update the carrier options details.
	 * 
	 * @param customerId
	 * @param carrierId
	 * @param carriers
	 * @return
	 */

	public ResponseEntity<AccountCustomObject> updateCarrierOptions(String customerId, String carrierId,
			List<CarrierOption> carriers) {
		logger.trace("Start updateCarrierOptions Method");

		if (null == carrierId || null == customerId || carriers == null || carriers.size() != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);

		String key = registrationHelperService.getAccountKeyById(accountId);

		AccountCustomObject accountCustomObject = null;

		Object obj = registrationHelperService.getAccountCustomObject(key);
		if (!(obj instanceof AccountCustomObject)) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}

		accountCustomObject = (AccountCustomObject) obj;

		CarrierOption carrierOption = carriers.get(0);

		RegistrationUtility.updateCarrierOptions(carrierId, carrierOption, accountCustomObject);

		getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());

		logger.trace("End createShippingOrCarrierOptions Method");
		return new ResponseEntity<>(accountCustomObject, HttpStatus.OK);

	}

	/**
	 * deleteCarrierOptions used to delete the carrier options.
	 * 
	 * @param customerId
	 * @param carrierId
	 * @return
	 */

	public ResponseEntity<Object> deleteCarrierOptions(String customerId, String carrierId) {
		logger.trace("Start deleteCarrierOptions Method");

		if (null == carrierId || null == customerId) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);

		String key = registrationHelperService.getAccountKeyById(accountId);

		AccountCustomObject accountCustomObject = null;

		Object obj = registrationHelperService.getAccountCustomObject(key);
		if (!(obj instanceof AccountCustomObject)) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}
		accountCustomObject = (AccountCustomObject) obj;

		Value value = accountCustomObject.getValue();

		if (value.getCarriers() == null) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}

		CarrierOptionCustomObject carrier = value.getCarriers().stream().filter(s -> carrierId.equals(s.getCarrierId()))
				.findAny().orElse(null);
		if(carrier == null) {
			return new ResponseEntity<>(new ErrorResponse(400, "Resources does not have a valid CarrierId"),HttpStatus.BAD_REQUEST);
		}
		value.getCarriers().removeIf(s -> carrierId.equals(s.getCarrierId()));

		if (carrierId.equals(value.getDefaultCarrierId())) {
			value.setDefaultCarrierId(null);
		}

		getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());

		logger.trace("End createShippingOrCarrierOptions Method");
		return new ResponseEntity<>(accountCustomObject, HttpStatus.OK);

	}

	/**
	 * deleteShippingOptions used to delete the shipping addresses.
	 * 
	 * @param customerId
	 * @param shippingAddressId
	 * @return
	 */
	public ResponseEntity<Object> deleteShippingOptions(String customerId, String shippingAddressId) {
		logger.trace("Started deleteShippingOptions Method");

		if (null == shippingAddressId || null == customerId) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);

		String key = registrationHelperService.getAccountKeyById(accountId);

		AccountCustomObject accountCustomObject = null;

		Object obj = registrationHelperService.getAccountCustomObject(key);
		if (!(obj instanceof AccountCustomObject)) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}
		accountCustomObject = (AccountCustomObject) obj;

		Value value = accountCustomObject.getValue();

		if (value == null || value.getAddresses() == null || value.getAddresses().getShippingAddresses() == null) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}

		ShippingAddresses shippingAddresses = value.getAddresses().getShippingAddresses().stream().filter(s -> shippingAddressId.equals(s.getShippingAddressId()))
				.findAny().orElse(null);
		if(shippingAddresses == null) {
			return new ResponseEntity<>(new ErrorResponse(400, "Resources does not have an valid shippingAddressesId"),HttpStatus.BAD_REQUEST);
		}

		value.getAddresses().getShippingAddresses().removeIf(s -> shippingAddressId.equals(s.getShippingAddressId()));

		if (shippingAddressId.equals(value.getDefaultShippingAddressId())) {
			value.setDefaultShippingAddressId(null);
		}

		getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());

		logger.trace("End delete shipping Options Method");
		return new ResponseEntity<>(accountCustomObject, HttpStatus.OK);

	}

	/**
	 * 
	 * @param customerId
	 * @param invoiceEmail
	 * @return
	 */
	public ResponseEntity<String> updateInvoiceEmail(String customerId, String invoiceEmail) {

		if (null == customerId || null == invoiceEmail) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		List<String> emailList = Arrays.asList(invoiceEmail.split(","));
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_CUSTOMERS_SLASH + customerId;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String responseData = netConnectionHelper.sendGetWithoutBody(token, customerBaseUrl);
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(responseData);
		if (accountJsonObject.get(CUSTOM) != null && accountJsonObject.get(VERSION) != null) {
			CustomerDTO cognitoCustomerDTO = new CustomerDTO();
			List<CustomerDTO.Action> actions = new ArrayList<>();
			CustomerDTO.Action customerAction = new CustomerDTO.Action();
			customerAction.setActionName(SET_CUSTOM_FIELD);
			customerAction.setName("invoiceEmailAddress");
			customerAction.setValue(emailList);
			actions.add(customerAction);
			cognitoCustomerDTO.setVersion(accountJsonObject.get(VERSION).getAsInt());
			cognitoCustomerDTO.setActions(actions);
			sendPostRequest(customerBaseUrl, token, cognitoCustomerDTO);

		}

		return new ResponseEntity<>(invoiceEmail, HttpStatus.OK);
	}



	/**
	 * getAvalaraCertcaptureeCommerceToken method is used to get the access token
	 * from Avalara Cert Capture API.
	 * 
	 * @param customerId
	 * @return String - it contain token value.
	 */
	public String getAvalaraCertcaptureeCommerceToken(String customerId) {
		String baseUrl = ctEnvProperties.getAvalaraHost();
		String token = new StringBuilder().append("Basic ").append(ctEnvProperties.getAvalaraAuthValue()).toString();

		WebClient webClient = ctServerHelperService.fetchWebclient(baseUrl);
		Mono<String> accessToken = webClient.post().accept(MediaType.APPLICATION_JSON).header("Authorization", token)
				.header("x-client-id", ctEnvProperties.getAvalaraXclientId()).header("x-customer-number", customerId)
				.exchangeToMono(clientResponse -> {
					logger.trace("Access Token Api Status Code: {}", clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						throw new CustomRunTimeException("Unable to get Access Token From the Avalara Cert Capture ");
					}
					return clientResponse.bodyToMono(String.class);
				});
		return accessToken.block();
	}

	/**
	 * isAccountLocked is used to get whether an account is locked or not.
	 * 
	 * @param customerId
	 * @return Boolean
	 */
	public Boolean isAccountLocked(String customerId) {
		Boolean isAccountLocked = false;
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_CUSTOMERS_SLASH + customerId;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String responseData = netConnectionHelper.sendGetWithoutBody(token, customerBaseUrl);
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(responseData);
		if (accountJsonObject.get(CUSTOM) != null && accountJsonObject.get(VERSION) != null) {
			JsonObject customJson = accountJsonObject.get(CUSTOM).getAsJsonObject();
			JsonObject fieldsJson = customJson.get(FIELDS).getAsJsonObject();
			isAccountLocked = fieldsJson.get(ACCOUNT_LOCKED).getAsBoolean();
		}
		return isAccountLocked;
	}

	/**
	 * getPaymentOption method is used to get the payment details.
	 * 
	 * @param customerId
	 * @param paymentId
	 * @return ResponseEntity<Payments>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<Payments> getPaymentOption(String customerId, String paymentId) {
		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);
		String key = registrationHelperService.getAccountKeyById(accountId);
		AccountCustomObject accountCustomObject = null;
		Object obj = registrationHelperService.getAccountCustomObject(key);
		if (!(obj instanceof AccountCustomObject)) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}
		accountCustomObject = (AccountCustomObject) obj;

		Value value = accountCustomObject.getValue();
		List<Payments> paymentList = value.getPayments().stream().filter(action -> action.getPaymentId().equals(paymentId)).collect(Collectors.toList());
		if(paymentList.isEmpty()) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "This PaymentId is not assosiated with this customer");
			return new ResponseEntity(errorResponse,HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(paymentList.get(0), HttpStatus.OK);
	}

	public String signin(SignRequestBean signRequestBean) {
		Map<String, Object> responseMap = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString;
		String email = signRequestBean.getEmail();
		String clientId = ctEnvProperties.getCognitoClientId();

		Map<String, Object> initiateAuthRequest = authenticationHelperService.initiateUserSrpAuthRequest(email);
		try {
			jsonString = mapper.writeValueAsString(initiateAuthRequest);
			String initiateAuthResponse = registrationHelperService.cognitoApiCall(jsonString, ctEnvProperties.getCognitoHeaderAmzInitiateAuth());
			JsonObject initiateAuthJsonObject = MolekuleUtility.parseJsonObject(initiateAuthResponse);
			if(initiateAuthJsonObject.has(__TYPE)) {
				if(initiateAuthJsonObject.get("message").getAsString().contains("PreAuthentication failed with error Exceeded maximum login attempts.")) {
					setAccountLocked(email, true);
				}
				return initiateAuthResponse;
			}
			ResponseEntity<Map<String, Object>> srpAttributes = authenticationHelperService.performSRPAuthentication(email, signRequestBean.getPassword(), initiateAuthJsonObject);
			if(srpAttributes.getStatusCode().is2xxSuccessful()) {
				Map<String, Object> srpAttributesMap = srpAttributes.getBody();
				Map<String, Object> cognitoSiginChallangeRequest = new HashMap<>();
				cognitoSiginChallangeRequest.put(CLIENT_ID, clientId);
				cognitoSiginChallangeRequest.put("ChallengeName", "PASSWORD_VERIFIER");
				cognitoSiginChallangeRequest.put("ChallengeResponses", srpAttributesMap);
				jsonString = mapper.writeValueAsString(cognitoSiginChallangeRequest);
				String response = registrationHelperService.cognitoApiCall(jsonString, ctEnvProperties.getCognitoHeaderAmzRespondToAuthChallenge());
				JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
				if(jsonObject.has(__TYPE)) {
					return response;
				}
				responseMap.put("tokens", jsonObject.toString());
				String accessToken = jsonObject.get("AuthenticationResult").getAsJsonObject().get("AccessToken").getAsString();
				ResponseEntity<String> userAttributesPesponse = getCognitoUserData(accessToken);
				responseMap.put(USER_ATTRIBUTES, userAttributesPesponse.getBody());
				return MolekuleUtility.parseJsonObject(responseMap.toString()).toString();
			} else {
				Map<String, Object> errorMessage = srpAttributes.getBody();
				if(null != errorMessage && errorMessage.get("error").toString().contains("PreAuthentication failed with error Exceeded maximum login attempts.")) {
					setAccountLocked(email, true);
				}
				String response = null;
				if(errorMessage != null) {
					response = MolekuleUtility.parseJsonObject(errorMessage.toString()).toString();
				}
				return response;
			}
		} catch (final Exception ex) {
			logger.error("Exception occured during sign in:", ex);
			String message = new Gson().toJson(ex.getMessage());
			Map<String, Object> error = new HashMap<>();
			error.put("error", message);
			return MolekuleUtility.parseJsonObject(error.toString()).toString();
		}
	}

	private void setAccountLocked(String email, boolean locked) {
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/customers/";
		String customerEmailUrl = customerBaseUrl + "?where=" + MolekuleUtility.convertToEncodeUri("email=\"" + email + "\"");
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String customerResponseData = null;
		try {
			customerResponseData = netConnectionHelper.httpConnectionHelper(customerEmailUrl, token);
		} catch (IOException e1) {
			logger.error("Exception occured when getting customer Data", e1);
		}
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponseData);
		JsonArray resultsArray = customerJsonObject.get(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		String customerUrl = customerBaseUrl + resultsObject.get(ID).getAsString();
		CustomerAction customerAction = new CustomerAction();
		List<CustomerAction.Actions> actions = new ArrayList<>();
		CustomerAction.Actions action = new CustomerAction.Actions();
		action.setAction(SET_CUSTOM_FIELD);
		action.setName(ACCOUNT_LOCKED);
		action.setValue(locked);
		actions.add(action);
		customerAction.setVersion(resultsObject.get(VERSION).getAsLong());
		customerAction.setActions(actions);
		netConnectionHelper.sendPostRequest(customerUrl, token, customerAction);
	}

	public ResponseEntity<String> getCognitoUserData(String accessToken) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> params = new HashMap<>();
		params.put(ACCESS_TOKEN, accessToken);
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			String response = registrationHelperService.cognitoApiCall(jsonString,
					ctEnvProperties.getCognitoHeaderAmzGetUserTarget());
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
			if(jsonObject.has(__TYPE)) {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			logger.error("Exception occured while getting cognito data: ", e);
			message = new Gson().toJson(e.getMessage());
		}
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<String> refreshTokens(RefreshToken refreshToken) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> params = new HashMap<>();
		Map<String, String> authParams = new HashMap<>();
		authParams.put(REFRESH_TOKEN_UPPERCASE, refreshToken.getRefreshToken());
		params.put("AuthFlow", "REFRESH_TOKEN_AUTH");
		params.put(CLIENT_ID, ctEnvProperties.getCognitoClientId());
		params.put("AuthParameters", authParams);
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			String response = registrationHelperService.cognitoApiCall(jsonString,
					ctEnvProperties.getCognitoHeaderAmzInitiateAuth());
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
			if(jsonObject.has(__TYPE)) {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			logger.error("Exception occured in refresh tokens: ", e);
			message = new Gson().toJson(e.getMessage());
		}
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<String> forgotPassword(ForgotPasswordRequestBean forgotPasswordRequestBean) {
		return registrationHelperService.forgotPassword(forgotPasswordRequestBean,forgotPasswordRequestBean.getPasswordType());
	}

	public ResponseEntity<String> resetPassword(ResetPasswordRequestBean resetPasswordRequestBean) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> params = new HashMap<>();
		String email = resetPasswordRequestBean.getEmailId();
		params.put(CLIENT_ID, ctEnvProperties.getCognitoClientId());
		params.put("Username", email);
		params.put("ConfirmationCode", resetPasswordRequestBean.getConfirmationCode());
		params.put("Password", resetPasswordRequestBean.getPassword());
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			String response = registrationHelperService.cognitoApiCall(jsonString,
					ctEnvProperties.getCognitoHeaderAmzConfirmForgotPasswordTarget());
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
			if(jsonObject.has(__TYPE)) {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			setAccountLocked(email, false);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			logger.error("Exception occured in reset password: ", e);
			message = new Gson().toJson(e.getMessage());
		}
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<String> changePassword(ChangePasswordRequestBean changePasswordRequestBean) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> params = new HashMap<>();
		Map<String,String> userAttributeMap = new HashMap<>();
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		params.put(ACCESS_TOKEN, changePasswordRequestBean.getAccessToken());
		params.put("PreviousPassword", changePasswordRequestBean.getPreviousPassword());
		params.put("ProposedPassword", changePasswordRequestBean.getProposedPassword());
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			String response = registrationHelperService.cognitoApiCall(jsonString,
					ctEnvProperties.getCognitoHeaderAmzChangePasswordTarget());
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
			if(jsonObject.has(__TYPE)) {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			ResponseEntity<String> userAttributesPesponse = getCognitoUserData(changePasswordRequestBean.getAccessToken());
			userAttributeMap.put(USER_ATTRIBUTES, userAttributesPesponse.getBody());
			JsonObject userAttributeObj = MolekuleUtility.parseJsonObject(userAttributeMap.toString());
			JsonArray userAttributeArray = userAttributeObj.get(USER_ATTRIBUTES).getAsJsonObject().get("UserAttributes").getAsJsonArray();
			String customerId = null;
			for(int i=0;i<userAttributeArray.size();i++) {
				if(userAttributeArray.get(i).getAsJsonObject().get("Name").getAsString().equalsIgnoreCase("custom:customerId")) {
					customerId = userAttributeArray.get(i).getAsJsonObject().get("Value").getAsString();
				}
			}
			SendGridModel sendgridModel = new SendGridModel();
			if(customerId != null) {
				String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ SLASH_CUSTOMERS_SLASH + customerId;
				String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
				JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
				sendgridModel.setEmail(customerObject.get("email").getAsString());
				sendgridModel.setFirstName(customerObject.get("firstName").getAsString());
				sendgridModel.setCustomerPassword(changePasswordRequestBean.getProposedPassword());
			}
			customerPasswordUpdateMailSender.sendMail(sendgridModel);

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			logger.error("Exception occured : ", e);
			message = new Gson().toJson(e.getMessage());
		}
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<String> logout(String accessToken) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> params = new HashMap<>();
		params.put(ACCESS_TOKEN, accessToken);
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			String response = registrationHelperService.cognitoApiCall(jsonString,
					ctEnvProperties.getCognitoHeaderAmzGlobalSignOut());
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
			if(jsonObject.has(__TYPE)) {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			logger.error("Exception occured : ", e);
			message = new Gson().toJson(e.getMessage());
		}
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}

}
