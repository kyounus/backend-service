package com.molekule.api.v1.useraccountservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMAIL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMPTY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import lombok.Data;
@Data
public class TealiumSendgridThread implements Runnable {

	Logger logger = LoggerFactory.getLogger(TealiumSendgridThread.class);
	private TemplateMailRequestHelper paymentTermsRequestConfirmationSender;
	private TemplateMailRequestHelper paymentTermsPendingApprovalSender;
	private NetConnectionHelper netConnectionHelper;
	private CtServerHelperService ctServerHelperService;
	private RegistrationHelperService registrationHelperService;
	private TealiumHelperService tealiumHelperService;
	private PaymentRequestBean paymentRequestBean;
	Map<String, String> requestData;

	@Override
	public void run() {
		logger.info("SendGrid and tealium implementation starts");
		SendGridModel sendGridModel = createEmailDynamicInfo(requestData.get("customerId"));
		if (paymentRequestBean.getPaymentPreferences().equals(PaymentRequestBean.Payment.PAYMENTTERMS.name())) {
				paymentTermsRequestConfirmationSender.sendMail(sendGridModel);
				paymentTermsPendingApprovalSender.sendMail(sendGridModel);
		}
		tealiumHelperService.setAccountInfo(requestData.get("response"));
		logger.info("SendGrid and tealium implementation ends");
	}

	public SendGridModel createEmailDynamicInfo(String customerId) {
		SendGridModel sendGridModel = new SendGridModel();
		CustomerResponseBean customer = registrationHelperService.getCustomerById(customerId);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		sendGridModel.setCustomerId(customerId);
		sendGridModel.setRequestedDate(formatter.format(date));
		sendGridModel.setEmail(customer.getEmail());
		sendGridModel.setFirstName(customer.getFirstName());
		sendGridModel.setLastName(customer.getLastName());
		sendGridModel.setCustomerCompanyName(customer.getCompanyName());
		sendGridModel.setPhoneNumber(customer.getCustom().getFields().getPhone());
		if(customer.getCustom().getFields().getChannel().equals("B2B")) {
			String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);
			sendGridModel.setAccountId(accountId);
			String accountStringResponse = netConnectionHelper.sendGetWithoutBody(
					ctServerHelperService.getStringAccessToken(),
					registrationHelperService.getCTCustomObjectsByAccountIdUrl(accountId));
			JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
			if (accountJsonObject.get(STATUS_CODE) != null) {
				return null;
			}
			processResultsValue(sendGridModel, accountJsonObject);
		}
		return sendGridModel;
	}

	private void processResultsValue(SendGridModel sendGridModel, JsonObject accountJsonObject) {
		if (accountJsonObject.getAsJsonArray(RESULTS) != null) {
			JsonArray resultsArray = accountJsonObject.get(RESULTS).getAsJsonArray();
			JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
			if (resultsObject.getAsJsonObject(VALUE) != null) {
				JsonObject valueJsonObject = resultsObject.get(VALUE).getAsJsonObject();
				if (valueJsonObject.getAsJsonObject("companyCategory") != null) {
					JsonObject companyCategory = valueJsonObject.get("companyCategory").getAsJsonObject();
					sendGridModel.setCompanyIndustry(companyCategory.get("name").getAsString());
					if (valueJsonObject.getAsJsonObject("salesRepresentative") != null) {
						setSalesRepDetails(sendGridModel, valueJsonObject);
					}
				}
			}
		}
	}

	private void setSalesRepDetails(SendGridModel sendGridModel, JsonObject valueJsonObject) {
		JsonObject salesRep = valueJsonObject.get("salesRepresentative").getAsJsonObject();
		if (salesRep.get("name").getAsString().trim().split(" ").length > 1) {
			sendGridModel.setSalesRepFirstName(salesRep.get("name").getAsString().trim().split(" ")[0]);
			sendGridModel.setSalesRepLastName(salesRep.get("name").getAsString().trim().split(" ")[1]);
		} else {
			sendGridModel.setSalesRepFirstName(salesRep.get("name").getAsString().trim().split(" ")[0]);
			sendGridModel.setSalesRepLastName(EMPTY);
		}
		sendGridModel.setSalesRepPhoneNumber(salesRep.get(PHONE).getAsString());
		sendGridModel.setSalesRepEmail(salesRep.get(EMAIL).getAsString());
		sendGridModel.setSalesRepCalendyLink("Calendy Link");
	}

}
