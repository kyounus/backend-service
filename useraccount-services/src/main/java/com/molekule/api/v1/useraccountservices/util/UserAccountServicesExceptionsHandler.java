package com.molekule.api.v1.useraccountservices.util;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.molekule.api.v1.commonframework.util.CustomExceptionsHandler;

@ControllerAdvice
public class UserAccountServicesExceptionsHandler extends CustomExceptionsHandler {

}
