package com.molekule.api.v1.useraccountservices.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The class Cache is used to store the company-info, shipping-choice and payment-option.
 */
public class Cache {
	
	private  Map<String,String> keyCache = null;
	private static Cache instance = null;
	
	private Cache() {
		keyCache = new ConcurrentHashMap<>();
	}
	
	public static Cache getInstance() {
		
			if(instance == null) {
				instance = new Cache();
			}
		
		return instance;
	}
	
	public void addCache(String key,String value) {
		keyCache.put(key, value);
	}
	
	public String get(String key) {
		return keyCache.get(key);
	}
	
	public boolean containsKey(String key) {
		return keyCache.containsKey(key);
	}

}
