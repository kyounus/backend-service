package com.molekule.api.v1.useraccountservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.NonProfitDocumentProofDto;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.model.registration.ChangePasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoBean;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoResponseBean;
import com.molekule.api.v1.commonframework.model.registration.ForgotPasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.RefreshToken;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationResponseBean;
import com.molekule.api.v1.commonframework.model.registration.ResetPasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressResponse;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressResponseBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.SignRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UserAttributesRequestBean;
import com.molekule.api.v1.useraccountservices.service.AmazonS3Service;
import com.molekule.api.v1.useraccountservices.service.RegistrationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The class RegistrationController is used to write registration related apis.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/user")
public class RegistrationController {

	Logger logger = LoggerFactory.getLogger(RegistrationController.class);
	@Autowired
	@Qualifier("registrationService")
	RegistrationService registrationService;

	@Autowired
	@Qualifier("amazonS3Service")
	private AmazonS3Service amazonS3Service;


	/**
	 * getRegistrationData method is used to post the quick registration details.
	 * 
	 * @param registrationBean
	 * @return String - it contains customerSignUpResponse data.
	 */
	@PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "signup", response = RegistrationResponseBean.class)
	public String getRegistrationData(
			@ApiParam(value = "customer signup", required = true) @RequestBody RegistrationBean registrationBean) {
		logger.trace("accessing on getRegistrationData method");
		CustomerDTO customerDTO = new CustomerDTO();
		if(StringUtils.hasText(registrationBean.getFirstName())) {
			customerDTO.setFirstName(registrationBean.getFirstName().toUpperCase());
		}
		if(StringUtils.hasText(registrationBean.getLastName())) {
			customerDTO.setLastName(registrationBean.getLastName().toUpperCase());
		}
		if(StringUtils.hasText(registrationBean.getEmail())) {
			customerDTO.setEmail(registrationBean.getEmail().toLowerCase());
		}
		customerDTO.setCompanyName(registrationBean.getCompanyName());
		customerDTO.setCompanyCategoryId(registrationBean.getCompanyCategoryId());
		customerDTO.setPhone(registrationBean.getMobileNumber());
		customerDTO.setPassword(registrationBean.getPassword());
		return registrationService.createCustomer(customerDTO, registrationBean);
	}
	
	/**
	 * signin method is used to login.
	 * 
	 * @param registrationBean
	 * @return String - it contains customerSignUpResponse data.
	 */
	@PostMapping(value = "/signin/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "signin")
	public String signin(
			@ApiParam(value = "Signin Details", required = true) @RequestBody SignRequestBean signRequestBean) {
		return registrationService.signin(signRequestBean);
	}
	
	/**
	 * refreshTokens method is used to get new tokens.
	 * 
	 * @param refreshToken
	 * @return String - it contains customerSignUpResponse data.
	 */
	@PostMapping(value = "/refresh/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Refresh")
	public ResponseEntity<String> refreshTokens(
			@ApiParam(value = "Refresh Token", required = true) @RequestBody RefreshToken refreshToken) {
		return registrationService.refreshTokens(refreshToken);
	}
	
	/**
	 * forgotPassword method is used to send password reset link to the user.
	 * 
	 * @param emailId
	 * @return String
	 */
	@PostMapping(value = "/forgot-password/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Forgot Password")
	public ResponseEntity<String> forgotPassword(
			@ApiParam(value = "ForgotPasswordRequestBean", required = true) @RequestBody ForgotPasswordRequestBean forgotPasswordRequestBean) {
		return registrationService.forgotPassword(forgotPasswordRequestBean);
	}
	
	/**
	 * resetPassword method is used to reset password from reset link of the user.
	 * 
	 * @param emailId
	 * @return String
	 */
	@PostMapping(value = "/reset-password/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reset Password")
	public ResponseEntity<String> resetPassword(
			@ApiParam(value = "RestPasswordRequestBean", required = true) @RequestBody ResetPasswordRequestBean resetPasswordRequestBean) {
		return registrationService.resetPassword(resetPasswordRequestBean);
	}
	
	/**
	 * changePassword method is used to reset password from reset link of the user.
	 * 
	 * @param emailId
	 * @return String
	 */
	@PostMapping(value = "/change-password/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Change Password")
	public ResponseEntity<String> changePassword(
			@ApiParam(value = "ChangePasswordRequestBean", required = true) @RequestBody ChangePasswordRequestBean changePasswordRequestBean) {
		return registrationService.changePassword(changePasswordRequestBean);
	}
	
	/**
	 * getUserAttributes method is used to reset password from reset link of the user.
	 * 
	 * @param accessToken
	 * @return String
	 */
	@PostMapping(value = "/user-attributes/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get User Attributes")
	public ResponseEntity<String> getUserAttributes(
			@ApiParam(value = "UserAttributesRequestBean", required = true) @RequestBody UserAttributesRequestBean userAttributesRequestBean) {
		return registrationService.getCognitoUserData(userAttributesRequestBean.getAccessToken());
	}
	
	/**
	 * logout method is used to reset password from reset link of the user.
	 * 
	 * @param accessToken
	 * @return String
	 */
	@PostMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Logout")
	public ResponseEntity<String> logout(
			@ApiParam(value = "UserAttributesRequestBean", required = true) @RequestBody UserAttributesRequestBean userAttributesRequestBean) {
		return registrationService.logout(userAttributesRequestBean.getAccessToken());
	}

	/**
	 * getCompanyInfo method is used to register the company information.
	 * 
	 * @param companyInfoBean
	 * @return String - it contains company-info response.
	 */
	@PostMapping(value = "/signup/company-info", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "company information", response = CompanyInfoResponseBean.class)
	public String getCompanyInfo(
			@ApiParam(value = "send company information along with accountId, customerId, customerNumber", required = true) @RequestBody CompanyInfoBean companyInfoBean) {
		return registrationService.updateCompanyInfo(companyInfoBean);
	}

	/**
	 * saveShippingAddress method is used to register the shipping choice information.
	 * 
	 * @param shippingAddressRequestBean
	 * @return String - it contains shipping-choice response.
	 */
	@PostMapping(value = "/signup/shipping-choice", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "customer shipping choice", response = ShippingAddressResponseBean.class)
	public String saveShippingAddress(
			@ApiParam(value = "send customer shipping choice along with accountId, customerId, customerNumber", required = true) @RequestBody ShippingAddressRequestBean shippingAddressRequestBean) {
		logger.trace("execuing shipping address...");
		return registrationService.addShippingAddress(shippingAddressRequestBean);
	}

	/**
	 * uploadFile method is used to upload the non profit document to s3 bucket.
	 * 
	 * @param fileList
	 * @param customerId
	 * @return NonProfitDocumentProofDto -  it contain filename and location.
	 */
	@PostMapping(value = "/files/{customerId}",consumes  = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiOperation(value = "Upload Non-profit Documents")
	public NonProfitDocumentProofDto uploadFile(@RequestPart("file") MultipartFile file,
			@ApiParam(value = "The customer Id", required = true) @PathVariable(CUSTOMER_ID) String  customerId){
		return amazonS3Service.uploadFileToS3Bucket(file, true,customerId);
	}
	/**
	 * deleteFile  method is used to delete the  non profit doc in S3 bucket.
	 * 
	 * @param fileName
	 * @return String - name of the file delete in S3.
	 */
	@DeleteMapping(value = "/files/{fileName}")
	@ApiOperation(value = "Delete Non-profit Document")
	public ResponseEntity<Object> deleteFile(@ApiParam(value = "The file name", required = true) @PathVariable("fileName") String fileName){
		return amazonS3Service.deleteFileFromS3Bucket(fileName);
	}

	/**
	 * getUploadedFilesByCustomerId method is used to list the file names which are uploaded on s3 bucket based on customer id.
	 * 
	 * @param customerId
	 * @return List<NonProfitDocumentProofDto> - return the file name  and location.
	 */
	@GetMapping(path = "/files/{customerId}")
	@ApiOperation(value = "Get List of Non-profit document by Customer Id")
	public List<NonProfitDocumentProofDto> getUploadedFilesByCustomerId(@ApiParam(value = "The customer Id", required = true) @PathVariable(CUSTOMER_ID) String  customerId){
		return amazonS3Service.getUploadedFilesByCustomerId(customerId);
	}

	/**
	 * createPaymentOption method is used to register the payment options.
	 * 
	 * @param PaymentRequestBean
	 * @return String - it contains AccountCustomObject as response.
	 */
	@PostMapping(value = "/{customerId}/payments", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "create payment option", response = AccountCustomObject.class)
	public AccountCustomObject createPaymentOption(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String  customerId,
			@ApiParam(value = "Request payment option", required = true) @RequestBody PaymentRequestBean paymentRequestBean) {
		logger.trace("execuing paymnet option...");
		return registrationService.createPaymentOption(customerId, paymentRequestBean);
	}

	/**
	 * downloadFile method is used to download the file from S3 bucket.
	 * @param fileName
	 * @return ResponseEntity<ByteArrayResource>
	 * @throws IOException
	 */
	@GetMapping("/files/download/{fileName}")
	@ApiOperation(value = "Download Non-Profit Document")
	public ResponseEntity<ByteArrayResource> downloadFile(@ApiParam(value = "The file name", required = true) @PathVariable("fileName") String fileName)  {
		final byte[] data = amazonS3Service.downloadFile(fileName);
		final ByteArrayResource resource = new ByteArrayResource(data);
		return ResponseEntity
				.ok()
				.contentLength(data.length)
				.header("Content-type", "application/octet-stream")
				.header("Content-disposition", "attachment; filename=\"" + fileName + "\"")
				.body(resource);
	}

	/**
	 * updatePaymentOption method is used to update the payment options.
	 * 
	 * @param paymentOptionRequestBean
	 * @return String - it contains AccountCustomObject as response.
	 */
	@PostMapping(value = "/{customerId}/payment-options/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "update payment option", response = AccountCustomObject.class)
	public String updatePaymentOption(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String  customerId,
			@ApiParam(value = "payment id", required = true) @PathVariable("paymentId") String  paymentId,
			@ApiParam(value = "Request payment option", required = true) @RequestBody UpdatePaymentRequestBean updatePaymentRequestBean) {
		logger.trace("execuing paymnet option...");
		return registrationService.updatePaymentOption(customerId, paymentId, updatePaymentRequestBean, null);
	}
	
	/**
	 * getPaymentOption method is used to get the payment details.
	 * 
	 * @param customerId
	 * @param paymentId
	 * @return Payments
	 */
	@GetMapping(value = "/{customerId}/payment-options/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get payment option", response = AccountCustomObject.class)
	public ResponseEntity<Payments> getPaymentOption(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String  customerId,
			@ApiParam(value = "payment id", required = true) @PathVariable("paymentId") String  paymentId) {
		logger.trace("execuing get paymnet option...");
		return registrationService.getPaymentOption(customerId, paymentId);
	}

	/**
	 * createShippingOptions method used to create the shipping options.
	 * @param customerId
	 * @param shippingCarrierOptionsBean
	 * @return
	 */

	@PostMapping(value = "/{customerId}/shipping-options", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "create Shipping Options", response = AccountCustomObject.class)
	public ResponseEntity<ShippingAddressResponse> createShippingOptions(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "Request Shipping options", required = true) @RequestBody ShippingCarrierOptionsBean shippingCarrierOptionsBean) {
		logger.trace("executing createShippingOrCarrierOptions");
		return registrationService.createShippingOptions(customerId, shippingCarrierOptionsBean);
	}

	/**
	 * updateShippingOptions method used to update the shipping options.
	 * 
	 * @param customerId
	 * @param shippingAddressId
	 * @param shippingCarrierOptionsBean
	 * @return
	 */

	@PostMapping(value = "/{customerId}/shipping-options/{shippingAddressId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update Shipping Options", response = AccountCustomObject.class)
	public ResponseEntity<AccountCustomObject> updateShippingOptions(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "Shipping Address Id", required = true) @PathVariable("shippingAddressId") String shippingAddressId,
			@ApiParam(value = "Update Shipping options", required = true) @RequestBody ShippingCarrierOptionsBean shippingCarrierOptionsBean) {

		logger.trace("executing updateShippingOptions");

		return registrationService.updateShippingOptions(customerId, shippingAddressId, shippingCarrierOptionsBean);
	}


	/**
	 * createCarrierOptions method is used to create the carrier information.
	 * @param customerId
	 * @param shippingCarrierOptionsBean
	 * @return
	 */

	@PostMapping(value = "/{customerId}/carriers", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "create Carrier Options", response = AccountCustomObject.class)
	public ResponseEntity<AccountCustomObject> createCarrierOptions(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "Request Shipping or Carrier options", required = true) @RequestBody ShippingCarrierOptionsBean shippingCarrierOptionsBean) {
		logger.trace("executing createShippingOrCarrierOptions");
		if (null == shippingCarrierOptionsBean.getCarriers()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return registrationService.createCarrierOptions(customerId, shippingCarrierOptionsBean.getCarriers());
	}

	/**
	 * updateCarrierOptions method is used to update the carrier information.
	 * @param customerId
	 * @param carrierId
	 * @param shippingCarrierOptionsBean
	 * @return
	 */

	@PostMapping(value = "/{customerId}/carriers/{carrierId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update Carrier Options", response = AccountCustomObject.class)
	public ResponseEntity<AccountCustomObject> updateCarrierOptions(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "carrier id", required = true) @PathVariable("carrierId") String carrierId,
			@ApiParam(value = "Update Carrier Options", required = true) @RequestBody ShippingCarrierOptionsBean shippingCarrierOptionsBean) {

		logger.trace("executing updateCarrierOptions");

		return registrationService.updateCarrierOptions(customerId, carrierId, shippingCarrierOptionsBean.getCarriers());
	}

	/**
	 * deleteCarrier method used to remove the carrier details from the account.
	 * @param customerId
	 * @param carrierId
	 * @return
	 */
	@DeleteMapping(value = "/{customerId}/carriers/{carrierId}")
	@ApiOperation(value = "Delete Carrier")
	public ResponseEntity<Object> deleteCarrier(
			@ApiParam(value = "customer Id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "carrier id", required = true) @PathVariable("carrierId") String carrierId) {

		logger.trace("Deleting Carrier Method Started");

		return registrationService.deleteCarrierOptions(customerId, carrierId);
	}


	/**
	 * deleteShippingOptions method used to remove the shipping address details from the account.
	 * @param customerId
	 * @param shippingAddressId
	 * @return
	 */
	@DeleteMapping(value = "/{customerId}/shipping-options/{shippingAddressId}")
	@ApiOperation(value = "Delete shipping options")
	public ResponseEntity<Object> deleteShippingOptions(
			@ApiParam(value = "Customer Id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "Shipping Address Id", required = true) @PathVariable("shippingAddressId") String shippingAddressId) {
		logger.trace("Deleting Shipping Options Started");
		return registrationService.deleteShippingOptions(customerId, shippingAddressId);
	}

	/**
	 * updateInvoiceEmail method used to update invoice email in your profile page.
	 * @param customerId
	 * @param invoiceEmail
	 * @return
	 */
	@PostMapping(value = "/{customerId}/invoice-email")
	@ApiOperation(value = "Update Invoice Email")
	public ResponseEntity<String> updateInvoiceEmail(
			@ApiParam(value = "Customer Id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "Invoice Email", required = true) @RequestParam String invoiceEmail) {
		logger.trace("Deleting Shipping Options Started");
		return registrationService.updateInvoiceEmail(customerId, invoiceEmail);
	}
	
	/**
	 * getAvalaraCertcaptureeCommerceToken method is used to get the access token from Avalara Cert Capture API.
	 * @param customerId
	 * @return String - it contain token value.
	 */
	@PostMapping(value = "/{customerId}/tax-certificate")
	@ApiOperation(value = "Get Avalara Certcapturee Commerce Token")
	public String getAvalaraCertcaptureeCommerceToken(
			@ApiParam(value = "Customer Id", required = true) @PathVariable(CUSTOMER_ID) String customerId) {
		logger.trace("Get Avalara Certcapturee Commerce Token method Started");
		return registrationService.getAvalaraCertcaptureeCommerceToken(customerId);
	}
}
