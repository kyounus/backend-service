package com.molekule.api.v1.useraccountservices.util;

public enum BulkUpdateActionEnum {
	UPDATE_NEXT_ORDER_DATE,
	TURN_ON, 
	TURN_OFF
}
