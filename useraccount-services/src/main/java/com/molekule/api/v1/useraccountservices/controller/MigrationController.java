package com.molekule.api.v1.useraccountservices.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationResponseBean;
import com.molekule.api.v1.useraccountservices.service.RegistrationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The class MigrationController is used to write migrated related apis.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/migration/user")
public class MigrationController {

	Logger logger = LoggerFactory.getLogger(MigrationController.class);
	@Autowired
	@Qualifier("registrationService")
	RegistrationService registrationService;
	
	@Autowired
	CTEnvProperties ctEnvProperties;

	/**
	 * getRegistrationData method is used to post the quick registration details.
	 * 
	 * @param registrationBean
	 * @return String - it contains customerSignUpResponse data.
	 */
	@PostMapping(value = "/signup/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "signup", response = RegistrationResponseBean.class)
	public String getRegistrationData(
			@ApiParam(value = "customer signup", required = true) @RequestBody RegistrationBean registrationBean,
			@ApiParam(value = "customer key", required = true) @RequestParam String customerKey) {
		logger.trace("accessing on getRegistrationData method");
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setFirstName(registrationBean.getFirstName());
		customerDTO.setLastName(registrationBean.getLastName());
		customerDTO.setEmail(registrationBean.getEmail());
		customerDTO.setCompanyName(registrationBean.getCompanyName());
		customerDTO.setCompanyCategoryId(registrationBean.getCompanyCategoryId());
		customerDTO.setPhone(registrationBean.getMobileNumber());
		customerDTO.setPassword(ctEnvProperties.getPw0rd());
		customerDTO.setKey(customerKey);			
		return registrationService.createCustomer(customerDTO, registrationBean);
	}

}
