package com.molekule.api.v1.useraccountservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.dto.registration.CustomerResponse;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.model.aurora.AuroraCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.AuroraUpdateCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.CategoryObjectBean;
import com.molekule.api.v1.commonframework.model.aurora.CustomerSortActionEnum;
import com.molekule.api.v1.commonframework.model.aurora.GetCustomerBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepresentativesBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepsResponse;
import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.model.registration.AccountCustomResponseBean;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReviewsRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.useraccountservices.service.AuroraService;
import com.molekule.api.v1.useraccountservices.service.RegistrationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * AuroraApiController is used to get the customer and account list.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1")
public class UserAccountController {

	@Autowired
	@Qualifier("auroraService")
	AuroraService auroraService;

	@Autowired
	//	@Qualifier("csrHelperService")
	CsrHelperService csrHelperService;

	@Autowired
	RegistrationService registrationService;

	@Autowired
	@Qualifier("auroraHelperService")
	AuroraHelperService auroraHelperService;

	/**
	 * getListOfCustomer method is used to get list of customers.
	 * 
	 * @param limit
	 * @param offset
	 * @param email 
	 * @param companyName 
	 * @return String- it contain all customer related values.
	 */
	@GetMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get list of all customer information")
	public String getListOfCustomers(
			@ApiParam(value = "The limit is",example = "20", required = true) @RequestParam int limit,
			@ApiParam(value = "The offset is",example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "Comapny name", required = false) @RequestParam(defaultValue = "" ) String companyName,
			@ApiParam(value = "Customer email", required = false) @RequestParam(defaultValue = "" ) String email,
			@ApiParam(value = "The First Name  is", required = false) @RequestParam(required = false) String firstName,
			@ApiParam(value = "The Last Name  is", required = false) @RequestParam(required = false) String lastName,
			@ApiParam(value = "The Customer Channel  is", required = false) @RequestParam(required = false) String customerChannel,
			@ApiParam(value= "Sort Action", required = false) @RequestParam(defaultValue = "") CustomerSortActionEnum sortAction,
			@ApiParam(value = "Sort Order", required = false) @RequestParam(defaultValue = "") SortOrderEnum sortOrder) {
		GetCustomerBean getCustomerBean = new GetCustomerBean();
		getCustomerBean.setLimit(limit);
		getCustomerBean.setOffset(offset);
		getCustomerBean.setCompanyName(companyName);
		getCustomerBean.setEmail(email);
		getCustomerBean.setFirstName(firstName);
		getCustomerBean.setLastName(lastName);
		getCustomerBean.setCustomerChannel(customerChannel);
		getCustomerBean.setSortAction(sortAction);
		getCustomerBean.setSortOrder(sortOrder);
		return auroraService.getListOfCustomer(getCustomerBean);
	}

	/**
	 * getListOfAccounts method is used to get the list of accounts.
	 * 
	 * @param limit
	 * @param offset
	 * @param companyName
	 * @param termStatus
	 * @return
	 */
	@GetMapping(value = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get list of all accounts information")
	public String getListOfAccounts(
			@ApiParam(value = "The limit is",example = "20", required = true) @RequestParam int limit,
			@ApiParam(value = "The offset is",example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "Comapny name", required = false) @RequestParam(defaultValue = "" ) String companyName,
			@ApiParam(value = "Term status ", required = false) @RequestParam(defaultValue = "" ) String termStatus) {
		return auroraService.getListOfAccounts(limit,offset,companyName,termStatus);
	}

	/**
	 * getCompanyCategories method is used to fetch list of Company Categories.
	 * 
	 * @return String - it contains Company Categories
	 */
	@GetMapping(value = "/accounts/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get List of Company Categories")
	public String getCompanyCategories() {
		return auroraHelperService.getCompanyCategories();
	}

	/**
	 * getCustomers method is used to get the customers information by passing accountId.
	 * 
	 * @param accountId
	 * @return List<CustomerResponse>
	 */
	@GetMapping(value = "/accounts/{accountId}/users/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get customers by account and customer id")
	public List<CustomerResponse> getCustomers(
			@ApiParam(value = "The account id", required = true) @PathVariable("accountId") String accountId,
			@ApiParam(value = "The customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId) {
		return auroraService.getCustomersByAccountId(customerId);
	}

	/**
	 * listCustomersByAccount method is used to get the customers information by passing accountId.
	 * 
	 * @param accountId
	 * @return List<CustomerResponse>
	 */
	@GetMapping(value = "/accounts/{accountId}/users", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get list of customers by account")
	public List<CustomerResponseBean> listCustomersByAccount(
			@ApiParam(value = "The account id", required = true) @PathVariable("accountId") String accountId) {
		return auroraService.getAllCustomersByAccountId(accountId);
	}

	/**
	 * getSalesRep method is used to get the list of sales rep.
	 * 
	 * @return SalesRepresentativesBean - it contains list of sales rep.
	 */
	@GetMapping(value = "/accounts/salesrep", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get the list of sales rep")
	public SalesRepresentativesBean getSalesRep(
			@ApiParam(value = "The limit is", required = false) @RequestParam(defaultValue= "") Integer limit,
			@ApiParam(value = "The offset is", required = false) @RequestParam(defaultValue = "" ) Integer offset) {
		return auroraService.getAllSalesReps(limit,offset);
	}

	/**
	 * getListCustomerGroups method is used to fetch list of Customer Groups.
	 * 
	 * @return String - it contains Customer Groups
	 */
	@GetMapping(value = "/customer/group", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get List of Customer Groups")
	public String getListCustomerGroups() {
		return auroraService.getListCustomerGroups();
	}

	/**
	 * createAccount method is used to create a custom object from merchant center.
	 * 
	 * @param accountCustomResponseBean
	 * 
	 * @return String - it contains custom-object detail.
	 */
	@PostMapping(value = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Account", response = AccountCustomResponseBean.class)
	public String createAccount(
			@ApiParam(value = "Company Information", required = true) @RequestBody AccountCustomResponseBean accountCustomResponseBean) {
		return auroraService.createAccount(accountCustomResponseBean);
	}

	/**
	 * createTaxExemptReviews method is used to create the tax exempt reviews.
	 * 
	 * @param accountId
	 * @param taxExemptReviewsRequestBean
	 * @return ResponseEntity<List<TaxExemptReview>>
	 */
	@PostMapping(value="/accounts/{accountId}/taxexempt-reviews", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create TaxExempt Reviews")
	public ResponseEntity<Object> createTaxExemptReviews(
			@ApiParam(value = "The account id", required = true) @PathVariable("accountId") String accountId,
			@ApiParam(value = "Send values to add a tax exempt reviews", required = true) @RequestBody TaxExemptReviewsRequestBean taxExemptReviewsRequestBean) {
		return auroraService.addTaxExemptReviews(accountId, taxExemptReviewsRequestBean);
	}

	/**
	 * createCustomer method is used to create customer and
	 * customer will update for given accountid.
	 * 
	 * @param accountId
	 * @return String - it contains customer response.
	 */
	@PostMapping(value = "/accounts/{accountId}/users", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Customer")
	public String createCustomer(
			@ApiParam(value = "create customer and map customer to account by id", required = true) @PathVariable("accountId") String  accountId,
			@ApiParam(value = "customer request data", required = true) @RequestBody AuroraCustomerRequestBean customerRequestBean) {

		return auroraService.createCustomer(accountId,customerRequestBean);
	}

	/**
	 * updateCustomer method is used to create customer and
	 * customer will update for given accountid.
	 * 
	 * @param accountId,customerId
	 * @return String - it contains customer response.
	 */
	@PostMapping(value = "/accounts/{accountId}/users/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update Customer")
	public String updateCustomer(
			@ApiParam(value = "account Id", required = true) @PathVariable("accountId") String  accountId,
			@ApiParam(value = "update the customer by id", required = true) @PathVariable(CUSTOMER_ID) String  customerId,
			@ApiParam(value = " update customer request data", required = true) @RequestBody AuroraUpdateCustomerRequestBean customerRequestBean) {

		return auroraService.updateCustomer(customerId, customerRequestBean);
	}

	/**
	 * assignCustomerGroup method is used to associate customerGroup with customer        
	 * 
	 * @param customerId,customerGroupId
	 * @return String - it contains customer response.
	 */
	@PostMapping(value="/users/{customerId}/group", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Associate Customer Group")
	public CustomerResponseBean assignCustomerGroup(
			@ApiParam(value = "Customer Id is", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "Customer Group Id is", required = true) @RequestParam String customerGroupId) {
		return auroraService.assignCustomerGroup(customerId, customerGroupId);
	}

	/**
	 * assignSalesrepToCompany method is used to update category object with sales representative.
	 * @param categoryId
	 * @param salesRepId
	 * @return CategoryObjectBean
	 */
	@PostMapping(value = "/accounts/categories/{categoryId}/sales-rep/{salesRepId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Assign sales rep to company", response = CategoryObjectBean.class)
	public CategoryObjectBean assignSalesrepToCompany(
			@ApiParam(value = "The category id", required = true) @PathVariable("categoryId") String categoryId,
			@ApiParam(value = "The sales rep id", required = true) @PathVariable("salesRepId") String salesRepId) {
		return auroraService.assignSalesrepToCompanyCategory(categoryId, salesRepId);
	}

	/**
	 * removeTaxExemptReviews method is used to remove the tax exempt reviews.
	 * @param accountId
	 * @param certificateId
	 * @return ResponseEntity<List<TaxExemptReview>>
	 */
	@PostMapping(value="/accounts/{accountId}/taxexempt-reviews/{certificateId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Remove TaxExempt Reviews")
	public ResponseEntity<Object> removeTaxExemptReviews(
			@ApiParam(value = "The account id", required = true) @PathVariable("accountId") String accountId,
			@ApiParam(value = "The certificate id", required = true) @PathVariable("certificateId") String certificateId) {
		return auroraService.removeTaxExemptReviews(accountId, certificateId);
	}

	/**
	 * createCompanyCategory method is used to create new category.
	 * 
	 * @param categoryName
	 * @param salesRepId
	 * @return CategoryObjectBean
	 */
	@PostMapping(value = "accounts/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create company category", response = CategoryObjectBean.class)
	public CategoryObjectBean createCompanyCategory(
			@ApiParam(value = "The category name is", required = true) @RequestParam String categoryName, 
			@ApiParam(value = "The sales rep id is", required = true) @RequestParam String salesRepId) {
		return auroraService.createCategory(categoryName, salesRepId);
	}

	/**
	 * removeCompanyCategory method is used to remove the company category details from the company custom-object and 
	 * to update the account custom-object with others category.
	 * 
	 * @param categoryId
	 * @return String - it contains the company category details after removal.
	 */
	@PostMapping(value="/accounts/categories/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Remove Company Category")
	public String removeCompanyCategory(
			@ApiParam(value = "The category id", required = true) @PathVariable("categoryId") String categoryId) {
		return auroraService.removeCategoryId(categoryId);
	}

	/**
	 * getCustomerById method is used to get customer data.
	 * @param customerId
	 * @param limit
	 * 
	 * @return CustomerDetailBean 
	 */
	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Customer Details")
	public CustomerDetailBean getCustomerData(
			@ApiParam(value = "The Customer Id  is", required = false) @RequestParam(required = false) String customerId,
			@ApiParam(value = "The Email Id  is", required = false) @RequestParam(required = false) String emailId,
			@ApiParam(value = "The Customer Source is", required = false) @RequestParam (required = false) String customerSource,
			@ApiParam(value = "The limit is", required = false) @RequestParam(defaultValue = "5" ) String limit) {
		return csrHelperService.getCustomerData(customerId ,emailId,Integer.parseInt(limit), customerSource);
	}

	/**
	 * updatePaymentOptionByAccount method is used to update the payment options.
	 * 
	 * @param paymentOptionRequestBean
	 * @return String - it contains AccountCustomObject as response.
	 */
	@PostMapping(value = "/accounts/{accountId}/payment-options/{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "update payment option by account id", response = String.class)
	public String updatePaymentOptionByAccount(
			@ApiParam(value = "account id", required = true) @PathVariable("accountId") String  accountId,
			@ApiParam(value = "payment id", required = true) @PathVariable("paymentId") String  paymentId,
			@ApiParam(value = "Request payment option", required = true) @RequestBody UpdatePaymentRequestBean updatePaymentRequestBean) {
		return registrationService.updatePaymentOption(null, paymentId, updatePaymentRequestBean, accountId);
	}

	/**
	 * createSalesRepresentative method is used to create new SalesRepresentative.
	 * 
	 * @param AuroraSalesRepRequestBean
	 * @return response -SalesRepsResponse
	 */
	@PostMapping(value = "/accounts/salesrep", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create a new Sales Representative")
	public SalesRepsResponse createSalesRepresentative(
			@ApiParam(value = "Sales Representative Request Bean", required = true) @RequestBody SalesRepRequestBean salesRequestBean) {
		return auroraService.createSalesRepresentative(salesRequestBean);
	}

	/**
	 * updateSalesRepresentative method is used to update a SalesRepresentative.
	 * 
	 * @param SalesRepRequestBean
	 * @return response -SalesRepsResponse
	 */
	@PostMapping(value = "/accounts/salesrep/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update a Sales Representative")
	public SalesRepsResponse updateSalesRepresentative(
			@ApiParam(value = "Sales-Rep-Id", required = true) @PathVariable("id") String  id,
			@ApiParam(value = "Sales Representative Request Bean", required = true) @RequestBody SalesRepRequestBean salesRequestBean) {
		return auroraService.updateSalesRepresentative(id ,salesRequestBean);
	}

}
