package com.molekule.api.v1.useraccountservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;

import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;

import lombok.Data;

@Data
public class ProcessOrderSendGridThread implements Runnable{
	
	Logger logger = LoggerFactory.getLogger(ProcessOrderSendGridThread.class);
	
	Map<String, String> requestData;
	private TemplateMailRequestHelper taxExemptionPendingApprovalSender;
	private TemplateMailRequestHelper taxExemptionSuccessConfirmationSender;
	private Value postResponseValue;
	private CTEnvProperties ctEnvProperties;
	private RegistrationHelperService registrationHelperService;
	private CheckoutHelperService checkoutHelperService;
	
	@Override
	public void run() {
		if (postResponseValue != null
				&& postResponseValue.isTaxExemptFlag()) {
			registrationHelperService.processOrder(requestData.get(CUSTOMER_ID), "TaxHold", requestData.get("accountId"));
		}
		if (Objects.nonNull(requestData.get(CUSTOMER_ID))) {
			SendGridModel sendGridModel = registrationHelperService.createEmailDynamicInfo(requestData.get(CUSTOMER_ID));
			taxExemptionPendingApprovalSender.sendMail(sendGridModel);
			if (postResponseValue != null
					&& postResponseValue.isTaxExemptFlag()) {
				taxExemptionSuccessConfirmationSender.sendMail(sendGridModel);
			}
		}
	}
	
}
