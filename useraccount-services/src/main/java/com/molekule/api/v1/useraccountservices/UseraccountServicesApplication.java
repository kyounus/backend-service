package com.molekule.api.v1.useraccountservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class UseraccountServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(UseraccountServicesApplication.class, args);
	}

}
