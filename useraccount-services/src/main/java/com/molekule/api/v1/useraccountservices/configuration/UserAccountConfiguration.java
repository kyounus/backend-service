package com.molekule.api.v1.useraccountservices.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.mail.sendgrid.AutoRefilActivationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.AutoRefillConfirmationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.CustomerEmailUpdateMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.CustomerPasswordUpdateMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.FreightInformationConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderCancelMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderProcessedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderShippedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderUpdatedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentReceivedConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentTermsPendingApprovalSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentTermsRequestConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentTermsSuccessConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.QuoteRequestConfirmationMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.QuoteRequestPendingApprovalMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.RegistartionConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.RegistrationBySalesRepConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.SubScriptionRenewalDateUpdationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.SubscriptionPaymentFailedMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.SubscriptionSignupConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TaxExemptionConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TaxExemptionPendingApprovalSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TaxExemptionSuccessConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.cart.FreightHandlingCostCalculationServiceImpl;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostCalculationService;
import com.molekule.api.v1.commonframework.service.cart.HandlingCostHelperService;
import com.molekule.api.v1.commonframework.service.cart.ParcelHandlingCostCalculationServieImpl;
import com.molekule.api.v1.commonframework.service.cart.TaxService;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.AuthenticationHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.CustomerProfileHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.sns.NotificationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.EncryptUtil;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.useraccountservices.util.CustomerProfileUtility;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The class MolekuleBeanConfiguration is used to configure the beans for the
 * application.
 */
@Configuration
@EnableSwagger2
//@PropertySource(value = ("${common.application.properties}"))
public class UserAccountConfiguration {

	@Value("${ct.aws.access.key.id}")
	private String awsAccessKeyId;

	@Value("${ct.aws.access.secret.key}")
	private String awsSecretKey;

	@Value("${ct.aws.region}")
	private String awsRegion;

	@Value("${ct.aws.s3.bucket.name}")
	private String awsS3BucketName;
    @Value("${devHost}")
    private String devHost;
    
    @Value("${qaHost}")
    private String qaHost;
    @Value("${ukHost}")
    private String ukHost;
    
    @Value("${profile}")
    private String profile;
    
    @Value("${ct.cognitoUserPool}")
	private String cognitoUserPool;
    
    @Value("${ct.cognitoClientId}")
	private String cognitoClientId;
    
    @Value("${ct.cognitoClientSecret}")
	private String cognitoClientSecret;

	@Bean(name = "dynamoDB")
	public DynamoDB getDynamoDB() {
		return new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(awsRegion)
				.withCredentials(getAWSCredentials()).build());

	}

	/**
	 * getDocket bean is written to get all apis in the swagger ui.
	 * 
	 * @return Docket
	 */
	@Bean
	public Docket getDocket() {
		  String host = null;	  
		   if("dev".equals(profile)) {
			   host = devHost;
		   }else if("qa".equals(profile)){
			   host = qaHost;
		   }else if("uk".equals(profile)){
			   host = ukHost;
		   }
		Docket swaggerDoc = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).build();
		if(host!= null) {
			swaggerDoc.host(host);
		}
		return swaggerDoc.apiInfo(getApiInfo());
	}

	/**
	 * crosConfigure bean is written to configure the cross platform applications.
	 * 
	 * @return WebMvcConfigurer
	 */
	@Bean
	public WebMvcConfigurer crosConfigure() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST","DELETE").allowedOrigins("*").allowedHeaders("*");
				WebMvcConfigurer.super.addCorsMappings(registry);
			}

		};
	}

	/**
	 * getApiInfo method is used to get the api information.
	 * 
	 * @return ApiInfo
	 */
	public ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("Molekule Integration Api's For User Account Service")
				.description("exposed integration api's from molekule to ct").build();
	}

	@Bean(name = "awsAccessKeyId")
	public String getAWSKeyId() {
		return EncryptUtil.decrypt(awsAccessKeyId);
	}

	@Bean(name = "awsSecretKey")
	public String getAWSKeySecret() {
		return EncryptUtil.decrypt(awsSecretKey);
	}

	@Bean(name = "awsRegion")
	public Region getAWSPollyRegion() {
		return Region.getRegion(Regions.fromName(awsRegion));
	}

	@Bean(name = "awsCredentialsProvider")
	public AWSCredentialsProvider getAWSCredentials() {
		BasicAWSCredentials awsCredentials = new BasicAWSCredentials(EncryptUtil.decrypt(this.awsAccessKeyId), EncryptUtil.decrypt(this.awsSecretKey));
		return new AWSStaticCredentialsProvider(awsCredentials);
	}

	@Bean(name = "awsS3BucketName")
	public String getAWSS3BucketName() {
		return awsS3BucketName;
	}

	@Bean
	public AmazonS3 generateS3Client() {
		return AmazonS3ClientBuilder.standard().withCredentials(getAWSCredentials()).withRegion(awsRegion).build();
	}

	@Bean
	public CTEnvProperties cTEnvProperties() {
		return new CTEnvProperties();
	}

	@Bean
	public NetConnectionHelper netConnectionHelper() {
		return new NetConnectionHelper();
	}

	@Bean
	public CtServerHelperService ctServerHelperService() {
		return new CtServerHelperService();
	}

	@Bean
	public RegistrationHelperService registrationHelperService() {
		return new RegistrationHelperService();
	}

	@Bean
	public CheckoutHelperService checkoutHelperService() {
		return new CheckoutHelperService();
	}

	@Bean
	public OrderHelperService orderHelperService() {
		return new OrderHelperService();
	}

	@Bean
	public CartHelperService cartHelperService() {
		return new CartHelperService();
	}

	@Bean("registrationConfirmation")
	@Primary
	public TemplateMailRequestHelper registartionConfirmationSender() {
		return new RegistartionConfirmationSender();
	}

	@Bean("paymentTermsPendingApprovalSender")
	public TemplateMailRequestHelper paymentTermsPendingApprovalSender() {
		return new PaymentTermsPendingApprovalSender();
	}

	@Bean("paymentTermsRequestConfirmationSender")
	public TemplateMailRequestHelper paymentTermsRequestConfirmationSender() {
		return new PaymentTermsRequestConfirmationSender();
	}

	@Bean("paymentTermsSuccessConfirmationSender")
	public TemplateMailRequestHelper paymentTermsSuccessConfirmationSender() {
		return new PaymentTermsSuccessConfirmationSender();
	}

	@Bean("taxExemptionConfirmationSender")
	public TemplateMailRequestHelper taxExemptionConfirmationSender() {
		return new TaxExemptionConfirmationSender();
	}
	@Bean("taxExemptionPendingApprovalSender")
	public TemplateMailRequestHelper taxExemptionPendingApprovalSender() {
		return new TaxExemptionPendingApprovalSender();
	}

	@Bean("taxExemptionSuccessConfirmationSender")
	public TemplateMailRequestHelper taxExemptionSuccessConfirmationSender() {
		return new TaxExemptionSuccessConfirmationSender();
	}

	@Bean("registrationBySalesRepConfirmationSender")
	public TemplateMailRequestHelper registrationBySalesRepConfirmationSender() {
		return new RegistrationBySalesRepConfirmationSender();
	}

	@Bean("freightInformationConfirmationSender")
	public TemplateMailRequestHelper freightInformationConfirmationSender() {
		return new FreightInformationConfirmationSender();
	}

	@Bean("subscriptionSignupConfirmationSender")
	public TemplateMailRequestHelper subscriptionSignupConfirmationSender() {
		return new SubscriptionSignupConfirmationSender();
	}
	
	@Bean("autoRefillConfirmationMailSender")
	public TemplateMailRequestHelper autoRefillConfirmationMailSender() {
		return new AutoRefillConfirmationMailSender();
	}

	@Bean("subScriptionRenewalDateUpdationSender")
	public TemplateMailRequestHelper subScriptionRenewalDateUpdationSender() {
		return new SubScriptionRenewalDateUpdationSender();
	}

	@Bean
	public CsrHelperService csrHelperService() {
		return new CsrHelperService();
	}

	@Bean
	public TaxService taxService() {
		return new TaxService();
	}

	@Bean
	public HandlingCostHelperService handlingCostHelperService() {
		return new HandlingCostHelperService();
	}

	@Bean("handlingCostCalculationService")
	public HandlingCostCalculationService parcelHandlingCostCalculationServieImpl() {
		return new ParcelHandlingCostCalculationServieImpl();
	}

	@Bean("handlingCostCalculationService")
	public HandlingCostCalculationService freightHandlingCostCalculationServiceImpl() {
		return new FreightHandlingCostCalculationServiceImpl();
	}

	@Bean
	public TealiumHelperService tealiumHelperService() {
		return new TealiumHelperService();
	}

	@Bean
	public CustomerProfileUtility customerProfileUtility() {
		return new CustomerProfileUtility();
	}

	@Bean
	public NotificationHelperService notificationHelperService() {
		return new NotificationHelperService();
	}

	@Bean
	public TemplateMailRequestHelper quoteRequestConfirmationMailSender() {
		return new QuoteRequestConfirmationMailSender();
	}

	@Bean
	public TemplateMailRequestHelper quoteRequestPendingApprovalMailSender() {
		return new QuoteRequestPendingApprovalMailSender();
	}

	@Bean
	public TemplateMailRequestHelper paymentReceivedConfirmationSender() {
		return new PaymentReceivedConfirmationSender();
	}

	@Bean
	public TemplateMailRequestHelper orderShippedMailSender() {
		return new OrderShippedMailSender();
	}
	
	@Bean
	public AuroraHelperService auroraHelperService() {
		return new AuroraHelperService();
	}
	
	@Bean
	public TemplateMailRequestHelper orderUpdatedMailSender() {
		return new OrderUpdatedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper subscriptionPaymentFailedMailSender() {
		return new SubscriptionPaymentFailedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderProcessedMailSender() {
		return new OrderProcessedMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper orderCancelMailSender() {
		return new OrderCancelMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper customerEmailUpdateMailSender() {
		return new CustomerEmailUpdateMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper customerPasswordUpdateMailSender() {
		return new CustomerPasswordUpdateMailSender();
	}
	
	@Bean
	public TemplateMailRequestHelper autoRefilActivationMailSender() {
		return new AutoRefilActivationMailSender();
	}
	
	@Bean
	public AuthenticationHelperService authenticationHelperService() {
		if(com.amazonaws.util.StringUtils.isNullOrEmpty(cognitoClientSecret)) {
			return new AuthenticationHelperService(cognitoUserPool, cognitoClientId, null);			
		} else {
			return new AuthenticationHelperService(cognitoUserPool, cognitoClientId, cognitoClientSecret);
		}
	}
	
	@Bean
	public CustomerProfileHelperService customerProfileHelperService() {
		return new CustomerProfileHelperService();
	}
}
