package com.molekule.api.v1.useraccountservices.controller;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.model.customerprofile.AutoRefillsResponseBean;
import com.molekule.api.v1.commonframework.model.customerprofile.BulkUpdateRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.CustomerProfile;
import com.molekule.api.v1.commonframework.model.customerprofile.DeviceRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.GetSubscriptionBean;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionConfiguration;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSortEnum;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSurveyResponse;
import com.molekule.api.v1.commonframework.model.customerprofile.UpdateAutoRefillsRequestBean;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.model.registration.CustomerRequestBean;
import com.molekule.api.v1.commonframework.service.sns.NotificationHelperService;
import com.molekule.api.v1.commonframework.util.CustomerChannelEnum;
import com.molekule.api.v1.useraccountservices.service.AuroraService;
import com.molekule.api.v1.useraccountservices.service.CustomerProfileService;
import com.molekule.api.v1.useraccountservices.util.BulkUpdateActionEnum;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/v1")
public class CustomerProfileController {

	@Autowired
	@Qualifier("auroraService")
	AuroraService auroraService;

	@Autowired
	@Qualifier("customerProfileService")
	CustomerProfileService customerProfileService;

	@Autowired
	@Qualifier("notificationHelperService")
	NotificationHelperService notificationHelperService;

	Logger logger = LoggerFactory.getLogger(CustomerProfileController.class);

	/**
	 * getCustomerProfile method issue to get customer profile data.
	 * 
	 * @return ResponseEntity<CustomerProfile>
	 */
	@GetMapping(value = "/accounts/users/{customerId}/dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get customer profile")
	public ResponseEntity<CustomerProfile> getCustomerProfile(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "The limit is", required = false) @RequestParam(defaultValue = "5") String limit,
			@ApiParam(value = "The offset is", example = "0", required = false) @RequestParam int offset) {
		return customerProfileService.getCustomerProfile(customerId, limit, offset);
	}

	/**
	 * getSubscriptions method issue to get auto-refills data.
	 * 
	 * @param customerId
	 * @param shippingAddressId
	 * @return
	 */

	@GetMapping(value = "/users/{customerId}/subscriptions", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get auto-refill subscriptions")
	public ResponseEntity<AutoRefillsResponseBean> getSubscriptions(
			@ApiParam(value = "The limit is", example = "20", required = true) @RequestParam int limit,
			@ApiParam(value = "The offset is", example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "shipping address id", required = false) @RequestParam(defaultValue = "") String shippingAddressId,
			@ApiParam(value = "payment id", required = false) @RequestParam(defaultValue = "") String paymentId) {
		return customerProfileService.getAutoRefillsSubscriptions(customerId, shippingAddressId, paymentId, limit,
				offset);
	}

	/**
	 * getAllSubscriptions method issue to get all subscriptions data.
	 * 
	 * @param limit
	 * @param offset
	 * @param nextOrderDate
	 * @param email
	 * @return ResponseEntity<AutoRefillsResponseBean>
	 */
	@GetMapping(value = "/users/subscriptions", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all subscriptions")
	public ResponseEntity<AutoRefillsResponseBean> getAllSubscriptions(
			@ApiParam(value = "The limit is", example = "20", required = true) @RequestParam int limit,
			@ApiParam(value = "The offset is", example = "0", required = true) @RequestParam int offset,
			@ApiParam(value = "Next Order Date", required = false) @RequestParam(defaultValue = "") String nextOrderDate,
			@ApiParam(value = "email", required = false) @RequestParam(defaultValue = "") String email,
			@ApiParam(value = "Subscription Id", required = false) @RequestParam(defaultValue = "") String subscriptionId,
			@ApiParam(value = "The Customer Id", required = false) @RequestParam(defaultValue = "") String customerId,
			@ApiParam(value = "Channel", required = false) @RequestParam(defaultValue = "") String channel,
			@ApiParam(value = "Sort Action", required = false) @RequestParam(defaultValue = "") SubscriptionSortEnum sortAction,
			@ApiParam(value = "Sort Order", required = false) @RequestParam(defaultValue = "") SortOrderEnum sortOrder) {
			
		GetSubscriptionBean getSubscriptionBean = new GetSubscriptionBean();
		getSubscriptionBean.setLimit(limit);
		getSubscriptionBean.setOffset(offset);
		getSubscriptionBean.setNextOrderDate(nextOrderDate);
		getSubscriptionBean.setEmail(email);
		getSubscriptionBean.setSubscriptionId(subscriptionId);
		getSubscriptionBean.setCustomerId(customerId);
		getSubscriptionBean.setChannel(channel);
		getSubscriptionBean.setSortAction(sortAction);
		getSubscriptionBean.setSortOrder(sortOrder);
		
		return customerProfileService.getAllSubscriptions(getSubscriptionBean);
	}

	/**
	 * updateAutoRefills method is used to update the auto-refills.
	 * 
	 * @param customerId
	 * @param container
	 * @param key
	 * @return
	 */
	@PostMapping(value = "/users/{customerId}/subscriptions/{container}/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update auto-refills", response = String.class)
	public ResponseEntity<String> updateAutoRefills(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = CONTAINER, required = true) @PathVariable(CONTAINER) String container,
			@ApiParam(value = "key", required = true) @PathVariable("key") String key,
			@ApiParam(value = "Update Auto Refills Request Bean", required = true) @RequestBody UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean) {
		return customerProfileService.updateAutoRefills(customerId, container, key, updateAutoRefillsRequestBean);
	}

	/**
	 * getAllSurveysResponse method issue to get all Survey questions .
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/users/subscriptions/surveys", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all Subcription Survey Questions")
	public ResponseEntity<SubscriptionSurveyResponse> getAllSurveysResponse(
			@ApiParam(value = "Channel", required = false) @RequestParam(defaultValue = "") CustomerChannelEnum channel) {
		return customerProfileService.getAllSurveysResponse(channel);
	}

	/**
	 * getSurveysResponseById method issue to get particular Survey questions .
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/users/subscriptions/surveys/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Subcription Survey Question By Id")
	public SubscriptionSurveyResponse getSurveysResponseById(
			@ApiParam(value = "The id", required = true) @PathVariable("id") String id) {
		return customerProfileService.getSurveysResponseById(id);
	}

	/**
	 * addSurveyResponse method is to add a Survey questions .
	 * 
	 * @return ResponseEntity<String>
	 * @throws Exception
	 */
	@PostMapping(value = "/users/subscriptions/surveys", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Add A Subscription Survey Question")
	public ResponseEntity<Object> addSurveyResponse(
			@ApiParam(value = "The Question is", required = true) @RequestParam("question") String question,
			@ApiParam(value = "Channel", required = false) @RequestParam(defaultValue = "") CustomerChannelEnum channel) {
		return customerProfileService.addSubcriptionSurveyQuestion(question, channel);
	}

	/**
	 * updateSurveyResponse method is to update a Survey question by Id .
	 * 
	 * @return ResponseEntity<String>
	 */
	@PostMapping(value = "/users/subscriptions/surveys/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update A Subscription Survey Question")
	public ResponseEntity<SubscriptionSurveyResponse> updateSurveyResponse(
			@ApiParam(value = "The id", required = true) @PathVariable("id") String id,
			@ApiParam(value = "The Question is", required = true) @RequestParam("question") String question) {
		return customerProfileService.updateSubcriptionSurveyQuestion(id, question);
	}

	@PostMapping(value = "/users/subscriptions/event-nofication", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Send Subscription Notification Mail To Customer")
	public String subscriptionEventNotification(HttpServletRequest serverHttpRequest,
			HttpServletResponse serverHttpResponse) {
		String response = notificationHelperService.getSNSResponse(serverHttpRequest);
		logger.info("Response Body : {}", response);
		return response;
	}

	/**
	 * updateBulkSubscription method is used to update bulk subscriptions.
	 * 
	 * @param bulkUpdateActionEnum
	 * @param bulkUpdateRequestBean
	 * @return ResponseEntity<String>
	 */
	@PostMapping(value = "/users/subscriptions", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update bulk subscription", response = String.class)
	public ResponseEntity<String> updateBulkSubscription(
			@ApiParam(value = ACTION, required = true) @RequestParam BulkUpdateActionEnum bulkUpdateActionEnum,
			@ApiParam(value = "BulkUpdateRequestBean", required = false) @RequestBody BulkUpdateRequestBean bulkUpdateRequestBean) {
		return customerProfileService.updateBulkSubscription(bulkUpdateActionEnum, bulkUpdateRequestBean);
	}

	/**
	 * getSubscriptionsById method issue to get specific subscriptions data.
	 * 
	 * @param subscriptionId
	 * @return ResponseEntity<AutoRefillsResponseBean>
	 */
	@GetMapping(value = "/users/subscriptions/{subscriptionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get subscription by id")
	public ResponseEntity<AutoRefillsResponseBean> getSubscriptionsById(
			@ApiParam(value = "Subscription id", required = true) @PathVariable("subscriptionId") String subscriptionId) {
		return customerProfileService.getSubscriptionsById(subscriptionId);
	}
	
	/**
	 * configureDeviceFilterSubscriptionJob method is used to configure subscription job.
	 * 
	 * @param subscriptionConfiguration
	 * @return ResponseEntity<String>
	 */
	@PostMapping(value = "/users/subscriptions/configure", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Configure subscripiton job", response = String.class)
	public ResponseEntity<String> configureDeviceFilterSubscriptionJob(
			@ApiParam(value = "SubscriptionConfiguration", required = true) @RequestBody SubscriptionConfiguration  subscriptionConfiguration ) {
		return customerProfileService.configureDeviceFilterSubscriptionJob(subscriptionConfiguration);
	}
	
	/**
	 * getDeviceFilterSubscriptionJob method is used to get subscription job.
	 * 
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/users/subscriptions/configure", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get subscripiton job", response = String.class)
	public ResponseEntity<String> getDeviceFilterSubscriptionJob() {
		return customerProfileService.getDeviceFilterSubscriptionJob();
	}
	
	/**
	 * updateCustomerById method is used to update the customer Data.
	 * 
	 * @param customerRequestBean
	 * @return String
	 */
	@PostMapping(value = "/users/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "updateCustomerById")
	public String updateCustomerById(
			@ApiParam(value = "The customer id", required = true) @PathVariable("customerId") String customerId,
			@ApiParam(value = "Update Customer Data", required = true) @RequestBody CustomerRequestBean customerRequestBean,@RequestHeader("country") String country) {
		return customerProfileService.updateCustomerById(customerId, customerRequestBean,country);
	}

	/**
	 * registerDevice method is used to register a device.
	 * 
	 * @param deviceRequestBean
	 * @return ResponseEntity<String>
	 */
	@PostMapping(value = "/users/devices", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registerDevice")
	public ResponseEntity<String> registerDevice(
			@ApiParam(value = "Device Request Bean", required = true) @RequestBody DeviceRequestBean deviceRequestBean) {
		return customerProfileService.registerDevice(deviceRequestBean);
	}
	
	/**
	 * getAllDevices method is used to list all devices.
	 * 
	 * @param deviceId
	 * @param serialNumber
	 * @param sku
	 * @param purchaseDate
	 * @param customerId
	 * @param createdAt
	 * @param updatedAt
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/users/devices", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get All Devices", response = String.class)
	public ResponseEntity<String> getAllDevices(
			@ApiParam(value = "device Id", required = false) @RequestParam(defaultValue = "") String deviceId,
			@ApiParam(value = "serial Number", required = false) @RequestParam(defaultValue = "") String serialNumber,
			@ApiParam(value = "sku", required = false) @RequestParam(defaultValue = "") String sku,
			@ApiParam(value = "purchase Date", required = false) @RequestParam(defaultValue = "") String purchaseDate,
			@ApiParam(value = "customer id", required = false) @RequestParam(defaultValue = "") String customerId,
			@ApiParam(value = "created At", required = false) @RequestParam(defaultValue = "") String createdAt,
			@ApiParam(value = "updated At", required = false) @RequestParam(defaultValue = "") String updatedAt) {
			
			Map<String,String> filterRequestMap = new HashMap<>();
			filterRequestMap.put("deviceId", deviceId);
			filterRequestMap.put("serialNumber", serialNumber);
			filterRequestMap.put("sku", sku);
			filterRequestMap.put("purchaseDate", purchaseDate);
			filterRequestMap.put("customerId", customerId);
			filterRequestMap.put("createdAt", createdAt);
			filterRequestMap.put("updatedAt", updatedAt);
		
		return customerProfileService.getAllDevices(filterRequestMap);
	}
	
	/**
	 * getDeviceDetail method is used to get a specific device detail.
	 * 
	 * @param deviceId
	 * @return ResponseEntity<String>
	 */
	@GetMapping(value = "/users/devices/{deviceId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Device Detail", response = String.class)
	public ResponseEntity<String> getDeviceDetail(@ApiParam(value = "device Id", required = true) @PathVariable("deviceId") String deviceId) {
		return customerProfileService.getDeviceDetail(deviceId);
	}
	
	/**
	 * updateDevice method is used to update specific device with device id.
	 * 
	 * @param deviceId
	 * @param deviceRequestBean
	 * @return ResponseEntity<String>
	 */
	@PostMapping(value = "/users/devices/{deviceId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "updateDevice")
	public ResponseEntity<String> updateDevice(
			@ApiParam(value = "device Id", required = true) @PathVariable("deviceId") String deviceId,
			@ApiParam(value = "Device Request Bean", required = false) @RequestBody DeviceRequestBean deviceRequestBean) {
		return customerProfileService.updateDevice(deviceId, deviceRequestBean);
	}
	
	/**
	 * deleteDevice method is used to delete a specific device from list.
	 * 
	 * @param deviceId
	 * @return ResponseEntity<String>
	 */
	@DeleteMapping(value = "/users/devices/{deviceId}")
	@ApiOperation(value = "Delete Device")
	public ResponseEntity<String> deleteDevice(@ApiParam(value = "deviceId", required = true) @PathVariable("deviceId") String deviceId){
		return customerProfileService.deleteDevice(deviceId);
	}
	
	/**
	 * getSubscriptionContainerAndKey method is used to get container and key of subscription data.
	 * 
	 * @param customerId
	 * @param orderId
	 * @return
	 */
	@GetMapping(value = "/users/{customerId}/subscriptions/{orderId}")
	@ApiOperation(value = "Get Subscription Container And Key")
	public ResponseEntity<String> getSubscriptionContainerAndKey(
			@ApiParam(value = "customer id", required = true) @PathVariable(CUSTOMER_ID) String customerId,
			@ApiParam(value = "orderId", required = true) @PathVariable("orderId") String orderId) {
		return customerProfileService.getSubscriptionContainerAndKey(customerId, orderId);
	}
	
	/**
	 * getSubscriptionPlans method is used to get a Subscription Plans.
	 * 
	 * @return String
	 */
	@GetMapping(value = "/users/subscriptions/plan", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Subscription Plans", response = String.class)
	public String getSubscriptionPlans() {
		return customerProfileService.getSubscriptionPlans();
	}
	
	/**
	 * createSubscription method is used to create new Subscription .
	 * @return 
	 * 
	 * @return String
	 */
	@PostMapping(value = "/users/subscriptions/create", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create Subscription", response = String.class)
	public String createSubscription(
			@ApiParam(value = "Subscription Request Bean", required = true) @RequestBody SubscriptionRequestBean subscriptionRequestBean,
			@RequestHeader("country") String country) {
	return customerProfileService.createSubscription(subscriptionRequestBean,country);
	}
}
