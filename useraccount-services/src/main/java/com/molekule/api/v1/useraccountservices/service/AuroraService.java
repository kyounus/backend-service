package com.molekule.api.v1.useraccountservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.CompanyCategories;
import com.molekule.api.v1.commonframework.dto.registration.Custom;
import com.molekule.api.v1.commonframework.dto.registration.Customer;
import com.molekule.api.v1.commonframework.dto.registration.CustomerAction;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponse;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.aurora.AuroraCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.AuroraUpdateCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.CategoryObjectBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepresentativesBean;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepsResponse;
import com.molekule.api.v1.commonframework.model.aurora.SalesRepsResponse.CompanyCategory;
import com.molekule.api.v1.commonframework.model.aurora.CustomerSortActionEnum;
import com.molekule.api.v1.commonframework.model.aurora.GetCustomerBean;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.model.registration.AccountCustomResponseBean;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReview;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReviewsRequestBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.useraccountservices.util.RegistrationUtility;

/**
 * The class AuroraService is used to write the api calls for the Merchant
 * Center.
 * 
 * @version 1.0
 */
@Service("auroraService")
public class AuroraService {

	Logger logger = LoggerFactory.getLogger(AuroraService.class);

	private static final SecureRandom RANDOM = new SecureRandom();

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("registrationService")
	RegistrationService registrationService;

	@Autowired
	@Qualifier("registrationBySalesRepConfirmationSender")
	TemplateMailRequestHelper registrationBySalesRepConfirmationSender;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("auroraHelperService")
	AuroraHelperService auroraHelperService;

	/**
	 * getListOfCustomer method is used to get list of customers.
	 * 
	 * @param limit
	 * @param offset
	 * @param email
	 * @param companyName
	 * @param customerChannel 
	 * @param lastName 
	 * @param firstName 
	 * @return String- it contain all customer related values.
	 */                             
	public String getListOfCustomer(GetCustomerBean getCustomerBean) {
		int limit = getCustomerBean.getLimit();
		int offset = getCustomerBean.getOffset();
		String companyName = getCustomerBean.getCompanyName();
		String email = StringUtils.hasText(getCustomerBean.getEmail()) ? getCustomerBean.getEmail().toLowerCase() : null;
		String firstName = StringUtils.hasText(getCustomerBean.getFirstName()) ? getCustomerBean.getFirstName().toUpperCase() : null;
		String lastName = StringUtils.hasText(getCustomerBean.getLastName()) ? getCustomerBean.getLastName().toUpperCase() : null;
		String customerChannel = getCustomerBean.getCustomerChannel();
		CustomerSortActionEnum sortAction = getCustomerBean.getSortAction();
		SortOrderEnum sortOrder = getCustomerBean.getSortOrder();
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customers?"
				+ "limit=" + limit + OFF_SET + offset + "&expand=customerGroup.typeId";
		String token = ctServerHelperService.getStringAccessToken();
		String response = null;
		if(sortAction!=null && sortOrder != null) {
			customerBaseUrl = sortCustomerUrl(sortAction, sortOrder,customerBaseUrl);
		}
		if(StringUtils.hasText(companyName) || StringUtils.hasText(email) || 
				StringUtils.hasText(firstName)||StringUtils.hasText(lastName) || StringUtils.hasText(customerChannel)) {
			customerBaseUrl = enocodeUrlParameters(companyName, email,customerBaseUrl,firstName,lastName,customerChannel);
			try {
				response = netConnectionHelper.httpConnectionHelper(customerBaseUrl,token);
			} catch (IOException e) {
				logger.error("IOException Occured", e);
			}
		}else {
			if(sortAction == null && sortOrder == null)
				customerBaseUrl = customerBaseUrl + "&sort=createdAt desc";
			response = netConnectionHelper.sendGetWithoutBody(token, customerBaseUrl);
		}
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonArray resultsJsonArray = jsonObject.get(RESULTS).getAsJsonArray();
		if(resultsJsonArray.size() <= 0) {
			return response;
		}
		for (int i = 0; i < resultsJsonArray.size(); i++) {
			JsonObject resultJsonObject = resultsJsonArray.get(i).getAsJsonObject();
			resultJsonObject.remove(PASSW0RD);
		}
		return jsonObject.toString();
	}
	/**
	 * enocodeUrlParameters is used to enocode the url parameters.
	 * @param companyName
	 * @param email
	 * @param customerBaseUrl
	 * @param customerStore 
	 * @param customerChannel 
	 * @param dob 
	 * @param lastName 
	 * @param firstName 
	 * @return String
	 */
	private String enocodeUrlParameters(String companyName, String email, String customerBaseUrl, String firstName, String lastName, String customerChannel) {
		if (StringUtils.hasText(companyName)) {
			String companyNameStr = "companyName=\""+ companyName + "\"";
			customerBaseUrl = customerBaseUrl + WHERE + MolekuleUtility.convertToEncodeUri(companyNameStr);
		} 
		if (StringUtils.hasText(email)) {
			String emailStr = "email=\""+ email + "\"";
			customerBaseUrl = customerBaseUrl + WHERE + MolekuleUtility.convertToEncodeUri(emailStr);
		}
		if (StringUtils.hasText(firstName)) {
			String firstNameStr = "firstName=\""+ firstName + "\"";
			customerBaseUrl = customerBaseUrl + WHERE + MolekuleUtility.convertToEncodeUri(firstNameStr);
		}
		if (StringUtils.hasText(lastName)) {
			String lastNameStr = "lastName=\""+ lastName + "\"";
			customerBaseUrl = customerBaseUrl + WHERE + MolekuleUtility.convertToEncodeUri(lastNameStr);
		}
		if (StringUtils.hasText(customerChannel)) {
			String customerChannelStr = "custom(fields(channel=\""+ customerChannel + "\"))";
			customerBaseUrl = customerBaseUrl + WHERE + MolekuleUtility.convertToEncodeUri(customerChannelStr);
		}
		return customerBaseUrl;
	}

	private String sortCustomerUrl(CustomerSortActionEnum sortAction,SortOrderEnum sortOrder,String customerBaseUrl) {
		if(sortAction.name().equals(CustomerSortActionEnum.CUSTOMER_NUMBER.name())){
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "customerNumber");
		}else if(sortAction.name().equals(CustomerSortActionEnum.FIRST_NAME.name())) {
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "firstName");
		}else if(sortAction.name().equals(CustomerSortActionEnum.LAST_NAME.name())) {
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "lastName");
		}else if(sortAction.name().equals(CustomerSortActionEnum.COMPANY_NAME.name())) {
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "companyName");
		}else if(sortAction.name().equals(CustomerSortActionEnum.EMAIL.name())) {
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "email");
		}else if(sortAction.name().equals(CustomerSortActionEnum.DATE_CREATED.name())) {
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "createdAt");
		}else if(sortAction.name().equals(CustomerSortActionEnum.DATE_MODIFIED.name())) {
			customerBaseUrl = getcustomerSortUrl(sortOrder, customerBaseUrl, "lastModifiedAt");
		}
		return customerBaseUrl;
	}
	private String getcustomerSortUrl(SortOrderEnum sortOrder, String customerBaseUrl, String sortField) {
		String fieldName;
		if( sortOrder.name().equals(SortOrderEnum.ASC.name())) {
			fieldName = sortField + " asc";
			customerBaseUrl = customerBaseUrl + AND_SORT +MolekuleUtility.convertToEncodeUri(fieldName);
		}else if( sortOrder.name().equals(SortOrderEnum.DESC.name())) {
			fieldName = sortField + " desc";
			customerBaseUrl = customerBaseUrl + AND_SORT +MolekuleUtility.convertToEncodeUri(fieldName)	;
		}
		return customerBaseUrl;
	}

	/**
	 * getListOfAccounts method is used to get the list of accounts.
	 * 
	 * @param limit
	 * @param offset
	 * @param companyName
	 * @param termStatus
	 * @return
	 */
	public String getListOfAccounts(int limit, int offset, String companyName, String termStatus) {
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?limit=" + limit + OFF_SET + offset + "&sort=createdAt desc";
		if (StringUtils.hasText(companyName)) {
			accountBaseUrl = accountBaseUrl + "&where=value(companyName=\"" + companyName + "\")";
		} else if (StringUtils.hasText(termStatus)) {
			accountBaseUrl = accountBaseUrl + "&where=value(payments(termsStatus=\"" + termStatus + "\"))";
		}
		String token = ctServerHelperService.getStringAccessToken();
		return netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
	}

	/**
	 * getCustomersByAccountId method is used to get list of customers data by
	 * passing accountId.
	 * 
	 * @param accountId
	 * @param customerId
	 * @return List<CustomerResponse>
	 */
	public List<CustomerResponse> getCustomersByAccountId(String customerId) {
		List<CustomerResponse> customersList = new ArrayList<>();
		String token = ctServerHelperService.getStringAccessToken();
		CustomerResponse response = new CustomerResponse();
		CustomerResponseBean customerResponse = registrationHelperService.getCustomerById(customerId);
		String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
				+ "?where=customerId=\"" + customerId + "\""
				+ "&where=custom(fields(parentOrderReferenceId is not defined))"
				+ "&where=custom(fields(Quote is not defined))" + "&where=cartState=\"Active\"";
		JsonObject cartObject = MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(token, cartUrl));
		if(cartObject.get("results").getAsJsonArray().size() > 0) {
			JsonObject resultObject = cartObject.get("results").getAsJsonArray().get(0).getAsJsonObject();
			if(resultObject.has("id")) {
				response.setCartId(resultObject.get("id").getAsString());
			}
		}
		response.setCustomerResponseBean(customerResponse);
		customersList.add(response);
		return customersList;
	}

	/**
	 * getAllCustomersByAccountId method is used to get the customers information by
	 * passing accountId.
	 * 
	 * @param accountId
	 * @return List<CustomerResponseBean>
	 */
	public List<CustomerResponseBean> getAllCustomersByAccountId(String accountId) {
		List<CustomerResponseBean> customersList = new ArrayList<>();
		List<String> employeeIds = new ArrayList<>();
		String token = ctServerHelperService.getStringAccessToken();

		String url = getCTCustomObjectsByAccountIdUrl(accountId);
		String accountStringResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
		if (accountJsonObject.get(STATUS_CODE) != null) {
			return customersList;
		}
		JsonArray resultsArray = accountJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		if (resultsObject.getAsJsonObject(VALUE) != null) {
			JsonObject valueObject = resultsObject.getAsJsonObject(VALUE).getAsJsonObject();
			JsonArray employeesArray = valueObject.getAsJsonArray("employeeIds").getAsJsonArray();
			if (Objects.nonNull(employeesArray)) {
				int len = employeesArray.size();
				for (int i = 0; i < len; i++) {
					employeeIds.add(employeesArray.get(i).toString().replace("\"", ""));
				}
			}
		}
		employeeIds.stream().forEach(employee -> {
			CustomerResponseBean customerResponse = registrationHelperService.getCustomerById(employee);
			customersList.add(customerResponse);
		});

		return customersList;
	}

	/**
	 * getCTCustomObjectsByAccountIdUrl method is used to form url for
	 * custom-objects where condition by id.
	 * 
	 * @param accountId
	 * @return String
	 */
	public String getCTCustomObjectsByAccountIdUrl(String accountId) {
		return new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
				.append("/custom-objects?where=id=\"").append(accountId).append("\"").toString();
	}

	/**
	 * getListCustomerGroups method is used to get the list of Customer Groups.
	 * 
	 * @return String- it contains list of Customer Groups.
	 */
	public String getListCustomerGroups() {
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customer-groups";
		String token = ctServerHelperService.getStringAccessToken();
		return netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
	}

	/**
	 * createAccount method is used to create a account from merchant center.
	 * 
	 * @param accountCustomResponseBean
	 * 
	 * @return String - it contains custom-object detail.
	 */
	public String createAccount(AccountCustomResponseBean accountCustomResponseBean) {
		//Checking company name is already exist or not
		String accountCheckUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=value(companyName=\"" + accountCustomResponseBean.getCompanyName() + "\")&sort=createdAt desc";

		String companyResponse = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(), accountCheckUrl);
		JsonObject companyObject = MolekuleUtility.parseJsonObject(companyResponse);
		if(companyObject.has(RESULTS) && companyObject.get(RESULTS).getAsJsonArray().size() >0) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "This is an existing account");
			return errorResponse.toString();
		}

		String key = RegistrationUtility.generateKey(accountCustomResponseBean.getCompanyName(), "Merchant-Center");
		// Get category Details from CT server.
		String ctCategoryResponse = auroraHelperService.getCompanyCategories();
		List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);
		List<CategoryDto> filterdCategoryDtoList = categoryDtoList.stream()
				.filter(dto -> dto.getId().equals(accountCustomResponseBean.getCompanyCategoryId()))
				.collect(Collectors.toList());
		String salesRepId = auroraHelperService.getSalesRepValueFromCategoryList(categoryDtoList,
				accountCustomResponseBean.getCompanyCategoryId());
		// Get SalesRep Details from CT server.
		String ctSalesRepResponse = auroraHelperService.getListOfSalesRep();
		List<SalesRepresentativeDto> salesRepresentativeDtoList = MolekuleUtility
				.getSalesRepresentativeDtoList(ctSalesRepResponse);
		SalesRepresentativeDto salesRespresentativeDto = auroraHelperService
				.getSalesRepDetails(salesRepresentativeDtoList, salesRepId);
		AccountCustomObject custom = new AccountCustomObject();
		custom.setContainer("b2b");
		custom.setKey(key);
		Value value = new Value();
		CategoryDto categoryDto = filterdCategoryDtoList.get(0);
		value.setCompanyCategory(categoryDto);
		value.setCompanyName(accountCustomResponseBean.getCompanyName());
		value.setNoOfEmployees(accountCustomResponseBean.getNumberOfEmployees());
		value.setNoOfOfficeSpaces(accountCustomResponseBean.getNumberOfOfficeSpaces());
		value.setSalesRepresentative(salesRespresentativeDto);
		custom.setValue(value);
		return registrationService.getAccountID(custom, ctServerHelperService.getStringAccessToken());
	}

	/**
	 * addTaxExemptReviews method is used to add the tax exempt reviews.
	 * 
	 * @param accountId
	 * @param taxExemptReviewsRequestBean
	 * @return ResponseEntity<List<TaxExemptReview>>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<Object> addTaxExemptReviews(String accountId,
			TaxExemptReviewsRequestBean taxExemptReviewsRequestBean) {
		List<TaxExemptReview> taxExemptReviewList = new ArrayList<>();
		String url = getCTCustomObjectsByAccountIdUrl(accountId);
		String token = ctServerHelperService.getStringAccessToken();
		String accountStringResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
		if (accountJsonObject.get(STATUS_CODE) != null) {
			return new ResponseEntity(accountJsonObject,
					HttpStatus.valueOf(accountJsonObject.get(STATUS_CODE).getAsInt()));
		}
		JsonArray resultsArray = accountJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		if (resultsArray.size() != 0) {
			JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
			if (resultsObject.getAsJsonObject(VALUE) != null) {
				JsonObject valueObject = resultsObject.getAsJsonObject(VALUE).getAsJsonObject();
				if (valueObject.getAsJsonArray(TAX_EXEMPT_REVIEW) != null) {
					prepareTaxExemptReviewList(taxExemptReviewList, valueObject);
				}
				TaxExemptReview addTaxExemptReview = new TaxExemptReview();
				addTaxExemptReview.setCertificateId(UUID.randomUUID().toString());
				addTaxExemptReview.setCountry(taxExemptReviewsRequestBean.getCountry());
				addTaxExemptReview.setState(taxExemptReviewsRequestBean.getState());
				addTaxExemptReview.setExpiryDate(taxExemptReviewsRequestBean.getExpirationDate());
				addTaxExemptReview.setTaxCertificationId(taxExemptReviewsRequestBean.getTaxCertificationId());
				taxExemptReviewList.add(addTaxExemptReview);
				return updateCustomObject(taxExemptReviewList, token, resultsObject, valueObject);
			}
		}
		return new ResponseEntity(HttpStatus.BAD_REQUEST);

	}

	public String createCustomer(String accountId, AuroraCustomerRequestBean customerRequest) {
		String password = ctEnvProperties.getPw0rd();
		CustomerDTO customerDTO = new CustomerDTO();
		if(StringUtils.hasText(customerRequest.getFirstName()))
			customerDTO.setFirstName(customerRequest.getFirstName().toUpperCase());
		if(StringUtils.hasText(customerRequest.getLastName()))
			customerDTO.setLastName(customerRequest.getLastName().toUpperCase());
		customerDTO.setEmail(customerRequest.getEmail());
		customerDTO.setPassword(password);
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customers?";
		String token = ctServerHelperService.getStringAccessToken();
		String customerNumber = RegistrationUtility.getCustomerNumber();
		customerDTO.setCustomerNumber(customerNumber);

		String customerGroup = (String) registrationHelperService
				.getCustomerGroup(ctEnvProperties.getCustomerGroupId());
		JsonObject customerGroupJsonObject = MolekuleUtility.parseJsonObject(customerGroup);
		String customerGroupName = customerGroupJsonObject.get("name").getAsString();

		Map<String, Object> createCustomObjectParamMap = new HashMap<>();
		createCustomObjectParamMap.put("typeId", ctEnvProperties.getCustomAttributeId());
		createCustomObjectParamMap.put("accountId", accountId);
		createCustomObjectParamMap.put("salutation", EMPTY);
		createCustomObjectParamMap.put("email", customerRequest.getEmail());
		createCustomObjectParamMap.put("phone", EMPTY);
		createCustomObjectParamMap.put("customerGroupName", customerGroupName);
		createCustomObjectParamMap.put("countryCode", customerRequest.getCountryCode());
		createCustomObjectParamMap.put("store", customerRequest.getStore());
		createCustomObjectParamMap.put(CHANNEL, null);
		Custom custom = RegistrationUtility.createCustomObj(false, createCustomObjectParamMap);
		customerDTO.setCustom(custom);
		String customerResponse = (String) registrationService.sendPostRequest(customerBaseUrl, token, customerDTO);
		logger.trace("Create Customer Response Data {}", customerResponse);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		if (jsonObject.get(STATUS_CODE) != null) {
			return RegistrationUtility.removeEmailFromErrorsMessage(jsonObject);
		}
		String customerId = jsonObject.getAsJsonObject(CUSTOMER).get("id").getAsString();
		Customer customer = new Customer();
		customer.setId(customerId.trim());
		String key = registrationHelperService.getAccountKeyById(accountId);
		AccountCustomObject accountCustomObject = (AccountCustomObject) registrationHelperService
				.getAccountCustomObject(key);
		AccountCustomObject updatedAccount = RegistrationUtility.getAccountCustomObject("b2b",
				accountCustomObject.getKey(), customer, accountCustomObject, null);
		registrationService.getAccountID(updatedAccount, token);

		// customer group and key assignment for quick registration
		String customerIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
				+ customerId;

		CustomerAction customerAction = new CustomerAction();
		customerAction.setVersion(jsonObject.getAsJsonObject(CUSTOMER).get(VERSION).getAsLong());
		List<CustomerAction.Actions> list = new ArrayList<>();

		// customer group
		CustomerAction.Actions.CustomerGroup customerGroupObj = new CustomerAction.Actions.CustomerGroup();
		customerGroupObj.setId(ctEnvProperties.getCustomerGroupId());
		customerGroupObj.setCustomerGroupName(customerGroupName);
		customerGroupObj.setTypeId("customer-group");
		CustomerAction.Actions actionsObj1 = new CustomerAction.Actions();
		actionsObj1.setAction("setCustomerGroup");
		actionsObj1.setCustomerGroup(customerGroupObj);
		list.add(actionsObj1);
		customerDTO.setCustomerGroup(customerGroupObj);
		// customer key
		CustomerAction.Actions actionsObj2 = new CustomerAction.Actions();
		actionsObj2.setAction("setKey");
		actionsObj2.setKey(customerId);
		list.add(actionsObj2);
		customerAction.setActions(list);
		// updating customer group and key in CT
		customerResponse = (String) registrationService.sendPostRequest(customerIdUrl, token, customerAction);
		// Cognito
		Long version = 0L;
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		if (customerJsonObject.get(STATUS_CODE) == null) {
			version = customerJsonObject.get(VERSION).getAsLong();
		}
		String channel = customerRequest.getChannel();
		if(null==channel || channel.isEmpty()) {
			channel = updatedAccount.getContainer().toUpperCase();
		}
		registrationHelperService.setCognitoResponseDetails(customerDTO, channel, password, accountId, token, customerId, version);

		// SendGrid Api call for Quick Registration Welcome mail
		SendGridModel sendGridModel = new SendGridModel();
		sendGridModel.setEmail(customerDTO.getEmail());
		sendGridModel.setFirstName(customerDTO.getFirstName());
		if (accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ").length > 1) {
			sendGridModel.setSalesRepFirstName(
					accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ")[0]);
			sendGridModel.setSalesRepLastName(
					accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ")[1]);
		} else {
			sendGridModel.setSalesRepFirstName(
					accountCustomObject.getValue().getSalesRepresentative().getName().trim().split(" ")[0]);
			sendGridModel.setSalesRepLastName(EMPTY);
		}
		sendGridModel.setSalesRepPhoneNumber(accountCustomObject.getValue().getSalesRepresentative().getPhone());
		sendGridModel.setSalesRepEmail(accountCustomObject.getValue().getSalesRepresentative().getEmail());
		sendGridModel.setSalesRepCalendyLink("Calendy Link");
		registrationBySalesRepConfirmationSender.sendMail(sendGridModel);
		jsonObject.get("customer").getAsJsonObject().remove(PASSW0RD);
		return jsonObject.toString();
	}

	/**
	 * generatePassword method is used to generate password.
	 * 
	 * @param length
	 * @return String
	 */
	public String generatePassword(Integer length) {
		String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
		String specialCharacters = "!@#$";
		String numbers = "1234567890";
		String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
		char[] password = new char[length];

		password[0] = lowerCaseLetters.charAt(RANDOM.nextInt(lowerCaseLetters.length()));
		password[1] = capitalCaseLetters.charAt(RANDOM.nextInt(capitalCaseLetters.length()));
		password[2] = specialCharacters.charAt(RANDOM.nextInt(specialCharacters.length()));
		password[3] = numbers.charAt(RANDOM.nextInt(numbers.length()));

		for (int i = 4; i < length; i++) {
			password[i] = combinedChars.charAt(RANDOM.nextInt(combinedChars.length()));
		}

		return new String(password);
	}

	public String updateCustomer(String customerId, AuroraUpdateCustomerRequestBean customerRequest) {
		CustomerResponseBean customerResponseBean = registrationHelperService.getCustomerById(customerId);
		CustomerDTO customerDTO = new CustomerDTO();
		String token = ctServerHelperService.getStringAccessToken();
		long version = customerResponseBean.getVersion();
		List<CustomerDTO.Action> actions = new ArrayList<>();
		if (customerRequest.getEmail() != null) {
			CustomerDTO.Action emailChange = new CustomerDTO.Action();
			emailChange.setActionName("changeEmail");
			emailChange.setEmail(customerRequest.getEmail());
			actions.add(emailChange);
		}
		if (customerRequest.getFirstName() != null) {
			CustomerDTO.Action firstNameChange = new CustomerDTO.Action();
			firstNameChange.setActionName("setFirstName");
			firstNameChange.setFirstName(customerRequest.getFirstName().toUpperCase());
			actions.add(firstNameChange);
		}
		if (customerRequest.getLastName() != null) {
			CustomerDTO.Action lastNameChange = new CustomerDTO.Action();
			lastNameChange.setActionName("setLastName");
			lastNameChange.setLastName(customerRequest.getLastName().toUpperCase());
			actions.add(lastNameChange);
		}
		if (customerRequest.getTitle() != null) {
			CustomerDTO.Action titleChange = new CustomerDTO.Action();
			titleChange.setActionName("setTitle");
			titleChange.setTitle(customerRequest.getTitle());
			actions.add(titleChange);
		}
		if (customerRequest.getPhone() != null) {
			CustomerDTO.Action phoneChange = new CustomerDTO.Action();
			phoneChange.setActionName(SET_CUSTOM_FIELD);
			phoneChange.setName(PHONE);
			phoneChange.setValue(customerRequest.getPhone());
			actions.add(phoneChange);
		}
		if (customerRequest.getAccountLocked() != null) {
			CustomerDTO.Action accountLockedChange = new CustomerDTO.Action();
			accountLockedChange.setActionName(SET_CUSTOM_FIELD);
			accountLockedChange.setName("accountLocked");
			accountLockedChange.setValue(customerRequest.getAccountLocked());
			actions.add(accountLockedChange);
		}
		if (customerRequest.getCognitoId() != null) {
			CustomerDTO.Action cognitoIdChange = new CustomerDTO.Action();
			cognitoIdChange.setActionName(SET_CUSTOM_FIELD);
			cognitoIdChange.setName("cognitoId");
			cognitoIdChange.setValue(customerRequest.getCognitoId());
			actions.add(cognitoIdChange);
		}
		customerDTO.setVersion(version);
		customerDTO.setActions(actions);
		String resposne = registrationService.updateCustomerId(customerId, customerDTO, token);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(resposne);
		if(jsonObject.get(STATUS_CODE) != null) {
			return resposne;
		}
		jsonObject.remove(PASSW0RD);
		return jsonObject.toString();
	}

	/**
	 * assignCustomerGroup method is used to assign CustomerGroup to Customer.
	 * 
	 * @param customerId
	 * @param customerGroupId
	 * @return Object of CustomerResponseBean
	 */

	public CustomerResponseBean assignCustomerGroup(String customerId, String customerGroupId) {
		CustomerGroupRequestBean customerGroupRequestBean = new CustomerGroupRequestBean();
		CustomerResponseBean customerResponseBean = registrationHelperService.getCustomerById(customerId);
		String token = ctServerHelperService.getStringAccessToken();
		CustomerGroupRequestBean.Action.CustomerGroup customerGroup = new CustomerGroupRequestBean.Action.CustomerGroup();
		CustomerGroupRequestBean.Action customerGroupAction = new CustomerGroupRequestBean.Action();
		List<CustomerGroupRequestBean.Action> actionList = new ArrayList<>();
		String customerGroupList = getListCustomerGroups();
		JsonObject customerGroupJsonObject = MolekuleUtility.parseJsonObject(customerGroupList);
		customerGroupJsonObject.get(RESULTS).getAsJsonArray().forEach(a -> {
			if (a.getAsJsonObject().get("id").getAsString().equals(customerGroupId)) {
				customerGroup.setId(a.getAsJsonObject().get("id").getAsString());
				customerGroup.setCustomerGroupName(a.getAsJsonObject().get("name").getAsString());
				customerGroup.setTypeId("customer-group");
			}
		});
		customerGroupAction.setActionName("setCustomerGroup");
		customerGroupAction.setCustomerGroup(customerGroup);
		actionList.add(customerGroupAction);
		customerGroupRequestBean.setVersion(customerResponseBean.getVersion());
		customerGroupRequestBean.setActions(actionList);
		String customerGroupResponse = registrationService.assignCustomerGroup(customerId, customerGroupRequestBean,
				token);
		logger.trace("Associated Customer Group Response Data {}", customerGroupResponse);
		customerResponseBean.setCustomerGroup(customerGroup);
		return customerResponseBean;
	}

	/**
	 * assignSalesrepToCompanyCategory method is used to assign sales rep to
	 * company.
	 * 
	 * @param categoryId
	 * @param salrsRepId
	 * @return Object of CategoryObjectBean
	 */
	public CategoryObjectBean assignSalesrepToCompanyCategory(String categoryId, String salesRepId) {
		CategoryObjectBean categoryObjectBean = new CategoryObjectBean();
		String ctCategoryResponse = auroraHelperService.getCompanyCategories();
		List<CategoryDto> categoryDtoList = new ArrayList<>();
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(ctCategoryResponse);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		JsonArray categoryListArray = valueObject.get(COMPANY_CATEGORIES).getAsJsonObject().get(COMPANY_CATEGORY)
				.getAsJsonArray();
		for (int iterator = 0; iterator < categoryListArray.size(); iterator++) {
			JsonObject categoryObject = categoryListArray.get(iterator).getAsJsonObject();
			CategoryDto categoryDto = new CategoryDto();
			categoryDto.setId(categoryObject.get("id").getAsString());
			categoryDto.setName(categoryObject.get("name").getAsString());
			if (categoryObject.get("id").getAsString().equals(categoryId)) {
				categoryDto.setSalesRepId(salesRepId);
				categoryObjectBean.setCategory(categoryDto);
			} else {
				categoryDto.setSalesRepId(categoryObject.get(SALES_REP_ID).getAsString());
			}
			categoryDtoList.add(categoryDto);
		}
		String container = jsonObject.get(CONTAINER).getAsString();
		String key = jsonObject.get("key").getAsString();
		int version = jsonObject.get(VERSION).getAsInt();
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		Value value = new Value();
		CompanyCategories companyCategories = new CompanyCategories();
		companyCategories.setCompanyCategory(categoryDtoList);
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setCompanyCategories(companyCategories);
			value.setId(jsonObject.get("id").getAsString());
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		accountCustomObject.setContainer(container);
		accountCustomObject.setKey(key);
		accountCustomObject.setVersion(version);
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		// updating sales rep id in company.
		registrationService.sendPostRequest(accountBaseUrl, ctServerHelperService.getStringAccessToken(),
				accountCustomObject);
		// Get SalesRep Details from CT server.
		String ctSalesRepResponse = auroraHelperService.getListOfSalesRep();
		List<SalesRepresentativeDto> salesRepresentativeDtoList = MolekuleUtility
				.getSalesRepresentativeDtoList(ctSalesRepResponse);
		salesRepresentativeDtoList = salesRepresentativeDtoList.stream().filter(data -> data.getId().equals(salesRepId))
				.collect(Collectors.toList());
		categoryObjectBean.setSalesRep(salesRepresentativeDtoList.get(0));
		//
		// update in custom-object with category
		String token = ctServerHelperService.getStringAccessToken();
		int limit = 500;
		int offset = 0;
		int count = 0;
		JsonArray resultArray = new JsonArray();
		for (int i = 0; i <= count; i++) {
			String categoryfilterBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ CUSTOM_OBJECTS + "?where=value(companyCategory(id=\"" + categoryId + "\"))" + "&limit=" + limit
			+ OFF_SET + offset;
			String accountCustomResponse = netConnectionHelper.sendGetWithoutBody(token, categoryfilterBaseUrl);
			JsonObject categeroyJsonObject = MolekuleUtility.parseJsonObject(accountCustomResponse);
			int totalRecords = categeroyJsonObject.get("total").getAsInt();
			resultArray.addAll(categeroyJsonObject.get(RESULTS).getAsJsonArray());
			if (count == 0) {
				count = (totalRecords / 500) + 1;
				offset = offset + limit + 1;
			} else {
				offset = offset + limit;
			}
		}
		List<AccountCustomObject> accountCustomObjectList = convertJsonArrayToEntity(resultArray);
		accountCustomObjectList.stream().forEach(obj -> {
			AccountCustomObject accountCustomObject1 = new AccountCustomObject();
			Object accountObject = registrationHelperService.getAccountCustomObject(obj.getKey());
			if (accountObject instanceof AccountCustomObject) {
				accountCustomObject1 = (AccountCustomObject) accountObject;
			}
			accountCustomObject1.setContainer(obj.getContainer());
			accountCustomObject1.setKey(obj.getKey());
			Value updatedValueObject = new Value();
			updatedValueObject.setCompanyCategory(categoryObjectBean.getCategory());
			updatedValueObject.setSalesRepresentative(categoryObjectBean.getSalesRep());
			accountCustomObject1.setValue(updatedValueObject);
			registrationService.getAccountID(accountCustomObject1, ctServerHelperService.getStringAccessToken());
		});
		return categoryObjectBean;
	}

	/**
	 * updateCustomObject method is used to update the custom object with updated
	 * tax exempt details
	 * 
	 * @param taxExemptReviewList
	 * @param token
	 * @param resultsObject
	 * @param valueObject
	 * @return ResponseEntity<Object> of tax exempt list.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ResponseEntity<Object> updateCustomObject(List<TaxExemptReview> taxExemptReviewList, String token,
			JsonObject resultsObject, JsonObject valueObject) {
		String accountCustomerObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/custom-objects?";
		String container = resultsObject.get(CONTAINER).getAsString();
		String key = resultsObject.get("key").getAsString();

		AccountCustomObject accountCustomObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		Value value = new Value();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setTaxExemptReview(taxExemptReviewList);
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured updateCustomObject ", e);
		}
		accountCustomObject.setContainer(container);
		accountCustomObject.setKey(key);
		accountCustomObject.setVersion(resultsObject.get(VERSION).getAsInt());

		String newAddedResponse = (String) registrationService.sendPostRequest(accountCustomerObjectUrl, token,
				accountCustomObject);
		JsonObject newAccountJsonObject = MolekuleUtility.parseJsonObject(newAddedResponse);
		if (newAccountJsonObject.get(STATUS_CODE) != null) {
			return new ResponseEntity(newAccountJsonObject,
					HttpStatus.valueOf(newAccountJsonObject.get(STATUS_CODE).getAsInt()));
		}
		return new ResponseEntity<>(taxExemptReviewList, HttpStatus.OK);
	}

	/**
	 * prepareTaxExemptReviewList method is used to frame the tax exempt detail from
	 * the json object.
	 * 
	 * @param taxExemptReviewList
	 * @param valueObject
	 */
	private void prepareTaxExemptReviewList(List<TaxExemptReview> taxExemptReviewList, JsonObject valueObject) {
		JsonArray taxExemptReviewArray = valueObject.getAsJsonArray(TAX_EXEMPT_REVIEW).getAsJsonArray();
		int len = taxExemptReviewArray.size();
		for (int i = 0; i < len; i++) {
			JsonObject taxExemptReviewObject = taxExemptReviewArray.get(i).getAsJsonObject();
			TaxExemptReview taxExemptReview = new TaxExemptReview();
			taxExemptReview.setCertificateId(taxExemptReviewObject.get("certificateId").getAsString());
			taxExemptReview.setState(taxExemptReviewObject.get(STATE).getAsString());
			taxExemptReview.setCountry(taxExemptReviewObject.get("country").getAsString());
			taxExemptReview.setExpiryDate(taxExemptReviewObject.get("expiryDate").getAsString());
			if(taxExemptReviewObject.has("taxCertificationId")) {
				taxExemptReview.setTaxCertificationId(taxExemptReviewObject.get("taxCertificationId").getAsString());
			}
			taxExemptReviewList.add(taxExemptReview);
		}
	}

	/**
	 * removeTaxExempReviews method is used to remove the tax exempt detail using
	 * certificate id.
	 * 
	 * @param accountId
	 * @param certificateId
	 * @return ResponseEntity<List<TaxExemptReview>>
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ResponseEntity<Object> removeTaxExemptReviews(String accountId, String certificateId) {
		List<TaxExemptReview> taxExemptReviewList = new ArrayList<>();
		String url = getCTCustomObjectsByAccountIdUrl(accountId);
		String token = ctServerHelperService.getStringAccessToken();
		String accountStringResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
		if (accountJsonObject.get(STATUS_CODE) != null) {
			return new ResponseEntity(accountJsonObject,
					HttpStatus.valueOf(accountJsonObject.get(STATUS_CODE).getAsInt()));
		}
		JsonArray resultsArray = accountJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		if (resultsArray.size() != 0) {
			JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
			if (resultsObject.getAsJsonObject(VALUE) != null) {
				JsonObject valueObject = resultsObject.getAsJsonObject(VALUE).getAsJsonObject();
				if (valueObject.getAsJsonArray(TAX_EXEMPT_REVIEW) != null) {
					prepareTaxExemptReviewList(taxExemptReviewList, valueObject);
				}
				taxExemptReviewList.removeIf(item -> item.getCertificateId().equals(certificateId));
				return updateCustomObject(taxExemptReviewList, token, resultsObject, valueObject);
			}
		}
		return new ResponseEntity(HttpStatus.BAD_REQUEST);
	}

	/**
	 * createCategory method is used to create new category in category list
	 * 
	 * @param categoryName
	 * @param salesRepId
	 * @return CategoryObjectBean
	 */
	public CategoryObjectBean createCategory(String categoryName, String salesRepId) {
		CategoryObjectBean categoryObjectBean = new CategoryObjectBean();
		List<CategoryDto> categoryDtoList = new ArrayList<>();
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(auroraHelperService.getCompanyCategories());
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		JsonArray categoryListArray  = valueObject.has(COMPANY_CATEGORIES)?valueObject.get(COMPANY_CATEGORIES).getAsJsonObject().get(COMPANY_CATEGORY).getAsJsonArray(): new JsonArray();
		categoryListArray.forEach(a-> {
			CategoryDto categoryDto = new CategoryDto();
			categoryDto.setId(a.getAsJsonObject().get("id").getAsString());
			categoryDto.setName(a.getAsJsonObject().get("name").getAsString());
			categoryDto.setSalesRepId(a.getAsJsonObject().get(SALES_REP_ID).getAsString());
			categoryDtoList.add(categoryDto);
		});
		// new category details
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setId(UUID.randomUUID().toString());
		categoryDto.setName(categoryName);
		categoryDto.setSalesRepId(salesRepId);
		categoryDtoList.add(categoryDto);
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		Value value = new Value();
		CompanyCategories companyCategories = new CompanyCategories();
		companyCategories.setCompanyCategory(categoryDtoList);
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setCompanyCategories(companyCategories);
			value.setId(jsonObject.get("id").getAsString());
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured createCategory", e);
		}
		accountCustomObject.setContainer(jsonObject.get(CONTAINER).getAsString());
		accountCustomObject.setKey(jsonObject.get("key").getAsString());
		accountCustomObject.setVersion(jsonObject.get(VERSION).getAsInt());
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		// updating category custom object in CT server.
		registrationService.sendPostRequest(accountBaseUrl, ctServerHelperService.getStringAccessToken(),
				accountCustomObject);
		// Get SalesRep Details from CT server.
		List<SalesRepresentativeDto> salesRepresentativeDtoList = MolekuleUtility
				.getSalesRepresentativeDtoList(auroraHelperService.getListOfSalesRep());
		salesRepresentativeDtoList = salesRepresentativeDtoList.stream().filter(data -> data.getId().equals(salesRepId))
				.collect(Collectors.toList());
		categoryObjectBean.setCategory(categoryDto);
		categoryObjectBean.setSalesRep(salesRepresentativeDtoList.get(0));
		return categoryObjectBean;
	}

	/**
	 * removeCompanyCategory method is used to remove the company category details
	 * from the company custom-object and to update the account custom-object with
	 * others category.
	 * 
	 * @param categoryId
	 * @return String - it contains the company category details after removal.
	 */
	public String removeCategoryId(String categoryId) {
		String token = ctServerHelperService.getStringAccessToken();
		String ctCategoryResponse = auroraHelperService.getCompanyCategories();
		JsonObject jsonAccountObj = MolekuleUtility.parseJsonObject(ctCategoryResponse);
		List<CategoryDto> newCategoryDtoList = new ArrayList<>();
		List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);
		newCategoryDtoList.addAll(categoryDtoList);
		// remove category id
		newCategoryDtoList.removeIf(categoryDto -> categoryDto.getId().equals(categoryId));
		// update category custom-object
		AccountCustomObject custom = new AccountCustomObject();
		String categoryCustomObjectKey = jsonAccountObj.get("key").getAsString();
		Object obj = registrationHelperService.getAccountCustomObject(categoryCustomObjectKey);
		if (obj instanceof AccountCustomObject) {
			custom = (AccountCustomObject) obj;
		} else if (obj instanceof String) {
			return (String) obj;
		}
		Integer version = jsonAccountObj.get(VERSION).getAsInt();
		custom.setContainer(jsonAccountObj.get(CONTAINER).getAsString());
		custom.setKey(categoryCustomObjectKey);
		custom.setVersion(version);
		Value value = new Value();
		CompanyCategories companyCategories = new CompanyCategories();
		companyCategories.setCompanyCategory(newCategoryDtoList);
		value.setCompanyCategories(companyCategories);
		custom.setValue(value);
		registrationService.getAccountID(custom, ctServerHelperService.getStringAccessToken());
		// update in custom-object with category
		int limit = 500;
		int offset = 0;
		int count = 0;
		JsonArray resultArray = new JsonArray();
		for (int i = 0; i <= count; i++) {
			String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
					+ "?where=value(companyCategory(id=\"" + categoryId + "\"))" + "&limit=" + limit + OFF_SET + offset;
			String accountCustomResponse = netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(accountCustomResponse);
			int totalRecords = jsonObject.get("total").getAsInt();
			resultArray.addAll(jsonObject.get(RESULTS).getAsJsonArray());
			if (count == 0) {
				count = (totalRecords / 500) + 1;
				offset = offset + limit + 1;
			} else {
				offset = offset + limit;
			}
		}
		List<AccountCustomObject> accountCustomObjectList = convertJsonArrayToEntity(resultArray);
		List<CategoryDto> othersCategoryList = categoryDtoList.stream()
				.filter(filter -> filter.getName().equals("Other")).collect(Collectors.toList());
		String salesRepId = auroraHelperService.getSalesRepValueFromCategoryList(categoryDtoList,
				othersCategoryList.get(0).getId());
		// Get SalesRep Details from CT server.
		String ctSalesRepResponse = auroraHelperService.getListOfSalesRep();
		List<SalesRepresentativeDto> salesRepresentativeDtoList = MolekuleUtility
				.getSalesRepresentativeDtoList(ctSalesRepResponse);
		SalesRepresentativeDto salesRespresentativeDto = auroraHelperService
				.getSalesRepDetails(salesRepresentativeDtoList, salesRepId);
		updateCustomObjectWithCategory(accountCustomObjectList, othersCategoryList, salesRespresentativeDto);
		return auroraHelperService.getCompanyCategories();
	}

	/**
	 * updateCustomObjectWithCategory method is used to update the category
	 * custom-object after removal.
	 * 
	 * @param filteredCustomObjectList
	 * @param othersCategoryList
	 * @param salesRespresentativeDto
	 */
	private void updateCustomObjectWithCategory(List<AccountCustomObject> filteredCustomObjectList,
			List<CategoryDto> othersCategoryList, SalesRepresentativeDto salesRespresentativeDto) {
		filteredCustomObjectList.stream().forEach(obj -> {
			AccountCustomObject accountCustomObject = new AccountCustomObject();
			Object accountObject = registrationHelperService.getAccountCustomObject(obj.getKey());
			if (accountObject instanceof AccountCustomObject) {
				accountCustomObject = (AccountCustomObject) accountObject;
			}
			accountCustomObject.setContainer(obj.getContainer());
			accountCustomObject.setKey(obj.getKey());
			Value updatedValueObject = new Value();
			updatedValueObject.setCompanyCategory(othersCategoryList.get(0));
			updatedValueObject.setSalesRepresentative(salesRespresentativeDto);
			accountCustomObject.setValue(updatedValueObject);
			registrationService.getAccountID(accountCustomObject, ctServerHelperService.getStringAccessToken());
		});
	}

	/**
	 * convertJsonArrayToEntity method is used to covert the jsonArray object to the
	 * account custom-object list
	 * 
	 * @param resultArray
	 * @return List<AccountCustomObject>
	 */
	private List<AccountCustomObject> convertJsonArrayToEntity(JsonArray resultArray) {
		List<AccountCustomObject> accountCustomObjectList = new ArrayList<>();
		for (int iterator = 0; iterator < resultArray.size(); iterator++) {
			JsonObject accountObject = resultArray.get(iterator).getAsJsonObject();
			AccountCustomObject accountCustomObject = new AccountCustomObject();
			accountCustomObject.setContainer(accountObject.get(CONTAINER).getAsString());
			accountCustomObject.setKey(accountObject.get("key").getAsString());
			if (accountObject.get(VALUE) != null) {
				JsonObject valueObject = accountObject.get(VALUE).getAsJsonObject();
				if (valueObject.get(COMPANY_CATEGORY) != null) {
					JsonObject companyCategoryObject = valueObject.get(COMPANY_CATEGORY).getAsJsonObject();
					Value accountValueObject = new Value();
					CategoryDto categoryDto = new CategoryDto();
					categoryDto.setId(companyCategoryObject.get("id").getAsString());
					categoryDto.setName(companyCategoryObject.get("name").getAsString());
					categoryDto.setSalesRepId(companyCategoryObject.get(SALES_REP_ID).getAsString());
					accountValueObject.setCompanyCategory(categoryDto);
					accountCustomObject.setValue(accountValueObject);
					accountCustomObjectList.add(accountCustomObject);
				}
			}
		}
		return accountCustomObjectList;
	}

	/**
	 * createSalesRepresentative method is used to create new SalesRepresentative.
	 * 
	 * @param salesRequestBean
	 * @return response -SalesRepsResponse
	 */
	public SalesRepsResponse createSalesRepresentative(SalesRepRequestBean salesRequestBean) {

		// 1.Get a List Of Sales Representatives From CT server
		String responseData = auroraHelperService.getListOfSalesRep();
		List<SalesRepresentativeDto> salesRepresentativeDtoList = new ArrayList<>(
				MolekuleUtility.getSalesRepresentativeDtoList(responseData));

		// 2.Create a New Sales Rep from AuroraSalesRepRequestBean
		SalesRepresentativeDto salesRepresentativeDto = new SalesRepresentativeDto();
		salesRepresentativeDto.setId(UUID.randomUUID().toString());
		salesRepresentativeDto.setName(salesRequestBean.getFirstName() + " " + salesRequestBean.getLastName());
		salesRepresentativeDto.setFirstName(salesRequestBean.getFirstName());
		salesRepresentativeDto.setLastName(salesRequestBean.getLastName());
		salesRepresentativeDto.setEmail(salesRequestBean.getEmail());
		salesRepresentativeDto.setPhone(salesRequestBean.getPhone());
		salesRepresentativeDto.setCalendlyLink(salesRequestBean.getCalendlyLink());
		salesRepresentativeDtoList.add(salesRepresentativeDto);

		// 3.updating Sales Representative custom object in CT server.
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(responseData);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		Value value = new Value();
		ObjectMapper mapper = new ObjectMapper();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setSalesRepresentativeList(salesRepresentativeDtoList);
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured CreateSalesRep", e);
		}
		accountCustomObject.setContainer(jsonObject.get(CONTAINER).getAsString());
		accountCustomObject.setKey(jsonObject.get("key").getAsString());
		accountCustomObject.setVersion(jsonObject.get(VERSION).getAsInt());

		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		// updating sales-rep custom object in CT server.
		registrationService.sendPostRequest(accountBaseUrl, ctServerHelperService.getStringAccessToken(),
				accountCustomObject);

		// updating sales-rep to category custom object in CT server.
		if (salesRequestBean.getCategoryId() != null) {
			salesRequestBean.getCategoryId()
			.forEach(categoryId -> assignSalesrepToCompanyCategory(categoryId, salesRepresentativeDto.getId()));
		}

		String ctCategoryResponse = auroraHelperService.getCompanyCategories();
		List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);
		SalesRepsResponse salesRepsResponse = new SalesRepsResponse();
		salesRepsResponse.setSalesRep(salesRepresentativeDto);
		List<CompanyCategory> companyCategories = new ArrayList<>();
		categoryDtoList.stream().filter(dto -> dto.getSalesRepId().equals(salesRepresentativeDto.getId()))
		.forEach(categoryDto -> {
			CompanyCategory companyCategory = new CompanyCategory();
			companyCategory.setId(categoryDto.getId());
			companyCategory.setName(categoryDto.getName());
			companyCategories.add(companyCategory);
		});
		salesRepsResponse.setCompanyCategories(companyCategories);

		return salesRepsResponse;
	}

	/**
	 * updateSalesRepresentative method is used to update a SalesRepresentative.
	 * 
	 * @param salesRequestBean
	 * @return response -String
	 */
	public SalesRepsResponse updateSalesRepresentative(String salesRepId, SalesRepRequestBean salesRequestBean) {

		SalesRepsResponse salesRepsResponse = new SalesRepsResponse();

		// 1.Get a List Of Sales Representatives From CT server
		String responseData = auroraHelperService.getListOfSalesRep();
		List<SalesRepresentativeDto> salesRepresentativeDtoList = new ArrayList<>(
				MolekuleUtility.getSalesRepresentativeDtoList(responseData));

		// 2.Update a Sales Rep from SalesRepRequestBean
		salesRepresentativeDtoList.stream().filter(salesRep -> salesRep.getId().equals(salesRepId))
		.forEach(salesRep -> {
			salesRep.setFirstName(salesRequestBean.getFirstName());
			salesRep.setLastName(salesRequestBean.getLastName());
			salesRep.setName(salesRequestBean.getFirstName() + " " + salesRequestBean.getLastName());
			salesRep.setEmail(salesRequestBean.getEmail());
			salesRep.setPhone(salesRequestBean.getPhone());
			salesRep.setCalendlyLink(salesRequestBean.getCalendlyLink());
			salesRepsResponse.setSalesRep(salesRep);

		});

		// 3.updating Sales Representative custom object in CT server.
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(responseData);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		Value value = new Value();
		ObjectMapper mapper = new ObjectMapper();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setSalesRepresentativeList(salesRepresentativeDtoList);
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured updateSalesRep", e);
		}
		accountCustomObject.setContainer(jsonObject.get(CONTAINER).getAsString());
		accountCustomObject.setKey(jsonObject.get("key").getAsString());
		accountCustomObject.setVersion(jsonObject.get(VERSION).getAsInt());

		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		// updating sales-rep custom object in CT server.
		registrationService.sendPostRequest(accountBaseUrl, ctServerHelperService.getStringAccessToken(),
				accountCustomObject);

		// updating sales-rep to category custom object in CT server.
		if (salesRequestBean.getCategoryId() != null) {
			salesRequestBean.getCategoryId()
			.forEach(categoryId -> assignSalesrepToCompanyCategory(categoryId, salesRepId));
		}

		String ctCategoryResponse = auroraHelperService.getCompanyCategories();
		List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);
		List<CompanyCategory> companyCategories = new ArrayList<>();
		categoryDtoList.stream().filter(dto -> dto.getSalesRepId().equals(salesRepId)).forEach(categoryDto -> {
			CompanyCategory companyCategory = new CompanyCategory();
			companyCategory.setId(categoryDto.getId());
			companyCategory.setName(categoryDto.getName());
			companyCategories.add(companyCategory);
		});
		salesRepsResponse.setCompanyCategories(companyCategories);
		return salesRepsResponse;
	}

	/**
	 * getAllSalesReps method is used to get the list of sales rep along with
	 * categories.
	 * 
	 * @return SalesRepresentativesBean- it contains list of SalesRepsResponse.
	 */
	public SalesRepresentativesBean getAllSalesReps(Integer limit, Integer offset) {
		SalesRepresentativesBean salesRepresentativesBean = new SalesRepresentativesBean();

		List<SalesRepsResponse> salesRepsResponseList = new ArrayList<>();
		String ctCategoryResponse = auroraHelperService.getCompanyCategories();
		List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);

		List<SalesRepresentativeDto> salesRepresentativeDtoList = new ArrayList<>(
				MolekuleUtility.getSalesRepresentativeDtoList(auroraHelperService.getListOfSalesRep()));
		salesRepresentativesBean.setTotal(salesRepresentativeDtoList.size());
		if(limit != null && offset != null) {
			salesRepresentativeDtoList = salesRepresentativeDtoList.stream().skip(offset).limit(limit)
					.collect(Collectors.toList());
		}
		salesRepresentativeDtoList.stream().forEach(salesRep -> {
			SalesRepsResponse salesRepsResponse = new SalesRepsResponse();
			salesRepsResponse.setSalesRep(salesRep);
			List<CompanyCategory> companyCategories = new ArrayList<>();
			categoryDtoList.stream().filter(category -> category.getSalesRepId().equals(salesRep.getId()))
			.forEach(category -> {
				CompanyCategory companyCategory = new CompanyCategory();
				companyCategory.setId(category.getId());
				companyCategory.setName(category.getName());
				companyCategories.add(companyCategory);
			});
			salesRepsResponse.setCompanyCategories(companyCategories);
			salesRepsResponseList.add(salesRepsResponse);
		});
		salesRepresentativesBean.setSalesReps(salesRepsResponseList);

		return salesRepresentativesBean;
	}
}
