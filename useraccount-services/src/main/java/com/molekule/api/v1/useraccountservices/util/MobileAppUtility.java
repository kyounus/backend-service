package com.molekule.api.v1.useraccountservices.util;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.model.mobileapp.Address;
import com.molekule.api.v1.commonframework.model.mobileapp.CustomerResponse;
import com.molekule.api.v1.commonframework.model.registration.AddressV2;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;

public class MobileAppUtility {

    private MobileAppUtility() {
    	
    }

	public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss Z");
	public static final Logger LOGGER = LoggerFactory.getLogger(MobileAppUtility.class);
	public static CustomerResponse setCustomerResponse(CustomerResponseBean customerResponseBean) {
		CustomerResponse customerResponse = new CustomerResponse();
		customerResponse.setId(customerResponseBean.getId());
		customerResponse.setCreated_at(customerResponseBean.getCreatedAt().format(FORMATTER));
		customerResponse.setUpdated_at(customerResponseBean.getLastModifiedAt().format(FORMATTER));
		customerResponse.setEmail(customerResponseBean.getEmail());
		customerResponse.setFirstname(customerResponseBean.getFirstName());
		customerResponse.setLastname(customerResponseBean.getLastName());
		return customerResponse;
	}
	
	public static CustomerResponse setAccountResponse(AccountCustomObject accountCustomObject,CustomerResponse customerResponse) {
		customerResponse.setDefault_billing(accountCustomObject.getValue().getDefaultBillingAddressId());
		customerResponse.setDefault_shipping(accountCustomObject.getValue().getDefaultShippingAddressId());
		List<Address> mobileAppAddressList = new ArrayList<>();
	    AddressV2 ctAddressList = accountCustomObject.getValue().getAddresses();
	    if(ctAddressList != null) {
	    	List<ShippingAddresses> shippingAddressesList = ctAddressList.getShippingAddresses();
	    	List<BillingAddresses> billingAddressesList = ctAddressList.getBillingAddresses();
	        ListIterator<ShippingAddresses> shippingListIterator =  shippingAddressesList.listIterator();
	        while(shippingListIterator.hasNext()) {
	        	ShippingAddresses shippingAddresses = shippingListIterator.next();
	        	Address address = new Address();
	        	address.setCity(shippingAddresses.getCity());
	        	address.setId(shippingAddresses.getShippingAddressId());
	        	List<String> streets = new ArrayList<>();
	        	streets.add(shippingAddresses.getStreetAddress1());
	        	streets.add(shippingAddresses.getStreetAddress2());
	        	address.setStreet(streets);
	        	address.setTelephone(shippingAddresses.getPhoneNumber());
	        	address.setPostcode(shippingAddresses.getPostalCode());
	        	address.setFirstname(shippingAddresses.getFirstName());
	        	address.setLastname(shippingAddresses.getLastName());
	        	boolean setDefaultShipping = (accountCustomObject.getValue().getDefaultShippingAddressId() != null && accountCustomObject.getValue().getDefaultShippingAddressId().equals(shippingAddresses.getShippingAddressId()));
	        	address.setDefault_shipping(setDefaultShipping);
	        	mobileAppAddressList.add(address);
	        }
	        ListIterator<BillingAddresses> billingListIterator =  billingAddressesList.listIterator();
	        while(billingListIterator.hasNext()) {
	        	BillingAddresses billingAddresses = billingListIterator.next();
	        	Address address = new Address();
	        	address.setCity(billingAddresses.getCity());
	        	address.setId(billingAddresses.getBillingAddressId());
	        	List<String> streets = new ArrayList<>();
	        	streets.add(billingAddresses.getStreetAddress1());
	        	streets.add(billingAddresses.getStreetAddress2());
	        	address.setStreet(streets);
	        	address.setTelephone(billingAddresses.getPhoneNumber());
	        	address.setPostcode(billingAddresses.getPostalCode());
	        	address.setFirstname(billingAddresses.getFirstName());
	        	address.setLastname(billingAddresses.getLastName());
	        	boolean setDefaultBillingValue =(accountCustomObject.getValue().getDefaultBillingAddressId() != null && accountCustomObject.getValue().getDefaultBillingAddressId().equals(billingAddresses.getBillingAddressId())) ;
	        	address.setDefault_billing(setDefaultBillingValue);
	        	mobileAppAddressList.add(address);
	        }
	    }
	    customerResponse.setAddresses(mobileAppAddressList);
		return customerResponse;
	}
	
	public static  String convertJsonToString(Object obj) {
		 ObjectMapper objectMapper = new ObjectMapper();
		 String data = null;
		 try {
			 data = objectMapper.writeValueAsString(obj);
		 }catch(Exception ex) {
			LOGGER.error("Error -",ex); 
		 }
		 return data;
	}
}
