package com.molekule.api.v1.useraccountservices.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.Base64;
import com.amazonaws.util.IOUtils;
import com.molekule.api.v1.commonframework.dto.registration.NonProfitDocumentProofDto;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.useraccountservices.configuration.UserAccountConfiguration;

/**
 * AmazonS3Service class is use to handle the S3 bucket related operation.
 * 
 *
 */
@Component
public class AmazonS3Service {
	@Autowired
	private AmazonS3 amazonS3;

	private static Pattern fileExtnPtrn = Pattern.compile("([0-9a-zA-Z_-]+(\\.(?i)(pdf|bmp|jpeg|jpg|tiff|docx))$)");

	@Autowired
	@Qualifier("userAccountConfiguration")
	UserAccountConfiguration userAccountConfiguration;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("taxExemptionConfirmationSender")
	TemplateMailRequestHelper taxExemptionConfirmationSender;

	private static final Logger logger = LoggerFactory.getLogger(AmazonS3Service.class);

	/**
	 * uploadFileToS3Bucket method is used to upload the files to S3 bucket.
	 * 
	 * @param multipartFileList
	 * @param enablePublicReadAccess
	 * @param customerId
	 * 
	 * @return String - list file names uploaded to s3 bucket.
	 */
	@Async
	public NonProfitDocumentProofDto uploadFileToS3Bucket(MultipartFile multipartFile, boolean enablePublicReadAccess,
			String customerId) {
		String orignalFileName = multipartFile.getOriginalFilename();
		if (!isValidFileName(orignalFileName)) {
			throw new CustomRunTimeException("Invalid File Name");
		}
		NonProfitDocumentProofDto nonProfitDocumentProofDto = new NonProfitDocumentProofDto();
		String fileName = customerId + "_" + orignalFileName;
		try {
			File file = new File(fileName);
			FileUtils.writeByteArrayToFile(file, Base64.encode(multipartFile.getBytes()));
			PutObjectRequest putObjectRequest = new PutObjectRequest(userAccountConfiguration.getAWSS3BucketName(),
					fileName, file);
			if (enablePublicReadAccess) {
				putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
			}
			this.amazonS3.putObject(putObjectRequest);
			// removing the file created in the server
			Files.deleteIfExists(Paths.get(file.getPath()));
		} catch (IOException | AmazonServiceException ex) {
			logger.error("error occured when upload  file '{}' to s3 bucket", fileName, ex);
		}
		String s3Url = amazonS3.getUrl(userAccountConfiguration.getAWSS3BucketName(), fileName).toString();
		String[] splitUrl = s3Url.split("https://");
		String url = splitUrl[1];
		nonProfitDocumentProofDto.setId(fileName);
		nonProfitDocumentProofDto.setLocation(url);
		// SendGrid Api call for tax exempt file upload confirmation
		SendGridModel sendGridModel = registrationHelperService.createEmailDynamicInfo(customerId);
		taxExemptionConfirmationSender.sendMail(sendGridModel);
		return nonProfitDocumentProofDto;
	}

	public static boolean isValidFileName(String userName) {
		return StringUtils.hasText(userName) && fileExtnPtrn.matcher(userName).matches();
	}

	/**
	 * deleteFileFromS3Bucket method is used to delete the file from S3 bucket.
	 * 
	 * @param fileName
	 * 
	 * @return String - name of the file delete in S3.
	 */
	@Async
	public ResponseEntity<Object> deleteFileFromS3Bucket(String fileName) {
		String response;
		try {
			List<String> s3bucketFileKeyList = new ArrayList<>();
			ObjectListing list = amazonS3.listObjects(userAccountConfiguration.getAWSS3BucketName(), fileName);
			List<S3ObjectSummary> summaries = list.getObjectSummaries();
			while (list.isTruncated()) {
				list = amazonS3.listNextBatchOfObjects(list);
				summaries.addAll(list.getObjectSummaries());
			}
			summaries.stream().forEach(key -> s3bucketFileKeyList.add(key.getKey()));
			if(!s3bucketFileKeyList.isEmpty()) {
				amazonS3.deleteObject(new DeleteObjectRequest(userAccountConfiguration.getAWSS3BucketName(), fileName));
				response = fileName + " removed successfully.";
			}else {
				return new ResponseEntity<>(new ErrorResponse(400, "File not found"),HttpStatus.BAD_REQUEST);
			}
		} catch (AmazonServiceException ex) {
			response = "file [" + fileName + "] failed to delete";
			logger.error("error occured when removing  the file Name {}{}", fileName, ex);
		}
		
		return new ResponseEntity<>(response,HttpStatus.OK);
	}

	/**
	 * getUploadedFilesByCustomerId method is used to list the file names which are
	 * uploaded on s3 bucket based on customer id.
	 * 
	 * @param customerId
	 * @return List<String> - return the file name of particular customer id.
	 */
	public List<NonProfitDocumentProofDto> getUploadedFilesByCustomerId(String customerId) {
		List<String> s3bucketFileKeyList = new ArrayList<>();
		List<NonProfitDocumentProofDto> nonProfitDocumentProofDtoList = new ArrayList<>();
		ObjectListing list = amazonS3.listObjects(userAccountConfiguration.getAWSS3BucketName(), customerId);
		List<S3ObjectSummary> summaries = list.getObjectSummaries();
		while (list.isTruncated()) {
			list = amazonS3.listNextBatchOfObjects(list);
			summaries.addAll(list.getObjectSummaries());
		}
		summaries.stream().forEach(key -> s3bucketFileKeyList.add(key.getKey()));
		s3bucketFileKeyList.stream().forEach(file -> {
			NonProfitDocumentProofDto nonProfitDocumentProofDto = new NonProfitDocumentProofDto();
			String s3Url = amazonS3.getUrl(userAccountConfiguration.getAWSS3BucketName(), file).toString();
			String[] splitUrl = s3Url.split("https://");
			String url = splitUrl[1];
			nonProfitDocumentProofDto.setId(file);
			nonProfitDocumentProofDto.setLocation(url);
			nonProfitDocumentProofDtoList.add(nonProfitDocumentProofDto);
		});
		return nonProfitDocumentProofDtoList;
	}

	@Async
	public byte[] downloadFile(final String keyName) {
		byte[] content = null;
		final S3Object s3Object = amazonS3.getObject(userAccountConfiguration.getAWSS3BucketName(), keyName);
		final S3ObjectInputStream stream = s3Object.getObjectContent();
		try {
			content = Base64.decode(IOUtils.toByteArray(stream));
			s3Object.close();
		} catch (final IOException ex) {
			logger.error("error occured when download the file", ex);
		}
		return content;
	}
}
