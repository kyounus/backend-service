package com.molekule.api.v1.useraccountservices.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ADDRESSES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMAIL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMPTY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ERRORS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIRST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.POSTAL_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STREET_ADDRESS_1;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STREET_ADDRESS_2;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.Custom;
import com.molekule.api.v1.commonframework.dto.registration.Customer;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerSignupResponseDTO;
import com.molekule.api.v1.commonframework.dto.registration.Fields;
import com.molekule.api.v1.commonframework.dto.registration.NonProfitDocumentProofDto;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Payments.TermsStatus;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;
import com.molekule.api.v1.commonframework.dto.registration.Type;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.model.registration.AdditionalFreightInfo;
import com.molekule.api.v1.commonframework.model.registration.AddressV2;
import com.molekule.api.v1.commonframework.model.registration.BankInformation;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.Business;
import com.molekule.api.v1.commonframework.model.registration.BusinessReferences;
import com.molekule.api.v1.commonframework.model.registration.CarrierOption;
import com.molekule.api.v1.commonframework.model.registration.CarrierOptionCustomObject;
import com.molekule.api.v1.commonframework.model.registration.CarrierType;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoBean;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;
import com.molekule.api.v1.commonframework.model.registration.MolekuleCarrier;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressResponse;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierRelationShip;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
/**
 * The class RegistrationUtility is used to convert data.
 */
public class RegistrationUtility {
	static Logger logger = LoggerFactory.getLogger(RegistrationUtility.class);

	private static final SecureRandom RANDOM = new SecureRandom();
	public enum DeliveryType {
		PARCEL, FREIGHT, BOTH
	}

	/**
	 * convertToJsonObject method is used to convert the data to jsonObject.
	 * 
	 * @param <T>
	 * 
	 * @param data
	 * @param classType
	 * @return Object
	 */
	public static <T> Object convertToJsonObject(String data, Class<T> classType) {
		ObjectMapper objectMapper = new ObjectMapper();
		Object dataObject = null;
		try {
			dataObject = objectMapper.readValue(data, classType);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured in convertToJsonObject -", e);
		}
		return dataObject;
	}

	/**
	 * convertObjectToJsonStr method is used to convert the data to jsonString.
	 * 
	 * @param obj
	 * @return String
	 */
	public static String convertObjectToJsonStr(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = null;
		try {
			jsonStr = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured in convertObjectToJsonStr - ", e);
		}
		return jsonStr;
	}

	/**
	 * createCustomObj method is used to create the custom object.
	 * 
	 * @param customerId
	 * @param accountId
	 * @param isAdmin
	 * @param salutation
	 * @param email
	 * @param phone
	 * @param customerGroupName
	 * @param countryCode 
	 * @param store 
	 * @param registeredChannel 
	 * @param string 
	 * @return Custom
	 */
	public static Custom createCustomObj(boolean isAdmin, Map<String, Object> createCustomObjectParamMap) {
		String typeId = createCustomObjectParamMap.get("typeId") !=null ? createCustomObjectParamMap.get("typeId").toString() : null;
		String accountId = createCustomObjectParamMap.get("accountId") !=null ? createCustomObjectParamMap.get("accountId").toString() : null;
		String salutation = createCustomObjectParamMap.get("salutation") !=null ? createCustomObjectParamMap.get("salutation").toString() : null;
		String email = createCustomObjectParamMap.get("email") !=null ? createCustomObjectParamMap.get("email").toString() : null;
		String phone = createCustomObjectParamMap.get("phone") !=null ? createCustomObjectParamMap.get("phone").toString() : null;
		String customerGroupName = createCustomObjectParamMap.get("customerGroupName") !=null ? createCustomObjectParamMap.get("customerGroupName").toString() : null;
		String countryCode = createCustomObjectParamMap.get("countryCode") !=null ? createCustomObjectParamMap.get("countryCode").toString() : null;
		String store = createCustomObjectParamMap.get("store") !=null ? createCustomObjectParamMap.get("store").toString() : null;
		String channel = createCustomObjectParamMap.get(CHANNEL) !=null ? createCustomObjectParamMap.get(CHANNEL).toString() : null;
		boolean termsOfService = (boolean) createCustomObjectParamMap.get("termsOfService");
		boolean marketingOffers= (boolean) createCustomObjectParamMap.get("marketingOffers");
		
		Custom custom = new Custom();
		Fields fields = new Fields();
		if(null != accountId) {
			fields.setAccountId(accountId);			
		} else {
			fields.setAccountId("  ");
		}
		fields.setAdmin(isAdmin);
		fields.setSalutation(salutation);
		fields.setPhone(phone);
		fields.setCustomerGroupName(customerGroupName);
		setCustomerStore(countryCode, store, fields);
		List<String> mailList = new ArrayList<>();
		mailList.add(email);
		fields.setInvoiceEmailAddress(mailList);
		if(StringUtils.hasText(channel)) {
		fields.setChannel(channel);
		}
		fields.setTermsOfService(termsOfService);
		fields.setMarketingOffers(marketingOffers);
		custom.setFields(fields);
		Type type = new Type();
		type.setId(typeId);
		custom.setType(type);
		return custom;
	}

	public static void setCustomerStore(String countryCode, String store, Fields fields) {
		if(!com.amazonaws.util.StringUtils.isNullOrEmpty(store)) {
			fields.setStore(store);
		} else if("CA".equals(countryCode)) {
			fields.setStore("MLK CA - Website");
		} else if("US".equals(countryCode)){
			fields.setStore("MLK US - Website");
		} else if ("GB".equals(countryCode)) {
			fields.setStore("MLK_UK");
		} else {
			fields.setStore("MLK_EU");
		}
	}

	/**
	 * getAccountCustomObject method is used to get the account custom object.
	 * 
	 * @param container
	 * @param key
	 * @param obj
	 * @param accountCustomObject
	 * @param customerId
	 * @return AccountCustomObject
	 */
	public static AccountCustomObject getAccountCustomObject(String container, String key, Object obj,
			AccountCustomObject accountCustomObject, String customerId) {
		accountCustomObject.setContainer(container);
		accountCustomObject.setKey(key);
		BillingAddresses billingAddresses = null;
		List<ShippingAddresses> shippingAddressesList = new ArrayList<>();
		List<BillingAddresses> billingAddressesList = new ArrayList<>();
		Value value = null;
		if (accountCustomObject.getValue() != null) {
			value = accountCustomObject.getValue();
		} else {
			value = new Value();
		}
		if (obj instanceof Customer) {
			Customer customer = (Customer) obj;
			List<String> employeeIds = value.getEmployeeIds();
			if (employeeIds == null) {
				employeeIds = new ArrayList<>();
			}
			employeeIds.add(customer.getId());
			value.setEmployeeIds(employeeIds);
		} else if (obj instanceof ShippingAddressRequestBean) {
			setShippingAddressRequestBeanValue(obj, accountCustomObject, billingAddresses, shippingAddressesList,
					billingAddressesList, value);
		} else if (obj instanceof CompanyInfoBean) {
			setCompanyInfoBeanValue(obj, value);
		} else if (obj instanceof CustomerDTO) {
			CustomerDTO customerDTO = (CustomerDTO) obj;
			value.setCompanyName(customerDTO.getCompanyName());
		} else if (obj instanceof PaymentRequestBean) {
			PaymentRequestBean paymentRequestBean = (PaymentRequestBean) obj;
			setPaymentOption(customerId, value, paymentRequestBean);
		} else if (obj instanceof CategoryDto) {
			CategoryDto categoryDto = (CategoryDto) obj;
			value.setCompanyCategory(categoryDto);
		} else if (obj instanceof SalesRepresentativeDto) {
			SalesRepresentativeDto salesRepresentativeDto = (SalesRepresentativeDto) obj;
			value.setSalesRepresentative(salesRepresentativeDto);
		}

		accountCustomObject.setValue(value);
		return accountCustomObject;

	}

	private static void setCompanyInfoBeanValue(Object obj, Value value) {
		CompanyInfoBean companyInfoBean = (CompanyInfoBean) obj;
		value.setNoOfEmployees(companyInfoBean.getNumberOfEmployees());
		value.setNoOfOfficeSpaces(companyInfoBean.getNumberOfOfficeSpaces());
		value.setCompanyAddress(companyInfoBean.getCompanyAddress());
		value.setTaxExemptFlag(companyInfoBean.isTaxExempt());
		if (StringUtils.hasText(companyInfoBean.getCompanyName()))
			value.setCompanyName(companyInfoBean.getCompanyName());
		if (companyInfoBean.getTaxCertificateList() != null) {
			List<String> taxCertificateList = new ArrayList<>();
			taxCertificateList.addAll(companyInfoBean.getTaxCertificateList());
			if (value.getTaxCertificateList() != null) {
				taxCertificateList.addAll(value.getTaxCertificateList());
			}

			value.setTaxCertificateList(taxCertificateList);
		}
	}

	private static void setShippingAddressRequestBeanValue(Object obj, AccountCustomObject accountCustomObject,
			BillingAddresses billingAddresses, List<ShippingAddresses> shippingAddressesList,
			List<BillingAddresses> billingAddressesList, Value value) {
		if (accountCustomObject.getValue() != null && accountCustomObject.getValue().getAddresses() != null) {
			shippingAddressesList = value.getAddresses().getShippingAddresses();
			billingAddressesList = value.getAddresses().getBillingAddresses();
		}
		ShippingAddressRequestBean shippingAddressRequestBean = (ShippingAddressRequestBean) obj;
		value.setId(shippingAddressRequestBean.getCustomerId());
		ShippingAddresses shippingAddresses = null;
		if (shippingAddressRequestBean.getShippingAddress() != null) {
			shippingAddresses = setShippingAddressValue(value, shippingAddressRequestBean);

		}
		if (shippingAddressRequestBean.isOwnCarrier() && shippingAddressRequestBean.getMolekuleCarrier() != null) {
			billingAddresses = processCarrierInformations(value, shippingAddressRequestBean);
		}
		if (billingAddresses != null) {
			AddressV2 address = new AddressV2();
			if (shippingAddresses != null) {
				shippingAddressesList.add(shippingAddresses);
			}
			billingAddressesList.add(billingAddresses);

			address.setShippingAddresses(shippingAddressesList);
			address.setBillingAddresses(billingAddressesList);

			value.setAddresses(address);
		} else {
			AddressV2 address = new AddressV2();
			if (shippingAddresses != null) {
				shippingAddressesList.add(shippingAddresses);
			}
			address.setShippingAddresses(shippingAddressesList);

			value.setAddresses(address);
		}
	}

	private static BillingAddresses processCarrierInformations(Value value,
			ShippingAddressRequestBean shippingAddressRequestBean) {
		BillingAddresses billingAddresses;
		List<MolekuleCarrier> shippingMethods = value.getShippingMethods();
		if (CollectionUtils.isEmpty(shippingMethods)) {
			shippingMethods = new ArrayList<>();
		}
		MolekuleCarrier molekuleCarrier = new MolekuleCarrier();
		molekuleCarrier.setCarrierName(shippingAddressRequestBean.getMolekuleCarrier().getCarrierName());
		molekuleCarrier.setCarrierAccountNumber(
				shippingAddressRequestBean.getMolekuleCarrier().getCarrierAccountNumber());
		molekuleCarrier.setDeliveryType(shippingAddressRequestBean.getMolekuleCarrier().getDeliveryType());
		shippingMethods.add(molekuleCarrier);
		value.setShippingMethods(shippingMethods);

		billingAddresses = new BillingAddresses();
		billingAddresses.setBillingAddressId(UUID.randomUUID().toString());
		billingAddresses.setFirstName(
				shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getFirstName());
		billingAddresses
		.setLastName(shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getLastName());
		billingAddresses.setCity(shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getCity());
		billingAddresses.setStreetAddress1(
				shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getStreetAddress1());
		billingAddresses.setStreetAddress2(
				shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getStreetAddress2());
		billingAddresses
		.setState(shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getState());
		billingAddresses.setPhoneNumber(
				shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getPhoneNumber());
		billingAddresses.setPostalCode(
				shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getPostalCode());
		billingAddresses
		.setCountry(shippingAddressRequestBean.getMolekuleCarrier().getBillingAddress().getCountry());
		if (DeliveryType.PARCEL.name()
				.equals(shippingAddressRequestBean.getMolekuleCarrier().getDeliveryType())) {
			value.setParcelFlag(true);
		} else if (DeliveryType.FREIGHT.name()
				.equals(shippingAddressRequestBean.getMolekuleCarrier().getDeliveryType())) {
			value.setFreightFlag(true);
		} else if (DeliveryType.BOTH.name()
				.equals(shippingAddressRequestBean.getMolekuleCarrier().getDeliveryType())) {
			value.setParcelFlag(true);
			value.setFreightFlag(true);
		}
		return billingAddresses;
	}

	private static ShippingAddresses setShippingAddressValue(Value value,
			ShippingAddressRequestBean shippingAddressRequestBean) {
		ShippingAddresses shippingAddresses;
		shippingAddresses = new ShippingAddresses();
		String shippingAddressId = UUID.randomUUID().toString();
		shippingAddresses.setShippingAddressId(shippingAddressId);
		shippingAddresses.setFirstName(shippingAddressRequestBean.getShippingAddress().getFirstName());
		shippingAddresses.setLastName(shippingAddressRequestBean.getShippingAddress().getLastName());
		shippingAddresses
		.setStreetAddress1(shippingAddressRequestBean.getShippingAddress().getStreetAddress1());
		shippingAddresses
		.setStreetAddress2(shippingAddressRequestBean.getShippingAddress().getStreetAddress2());
		shippingAddresses.setCity(shippingAddressRequestBean.getShippingAddress().getCity());
		shippingAddresses.setState(shippingAddressRequestBean.getShippingAddress().getState());
		shippingAddresses.setPostalCode(shippingAddressRequestBean.getShippingAddress().getPostalCode());
		shippingAddresses.setPhoneNumber(shippingAddressRequestBean.getShippingAddress().getPhoneNumber());
		shippingAddresses.setCountry(shippingAddressRequestBean.getShippingAddress().getCountry());
		shippingAddresses.setCompanyName(shippingAddressRequestBean.getShippingAddress().getCompanyName());
		if (Boolean.TRUE.equals(shippingAddressRequestBean.isPreferredShippingAddress())) {
			value.setDefaultShippingAddressId(shippingAddressId);
		}
		return shippingAddresses;
	}

	/**
	 * setPaymentOption method is used to set paymet details in value object
	 * @param customerId
	 * @param value
	 * @param paymentRequestBean
	 */
	private static void setPaymentOption(String customerId, Value value, PaymentRequestBean paymentRequestBean) {
		Payments payments = new Payments();
		List<Payments> paymentList = new ArrayList<>();
		List<Payments> oldPaymentList = value.getPayments();
		payments.setCustomerId(customerId);
		if (PaymentRequestBean.Payment.CREDITCARD.toString().equals(paymentRequestBean.getPaymentPreferences())) {
			payments.setPaymentId(UUID.randomUUID().toString());
			payments.setPaymentType(paymentRequestBean.getPaymentPreferences());
			payments.setPaymentToken(paymentRequestBean.getPaymentToken());
			payments.setCustomerId(customerId);
			setBillingAddress(paymentRequestBean, value, payments);
		} else if (PaymentRequestBean.Payment.ACH.toString().equals(paymentRequestBean.getPaymentPreferences())) {
			payments.setPaymentId(UUID.randomUUID().toString());
			payments.setPaymentType(paymentRequestBean.getPaymentPreferences());
			setBillingAddress(paymentRequestBean, value, payments);
		} else if (PaymentRequestBean.Payment.PAYMENTTERMS.toString()
				.equals(paymentRequestBean.getPaymentPreferences())) {
			payments.setPaymentId(UUID.randomUUID().toString());
			payments.setPaymentType(paymentRequestBean.getPaymentPreferences());
			setBillingAddress(paymentRequestBean, value, payments);
			Business business = new Business();
			business.setBusinessType(paymentRequestBean.getBusinessType());

			if (PaymentRequestBean.BusinessType.PUBLIC.toString().equals(paymentRequestBean.getBusinessType())) {
				// Stock ticker and exchange
				business.setStockTicker(paymentRequestBean.getStockTickerName());
				business.setStockExchange(paymentRequestBean.getStockExchange());
			} else if (PaymentRequestBean.BusinessType.PRIVATE.toString()
					.equals(paymentRequestBean.getBusinessType())) {
				// Business Reference
				Business.References references = new Business.References();
				BusinessReferences refObjectOne = new BusinessReferences();
				BusinessReferences refObjectTwo = new BusinessReferences();
				refObjectOne.setFirstName(paymentRequestBean.getBusinessReferences1().getFirstName());
				refObjectOne.setLastName(paymentRequestBean.getBusinessReferences1().getLastName());
				refObjectOne.setEmail(paymentRequestBean.getBusinessReferences1().getEmail());
				refObjectOne.setPhoneNumber(paymentRequestBean.getBusinessReferences1().getPhoneNumber());
				refObjectTwo.setFirstName(paymentRequestBean.getBusinessReferences2().getFirstName());
				refObjectTwo.setLastName(paymentRequestBean.getBusinessReferences2().getLastName());
				refObjectTwo.setEmail(paymentRequestBean.getBusinessReferences2().getEmail());
				refObjectTwo.setPhoneNumber(paymentRequestBean.getBusinessReferences2().getPhoneNumber());
				references.setFirstReference(refObjectOne);
				references.setSecondReference(refObjectTwo);
				business.setReferences(references);

				// Bank Information
				BankInformation bankInformation = new BankInformation();
				bankInformation.setName(paymentRequestBean.getBankInformation().getName());
				bankInformation.setStreetAddress1(paymentRequestBean.getBankInformation().getStreetAddress1());
				bankInformation.setStreetAddress2(paymentRequestBean.getBankInformation().getStreetAddress2());
				bankInformation.setCity(paymentRequestBean.getBankInformation().getCity());
				bankInformation.setState(paymentRequestBean.getBankInformation().getState());
				bankInformation.setPostalCode(paymentRequestBean.getBankInformation().getPostalCode());
				bankInformation.setCountry(paymentRequestBean.getBankInformation().getCountry());
				bankInformation.setPhoneNumber(paymentRequestBean.getBankInformation().getPhoneNumber());
				business.setBankInformation(bankInformation);

			} else if (PaymentRequestBean.BusinessType.GOVERNMENT.toString()
					.equals(paymentRequestBean.getBusinessType())) {
				// Government Verfication
				business.setEmail(paymentRequestBean.getGovtVerification().getGovernmentEmail());
				business.setPhoneNumber(paymentRequestBean.getGovtVerification().getDeptPhoneNumber());

			} else if (PaymentRequestBean.BusinessType.NONPROFIT.toString()
					.equals(paymentRequestBean.getBusinessType())) {
				// NonProfit
				business.setDocumentProof(paymentRequestBean.getDocumentProof());
			}
			payments.setTermsStatus(TermsStatus.PENDINGREVIEW);
			payments.setBusiness(business);
		}
		if (paymentRequestBean.isDefaultPayment()) {
			value.setDefaultPaymentId(payments.getPaymentId());
			value.setDefaultBillingAddressId(payments.getBillingAddressId());
		}
		paymentList.add(payments);
		if (oldPaymentList != null)
			paymentList.addAll(oldPaymentList);
		value.setPayments(paymentList);
	}

	/**
	 * 
	 * Populated the shipping option values.
	 * 
	 * @param container
	 * @param key
	 * @param data
	 * @param accountCustomObject
	 * @return
	 */

	public static ShippingAddressResponse populateShippingOptions(String container, String key,
			ShippingCarrierOptionsBean data, AccountCustomObject accountCustomObject) {

		ShippingAddressResponse shippingAddressResponse = new ShippingAddressResponse();
		accountCustomObject.setContainer(container);
		accountCustomObject.setKey(key);
		Value value = null;
		if (accountCustomObject.getValue() != null) {
			value = accountCustomObject.getValue();
		} else {
			value = new Value();
		}

		String shippingAddressId = null;
		if (null != data.getShippingAddress()) {
			AddressV2 addresses = Optional.ofNullable(value.getAddresses()).orElse(new AddressV2());
			List<ShippingAddresses> shippingAddresses = Optional.ofNullable(addresses.getShippingAddresses())
					.orElse(new ArrayList<>());
			ShippingAddresses shippingAddress = data.getShippingAddress();
			shippingAddressId = UUID.randomUUID().toString();
			shippingAddress.setShippingAddressId(shippingAddressId);
			shippingAddresses.add(shippingAddress);
			addresses.setShippingAddresses(shippingAddresses);
			value.setAddresses(addresses);
			shippingAddressResponse.setShippingAddresses(shippingAddress);
		}
		if (data.isPrimaryShipping())
			value.setDefaultShippingAddressId(shippingAddressId);

		if (null != data.getAdditionalFreightInfo()) {
			List<AdditionalFreightInfo> additionalFreightInfos = Optional.ofNullable(value.getAdditionalFreightInfos())
					.orElse(new ArrayList<>());
			AdditionalFreightInfo additionalFreightInfo = data.getAdditionalFreightInfo();
			additionalFreightInfo.setShippingAddressId(shippingAddressId);
			additionalFreightInfos.add(additionalFreightInfo);
			value.setAdditionalFreightInfos(additionalFreightInfos);
			value.setFreightFlag(true);
			shippingAddressResponse.setAdditionalFreightInfo(additionalFreightInfo);
		}

		if (null != data.getShippingAddress() && !CollectionUtils.isEmpty(data.getCarriers())) {
			List<String> carrierIds = populateCarrierOptions(data.getCarriers(), value);
			ShippingCarrierRelationShip shippingCarrierRelationShip = new ShippingCarrierRelationShip();
			shippingCarrierRelationShip.setShippingAddressId(shippingAddressId);
			shippingCarrierRelationShip.setCarrierIds(carrierIds);

			List<ShippingCarrierRelationShip> shippingCarrierRelationShips = Optional
					.ofNullable(value.getShippingCarrierRelationShip()).orElse(new ArrayList<>());
			shippingCarrierRelationShips.add(shippingCarrierRelationShip);
			value.setShippingCarrierRelationShip(shippingCarrierRelationShips);
		}
		accountCustomObject.setValue(value);
		return shippingAddressResponse;

	}

	/**
	 * Populated the carrier options values
	 * 
	 * @param container
	 * @param key
	 * @param data
	 * @param accountCustomObject
	 * @return
	 */

	public static AccountCustomObject populateCarrierOptions(String container, String key, List<CarrierOption> data,
			AccountCustomObject accountCustomObject) {

		accountCustomObject.setContainer(container);
		accountCustomObject.setKey(key);
		Value value = null;
		if (accountCustomObject.getValue() != null) {
			value = accountCustomObject.getValue();
		} else {
			value = new Value();
		}

		populateCarrierOptions(data, value);

		accountCustomObject.setValue(value);
		return accountCustomObject;

	}

	/**
	 * To update the shipping options.
	 * 
	 * @param shippingAddressId
	 * @param shippingCarrierOptionsBean
	 * @param accountCustomObject
	 * @return
	 */

	public static AccountCustomObject updateShippingOptions(String shippingAddressId,
			ShippingCarrierOptionsBean shippingCarrierOptionsBean, AccountCustomObject accountCustomObject) {

		ShippingAddresses toUpdateShippingAddress = shippingCarrierOptionsBean.getShippingAddress();

		Value value = accountCustomObject.getValue();

		AddressV2 addresses = value!=null?value.getAddresses():null;

		if (addresses != null && addresses.getShippingAddresses() != null) {

			ShippingAddresses existingAddress = addresses.getShippingAddresses().stream()
					.filter(s -> shippingAddressId.equals(s.getShippingAddressId())).findAny().orElse(null);

			if (existingAddress != null) {

				processExistingAddress(shippingAddressId, shippingCarrierOptionsBean, toUpdateShippingAddress, value,
						existingAddress);

			}

			if (Boolean.FALSE.equals(shippingCarrierOptionsBean.isAcceptFreight())
					&& value.getAdditionalFreightInfos() != null) {
				value.getAdditionalFreightInfos().removeIf(s -> shippingAddressId.equals(s.getShippingAddressId()));
			}

			if (null != shippingCarrierOptionsBean.getAdditionalFreightInfo()) {
				List<AdditionalFreightInfo> additionalFreightInfos = Optional
						.ofNullable(value.getAdditionalFreightInfos()).orElse(new ArrayList<>());
				AdditionalFreightInfo additionalFreightInfo = additionalFreightInfos.stream()
						.filter(a -> a.getShippingAddressId().equals(shippingAddressId)).findAny().orElse(null);
				if (null == additionalFreightInfo) {
					additionalFreightInfo = new AdditionalFreightInfo();
					additionalFreightInfos.add(additionalFreightInfo);
					value.setAdditionalFreightInfos(additionalFreightInfos);
					value.setFreightFlag(true);
				}
				BeanUtils.copyProperties(shippingCarrierOptionsBean.getAdditionalFreightInfo(), additionalFreightInfo);
				additionalFreightInfo.setShippingAddressId(shippingAddressId);
			}

		}
		accountCustomObject.setValue(value);

		return accountCustomObject;

	}

	private static void processExistingAddress(String shippingAddressId,
			ShippingCarrierOptionsBean shippingCarrierOptionsBean, ShippingAddresses toUpdateShippingAddress,
			Value value, ShippingAddresses existingAddress) {
		if (toUpdateShippingAddress != null) {
			BeanUtils.copyProperties(toUpdateShippingAddress, existingAddress);
			existingAddress.setShippingAddressId(shippingAddressId);

		}
		/* To Update the Default Shipping Value */

		if (Boolean.TRUE.equals(shippingCarrierOptionsBean.isPrimaryShipping())) {
			value.setDefaultShippingAddressId(shippingAddressId);
		} else if (shippingAddressId.equals(value.getDefaultShippingAddressId())) {
			value.setDefaultShippingAddressId(null);
		}
	}

	/**
	 * To update the carrier options.
	 * 
	 * @param carrierId
	 * @param toUpdateCarrierOptionValue
	 * @param accountCustomObject
	 * @return
	 */

	public static AccountCustomObject updateCarrierOptions(String carrierId, CarrierOption toUpdateCarrierOptionValue,
			AccountCustomObject accountCustomObject) {

		Value value = accountCustomObject.getValue();

		List<CarrierOptionCustomObject> existingCarrierOptionCustomObjects = value.getCarriers();

		if (existingCarrierOptionCustomObjects != null) {

			/* Select the required Custom Object to update */

			CarrierOptionCustomObject existingCarrierOptionCustomObject = existingCarrierOptionCustomObjects.stream()
					.filter(s1 -> carrierId.equals(s1.getCarrierId())).findAny().orElse(null);

			if (existingCarrierOptionCustomObject != null) {

				toUpdateCarrierOptionValue.setBillingAddressId(existingCarrierOptionCustomObject.getBillingAddressId());

				/*
				 * In the request "defaultCarrier" value as true then setDefaultCarrierId.
				 */

				if (Boolean.TRUE.equals(toUpdateCarrierOptionValue.isDefaultCarrier())) {
					value.setDefaultCarrierId(carrierId);
				} else if (carrierId.equals(value.getDefaultCarrierId())) {
					value.setDefaultCarrierId(null);
				}

				// Update the Carrier options value to existing custom object.
				BeanUtils.copyProperties(toUpdateCarrierOptionValue, existingCarrierOptionCustomObject);

				/* Update the Billing Address value */
				copyBillingAddressValues(toUpdateCarrierOptionValue.getBillingAddress(), value,
						existingCarrierOptionCustomObject);

			}
		}

		accountCustomObject.setValue(value);

		return accountCustomObject;

	}

	/**
	 * To copy the billing address
	 * 
	 * @param toUpdateBillingAddress
	 * @param value
	 * @param carrierOptionCustomObject
	 */
	private static void copyBillingAddressValues(BillingAddresses toUpdateBillingAddress, Value value,
			CarrierOptionCustomObject carrierOptionCustomObject) {

		if (value.getAddresses() == null || value.getAddresses().getBillingAddresses() == null
				|| carrierOptionCustomObject.getBillingAddressId() == null || toUpdateBillingAddress == null) {
			return;
		}

		BillingAddresses existingBillingAddress = value.getAddresses().getBillingAddresses().stream()
				.filter(s -> carrierOptionCustomObject.getBillingAddressId().equals(s.getBillingAddressId())).findAny()
				.orElse(null);

		if (existingBillingAddress != null) {
			BeanUtils.copyProperties(toUpdateBillingAddress, existingBillingAddress);
			existingBillingAddress.setBillingAddressId(carrierOptionCustomObject.getBillingAddressId());
		}

	}

	/**
	 * Populated carrier options.
	 * 
	 * @param carrierOptions
	 * @param value
	 * @return
	 */

	private static List<String> populateCarrierOptions(List<CarrierOption> carrierOptions, Value value) {
		List<String> carrierIds = new ArrayList<>();

		if (CollectionUtils.isEmpty(carrierOptions)) {
			return carrierIds;
		}
		List<CarrierOptionCustomObject> carriers = new ArrayList<>();
		boolean isCarrierTypeBoth = carrierOptions.size() > 1;

		for (CarrierOption carrierOption : carrierOptions) {
			CarrierOptionCustomObject carrierOptionCustomObject = new CarrierOptionCustomObject();
			BeanUtils.copyProperties(carrierOption, carrierOptionCustomObject);
			String billingAddressId = null;
			if (null != carrierOption.getBillingAddress()) {
				AddressV2 addresses = Optional.ofNullable(value.getAddresses()).orElse(new AddressV2());
				List<BillingAddresses> billingAddresses = Optional.ofNullable(addresses.getBillingAddresses())
						.orElse(new ArrayList<>());
				BillingAddresses billingAddress = carrierOption.getBillingAddress();
				billingAddressId = UUID.randomUUID().toString();
				billingAddress.setBillingAddressId(billingAddressId);
				billingAddresses.add(billingAddress);
				addresses.setBillingAddresses(billingAddresses);
				value.setAddresses(addresses);
			} else {
				billingAddressId = carrierOption.getBillingAddressId();
			}
			carrierOptionCustomObject.setBillingAddressId(billingAddressId);
			carrierOptionCustomObject.setCarrierId(UUID.randomUUID().toString());
			if (carrierOption.isDefaultCarrier()) {
				value.setDefaultCarrierId(carrierOptionCustomObject.getCarrierId());
			}
			if (carrierOption.getType().equals(CarrierType.PARCEL)) {
				value.setParcelFlag(true);
			} else if (carrierOption.getType().equals(CarrierType.FREIGHT)) {
				value.setFreightFlag(true);
			}
			value.setMyOwnCarrier(true);
			carrierIds.add(carrierOptionCustomObject.getCarrierId());
			carriers.add(carrierOptionCustomObject);
		}

		if (isCarrierTypeBoth) {
			CarrierOptionCustomObject carrierOptionCustomObject = new CarrierOptionCustomObject();
			carrierOptionCustomObject.setCarrierId(UUID.randomUUID().toString());
			carrierOptionCustomObject.setType(CarrierType.BOTH);
			carrierOptionCustomObject.setParcelAndFrieghtIds(carrierIds);
			carriers.add(carrierOptionCustomObject);
			carrierIds.clear();
			carrierIds.add(carrierOptionCustomObject.getCarrierId());
		}

		List<CarrierOptionCustomObject> carrierOptionCustomObjects = Optional.ofNullable(value.getCarriers())
				.orElse(new ArrayList<>());
		carrierOptionCustomObjects.addAll(carriers);
		value.setCarriers(carrierOptionCustomObjects);
		return carrierIds;
	}

	/**
	 * setBillingAddress method is used to set the new billing address.
	 * 
	 * @param paymentRequestBean
	 * @param value
	 * @param payments
	 */
	private static void setBillingAddress(PaymentRequestBean paymentRequestBean, Value value, Payments payments) {
		List<BillingAddresses> oldBillingAddress = new ArrayList<>();
		if (value.getAddresses() != null && value.getAddresses().getBillingAddresses() != null) {
			oldBillingAddress = value.getAddresses().getBillingAddresses();
		}
		BillingAddresses billingAddresses = new BillingAddresses();
		String billingAddressId = UUID.randomUUID().toString();
		BillingAddresses billingAddressRequestBean = paymentRequestBean.getBillingAddress();
		billingAddresses.setBillingAddressId(billingAddressId);
		billingAddresses.setFirstName(billingAddressRequestBean.getFirstName());
		billingAddresses.setLastName(billingAddressRequestBean.getLastName());
		billingAddresses.setStreetAddress1(billingAddressRequestBean.getStreetAddress1());
		billingAddresses.setStreetAddress2(billingAddressRequestBean.getStreetAddress2());
		billingAddresses.setCity(billingAddressRequestBean.getCity());
		billingAddresses.setState(billingAddressRequestBean.getState());
		billingAddresses.setPostalCode(billingAddressRequestBean.getPostalCode());
		billingAddresses.setPhoneNumber(billingAddressRequestBean.getPhoneNumber());
		billingAddresses.setCountry(billingAddressRequestBean.getCountry());
		oldBillingAddress.add(billingAddresses);
		AddressV2 address = new AddressV2();
		if (value.getAddresses() != null && value.getAddresses().getShippingAddresses() != null) {
			address.setShippingAddresses(value.getAddresses().getShippingAddresses());
		}
		address.setBillingAddresses(oldBillingAddress);
		payments.setBillingAddressId(billingAddressId);
		value.setAddresses(address);
	}

	/**
	 * getCustomerResponse method is used to get the customer response.
	 * 
	 * @param customerId
	 * @param accountId
	 * @param accountVersion
	 * @param customerDTO
	 * @return CustomerSignupResponseDTO
	 */
	public static CustomerSignupResponseDTO getCustomerResponse(String customerId, String accountId,
			CustomerDTO customerDTO) {
		CustomerSignupResponseDTO cuResponseDTO = new CustomerSignupResponseDTO();
		cuResponseDTO.setCustomerId(customerId);
		cuResponseDTO.setAccountId(accountId);
		cuResponseDTO.setFirstName(customerDTO.getFirstName());
		cuResponseDTO.setLastName(customerDTO.getLastName());
		cuResponseDTO.setEmail(customerDTO.getEmail());
		cuResponseDTO.setCustomerNumber(customerDTO.getCustomerNumber());
		cuResponseDTO.setCustomerGroup(customerDTO.getCustomerGroup());
		return cuResponseDTO;
	}

	/**
	 * generateKey method is used to generate the key for account custom object.
	 * 
	 * @param companyName
	 * @param name
	 * @return String
	 */
	public static String generateKey(String companyName, String name) {
		int randomValue = RANDOM.nextInt(1000);
		StringBuilder randomKey = new StringBuilder(companyName.replaceAll("[^a-zA-Z0-9]", EMPTY)).append(randomValue)
				.append(name.replaceAll("\\s", EMPTY));
		return randomKey.toString();
	}

	public static String getCustomerNumber() {
		int number = RANDOM.nextInt(999999);
		return String.format("%06d", number);
	}

	public static Customer createCustomerObj(String respString) {
		Customer customer = new Customer();
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(respString);
		customer.setId(jsonObject.getAsJsonObject(CUSTOMER).get("id").getAsString());
		customer.setEmail(jsonObject.getAsJsonObject(CUSTOMER).get(EMAIL).getAsString());
		customer.setFirstName(jsonObject.getAsJsonObject(CUSTOMER).get(FIRST_NAME).getAsString());
		customer.setLastName(jsonObject.getAsJsonObject(CUSTOMER).get(LAST_NAME).getAsString());
		customer.setPassword(jsonObject.getAsJsonObject(CUSTOMER).get("password").getAsString());
		customer.setCustomerNumber(jsonObject.getAsJsonObject(CUSTOMER).get("customerNumber").getAsString());
		return customer;
	}

	public static String createOrderNumber() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
		return sdf.format(new Date());
	}

	/**
	 * updatePaymentsObject method is used to edit or delete payment option.
	 * 
	 * @param billingAddressId
	 * @param payments
	 * @param paymentObject
	 * @param updatePaymentRequestBean
	 */
	public static void updatePaymentsObject(String billingAddressId, Payments payments, JsonObject paymentObject,
			UpdatePaymentRequestBean updatePaymentRequestBean) {
		payments.setPaymentId(paymentObject.get("paymentId").getAsString());
		if (paymentObject.has(CUSTOMER_ID)) {
			payments.setCustomerId(paymentObject.get(CUSTOMER_ID).getAsString());
		}
		payments.setPaymentType(paymentObject.get(MolekuleConstant.PAYMENT_TYPE).getAsString());
		payments.setBillingAddressId(billingAddressId);
		if (paymentObject.get(MolekuleConstant.PAYMENT_TYPE).getAsString()
				.equals(PaymentRequestBean.Payment.CREDITCARD.toString())) {
			setCreditCardPaymentType(billingAddressId, payments, paymentObject);
		} else if (paymentObject.get(MolekuleConstant.PAYMENT_TYPE).getAsString()
				.equals(PaymentRequestBean.Payment.PAYMENTTERMS.toString())) {
			paymentIntermsType(billingAddressId, payments, paymentObject, updatePaymentRequestBean);
		}
	}

	private static void paymentIntermsType(String billingAddressId, Payments payments, JsonObject paymentObject,
			UpdatePaymentRequestBean updatePaymentRequestBean) {
		payments.setBillingAddressId(billingAddressId);
		Business business = new Business();
		JsonObject businessObj = paymentObject.get("business").getAsJsonObject();
		business.setBusinessType(businessObj.get(MolekuleConstant.BUSINESS_TYPE).getAsString());

		updatePaymentRequestBean(payments, paymentObject, updatePaymentRequestBean, business, businessObj);

		if (PaymentRequestBean.BusinessType.PUBLIC.toString()
				.equals(businessObj.get(MolekuleConstant.BUSINESS_TYPE).getAsString())) {
			business.setStockTicker(businessObj.get("stockTicker").getAsString());
			business.setStockExchange(businessObj.get("stockExchange").getAsString());
		} else if (PaymentRequestBean.BusinessType.PRIVATE.toString()
				.equals(businessObj.get(MolekuleConstant.BUSINESS_TYPE).getAsString())) {
			// Business Reference
			Business.References references = new Business.References();
			BusinessReferences refObjectOne = new BusinessReferences();
			BusinessReferences refObjectTwo = new BusinessReferences();
			JsonObject firstReferenceObj = businessObj.get("references").getAsJsonObject().get("firstReference")
					.getAsJsonObject();
			refObjectOne.setFirstName(firstReferenceObj.get(FIRST_NAME).getAsString());
			refObjectOne.setLastName(firstReferenceObj.get(LAST_NAME).getAsString());
			refObjectOne.setEmail(firstReferenceObj.get(EMAIL).getAsString());
			refObjectOne.setPhoneNumber(firstReferenceObj.get(PHONE_NUMBER).getAsString());

			JsonObject secondReferenceObj = businessObj.get("references").getAsJsonObject().get("secondReference")
					.getAsJsonObject();
			refObjectTwo.setFirstName(secondReferenceObj.get(FIRST_NAME).getAsString());
			refObjectTwo.setLastName(secondReferenceObj.get(LAST_NAME).getAsString());
			refObjectTwo.setEmail(secondReferenceObj.get(EMAIL).getAsString());
			refObjectTwo.setPhoneNumber(secondReferenceObj.get(PHONE_NUMBER).getAsString());
			references.setFirstReference(refObjectOne);
			references.setSecondReference(refObjectTwo);
			business.setReferences(references);

			BankInformation bankInformation = new BankInformation();
			JsonObject bankInformationObj = businessObj.get("bankInformation").getAsJsonObject();
			bankInformation.setName(bankInformationObj.get("name").getAsString());
			bankInformation.setStreetAddress1(bankInformationObj.get(STREET_ADDRESS_1).getAsString());
			bankInformation.setStreetAddress2(bankInformationObj.get(STREET_ADDRESS_2).getAsString());
			bankInformation.setCity(bankInformationObj.get("city").getAsString());
			bankInformation.setState(bankInformationObj.get(STATE).getAsString());
			bankInformation.setPostalCode(bankInformationObj.get(POSTAL_CODE).getAsInt());
			bankInformation.setCountry(bankInformationObj.get(COUNTRY).getAsString());
			bankInformation.setPhoneNumber(bankInformationObj.get(PHONE_NUMBER).getAsString());
			business.setBankInformation(bankInformation);

		} else if (PaymentRequestBean.BusinessType.GOVERNMENT.toString()
				.equals(businessObj.get(MolekuleConstant.BUSINESS_TYPE).getAsString())) {
			// Government Verfication
			business.setEmail(businessObj.get(EMAIL).getAsString());
			business.setPhoneNumber(businessObj.get(PHONE_NUMBER).getAsString());

		} else if (PaymentRequestBean.BusinessType.NONPROFIT.toString()
				.equals(businessObj.get(MolekuleConstant.BUSINESS_TYPE).getAsString())) {
			// NonProfit
			JsonArray documentProofArray = businessObj.get("documentProof").getAsJsonArray();
			List<NonProfitDocumentProofDto> docList = new ArrayList<>();
			for (int i = 0; i < documentProofArray.size(); i++) {
				JsonObject documentProofObj = documentProofArray.get(i).getAsJsonObject();
				NonProfitDocumentProofDto docProof = new NonProfitDocumentProofDto();
				docProof.setId(documentProofObj.get("id").getAsString());
				docProof.setLocation(documentProofObj.get("location").getAsString());
				docList.add(docProof);
			}
			business.setDocumentProof(docList);
		}
		payments.setBusiness(business);
	}

	private static void updatePaymentRequestBean(Payments payments, JsonObject paymentObject,
			UpdatePaymentRequestBean updatePaymentRequestBean, Business business, JsonObject businessObj) {
		if (Objects.nonNull(updatePaymentRequestBean) && updatePaymentRequestBean.getTermsStatus() != null) {
			payments.setTermsStatus(updatePaymentRequestBean.getTermsStatus());
			business.setCurrency(updatePaymentRequestBean.getCurrency());
			payments.setTransactionNeedApproval(updatePaymentRequestBean.getTransactionNeedApproval());
			payments.setTotalApprovedCredit(updatePaymentRequestBean.getTotalApprovedCredit());
			payments.setTotalUsedCredit(updatePaymentRequestBean.getTotalUsedCredit());
			payments.setNetTerms(updatePaymentRequestBean.getNetTerms());

			if (updatePaymentRequestBean.getTermsStatus().toString().equals(TermsStatus.APPROVED.name())) {
				Date termsApprovalDate = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				payments.setTermsApprovalDate(formatter.format(termsApprovalDate));
			}
		} else {
			payments.setTermsStatus(TermsStatus.valueOf(paymentObject.get("termsStatus").getAsString()));
			setBusinessObjectValue(payments, paymentObject, business, businessObj);

		}
	}

	private static void setBusinessObjectValue(Payments payments, JsonObject paymentObject, Business business,
			JsonObject businessObj) {
		if (businessObj.has("currency"))
			business.setCurrency(businessObj.get("currency").getAsString());
		if (paymentObject.has("transactionNeedApproval"))
			payments.setTransactionNeedApproval(paymentObject.get("transactionNeedApproval").getAsBoolean());
		if (paymentObject.has("totalApprovedCredit"))
			payments.setTotalApprovedCredit(paymentObject.get("totalApprovedCredit").getAsString());
		if (paymentObject.has("totalUsedCredit"))
			payments.setTotalUsedCredit(paymentObject.get("totalUsedCredit").getAsString());
		if (paymentObject.has("netTerms"))
			payments.setNetTerms(paymentObject.get("netTerms").getAsString());
		if (paymentObject.has("termsApprovalDate"))
			payments.setTermsApprovalDate(paymentObject.get("termsApprovalDate").getAsString());
	}

	private static void setCreditCardPaymentType(String billingAddressId, Payments payments, JsonObject paymentObject) {
		payments.setPaymentToken(paymentObject.get("paymentToken").getAsString());
		payments.setCustomerId(paymentObject.get(CUSTOMER_ID).getAsString());
		payments.setBillingAddressId(billingAddressId);
		if(paymentObject.has("cardObject")) {
			JsonObject cardJsonObject = paymentObject.get("cardObject").getAsJsonObject();
			CreditCard creditCard = new CreditCard();
			creditCard.setBrand(cardJsonObject.get("brand").getAsString());
			creditCard.setExpMonth(cardJsonObject.get("expMonth").getAsString());
			creditCard.setExpYear(cardJsonObject.get("expYear").getAsString());
			creditCard.setLast4(cardJsonObject.get("last4").getAsString());
			payments.setCardObject(creditCard);
		}
	}

	/**
	 * updateAddressList is used to update Address List.
	 * 
	 * @param updatePaymentRequestBean
	 * @param billingAddressId
	 * @param valueObject
	 * @param billingAddressList
	 * @param shippingList
	 * @param paymentType
	 */
	public static void updateAddressList(UpdatePaymentRequestBean updatePaymentRequestBean, String billingAddressId,
			JsonObject valueObject, List<BillingAddresses> billingAddressList, List<ShippingAddresses> shippingList) {
		JsonObject addressesObject = valueObject.getAsJsonObject(ADDRESSES).getAsJsonObject();
		if (addressesObject.get("billingAddresses") != null) {
			JsonArray billingAddressArray = addressesObject.get("billingAddresses").getAsJsonArray();
			for (int i = 0; i < billingAddressArray.size(); i++) {
				JsonObject billingObject = billingAddressArray.get(i).getAsJsonObject();
				BillingAddresses billingAddresses = new BillingAddresses();
				billingAddresses
				.setBillingAddressId(billingObject.get(MolekuleConstant.BILLING_ADDRESS_ID).getAsString());
				if (billingAddressId.equals(billingObject.get(MolekuleConstant.BILLING_ADDRESS_ID).getAsString())
						&& Objects.nonNull(updatePaymentRequestBean.getBillingAddress())) {
					setBillingAddressValueFromUpdatePaymentRequestBean(updatePaymentRequestBean, billingAddressList,
							billingAddresses);
				} else {
					setBillingAddressValue(billingAddressList, billingObject, billingAddresses);
				}
			}
		}
		if (addressesObject.get("shippingAddresses") != null) {
			JsonArray shippingAddressArray = addressesObject.get("shippingAddresses").getAsJsonArray();
			for (int i = 0; i < shippingAddressArray.size(); i++) {
				JsonObject shippingObject = shippingAddressArray.get(i).getAsJsonObject();
				setShippingAddressValue(shippingList, shippingObject);
			}

		}
	}

	private static void setShippingAddressValue(List<ShippingAddresses> shippingList, JsonObject shippingObject) {
		ShippingAddresses shippingAddresses = new ShippingAddresses();
		if(shippingObject.has("shippingAddressId"))
			shippingAddresses.setShippingAddressId(shippingObject.get("shippingAddressId").getAsString());
		if(shippingObject.has(FIRST_NAME))
			shippingAddresses.setFirstName(shippingObject.get(FIRST_NAME).getAsString());
		if(shippingObject.has(LAST_NAME))
			shippingAddresses.setLastName(shippingObject.get(LAST_NAME).getAsString());
		if(shippingObject.has(STREET_ADDRESS_1))
			shippingAddresses.setStreetAddress1(shippingObject.get(STREET_ADDRESS_1).getAsString());
		if(shippingObject.has(STREET_ADDRESS_2))
			shippingAddresses.setStreetAddress2(shippingObject.get(STREET_ADDRESS_2).getAsString());
		if(shippingObject.has("city"))
			shippingAddresses.setCity(shippingObject.get("city").getAsString());
		if(shippingObject.has(STATE))
			shippingAddresses.setState(shippingObject.get(STATE).getAsString());
		if(shippingObject.has(POSTAL_CODE))
			shippingAddresses.setPostalCode(shippingObject.get(POSTAL_CODE).getAsString());
		if(shippingObject.has(PHONE_NUMBER))
			shippingAddresses.setPhoneNumber(shippingObject.get(PHONE_NUMBER).getAsString());
		if(shippingObject.has(COUNTRY))
			shippingAddresses.setCountry(shippingObject.get(COUNTRY).getAsString());
		shippingList.add(shippingAddresses);
	}

	private static void setBillingAddressValueFromUpdatePaymentRequestBean(
			UpdatePaymentRequestBean updatePaymentRequestBean, List<BillingAddresses> billingAddressList,
			BillingAddresses billingAddresses) {
		billingAddresses.setFirstName(updatePaymentRequestBean.getBillingAddress().getFirstName());
		billingAddresses.setLastName(updatePaymentRequestBean.getBillingAddress().getLastName());
		billingAddresses
		.setStreetAddress1(updatePaymentRequestBean.getBillingAddress().getStreetAddress1());
		billingAddresses
		.setStreetAddress2(updatePaymentRequestBean.getBillingAddress().getStreetAddress2());
		billingAddresses.setCity(updatePaymentRequestBean.getBillingAddress().getCity());
		billingAddresses.setState(updatePaymentRequestBean.getBillingAddress().getState());
		billingAddresses.setPostalCode(updatePaymentRequestBean.getBillingAddress().getPostalCode());
		billingAddresses.setPhoneNumber(updatePaymentRequestBean.getBillingAddress().getPhoneNumber());
		billingAddresses.setCountry(updatePaymentRequestBean.getBillingAddress().getCountry());
		billingAddressList.add(billingAddresses);
	}

	private static void setBillingAddressValue(List<BillingAddresses> billingAddressList, JsonObject billingObject,
			BillingAddresses billingAddresses) {
		if (billingObject.has(FIRST_NAME))
			billingAddresses.setFirstName(billingObject.get(FIRST_NAME).getAsString());
		if (billingObject.has(LAST_NAME))
			billingAddresses.setLastName(billingObject.get(LAST_NAME).getAsString());
		if (billingObject.has(STREET_ADDRESS_1))
			billingAddresses.setStreetAddress1(billingObject.get(STREET_ADDRESS_1).getAsString());
		if (billingObject.has(STREET_ADDRESS_2))
			billingAddresses.setStreetAddress2(billingObject.get(STREET_ADDRESS_2).getAsString());
		if (billingObject.has("city"))
			billingAddresses.setCity(billingObject.get("city").getAsString());
		if (billingObject.has(STATE))
			billingAddresses.setState(billingObject.get(STATE).getAsString());
		if (billingObject.has(POSTAL_CODE))
			billingAddresses.setPostalCode(billingObject.get(POSTAL_CODE).getAsString());
		if (billingObject.has(PHONE_NUMBER))
			billingAddresses.setPhoneNumber(billingObject.get(PHONE_NUMBER).getAsString());
		if (billingObject.has(COUNTRY))
			billingAddresses.setCountry(billingObject.get(COUNTRY).getAsString());
		billingAddressList.add(billingAddresses);
	}

	/**
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static String removeEmailFromErrorsMessage(JsonObject jsonObject) {

		// Removed the email from the response when received duplicate email error.

		if (jsonObject.get(ERRORS) != null && jsonObject.get(ERRORS).getAsJsonArray().size() > 0) {

			JsonObject errors = (com.google.gson.JsonObject) jsonObject.get(ERRORS).getAsJsonArray().get(0);

			if ("DuplicateField".equals(errors.get("code").getAsString())
					&& EMAIL.equals(errors.get("field").getAsString())) {
				jsonObject.addProperty("statusCode", "400");
				jsonObject.addProperty("message", MolekuleConstant.EMAIL_VALIDATION);
				errors.addProperty("message", MolekuleConstant.EMAIL_VALIDATION);
				jsonObject.add(ERRORS, errors);
			}

		}
		return jsonObject.toString();
	}

}
