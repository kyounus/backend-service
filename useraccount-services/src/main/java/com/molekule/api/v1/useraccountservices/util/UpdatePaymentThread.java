package com.molekule.api.v1.useraccountservices.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Payments.TermsStatus;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import lombok.Data;

@Data
public class UpdatePaymentThread implements Runnable{
	Logger logger = LoggerFactory.getLogger(UpdatePaymentThread.class);
	Map<String, String> requestData;
	
	private UpdatePaymentRequestBean updatePaymentRequestBean;
	private TealiumHelperService tealiumHelperService;
	private AccountCustomObject accountCustomObject;
	private RegistrationHelperService registrationHelperService;
	private TemplateMailRequestHelper paymentTermsSuccessConfirmationSender;
	@Override
	public void run() {
		logger.info("SendGrid and tealium implementation starts");
		accountCustomObject = registrationHelperService.mappingStringResponseToCustomObject(requestData.get("response"));
		//send grid implementation
		processSendGridImplementation(updatePaymentRequestBean, accountCustomObject);
		paymentProcessing(requestData.get(CUSTOMER_ID), requestData.get(ACCOUNT_ID), accountCustomObject);
		tealiumHelperService.setAccountInfo(requestData.get("response"));
		logger.info("SendGrid and tealium implementation ends");
	}
	
	private void processSendGridImplementation(UpdatePaymentRequestBean updatePaymentRequestBean,
			AccountCustomObject accountCustomObject) {
		if(accountCustomObject != null && accountCustomObject.getValue() != null && accountCustomObject.getValue().getPayments() != null) {
			List<Payments> responsePaymentList = accountCustomObject.getValue().getPayments();
			for(int i =0;i<responsePaymentList.size();i++) {
				if(responsePaymentList.get(i).getPaymentType().equals("PAYMENTTERMS") && 
						(updatePaymentRequestBean.getTermsStatus()!= null) &&
						(updatePaymentRequestBean.getTermsStatus().toString().equals(TermsStatus.APPROVED.name())) &&
						(responsePaymentList.get(i).getTermsStatus().toString().equals(TermsStatus.APPROVED.name()))

						){
					SendGridModel sendGridModel = registrationHelperService.createEmailDynamicInfo(responsePaymentList.get(i).getCustomerId());
					paymentTermsSuccessConfirmationSender.sendMail(sendGridModel);
				}
			}
		}
	}
	
	private void paymentProcessing(String customerId, String accountId, AccountCustomObject accountCustomObject) {
		List<Payments> payments = null;
		if (accountCustomObject != null && accountCustomObject.getValue() != null) {
			payments = accountCustomObject.getValue().getPayments();
		}
		Payments payment = processPayment(payments);

		if (payment != null && payment.getBusiness() != null
				&& TermsStatus.APPROVED.name().equals(payment.getTermsStatus().name())) {
			registrationHelperService.processOrder(customerId, "CreditHold", accountId);
		}
	}
	
	private Payments processPayment(List<Payments> payments) {
		Payments payment = null;
		if (payments != null) {
			Iterator<Payments> paymentsIte = payments.listIterator();
			while (paymentsIte.hasNext()) {
				Payments payments2 = paymentsIte.next();
				if (PaymentRequestBean.Payment.PAYMENTTERMS.name().equals(payments2.getPaymentType())) {
					payment = payments2;
					break;
				}
			}
		}
		return payment;
	}

}
