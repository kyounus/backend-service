package com.molekule.api.v1.useraccountservices.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ATTRIBUTES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BRAND;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CANCELLED;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CART;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CART_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CENT_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHARGES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COMPANY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CREATED_AT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CREDITCARD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CURRENCY_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID_WITH_QUERY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM_OBJECTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.D2C;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.D2C_PAYMENT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DATA;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DEVICE_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMPTY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EN_US;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EXPAND_CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIRST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ID_WITH_QUERY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.INCLUDED_DISCOUNTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST4;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_MODIFIED_DESC;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LIMIT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LINE_ITEMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.NEXT_ORDER_DATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.NEXT_ORDER_DATE_WITH_QUERY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.OFFSET;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PARENT_ORDER_REFERENCE_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENTTERMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_INFO;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_INTENT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_METHOD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENT_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PERIOD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.POSTAL_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SCHEDULED;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SERIAL_NUMBERS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SET_CUSTOM_FIELD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SET_CUSTOM_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STREET_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STREET_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOKEN;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VARIANT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartFilterServiceBean;
import com.molekule.api.v1.commonframework.dto.customerprofile.DeviceCustomObject;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionConfigurationDto;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionConfigurationDto.Value.Blackout;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionConfigurationDto.Value.RetryDelay;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionConfigurationDto.Value.Shipping;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionConfigurationDto.Value.Shipping.Option;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionCustomObject;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionCustomObject.Value.TransactionObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponse;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.aurora.GetCustomerBean;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.Payment;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart;
import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;
import com.molekule.api.v1.commonframework.model.customerprofile.AutoRefillsResponseBean;
import com.molekule.api.v1.commonframework.model.customerprofile.AutoRefillsResponseBean.SubscriptionsObject;
import com.molekule.api.v1.commonframework.model.customerprofile.AutoRefillsResponseBean.SubscriptionsObject.LineItems;
import com.molekule.api.v1.commonframework.model.customerprofile.BulkUpdateRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.CustomerProfile;
import com.molekule.api.v1.commonframework.model.customerprofile.DeviceRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.GetSubscriptionBean;
import com.molekule.api.v1.commonframework.model.customerprofile.MessageCenterBean;
import com.molekule.api.v1.commonframework.model.customerprofile.MessageCenterBean.MessageCenter;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionConfiguration;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSortEnum;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSurveyResponse;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSurveyResponse.SurveyQuestion;
import com.molekule.api.v1.commonframework.model.customerprofile.TermsPayment;
import com.molekule.api.v1.commonframework.model.customerprofile.UpdateAutoRefillsRequestBean;
import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;
import com.molekule.api.v1.commonframework.model.registration.CustomerRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ProductResponseBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.CustomerProfileHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.CartServiceUtility;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.CustomerChannelEnum;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.useraccountservices.util.BulkUpdateActionEnum;
import com.molekule.api.v1.useraccountservices.util.CustomerProfileUtility;
import com.molekule.api.v1.useraccountservices.util.RegistrationUtility;

@Service("customerProfileService")
public class CustomerProfileService {
	Logger logger = LoggerFactory.getLogger(CustomerProfileService.class);
	@Autowired
	@Qualifier("checkoutHelperService")
	CheckoutHelperService checkoutHelperService;

	@Autowired
	@Qualifier("auroraService")
	AuroraService auroraService;

	@Autowired
	@Qualifier("orderHelperService")
	OrderHelperService orderHelperService;

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("cartHelperService")
	CartHelperService cartHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	@Autowired
	@Qualifier(value = "subscriptionSignupConfirmationSender")
	TemplateMailRequestHelper subscriptionSignupConfirmationSender;

	@Autowired
	@Qualifier(value = "subScriptionRenewalDateUpdationSender")
	TemplateMailRequestHelper subScriptionRenewalDateUpdationSender;

	@Autowired
	@Qualifier(value = "autoRefilActivationMailSender")
	TemplateMailRequestHelper autoRefilActivationMailSender;

	@Autowired
	RegistrationService registrationService;

	@Autowired
	NetConnectionHelper netConnectionHelper;

	@Autowired
	CtServerHelperService ctServerHelperService;

	@Autowired
	CustomerProfileUtility customerProfileUtility;

	@Autowired
	RegistrationHelperService registrationHelperService;

	@Autowired
	CustomerProfileHelperService customerProfileHelperService;

	@Autowired
	CsrHelperService csrHelperService;

	@org.springframework.beans.factory.annotation.Value("${stripeApiKey}")
	private String stripeAuthKey;

	@org.springframework.beans.factory.annotation.Value("${paymentIntentHost}")
	private String paymentIntentUrl;

	@org.springframework.beans.factory.annotation.Value("${setupPaymentIntentUrl}")
	private String setupPaymentIntentUrl;

	/**
	 * getCustomerProfile method is used to get customer profile data with account,
	 * customer and order list.
	 * 
	 * @param accountId
	 * @param customerId
	 * @param limit
	 * @param offset
	 * @return
	 */
	public ResponseEntity<CustomerProfile> getCustomerProfile(String customerId, String limit,
			int offset) {
		CustomerProfile customerProfile = new CustomerProfile();
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String channel = cartHelperService.getCustomerChannel(customerId);
		String accountId = registrationHelperService.getAccountIdWithCustomerId(customerId);
		if(accountId != null) {
			ResponseEntity<AccountCustomObject> accountCustomObject = checkoutHelperService
					.getAccountCustomObjectApi(accountId, customerId);
			customerProfile.setAccountCustomObject(accountCustomObject.getBody());
		}
		List<CustomerResponse> customerResponseList = auroraService.getCustomersByAccountId(customerId);
		customerProfile.setCustomerResponseBean(customerResponseList.get(0).getCustomerResponseBean());
		OrderSummaryResponseBean orderSummaryResponseObject = orderHelperService.getAllOrders(customerId,
				Integer.valueOf(limit), offset, null, null, null, null, null, null, null);
		customerProfile.setOrderSummaryResponseBean(orderSummaryResponseObject);
		List<MessageCenter> messageCenterList = new ArrayList<>();
		orderSummaryResponseObject.getOrderSummaryList().stream().forEach(order -> {
			MessageCenter messageCenter = new MessageCenter();
			messageCenter.setOrderId(order.getOrderId());
			messageCenter.setOrderNumber(order.getOrderNumber());
			messageCenter.setOrderState(Arrays.asList(order.getOrderState()));
			messageCenter.setReadFlag(order.getReadFlag());
			messageCenterList.add(messageCenter);
		});
		MessageCenterBean messageCenterBean = new MessageCenterBean();
		messageCenterBean.setMessageCenter(messageCenterList);
		customerProfile.setMessageCenterBean(messageCenterBean);
		customerProfile.setAutoRefillsResponseBean(getAutoRefillsSubscriptions(customerId, null, null, Integer.parseInt(limit), offset).getBody());
		PaymentCustomObject customPaymentResponse = null;
		if(StringUtils.hasText(channel) && D2C.equals(channel)) {
			String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects" + D2C_PAYMENT
					+ customerId;
			customPaymentResponse = netConnectionHelper.getPaymentCustomObject(token, customPaymentBaseUrl);
			customerProfile.setPaymentCustomObject(customPaymentResponse);
		}
		return new ResponseEntity<>(customerProfile, HttpStatus.OK);
	}

	/**
	 * getautoRefillSubscriptions method is used to get all auto refill
	 * subscriptions.
	 * 
	 * @param customerId
	 * @param shippingAddressId
	 * @param paymentId
	 * @param offset
	 * @param limit
	 * @return ResponseEntity<String>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<AutoRefillsResponseBean> getAutoRefillsSubscriptions(String customerId,
			String shippingAddressId, String paymentId, int limit, int offset) {
		String baseUrl = getSubscriptionUrl(customerId, limit, offset);
		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, baseUrl);
		JsonObject josonObject = MolekuleUtility.parseJsonObject(response);
		JsonArray resultArrays = josonObject.get(RESULTS).getAsJsonArray();
		if (StringUtils.hasText(shippingAddressId)) {
			List<String> shippingIdList = new ArrayList<>();
			for (int i = 0; i < resultArrays.size(); i++) {
				setShippingAddressDetails(resultArrays, shippingIdList, i);
			}
			List<String> filteredShippingId = shippingIdList.stream()
					.filter(address -> address.equals(shippingAddressId)).collect(Collectors.toList());
			if (filteredShippingId.isEmpty()) {
				ErrorResponse errorResponse = new ErrorResponse(3000,
						"Subscription Object is not available for this ShippingAddressId");
				return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
			}
		} else if (StringUtils.hasText(paymentId)) {
			boolean isSubscriptionPayment;
			isSubscriptionPayment = isPaymentHasSubscripiton(paymentId, resultArrays, token);

			if (!isSubscriptionPayment) {
				ErrorResponse errorResponse = new ErrorResponse(3000,
						"Subscription Object is not available for this paymentId");
				return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
			}
		}
		AutoRefillsResponseBean autoRefillsResponseBean = getAutoRefillsResponse(token, response);
		return new ResponseEntity(autoRefillsResponseBean, HttpStatus.OK);

	}

	private boolean isPaymentHasSubscripiton(String paymentId, JsonArray resultArrays, String token) {
		boolean isSubscriptionPayment = false;
		Set<String> paymentsIdSet = new HashSet<>();
		processResultsArrayValues(token, resultArrays, paymentsIdSet);
		Set<String> finalPaymentSet = new HashSet<>();
		for (String id : paymentsIdSet) {
			String paymentCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments/"
					+ id;
			String paymentResponse = netConnectionHelper.sendGetWithoutBody(token, paymentCtUrl);
			JsonObject paymentObject = MolekuleUtility.parseJsonObject(paymentResponse);
			JsonObject fieldObject = new JsonObject();
			if(paymentObject.has(CUSTOM) && paymentObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
				fieldObject = paymentObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			}
			if(fieldObject.has("paymentId")) {
				finalPaymentSet.add(fieldObject.get("paymentId").getAsString());
			}
		}
		for(String finalPaymentId : finalPaymentSet) {
			if(finalPaymentId.equals(paymentId)) {
				isSubscriptionPayment = true;
				break;
			}
		}
		return isSubscriptionPayment;
	}

	private void processResultsArrayValues(String token, JsonArray resultArrays, Set<String> paymentIdSet) {
		for (int i = 0; i < resultArrays.size(); i++) {
			JsonObject resultCartObject = resultArrays.get(i).getAsJsonObject().get(VALUE).getAsJsonObject().get("cart").getAsJsonObject();
			String paymentTypeId = null;
			JsonArray paymentArray = null;
			if(resultCartObject.has("obj")) {
				if (resultCartObject.get("obj").getAsJsonObject().has(PAYMENT_INFO)) {
					paymentArray = resultCartObject.get("obj").getAsJsonObject().get(PAYMENT_INFO).getAsJsonObject().get(PAYMENTS).getAsJsonArray();
				} else {
					String parentOrderId = resultCartObject.get("obj").getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
							.get(PARENT_ORDER_REFERENCE_ID).getAsJsonObject().get("id").getAsString();
					String ordersBaseUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
							.append(ctEnvProperties.getProjectKey()).append("/orders/").append(parentOrderId)
							.append("?expand=paymentInfo.payments[*].id&expand=state.id").toString();
					String orderResponse = netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
					JsonObject orderObject = MolekuleUtility.parseJsonObject(orderResponse);
					paymentArray = orderObject.get(PAYMENT_INFO).getAsJsonObject().get(PAYMENTS).getAsJsonArray();
				}
				paymentTypeId = paymentArray.get(paymentArray.size() - 1).getAsJsonObject().get("id").getAsString();
				paymentIdSet.add(paymentTypeId);
			}
		}
	}

	private void setShippingAddressDetails(JsonArray resultArrays, List<String> shippingIdList, int i) {
		JsonObject cartJsonObject = resultArrays.get(i).getAsJsonObject().get(VALUE).getAsJsonObject().get("cart").getAsJsonObject();
		if(cartJsonObject.has("obj")) {
			JsonObject cartObject = cartJsonObject.get("obj").getAsJsonObject();
			if (cartObject.has(SHIPPING_ADDRESS) && cartObject.get(SHIPPING_ADDRESS).getAsJsonObject().has("id")) {
				String resultShippingId = cartObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id")
						.getAsString();
				shippingIdList.add(resultShippingId);
			}
		}
	}

	/**
	 * getAllSubscriptions method issue to get all subscriptions data.
	 * 
	 * @param limit
	 * @param offset
	 * @param email
	 * @param nextOrderDate
	 * @param sortOrder 
	 * @param sortAction 
	 * @return ResponseEntity<String>
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ResponseEntity<AutoRefillsResponseBean> getAllSubscriptions(GetSubscriptionBean getSubscriptionBean) {
		int limit = getSubscriptionBean.getLimit();
		int offset = getSubscriptionBean.getOffset();
		String email = getSubscriptionBean.getEmail();
		String nextOrderDate = getSubscriptionBean.getNextOrderDate();
		String customerId = getSubscriptionBean.getCustomerId();
		String subscriptionId = getSubscriptionBean.getSubscriptionId();
		String channel = getSubscriptionBean.getChannel();
		SubscriptionSortEnum sortAction = getSubscriptionBean.getSortAction();
		SortOrderEnum sortOrder = getSubscriptionBean.getSortOrder();
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + LIMIT
				+ limit + OFFSET + offset + "&expand=value.cart.id&expand=value.customer.id"
				+ "&where=value(subscription=true)&where=value((state!=\"Renew\")and(state!=\"Processed\"))";
		String token = ctServerHelperService.getStringAccessToken();

		baseUrl = sortUrl(sortAction, sortOrder, baseUrl);
		if (StringUtils.hasText(email)) {
			GetCustomerBean getCustomerBean = new GetCustomerBean();
			getCustomerBean.setLimit(20);
			getCustomerBean.setOffset(0);
			getCustomerBean.setEmail(email);			
			JsonObject customerResponseObject = MolekuleUtility
					.parseJsonObject(auroraService.getListOfCustomer(getCustomerBean));
			JsonArray resultArray = customerResponseObject.get(RESULTS).getAsJsonArray();
			if(resultArray.size() == 0){
				ErrorResponse errorResponse = new ErrorResponse(400, "No results for the requested params.");
				return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
			}
			String id = resultArray.get(0).getAsJsonObject().get("id").getAsString();
			baseUrl = baseUrl + CUSTOMER_ID_WITH_QUERY + id + "\"))";
		} 
		if (StringUtils.hasText(nextOrderDate)) {
			baseUrl = baseUrl + NEXT_ORDER_DATE_WITH_QUERY + nextOrderDate + "\")";
		} 
		if (StringUtils.hasText(channel)) {
			baseUrl = baseUrl + "&where=value(channel=\"" + channel + "\")";
		} 
		if (StringUtils.hasText(subscriptionId)) {
			baseUrl = baseUrl + ID_WITH_QUERY + subscriptionId + "\"";
		} 
		if (StringUtils.hasText(customerId)) {
			baseUrl = baseUrl + CUSTOMER_ID_WITH_QUERY + customerId + "\"))";
		} 		
		String response = netConnectionHelper.sendGetWithoutBody(token, baseUrl);
		AutoRefillsResponseBean autoRefillsResponseBean = getAutoRefillsResponse(token, response);
		if(autoRefillsResponseBean.getSubscriptionsObject().isEmpty()){
			ErrorResponse errorResponse = new ErrorResponse(400, "No results for the requested params.");
			return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(autoRefillsResponseBean, HttpStatus.OK);
	}

	private String sortUrl(SubscriptionSortEnum sortAction, SortOrderEnum sortOrder, String baseUrl) {
		if(sortAction !=null && sortOrder !=null) {
			if(sortAction.name().equals(SubscriptionSortEnum.DATE_CREATED.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				baseUrl = baseUrl + "&sort=createdAt asc";
			}else if(sortAction.name().equals(SubscriptionSortEnum.DATE_CREATED.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				baseUrl = baseUrl + "&sort=createdAt desc";
			}else if(sortAction.name().equals(SubscriptionSortEnum.DATE_MODIFIED.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				baseUrl = baseUrl + "&sort=lastModifiedAt asc";
			}else if(sortAction.name().equals(SubscriptionSortEnum.DATE_MODIFIED.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				baseUrl = baseUrl + LAST_MODIFIED_DESC;
			}else {
				baseUrl = baseUrl + LAST_MODIFIED_DESC;
			}
		}else {
			baseUrl = baseUrl + LAST_MODIFIED_DESC;
		}
		return baseUrl;
	}

	/**
	 * getAutoRefillsResponse method is form the subscription response.
	 * 
	 * @param token
	 * @param response
	 * @return
	 */
	private AutoRefillsResponseBean getAutoRefillsResponse(String token, String response) {
		AutoRefillsResponseBean autoRefillsResponseBean = new AutoRefillsResponseBean();
		JsonObject josonObject = MolekuleUtility.parseJsonObject(response);
		autoRefillsResponseBean.setLimit(josonObject.get("limit").getAsLong());
		autoRefillsResponseBean.setOffset(josonObject.get("offset").getAsLong());
		autoRefillsResponseBean.setCount(josonObject.get("count").getAsLong());
		autoRefillsResponseBean.setTotal(josonObject.get(TOTAL).getAsLong());
		JsonArray resultArrays = josonObject.get(RESULTS).getAsJsonArray();
		List<SubscriptionsObject> subscriptionsList = new ArrayList<>();
		Map<String, SubscriptionsObject> subscriptionsPaments = new HashMap<>();
		for (int i = 0; i < resultArrays.size(); i++) {
			processResultsArray(resultArrays, subscriptionsList, i, subscriptionsPaments);
		}
		setSubscriptionPaymentDetails(token,subscriptionsPaments);
		autoRefillsResponseBean.setSubscriptionsObject(subscriptionsList);
		return autoRefillsResponseBean;
	}

	private void processResultsArray(JsonArray resultArrays, List<SubscriptionsObject> subscriptionsList,
			int i, Map<String, SubscriptionsObject> subscriptionsPaments) {
		SubscriptionsObject subscriptionsObject = new SubscriptionsObject();
		JsonObject resultObject = resultArrays.get(i).getAsJsonObject();
		subscriptionsObject.setId(resultObject.get("id").getAsString());
		subscriptionsObject.setCreatedAt(resultObject.get(CREATED_AT).getAsString());
		subscriptionsObject.setLastModifiedAt(resultObject.get("lastModifiedAt").getAsString());
		subscriptionsObject.setContainer(resultObject.get(CONTAINER).getAsString());
		subscriptionsObject.setKey(resultObject.get("key").getAsString());
		if (resultObject.has(VALUE)) {
			JsonObject valueObject = resultObject.get(VALUE).getAsJsonObject();
			if (valueObject.has("cart") && valueObject.has(CUSTOMER)
					&& valueObject.get("cart").getAsJsonObject().has("id")) {
				String cartId = valueObject.get("cart").getAsJsonObject().get("id").getAsString();
				if (valueObject.get("cart").getAsJsonObject().has("obj")
						&& valueObject.get(CUSTOMER).getAsJsonObject().has("obj")) {
					processCustomerValues(subscriptionsObject, valueObject, cartId, subscriptionsPaments);
				}
			}
			List<TransactionObject> transactionsList = new ArrayList<>();
			JsonArray reasonArray= valueObject.get("transactions").getAsJsonArray();
			for(int j=0;j<reasonArray.size();j++) {			
				JsonObject transactionObject = reasonArray.get(j).getAsJsonObject();
				TransactionObject transaction = new TransactionObject();
				transaction.setMessage(transactionObject.get("messages").getAsString());
				transaction.setType(transactionObject.get("type").getAsString());
				transactionsList.add(transaction);
			}
			subscriptionsObject.setTransactions(transactionsList);
		}
		subscriptionsList.add(subscriptionsObject);
	}

	private void processCustomerValues(SubscriptionsObject subscriptionsObject, JsonObject valueObject,
			String cartId, Map<String, SubscriptionsObject> subscriptionsPaments) {
		JsonObject objJson = valueObject.get("cart").getAsJsonObject().get("obj").getAsJsonObject();
		JsonObject customerObjJson = valueObject.get(CUSTOMER).getAsJsonObject().get("obj")
				.getAsJsonObject();
		if (valueObject.has(STATE)) {
			setStateValues(subscriptionsObject, valueObject);
		}
		subscriptionsObject.setCartId(cartId);
		subscriptionsObject.setCustomerEmail(customerObjJson.get("email").getAsString());
		subscriptionsObject.setCustomerName(customerObjJson.get(FIRST_NAME).getAsString());
		if(valueObject.has("lastOrderDate")) {
			subscriptionsObject.setLastOrderDate(valueObject.get("lastOrderDate").getAsString());
		}
		if(valueObject.has(PRODUCT)) {
			subscriptionsObject.setPlanName(valueObject.get(PRODUCT).getAsJsonObject().get("name").getAsJsonObject().get(EN_US).getAsString());
			subscriptionsObject.setPlanDescription(valueObject.get(PRODUCT).getAsJsonObject().get("description").getAsJsonObject().get(EN_US).getAsString());
		}
		if(valueObject.has(CHANNEL)) {
			subscriptionsObject.setChannel(valueObject.get(CHANNEL).getAsString());
		}else{
			processChannelValues(subscriptionsObject, customerObjJson);
		}
		subscriptionsObject.setNextOrderDate(valueObject.get(NEXT_ORDER_DATE).getAsString());
		subscriptionsObject.setSubscription(valueObject.get("subscription").getAsBoolean());
		subscriptionsObject.setCustomerId(objJson.get(CUSTOMER_ID).getAsString());
		subscriptionsObject.setTotalPrice(
				objJson.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsString());
		if (objJson.has("taxedPrice")) {
			String taxedPrice = objJson.get("taxedPrice").getAsJsonObject().get("totalGross")
					.getAsJsonObject().get(CENT_AMOUNT).getAsString();
			subscriptionsObject.setTaxedPrice(taxedPrice);
		}

		List<LineItems> list = new ArrayList<>();
		JsonObject cartObject = MolekuleUtility.parseJsonObject(cartHelperService.getCartById(cartId));
		JsonArray lineItemArray = cartObject.get(LINE_ITEMS).getAsJsonArray();
		setLineItem(subscriptionsObject, list, lineItemArray);
		subscriptionsObject.setLineItemList(list);
		JsonObject fieldObject = objJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		if(fieldObject.has(PERIOD)) {
		subscriptionsObject.setPeriod(fieldObject.get(PERIOD).getAsInt());
		}
		if (fieldObject.has("failureReason")) {
			subscriptionsObject.setStatus(fieldObject.get("failureReason").getAsString());
		}
		if(objJson.has(SHIPPING_ADDRESS)) {
		AddressRequestBean addressRequestBean = new AddressRequestBean();
		JsonObject shippingObject = objJson.get(SHIPPING_ADDRESS).getAsJsonObject();
		// setting shipping address
		setShippingAddress(addressRequestBean, shippingObject);
		subscriptionsObject.setShippingAddress(addressRequestBean);
		}
		// getting payment information
		JsonArray paymentArray = null;
		if (objJson.has("paymentInfo")) {
			paymentArray = objJson.get(PAYMENT_INFO).getAsJsonObject().get(PAYMENTS).getAsJsonArray();
		} /** This logic for old subscription, Right now received payment information from the Cart Response.
		  else {
			if(fieldObject.has(PARENT_ORDER_REFERENCE_ID)){
				String parentOrderId = objJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
						.get(PARENT_ORDER_REFERENCE_ID).getAsJsonObject().get("id").getAsString();
				String ordersBaseUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
						.append(ctEnvProperties.getProjectKey()).append("/orders/").append(parentOrderId)
						.append("?expand=paymentInfo.payments[*].id&expand=state.id").toString();
				String orderResponse = netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
				JsonObject orderObject = MolekuleUtility.parseJsonObject(orderResponse);
				if(orderObject.has(PAYMENT_INFO)) {
					paymentArray = orderObject.get(PAYMENT_INFO).getAsJsonObject().get(PAYMENTS)
							.getAsJsonArray();
				}
			}
		} */
		if(paymentArray != null) {
			/**setSubscriptionPayment(token, subscriptionsObject, paymentArray);*/
			String paymentTypeId = paymentArray.get(paymentArray.size() - 1).getAsJsonObject().get("id")
					.getAsString();
			subscriptionsPaments.put(paymentTypeId, subscriptionsObject);
		}

	}

	public void setLineItem(SubscriptionsObject subscriptionsObject, List<LineItems> list, JsonArray lineItemArray) {
		for (int j = 0; j < lineItemArray.size(); j++) {
			SubscriptionsObject.LineItems lineitems = new SubscriptionsObject.LineItems();
			JsonObject lineItemObject = lineItemArray.get(j).getAsJsonObject();
			lineitems.setLineItemId(lineItemObject.get("id").getAsString());
			String productId = lineItemObject.get(PRODUCT_ID).getAsString();
			lineitems.setProductId(productId);
			lineitems.setName(lineItemObject.get("name").getAsJsonObject().get(EN_US).getAsString());
			lineitems.setQuantity(lineItemObject.get(QUANTITY).getAsLong());
			if (lineItemObject.has("productSlug"))
				lineitems.setSlug(
						lineItemObject.get("productSlug").getAsJsonObject().get(EN_US).getAsString());
			lineitems.setVariantId(
					lineItemObject.get(VARIANT).getAsJsonObject().get("id").getAsString());
			if (lineItemObject.get(VARIANT).getAsJsonObject().has("sku"))
				lineitems
				.setSku(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());
			lineitems.setPrice(lineItemObject.get(PRICE).getAsJsonObject().get(VALUE)
					.getAsJsonObject().get(CENT_AMOUNT).getAsString());
			processDiscountCode(lineitems,lineItemObject);
			processSerialNumber(lineItemObject,subscriptionsObject);
			list.add(lineitems);
		}
	}

	private void processSerialNumber(JsonObject lineItemObject, SubscriptionsObject subscriptionsObject) {
		if (lineItemObject.has(CUSTOM)) {
			JsonObject customObj= lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(customObj.has(SERIAL_NUMBERS)) {
				subscriptionsObject.setSerialNumber(customObj.get(SERIAL_NUMBERS).getAsJsonArray().get(0).getAsString());
			}
		}

	}

	private void processDiscountCode(LineItems lineitems, JsonObject lineItemObject) {
		if (lineItemObject.has(DISCOUNTED_PRICE)) {
			String token = ctServerHelperService.getStringAccessToken();
			List<String> promoList = new ArrayList<>();
			JsonArray includedDiscountsArray = lineItemObject.get(DISCOUNTED_PRICE).getAsJsonObject().get(INCLUDED_DISCOUNTS).getAsJsonArray();
			for (int discount = 0; discount < includedDiscountsArray.size(); discount++) {
				JsonObject includedDiscountsJson = includedDiscountsArray.get(discount).getAsJsonObject();
				String id = includedDiscountsJson.get("discount").getAsJsonObject().get("id").getAsString();
				String discountUrl =  ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/discount-codes?where=cartDiscounts(id=\""+id+"\")";
				JsonObject jsonObject= MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(token, discountUrl));
				if(jsonObject.get(RESULTS).getAsJsonArray().size() > 0) {
					promoList.add(jsonObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject().get("code").getAsString());					
				}
			}
			lineitems.setPromoCodes(promoList);
		}
	}

	private void processChannelValues(SubscriptionsObject subscriptionsObject, JsonObject customerObjJson) {
		JsonObject customObj= customerObjJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		if(customObj.has(CHANNEL)) {
			subscriptionsObject.setChannel(customObj.get(CHANNEL).getAsString());
		}
	}

	/** Commented for performance Issue
	private void setSubscriptionPayment(String token, SubscriptionsObject subscriptionsObject, JsonArray paymentArray) {
		String paymentTypeId = paymentArray.get(paymentArray.size() - 1).getAsJsonObject().get("id")
				.getAsString();
		String paymentUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/payments/").append(paymentTypeId)
				.toString();
		String paymentResposnse = netConnectionHelper.sendGetWithoutBody(token, paymentUrl);
		JsonObject paymentObject = MolekuleUtility.parseJsonObject(paymentResposnse);
		String paymentMethod = paymentObject.get("paymentMethodInfo").getAsJsonObject().get("method")
				.getAsString();
		if (paymentMethod.equals("CREDIT_CARD")) {			
			CreditCard cardObj = new CreditCard();
			// forming card object
			getCardObject(paymentObject, cardObj);
			subscriptionsObject.setCreditCard(cardObj);
			subscriptionsObject.setPaymentMethod(paymentMethod);
		} else if (paymentMethod.equals(PAYMENTTERMS) || paymentMethod.equals("ACH")) {
			subscriptionsObject.setPaymentMethod(paymentMethod);
		}
	}

 */
	
	private void setSubscriptionPaymentDetails(String token, Map<String, SubscriptionsObject> subscriptionsPaments) {

		if (subscriptionsPaments.size() == 0) {
			return;
		}

		List<String> paymentTypeIds = new ArrayList<>(subscriptionsPaments.keySet());
		
		StringBuilder paymentUrlBuilder = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/payments?where=id in(");
		appendUrl(paymentTypeIds, paymentUrlBuilder);
		paymentUrlBuilder.append(")");
		
		String paymentResposnse = netConnectionHelper.sendGetWithoutBody(token, paymentUrlBuilder.toString());
        JsonObject paymentResults = MolekuleUtility.parseJsonObject(paymentResposnse);
		
        if(paymentResults.get(RESULTS) == null) {
        	return ;
        }
 
        JsonArray paymentResultsArray = paymentResults.get(RESULTS).getAsJsonArray();
		for (int i = 0; i < paymentResultsArray.size(); i++) {
			JsonObject paymentObject = paymentResultsArray.get(i).getAsJsonObject();
			String paymentId = paymentObject.get("id").getAsString();
			SubscriptionsObject subscriptionsObject = subscriptionsPaments.get(paymentId);

			String paymentMethod = paymentObject.get("paymentMethodInfo").getAsJsonObject().get("method").getAsString();
			if (paymentMethod.equals("CREDIT_CARD")) {
				CreditCard cardObj = new CreditCard();
				// forming card object
				getCardObject(paymentObject, cardObj);
				subscriptionsObject.setCreditCard(cardObj);
				subscriptionsObject.setPaymentMethod(paymentMethod);
			} else if (paymentMethod.equals(PAYMENTTERMS) || paymentMethod.equals("ACH")) {
				subscriptionsObject.setPaymentMethod(paymentMethod);
			}
		}
	}
	
	private void appendUrl(List<String> valueList, StringBuilder baseUrl) {
		for(int i=0;i<valueList.size();i++) {
			if(valueList.size()==1 || i==valueList.size()-1) {
				baseUrl.append("\"").append(valueList.get(i)).append("\"");
			}else {
				baseUrl.append("\"").append(valueList.get(i)).append("\"").append(",");
			}
		}
	}

	private void setStateValues(SubscriptionsObject subscriptionsObject, JsonObject valueObject) {
		String subscriptionState = valueObject.get(STATE).getAsString();
		if (subscriptionState.equals(SCHEDULED) || subscriptionState.equals("Retry-Scheduled")) {
			subscriptionsObject.setSubscriptionState("AUTO_RENEW_ON");
			subscriptionsObject.setState("Active");
		} else if (subscriptionState.equals(CANCELLED) || subscriptionState.equals("Failed")) {
			subscriptionsObject.setSubscriptionState("AUTO_RENEW_OFF");
			subscriptionsObject.setState("Inactive");
		}
		/**else if (subscriptionState.equals("Failed")) {
			subscriptionsObject.setSubscriptionState("PROCESSING_ERROR");
			subscriptionsObject.setState("Error");
		} */

	}

	/**
	 * getCardObject method is used to get card details from payment intent CT.
	 * 
	 * @param paymentObject
	 * @param cardObj
	 */
	private void getCardObject(JsonObject paymentObject, CreditCard cardObj) {

		if(paymentObject.has(CUSTOM) && paymentObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			JsonObject paymentFieldsObject = paymentObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(paymentFieldsObject.has("expYear")) {
				cardObj.setExpYear(paymentFieldsObject.get("expYear").getAsString());
			}
			if(paymentFieldsObject.has("expMonth")) {
				cardObj.setExpMonth(paymentFieldsObject.get("expMonth").getAsString());
			}
			if(paymentFieldsObject.has(BRAND)) {
				cardObj.setBrand(paymentFieldsObject.get(BRAND).getAsString());
			}
			if(paymentFieldsObject.has(LAST4)) {
				cardObj.setLast4(paymentFieldsObject.get(LAST4).getAsString());
			}
		}
		/**
		else {
			String interfaceId = paymentObject.get(INTERFACE_ID).getAsString();
			String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
			String paymentIntentBaseUrl = paymentIntentUrl + "/" + interfaceId;
			String paymentIntentResponse = netConnectionHelper.sendGetWithoutBody(authToken, paymentIntentBaseUrl);
			JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentIntentResponse);
			if(paymentJson.has("charges")) {
				JsonObject dataJson = paymentJson.get("charges").getAsJsonObject().get("data").getAsJsonArray().get(0)
						.getAsJsonObject();
				JsonObject cardJson = dataJson.get("payment_method_details").getAsJsonObject().get("card").getAsJsonObject();
				cardObj.setBrand(cardJson.get(BRAND).getAsString());
				cardObj.setExpMonth(cardJson.get("exp_month").getAsString());
				cardObj.setExpYear(cardJson.get("exp_year").getAsString());
				cardObj.setLast4(cardJson.get(LAST4).getAsString());
			}
		}*/
	}

	/**
	 * setShippingAddress method is used to set shipping address.
	 * 
	 * @param addressRequestBean
	 * @param shippingObject
	 */
	private void setShippingAddress(AddressRequestBean addressRequestBean, JsonObject shippingObject) {
		if (shippingObject.has("id"))
			addressRequestBean.setId(shippingObject.get("id").getAsString());
		if (shippingObject.has(FIRST_NAME))
			addressRequestBean.setFirstName(shippingObject.get(FIRST_NAME).getAsString());
		if (shippingObject.has(LAST_NAME))
			addressRequestBean.setLastName(shippingObject.get(LAST_NAME).getAsString());
		if (shippingObject.has(STREET_NAME))
			addressRequestBean.setStreetName(shippingObject.get(STREET_NAME).getAsString());
		if (shippingObject.has(STREET_NUMBER))
			addressRequestBean.setStreetNumber(shippingObject.get(STREET_NUMBER).getAsString());
		if (shippingObject.has(STATE))
			addressRequestBean.setState(shippingObject.get(STATE).getAsString());
		if (shippingObject.has(POSTAL_CODE))
			addressRequestBean.setPostalCode(shippingObject.get(POSTAL_CODE).getAsString());
		if (shippingObject.has("city"))
			addressRequestBean.setCity(shippingObject.get("city").getAsString());
		if (shippingObject.has("region"))
			addressRequestBean.setRegion(shippingObject.get("region").getAsString());
		if (shippingObject.has(COUNTRY))
			addressRequestBean.setCountry(shippingObject.get(COUNTRY).getAsString());
		if (shippingObject.has(COMPANY))
			addressRequestBean.setCompany(shippingObject.get(COMPANY).getAsString());
		if (shippingObject.has(PHONE))
			addressRequestBean.setPhone(shippingObject.get(PHONE).getAsString());
	}

	/**
	 * getSubscriptionUrl method is to form the base url for subscription.
	 * 
	 * @param customerId
	 * @param offset
	 * @param limit
	 * @return String
	 */
	private String getSubscriptionUrl(String customerId, int limit, int offset) {
		return ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + LIMIT + limit
				+ OFFSET + offset + "&expand=value.cart.id" + EXPAND_CUSTOMER_ID
				+ CUSTOMER_ID_WITH_QUERY + customerId + "\"))&where=value((state!=\"Renew\")and(state!=\"Processed\"))&sort=lastModifiedAt desc";
	}

	/**
	 * updateAutoRefills method is used to update the auto-refills.
	 * 
	 * @param customerId
	 * @param container
	 * @param key
	 * @param updateAutoRefillsRequestBean
	 * @return
	 */
	public ResponseEntity<String> updateAutoRefills(String customerId, String container, String key,
			UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean) {
		String accountbaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + "/"
				+ container + "/" + key + "?where=value(customer(id=\"" + customerId + "\"))";
		String token = ctServerHelperService.getStringAccessToken();
		String accountResponse = netConnectionHelper.sendGetWithoutBody(token, accountbaseUrl);
		JsonObject jsonAccountObj = MolekuleUtility.parseJsonObject(accountResponse);
		String response = null;
		String subscriptionUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "/" + container + "/" + key + "?expand=value.cart.id&expand=value.customer.id";

		if (updateAutoRefillsRequestBean.getAction()
				.equals(UpdateAutoRefillsRequestBean.AutoRefillsActions.UPDATE_PAYMENT_OPTION)) {
			if(updateAutoRefillsRequestBean.getChannel().equals(CustomerChannelEnum.B2B)) {
				updatePaymentOptionProcess(customerId, updateAutoRefillsRequestBean, token, jsonAccountObj);
			}else if(updateAutoRefillsRequestBean.getChannel().equals(CustomerChannelEnum.D2C)) {
				updateD2CPayment(customerId, updateAutoRefillsRequestBean, token, jsonAccountObj);
			}
		} else if (updateAutoRefillsRequestBean.getAction()
				.equals(UpdateAutoRefillsRequestBean.AutoRefillsActions.TURN_ON)) {
			String turnOnResponse = doTurnOnAutoRefills(jsonAccountObj, container, key, token);
			return new ResponseEntity<>(turnOnResponse, HttpStatus.OK);
		} else if (updateAutoRefillsRequestBean.getAction()
				.equals(UpdateAutoRefillsRequestBean.AutoRefillsActions.TURN_OFF)) {
			String turnOffResponse = doTurnOffAutoRefills(updateAutoRefillsRequestBean.getSurveyQuestionId(),
					jsonAccountObj, container, key, token, updateAutoRefillsRequestBean.getOther());
			tealiumHelperService.setTurnOffAutorenewal(updateAutoRefillsRequestBean.getSurveyQuestionId(),
					subscriptionUrl);
			return new ResponseEntity<>(turnOffResponse, HttpStatus.OK);
		} else if (updateAutoRefillsRequestBean.getAction()
				.equals(UpdateAutoRefillsRequestBean.AutoRefillsActions.UPDATE_RENEWAL_DATE)) {
			String dateResponse = updateRenewalDate(customerId, updateAutoRefillsRequestBean.getRenewalDate(),
					jsonAccountObj, container, key, token);
			return new ResponseEntity<>(dateResponse, HttpStatus.OK);
		} else if (updateAutoRefillsRequestBean.getAction()
				.equals(UpdateAutoRefillsRequestBean.AutoRefillsActions.UPDATE_QUANTITY)) {
			if (jsonAccountObj.has(VALUE) && jsonAccountObj.get(VALUE).getAsJsonObject().has("cart")) {

				String cartId = jsonAccountObj.get(VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("id")
						.getAsString();
				cartHelperService.updateCartById(cartId, updateAutoRefillsRequestBean.getUpdateCartRequestBean());
			}
		} else if (updateAutoRefillsRequestBean.getAction()
				.equals(UpdateAutoRefillsRequestBean.AutoRefillsActions.UPDATE_SHIPPING_ADDRESS)) {

			updateShippingAddress(customerId, updateAutoRefillsRequestBean, jsonAccountObj);
		}
		response = netConnectionHelper.sendGetWithoutBody(token, subscriptionUrl);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private void updateShippingAddress(String customerId, UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean,
			JsonObject jsonAccountObj) {
		if (jsonAccountObj.has(VALUE) && jsonAccountObj.get(VALUE).getAsJsonObject().has("cart")) {
			String cartId = jsonAccountObj.get(VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("id")
					.getAsString();
			cartHelperService.updateCartById(cartId, updateAutoRefillsRequestBean.getUpdateCartRequestBean());
		}
		if (Objects.isNull(updateAutoRefillsRequestBean.getShippingAddressId())) {
			registrationService
			.createShippingOptions(customerId, updateAutoRefillsRequestBean.getShippingCarrierOptionsBean())
			.getBody();
		} else {
			registrationService.updateShippingOptions(customerId,
					updateAutoRefillsRequestBean.getShippingAddressId(),
					updateAutoRefillsRequestBean.getShippingCarrierOptionsBean());
		}
	}

	private void updateD2CPayment(String customerId, UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean,
			String token, JsonObject jsonAccountObj) {
		updatePayment("updateD2CPayment", customerId, updateAutoRefillsRequestBean, token, jsonAccountObj);
	}

	public void updatePayment(String methodName, String customerId, UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean,
			String token, JsonObject jsonAccountObj) {
		String cartId = null;
		String cartResponse = null;
		JsonObject cartObject = null;
		if (jsonAccountObj.has(VALUE) && jsonAccountObj.get(VALUE).getAsJsonObject().has("cart")) {
			cartId = jsonAccountObj.get(VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("id").getAsString();
			cartResponse = cartHelperService.getCartById(cartId);
			cartObject = MolekuleUtility.parseJsonObject(cartResponse);

			String totalAmount = cartObject.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsString();
			String currencyCode = cartObject.get(TOTAL_PRICE).getAsJsonObject().get(CURRENCY_CODE).getAsString();

			Map<String, Object> paymentData = new HashMap<>();
			paymentData.put(TOTAL_AMOUNT, totalAmount);
			paymentData.put(CURRENCY_CODE, currencyCode);
			
			if(methodName.equals("updateD2CPayment")) {
				Map<String,String> values = new HashMap<>();
				values.put(CUSTOMER_ID, customerId);
				values.put(TOKEN, token);
				values.put(CART_ID, cartId);
				creditCardProcess(values, updateAutoRefillsRequestBean, cartObject, totalAmount,
						currencyCode, paymentData);
			}else if(methodName.equals("updateSubscriptionPayment")) {
				if (updateAutoRefillsRequestBean.getPaymentType().equals(CREDITCARD)) {
					Map<String,String> values = new HashMap<>();
					values.put(CUSTOMER_ID, customerId);
					values.put(TOKEN, token);
					values.put(CART_ID, cartId);
					creditCardProcess(values, updateAutoRefillsRequestBean, cartObject, totalAmount,
							currencyCode, paymentData);

				} else if (updateAutoRefillsRequestBean.getPaymentType().equals(PAYMENTTERMS)) {
					paymentData.put(PAYMENT_TYPE, PAYMENTTERMS);
					createPaymentCart(token, cartId, cartObject, "", paymentData, null, updateAutoRefillsRequestBean);
				}
			}
		}
	}

	private void updatePaymentOptionProcess(String customerId,
			UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean, String token, JsonObject jsonAccountObj) {
		if (Objects.isNull(updateAutoRefillsRequestBean.getPaymentId())) {
			// create PaymentIntent
			createCartAndAccountCreditCartPayment(customerId, updateAutoRefillsRequestBean, token, jsonAccountObj);

		} else {
			updateSubscriptionPayment(customerId, updateAutoRefillsRequestBean, token, jsonAccountObj);
		}
	}

	/**
	 * updateSubscriptionPayment method is used to update payments in broth account
	 * and subscription.
	 * 
	 * @param customerId
	 * @param updateAutoRefillsRequestBean
	 * @param token
	 * @param jsonAccountObj
	 */
	private void updateSubscriptionPayment(String customerId, UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean,
			String token, JsonObject jsonAccountObj) {
		updatePayment("updateSubscriptionPayment", customerId, updateAutoRefillsRequestBean, token, jsonAccountObj);
	}

	private void creditCardProcess(Map<String,String> values, UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean,
			JsonObject cartObject, String totalAmount, String currencyCode,
			Map<String, Object> paymentData) {
		String customerId = values.get(CUSTOMER_ID);
		String ctToken = ctServerHelperService.getStringAccessToken();
		String token = values.get(TOKEN);
		String cartId = values.get(CART_ID);
		String custom;
		String paymentId;
		if(updateAutoRefillsRequestBean.getD2CPaymentRequestBean() != null && updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getPaymentId() != null) {
			CustomerRequestBean customerRequestBean = new CustomerRequestBean();
			customerRequestBean.setAction("UPDATE_PAYMENT");
			customerRequestBean.setIsDefaultAddress(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getIsDefaultAddress());
			customerRequestBean.setBillingAddress(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getBillingAddress());
			customerRequestBean.setIsDefaultPayment(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getIsDefaultPayment());
			customerRequestBean.setPaymentType(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getPaymentType());
			customerRequestBean.setPaymentId(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getPaymentId());
			customerRequestBean.setPaymentToken(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getPaymentToken());

			custom = customerProfileHelperService.updateCustomerById(customerId, customerRequestBean,"US",ctToken,null);
			paymentId = updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getPaymentId();
		}else {
			custom = registrationService.updatePaymentOption(customerId,
					updateAutoRefillsRequestBean.getPaymentId(),
					updateAutoRefillsRequestBean.getUpdatePaymentRequest(), null);
			paymentId = updateAutoRefillsRequestBean.getPaymentId();
		}
		JsonObject customObject = MolekuleUtility.parseJsonObject(custom);
		JsonObject valueObject = customObject.get(VALUE).getAsJsonObject();
		ObjectMapper mapper = new ObjectMapper();

		Value value = new Value();
		List<Payments> paymentList = new ArrayList<>();
		value = getPaymentList(valueObject, mapper, value, paymentList);

		String paymentToken = null;
		for (Payments payment : paymentList) {
			if (payment.getPaymentId().equals(paymentId)) {
				paymentToken = payment.getPaymentToken();
				if (Objects.nonNull(updateAutoRefillsRequestBean.getUpdatePaymentRequest()) && Objects
						.isNull(updateAutoRefillsRequestBean.getUpdatePaymentRequest().getBillingAddress())) {
					BillingAddresses billingAddress = new BillingAddresses();
					billingAddress.setBillingAddressId(payment.getBillingAddressId());
					updateAutoRefillsRequestBean.getUpdatePaymentRequest().setBillingAddress(billingAddress);
				} else if(Objects.nonNull(updateAutoRefillsRequestBean.getUpdatePaymentRequest())){
					updateAutoRefillsRequestBean.getUpdatePaymentRequest().getBillingAddress()
					.setBillingAddressId(payment.getBillingAddressId());
				}
				break;
			}
		}
		JsonObject paymentIntentJsonObject = createPaymentIntent(paymentToken, totalAmount, currencyCode);
		String paymentIntentId = paymentIntentJsonObject.get("id").getAsString();
		paymentData.put(PAYMENT_TYPE, CREDITCARD);
		paymentData.put(PAYMENT_INTENT_ID, paymentIntentId);

		// Create the payment intent
		createPaymentCart(token, cartId, cartObject, paymentIntentId, paymentData, value,
				updateAutoRefillsRequestBean);
	}

	private Value getPaymentList(JsonObject valueObject, ObjectMapper mapper, Value value, List<Payments> paymentList) {
		if(!valueObject.has("companyName")) {

			PaymentCustomObject.Value paymentValue = new PaymentCustomObject.Value();
			try {
				paymentValue = mapper.readValue(valueObject.toString(), PaymentCustomObject.Value.class);
				paymentList.addAll(paymentValue.getPayments());
			} catch (JsonProcessingException e) {
				logger.error(e.getMessage());
			}

		}else {
			try {
				value = mapper.readValue(valueObject.toString(), Value.class);
				paymentList.addAll(value.getPayments());
			} catch (JsonProcessingException e) {
				logger.error(e.getMessage());
			}
		}
		return value;
	}

	/**
	 * createCartAndAccountCreditCartPayment method is used to create new card
	 * payment for subscription.
	 * 
	 * @param customerId
	 * @param updateAutoRefillsRequestBean
	 * @param token
	 * @param jsonAccountObj
	 */
	private void createCartAndAccountCreditCartPayment(String customerId,
			UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean, String token, JsonObject jsonAccountObj) {
		AccountCustomObject customObject = registrationService.createPaymentOption(customerId,
				updateAutoRefillsRequestBean.getPaymentRequestBean());
		String paymentToken = null;
		String billingAddressId = null;
		if (customObject.getValue().getPayments().get(0).getPaymentType().equals(CREDITCARD)) {
			paymentToken = customObject.getValue().getPayments().get(0).getPaymentToken();
			billingAddressId = customObject.getValue().getPayments().get(0).getBillingAddressId();
		}
		updateAutoRefillsRequestBean.getPaymentRequestBean().getBillingAddress().setBillingAddressId(billingAddressId);
		if (jsonAccountObj.has(VALUE) && jsonAccountObj.get(VALUE).getAsJsonObject().has("cart")) {

			String cartId = jsonAccountObj.get(VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("id")
					.getAsString();
			String cartResponse = cartHelperService.getCartById(cartId);
			JsonObject cartObject = MolekuleUtility.parseJsonObject(cartResponse);
			String totalAmount = cartObject.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsString();
			String currencyCode = cartObject.get(TOTAL_PRICE).getAsJsonObject().get(CURRENCY_CODE).getAsString();
			// Create the payment intent
			JsonObject paymentIntentJsonObject = createPaymentIntent(paymentToken, totalAmount, currencyCode);
			String paymentIntentId = paymentIntentJsonObject.get("id").getAsString();

			// Create Payment
			Map<String, Object> paymentData = new HashMap<>();
			paymentData.put(PAYMENT_INTENT_ID, paymentIntentId);
			paymentData.put(TOTAL_AMOUNT, totalAmount);
			paymentData.put(CURRENCY_CODE, currencyCode);
			Value value = customObject.getValue();
			createPaymentCart(token, cartId, cartObject, paymentIntentId, paymentData, value,
					updateAutoRefillsRequestBean);

		}
	}

	/**
	 * createPaymentIntent methos is used to create paymentIntentObject from
	 * paymentToken.
	 * 
	 * @param paymentToken
	 * @param totalAmount
	 * @param currencyCode
	 * @return
	 */
	private JsonObject createPaymentIntent(String paymentToken, String totalAmount, String currencyCode) {
		String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
		String setupPaymentIntentResponse = getSetUpIntentResponse(paymentToken, authToken);
		JsonObject setupIntentJsonObject = MolekuleUtility.parseJsonObject(setupPaymentIntentResponse);
		String paymentMethodId = setupIntentJsonObject.get(PAYMENT_METHOD).getAsString();
		MultiValueMap<String, String> intentFormData = new LinkedMultiValueMap<>();
		intentFormData.add("amount", totalAmount);
		intentFormData.add("currency", currencyCode);
		intentFormData.add("capture_method", "manual");
		intentFormData.add(PAYMENT_METHOD, paymentMethodId);
		intentFormData.add("confirm", "true");
		intentFormData.add(CUSTOMER, setupIntentJsonObject.get(CUSTOMER).getAsString());
		String paymentIntentResponse = (String) netConnectionHelper.sendFormUrlEncoded(paymentIntentUrl, authToken,
				intentFormData);
		return MolekuleUtility.parseJsonObject(paymentIntentResponse);
	}

	private String getSetUpIntentResponse(String paymentToken, String authToken) {
		String retrievePaymentIntentBaseUrl = setupPaymentIntentUrl + "/" + paymentToken;
		return netConnectionHelper.sendGetWithoutBody(authToken, retrievePaymentIntentBaseUrl);
	}

	private void createPaymentCart(String token, String cartId, JsonObject cartObject, String paymentIntentId,
			Map<String, Object> paymentData, Value value, UpdateAutoRefillsRequestBean updateAutoRefillsRequestBean) {
		Payment payment = customerProfileUtility.createPaymentRequest(paymentData);
		String paymentResponse;
		String paymentCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments";
		if (StringUtils.hasText(paymentIntentId)) {
			payment.setInterfaceId(paymentIntentId);
			paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentCtUrl, token, payment);
		} else {
			TermsPayment termsPayment = new TermsPayment();
			termsPayment.setAmountPlanned(payment.getAmountPlanned());
			termsPayment.setPaymentMethodInfo(payment.getPaymentMethodInfo());
			termsPayment.setTransactions(payment.getTransactions());
			paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentCtUrl, token, termsPayment);

		}
		JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
		String paymentId = paymentJson.get("id").getAsString();
		// Update payment to cart
		UpdateActionsOnCart addPaymentToCart = new UpdateActionsOnCart();
		addPaymentToCart.setVersion(cartObject.get(VERSION).getAsLong());
		List<UpdateActionsOnCart.Action> actions = new ArrayList<>();
		UpdateActionsOnCart.Action addPaymentAction = customerProfileUtility.getAddPaymentRequestObj(paymentId);
		actions.add(addPaymentAction);
		String billingAddressId = null;
		if (Objects.nonNull(updateAutoRefillsRequestBean.getPaymentType()) && updateAutoRefillsRequestBean.getPaymentType().equals(CREDITCARD)) {
			if (Objects.nonNull(updateAutoRefillsRequestBean.getPaymentRequestBean())) {
				billingAddressId = updateAutoRefillsRequestBean.getPaymentRequestBean().getBillingAddress()
						.getBillingAddressId();
			} else if (Objects.nonNull(updateAutoRefillsRequestBean.getUpdatePaymentRequest())) {
				billingAddressId = updateAutoRefillsRequestBean.getUpdatePaymentRequest().getBillingAddress()
						.getBillingAddressId();
			}
			UpdateActionsOnCart.Action setBillingAddressAction = customerProfileUtility
					.getBillingAddressAction(billingAddressId, value);
			if (setBillingAddressAction != null) {
				actions.add(setBillingAddressAction);
			}
		}else if(Objects.nonNull(updateAutoRefillsRequestBean.getD2CPaymentRequestBean()) 
				&& Objects.nonNull(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getBillingAddress()) 
				&& updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getBillingAddress().getId() != null) {
			UpdateActionsOnCart.Action setBillingAddressAction = customerProfileUtility
					.getD2CBillingAddressAction(updateAutoRefillsRequestBean.getD2CPaymentRequestBean().getBillingAddress());
			if (setBillingAddressAction != null) {
				actions.add(setBillingAddressAction);
			}
		}
		addPaymentToCart.setActions(actions);
		String updateCartCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId;
		netConnectionHelper.sendPostRequest(updateCartCtUrl, token, addPaymentToCart);
	}

	/**
	 * doTurnOffAutoRefills method is used to turn off/ cancel the subscription.
	 * 
	 * @param string
	 * 
	 * @param jsonAccountObj
	 * @param container
	 * @param key
	 * @param token
	 * @return
	 */
	private String doTurnOffAutoRefills(String surveyQuestionId, JsonObject jsonAccountObj, String container,
			String key, String token, String other) {
		return customerProfileHelperService.doTurnOffAutoRefills(surveyQuestionId, jsonAccountObj, container, key, token, other);
	}

	/**
	 * doTurnOnAutoRefills method is used to turn on/ schedule the subscription.
	 * 
	 * @param jsonAccountObj
	 * @param container
	 * @param key
	 * @param token
	 * @return
	 */
	private String doTurnOnAutoRefills(JsonObject jsonAccountObj, String container, String key, String token) {

		ObjectMapper mapper = new ObjectMapper();
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		JsonObject valueObject = jsonAccountObj.get(VALUE).getAsJsonObject();
		SubscriptionCustomObject customObject = new SubscriptionCustomObject();
		SubscriptionCustomObject.Value value = new SubscriptionCustomObject.Value();
		String response;
		try {
			value = mapper.readValue(valueObject.toString(), SubscriptionCustomObject.Value.class);

		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		if(value.getState().equals(SCHEDULED)) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "Subscription is already in scheduled state");
			return errorResponse.toString();
		}
		String[] dateSplitArray = value.getNextOrderDate().split("-");
		int year = Integer.parseInt(dateSplitArray[0]);
		int month = Integer.parseInt(dateSplitArray[1]);
		int day = Integer.parseInt(dateSplitArray[2]);
		Calendar nextOrderDateCalendar = new GregorianCalendar(year, month - 1, day);
		LocalDate currentDate = LocalDate.now();
		Calendar currentDateCalendar = new GregorianCalendar(currentDate.getYear(), currentDate.getMonthValue() - 1,
				currentDate.getDayOfMonth());

		if (currentDateCalendar.compareTo(nextOrderDateCalendar) < 0) {
			// Current Date is less than nextOrderDate

			value.setState(SCHEDULED);
			List<TransactionObject> transactionList = new ArrayList<>();
			if(!value.getTransactions().isEmpty() && Objects.nonNull(value.getTransactions()) && value.getTransactions().get(0).getMessage() != null && value.getTransactions().get(0).getType() !=null) {
				transactionList = value.getTransactions();
			}
			TransactionObject transactionObject = new TransactionObject();
			transactionObject.setType("scheduled");
			transactionObject.setMessage("Subscription Turned On");

			transactionList.add(transactionObject);
			value.setTransactions(transactionList);
			customObject.setContainer(container);
			customObject.setKey(key);
			customObject.setVersion(jsonAccountObj.get(MolekuleConstant.VERSION).getAsLong());
			customObject.setValue(value);
			response = (String) netConnectionHelper.sendPostRequest(accountBaseUrl, token, customObject);
		} else {
			// Current Date is later than nextOrderDate
			nextOrderDateCalendar.add(Calendar.MONTH, 6);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy'-'MM'-'dd");
			String renewalDate = simpleDateFormat.format(nextOrderDateCalendar.getTime());
			response = updateRenewalDate(value.getCustomer().getId(), renewalDate, jsonAccountObj, container, key,
					token);
		}
		//sendgrid implementation
		SendGridModel sendGridModel = new SendGridModel();
		String subscriptionUrl = ctEnvProperties.getHost()+ "/" +ctEnvProperties.getProjectKey()+CUSTOM_OBJECTS+"/"+container+"/"+key+
				"?expand=value.cart.id" + EXPAND_CUSTOMER_ID + CUSTOMER_ID_WITH_QUERY + value.getCustomer().getId() + "\"))";
		String subscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, subscriptionUrl);
		JsonObject subscriptionjsonObj = MolekuleUtility.parseJsonObject(subscriptionResponse);
		JsonObject subscriptionValueObj = subscriptionjsonObj.get(VALUE).getAsJsonObject();
		sendGridModel.setSubscriptionPlanName(subscriptionValueObj.get(PRODUCT).getAsJsonObject().get("description").getAsJsonObject().get(EN_US).getAsString());
		sendGridModel.setSubscriptionRenewalDate(subscriptionValueObj.get("nextOrderDate").getAsString());
		JsonArray lineItemArray = subscriptionValueObj.getAsJsonObject("cart").getAsJsonObject("obj").getAsJsonArray(LINE_ITEMS);
		for(int i =0 ;i<lineItemArray.size();i++) {
			if (lineItemArray.get(i).getAsJsonObject().has(CUSTOM)) {
				JsonObject customObj=lineItemArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
				if(customObj.has(SERIAL_NUMBERS)) {
					sendGridModel.setSubscriptionDeviceSerial(customObj.get(SERIAL_NUMBERS).getAsJsonArray().get(0).getAsString());
				}
			}
		}
		JsonObject subscriptionShippingObj = subscriptionValueObj.getAsJsonObject("cart").getAsJsonObject("obj").getAsJsonObject(SHIPPING_ADDRESS);
		sendGridModel.setSubscriptionPrice(centToDollarConversion(subscriptionValueObj.getAsJsonObject("cart").getAsJsonObject("obj")
				.getAsJsonObject(CUSTOM).getAsJsonObject("fields").get("subTotal").getAsLong()));
		sendGridModel.setFirstName(subscriptionShippingObj.get("firstName").getAsString());
		sendGridModel.setLastName(subscriptionShippingObj.get(LAST_NAME).getAsString());
		sendGridModel.setEmail(subscriptionValueObj.getAsJsonObject("cart").getAsJsonObject("obj").get("customerEmail").getAsString());
		sendGridModel.setOrderShippingAddressStreet1(subscriptionShippingObj.get(STREET_NAME).getAsString());
		sendGridModel.setOrderShippingAddressStreet2(subscriptionShippingObj.get(STREET_NUMBER).getAsString());
		sendGridModel.setOrderShippingAddressCity(subscriptionShippingObj.get("city").getAsString());
		sendGridModel.setOrderShippingAddressState(subscriptionShippingObj.get("state").getAsString());
		sendGridModel.setOrderShippingAddressZip(subscriptionShippingObj.get(POSTAL_CODE).getAsString());
		sendGridModel.setOrderShippingAddressCountry(subscriptionShippingObj.get(COUNTRY).getAsString());
		if(sendGridModel != null) {
			autoRefilActivationMailSender.sendMail(sendGridModel);
		}
		return response;
	}

	public static String centToDollarConversion(long centPayment) {
		NumberFormat numberFormat = null;
		if(Locale.getDefault().getCountry().equals("US")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		}else if(Locale.getDefault().getCountry().equals("GB")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		}
		return numberFormat!= null ? numberFormat.format(centPayment / 100.0) : null;
	}

	/**
	 * updateRenewalDate method is used to update nextOrderDate in CT.
	 * 
	 * @param renewalDate
	 * @param jsonAccountObj
	 * @param container
	 * @param key
	 * @param token
	 * @return
	 */
	private String updateRenewalDate(String customerId, String renewalDate, JsonObject jsonAccountObj, String container,
			String key, String token) {
		ObjectMapper mapper = new ObjectMapper();
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		JsonObject valueObject = jsonAccountObj.get(VALUE).getAsJsonObject();

		// creating new subscription object with renewal date
		SubscriptionCustomObject renewalCustomObject = new SubscriptionCustomObject();
		SubscriptionCustomObject.Value renewalValue = new SubscriptionCustomObject.Value();
		try {
			renewalValue = mapper.readValue(valueObject.toString(), SubscriptionCustomObject.Value.class);
			renewalValue.setState(SCHEDULED);
			renewalValue.setNextOrderDate(renewalDate);
			renewalValue.setRenewalReference(jsonAccountObj.get("id").getAsString());

		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}

		List<TransactionObject> renewalTransactionList = new ArrayList<>();
		if(!renewalValue.getTransactions().isEmpty() && Objects.nonNull(renewalValue.getTransactions()) && renewalValue.getTransactions().get(0).getMessage() != null && renewalValue.getTransactions().get(0).getType() !=null) {
			renewalTransactionList = renewalValue.getTransactions();
		}

		TransactionObject renewalTransactionObject = new TransactionObject();
		renewalTransactionObject.setType("scheduled");
		renewalTransactionObject.setMessage("Subscription scheduled for updating nextOrderDate");
		renewalTransactionList.add(renewalTransactionObject);
		renewalValue.setTransactions(renewalTransactionList);


		String[] arrOfStr = renewalDate.split("-", 2);
		renewalCustomObject.setContainer("b2b-sub-" + arrOfStr[1]);
		renewalCustomObject.setKey(UUID.randomUUID().toString());
		renewalCustomObject.setValue(renewalValue);
		String response = (String) netConnectionHelper.sendPostRequest(accountBaseUrl, token, renewalCustomObject);

		// updating existing subscription state as Renew
		SubscriptionCustomObject existingCustomObject = new SubscriptionCustomObject();
		SubscriptionCustomObject.Value existingValue = new SubscriptionCustomObject.Value();
		try {
			existingValue = mapper.readValue(valueObject.toString(), SubscriptionCustomObject.Value.class);
			existingValue.setState("Renew");
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}

		List<TransactionObject> transactionList = new ArrayList<>();
		if(!existingValue.getTransactions().isEmpty() && Objects.nonNull(existingValue.getTransactions()) && existingValue.getTransactions().get(0).getMessage() != null && existingValue.getTransactions().get(0).getType() !=null) {
			transactionList = existingValue.getTransactions();
		}
		TransactionObject transactionObject = new TransactionObject();
		transactionObject.setType("renewed");
		transactionObject.setMessage("NextOrderDate Updated with new date");
		transactionList.add(transactionObject);
		existingValue.setTransactions(transactionList);

		existingCustomObject.setContainer(container);
		existingCustomObject.setKey(key);
		existingCustomObject.setVersion(jsonAccountObj.get(MolekuleConstant.VERSION).getAsLong());
		existingCustomObject.setValue(existingValue);
		netConnectionHelper.sendPostRequest(accountBaseUrl, token, existingCustomObject);
		SendGridModel sendGirdModel = registrationHelperService.createEmailDynamicInfo(customerId);
		sendGirdModel.setChannel(valueObject.get(CHANNEL).getAsString());
		if(sendGirdModel != null) {
			subScriptionRenewalDateUpdationSender.sendMail(sendGirdModel);
		}
		return response;
	}

	/**
	 * getSubscriptionSurveyResponseUrl method is to form the base url for
	 * subscription Surveys Response.
	 * 
	 * @return String
	 */
	private String getSubscriptionSurveyResponseUrl() {
		return customerProfileHelperService.getSubscriptionSurveyResponseUrl();
	}

	/**
	 * getAllSurveysResponse method is to get all subscriptions survey questions.
	 * @param channel 
	 * 
	 * @return ResponseEntity<String>
	 */
	public ResponseEntity<SubscriptionSurveyResponse> getAllSurveysResponse(CustomerChannelEnum channel) {
		return customerProfileHelperService.getAllSurveysResponse(channel);
	}

	/**
	 * getAllSurveysResponse method is to get all subscriptions survey questions.
	 * 
	 * @return ResponseEntity<String>
	 */
	public SubscriptionSurveyResponse getSurveysResponseById(String id) {
		SubscriptionSurveyResponse subscriptionSurveyResponse = getAllSurveysResponse(null).getBody();

		if (Objects.nonNull(subscriptionSurveyResponse)
				&& Objects.nonNull(subscriptionSurveyResponse.getSurveyQuestions())) {
			List<SurveyQuestion> surveyQuestions = subscriptionSurveyResponse.getSurveyQuestions();
			surveyQuestions = surveyQuestions.stream().filter(survey -> survey.getId().equals(id))
					.collect(Collectors.toList());
			subscriptionSurveyResponse.setSurveyQuestions(surveyQuestions);
		}
		return subscriptionSurveyResponse;
	}

	/**
	 * getSubscriptionSurveyResponse method is to map SubscriptionSurveyResponse to
	 * response.
	 * 
	 * @return SubscriptionSurveyResponse
	 */

	public SubscriptionSurveyResponse getSubscriptionSurveyResponse(String response) {
		return customerProfileHelperService.getSubscriptionSurveyResponse(response);
	}

	/**
	 * updateSubcriptionSurveyQuestion method is to update survey question
	 * SurveyObject.
	 * 
	 * @return ResponseEntity<SubscriptionSurveyResponse>
	 */
	public ResponseEntity<SubscriptionSurveyResponse> updateSubcriptionSurveyQuestion(String id, String question) {

		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, getSubscriptionSurveyResponseUrl());

		List<SurveyQuestion> surveyQuestions = getSubscriptionSurveyResponse(response).getSurveyQuestions();

		surveyQuestions.stream().filter(survey -> survey.getId().equals(id))
		.forEach(survey -> survey.setQuestion(question));
		String surveyResponse = updateSubcriptionSurveyObject(response, surveyQuestions);
		return new ResponseEntity<>(getSubscriptionSurveyResponse(surveyResponse), HttpStatus.OK);

	}

	/**
	 * addSubcriptionSurveyQuestion method is to add new survey question in
	 * SurveyObject.
	 * @param channel 
	 * 
	 * @return ResponseEntity<SubscriptionSurveyResponse>
	 */
	public ResponseEntity<Object> addSubcriptionSurveyQuestion(String question, CustomerChannelEnum channel) {

		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, getSubscriptionSurveyResponseUrl());

		List<SurveyQuestion> surveyQuestions = getSubscriptionSurveyResponse(response).getSurveyQuestions();
		int surveyLimit = surveyQuestions.size();

		if(channel != null) {
			ResponseEntity<SubscriptionSurveyResponse> allSurvey = getAllSurveysResponse(channel);
			if(allSurvey.hasBody() && allSurvey.getBody() != null) {
				SubscriptionSurveyResponse subscriptionSurveyResponse = allSurvey.getBody();
				surveyLimit = subscriptionSurveyResponse != null?  subscriptionSurveyResponse.getSurveyQuestions().size() : surveyLimit;
			}
		}
		if(surveyLimit >= 10) {
			ErrorResponse errorResponse = new ErrorResponse(400, "Reached the limit to survey questions");
			return new ResponseEntity<>(errorResponse,HttpStatus.BAD_REQUEST);
		}	
		SurveyQuestion surveyQuestion= new SurveyQuestion();
		surveyQuestion.setId(MolekuleUtility.createUuidValue());
		surveyQuestion.setQuestion(question);
		surveyQuestion.setChannel(channel != null ? channel.name() : null);
		surveyQuestions.add(surveyQuestion);
		String surveyResponse = updateSubcriptionSurveyObject(response, surveyQuestions);

		return new ResponseEntity<>(getSubscriptionSurveyResponse(surveyResponse), HttpStatus.OK);

	}

	/**
	 * updateSubcriptionSurveyObject method is to update SurveyObject in
	 * accountcustomobject
	 * 
	 * @return ResponseEntity<SubscriptionSurveyResponse>
	 */
	public String updateSubcriptionSurveyObject(String response, List<SurveyQuestion> surveyQuestions) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		Value value = new Value();
		AccountCustomObject accountCustomObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
			value.setSurveyQuestions(surveyQuestions);
			accountCustomObject.setValue(value);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured in updateSubcriptionSurveyObject ", e);
		}
		accountCustomObject.setContainer(jsonObject.get(CONTAINER).getAsString());
		accountCustomObject.setKey(jsonObject.get("key").getAsString());
		accountCustomObject.setVersion(jsonObject.get(VERSION).getAsInt());
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		// updating survey custom object in CT server.
		return (String) registrationService.sendPostRequest(accountBaseUrl,
				ctServerHelperService.getStringAccessToken(), accountCustomObject);

	}

	/**
	 * updateBulkSubscription method is used to update subscription in bulk.
	 * 
	 * @param bulkUpdateActionEnum
	 * @param bulkUpdateRequestBean
	 * @return
	 */
	public ResponseEntity<String> updateBulkSubscription(BulkUpdateActionEnum bulkUpdateActionEnum,
			BulkUpdateRequestBean bulkUpdateRequestBean) {
		String token = ctServerHelperService.getStringAccessToken();
		String response = null;
		String container = null;
		String key = null;
		if (bulkUpdateActionEnum.equals(BulkUpdateActionEnum.UPDATE_NEXT_ORDER_DATE)) {
			return updateBulkRenewalDate(bulkUpdateRequestBean);
		} else if (bulkUpdateActionEnum.equals(BulkUpdateActionEnum.TURN_ON)) {
			for (int i = 0; i < bulkUpdateRequestBean.getSubscriptionIdList().size(); i++) {
				String accountbaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ CUSTOM_OBJECTS + LIMIT + bulkUpdateRequestBean.getLimit() + OFFSET
				+ bulkUpdateRequestBean.getOffset()
				+ "&where=value(state=\"Cancelled\")&where=value(subscription=true)&where=id=\""
				+ bulkUpdateRequestBean.getSubscriptionIdList().get(i) + "\"";
				String accountSubscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, accountbaseUrl);
				JsonObject jsonAccountObj = MolekuleUtility.parseJsonObject(accountSubscriptionResponse);	
				if( jsonAccountObj.get(RESULTS).getAsJsonArray().size()>0) {
					JsonObject resultObject = jsonAccountObj.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
					container = resultObject.get(CONTAINER).getAsString();
					key = resultObject.get("key").getAsString();
					doTurnOnAutoRefills(resultObject, container, key, token);
				}
			}
			String responseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
					+ LIMIT + bulkUpdateRequestBean.getLimit() + OFFSET + bulkUpdateRequestBean.getOffset()
					+ "&sort=lastModifiedAt desc&where=value(state=\"Scheduled\")&where=value(subscription=true)";
			response = netConnectionHelper.sendGetWithoutBody(token, responseUrl);
		} else if (bulkUpdateActionEnum.equals(BulkUpdateActionEnum.TURN_OFF)) {
			response = customerProfileHelperService.bulkSubscriptionTurnOff(bulkUpdateRequestBean, token);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * updateBulkRenewal method is used to update renewal date in bulk
	 * subscriptions.
	 * 
	 * @param bulkUpdateRequestBean
	 * @return ResponseEntity<String>
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ResponseEntity<String> updateBulkRenewalDate(BulkUpdateRequestBean bulkUpdateRequestBean) {
		String accountbaseUrl;
		String token = ctServerHelperService.getStringAccessToken();
		JsonObject jsonAccountObj = null;
		if (StringUtils.hasText(bulkUpdateRequestBean.getNextOrderDate())) {
			accountbaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + LIMIT
					+ bulkUpdateRequestBean.getLimit() + OFFSET + bulkUpdateRequestBean.getOffset()
					+ "&sort=lastModifiedAt desc&where=value((state=\"Scheduled\")or(state=\"Retry-Scheduled\"))"
					+ "&where=value(subscription=true)" + NEXT_ORDER_DATE_WITH_QUERY
					+ bulkUpdateRequestBean.getNextOrderDate() + "\")";

			String accountSubscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, accountbaseUrl);
			jsonAccountObj = MolekuleUtility.parseJsonObject(accountSubscriptionResponse);
			if (jsonAccountObj.get(TOTAL).getAsLong() == 0) {
				ErrorResponse errorResponse = new ErrorResponse(3000,
						"No Subscription available for given nextOrderDate");
				return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
			}
			updateBulkRenewal(bulkUpdateRequestBean.getNextOrderRenewalDate(), token, jsonAccountObj);
		} else if (Objects.nonNull(bulkUpdateRequestBean.getSubscriptionIdList())) {

			for (int i = 0; i < bulkUpdateRequestBean.getSubscriptionIdList().size(); i++) {
				accountbaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
						+ LIMIT + bulkUpdateRequestBean.getLimit() + OFFSET + bulkUpdateRequestBean.getOffset()
						+ "&where=value((state=\"Scheduled\")or(state=\"Retry-Scheduled\"))&where=value(subscription=true)"
						+ ID_WITH_QUERY + bulkUpdateRequestBean.getSubscriptionIdList().get(i) + "\"";
				String accountSubscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, accountbaseUrl);
				jsonAccountObj = MolekuleUtility.parseJsonObject(accountSubscriptionResponse);
				updateBulkRenewal(bulkUpdateRequestBean.getNextOrderRenewalDate(), token, jsonAccountObj);
			}
		}
		String responseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + LIMIT
				+ bulkUpdateRequestBean.getLimit() + OFFSET + bulkUpdateRequestBean.getOffset()
				+ "&sort=lastModifiedAt desc&where=value((state=\"Scheduled\")or(state=\"Retry-Scheduled\"))&where=value(subscription=true)"
				+ NEXT_ORDER_DATE_WITH_QUERY + bulkUpdateRequestBean.getNextOrderRenewalDate() + "\")";
		String response = netConnectionHelper.sendGetWithoutBody(token, responseUrl);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * updateBulkRenewal method is used to update renewaldate in bulk.
	 * 
	 * @param nextOrderNenewalDate
	 * @param token
	 * @param jsonAccountObj
	 */
	private void updateBulkRenewal(String nextOrderNenewalDate, String token,
			JsonObject jsonAccountObj) {
		JsonArray resultsArray= jsonAccountObj.get(RESULTS).getAsJsonArray();
		for(int i=0; i<resultsArray.size(); i++) {
			JsonObject resultObject = resultsArray.get(i).getAsJsonObject();
			String container = resultObject.get(CONTAINER).getAsString();
			String key = resultObject.get("key").getAsString();
			String singleSubscriptionUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ CUSTOM_OBJECTS + "/" + container + "/" + key;
			String subscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, singleSubscriptionUrl);
			JsonObject subscriptionObject = MolekuleUtility.parseJsonObject(subscriptionResponse);
			String customerId = subscriptionObject.get(VALUE).getAsJsonObject().get(CUSTOMER).getAsJsonObject()
					.get("id").getAsString();
			updateRenewalDate(customerId, nextOrderNenewalDate, subscriptionObject, container, key, token);
		}
	}

	/**
	 * getSubscriptionsById method issue to get specific subscriptions data.
	 * 
	 * @param subscriptionId
	 * @return ResponseEntity<AutoRefillsResponseBean>
	 */
	public ResponseEntity<AutoRefillsResponseBean> getSubscriptionsById(String subscriptionId) {
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?expand=value.cart.id" + EXPAND_CUSTOMER_ID + "&where=value(subscription=true)&where=id=\""
				+ subscriptionId + "\"";
		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, baseUrl);
		AutoRefillsResponseBean autoRefillsResponseBean = getAutoRefillsResponse(token, response);
		return new ResponseEntity<>(autoRefillsResponseBean, HttpStatus.OK);
	}

	/**
	 * configureDeviceFilterSubscriptionJob method is used to configure subscription job.
	 * 
	 * @param subscriptionConfiguration
	 * @return ResponseEntity<String>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<String> configureDeviceFilterSubscriptionJob(
			SubscriptionConfiguration subscriptionConfiguration) {
		String token = ctServerHelperService.getStringAccessToken();
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		SubscriptionConfigurationDto configurationObject = new SubscriptionConfigurationDto();
		SubscriptionConfigurationDto.Value value = new SubscriptionConfigurationDto.Value();

		String accountGetUrl = accountBaseUrl + "/b2b-sub/configuration";
		String getResponse = netConnectionHelper.sendGetWithoutBody(token, accountGetUrl);
		JsonObject responseJson = MolekuleUtility.parseJsonObject(getResponse);
		if(responseJson.has(STATUS_CODE)) {
			ErrorResponse errorResponse = new ErrorResponse(3000,
					"Subscription Object is not available for configuration");
			return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		}
		JsonObject valueObject = responseJson.get(VALUE).getAsJsonObject();

		ObjectMapper mapper = new ObjectMapper();
		try {
			value = mapper.readValue(valueObject.toString(), SubscriptionConfigurationDto.Value.class);

		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}

		Blackout blackout = new Blackout();
		blackout.setStartTime(subscriptionConfiguration.getBlackoutStartTime());
		blackout.setEndTime(subscriptionConfiguration.getBlackoutEndTime());
		value.setBlackout(blackout);
		RetryDelay retryDelay = new RetryDelay();
		retryDelay.setFirst(subscriptionConfiguration.getFirstPaymentRetryDays());
		retryDelay.setSecond(subscriptionConfiguration.getSecondPaymentRetryDays());
		retryDelay.setThird(subscriptionConfiguration.getThirdPaymentRetryDays());
		value.setRetryDelay(retryDelay);
		Shipping shipping = new Shipping();
		Option option = new Option();
		option.setName(subscriptionConfiguration.getShippingMethodName());
		option.setFlag(subscriptionConfiguration.isShippingPriceOveride());
		shipping.setOption(option);
		value.setShipping(shipping);

		configurationObject.setContainer("b2b-sub");
		configurationObject.setKey("configuration");
		configurationObject.setVersion(responseJson.get(VERSION).getAsLong());
		configurationObject.setValue(value);
		String response = (String) netConnectionHelper.sendPostRequest(accountBaseUrl, token, configurationObject);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * getDeviceFilterSubscriptionJob method is used to get subscription job.
	 * 
	 * @param subscriptionConfiguration
	 * @return ResponseEntity<String>
	 */
	public ResponseEntity<String> getDeviceFilterSubscriptionJob() {
		String token = ctServerHelperService.getStringAccessToken();
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + "/b2b-sub/configuration";
		String response = netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public String updateCustomerById(String customerId, CustomerRequestBean customerRequestBean,String country) {
		String ctToken = ctServerHelperService.getStringAccessToken();
		return customerProfileHelperService.updateCustomerById(customerId, customerRequestBean,country,ctToken,customerId);
	}

	public ResponseEntity<String> registerDevice(DeviceRequestBean deviceRequestBean) {
		String token =  ctServerHelperService.getStringAccessToken();
		return CustomerProfileHelperService.registerDevice(deviceRequestBean, token,ctEnvProperties.getHost() ,ctEnvProperties.getProjectKey(), netConnectionHelper);
	}

	public ResponseEntity<String> getAllDevices(Map<String, String> filterRequestMap) {
		String token =  ctServerHelperService.getStringAccessToken();

		String customObjectDeviceUrl = getAllDeviceUrl(filterRequestMap);

		String response = netConnectionHelper.sendGetWithoutBody(token, customObjectDeviceUrl);

		if(StringUtils.hasText(filterRequestMap.get(DEVICE_ID))){
			JsonObject responseObject = MolekuleUtility.parseJsonObject(response);
			JsonObject valueObject = responseObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject().get(VALUE).getAsJsonObject();
			ObjectMapper mapper = new ObjectMapper();
			DeviceCustomObject.Value value = new DeviceCustomObject.Value();
			try {
				value = mapper.readValue(valueObject.toString(), DeviceCustomObject.Value.class);
			} catch (JsonProcessingException e) {
				logger.error(e.getMessage());
			}
			if(!StringUtils.hasText(value.getSerialNumber())) {
				String orderUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders"
						+ "?sort=createdAt desc";
				String orderResponse = netConnectionHelper.sendGetWithoutBody(token, orderUrl);
				JsonArray orderResultArray = MolekuleUtility.parseJsonObject(orderResponse).get(RESULTS).getAsJsonArray();
				JsonObject orderObject = new JsonObject();
				if(orderResultArray.size()>0) {
					orderObject = orderResultArray.get(0).getAsJsonObject();
				}
				JsonArray lineItemArray = orderObject.get(LINE_ITEMS).getAsJsonArray();
				setDeviceSerialNumber(value, lineItemArray);
			}
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public void setDeviceSerialNumber(DeviceCustomObject.Value value, JsonArray lineItemArray) {
		for(int i=0; i<lineItemArray.size(); i++) {
			JsonObject lineItemObject = lineItemArray.get(i).getAsJsonObject();
			if(lineItemObject.has("variant")) {
				JsonObject variantObject = lineItemObject.get("variant").getAsJsonObject();
				String sku = variantObject.has("sku") ? variantObject.get("sku").getAsString() : null;
				if(StringUtils.hasText(sku) && sku.equals(value.getSku()) 
						&& lineItemObject.has(CUSTOM) && lineItemObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
					JsonObject customField = lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
					updateSerialNumber(value, customField);
				}
			}
		}
	}

	public void updateSerialNumber(DeviceCustomObject.Value value, JsonObject customField) {
		String serialNumber;
		if(customField.has(SERIAL_NUMBERS)) {
			serialNumber = customField.get(SERIAL_NUMBERS).getAsJsonArray().size()>0 ? customField.get(SERIAL_NUMBERS).getAsJsonArray().get(0).getAsString() : null;
			DeviceRequestBean deviceRequestBean = new DeviceRequestBean();
			deviceRequestBean.setSerialNumber(serialNumber);
			updateDevice(value.getDeviceId(), deviceRequestBean);
		}
	}

	private String getAllDeviceUrl(Map<String, String> filterRequestMap) {
		String customObjectDeviceUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS+ "/d2c_device?sort=createdAt desc";
		if(StringUtils.hasText(filterRequestMap.get(DEVICE_ID))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=value(deviceId=\"" + filterRequestMap.get(DEVICE_ID) + "\")";
		}
		if(StringUtils.hasText(filterRequestMap.get("serialNumber"))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=value(serialNumber=\"" + filterRequestMap.get("serialNumber") + "\")";
		}
		if(StringUtils.hasText(filterRequestMap.get("sku"))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=value(sku=\"" + filterRequestMap.get("sku") + "\")";
		}
		if(StringUtils.hasText(filterRequestMap.get("purchaseDate"))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=value(purchaseDate=\"" + filterRequestMap.get("purchaseDate") + "\")";
		}
		if(StringUtils.hasText(filterRequestMap.get(CUSTOMER_ID))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=value(customerId=\"" + filterRequestMap.get(CUSTOMER_ID) + "\")";
		}
		if(StringUtils.hasText(filterRequestMap.get(CREATED_AT))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=createdAt=\"" + filterRequestMap.get(CREATED_AT) + "\"";
		}
		if(StringUtils.hasText(filterRequestMap.get("updatedAt"))){
			customObjectDeviceUrl = customObjectDeviceUrl + "&where=lastModifiedAt=\"" + filterRequestMap.get("updatedAt") + "\"";
		}
		return customObjectDeviceUrl;
	}

	public ResponseEntity<String> getDeviceDetail(String deviceId) {
		Map<String, String> filterRequestMap = new HashMap<>();
		filterRequestMap.put(DEVICE_ID, deviceId);
		return getAllDevices(filterRequestMap);
	}

	public ResponseEntity<String> updateDevice(String deviceId, DeviceRequestBean deviceRequestBean) {
		String token =  ctServerHelperService.getStringAccessToken();
		String customObjectDeviceUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS+ "/d2c_device/" + deviceId;
		String response = netConnectionHelper.sendGetWithoutBody(token, customObjectDeviceUrl);
		JsonObject responseObject = MolekuleUtility.parseJsonObject(response);
		JsonObject valueObject = responseObject.get(VALUE).getAsJsonObject();
		ObjectMapper mapper = new ObjectMapper();
		DeviceCustomObject deviceCustomObject = new DeviceCustomObject();
		deviceCustomObject.setContainer("d2c_device");
		deviceCustomObject.setKey(deviceId);
		DeviceCustomObject.Value value = new DeviceCustomObject.Value();
		try {
			value = mapper.readValue(valueObject.toString(), DeviceCustomObject.Value.class);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		if(deviceRequestBean.getSerialNumber() != null) {
			value.setSerialNumber(deviceRequestBean.getSerialNumber());
		}
		if(deviceRequestBean.getSku() != null) {
			value.setSku(deviceRequestBean.getSku());
		}
		if(deviceRequestBean.getPurchaseDate() != null) {
			value.setPurchaseDate(deviceRequestBean.getPurchaseDate());
		}
		if(deviceRequestBean.getChannel() != null) {
			value.setChannel(deviceRequestBean.getChannel());
		}
		if(deviceRequestBean.isGift()|| !deviceRequestBean.isGift()) {
			value.setGift(deviceRequestBean.isGift());
		}
		if(deviceRequestBean.getRetailSeller() != null) {
			value.setRetailSeller(deviceRequestBean.getRetailSeller());
		}
		if(deviceRequestBean.getRegistraionDate() != null) {
			value.setRegistraionDate(deviceRequestBean.getRegistraionDate());
		}
		if(deviceRequestBean.getComments() != null) {
			value.setComments(deviceRequestBean.getComments());
		}
		if(deviceRequestBean.getCustomerId() != null) {
			value.setCustomerId(deviceRequestBean.getCustomerId());
		}
		if(deviceRequestBean.getCustomerNumber() != null) {
			value.setCustomerNumber(deviceRequestBean.getCustomerNumber());
		}
		deviceCustomObject.setValue(value);
		String customObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		deviceCustomObject.setVersion(responseObject.get(VERSION).getAsLong());
		String updatedDeviceResponse = (String) netConnectionHelper.sendPostRequest(customObjectUrl, token, deviceCustomObject);
		return new ResponseEntity<>(updatedDeviceResponse, HttpStatus.OK);

	}

	public ResponseEntity<String> deleteDevice(String deviceId) {
		String token =  ctServerHelperService.getStringAccessToken();
		String customObjectDeviceUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS+ "/d2c_device/" + deviceId;
		netConnectionHelper.deleteRequest(customObjectDeviceUrl, token);
		Map<String, String> emptyMap = new HashMap<>();
		return getAllDevices(emptyMap);
	}

	public ResponseEntity<String> getSubscriptionContainerAndKey(String customerId, String orderId) {
		String customObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=value(subscription=true)&sort=lastModifiedAt desc&where=value((state=\"Cancelled\"))&where=value(customer(id=\""
				+ customerId + "\"))&expand=value.cart.typeId";
		String response = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(),
				customObjectUrl);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		List<JsonObject> subscriptionLists = new ArrayList<>();
		if (0 != jsonObject.get(TOTAL).getAsInt()) {
			JsonArray resultsArray = jsonObject.get(RESULTS).getAsJsonArray();
			for (int i = 0; i < resultsArray.size(); i++) {
				JsonObject resultsObject = resultsArray.get(i).getAsJsonObject();
				JsonObject valueObject = resultsObject.get(VALUE).getAsJsonObject();
				if (valueObject.has("cart")) {
					JsonObject cartObject = valueObject.get("cart").getAsJsonObject();
					if (cartObject.get("obj").getAsJsonObject().has(CUSTOM)) {
						JsonObject fieldsObject = cartObject.get("obj").getAsJsonObject().get(CUSTOM).getAsJsonObject()
								.get(FIELDS).getAsJsonObject();
						String parentOrderReferenceId = fieldsObject.get("parentOrderReferenceId").getAsJsonObject().get(ID)
								.getAsString();
						if (orderId.equals(parentOrderReferenceId)) {
							subscriptionLists.add(resultsObject);
						}
					}
				}
			}

			return new ResponseEntity<>(subscriptionLists.toString(), HttpStatus.OK);
		}
		ErrorResponse errorResponse = new ErrorResponse(400, "No results for given data");
		return new ResponseEntity<>(errorResponse.toString(), HttpStatus.BAD_REQUEST);
	}

	public String getSubscriptionPlans() {
		String token =  ctServerHelperService.getStringAccessToken();
		String subscriptionProductTypeId = ctEnvProperties.getSubscriptionProductTypeId();
		String subscriptionProductsUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey()+"/products?where=productType(id=\""+subscriptionProductTypeId+"\")&where=masterData(published=\"true\")";
		return netConnectionHelper.sendGetWithoutBody(token, subscriptionProductsUrl);
	}

	public void createSubscriptionPayment(BillingAddresses billingAddress, Payments payments, JsonObject cartObject, String cartId) {

		String totalAmount = cartObject.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsString();
		String currencyCode = cartObject.get(TOTAL_PRICE).getAsJsonObject().get(CURRENCY_CODE).getAsString();
		String token = ctServerHelperService.getStringAccessToken();
		Map<String, Object> paymentData = new HashMap<>();
		paymentData.put(TOTAL_AMOUNT, totalAmount);
		paymentData.put(CURRENCY_CODE, currencyCode);

		String paymentToken = payments.getPaymentToken();
		JsonObject paymentIntentJsonObject = createPaymentIntent(paymentToken, totalAmount, currencyCode);
		String paymentIntentId = paymentIntentJsonObject.get("id").getAsString();
		paymentData.put(PAYMENT_TYPE, CREDITCARD);
		paymentData.put(PAYMENT_INTENT_ID, paymentIntentId);

		Payment payment = customerProfileUtility.createPaymentRequest(paymentData);
		String paymentResponse = null;
		String paymentCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments";
		if (StringUtils.hasText(paymentIntentId)) {
			payment.setInterfaceId(paymentIntentId);
			setPaymentCustom(paymentIntentJsonObject, payment, payments.getPaymentId());
			paymentResponse = (String) netConnectionHelper.sendPostRequest(paymentCtUrl, token, payment);
		} 
		JsonObject paymentJson = MolekuleUtility.parseJsonObject(paymentResponse);
		String paymentId = paymentJson.get("id").getAsString();
		// Update payment to cart
		UpdateActionsOnCart addPaymentToCart = new UpdateActionsOnCart();
		addPaymentToCart.setVersion(cartObject.get(VERSION).getAsLong());
		List<UpdateActionsOnCart.Action> actions = new ArrayList<>();
		UpdateActionsOnCart.Action addPaymentAction = customerProfileUtility.getAddPaymentRequestObj(paymentId);
		actions.add(addPaymentAction);

		UpdateActionsOnCart.Action action = new UpdateActionsOnCart.Action();
		if(Objects.nonNull(billingAddress)) {
			action.setActionName("setBillingAddress");
			action.setAddress(CustomerProfileUtility.getAddressAction(billingAddress));
		}
		actions.add(action);
		addPaymentToCart.setActions(actions);
		String updateCartCtUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId;
		JsonObject responseObject = MolekuleUtility.parseJsonObject((String)netConnectionHelper.sendPostRequest(updateCartCtUrl, token, addPaymentToCart));
		if(responseObject.get("statusCode") != null) {
			throw new CustomRunTimeException("Error occured in payment process");
		}
	}
	
	private Payment setPaymentCustom(JsonObject paymentIntentJsonObject, Payment payment, String paymentId) {
		Payment.Custom custom = new Payment.Custom();
		Payment.Custom.Type type = new Payment.Custom.Type();
		type.setId(ctEnvProperties.getPaymentTypeId());
		type.setTypeId("type");
		custom.setType(type);
		Payment.Custom.Fields field = new Payment.Custom.Fields();
		String streetCheck = "";
		String zipCheck = "";
		String cvcCheck = "";
		field.setPaymentId(paymentId);
		if (paymentIntentJsonObject.has(CHARGES) && paymentIntentJsonObject.get(CHARGES).getAsJsonObject().has(DATA)) {
			JsonArray dataJsonArray = paymentIntentJsonObject.get(CHARGES).getAsJsonObject().get(DATA).getAsJsonArray();
			JsonObject dataJsonObject = dataJsonArray.get(0).getAsJsonObject();
			JsonObject cardJsonObject = dataJsonObject.get("payment_method_details").getAsJsonObject().get("card")
					.getAsJsonObject();
			JsonObject checksJsonObject = cardJsonObject.get("checks").getAsJsonObject();
			String expMonth = cardJsonObject.get("exp_month").getAsString();
			String expYear = cardJsonObject.get("exp_year").getAsString();
			String last4 = cardJsonObject.get("last4").getAsString();
			String brand = cardJsonObject.get("brand").getAsString();
			String charge = dataJsonObject.get("status").getAsString();
			if (!checksJsonObject.get("address_line1_check").isJsonNull()) {
				streetCheck = checksJsonObject.get("address_line1_check").getAsString();
			}
			if (!checksJsonObject.get("address_postal_code_check").isJsonNull()) {
				zipCheck = checksJsonObject.get("address_postal_code_check").getAsString();
			}
			if (!checksJsonObject.get("cvc_check").isJsonNull()) {
				cvcCheck = checksJsonObject.get("cvc_check").getAsString();
			}
			Boolean captured = dataJsonObject.get("captured").getAsBoolean();
			Boolean refunded = dataJsonObject.get("refunded").getAsBoolean();
			String radarRisk = dataJsonObject.get("outcome").getAsJsonObject().get("risk_level").getAsString();
			String viewInStripeHost = "https://dashboard.stripe.com/";

			String stripePaymentId = dataJsonObject.get("id").getAsString();
			String stripeCustomerId = dataJsonObject.get("customer").getAsString();
			String paymentViewInStripeLink = viewInStripeHost+"payments/"+stripePaymentId;
			String customerViewInStripeLink = viewInStripeHost+"customers/"+stripeCustomerId;

		field.setExpMonth(expMonth);
		field.setExpYear(expYear);
		field.setLast4(last4);
		field.setBrand(brand);
		field.setCharge(charge);
		field.setStreetCheck(streetCheck);
		field.setZipCheck(zipCheck);
		field.setCvcCheck(cvcCheck);
		field.setCaptured(captured);
		field.setRefunded(refunded);
		field.setRadarRisk(radarRisk);
		field.setPaymentView(paymentViewInStripeLink);
		field.setCustomerView(customerViewInStripeLink);
		}
		custom.setFields(field);
		payment.setCustom(custom);	
		return payment;
	}

	public void setBillingAddressAttributes(BillingAddresses billingAddress ,AddressRequestBean addressRequestBean) {
		if(addressRequestBean.getId()!= null) {
			billingAddress.setBillingAddressId(addressRequestBean.getId());		
		}
		if(addressRequestBean.getFirstName()!= null) {
			billingAddress.setFirstName(addressRequestBean.getFirstName());		
		}
		if(addressRequestBean.getLastName()!= null) {
			billingAddress.setLastName(addressRequestBean.getLastName());		
		}
		if(addressRequestBean.getState()!= null) {
			billingAddress.setState(addressRequestBean.getState());		
		}
		if(addressRequestBean.getCountry()!= null) {
			billingAddress.setCountry(addressRequestBean.getCountry());		
		}
		if(addressRequestBean.getPostalCode()!= null) {
			billingAddress.setPostalCode(addressRequestBean.getPostalCode());		
		}
		if(addressRequestBean.getPhone()!= null) {
			billingAddress.setPhoneNumber(addressRequestBean.getPhone());		
		}
		if(addressRequestBean.getCity()!= null) {
			billingAddress.setCity(addressRequestBean.getCity());		
		}
		if(addressRequestBean.getStreetName()!= null) {
			billingAddress.setStreetAddress1(addressRequestBean.getStreetName());		
		}
	}

	public String createSubscription(SubscriptionRequestBean subscriptionRequestBean, String country) {
		SubscriptionCustomObject customObject = new SubscriptionCustomObject();
		SubscriptionCustomObject.Value value = new SubscriptionCustomObject.Value();
		String token = ctServerHelperService.getStringAccessToken();
		CustomerDetailBean customerDetailBean = csrHelperService.getCustomerData(subscriptionRequestBean.getCustomerId(), null, 5 , null);

		//Get Product by SerialNumber
		if(StringUtils.hasText(subscriptionRequestBean.getSerialNumber()) && !StringUtils.hasText(subscriptionRequestBean.getProductId())){
			subscriptionRequestBean.setProductId(getProductId(subscriptionRequestBean.getSerialNumber(), token));		
		}

		//Add customer
		SubscriptionCustomObject.Value.Customer customer = new SubscriptionCustomObject.Value.Customer();
		customer.setId(subscriptionRequestBean.getCustomerId());
		customer.setTypeId("customer");
		value.setCustomer(customer);
		//Add Product
		
		setProductToSubscription(subscriptionRequestBean, value);

		//create cart
		CreateCartRequestBean createCartRequestBean = new CreateCartRequestBean();
		createCartRequestBean.setCustomerId(subscriptionRequestBean.getCustomerId());
		createCartRequestBean.setCountry(country);
		String cartResponse = cartHelperService.createCart(createCartRequestBean);
		JsonObject cartObject = MolekuleUtility.parseJsonObject(cartResponse);
		String cartId = cartObject.get("id").getAsString();
		CartRequestBean cartRequestBean = new CartRequestBean();
		//Add line item to cart
		if(StringUtils.hasText(subscriptionRequestBean.getProductId())) {
			cartRequestBean.setAction(CartServiceUtility.CartActions.ADD_LINE_ITEM.toString());
			cartRequestBean.setProductId(subscriptionRequestBean.getProductId());
			cartRequestBean.setVariantId(1);
			cartRequestBean.setQuantity(1);	
			cartResponse = cartHelperService.updateCartById(cartId, cartRequestBean);
		}
		if(MolekuleUtility.parseJsonObject(cartResponse).has(STATUS_CODE)) {
			return cartResponse;	
		}

		if(subscriptionRequestBean.getPeriod() != 0) {
			cartResponse = addPeriodTocart(cartId, MolekuleUtility.parseJsonObject(cartResponse), subscriptionRequestBean);
			cartObject = MolekuleUtility.parseJsonObject(cartResponse);
		}
		if(cartObject.has(STATUS_CODE)) {
			return cartResponse;	
		}
		//update shipping Address to cart
		if(StringUtils.hasText(subscriptionRequestBean.getShippingAddressId())){
			cartRequestBean.setAction(CartServiceUtility.CartActions.SET_SHIPPING_ADDRESS.toString());
			AddressRequestBean addressRequestBean = customerDetailBean.getAddressList().stream().filter(address -> address.getId().equals(subscriptionRequestBean.getShippingAddressId())).findFirst().orElse(null);
			cartRequestBean.setShippingAddress(addressRequestBean);
			cartResponse = cartHelperService.updateCartById(cartId, cartRequestBean);	
		}
		if(StringUtils.hasText(subscriptionRequestBean.getPromoCode())) {
			cartResponse = checkoutHelperService.applyPromoCode(cartId, subscriptionRequestBean.getPromoCode());
			if(MolekuleUtility.parseJsonObject(cartResponse).has(STATUS_CODE)) {
				return cartResponse;
			}
		}
		if(StringUtils.hasText(subscriptionRequestBean.getShippingMethod())) {
			cartResponse = createShippingMethod(subscriptionRequestBean,cartRequestBean,cartId);
		}
		
		//Add payment
		setSubscriptionPayment(subscriptionRequestBean, customerDetailBean, cartResponse, cartId);
		//Add cart
		SubscriptionCustomObject.Value.Cart cart = new SubscriptionCustomObject.Value.Cart();
		cart.setId(cartId);
		cart.setTypeId("cart");
		value.setCart(cart);

		
		if(StringUtils.hasText(subscriptionRequestBean.getPurchaseDate())) {
			value.setLastOrderDate(subscriptionRequestBean.getPurchaseDate());
		}else {
			value.setLastOrderDate(LocalDate.now().toString());
		}
		setSubscripitonNextOrderDate(subscriptionRequestBean, value);
		value.setChannel(customerDetailBean.getChannel());
		if(StringUtils.hasText(subscriptionRequestBean.getSerialNumber())) {
			value.setState("Cancelled");
		}else {
			value.setState("Scheduled");
		}
		value.setSubscription(true);
		value.setTransactions(new ArrayList<>());

		String[] dateParts = value.getNextOrderDate(). split("-");
		String containerName = "d2c-sub-" + dateParts[1] + "-" + dateParts[2];
		customObject.setContainer(containerName);
		customObject.setKey(MolekuleUtility.createUuidValue());
		customObject.setVersion(0);
		customObject.setValue(value);
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + MolekuleConstant.CUSTOM_OBJECTS;

		return (String) netConnectionHelper.sendPostRequest(accountBaseUrl,
				token, customObject);
	}

	private String createShippingMethod(SubscriptionRequestBean subscriptionRequestBean, CartRequestBean cartRequestBean,
			String cartId) {
		cartRequestBean.setAction(CartServiceUtility.CartActions.SET_SHIPPING_METHOD.toString());		
		CartRequestBean.ShippingMethod shippingMethod = new CartRequestBean.ShippingMethod();
		shippingMethod.setName(subscriptionRequestBean.getShippingMethod());
		shippingMethod.setCost(subscriptionRequestBean.getCost());	
		cartRequestBean.setShippingMethod(shippingMethod);
		return cartHelperService.updateCartById(cartId, cartRequestBean);
		
	}
	
	
	public void setSubscripitonNextOrderDate(SubscriptionRequestBean subscriptionRequestBean,
			SubscriptionCustomObject.Value value) {
		if(StringUtils.hasText(subscriptionRequestBean.getNextOrderDate())) {
			value.setNextOrderDate(subscriptionRequestBean.getNextOrderDate());
			
		}else {
			String[] dateSplitArray = subscriptionRequestBean.getPurchaseDate().split("-");
			int year = Integer.parseInt(dateSplitArray[0]);
			int month = Integer.parseInt(dateSplitArray[1]);
			int day = Integer.parseInt(dateSplitArray[2]);
			Calendar nextOrderDateCalendar = new GregorianCalendar(year, month - 1, day);
			nextOrderDateCalendar.add(Calendar.MONTH, 6);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy'-'MM'-'dd");
			String renewalDate = simpleDateFormat.format(nextOrderDateCalendar.getTime());
			value.setNextOrderDate(renewalDate);
		}
	}

	public void setSubscriptionPayment(SubscriptionRequestBean subscriptionRequestBean,
			CustomerDetailBean customerDetailBean, String cartResponse, String cartId) {
		AddressRequestBean addressRequestBean;
		if(StringUtils.hasText(subscriptionRequestBean.getPaymentId())) {
			Payments payments= customerDetailBean.getPayments().stream().filter(payment -> payment.getPaymentId().equals(subscriptionRequestBean.getPaymentId())).findFirst().orElse(null);		
			addressRequestBean = customerDetailBean.getAddressList().stream()
					.filter(s->s.getId().equals(payments.getBillingAddressId())).findAny().orElse(null);
			BillingAddresses billingAddress = new BillingAddresses();
			if(addressRequestBean != null) {
				setBillingAddressAttributes(billingAddress, addressRequestBean);
			}
			createSubscriptionPayment(billingAddress,payments, MolekuleUtility.parseJsonObject(cartResponse), cartId);

		}
	}

	public void setProductToSubscription(SubscriptionRequestBean subscriptionRequestBean,
			SubscriptionCustomObject.Value value) {
		if(StringUtils.hasText(subscriptionRequestBean.getProductId())) {
			SubscriptionCustomObject.Value.Product product = new SubscriptionCustomObject.Value.Product();
			String productResponse = registrationService.getProductById(subscriptionRequestBean.getProductId());
			ProductResponseBean response = (ProductResponseBean) RegistrationUtility.convertToJsonObject(productResponse, ProductResponseBean.class);
			SubscriptionCustomObject.Value.Product.Name name = new SubscriptionCustomObject.Value.Product.Name();
			name.setEn(response.getProductName());
			SubscriptionCustomObject.Value.Product.Description description = new SubscriptionCustomObject.Value.Product.Description();
			description.setEn(response.getProductDescription());
			product.setName(name);
			product.setDescription(description);
			value.setProduct(product);
		}
		
	}

	private String getProductId(String serialNumber, String token) {
		String orderUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + 
				"/orders?where=lineItems(custom(fields(serialNumbers=\"" + serialNumber +"\")))";
		String response = netConnectionHelper.sendGetWithoutBody(token, orderUrl);
		String productId = EMPTY;
		JsonArray resultArray = MolekuleUtility.parseJsonObject(response).get(RESULTS).getAsJsonArray();
		JsonObject resultObject = new JsonObject();
		if(resultArray.size()>0) {
			resultObject = resultArray.get(0).getAsJsonObject();
		}
		JsonArray lineItemArray = new JsonArray();
		if(resultObject.has(LINE_ITEMS) && resultObject.get(LINE_ITEMS).getAsJsonArray().size()>0){
			lineItemArray = resultObject.get(LINE_ITEMS).getAsJsonArray();
		}
		for(int i=0; i<lineItemArray.size(); i++) {
			String serialNumberInLineItem = EMPTY;
			JsonObject lineItemObject = lineItemArray.get(i).getAsJsonObject();
			if(lineItemObject.has(CUSTOM) && lineItemObject.get(CUSTOM).getAsJsonObject().has(FIELDS) 
					&& lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(SERIAL_NUMBERS)) {
				JsonArray serialNumbersArray = lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(SERIAL_NUMBERS).getAsJsonArray();
				serialNumberInLineItem = serialNumbersArray.size()>0 ? serialNumbersArray.get(0).getAsString() : EMPTY;
			}
			productId = getProductIdFromSubscriptionLineItem(serialNumber, productId, serialNumberInLineItem,
					lineItemObject);
		}
		return productId;
	}

	public String getProductIdFromSubscriptionLineItem(String serialNumber, String productId,
			String serialNumberInLineItem, JsonObject lineItemObject) {
		if(serialNumberInLineItem.equals(serialNumber) && lineItemObject.has(VARIANT) 
				&& lineItemObject.get(VARIANT).getAsJsonObject().has(ATTRIBUTES)) {
			JsonArray attributeArray = lineItemObject.get(VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
			for(int j=0; j<attributeArray.size(); j++) {
				JsonObject attributeObject = attributeArray.get(j).getAsJsonObject();
				if(attributeObject.has("name") && attributeObject.get("name").getAsString().equals("SubscriptionIdentifier")) {
					productId = attributeObject.get(VALUE).getAsJsonObject().get("id").getAsString();
				}
			}
		}
		return productId;
	}

	private String addPeriodTocart(String cartId, JsonObject cartObject, SubscriptionRequestBean subscriptionRequestBean) {
		long version = cartObject.get(VERSION).getAsLong();
		List<UpdateCartFilterServiceBean.Action> actions = new ArrayList<>();

		UpdateCartFilterServiceBean upaCartServiceBean = new UpdateCartFilterServiceBean();
		UpdateCartFilterServiceBean.Action extend = new UpdateCartFilterServiceBean.Action();
		extend.setActionName("setDeleteDaysAfterLastModification");
		extend.setDeleteDaysAfterLastModification(30000);
		actions.add(extend);
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		upaCartServiceBean.setVersion(version);
		UpdateCartFilterServiceBean.Action action = new UpdateCartFilterServiceBean.Action();
		if(!cartObject.has(CUSTOM)) {
			action.setActionName(SET_CUSTOM_TYPE);
			UpdateCartFilterServiceBean.Action.Type updateCartServiceType =  new UpdateCartFilterServiceBean.Action.Type();
			updateCartServiceType.setId(ctEnvProperties.getOrderTypeId());
			updateCartServiceType.setTypeId("type");
			action.setType(updateCartServiceType);
			UpdateCartFilterServiceBean.Action.Fields fields =  new UpdateCartFilterServiceBean.Action.Fields();
			fields.setPeriod(subscriptionRequestBean.getPeriod());			
			action.setFields(fields);

		}else {
			action.setActionName(SET_CUSTOM_FIELD);
			action.setName("period");
			action.setValue(subscriptionRequestBean.getPeriod());
		}		
		actions.add(action);
		upaCartServiceBean.setActions(actions);
		return (String)netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);

	}


}
