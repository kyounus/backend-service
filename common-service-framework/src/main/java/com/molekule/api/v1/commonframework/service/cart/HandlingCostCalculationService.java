package com.molekule.api.v1.commonframework.service.cart;

import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
/**
 * HandlingCostCalculationService is an interface used for handling cost method instantiation.
 *
 */
@Service
public interface HandlingCostCalculationService {
	String calculateHandlingCharge(JsonObject shippingTableJsonObject, int deviceCount, int filterCount);
}
