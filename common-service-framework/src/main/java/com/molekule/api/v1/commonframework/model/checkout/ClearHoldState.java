package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClearHoldState {

	private long version;
	List<Action> actions;

	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		private State state;
		private String name;
		private Set<String> value;

		@Data
		public static class State {
			private String typeId;
			private String id;

		}
	}
}
