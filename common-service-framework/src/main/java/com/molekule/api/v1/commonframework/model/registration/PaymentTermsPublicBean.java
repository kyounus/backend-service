package com.molekule.api.v1.commonframework.model.registration;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The class PaymentTermsPublicBean is used to hold the request of payment option public type.
 */
@ApiModel(description = "Payment Terms on Public Type Api Model")
public class PaymentTermsPublicBean {
	
	@ApiModelProperty(required = true)
	private String stockTickerName;
	@ApiModelProperty(required = true)
	private String stockExchange;

}
