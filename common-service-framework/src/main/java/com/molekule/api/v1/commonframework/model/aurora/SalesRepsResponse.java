package com.molekule.api.v1.commonframework.model.aurora;

import java.util.List;

import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;

import lombok.Data;

@Data
public class SalesRepsResponse {

	private SalesRepresentativeDto salesRep;
	private List<CompanyCategory> companyCategories;
	
	@Data
	public static class CompanyCategory {
		
		private String id;

		private String name;
	}

	
}
