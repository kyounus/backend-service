package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

/**
 * The class ProductResponse is used to send response for product bean.
 *
 */
@Data
public class ProductResponse {

	private ProductBean productBean;
	private String averageRating;
	private String reviewCount;
	private String subscriptionPeriod;
	private boolean hasSubscription;
	private UpSellSku upSellSku;
	
	@Data
	public static class  UpSellSku {
		private String id;
		private String sku;
		private String name;
	}

}
