package com.molekule.api.v1.commonframework.model.registration;

import com.molekule.api.v1.commonframework.dto.registration.Payments.TermsStatus;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdatePaymentRequestBean {

	public enum Action{
		CHANGE,REMOVE
	}
	
	@ApiModelProperty(example = "CHANGE or REMOVE")
	private Action action;
	
	@ApiModelProperty(required = false)
	private BillingAddresses billingAddress;
	
	@ApiModelProperty(required = false)
	private boolean isDefaultPayment;
	
	@ApiModelProperty(example = "NOTAPPLIED/PENDINGREVIEW/APPROVED")
	private TermsStatus termsStatus;
	
	@ApiModelProperty(required = false)
	private String currency;
	@ApiModelProperty(required = false)
	private Boolean transactionNeedApproval;
	@ApiModelProperty(required = false)
	private String totalApprovedCredit;
	@ApiModelProperty(required = false)
	private String totalUsedCredit;
	@ApiModelProperty(required = false)
	private String netTerms;
	@ApiModelProperty(required = false)
	private String termsApprovalDate;
	
	
}
