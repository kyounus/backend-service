package com.molekule.api.v1.commonframework.service.aurora;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM_OBJECTS;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;
import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("auroraHelperService")
public class AuroraHelperService {
	
	@Autowired
	CTEnvProperties ctEnvProperties;
	
	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;
	
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;
	
	/**
	 * getShippingTables method is used to shipping tables from CT.
	 * 
	 * @param container 
	 * @param key 
	 * @return ShippingTable
	 */
	public ShippingTable getShippingTables(String container, String key) {
		String shippingTableUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/custom-objects/"+container+"/"+key;
		return netConnectionHelper.sendGetForShippingTables(ctServerHelperService.getStringAccessToken(), shippingTableUrl);
	}
	
	/**
	 * getCompanyCategories method is used to fetch list of Company Categories.
	 * 
	 * @return String - it contains Company Categories
	 */
	public String getCompanyCategories() {
		String accountBaseUrl = ctEnvProperties.getHost()+ "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + "/b2b" + "/" + "mo_company_cat"; 
		String token = ctServerHelperService.getStringAccessToken();
		return netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
	}
	
	/**
	 * getSalesRepValueFromCategoryList method is used to get the sales rep id based on the category.
	 * 
	 * @param categoryDtoList
	 * @param accountCustomResponseBean
	 * @return String - it contains the sales representative id.
	 */
	public String getSalesRepValueFromCategoryList(List<CategoryDto> categoryDtoList, String categoryId) {
		String salesRepId = null;
		List<CategoryDto> filteredList = categoryDtoList.stream().filter(dto-> dto.getId().equals(categoryId)).collect(Collectors.toList());
		CategoryDto categoryDto = filteredList.get(0);
		salesRepId =  categoryDto.getSalesRepId();
		return salesRepId;
	}
	
	/**
	 * getListOfSalesRep method is used to get the list of sales rep.
	 * 
	 * @return String- it contains list of sales rep.
	 */
	public String getListOfSalesRep() {
		String accountBaseUrl = ctEnvProperties.getHost()+ "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + "/b2b" + "/" + "mo_sales_rep"; 
		String token = ctServerHelperService.getStringAccessToken();
		return netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
	}
	
	/**
	 * getSalesRepDetails method is used to get the sales representative detail based on sales rep id mentioned in the category.
	 * 
	 * @param salesRespresentativeDtoList
	 * @param salesRepId
	 * @return SalesRespresentativeDto - contains the sales representative details.
	 */
	public SalesRepresentativeDto getSalesRepDetails(List<SalesRepresentativeDto> salesRespresentativeDtoList, String salesRepId) {
		if(salesRepId != null) {
			List<SalesRepresentativeDto> filteredList= salesRespresentativeDtoList.stream().filter(dto-> dto.getId().equals(salesRepId)).collect(Collectors.toList());
			return filteredList.get(0);
		}else {
			List<SalesRepresentativeDto> filteredList= salesRespresentativeDtoList.stream().filter(dto-> dto.getName().equals("Other")).collect(Collectors.toList());
			return filteredList.get(0);
		}

	}
}
