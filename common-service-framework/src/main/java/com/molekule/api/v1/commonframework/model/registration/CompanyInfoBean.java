package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * The class CompanyInfoBean is used to hold the request of company information.
 */
@ApiModel(description = "Company info model")
@Data
public class CompanyInfoBean {
	@ApiModelProperty(required = true)
	private String numberOfEmployees;
	@ApiModelProperty(required = true)
	private String numberOfOfficeSpaces;
	private String companyName;
	private String companyCategoryId;
	@ApiModelProperty(value = "If taxExempt true.This required", required = false)
	private List<String> taxCertificateList;
	@ApiModelProperty(required = true)
	private String accountId;
	@ApiModelProperty(required = false)
	private String customerId;
	@ApiModelProperty(required = true)
	private Integer customerNumber;
	private int version;

	private Address companyAddress;
	@ApiModelProperty(example = "false", required = false)
	private boolean taxExempt = false;
	
}