package com.molekule.api.v1.commonframework.mail.sendgrid;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;

@Component("orderInvoiceMailSender")
public class OrderInvoiceMailSender extends TemplateMailRequestHelper {

	private static final int BYTE_BUFFER_SIZE = 4096;
	
	Logger logger = LoggerFactory.getLogger(OrderInvoiceMailSender.class);

	@Value("${orderShippedTemplateId}")
	private String templateId;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public SendGridRequestBean prepareRequestBean(Object obj) {
		SendGridModel sendGridModel = (SendGridModel) obj;
		SendGridRequestBean mailResponseBean = new SendGridRequestBean();
		SendGridRequestBean.Personalizations personalizations = getPersonalizations(sendGridModel);
		SendGridRequestBean.Attachments attachment =  getAttachment(sendGridModel);
		SendGridRequestBean.From fromMail = new SendGridRequestBean.From();
		mailResponseBean.setTemplateId(templateId());
		fromMail.setEmail(getFromEmail());
		fromMail.setName(getFromName());
		mailResponseBean.setFrom(fromMail);
		List<SendGridRequestBean.Attachments> attachmentList = new ArrayList<>();
		attachmentList.add(attachment);
		List<SendGridRequestBean.Personalizations> personalizationsList = new ArrayList<>();
		personalizationsList.add(personalizations);
		mailResponseBean.setPersonalizations(personalizationsList);
		mailResponseBean.setAttachments(attachmentList);
		return mailResponseBean;

	}
	
	@Override
	public SendGridRequestBean.Personalizations getPersonalizations(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations personalizations = new SendGridRequestBean.Personalizations();
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = getDynamicTemplateData(
				sendGridModel);
		List<SendGridRequestBean.Personalizations.To> toList = new ArrayList<>();
		for(int i =0 ;i<sendGridModel.getInvoiceEmailAddresses().size();i++) {
			SendGridRequestBean.Personalizations.To to = new SendGridRequestBean.Personalizations.To();
			to.setEmail(sendGridModel.getInvoiceEmailAddresses().get(i));
			toList.add(to);
		}
		personalizations.setTo(toList);
   		personalizations.setDynamicTemplateData(dynamicTemplateData);
		return personalizations;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(
			SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		if (sendGridModel.getOrderFedExTrackingInfo() != null) {
			dynamicTemplateData.setOrderFedExTrackingInfo(sendGridModel.getOrderFedExTrackingInfo());
		}
		if (sendGridModel.getCustomerCarrierName() != null) {
			dynamicTemplateData.setCustomerCarrierName(sendGridModel.getCustomerCarrierName());
		}
		if (sendGridModel.getCustomerCarrierAccountNumber() != null) {
			dynamicTemplateData.setCustomerCarrierAccountNumber(sendGridModel.getCustomerCarrierAccountNumber());
		}
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber());
		return dynamicTemplateData;
	}

	public SendGridRequestBean.Attachments getAttachment(SendGridModel sendGridModel){

		SendGridRequestBean.Attachments attachment = new SendGridRequestBean.Attachments();
		String encodedString = null;
		try {
			encodedString = encodeToBase64(new URL(sendGridModel.getInvoiceUrl()).openStream());
		} catch (IOException e1) {
			logger.error("Error Occured when getAttachment", e1);
		}
		attachment.setContent(encodedString);
		attachment.setType("application/pdf");
		attachment.setFileName("invoice.pdf");
		return attachment;
	}


	private String encodeToBase64(InputStream content) {
		int read = 0;
		byte[] bytes = new byte[BYTE_BUFFER_SIZE];
		try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			while ((read = content.read(bytes)) != -1) {
				baos.write(bytes, 0, read);
			}
			return Base64.getEncoder().encodeToString(baos.toByteArray());
		} catch (IOException e) {
			throw new CustomRunTimeException("Unable to convert content stream to base 64 encoded string");
		}
	}
}
