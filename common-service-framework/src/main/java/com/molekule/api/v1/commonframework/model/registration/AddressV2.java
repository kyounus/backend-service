package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import lombok.Data;

/**
 * The class AddressV2 is used to hold the data for address. 
 * 
 * @version 2.0
 */
@Data
public class AddressV2 {
	
	List<ShippingAddresses> shippingAddresses;
	List<BillingAddresses> billingAddresses;
	
}
