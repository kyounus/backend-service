package com.molekule.api.v1.commonframework.model.aurora;

import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;

import lombok.Data;

@Data
public class GetCustomerBean {
	private int limit;
	private int offset;
	private String companyName;
	private String email;
	private String firstName;
	private String lastName;
	private String customerChannel;
	private CustomerSortActionEnum sortAction;
	private SortOrderEnum sortOrder;
}
