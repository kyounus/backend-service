package com.molekule.api.v1.commonframework.dto.registration;

import java.util.List;

import lombok.Data;

@Data
public class CustomerAction {

	private long version;
	private List<Actions> actions;
	@Data
	public static class Actions{
		private String action;
		private CustomerGroup customerGroup;
		private String key;
		private String name;
		private Object value;

		@Data
		public static class CustomerGroup{
			private String id;
			private String customerGroupName;
			private String typeId;
		}
	}
}
