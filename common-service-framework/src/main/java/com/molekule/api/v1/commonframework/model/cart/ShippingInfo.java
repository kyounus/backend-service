package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_PRICE;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class ShippingInfo {

	@JsonProperty("shippingMethodName")
	private String shippingMethodName;

	@JsonProperty("price")
	private TypedMoney price;

	@JsonProperty("shippingRate")
	private ShippingRate shippingRate;

	@JsonProperty("taxedPrice")
	private TaxedItemPrice taxedPrice;

	@JsonProperty("taxRate")
	private TaxRate taxRate;

	@JsonProperty("deliveries")
	private List<Delivery> deliveries;

	@JsonProperty(DISCOUNTED_PRICE)
	private DiscountedLineItemPrice discountedPrice;

	@JsonProperty("shippingMethodState")
	private String shippingMethodState;

	public String getShippingMethodName() {
		return shippingMethodName;
	}

	public void setShippingMethodName(String shippingMethodName) {
		this.shippingMethodName = shippingMethodName;
	}

	public TypedMoney getPrice() {
		return price;
	}

	public void setPrice(TypedMoney price) {
		this.price = price;
	}

	public ShippingRate getShippingRate() {
		return shippingRate;
	}

	public void setShippingRate(ShippingRate shippingRate) {
		this.shippingRate = shippingRate;
	}

	public TaxedItemPrice getTaxedPrice() {
		return taxedPrice;
	}

	public void setTaxedPrice(TaxedItemPrice taxedPrice) {
		this.taxedPrice = taxedPrice;
	}

	public TaxRate getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(TaxRate taxRate) {
		this.taxRate = taxRate;
	}

	public List<Delivery> getDeliveries() {
		return deliveries;
	}

	public void setDeliveries(List<Delivery> deliveries) {
		this.deliveries = deliveries;
	}

	public DiscountedLineItemPrice getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(DiscountedLineItemPrice discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public String getShippingMethodState() {
		return shippingMethodState;
	}

	public void setShippingMethodState(String shippingMethodState) {
		this.shippingMethodState = shippingMethodState;
	}

}
