package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ITEMS;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.registration.Address;

public class Delivery {

	@JsonProperty("id")
	private String id;

	@JsonProperty("createdAt")
	private ZonedDateTime createdAt;

	@JsonProperty(ITEMS)
	private List<DeliveryItem> items;

	@JsonProperty("parcels")
	private List<Parcel> parcels;

	@JsonProperty("address")
	private Address address;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public List<DeliveryItem> getItems() {
		return items;
	}

	public void setItems(List<DeliveryItem> items) {
		this.items = items;
	}

	public List<Parcel> getParcels() {
		return parcels;
	}

	public void setParcels(List<Parcel> parcels) {
		this.parcels = parcels;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
