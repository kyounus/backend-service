package com.molekule.api.v1.commonframework.dto.order;

import java.util.List;

import lombok.Data;

@Data
public class RefundCustomObject {
	private String container;
	private String key;
	private Value value;
	private Long version;
	
	@Data
	public static class Value {
		private List<LineItems> lineItemsList;
		private String refundShipping;
		private String adjustmentRefund;
		private String adjustmentFee;
		private String discountTotal;
		private String tax;
		private String refundTotal;
		private String comments;
	}
}
