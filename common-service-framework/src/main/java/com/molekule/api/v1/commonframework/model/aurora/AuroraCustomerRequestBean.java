package com.molekule.api.v1.commonframework.model.aurora;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * The class AuroraCustomerRequestBean is used to hold the customer
 * Request information.
 */
@Data
public class AuroraCustomerRequestBean {

	@ApiModelProperty(required = true)
	private String firstName;
	@ApiModelProperty(required = true)
	private String lastName;
	@ApiModelProperty(required = true)
	private String email;
	@ApiModelProperty(example = "B2B")
	private String channel;
	@ApiModelProperty(example = "US")
	private String countryCode;
	private String store;
	private String registeredChannel;
	private String comments;

}
