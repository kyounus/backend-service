package com.molekule.api.v1.commonframework.dto.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerGroupRequestBean {

	private long version;
	private List<Action> actions;

	 public static class Action{

		@JsonProperty(ACTION)
		private String actionName;

		private CustomerGroup customerGroup;

		 public static class CustomerGroup{
			private String id;
			@JsonProperty("CustomerGroupName")
			private String customerGroupName;
			private String typeId;

			public String getId() {
				return id;
			}
			public void setId(String id) {
				this.id = id;
			}
			public String getCustomerGroupName() {
				return customerGroupName;
			}
			public void setCustomerGroupName(String customerGroupName) {
				this.customerGroupName = customerGroupName;
			}
			public String getTypeId() {
				return typeId;
			}
			public void setTypeId(String typeId) {
				this.typeId = typeId;
			}
			
		}

		public String getActionName() {
			return actionName;
		}

		public void setActionName(String actionName) {
			this.actionName = actionName;
		}

		public CustomerGroup getCustomerGroup() {
			return customerGroup;
		}

		public void setCustomerGroup(CustomerGroup customerGroup) {
			this.customerGroup = customerGroup;
		}

	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
}
