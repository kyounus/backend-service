package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("paymentTermsSuccessConfirmationSender")
public class PaymentTermsSuccessConfirmationSender extends TemplateMailRequestHelper {
	
	@Value("${paymentTermSuccessConfirmationTemplateId}")
	private String templateId;

	@Value("${paymentTermSuccessConfirmationStatusURL}")
	private String statusURL;

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return getTemplateId();
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setB2bHomepageUrl(statusURL);
		dynamicTemplateData.setSalesRepresentativeFirstName(sendGridModel.getSalesRepFirstName());
		dynamicTemplateData.setSalesRepresentativeLastName(sendGridModel.getSalesRepLastName());
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(
				sendGridModel.getSalesRepPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		dynamicTemplateData.setSalesRepresentativeCalendlyLink(sendGridModel.getSalesRepCalendyLink());
		return dynamicTemplateData;

	}
	
}
