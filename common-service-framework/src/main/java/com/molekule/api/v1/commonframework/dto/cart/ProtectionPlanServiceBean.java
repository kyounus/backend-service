package com.molekule.api.v1.commonframework.dto.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
/**
 * ProtectionPlanServiceBean class is used to update the custom field object in cart response.
 * 
 *
 */
@Data
public class ProtectionPlanServiceBean {
	private Long version;
	private List<Action> actions = null;
	@Data
	public static class Action {
		
		@JsonProperty(ACTION)
		private String actionName;
		
		private Type type;
		private Fields fields;
		private String name;
		private String value;
		private String lineItemId;
		
		@Data
		public static class Type {
			private String id;
			private String typeId;
		}
		@Data
		public static class Fields {
			private String protectionPlanLineItem;
		}
	}
}
