package com.molekule.api.v1.commonframework.dto.registration;

import java.util.List;

import lombok.Data;

/**
 * CompanyCategories class is used to hold the list of company category.
 * 
 */
@Data
public class CompanyCategories {
	private List<CategoryDto> companyCategory;

}
