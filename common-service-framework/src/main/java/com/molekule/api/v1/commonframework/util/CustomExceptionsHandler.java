package com.molekule.api.v1.commonframework.util;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.HandlerMethod;

import lombok.Data;

@SuppressWarnings({ "unchecked", "rawtypes" })
@ControllerAdvice
public class CustomExceptionsHandler {
	
	private final SimpleDateFormat sdf = getDateFormated();

	private static final SimpleDateFormat getDateFormated() {
		return new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
	}

	@ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, HandlerMethod handlerMethod, HttpServletRequest request) {
		Class controllerName = handlerMethod.getMethod().getDeclaringClass();
        String methodName = handlerMethod.getMethod().getName();
        ErrorResponse error = new ErrorResponse();
        
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setErrorMessage(ex.getMessage());
        error.setControllerName(controllerName.toString());
        error.setMethodName(methodName);
        error.setTimeStamp(sdf.format(System.currentTimeMillis()));
        error.setServicePath(request.getRequestURL().toString());
        
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	@Data
	public class ErrorResponse 
	{
	    private Integer errorCode;
	    private String errorMessage;
	    private String controllerName;
	    private String methodName;
	    private String timeStamp;
	    private String servicePath;
	}
}
