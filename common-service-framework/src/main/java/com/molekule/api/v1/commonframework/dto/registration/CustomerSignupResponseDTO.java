package com.molekule.api.v1.commonframework.dto.registration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;

import lombok.Data;

/**
 * The class CustomerSignupResponseDTO is used to send response to the quick registration.
 */
@JsonRootName(value = CUSTOMER)
@Data
public class CustomerSignupResponseDTO {

	private String customerId;
	private String accountId;
	private String email;
	private String firstName;
	private String lastName;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	private int accountVersion;
	private String customerNumber;
	private CustomerAction.Actions.CustomerGroup customerGroup;
	
	public int getAccountVersion() {
		return accountVersion;
	}

	public void setAccountVersion(int accountVersion) {
		this.accountVersion = accountVersion;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	@Override
	public String toString() {
		return "CustomerSignupResponseDTO [customerId=" + customerId + ", accountId=" + accountId + ", email=" + email
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", password=" + password + ", accountVersion="
				+ accountVersion + ", customerNumber=" + customerNumber + ", customerGroup=" + customerGroup + "]";
	}

}
