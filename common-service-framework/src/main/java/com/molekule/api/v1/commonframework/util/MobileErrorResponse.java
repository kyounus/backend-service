package com.molekule.api.v1.commonframework.util;

import java.util.List;


import lombok.Data;

@Data
public class MobileErrorResponse {

	private String message;
	private List<String> parameters;
	
	@Override
	public String toString() {
		return "{\r\n"
				+ "    \"message\":"+message+",\r\n"
				+ "    \"parameters\":\""+parameters+"\"\r\n"
				+ "\r\n"
				+ "}";
	}

}
