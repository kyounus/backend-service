package com.molekule.api.v1.commonframework.model.customerprofile;

import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;

import lombok.Data;

@Data
public class D2CPaymentRequestBean {

	private Boolean isDefaultAddress;
	private AddressRequestBean billingAddress;
	private Boolean isDefaultPayment;
	private String paymentType;
	private String paymentId;
	private String paymentToken;
}
