package com.molekule.api.v1.commonframework.util;

public enum CustomerSourceEnum {
	B2B_WEB,
	D2C_WEB,
	MC
}
