package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class CustomerCards {

	private String id;
	private int stripe_customer_id;
	private String payment_id;
	private String billing_address;
	private String status;
	private String payment_code;
	private String expiration_date;
	private String created_at;
	private String updated_at;
	
}
