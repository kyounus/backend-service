package com.molekule.api.v1.commonframework.model.products;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ATTRIBUTES;

public class ProductVariant {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("sku")
	private String sku;

	@JsonProperty("key")
	private String key;

	@JsonProperty("prices")
	private List<Price> prices;

	@JsonProperty(ATTRIBUTES)
	private List<Attribute> attributes;

	@JsonProperty("price")
	private Price price;

	@JsonProperty("availability")
	private ProductVariantAvailability availability;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<Price> getPrices() {
		return prices;
	}

	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public ProductVariantAvailability getAvailability() {
		return availability;
	}

	public void setAvailability(ProductVariantAvailability availability) {
		this.availability = availability;
	}

}
