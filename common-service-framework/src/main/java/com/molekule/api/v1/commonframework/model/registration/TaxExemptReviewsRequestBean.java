package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class TaxExemptReviewsRequestBean is used to hold the request for TaxExemptReviews.
 */
public class TaxExemptReviewsRequestBean {

	private String country;
	private String state;
	private String expirationDate;
	private String taxCertificationId;
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getTaxCertificationId() {
		return taxCertificationId;
	}
	public void setTaxCertificationId(String taxCertificationId) {
		this.taxCertificationId = taxCertificationId;
	}
	
}
