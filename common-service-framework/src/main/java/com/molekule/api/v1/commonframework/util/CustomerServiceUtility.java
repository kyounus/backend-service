package com.molekule.api.v1.commonframework.util;

public class CustomerServiceUtility {
	public enum CustomerActions {
		ADD_SHIPPING_ADDRESS, 
		UPDATE_SHIPPING_ADDRESS, 
		REMOVE_SHIPPING_ADDRESS,
		ADD_PAYMENT, 
		UPDATE_PAYMENT,
		REMOVE_PAYMENT, 
		UPDATE_EMAIL, 
		UPDATE_PROFILE, 
		UPDATE_COMMENTS,
		UPDATE_MARKETING_OFFERS
	}
}
