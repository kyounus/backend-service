package com.molekule.api.v1.commonframework.model.aurora;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SalesRepresentativesBean {

	private int total;
	
	@JsonProperty("results")
	private List<SalesRepsResponse> salesReps;
	
	}
