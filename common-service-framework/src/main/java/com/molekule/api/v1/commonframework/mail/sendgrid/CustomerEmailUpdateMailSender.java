package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("customerEmailUpdateMailSender")
public class CustomerEmailUpdateMailSender extends TemplateMailRequestHelper {
	
	@Value("${customerEmailUpdateTemplateId}")
	private String templateId;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}
	
	@Value("${customerProfileURL}")
	private String myAccountUrl;

	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setCustomerEmail(sendGridModel.getUpdatedEmail());
		dynamicTemplateData.setPromo(true);
		dynamicTemplateData.setMyAccountUrl(myAccountUrl);
		return dynamicTemplateData;
	}

}
