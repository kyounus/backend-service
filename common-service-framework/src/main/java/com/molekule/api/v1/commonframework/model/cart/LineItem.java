package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VARIANT;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.Price;
import com.molekule.api.v1.commonframework.model.products.ProductTypeReference;
import com.molekule.api.v1.commonframework.model.products.ProductVariant;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class LineItem {

	@JsonProperty("id")
	private String id;

	@JsonProperty(PRODUCT_ID)
	private String productId;

	@JsonProperty("name")
	private Map<String, String> name;

	@JsonProperty("productSlug")
	private Map<String, String> productSlug;

	@JsonProperty(PRODUCT_TYPE)
	private ProductTypeReference productType;

	@JsonProperty(VARIANT)
	private ProductVariant variant;

	@JsonProperty("price")
	private Price price;

	@JsonProperty("taxedPrice")
	private TaxedItemPrice taxedPrice;

	@JsonProperty(TOTAL_PRICE)
	private TypedMoney totalPrice;

	@JsonProperty(QUANTITY)
	private Long quantity;

	@JsonProperty("addedAt")
	private ZonedDateTime addedAt;

	@JsonProperty(STATE)
	private List<ItemState> state;

	@JsonProperty("taxRate")
	private TaxRate taxRate;

	@JsonProperty("discountedPricePerQuantity")
	private List<DiscountedLineItemPriceForQuantity> discountedPricePerQuantity;

	@JsonProperty("priceMode")
	private String priceMode;

	@JsonProperty("lineItemMode")
	private String lineItemMode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Map<String, String> getName() {
		return name;
	}

	public void setName(Map<String, String> name) {
		this.name = name;
	}

	public Map<String, String> getProductSlug() {
		return productSlug;
	}

	public void setProductSlug(Map<String, String> productSlug) {
		this.productSlug = productSlug;
	}

	public ProductTypeReference getProductType() {
		return productType;
	}

	public void setProductType(ProductTypeReference productType) {
		this.productType = productType;
	}

	public ProductVariant getVariant() {
		return variant;
	}

	public void setVariant(ProductVariant variant) {
		this.variant = variant;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public TaxedItemPrice getTaxedPrice() {
		return taxedPrice;
	}

	public void setTaxedPrice(TaxedItemPrice taxedPrice) {
		this.taxedPrice = taxedPrice;
	}

	public TypedMoney getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(TypedMoney totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public ZonedDateTime getAddedAt() {
		return addedAt;
	}

	public void setAddedAt(ZonedDateTime addedAt) {
		this.addedAt = addedAt;
	}

	public List<ItemState> getState() {
		return state;
	}

	public void setState(List<ItemState> state) {
		this.state = state;
	}

	public TaxRate getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(TaxRate taxRate) {
		this.taxRate = taxRate;
	}

	public List<DiscountedLineItemPriceForQuantity> getDiscountedPricePerQuantity() {
		return discountedPricePerQuantity;
	}

	public void setDiscountedPricePerQuantity(List<DiscountedLineItemPriceForQuantity> discountedPricePerQuantity) {
		this.discountedPricePerQuantity = discountedPricePerQuantity;
	}

	public String getPriceMode() {
		return priceMode;
	}

	public void setPriceMode(String priceMode) {
		this.priceMode = priceMode;
	}

	public String getLineItemMode() {
		return lineItemMode;
	}

	public void setLineItemMode(String lineItemMode) {
		this.lineItemMode = lineItemMode;
	}

}
