package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

/**
 * TaxExemptionConfirmationSender is a helper class.it will Build SendGrid

 * Dynamic data
 * 
 * @version 1.0
 */

@Component("taxExemptionConfirmationSender")
public class TaxExemptionConfirmationSender extends TemplateMailRequestHelper {

	@Value("${taxExemptionConfirmationTemplateId}")
	private String templateId;

	@Value("${taxExemptionStatusUrl}")
	private String statusUrl;
	
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setTaxExemptionStatusUrl(statusUrl);
		dynamicTemplateData.setSalesRepresentativeFirstName(sendGridModel.getSalesRepFirstName());
		dynamicTemplateData.setSalesRepresentativeLastName(sendGridModel.getSalesRepLastName());
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber());
		return dynamicTemplateData;
	}

}
