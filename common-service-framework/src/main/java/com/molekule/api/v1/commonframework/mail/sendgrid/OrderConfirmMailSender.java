package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;
@Component("orderConfirmMailSender")
public class OrderConfirmMailSender extends TemplateMailRequestHelper{

	@Value("${orderConfirmedTemplateId}")
	private String templateId;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setCustomerFirstName(sendGridModel.getFirstName());
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setProducts(sendGridModel.getProducts());
		dynamicTemplateData.setOrderSummarySubtotal(sendGridModel.getOrderSummarySubtotal());
		dynamicTemplateData.setOrderSummaryDiscount(sendGridModel.getOrderSummaryDiscount());
		dynamicTemplateData.setOrderSummaryTax(sendGridModel.getOrderSummaryTax());
		dynamicTemplateData.setOrderSummaryShipping(sendGridModel.getOrderSummaryShipping());
		dynamicTemplateData.setOrderSummaryHandling(sendGridModel.getOrderSummaryHandling());
		dynamicTemplateData.setOrderSummaryTotal(sendGridModel.getOrderSummaryTotal());
		dynamicTemplateData.setOrderShippingAddressCompanyName(sendGridModel.getOrderShippingAddressCompanyName());
		dynamicTemplateData.setOrderShippingAddressContactFirstName(sendGridModel.getOrderShippingAddressContactFirstName());
		dynamicTemplateData.setOrderShippingAddressContactLastName(sendGridModel.getOrderShippingAddressContactLastName());
		dynamicTemplateData.setOrderShippingAddressStreet1(sendGridModel.getOrderShippingAddressStreet1());
		dynamicTemplateData.setOrderShippingAddressStreet2(sendGridModel.getOrderShippingAddressStreet2());
		dynamicTemplateData.setOrderShippingAddressCity(sendGridModel.getOrderShippingAddressCity());
		dynamicTemplateData.setOrderShippingAddressState(sendGridModel.getOrderShippingAddressState());
		dynamicTemplateData.setOrderShippingAddressZip(sendGridModel.getOrderShippingAddressZip());
		if(sendGridModel.getOrderShippingAddressPhoneNumber()!=null) {
			dynamicTemplateData.setOrderShippingAddressPhoneNumber(sendGridModel.getOrderShippingAddressPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		}
		dynamicTemplateData.setOrderBillingAddressFirstName(sendGridModel.getOrderBillingAddressFirstName());
		dynamicTemplateData.setOrderBillingAddressLastName(sendGridModel.getOrderBillingAddressLastName());
		dynamicTemplateData.setOrderBillingAddressStreet1(sendGridModel.getOrderBillingAddressStreet1());
		dynamicTemplateData.setOrderBillingAddressStreet2(sendGridModel.getOrderBillingAddressStreet2());
		dynamicTemplateData.setOrderBillingAddressCity(sendGridModel.getOrderBillingAddressCity());
		dynamicTemplateData.setOrderBillingAddressState(sendGridModel.getOrderBillingAddressState());
		dynamicTemplateData.setOrderBillingAddressZip(sendGridModel.getOrderBillingAddressZip());
		if(sendGridModel.getOrderBillingAddressPhoneNumber()!=null) {
			dynamicTemplateData.setOrderBillingAddressPhoneNumber(sendGridModel.getOrderBillingAddressPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		}
		dynamicTemplateData.setOrderShippingMethod(sendGridModel.getOrderShippingMethod());
		dynamicTemplateData.setOrderPaymentMethod(sendGridModel.getOrderPaymentMethod());
		return dynamicTemplateData;
	}
}
