package com.molekule.api.v1.commonframework.model.checkout;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AddressRequestBean {
    private String id;
	private String firstName;
	private String lastName;
	private String streetName;
	private String streetNumber;
	private String state;
	private String postalCode;
	private String city;
	private String region;
	@ApiModelProperty(required = true)
	private String country;
	private String company;
	private String phone;
	private Boolean isBusiness;

}
