package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeliveryItem {

	@JsonProperty("id")
	private String id;

	@JsonProperty(QUANTITY)
	private Double quantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

}
