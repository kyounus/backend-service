package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class UpdateActionsOnCart {

	private long version;
	private List<Action> actions;
	@Data
	public static class Action{
		
		@JsonProperty(ACTION)
		private String actionName;
		private Payment payment;
		private Address address;
		private Type type;
		private String name;
		private Set<String> value;
		private Fields fields;

		@Data
		public static class Fields {
			@JsonProperty("FreightHold")
			private String freightHold;
		}

		@Data
		public static class Type {
			private String id;
			private String typeId;
		}
		@Data
		public static class Address{
			 private String id;
			 private String key;
             private String title;
             private String salutation;
             private String firstName;
             private String lastName;
             private String streetName;
             private String streetNumber;
             private String additionalStreetInfo;
             private String postalCode;
             private String city;
             private String region;
             private String state;
             private String country;
             private String company;
             private String department;
             private String building;
             private String apartment;
             private String pOBox;
             private String phone;
             private String mobile;
             private String email;
             private String fax;
             private String additionalAddressInfo;
             private String externalId;
		}
		@Data
		public static class Payment{
			private String id;
	        private String typeId;
		}
	}
}