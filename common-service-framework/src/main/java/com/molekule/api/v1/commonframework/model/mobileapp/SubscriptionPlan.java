package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class SubscriptionPlan {
	
	private int id;
	private String title;
	private String short_description;
	private String long_description;
	private String frequency_units;
	private String frequency;
	private String trigger_sku;
	private String sort_order;
	private String duration;
	private String status;
	private boolean visible;
	private String created_at;
	private String updated_at;
	private String default_promo_ids;
	private int number_of_free_shipments;
	private String device_sku;
	private boolean payment_required_for_free;
	private String identifier;
	private long plan_price;
}
