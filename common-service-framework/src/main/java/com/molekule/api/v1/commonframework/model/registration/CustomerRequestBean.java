package com.molekule.api.v1.commonframework.model.registration;

import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;

import lombok.Data;

@Data
public class CustomerRequestBean {
	
	private String action;
	private AddressRequestBean shippingAddress;
	private Boolean isDefaultAddress;
	private AddressRequestBean billingAddress;
	private Boolean isDefaultPayment;
	private String paymentType;
	private String paymentToken;
	private String paymentId;
	private String email;
	private String password;
	private String countryCode;
	private String accessToken;
	private String comments;
	private CustomerProfileDetails customerProfileDetails;
	
	private boolean marketingOffers;
}
