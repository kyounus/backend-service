package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import com.molekule.api.v1.commonframework.dto.registration.NonProfitDocumentProofDto;

import lombok.Data;

@Data
public class Business {

	private String businessType;
	private String stockTicker;
	private String stockExchange;
	private References references;
	private BankInformation bankInformation;
	private String email;
	private String phoneNumber;
	private List<NonProfitDocumentProofDto> documentProof;
	
	private String currency;
	
	@Data
	public static class References {
		private BusinessReferences firstReference;
		private BusinessReferences secondReference;
	}
	
}
