package com.molekule.api.v1.commonframework.service.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.AdditionalFreightRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CarrierOptionRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CartCustomField;
import com.molekule.api.v1.commonframework.dto.cart.CartCustomField.CustomAction;
import com.molekule.api.v1.commonframework.dto.cart.CartCustomField.CustomAction.ProductValue;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean.QuoteCustomerDetail;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartDTO;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.ProtectionPlanServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.QuoteCustomFieldCartServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.QuoteCustomerDetailServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.RemoveLineItemBean;
import com.molekule.api.v1.commonframework.dto.cart.SetShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartCustomServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartFilterServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean.Action;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean.Action.Money;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean.Action.Name;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCustomerToCart;
import com.molekule.api.v1.commonframework.dto.cart.WarrentyCustomFieldServiceBean;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;
import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable.ShippingTableValue;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.cart.AvalaraRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.DiscountCodeRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.SetTaxAmount;
import com.molekule.api.v1.commonframework.model.registration.AdditionalFreightInfo;
import com.molekule.api.v1.commonframework.model.registration.CarrierOptionCustomObject;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;
import com.molekule.api.v1.commonframework.service.order.OrderHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.CartServiceUtility;
import com.molekule.api.v1.commonframework.util.CountryCodeEnum;
import com.molekule.api.v1.commonframework.util.CustomerStoreEnum;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("cartHelperService")
public class CartHelperService {

	private static final String SUB_URL = "?priceCurrency=USD&priceCountry=US&expand=masterData.current.masterVariant.attributes[*].value.id&staged=false&expand=productType.id";

	private static final String SLASH_PRODUCTS = "/products";
	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("taxService")
	private TaxService taxService;

	@Autowired
	@Qualifier("handlingCostHelperService")
	HandlingCostHelperService handlingCostHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	private TealiumHelperService tealiumHelperService;

	@Autowired
	@Qualifier("quoteRequestConfirmationMailSender")
	private TemplateMailRequestHelper quoteRequestConfirmationMailSender;

	@Autowired
	@Qualifier("quoteRequestPendingApprovalMailSender")
	private TemplateMailRequestHelper quoteRequestPendingApprovalMailSender;

	@Autowired
	@Qualifier("auroraHelperService")
	AuroraHelperService auroraHelperService;

	@Autowired
	@Qualifier("orderHelperService")
	OrderHelperService orderHelperService;


//	@Value("${currencyValue}")
//	private String currency;
	@Value("${taxMode}")
	private String taxMode;
	@Value("${handlingCostSlugName}")
	private String handlingCostSlugName;
	@Value("${handlingCostName}")
	private String handlingCostName;
	@Value("${companyCode}")
	private String companyCode;
	@Value("${orderTypeKey}")
	private String cartTypeKey;
	@Value("${type}")
	private String type;
//	@Value("${country}")
//	private String country;
	@Value("#{${shipping.methods.map}}") 
	private Map<String,String> shippingMethodDisplayCodes;

	Logger logger = LoggerFactory.getLogger(CartHelperService.class);

	public String processLineItem(JsonObject jsonObject, String cartId, String country) {
		JsonArray lineItemArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();

		Map<String, CartRequestBean> allLineItemWithRequestBeans = new HashMap<>();
		List<CartRequestBean> protectionCartRequestBeans = new ArrayList<>();
		List<ProtectionLineItemMapper> protectionLineItems = new ArrayList<>();

		for (JsonElement jsonElement : lineItemArray) {
			JsonObject lineItem = jsonElement.getAsJsonObject();
			String lineItemId = lineItem.get("id").getAsString();
			String productId = lineItem.get(PRODUCT_ID).getAsString();
			JsonObject varientJSONObejct = lineItem.get(VARIANT).getAsJsonObject();
			CartRequestBean cartRequestBean = new CartRequestBean();
			cartRequestBean.setProductId(productId);
			cartRequestBean.setQuantity(lineItem.get(QUANTITY).getAsInt());
			cartRequestBean.setVariantId(varientJSONObejct.get("id").getAsInt());
			cartRequestBean.setAction(CartServiceUtility.CartActions.ADD_LINE_ITEM.name());
			cartRequestBean.setVariantSKU(varientJSONObejct.get("sku").getAsString());
			allLineItemWithRequestBeans.put(lineItemId, cartRequestBean);

			// Added the clyde lineitem id into the
			if (lineItem.get(CUSTOM) != null && lineItem.get(CUSTOM).getAsJsonObject() != null) {
				JsonObject customJSONObject = lineItem.get(CUSTOM).getAsJsonObject();
				if (customJSONObject.get("fields") != null
						&& customJSONObject.get("fields").getAsJsonObject() != null) {
					JsonObject fieldsJSONObject = customJSONObject.get("fields").getAsJsonObject();
					if (fieldsJSONObject.get("protectionPlanLineItem") != null
							&& fieldsJSONObject.get("protectionPlanLineItem").getAsString() != null) {
						String protectionPlanLineItemId = fieldsJSONObject.get("protectionPlanLineItem").getAsString();
						ProtectionLineItemMapper protectionLineItem = new ProtectionLineItemMapper();
						protectionLineItem.setOriginalProduct(productId);
						protectionLineItem.setLineItemId(protectionPlanLineItemId);
						protectionLineItems.add(protectionLineItem);

					}
				}
			}
		}

		for (ProtectionLineItemMapper protectionLineItem : protectionLineItems) {
			for (JsonElement jsonElement : lineItemArray) {
				JsonObject lineItem = jsonElement.getAsJsonObject();
				String lineItemId = lineItem.get("id").getAsString();
				String productId = lineItem.get(PRODUCT_ID).getAsString();
				if (lineItemId.equals(protectionLineItem.getLineItemId())) {
					protectionLineItem.setProtectionProduct(productId);
					protectionCartRequestBeans.add(allLineItemWithRequestBeans.get(lineItemId));
					allLineItemWithRequestBeans.remove(lineItemId);
				}
			}
		}

		String response = "";

		// Added line items and exclueded the clyde lineitems.
		for (Entry<String, CartRequestBean> entry : allLineItemWithRequestBeans.entrySet()) {
			CartRequestBean cartRequestBean = entry.getValue();
			cartRequestBean.setVariantSKU(null);
			cartRequestBean.setCountry(country);
			response = updateCartById(cartId, cartRequestBean);
		}
		// Added Clyde line items.
		Map<String, String> updatedProdLineItemIds = new HashMap<>();
		if (response != null) {
			JsonObject updatedOrderResponse = MolekuleUtility.parseJsonObject(response);
			JsonArray newLineItemArray = updatedOrderResponse.get(LINE_ITEMS).getAsJsonArray();
			for (JsonElement jsonElement : newLineItemArray) {
				JsonObject updatedLineItem = jsonElement.getAsJsonObject();
				String lineItemId = updatedLineItem.get("id").getAsString();
				String productId = updatedLineItem.get(PRODUCT_ID).getAsString();
				updatedProdLineItemIds.put(productId, lineItemId);
			}
		}

		for (CartRequestBean cartRequestBean : protectionCartRequestBeans) {
			String productId = cartRequestBean.getProductId();
			String lineItemId = null;
			for (ProtectionLineItemMapper protectionLineItem : protectionLineItems) {
				if (protectionLineItem.getProtectionProduct().equals(productId)) {
					lineItemId = updatedProdLineItemIds.get(protectionLineItem.getOriginalProduct());
					break;
				}
			}
			cartRequestBean.setLineItemId(lineItemId);
			cartRequestBean.setProductId(null);
			cartRequestBean.setCountry(country);
			response = updateCartById(cartId, cartRequestBean);
		}

		return response;
	}

	class ProtectionLineItemMapper{
		private String lineItemId;
		private String originalProduct;
		private String protectionProduct;

		public String getLineItemId() {
			return lineItemId;
		}
		public void setLineItemId(String lineItemId) {
			this.lineItemId = lineItemId;
		}
		public String getOriginalProduct() {
			return originalProduct;
		}
		public void setOriginalProduct(String originalProduct) {
			this.originalProduct = originalProduct;
		}
		public String getProtectionProduct() {
			return protectionProduct;
		}
		public void setProtectionProduct(String protectionProduct) {
			this.protectionProduct = protectionProduct;
		}

	}


	public String createCart(String customerId, String country) {
		CreateCartRequestBean createCartRequestBean = new CreateCartRequestBean();
		createCartRequestBean.setCustomerId(customerId);
		createCartRequestBean.setCountry(country);
		String cartResponse = createCart(createCartRequestBean);

		JsonObject cartResponseObject  = MolekuleUtility.parseJsonObject(cartResponse);
		return cartResponseObject.get("id").getAsString();
	}

	public String loadShippingAddress(JsonObject orderJSONObject, String cartId, String country) {
		JsonObject shippingAddressObject = orderJSONObject.get(SHIPPING_ADDRESS).getAsJsonObject();
		AddressRequestBean shippingAddress = new AddressRequestBean(); 
		shippingAddress.setId(shippingAddressObject.has("id") ? shippingAddressObject.get("id").getAsString():null);
		shippingAddress.setFirstName(shippingAddressObject.has(FIRST_NAME) ? shippingAddressObject.get(FIRST_NAME).getAsString():null);
		shippingAddress.setLastName(shippingAddressObject.has(LAST_NAME) ? shippingAddressObject.get(LAST_NAME).getAsString():null);
		shippingAddress.setStreetName(shippingAddressObject.has("streetName") ? shippingAddressObject.get("streetName").getAsString():null);
		shippingAddress.setStreetNumber(shippingAddressObject.has("streetNumber") ? shippingAddressObject.get("streetNumber").getAsString():null);
		shippingAddress.setState(shippingAddressObject.has("state") ? shippingAddressObject.get("state").getAsString():null);
		shippingAddress.setPostalCode(shippingAddressObject.has("postalCode") ? shippingAddressObject.get("postalCode").getAsString():null);
		shippingAddress.setCity(shippingAddressObject.has("city") ? shippingAddressObject.get("city").getAsString():null);
		shippingAddress.setRegion(shippingAddressObject.has("region") ? shippingAddressObject.get("region").getAsString():null);
		shippingAddress.setCountry(shippingAddressObject.has("country") ? shippingAddressObject.get("country").getAsString():null);
		shippingAddress.setCompany(shippingAddressObject.has(MolekuleConstant.COMPANY) ? shippingAddressObject.get(MolekuleConstant.COMPANY).getAsString():null);
		shippingAddress.setPhone(shippingAddressObject.has("phone") ? shippingAddressObject.get("phone").getAsString():null);
		shippingAddress.setIsBusiness(shippingAddressObject.has("isBusiness") ? shippingAddressObject.get("isBusiness").getAsBoolean():null);

		CartRequestBean cartRequestBean = new CartRequestBean();
		cartRequestBean.setAction(CartServiceUtility.CartActions.SET_SHIPPING_ADDRESS.name());	
		cartRequestBean.setShippingAddress(shippingAddress);
		cartRequestBean.setCountry(country);
		return updateCartById(cartId, cartRequestBean);
	}

	public String updateCartById(String cartId, CartRequestBean cartRequestBean) {
		String currency = CountryCodeEnum.valueOf(cartRequestBean.getCountry()).getCurrency();
		String baseUrl = getBaseURL(cartId);
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		Long version = 0l;
		String cartResponse = getCartById(cartId);
		JsonObject cartJsonObj = MolekuleUtility.parseJsonObject(cartResponse);
		if(cartJsonObj.has(STATUS_CODE)) {
			return cartResponse;
		}
		version = cartJsonObj.get(VERSION).getAsLong();
		String channel;
		
		if(cartJsonObj.has(CUSTOM) && cartJsonObj.get(CUSTOM).getAsJsonObject().has(FIELDS)
				&& cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(CHANNEL)) {
			channel = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(CHANNEL).getAsString();
		} else if(cartJsonObj.has(CUSTOMER_ID)) {
			channel = getCustomerChannel(cartJsonObj.get("customerId").getAsString());
		} else {
			channel = D2C;
		}
		UpdateCartServiceBean upaCartServiceBean = new UpdateCartServiceBean();
		QuoteCustomerDetailServiceBean quoteCustomerDetailServiceBean = new QuoteCustomerDetailServiceBean();
		String updatedCartResponse = null;
		
		//set channel to cart
		updatedCartResponse = setChannelToCart(cartJsonObj,channel, cartId, baseUrl, token, updatedCartResponse);
		// setShippingAddress method is used to add shipping address for cart product by
		// cartid "setShippingAddress"
		if (cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_SHIPPING_ADDRESS.name())) {
			updatedCartResponse = setShippingAddress(upaCartServiceBean,version,cartRequestBean,cartId,
					baseUrl,token, channel);
			tealiumHelperService.setCustomerInfo(updatedCartResponse);
		}else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_SHIPPING_METHOD.name())){
			String shippingMethodName = null;
			if (cartRequestBean.getShippingMethod() != null
					&& cartRequestBean.getShippingMethod().getName() != null)
				shippingMethodName = cartRequestBean.getShippingMethod().getName();
			CarrierOptionCustomObject carrierOptionCustomObject = null;
			BigDecimal shippingCost = new BigDecimal(0);
			String customerId;
			if(cartJsonObj.has(ANONYMOUSID)) {
				customerId = cartJsonObj.get(ANONYMOUSID).getAsString();
			} else {
				customerId = cartJsonObj.get("customerId").getAsString();
			}
			AccountCustomObject accountCustomObject = new AccountCustomObject();
			if (cartRequestBean.getCarrierOptionId() != null
					&& cartRequestBean.getCarrierOptionId().length() > 0) {
				accountCustomObject = registrationHelperService.getAccountByCustomerId(customerId,token);
				carrierOptionCustomObject = CartServiceUtility
						.getCarrierOptionObjById(cartRequestBean.getCarrierOptionId(), accountCustomObject);
				String shippingAddressId = null;
				/*
				 * List<ShippingCarrierRelationShip> shippingCarrierRelationshipList =
				 * accountCustomObject.getValue() .getShippingCarrierRelationShip(); if
				 * (shippingCarrierRelationshipList != null) { shippingAddressId =
				 * setShippingCarrierRelationShip(updateCartRequestBean, shippingAddressId,
				 * shippingCarrierRelationshipList); }
				 */
				shippingAddressId = getShippingAddressIdFromCartResponse(cartJsonObj);
				AdditionalFreightInfo additionalFreightInfo = setAdditionalFreightInfo(accountCustomObject,
						shippingAddressId);
				upaCartServiceBean.setVersion(version);
				List<UpdateCartServiceBean.Action> actions = new ArrayList<>();
				UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
				action.setActionName("setCustomShippingMethod");
				action.setShippingMethodName("Contact Carrier");
				UpdateCartServiceBean.Action.ShippingRate shippingRate = new UpdateCartServiceBean.Action.ShippingRate();
				UpdateCartServiceBean.Action.ShippingRate.Price price = new UpdateCartServiceBean.Action.ShippingRate.Price();
				price.setCurrencyCode(currency);
				price.setCentAmount(shippingCost.longValue());
				shippingRate.setPrice(price);
				action.setShippingRate(shippingRate);
				actions.add(action);
				upaCartServiceBean.setActions(actions);
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
				cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
				if (cartJsonObj.get(STATUS_CODE) != null) {
					return updatedCartResponse;
				}
				Long oldFreeShippingDiscount = getOldFreeShippingDiscount(cartJsonObj);
				Long totalDiscountPrice = 0L;
				if (cartJsonObj.has(CUSTOM)) {
					JsonObject fieldsJsonObject = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
					if(fieldsJsonObject.has(TOTAL_DISCOUNT_PRICE)) {
						totalDiscountPrice = fieldsJsonObject.get(TOTAL_DISCOUNT_PRICE).getAsLong();
					}
				}
				Long newTotalDiscountPrice = totalDiscountPrice - oldFreeShippingDiscount;
				CarrierOptionRequestBean carrierOptionRequestBean = new CarrierOptionRequestBean();
				carrierOptionRequestBean.setVersion(cartJsonObj.get(VERSION).getAsLong());
				List<CarrierOptionRequestBean.Action> carrierCustomActions = new ArrayList<>();
				Map<String, Object> ownCarrierJson = getOwnCarrierJSON(carrierOptionCustomObject,
						additionalFreightInfo);
				carrierCustomActions.add(setCarrierCustomAction(OWN_CARRIER, ownCarrierJson.toString()));
				carrierCustomActions.add(setCarrierCustomAction(FREE_SHIPPING_DISCOUNT, "0"));
				carrierCustomActions.add(setCarrierCustomAction(TOTAL_DISCOUNT_PRICE, String.valueOf(newTotalDiscountPrice)));
				boolean isMolekuleCarrierAvalible = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(MOLEKULE_CARRIER);
				if(isMolekuleCarrierAvalible) {
					setUpMolekuleCarrierAvailable(carrierCustomActions);
				}
				carrierOptionRequestBean.setActions(carrierCustomActions);
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token,
						carrierOptionRequestBean);
				cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
				if (cartJsonObj.get(STATUS_CODE) != null) {
					return cartResponse;
				}
			} else {
				shippingCost = new BigDecimal(cartRequestBean.getShippingMethod().getCost());
				shippingCost = shippingCost.multiply(new BigDecimal(100));
				upaCartServiceBean.setVersion(version);
				List<UpdateCartServiceBean.Action> actions = new ArrayList<>();
				UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
				action.setActionName("setCustomShippingMethod");
				String shippingMethodValue = shippingMethodDisplayCodes.get(shippingMethodName);
				if(shippingMethodValue == null) {
					shippingMethodValue  = shippingMethodName;
				}
				action.setShippingMethodName(shippingMethodValue);
				UpdateCartServiceBean.Action.ShippingRate shippingRate = new UpdateCartServiceBean.Action.ShippingRate();	
				UpdateCartServiceBean.Action.ShippingRate.Price price = new UpdateCartServiceBean.Action.ShippingRate.Price();
				price.setCurrencyCode(currency);
				price.setCentAmount(shippingCost.longValue());
				shippingRate.setPrice(price);
				action.setShippingRate(shippingRate);
				if(!"US".equals(cartRequestBean.getCountry())){
					UpdateCartServiceBean.Action.TaxCategory taxCategory = new UpdateCartServiceBean.Action.TaxCategory();
					taxCategory.setId(ctEnvProperties.getTaxCatgeoryId());
					taxCategory.setTypeId("tax-category");
					action.setTaxCategory(taxCategory);	
				}
				actions.add(action);
				upaCartServiceBean.setActions(actions);
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
				cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
				if (cartJsonObj.get(STATUS_CODE) != null) {
					return updatedCartResponse;
				}
				version = cartJsonObj.get(VERSION).getAsLong();
				Long oldFreeShippingDiscount = getOldFreeShippingDiscount(cartJsonObj);
				Long freeShippingDiscount = 0L;
				Long totalDiscountPrice = 0L;
				if(cartJsonObj.has(SHIPPING_INFO)
						&& cartJsonObj.get(SHIPPING_INFO).getAsJsonObject().has(DISCOUNTED_PRICE)) {
					JsonArray includedDiscountsJsonArray = cartJsonObj.get(SHIPPING_INFO).getAsJsonObject().get(DISCOUNTED_PRICE).getAsJsonObject().get(INCLUDED_DISCOUNTS).getAsJsonArray();
					for(int i=0; i<includedDiscountsJsonArray.size();i++) {
						JsonObject includedDiscountsJson = includedDiscountsJsonArray.get(i).getAsJsonObject();
						long discountedAmount = includedDiscountsJson.get(DISCOUNTED_AMOUNT).getAsJsonObject().get(CENT_AMOUNT).getAsLong();
						freeShippingDiscount = freeShippingDiscount + discountedAmount;
						if(cartJsonObj.has(CUSTOM)) {
							JsonObject fieldObjectJson = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
							totalDiscountPrice = fieldObjectJson.has(TOTAL_DISCOUNT_PRICE) ? fieldObjectJson.get(TOTAL_DISCOUNT_PRICE).getAsLong() : 0;
							totalDiscountPrice = totalDiscountPrice+freeShippingDiscount - oldFreeShippingDiscount;
						}
					}
					updatedCartResponse = updatePoNumberCustomFieldIncart(baseUrl,token,freeShippingDiscount.toString(),FREE_SHIPPING_DISCOUNT,version);
					cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
					if (cartJsonObj.get(STATUS_CODE) != null) {
						return updatedCartResponse;
					}
					version = cartJsonObj.get(VERSION).getAsLong();
					updatedCartResponse = updatePoNumberCustomFieldIncart(baseUrl,token,totalDiscountPrice.toString(),TOTAL_DISCOUNT_PRICE,version);
					cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
					if (cartJsonObj.get(STATUS_CODE) != null) {
						return updatedCartResponse;
					}
				} else {
					if(cartJsonObj.has(CUSTOM)) {
						JsonObject fieldObjectJson = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
						totalDiscountPrice = fieldObjectJson.has(TOTAL_DISCOUNT_PRICE) ? fieldObjectJson.get(TOTAL_DISCOUNT_PRICE).getAsLong() : 0;
						totalDiscountPrice = totalDiscountPrice - oldFreeShippingDiscount;
					}
					updatedCartResponse = updatePoNumberCustomFieldIncart(baseUrl,token,"0",FREE_SHIPPING_DISCOUNT,version);
					cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
					if (cartJsonObj.get(STATUS_CODE) != null) {
						return updatedCartResponse;
					}
					version = cartJsonObj.get(VERSION).getAsLong();
					updatedCartResponse = updatePoNumberCustomFieldIncart(baseUrl,token,totalDiscountPrice.toString(),TOTAL_DISCOUNT_PRICE,version);
					cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
					if (cartJsonObj.get(STATUS_CODE) != null) {
						return updatedCartResponse;
					}
				}
				if(B2B.equals(channel)) {
					String shippingAddressId = cartJsonObj.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
					List<AdditionalFreightInfo> additionalFreightInfos = accountCustomObject.getValue()
							.getAdditionalFreightInfos();
					AdditionalFreightInfo additionalFreightInfo = null;
					if(additionalFreightInfos != null) {
						Iterator<AdditionalFreightInfo> addIterator = additionalFreightInfos.iterator();
						while (addIterator.hasNext()) {
							additionalFreightInfo = addIterator.next();
							if (shippingAddressId.equals(additionalFreightInfo.getShippingAddressId())) {
								break;
							}
							additionalFreightInfo = null;
						}
					}
					if (additionalFreightInfo != null) {
						AdditionalFreightRequestBean additionalFreightRequestBean = new AdditionalFreightRequestBean();
						additionalFreightRequestBean.setVersion(cartJsonObj.get(VERSION).getAsLong());
						List<AdditionalFreightRequestBean.Action> additionalFreightCustomList = new ArrayList<>();
						AdditionalFreightRequestBean.Action additionalFreightCustomAction = new AdditionalFreightRequestBean.Action();
						Map<String, Object> additionalFrieghtMap = new HashMap<>();
						additionalFrieghtMap.put(LOADING_DOCK, additionalFreightInfo.isLoadingDock());
						additionalFrieghtMap.put(FORK_LIFT, additionalFreightInfo.isForkLift());
						additionalFrieghtMap.put(DELIVERY_APPOINTMENT_REQUIRED,
								additionalFreightInfo.isDeliveryAppointmentRequired());
						additionalFrieghtMap.put(RECEIVING_HOURS, additionalFreightInfo.getReceivingHours());
						additionalFrieghtMap.put("instructions", additionalFreightInfo.getInstructions());
						additionalFreightCustomAction.setActionName(SET_CUSTOM_FIELD);
						additionalFreightCustomAction.setName(MOLEKULE_CARRIER);
						additionalFreightCustomAction.setValue(additionalFrieghtMap.toString());
						additionalFreightCustomList.add(additionalFreightCustomAction);
						additionalFreightRequestBean.setActions(additionalFreightCustomList);
						updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token,
								additionalFreightRequestBean);
					}
				}
				cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
				if (cartJsonObj.get(STATUS_CODE) != null) {
					return updatedCartResponse;
				}
				boolean isOwnCarrierAvalible = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(OWN_CARRIER);
				if(isOwnCarrierAvalible) {
					AdditionalFreightRequestBean additionalFreightRequestBean = new AdditionalFreightRequestBean();
					additionalFreightRequestBean.setVersion(cartJsonObj.get(VERSION).getAsLong());
					List<AdditionalFreightRequestBean.Action> additionalFreightCustomList = new ArrayList<>();
					AdditionalFreightRequestBean.Action owncarrAction = new AdditionalFreightRequestBean.Action();
					owncarrAction.setActionName(SET_CUSTOM_FIELD);
					owncarrAction.setName(OWN_CARRIER);
					owncarrAction.setValue(null);
					additionalFreightCustomList.add(owncarrAction);
					additionalFreightRequestBean.setActions(additionalFreightCustomList);
					updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token,
							additionalFreightRequestBean);
					cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
					if (cartJsonObj.get(STATUS_CODE) != null) {
						return updatedCartResponse;
					}
				}
			}
			// Update tax amount to shipping method START
			if ("US".equals(cartRequestBean.getCountry())) {
				UpdateCartServiceBean serviceBean = CartServiceUtility.getShippingMethodTaxRequestBean(cartJsonObj,
						currency);
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, serviceBean);
				// Update tax amount to shipping method END
				cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
				if (cartJsonObj.get(STATUS_CODE) != null) {
					return updatedCartResponse;
				}
				// calculate the tax
				JsonArray jsonArray = cartJsonObj.get(LINE_ITEMS).getAsJsonArray();
				SetTaxAmount setTaxAmount = null;
				if (jsonArray.size() > 0) {
					if (D2C.equals(channel) || !getTaxExempt(token, cartJsonObj)) {
						Object taxResponse = calculateTax(updatedCartResponse);
						if (taxResponse instanceof String) {
							return (String) taxResponse;
						}
						setTaxAmount = (SetTaxAmount) taxResponse;
					} else {
						setTaxAmount = CartServiceUtility.updateNoTaxToCart(updatedCartResponse, currency);
					}
					updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, setTaxAmount);
				}
			}
			updatedCartResponse = updateCustomField(updatedCartResponse, cartId, baseUrl, token);
		}else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_PO_NUMBER.name())) {
			updatedCartResponse = setPONumber(cartId, cartRequestBean, baseUrl, token, version,
					upaCartServiceBean);
		} else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_PERIOD.name())) {
			updatedCartResponse = setPeriodForFilter(cartId, cartRequestBean, baseUrl, token, version);
		}else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_EMAIL.name())) {
			updatedCartResponse = setEmailToCart(cartId, cartRequestBean, baseUrl, token, version);
		}  else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_CART_AS_QUOTE.name())) {
			assignCartOrQuote(cartId, baseUrl, token,true);
			quoteCustomerDetailServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			List<QuoteCustomerDetailServiceBean.Action> actionList = new ArrayList<>();
			//remove handling cost
			actionList.add(setQuoteCustomerBeanAction(EMPTY ,HANDLING_COST));
			quoteCustomerDetailServiceBean.setActions(actionList);
			updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, quoteCustomerDetailServiceBean);
		} else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_CUSTOMER_FOR_QUOTE.name())) {
			updatedCartResponse = setCustomerForQuote(cartId, baseUrl, token,cartRequestBean,quoteCustomerDetailServiceBean);
			tealiumHelperService.setQuoteInformation(updatedCartResponse);
		} else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.SET_QUOTE_AS_CART.name())) {
			updatedCartResponse = setQuoteAsCartValue(cartId, baseUrl, token, quoteCustomerDetailServiceBean, channel);
		} else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.MERGE_CART.name())) {
			updatedCartResponse = doCartMergeOperation(cartId,cartRequestBean.getCustomerId(),token,upaCartServiceBean,updatedCartResponse, channel, cartRequestBean);
			JsonObject a = MolekuleUtility.parseJsonObject(updatedCartResponse);
			cartId = a.get("id").getAsString();
			baseUrl =getBaseURL(cartId);

		} else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.ADD_LINE_ITEM.name())) {
			String response = checkLintItemQuantity(cartJsonObj, cartRequestBean.getProductId());
			if(null != response) {
				return response;
			}
			updatedCartResponse = addLineItemToCart(cartId, cartRequestBean, baseUrl, token, upaCartServiceBean,cartResponse);
			updatedCartResponse = calculateHandlingCharge(cartId, baseUrl, token, upaCartServiceBean,
					updatedCartResponse, channel,currency,cartRequestBean.getCountry());
		}else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.CHANGE_LINE_ITEM_QTY.name())){
			updatedCartResponse = changeLineItemQuantityInCart(cartId, cartRequestBean, baseUrl, token, upaCartServiceBean);
			updatedCartResponse = calculateHandlingCharge(cartId, baseUrl, token, upaCartServiceBean,
					updatedCartResponse, channel,currency,cartRequestBean.getCountry());
		} else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.REMOVE_LINE_ITEM.name())){
			updatedCartResponse = removeLineItemInCart(cartId, cartRequestBean, baseUrl, token);
			updatedCartResponse = calculateHandlingCharge(cartId, baseUrl, token, upaCartServiceBean,
					updatedCartResponse, channel,currency,cartRequestBean.getCountry());
		}else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.GIFT.name())) {
			updatedCartResponse = setGift(baseUrl, token, cartRequestBean, version,cartJsonObj);
		}else if(cartRequestBean.getAction().equals(CartServiceUtility.CartActions.WARRANTY.name())) {
			updatedCartResponse = updateWarrentyCustomField(baseUrl, token, cartRequestBean, version);
		}

		cartJsonObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
		//set promotionMessage and promotionProduct in custom fields
		JsonObject updatedCartResponseObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		if(updatedCartResponseObject.has(CUSTOM)) {
			JsonObject fields =updatedCartResponseObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			updatedCartResponse = setPromotionDetails(updatedCartResponse, baseUrl, token);
			boolean hasGift = fields.has("gift");
			if(hasGift) {
				JsonArray lineItemArray = updatedCartResponseObject.get(LINE_ITEMS).getAsJsonArray();
				if(lineItemArray.size() <= 0) {
					cartRequestBean.setGift(false);
					updatedCartResponseObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
					updatedCartResponse = setGift(baseUrl, token, cartRequestBean, updatedCartResponseObject.get(VERSION).getAsLong(), updatedCartResponseObject);    
				}
			}
		}

		return updatedCartResponse;
	}

	private String getBaseURL(String cartId) {
		return ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId + "?expand=lineItems[*].variant.attributes[*].value.typeId&expand=lineItems[*].productType.typeId"
				+ "&expand=lineItems[*].discountedPricePerQuantity[*].discountedPrice.includedDiscounts[*].discount.id"
				+ "&expand=custom.fields.promotionProduct.id";
	}

	private String updateWarrentyCustomField(String baseUrl, String token, CartRequestBean cartRequestBean, Long version) {
		WarrentyCustomFieldServiceBean warrentyCustomFieldServiceBean = new WarrentyCustomFieldServiceBean();
		warrentyCustomFieldServiceBean.setVersion(version);
		List<WarrentyCustomFieldServiceBean.Action> actionList = new ArrayList<>();
		actionList.add(setWarrentyServiceBeanAction(cartRequestBean.getOrderTag(),"orderTag"));
		actionList.add(setWarrentyServiceBeanAction(cartRequestBean.getWarrantyComment(),"warrantyComment"));
		actionList.add(setWarrentyServiceBeanAction(cartRequestBean.getPreviousOrderNumber(),"previousOrderNumber"));
		warrentyCustomFieldServiceBean.setActions(actionList);
		return (String)netConnectionHelper.sendPostRequest(baseUrl, token, warrentyCustomFieldServiceBean);
	}

	private WarrentyCustomFieldServiceBean.Action setWarrentyServiceBeanAction(String value,String fieldName ) {
		WarrentyCustomFieldServiceBean.Action action = new WarrentyCustomFieldServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(fieldName);
		action.setValue(value);
		return action;
	}

	private String setEmailToCart(String cartId, CartRequestBean cartRequestBean, String baseUrl, String token,
			Long version) {
		String emailStr = "email=\"" + cartRequestBean.getEmail() + "\"";
		String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/customers?where=" + MolekuleUtility.convertToEncodeUri(emailStr);
		String customerResponse = null;
		try {
			customerResponse = netConnectionHelper.httpConnectionHelper(customerByIdUrl, token);
		} catch (IOException e1) {
			logger.error("Exception occured when getting customer Data", e1);
		}
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		UpdateCustomerToCart updateCustomerToCart = new UpdateCustomerToCart();
		updateCustomerToCart.setVersion(version);
		List<UpdateCustomerToCart.Action> actions = new ArrayList<>();

		if (customerObject.get("results").getAsJsonArray().size() > 0) {
			UpdateCustomerToCart.Action action1 = new UpdateCustomerToCart.Action();
			action1.setActionName("setAnonymousId");
			action1.setAnonymousId(null);
			actions.add(action1);
			UpdateCustomerToCart.Action action2 = new UpdateCustomerToCart.Action();
			action2.setActionName("setCustomerId");
			action2.setCustomerId(customerObject.get("results").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString());
			actions.add(action2);
			updateCustomerToCart.setActions(actions);
		}
		UpdateCustomerToCart.Action action = new UpdateCustomerToCart.Action();
		action.setActionName("setCustomerEmail");
		action.setEmail(cartRequestBean.getEmail());
		actions.add(action);
		updateCustomerToCart.setActions(actions);
		return (String)netConnectionHelper.sendPostRequest(baseUrl, token,updateCustomerToCart);
	}

	private String setChannelToCart(JsonObject jsonObject, String channel, String cartId, String baseUrl, String token, String updatedCartResponse) {
		if(jsonObject.has(CUSTOM)) {
			JsonObject fields =jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(!fields.has(CHANNEL)) {
				UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
				List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
				UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
				action.setActionName(SET_CUSTOM_FIELD);
				action.setName(CHANNEL);
				action.setValue(channel);
				updateActionList.add(action);
				updateCartCustomServiceBean.setActions(updateActionList);
				updateCartCustomServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
				return (String)netConnectionHelper.sendPostRequest(baseUrl, token, updateCartCustomServiceBean);
			}
		} else {
			UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
			List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
			UpdateCartCustomServiceBean.Action customTypeAction = new UpdateCartCustomServiceBean.Action();
			UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
			UpdateCartCustomServiceBean.Action.Type type = new UpdateCartCustomServiceBean.Action.Type();
			
			customTypeAction.setActionName(SET_CUSTOM_TYPE);
			type.setId(ctEnvProperties.getOrderTypeId());
			type.setTypeId("type");
			customTypeAction.setType(type);
			updateActionList.add(customTypeAction);
			
			action.setActionName(SET_CUSTOM_FIELD);
			action.setName(CHANNEL);
			action.setValue(channel);
			updateActionList.add(action);
			updateCartCustomServiceBean.setActions(updateActionList);
			updateCartCustomServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			return (String)netConnectionHelper.sendPostRequest(baseUrl, token, updateCartCustomServiceBean);
			
		}
		return updatedCartResponse;
	}

	private String setPromotionDetails(String updatedCartResponse, String baseUrl, String token) {
		JsonObject updatedCartResponseObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		if (updatedCartResponseObject.get(STATUS_CODE) != null) {
			return updatedCartResponse;
		}
		JsonArray lineItemArray = updatedCartResponseObject.get("lineItems").getAsJsonArray();
		BigDecimal sortOrder = BigDecimal.ZERO;
		String promotionMessage = EMPTY;
		String promotionProduct = EMPTY;
		for(int i=0;i<lineItemArray.size();i++) {
			JsonObject lineItemObject = lineItemArray.get(i).getAsJsonObject();
			JsonArray discountedPricePerQuantityArray = lineItemObject.get("discountedPricePerQuantity").getAsJsonArray();
			for(int j=0; j<discountedPricePerQuantityArray.size();j++) {
				JsonObject discountedPricePerQuantityObj = discountedPricePerQuantityArray.get(j).getAsJsonObject();
				JsonArray includedDiscountsArray = discountedPricePerQuantityObj.get("discountedPrice").getAsJsonObject().get("includedDiscounts").getAsJsonArray();
				for(int k=0; k<includedDiscountsArray.size(); k++) {
					JsonObject includedDiscountsObject = includedDiscountsArray.get(k).getAsJsonObject();
					JsonObject discountObject = includedDiscountsObject.get("discount").getAsJsonObject();
					JsonObject obj = discountObject.has("obj")?discountObject.get("obj").getAsJsonObject():new JsonObject();
					if(obj.has("stackingMode") && obj.get("stackingMode").getAsString().equals("Stacking")) {
						String currentSortOrder =obj.has("sortOrder")?obj.get("sortOrder").getAsString():"0";
						BigDecimal currentSortDecimalValue = new BigDecimal(currentSortOrder);
						if(obj.has(CUSTOM) && obj.get(CUSTOM).getAsJsonObject().has("fields")) {
							JsonObject fieldsObject = obj.get(CUSTOM).getAsJsonObject().get("fields").getAsJsonObject();
							if(currentSortDecimalValue.compareTo(sortOrder) > 0 && fieldsObject.size()>0 ) {
								sortOrder = currentSortDecimalValue;
								promotionMessage = fieldsObject.has(PROMOTION_MESSAGE)?fieldsObject.get(PROMOTION_MESSAGE).getAsString():promotionMessage;
								promotionProduct = fieldsObject.has(PROMOTION_PRODUCT)?fieldsObject.get(PROMOTION_PRODUCT).getAsJsonObject().get("id").getAsString():promotionProduct;
							}
						}
					}
				}
			}
		}
		updatedCartResponse = updateCustomField(updatedCartResponse, baseUrl, token, updatedCartResponseObject,
				promotionMessage, promotionProduct);

		return updatedCartResponse;
	}

	private String updateCustomField(String updatedCartResponse, String baseUrl, String token,
			JsonObject updatedCartResponseObject, String promotionMessage, String promotionProduct) {
		CartCustomField cartCustomField = new CartCustomField();
		cartCustomField.setVersion(updatedCartResponseObject.get(VERSION).getAsLong());
		List<CustomAction> actionList = new ArrayList<>();
		JsonObject cartResponseJson = MolekuleUtility.parseJsonObject(updatedCartResponse);
		JsonObject fieldObject = new JsonObject();
		if(cartResponseJson.has(CUSTOM) && cartResponseJson.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			fieldObject = cartResponseJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}



		CustomAction promotionMessageAction = new CustomAction();
		promotionMessageAction.setActionName(SET_CUSTOM_FIELD);
		promotionMessageAction.setName(PROMOTION_MESSAGE);
		promotionMessageAction.setValueObject(promotionMessage);
		actionList.add(promotionMessageAction);

		CustomAction promotionProductAction = new CustomAction();
		promotionProductAction.setActionName(SET_CUSTOM_FIELD);
		promotionProductAction.setName(PROMOTION_PRODUCT);
		if(StringUtils.hasText(promotionProduct)) {
			ProductValue value = new ProductValue();
			value.setTypeId("product");
			value.setId(promotionProduct);
			promotionProductAction.setValueObject(value);
			actionList.add(promotionProductAction);
		}else {
			if(fieldObject.has("promotionProduct")) {
				promotionProductAction.setValueObject(null);
				actionList.add(promotionProductAction);
			}
		}

		if(!actionList.isEmpty()) {
			cartCustomField.setActions(actionList);
			updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, cartCustomField);
		}
		return updatedCartResponse;
	}

	private String checkLintItemQuantity(JsonObject jsonObject, String productId) {
		Boolean checkIfQuantityExceedsTwenty = false;
		JsonArray lineItemsArray = jsonObject.getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		ErrorResponse errorResponse = new ErrorResponse(400,
				"You cannot add more than 20 per lineItem.");
		for (int i = 0; i < lineItemsArray.size(); i++) {
			JsonObject lineItemJsonObject = lineItemsArray.get(i).getAsJsonObject();
			Integer quantity = lineItemJsonObject.get(QUANTITY).getAsInt();
			String lineItemProductId = lineItemJsonObject.get(PRODUCT_ID).getAsString();
			if(Boolean.TRUE.equals(lineItemProductId.equals(productId))) {
				if (quantity == 20) {
					checkIfQuantityExceedsTwenty = true;
				} else if(quantity > 19){
					return errorResponse.toString();
				} else {
					return null;
				}
			} 
		}
		if (Boolean.TRUE.equals(checkIfQuantityExceedsTwenty)) {
			return errorResponse.toString();
		}
		return null;
	}

	public String getCustomerChannel(String customerId) {
		String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_CUSTOMERS_SLASH + customerId;
		String token = ctServerHelperService.getStringAccessToken();
		String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		if (customerObject.get(STATUS_CODE) != null) {
			return customerResponse;
		}
		JsonObject fieldsJsonObject = customerObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		return fieldsJsonObject.has(CHANNEL) ? fieldsJsonObject.get(CHANNEL).getAsString(): null;

	}

	private String setGift(String baseUrl, String token,
			CartRequestBean cartRequestBean, Long version,JsonObject cartJson) {
		SetShippingAddressRequestBean shippingAddressRequestBean= new SetShippingAddressRequestBean();
		List<com.molekule.api.v1.commonframework.dto.cart.Action> actions = new ArrayList<>();
		List<com.molekule.api.v1.commonframework.dto.cart.Action> customActions = setGiftCustomAttributes(
				cartRequestBean, String.valueOf(cartRequestBean.isGift()), null, null,cartJson);
		actions.addAll(customActions);
		shippingAddressRequestBean.setActions(actions);
		shippingAddressRequestBean.setVersion(version);
		return (String)netConnectionHelper.sendPostRequest(baseUrl, token, shippingAddressRequestBean);
	}

	private String getShippingAddressIdFromCartResponse(JsonObject jsonObject) {
		String shippingAddresId = null;
		boolean isShippingAvalible = jsonObject.has(SHIPPING_ADDRESS);
		if(isShippingAvalible) {
			JsonObject shippingJson =  jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject();
			shippingAddresId = shippingJson.get("id").getAsString();
		}
		return shippingAddresId;
	}
	private String removeLineItemInCart(String cartId, CartRequestBean cartRequestBean, String baseUrl,
			String token) {
		String updatedCartResponse;
		if(cartRequestBean.isSubscriptionEnabled()){
			updatedCartResponse = removeLineItem(cartId, cartRequestBean, baseUrl, token);
		}else {
			updatedCartResponse = removeMasterChildItem(cartId, cartRequestBean, baseUrl, token);
		}
		return updatedCartResponse;
	}
	private String removeMasterChildItem(String cartId, CartRequestBean cartRequestBean, String baseUrl,
			String token) {
		String updatedCartResponse = null;
		String subscriptionProductId;
		String productId = null;
		String masterProductLineItemId = cartRequestBean.getLineItemId();
		String subscriptionLineItemId = null;
		String protectionLineItemId = null;
		String cartResponse = getCartById(cartId);
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonArray cartLineItemArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i=0;i<cartLineItemArray.size();i++) {
			JsonObject lineItem = cartLineItemArray.get(i).getAsJsonObject();
			if(lineItem.get("id").getAsString().equals(masterProductLineItemId)) {
				productId = lineItem.get(PRODUCT_ID).getAsString();
				if(lineItem.has(CUSTOM) && lineItem.get(CUSTOM).getAsJsonObject().has(FIELDS) &&lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(PROTECTION_PLAN_LINE_ITEM)) {
					protectionLineItemId = lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(PROTECTION_PLAN_LINE_ITEM).getAsString();
				}
			}
		}
		String productBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_PRODUCTS;
		// Get Subscription By ProductId
		String getSubIdUrl = productBaseUrl + "/" + productId
				+ SUB_URL;
		String subProductResponse = netConnectionHelper.sendGetWithoutBody(token, getSubIdUrl);
		if (subProductResponse != null) {
			subscriptionProductId = CartServiceUtility.getSubscriptionProductId(subProductResponse);
			if(subscriptionProductId!=null) {
				for(int i=0;i<cartLineItemArray.size();i++) {
					if(cartLineItemArray.get(i).getAsJsonObject().get(PRODUCT_ID).getAsString().equals(subscriptionProductId)) {
						subscriptionLineItemId = cartLineItemArray.get(i).getAsJsonObject().get("id").getAsString();
					}
				}
				// remove subscription product in cart
				RemoveLineItemBean subscriptionBean = new RemoveLineItemBean();
				subscriptionBean.setVersion(getCartCurrentVersionByCartId(cartId));
				List<RemoveLineItemBean.Action> subActionList = new ArrayList<>();
				RemoveLineItemBean.Action subAction = new RemoveLineItemBean.Action();
				subAction.setActionName("removeLineItem");
				subAction.setLineItemId(subscriptionLineItemId);
				subActionList.add(subAction);
				subscriptionBean.setActions(subActionList);
				netConnectionHelper.sendPostRequest(baseUrl, token, subscriptionBean);
			}
		}
		updatedCartResponse = removeLineItem(cartId, cartRequestBean, baseUrl, token);
		if(StringUtils.hasText(protectionLineItemId)) {
			CartRequestBean protectionCartRequestBean = new CartRequestBean();
			protectionCartRequestBean.setLineItemId(protectionLineItemId);
			updatedCartResponse = removeLineItem(cartId, protectionCartRequestBean, baseUrl, token);
		}
		return updatedCartResponse;
	}

	private String removeLineItem(String cartId, CartRequestBean cartRequestBean, String baseUrl,
			String token) {
		String cartResponse = getCartById(cartId);
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		// remove product
		RemoveLineItemBean removeLineItemBean = new RemoveLineItemBean();
		removeLineItemBean.setVersion(cartJsonObject.get(VERSION).getAsLong());
		List<RemoveLineItemBean.Action> actionList = new ArrayList<>();
		RemoveLineItemBean.Action action = new RemoveLineItemBean.Action();
		action.setActionName("removeLineItem");
		action.setLineItemId(cartRequestBean.getLineItemId());
		actionList.add(action);
		checkAndRemoveProtectionPlanLineItemInParent(cartRequestBean, cartJsonObject, actionList);
		removeTotalDiscountInCustomField(actionList);
		removeLineItemBean.setActions(actionList);
		return (String)netConnectionHelper.sendPostRequest(baseUrl, token, removeLineItemBean);
	}

	private void removeTotalDiscountInCustomField(List<RemoveLineItemBean.Action> actionList) {
		RemoveLineItemBean.Action totalDiscountAction = new RemoveLineItemBean.Action();
		totalDiscountAction.setActionName(SET_CUSTOM_FIELD);
		totalDiscountAction.setName(TOTAL_DISCOUNT_PRICE);
		totalDiscountAction.setValue(String.valueOf(Long.parseLong(String.valueOf(0))));
		actionList.add(totalDiscountAction);
	}

	private void checkAndRemoveProtectionPlanLineItemInParent(CartRequestBean cartRequestBean,
			JsonObject cartJsonObject, List<RemoveLineItemBean.Action> actionList) {
		JsonArray cartLineItemArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i=0;i<cartLineItemArray.size();i++) {
			JsonObject lineItem = cartLineItemArray.get(i).getAsJsonObject();
			if(lineItem.has(CUSTOM) && 
					lineItem.get(CUSTOM).getAsJsonObject().has(FIELDS) &&
					lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(PROTECTION_PLAN_LINE_ITEM)&&
					lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(PROTECTION_PLAN_LINE_ITEM).getAsString().equals(cartRequestBean.getLineItemId())) {
				RemoveLineItemBean.Action protectionAction = new RemoveLineItemBean.Action();
				protectionAction.setActionName("setLineItemCustomType");
				protectionAction.setLineItemId(lineItem.get("id").getAsString());
				RemoveLineItemBean.Action.Type type = new RemoveLineItemBean.Action.Type();
				type.setTypeId("type");
				type.setId(ctEnvProperties.getLineItemTypeId());
				protectionAction.setType(type);
				RemoveLineItemBean.Action.Fields fields = new RemoveLineItemBean.Action.Fields();
				fields.setProtectionPlanLineItem("");
				protectionAction.setFields(fields);
				actionList.add(protectionAction);
			}
		}
	}
	private String changeLineItemQuantityInCart(String cartId, CartRequestBean cartRequestBean, String baseUrl,
			String token, UpdateCartServiceBean upaCartServiceBean) {
		String cartResponse = getCartById(cartId);
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonArray updatedCartLineItemArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		upaCartServiceBean.setVersion(cartJsonObject.get(VERSION).getAsLong());
		List<UpdateCartServiceBean.Action> actionList = new ArrayList<>();
		UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
		action.setActionName(CHANGE_LINE_ITEM_QUANTITY);
		action.setLineItemId(cartRequestBean.getLineItemId());
		action.setQuantity(cartRequestBean.getQuantity());
		checkAndSetProtectionLineItemAction(cartRequestBean, updatedCartLineItemArray, actionList);
		actionList.add(action);
		upaCartServiceBean.setActions(actionList);
		return (String) netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
	}

	private void checkAndSetProtectionLineItemAction(CartRequestBean cartRequestBean, JsonArray updatedCartLineItemArray,
			List<UpdateCartServiceBean.Action> actionList) {
		Map<String,Integer> quantityMap = new HashMap<>();
		for(int i=0;i<updatedCartLineItemArray.size();i++) {
			quantityMap.put(updatedCartLineItemArray.get(i).getAsJsonObject().get("id").getAsString(),updatedCartLineItemArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt());
		}
		for(int i=0;i<updatedCartLineItemArray.size();i++) {
			JsonObject lineItem = updatedCartLineItemArray.get(i).getAsJsonObject();
			if(lineItem.has(CUSTOM) && 
					lineItem.get(CUSTOM).getAsJsonObject().has(FIELDS) &&
					lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(PROTECTION_PLAN_LINE_ITEM)&&
					lineItem.get("id").getAsString().equals(cartRequestBean.getLineItemId())) {
				int protectionQuantity = quantityMap.get(lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(PROTECTION_PLAN_LINE_ITEM).getAsString());
				if(cartRequestBean.getQuantity()<protectionQuantity) {
					UpdateCartServiceBean.Action protectionAction = new UpdateCartServiceBean.Action();
					protectionAction.setActionName(CHANGE_LINE_ITEM_QUANTITY);
					protectionAction.setLineItemId(lineItem.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(PROTECTION_PLAN_LINE_ITEM).getAsString());
					protectionAction.setQuantity(cartRequestBean.getQuantity());
					actionList.add(protectionAction);
				}
			}
		}
	}
	private String addLineItemToCart(String cartId, CartRequestBean cartRequestBean, String baseUrl,
			String token, UpdateCartServiceBean upaCartServiceBean, String getCartResponse) {
		String updatedCartResponse = getCartResponse;
		String productId = cartRequestBean.getProductId();
		String variantSku = cartRequestBean.getVariantSKU();
		if(cartRequestBean.getFilterBuyOption() != null && cartRequestBean.getFilterBuyOption().equals(CartServiceUtility.filterBuyOption.FILTER_ONE_TIME.name())) {
			updatedCartResponse = addMasterLineItem(cartId,upaCartServiceBean,token,cartRequestBean,baseUrl);
		} else if(cartRequestBean.getFilterBuyOption() != null && cartRequestBean.getFilterBuyOption().equals(CartServiceUtility.filterBuyOption.FILTER_AUTO_REFILL.name())||
				!cartRequestBean.isSubscriptionEnabled() && variantSku == null) {
			updatedCartResponse = addMasterLineItem(cartId,upaCartServiceBean,token,cartRequestBean,baseUrl);
			boolean isSubscriptionProduct = checkSubscriptionProduct(productId,token);
			if(isSubscriptionProduct) {
				updatedCartResponse = addSubscriptionToCart(cartId, cartRequestBean, baseUrl, token, productId,updatedCartResponse);
			}
		} else if(cartRequestBean.isSubscriptionEnabled()) {
			updatedCartResponse = addSubscriptionToCart(cartId, cartRequestBean, baseUrl, token, productId,getCartResponse);
		} else if(variantSku !=null) {
			String productType = null;
			String productBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_PRODUCTS
					+ "?expand=productType.typeId"
					+ "&expand=masterData.current.masterVariant.attributes[*].value[*].typeId" 
					+ "&where="+ MolekuleUtility.convertToEncodeUri("masterData(current(masterVariant(sku=\"" + variantSku +"\")))");
			String productResponse = null;
			try {
				productResponse = netConnectionHelper.httpConnectionHelper(productBaseUrl,token);
			} catch (IOException e) {
				logger.error("IOException Occured", e);
			}
			JsonObject resultObject = MolekuleUtility.parseJsonObject(productResponse);
			JsonArray resultArray = resultObject.get(RESULTS).getAsJsonArray();
			JsonObject productJsonObject = new JsonObject();
			if(resultArray.size() >0) {
				productJsonObject = resultArray.get(0).getAsJsonObject();
			}
			if (productJsonObject.get(STATUS_CODE) != null) {
				productType = "";
			}else {
				productType =  productJsonObject.get("productType").getAsJsonObject().get("obj").getAsJsonObject().get("name").getAsString();
			}
			if(productType.equals(ctEnvProperties.getBundleProductTypeName())) {
				updatedCartResponse = addBundleProductToCart(cartId,productJsonObject,token,baseUrl,upaCartServiceBean,cartRequestBean,getCartResponse);
			}else {
				String cartResponse = getCartById(cartId);
				List<String> oldLineItemList = getLineItemIdList(cartResponse);
				updatedCartResponse = addMasterLineItem(cartId,upaCartServiceBean,token,cartRequestBean,baseUrl);
				boolean isSubscriptionProduct = checkSubscriptionProduct(productJsonObject.get("id").getAsString(),token);
				if(isSubscriptionProduct) {
					updatedCartResponse = addSubscriptionToCart(cartId, cartRequestBean, baseUrl, token, productJsonObject.get("id").getAsString(),updatedCartResponse);
				}
				List<String> newLineItemList = getLineItemIdList(updatedCartResponse);
				newLineItemList.removeAll(oldLineItemList);
				if(!newLineItemList.isEmpty() && cartRequestBean.getLineItemId()!=null){
					updatedCartResponse = setProtectionPlanInParent(cartId, cartRequestBean, baseUrl, token, newLineItemList);
				}
			}
		}
		return updatedCartResponse;
	}

	private String addBundleProductToCart(String cartId, JsonObject productJsonObject, String token, String baseUrl,
			UpdateCartServiceBean upaCartServiceBean, CartRequestBean cartRequestBean, String getCartResponse) {
		JsonArray attributes = productJsonObject.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		JsonArray valueJsonArray = new JsonArray();
		for(int i=0;i<attributes.size();i++) {
			String name = attributes.get(i).getAsJsonObject().get("name").getAsString();
			if("product-list".equals(name)) {
				valueJsonArray = attributes.get(i).getAsJsonObject().get(VALUE).getAsJsonArray();
			}
		}
		Map<String,Integer> bundleProductMap = new HashMap<>();
		for(int i=0;i<valueJsonArray.size();i++) {
			JsonArray productAttributesArray = valueJsonArray.get(i).getAsJsonObject().get("obj").getAsJsonObject().get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
			String productId = null;
			Integer quantity = null ;
			for(int j=0;j<productAttributesArray.size();j++) {
				String name = productAttributesArray.get(j).getAsJsonObject().get("name").getAsString();
				if("bundleProduct".equals(name)) {
					productId= productAttributesArray.get(j).getAsJsonObject().get(VALUE).getAsJsonObject().get("id").getAsString();
				} else if("bundleQuantity".equals(name)) {
					quantity = productAttributesArray.get(j).getAsJsonObject().get(VALUE).getAsInt();
				}
			}
			bundleProductMap.put(productId, quantity);
		}
		List<UpdateCartServiceBean.Action> actionList = new ArrayList<>();
		bundleProductMap.entrySet().stream().forEach(bundle->{
			String productId = bundle.getKey();
			createCartAction(cartRequestBean, actionList, productId,bundle);
			boolean isSubscriptionProduct = checkSubscriptionProduct(productId,token);
			if(isSubscriptionProduct) {
				addSubscriptionToCart(cartId, cartRequestBean, baseUrl, token, productId,getCartResponse);
			}
		});
		upaCartServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
		upaCartServiceBean.setActions(actionList);
		return (String) netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
	}

	private void createCartAction(CartRequestBean cartRequestBean, List<UpdateCartServiceBean.Action> actionList,
			String productId, Entry<String, Integer> bundle) {
		UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
		action.setActionName(ADD_LINE_ITEM);
		action.setProductId(productId);
		action.setVariantId(cartRequestBean.getVariantId());
		action.setQuantity(bundle.getValue());
		actionList.add(action);
	}

	private String setProtectionPlanInParent(String cartId, CartRequestBean cartRequestBean, String baseUrl, String token,
			List<String> newLineItemList) {
		String protectionLineItemId = newLineItemList.get(0);
		String updatedCartResponse;
		ProtectionPlanServiceBean protectionPlanServiceBean = new ProtectionPlanServiceBean();
		protectionPlanServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
		List<ProtectionPlanServiceBean.Action> actions = new ArrayList<>();
		ProtectionPlanServiceBean.Action lineItemCustom = new ProtectionPlanServiceBean.Action();
		lineItemCustom.setActionName("setLineItemCustomType");
		lineItemCustom.setLineItemId(cartRequestBean.getLineItemId());
		ProtectionPlanServiceBean.Action.Type type = new ProtectionPlanServiceBean.Action.Type();
		type.setTypeId("type");
		type.setId(ctEnvProperties.getLineItemTypeId());
		lineItemCustom.setType(type);
		ProtectionPlanServiceBean.Action.Fields fields = new ProtectionPlanServiceBean.Action.Fields();
		fields.setProtectionPlanLineItem(protectionLineItemId);
		lineItemCustom.setFields(fields);
		actions.add(lineItemCustom);
		protectionPlanServiceBean.setActions(actions);
		updatedCartResponse =(String) netConnectionHelper.sendPostRequest(baseUrl, token, protectionPlanServiceBean);
		return updatedCartResponse;
	}

	private List<String> getLineItemIdList(String cartResponse) {
		List<String> lineItemIdList = new ArrayList<>();
		JsonObject cartjsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonArray lineItemArray = cartjsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i=0;i<lineItemArray.size();i++) {
			lineItemIdList.add(lineItemArray.get(i).getAsJsonObject().get("id").getAsString());
		}
		return lineItemIdList;
	}
	private boolean checkSubscriptionProduct(String productId, String token) {
		String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + PRODUCTS+ productId + "?expand=masterData.current.masterVariant.attributes[*].value.typeId";
		String productResponse = netConnectionHelper.sendGetWithoutBody(token, productUrl);
		JsonObject productJsonObject = MolekuleUtility.parseJsonObject(productResponse);
		JsonArray attributes = productJsonObject.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		if(attributes == null || attributes.size() <= 0) {
			return false;
		}
		for(int i=0;i<attributes.size();i++) {
			String name = attributes.get(i).getAsJsonObject().get("name").getAsString();
			if("SubscriptionIdentifier".equals(name)) {
				return true;
			}
		}
		return false;
	}

	private String addSubscriptionToCart(String cartId, CartRequestBean updateCartRequestBean, String baseUrl,
			String token, String productId, String getCartResponse) {
		String currency = CountryCodeEnum.valueOf(updateCartRequestBean.getCountry()).getCurrency();
		List<String> productList = new ArrayList<>();
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(getCartResponse);
		JsonArray lineItemArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i=0;i<lineItemArray.size();i++) {
			productList.add(lineItemArray.get(i).getAsJsonObject().get(PRODUCT_ID).getAsString());
		}
		UpdateCartServiceBean upaCartServiceBean;
		String updatedCartResponse = getCartResponse;
		String subscriptionProductId = null;
		String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + PRODUCTS+ productId + "?expand=masterData.current.masterVariant.attributes[*].value.typeId";
		String productResponse = netConnectionHelper.sendGetWithoutBody(token, productUrl);
		subscriptionProductId = CartServiceUtility.getSubscriptionProductId(productResponse);
		if(!productList.contains(subscriptionProductId)) {
			CartRequestBean subCartRequestBean = new CartRequestBean();
			subCartRequestBean.setAction(updateCartRequestBean.getAction());
			subCartRequestBean.setProductId(subscriptionProductId);
			long variantId = updateCartRequestBean.getVariantId()!=0?updateCartRequestBean.getVariantId():1;
			subCartRequestBean.setVariantId(variantId);
			UpdateCartServiceBean subscriptionBean = CartServiceUtility.getSubscriptionRequestBean(subCartRequestBean,
					getCartCurrentVersionByCartId(cartId), variantId, currency);
			updatedCartResponse =(String) netConnectionHelper.sendPostRequest(baseUrl, token, subscriptionBean);
			JsonObject cartJson = MolekuleUtility.parseJsonObject(updatedCartResponse);
			String subcriptionLineItemId = CartServiceUtility.getSubLineItemId(cartJson, subscriptionProductId);
			upaCartServiceBean = CartServiceUtility.getSubCustomTypeAction(getCartCurrentVersionByCartId(cartId),
					subcriptionLineItemId, ctEnvProperties.getLineItemTypeId());
			updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token,
					upaCartServiceBean);
		}
		return updatedCartResponse;
	}
	private String addMasterLineItem(String cartId, UpdateCartServiceBean upaCartServiceBean, String token, CartRequestBean cartRequestBean, String baseUrl) {
		String productId = cartRequestBean.getProductId()!= null?cartRequestBean.getProductId():cartRequestBean.getVariantSKU();
		String updatedCartResponse = null;
		Map<String,JsonObject> productMap = new HashMap<>();
		List<String> productList = new ArrayList<>();
		String cartResponse = getCartById(cartId);
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		
		String channel = cartJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(CHANNEL).getAsString();
		
		JsonArray cartLineItemArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		for(int i =0;i<cartLineItemArray.size();i++) {
			productList.add(cartLineItemArray.get(i).getAsJsonObject().get("productId").getAsString());
			productMap.put(cartLineItemArray.get(i).getAsJsonObject().get("productId").getAsString(),cartLineItemArray.get(i).getAsJsonObject());
		}
		if(productList.contains(productId) && (productMap.get(productId).has(CUSTOM) && 
				productMap.get(productId).get(CUSTOM).getAsJsonObject().has(FIELDS) &&
				productMap.get(productId).get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(PROTECTION_PLAN_LINE_ITEM))) {
			//update protection parent product quantity
			String lineItemId = productMap.get(productId).get("id").getAsString();
			int quantity = productMap.get(productId).get("quantity").getAsInt() + cartRequestBean.getQuantity();
			upaCartServiceBean.setVersion(cartJsonObject.get(VERSION).getAsLong());
			List<UpdateCartServiceBean.Action> actionList = new ArrayList<>();
			UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
			action.setActionName(CHANGE_LINE_ITEM_QUANTITY);
			action.setLineItemId(lineItemId);
			action.setQuantity(quantity);
			actionList.add(action);
			upaCartServiceBean.setActions(actionList);
			updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
		} else {
			// add line item
			List<UpdateCartServiceBean.Action> actionList = new ArrayList<>();
			UpdateCartServiceBean.Action.DistributionChannel distributionChannel = new UpdateCartServiceBean.Action.DistributionChannel();
			upaCartServiceBean.setVersion(cartJsonObject.get(VERSION).getAsLong());
			UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
			action.setActionName(ADD_LINE_ITEM);
			if(cartRequestBean.getProductId()!= null) {
				action.setProductId(productId);
			} else {
				action.setSku(productId);
			}
			action.setVariantId(cartRequestBean.getVariantId());
			action.setQuantity(cartRequestBean.getQuantity());
			if(StringUtils.hasText(channel) && B2B.equals(channel)) {
				distributionChannel.setId(ctEnvProperties.getDistributionChannelB2BId());				
			} else {
				distributionChannel.setId(ctEnvProperties.getDistributionChannelB2CId());
			}
			distributionChannel.setTypeId(CHANNEL);
			action.setDistributionChannel(distributionChannel);
			actionList.add(action);
			upaCartServiceBean.setActions(actionList);
			updatedCartResponse =(String) netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
		}
		return updatedCartResponse;

	}
	private String calculateHandlingCharge(String cartId, String baseUrl, String token,
			UpdateCartServiceBean upaCartServiceBean, String updatedCartResponse, String channel, String currency, String country) {
		// calculateHandlingCharge
		long totalHandlingCharge = 0;
		if(B2B.equals(channel)) {
			totalHandlingCharge = handlingCostHelperService.calculateHandlingCostForParcelAndFreight(cartId,token);
			addCustomLineItemToCart(cartId, upaCartServiceBean, totalHandlingCharge,updatedCartResponse,currency);
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
		}
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		boolean isShippingAddressAvailable = jsonObject.has(SHIPPING_ADDRESS);
		SetTaxAmount setTaxAmount = null;
		if (isShippingAddressAvailable) {
			if (D2C.equals(channel) || !getTaxExempt(token, jsonObject)) {
				if("US".equals(country)){
					Object taxResponse = calculateTax(updatedCartResponse);
					if(taxResponse instanceof String) {
						return (String) taxResponse;
					}
					setTaxAmount = (SetTaxAmount) taxResponse;
				}
			} else {
				setTaxAmount= CartServiceUtility.updateNoTaxToCart(updatedCartResponse,currency);
			}
			if(null != setTaxAmount) {
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, setTaxAmount);				
			}
		}
		updatedCartResponse = updateCustomField(updatedCartResponse,cartId,baseUrl,token);
		updatedCartResponse = removePromoCodeOnCart(updatedCartResponse,token,currency);
		return updatedCartResponse;
	}
	private void setUpMolekuleCarrierAvailable(List<CarrierOptionRequestBean.Action> carrierCustomActions) {
		CarrierOptionRequestBean.Action molekuleCarrierAction = new CarrierOptionRequestBean.Action();
		molekuleCarrierAction.setActionName(SET_CUSTOM_FIELD);
		molekuleCarrierAction.setName(MOLEKULE_CARRIER);
		molekuleCarrierAction.setValue(null);
		carrierCustomActions.add(molekuleCarrierAction);
	}
	private Map<String, Object> getOwnCarrierJSON(CarrierOptionCustomObject carrierOptionCustomObject,
			AdditionalFreightInfo additionalFreightInfo) {
		Map<String, Object> ownCarrierJson = new HashMap<>();
		ownCarrierJson.put("carrierId", carrierOptionCustomObject.getCarrierId());
		ownCarrierJson.put("carrierName", carrierOptionCustomObject.getCarrierName());
		ownCarrierJson.put("deliveryType", carrierOptionCustomObject.getType().name());
		ownCarrierJson.put("carrierAccountNumber", carrierOptionCustomObject.getCarrierAccountNumber());
		if(additionalFreightInfo != null) {
			ownCarrierJson.put(LOADING_DOCK, additionalFreightInfo.isLoadingDock());
			ownCarrierJson.put(FORK_LIFT, additionalFreightInfo.isForkLift());
			ownCarrierJson.put(DELIVERY_APPOINTMENT_REQUIRED,additionalFreightInfo.isDeliveryAppointmentRequired());
			ownCarrierJson.put(RECEIVING_HOURS, additionalFreightInfo.getReceivingHours());
			ownCarrierJson.put("instructions", additionalFreightInfo.getInstructions());
		}else {
			ownCarrierJson.put(LOADING_DOCK, carrierOptionCustomObject.isLoadingDock());
			ownCarrierJson.put(FORK_LIFT, carrierOptionCustomObject.isForkLift());
			ownCarrierJson.put(DELIVERY_APPOINTMENT_REQUIRED,carrierOptionCustomObject.isDeliveryAppointmentRequired());
			ownCarrierJson.put(RECEIVING_HOURS, carrierOptionCustomObject.getReceivingHours());
			ownCarrierJson.put("instructions", carrierOptionCustomObject.getInstructions());
		}

		return ownCarrierJson;
	}
	private AdditionalFreightInfo setAdditionalFreightInfo(AccountCustomObject accountCustomObject,
			String shippingAddressId) {
		List<AdditionalFreightInfo> additionalFreightInfos = accountCustomObject.getValue()
				.getAdditionalFreightInfos();
		AdditionalFreightInfo additionalFreightInfo = null;
		if(additionalFreightInfos != null && shippingAddressId != null) {
			Iterator<AdditionalFreightInfo> addIterator = additionalFreightInfos.iterator();
			while (addIterator.hasNext()) {
				additionalFreightInfo = addIterator.next();
				if (shippingAddressId.equals(additionalFreightInfo.getShippingAddressId())) {
					break;
				}
				additionalFreightInfo = null;
			}
		}
		return additionalFreightInfo;
	}

	private String setQuoteAsCartValue(String cartId, String baseUrl, String token,
			QuoteCustomerDetailServiceBean quoteCustomerDetailServiceBean, String channel) {
		String updatedCartResponse;
		assignCartOrQuote(cartId, baseUrl, token,false);
		quoteCustomerDetailServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
		List<QuoteCustomerDetailServiceBean.Action> actionList = new ArrayList<>();
		// remove first name 
		actionList.add(setQuoteCustomerBeanAction(EMPTY ,FIRST_NAME));
		// remove last name
		actionList.add(setQuoteCustomerBeanAction(EMPTY,LAST_NAME));
		// remove email
		actionList.add(setQuoteCustomerBeanAction(EMPTY,EMAIL));
		// remove phone
		actionList.add(setQuoteCustomerBeanAction(EMPTY,PHONE));
		// remove comments
		actionList.add(setQuoteCustomerBeanAction(EMPTY,"comments"));
		//add handling cost
		long totalHandlingCharge = 0;
		if(B2B.equals(channel)) {
			totalHandlingCharge = handlingCostHelperService.calculateHandlingCostForParcelAndFreight(cartId,token);
		}
		actionList.add(setQuoteCustomerBeanAction(totalHandlingCharge * 100 ,HANDLING_COST));
		quoteCustomerDetailServiceBean.setActions(actionList);		
		updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, quoteCustomerDetailServiceBean);
		return updatedCartResponse;
	}
	private String setPONumber(String cartId, CartRequestBean cartRequestBean, String baseUrl, String token,
			Long version, UpdateCartServiceBean upaCartServiceBean) {
		String updatedCartResponse;
		//SET_PO_NUMBER action is to set PONumber for the cart object by cartId
		updatedCartResponse = getCartById(cartId);
		JsonObject jsonCartObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		//Add new poNumber custom if there is no other fields
		if(!jsonCartObject.has(CUSTOM)) {
			upaCartServiceBean.setVersion(version);
			UpdateCartServiceBean.Action action = new UpdateCartServiceBean.Action();
			action.setActionName(SET_CUSTOM_TYPE);
			UpdateCartServiceBean.Action.Type updateCartServiceType =  new UpdateCartServiceBean.Action.Type();
			updateCartServiceType.setId(ctEnvProperties.getOrderTypeId());
			updateCartServiceType.setTypeId("type");
			action.setType(updateCartServiceType);
			UpdateCartServiceBean.Action.Fields fields =  new UpdateCartServiceBean.Action.Fields();
			action.setFields(fields);
			List<UpdateCartServiceBean.Action> actions = new ArrayList<>();
			actions.add(action);
			upaCartServiceBean.setActions(actions);
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
		}else {
			//Add poNumber custom fields to already existing fields
			updatedCartResponse = updatePoNumberCustomFieldIncart(baseUrl,token,cartRequestBean.getPoNumber(),"poNumber",version);
		}
		return updatedCartResponse;
	}
	private String setPeriodForFilter(String cartId, CartRequestBean cartRequestBean, String baseUrl, String token,
			Long version) {
		String updatedCartResponse;
		//SET_PERIOD action is to set period for the cart object by cartId
		updatedCartResponse = getCartById(cartId);
		JsonObject jsonCartObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		UpdateCartFilterServiceBean upaCartServiceBean = new UpdateCartFilterServiceBean();
		if(!jsonCartObject.has(CUSTOM)) {
			upaCartServiceBean.setVersion(version);
			UpdateCartFilterServiceBean.Action action = new UpdateCartFilterServiceBean.Action();
			action.setActionName(SET_CUSTOM_TYPE);
			UpdateCartFilterServiceBean.Action.Type updateCartServiceType =  new UpdateCartFilterServiceBean.Action.Type();
			updateCartServiceType.setId(ctEnvProperties.getOrderTypeId());
			updateCartServiceType.setTypeId("type");
			action.setType(updateCartServiceType);
			UpdateCartFilterServiceBean.Action.Fields fields =  new UpdateCartFilterServiceBean.Action.Fields();
			fields.setPeriod(cartRequestBean.getPeriod());			
			action.setFields(fields);
			List<UpdateCartFilterServiceBean.Action> actions = new ArrayList<>();
			actions.add(action);
			upaCartServiceBean.setActions(actions);
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);
		}else {
			//Add period custom fields to already existing fields
			List<UpdateCartFilterServiceBean.Action> updateActionList = new ArrayList<>();
			UpdateCartFilterServiceBean.Action action = new UpdateCartFilterServiceBean.Action();
			action.setActionName(SET_CUSTOM_FIELD);
			action.setName("period");
			action.setValue(cartRequestBean.getPeriod());
			updateActionList.add(action);
			upaCartServiceBean.setActions(updateActionList);
			upaCartServiceBean.setVersion(version);
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, upaCartServiceBean);

		}
		return updatedCartResponse;
	}

	/**
	 * setQuoteCustomerBeanAction method is used to set the action for QuoteCustomerDetailServiceBean.
	 * @param value
	 * @param fieldName
	 * @return action
	 */
	private QuoteCustomerDetailServiceBean.Action setQuoteCustomerBeanAction(Object value,String fieldName ) {
		QuoteCustomerDetailServiceBean.Action action = new QuoteCustomerDetailServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(fieldName);
		action.setValue(String.valueOf(value));
		return action;
	}
	/**
	 * setShippingAddress method is used to set the shipping address value in cart object.
	 * @param upaCartServiceBean
	 * @param version
	 * @param cartRequestBean
	 * @param cartId
	 * @param baseUrl
	 * @param token
	 * @param channel 
	 * @return String
	 */
	private String setShippingAddress(UpdateCartServiceBean upaCartServiceBean, Long version, CartRequestBean cartRequestBean, 
			String cartId, String baseUrl, String token, String channel) {
		String currency = CountryCodeEnum.valueOf(cartRequestBean.getCountry()).getCurrency();
		String updatedCartResponse = null;
		SetShippingAddressRequestBean setShippingAddressRequestBean= new SetShippingAddressRequestBean();
		setShippingAddressRequestBean.setVersion(version);
		com.molekule.api.v1.commonframework.dto.cart.Action action = new com.molekule.api.v1.commonframework.dto.cart.Action();
		action.setShippingAddress(cartRequestBean.getShippingAddress());
		action.setAction("setShippingAddress");
		List<com.molekule.api.v1.commonframework.dto.cart.Action> actions = new ArrayList<>();
		if (cartRequestBean.isGift()) {
			String cartResponse = getCartById(cartId);
			JsonObject cartJson = MolekuleUtility.parseJsonObject(cartResponse);
			List<com.molekule.api.v1.commonframework.dto.cart.Action> customActions = setGiftCustomAttributes(
					cartRequestBean, String.valueOf(cartRequestBean.isGift()), 
					cartRequestBean.getRecipientsEmail(), cartRequestBean.getGiftMessage(),cartJson);
			actions.addAll(customActions);
		} else if(!cartRequestBean.isGift()){
			String cartResponse = getCartById(cartId);
			JsonObject cartJson = MolekuleUtility.parseJsonObject(cartResponse);
			boolean hasCustom = cartJson.has(CUSTOM);
			if(hasCustom) {
				JsonObject fieldsJson = cartJson.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
				if(fieldsJson.has("gift")) {
					List<com.molekule.api.v1.commonframework.dto.cart.Action> customActions = setGiftCustomAttributes(
							cartRequestBean,null, null, null,cartJson);
					actions.addAll(customActions);
					setShippingAddressRequestBean.setVersion(cartJson.get(VERSION).getAsLong());
				}

			}

		}
		actions.add(action);
		setShippingAddressRequestBean.setActions(actions);
		try {
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, setShippingAddressRequestBean);
		}catch(Exception ex) {
			logger.error("Error occured when set Shipping Addresses -", ex);
			ErrorResponse errorResponse = new ErrorResponse(400,"Unable to set shipping address to cart.");
			return errorResponse.toString();
		}
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		if(jsonObject.get(STATUS_CODE) != null) {
			return updatedCartResponse;
		}
		JsonArray jsonArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
		SetTaxAmount setTaxAmount = null;
		if (jsonArray.size() > 0) {
			if (D2C.equals(channel) || !getTaxExempt(token, jsonObject)) {
				if("US".equals(cartRequestBean.getCountry())){
					Object taxResponse = calculateTax(updatedCartResponse);
					if(taxResponse instanceof String) {
						return (String) taxResponse;
					}
					setTaxAmount = (SetTaxAmount) taxResponse;
				}		
			} else {
				setTaxAmount= CartServiceUtility.updateNoTaxToCart(updatedCartResponse,currency);
				if (setTaxAmount == null) {
					ErrorResponse errorResponse = new ErrorResponse(400, "Unable to calculate the tax.");
					return errorResponse.toString();
				}
			}
			if("US".equals(cartRequestBean.getCountry())){
			try {
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(baseUrl, token, setTaxAmount);
			}catch(Exception ex) {
				logger.error("Error occured when update cart response" , ex);
				ErrorResponse errorResponse = new ErrorResponse(400,"Unable to update tax into cart.");
				return errorResponse.toString();
			}
			}
		}
		try {
			updatedCartResponse = updateCustomField(updatedCartResponse, cartId, baseUrl, token);
		} catch (Exception ex) {
			logger.error("Error occured when updateCustomField", ex);
			ErrorResponse errorResponse = new ErrorResponse(400, "Unable to update order total into cart.");
			return errorResponse.toString();
		}
		return updatedCartResponse;
	}

	private List<com.molekule.api.v1.commonframework.dto.cart.Action> setGiftCustomAttributes(
			CartRequestBean cartRequestBean, String gift, String recipientsEmail, String giftMessage,JsonObject cartResponse) {
		List<com.molekule.api.v1.commonframework.dto.cart.Action> customActions = new ArrayList<>();
		if("false".equals(gift)) {
			customActions.add(setCustomFields("gift", null));JsonObject fieldsJsonObject = cartResponse.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(fieldsJsonObject.has("recipientsEmail")) {
				customActions.add(setCustomFields("recipientsEmail", null));
			}
			if(fieldsJsonObject.has("giftMessage")) {
				customActions.add(setCustomFields("giftMessage", null));
			}
		} else {
			customActions.add(setCustomFields("gift", Boolean.parseBoolean(gift)));			
		}
		if(null != recipientsEmail || gift == null) {
			customActions.add(setCustomFields("recipientsEmail", recipientsEmail));
		}
		if(null != giftMessage || gift == null) {
			customActions.add(setCustomFields("giftMessage", giftMessage));			
		}
		return customActions;
	}

	private com.molekule.api.v1.commonframework.dto.cart.Action setCustomFields(String name, Object value) {
		com.molekule.api.v1.commonframework.dto.cart.Action action = new com.molekule.api.v1.commonframework.dto.cart.Action();
		action.setAction("setCustomField");
		action.setName(name);
		action.setValue(value);
		return action;
	}

	private com.molekule.api.v1.commonframework.dto.cart.CarrierOptionRequestBean.Action setCarrierCustomAction(String name, String value) {
		CarrierOptionRequestBean.Action shippingOptionAction = new CarrierOptionRequestBean.Action();
		shippingOptionAction.setActionName(SET_CUSTOM_FIELD);
		shippingOptionAction.setName(name);
		shippingOptionAction.setValue(value);
		return shippingOptionAction;
	}

	private Long getOldFreeShippingDiscount(JsonObject cartJsonObj) {
		Long freeShippingDiscount = 0L;
		if (cartJsonObj.has(CUSTOM)) {
			JsonObject fieldObjectJson = cartJsonObj.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(fieldObjectJson.has(FREE_SHIPPING_DISCOUNT)) {
				freeShippingDiscount = fieldObjectJson.get(FREE_SHIPPING_DISCOUNT).getAsLong();
			}
		}
		return freeShippingDiscount;
	}

	private void addSubscriptionProduct(JsonObject guestLineItemObject, String token, String customerCartId, String cartBaseUrl, List<String> guestProductList, String currency) {
		UpdateCartServiceBean updateCartServiceBean = null;
		String subscriptionProductId = null;
		String productId = guestLineItemObject.get(PRODUCT_ID).getAsString();
		String productBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_PRODUCTS;
		// Get Subscription By ProductId
		String getSubIdUrl = productBaseUrl + "/" + productId
				+ SUB_URL;
		String subProductResponse = netConnectionHelper.sendGetWithoutBody(token, getSubIdUrl);
		if (subProductResponse != null) {
			subscriptionProductId = CartServiceUtility.getSubscriptionProductId(subProductResponse);
			if (subscriptionProductId != null) {
				boolean isSubscriptionProduct = checkIsSubscriptionProductOrNot(productBaseUrl,
						subscriptionProductId, token);
				if (isSubscriptionProduct && guestProductList.contains(subscriptionProductId)) {
					CartRequestBean subCartRequestBean = new CartRequestBean();
					subCartRequestBean.setAction(CartServiceUtility.CartActions.ADD_LINE_ITEM.name());
					subCartRequestBean.setProductId(subscriptionProductId);
					subCartRequestBean.setVariantId(guestLineItemObject.get(VARIANT).getAsJsonObject().get("id").getAsLong());
					String subcriptionLineItemId = productId.equals(subscriptionProductId)?guestLineItemObject.get("id").getAsString():null;
					if(subcriptionLineItemId ==null) {
						updateCartServiceBean = CartServiceUtility.getSubscriptionRequestBean(
								subCartRequestBean, getCartCurrentVersionByCartId(customerCartId),
								guestLineItemObject.get(VARIANT).getAsJsonObject().get("id").getAsLong(), currency);
						String updatedCartResponse = (String)netConnectionHelper.sendPostRequest(cartBaseUrl, token,
								updateCartServiceBean);
						JsonObject cartJson = MolekuleUtility.parseJsonObject(updatedCartResponse);
						subcriptionLineItemId = CartServiceUtility.getSubLineItemId(cartJson,
								subscriptionProductId);
						updateCartServiceBean = CartServiceUtility.getSubCustomTypeAction(
								getCartCurrentVersionByCartId(customerCartId), subcriptionLineItemId,
								ctEnvProperties.getLineItemTypeId());
						netConnectionHelper.sendPostRequest(cartBaseUrl, token,
								updateCartServiceBean);
					}
				}
			}
		}
	}
	/**
	 * removePromoCodeOnCart method is used to remove promo code from cart.
	 * 
	 * @param updatedCartResponse
	 * @param token 
	 * @param currency 
	 * @return String
	 */
	private String removePromoCodeOnCart(String updatedCartResponse, String token, String currency) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		if (jsonObject.get(STATUS_CODE) != null) {
			return updatedCartResponse;
		}
		String cartId = jsonObject.get("id").getAsString();
		String url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId;
		//		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();

		if (jsonObject.has(CUSTOM)) {
			JsonObject customObject = jsonObject.get(CUSTOM).getAsJsonObject();
			JsonObject fieldsJsonObject = customObject.get(FIELDS).getAsJsonObject();
			if(fieldsJsonObject.has(DISCOUNT_MESSAGE)) {
				updatedCartResponse = setDiscountMessage(jsonObject, url, token, DISCOUNT_MESSAGE, "");				
			}
			jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
			if (jsonObject.get(STATUS_CODE) != null) {
				return updatedCartResponse;
			}
		}
		updatedCartResponse = updateCartResponse(updatedCartResponse, jsonObject, cartId, url, token,currency);
		return updatedCartResponse;
	}
	private String updateCartResponse(String updatedCartResponse, JsonObject jsonObject, String cartId, String url,
			String token, String currency) {
		long cartVersion = jsonObject.get(VERSION).getAsLong();
		if(jsonObject.has(DISCOUNT_CODES)) {
			JsonArray discountCodesJsonArray = jsonObject.get(DISCOUNT_CODES).getAsJsonArray();
			if(discountCodesJsonArray.size() > 0) {
				for(int i= 0; i < discountCodesJsonArray.size(); i++) {
					JsonObject discountCodesJsonObject = discountCodesJsonArray.get(i).getAsJsonObject();

					DiscountCodeRequestBean disCodeRequestBean = new DiscountCodeRequestBean();
					disCodeRequestBean.setVersion(cartVersion);
					DiscountCodeRequestBean.Action action = new DiscountCodeRequestBean.Action();
					action.setActionName("removeDiscountCode");
					DiscountCodeRequestBean.Action.DiscountCode discountCode = new DiscountCodeRequestBean.Action.DiscountCode();
					discountCode.setTypeId("discount-code");
					discountCode.setId(discountCodesJsonObject.get(DISCOUNT_CODE).getAsJsonObject().get("id").getAsString());
					action.setDiscountCode(discountCode);
					List<DiscountCodeRequestBean.Action> actions = new ArrayList<>();
					actions.add(action);
					disCodeRequestBean.setActions(actions);
					updatedCartResponse = calculationsAfterPromoCode(url, token, disCodeRequestBean, cartId,currency);
					jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
					if(jsonObject.get(STATUS_CODE) == null) {
						cartVersion = jsonObject.get(VERSION).getAsLong();
					}
				}
			}
		}
		return updatedCartResponse;
	}

	private String setDiscountMessage(JsonObject jsonObject, String url, String token, String name, String value) {
		Long version = jsonObject.get(VERSION).getAsLong();
		UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
		List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
		UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(name);
		action.setValue(value);
		updateActionList.add(action);
		updateCartCustomServiceBean.setActions(updateActionList);
		updateCartCustomServiceBean.setVersion(version);
		return (String) netConnectionHelper.sendPostRequest(url, token, updateCartCustomServiceBean);
	}

	/**
	 * calculationsAfterPromoCode method is common method to do calculations after remove promo code.
	 *  
	 * @param url
	 * @param token
	 * @param disCodeRequestBean
	 * @param cartId
	 * @param currency 
	 * @return String
	 */
	public String calculationsAfterPromoCode(String url, String token, DiscountCodeRequestBean disCodeRequestBean, String cartId, String currency){
		String response = (String) netConnectionHelper.sendPostRequest(url, token, disCodeRequestBean);
		long discountedAmount = getCustomDiscountValue(response, "removePromo");
		DiscountCodeRequestBean discountCodeRequestBean = new DiscountCodeRequestBean();
		List<DiscountCodeRequestBean.Action> customActions = new ArrayList<>();
		long currentVersion = getCartCurrentVersionByCartId(cartId);
		discountCodeRequestBean.setVersion(currentVersion);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		boolean isJsonObject = jsonObject.has(CUSTOM);
		if(isJsonObject) {
			DiscountCodeRequestBean.Action customAction = new DiscountCodeRequestBean.Action();
			customAction.setActionName(SET_CUSTOM_FIELD);
			customAction.setName(TOTAL_DISCOUNT_PRICE);
			customAction.setValue(String.valueOf(discountedAmount));
			customActions.add(customAction);
		}else{
			DiscountCodeRequestBean.Action customAction = new DiscountCodeRequestBean.Action();
			DiscountCodeRequestBean.Action.Type discountCodeRequestBeanActionType = new DiscountCodeRequestBean.Action.Type();
			customAction.setActionName("setCustomType");
			discountCodeRequestBeanActionType.setId(ctEnvProperties.getOrderTypeId());
			discountCodeRequestBeanActionType.setTypeId("type");
			customAction.setType(discountCodeRequestBeanActionType);
			DiscountCodeRequestBean.Action.Fields fields = new DiscountCodeRequestBean.Action.Fields();
			fields.setTotalDiscountPrice(String.valueOf(discountedAmount));
			customAction.setFields(fields);
			customActions.add(customAction);
		}
		discountCodeRequestBean.setActions(customActions);
		response = (String) netConnectionHelper.sendPostRequest(url, token, discountCodeRequestBean);
		jsonObject = MolekuleUtility.parseJsonObject(response);
		boolean isShippingAddressAvailable = jsonObject.has(SHIPPING_ADDRESS);
		if (isShippingAddressAvailable) {
			Object taxResponse = calculateTax(response);
			if(taxResponse instanceof String) {
				return (String) taxResponse;
			}
			SetTaxAmount setTaxAmount = (SetTaxAmount) taxResponse;
			response = (String) netConnectionHelper.sendPostRequest(url, token, setTaxAmount);
		}
		response = updateCustomField(response, cartId, url, token);
		if (response != null) {
			jsonObject = MolekuleUtility.parseJsonObject(response);
			if (jsonObject.get(STATUS_CODE) == null && jsonObject.has(SHIPPING_INFO)) {
				// Added the shipping method tax
				UpdateCartServiceBean serviceBean = CartServiceUtility.getShippingMethodTaxRequestBean(jsonObject, currency);
				response = (String) netConnectionHelper.sendPostRequest(url, token, serviceBean);
			}
		}
		return response;
	}

	/**
	 * getCustomDiscountValue method is used to calculate discount amount.
	 * 
	 * @param response
	 * @param promoType
	 * @return long
	 */
	public long getCustomDiscountValue(String response, String promoType) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		long discountedAmount = 0;
		JsonArray lineItemArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
		for (int i = 0; i < lineItemArray.size(); i++) {
			JsonObject lineItemObj = lineItemArray.get(i).getAsJsonObject();
			boolean isDiscountedPrice = lineItemObj.has(DISCOUNTED_PRICE);
			if (isDiscountedPrice) {
				JsonArray discountedPricePerQuantityArray = lineItemObj.get(DISCOUNTED_PRICE_PER_QUANTITY).getAsJsonArray();
				for (int z = 0; z < discountedPricePerQuantityArray.size(); z++) {
					JsonObject discountedPricePerQuantityJsonObject = discountedPricePerQuantityArray.get(z).getAsJsonObject();
					Integer quantity = discountedPricePerQuantityJsonObject.get(QUANTITY).getAsInt();
					JsonArray includedDiscountsArray = discountedPricePerQuantityJsonObject.get(DISCOUNTED_PRICE).getAsJsonObject().get(INCLUDED_DISCOUNTS).getAsJsonArray();
					for (int j = 0; j < includedDiscountsArray.size(); j++) {
						JsonObject includedDiscountsJson = includedDiscountsArray.get(j).getAsJsonObject();
						long lineItemDiscount = includedDiscountsJson.get(DISCOUNTED_AMOUNT).getAsJsonObject().get(CENT_AMOUNT).getAsLong();
						long totalDiscountLineItems = lineItemDiscount * quantity;
						discountedAmount = discountedAmount + totalDiscountLineItems;
					}
				}
			}
		}

		if (promoType.equals("addPromo")) {
			long freeshippingDiscount = 0;
			if (jsonObject.has(CUSTOM) && jsonObject.get(CUSTOM).getAsJsonObject().has(FIELDS) && jsonObject
					.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(FREE_SHIPPING_DISCOUNT)) {
				freeshippingDiscount = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
						.get(FREE_SHIPPING_DISCOUNT).getAsLong();
				discountedAmount = freeshippingDiscount + discountedAmount;
			}
		}
		return discountedAmount;
	}

	/**
	 * doCartMergeOperation method is used to do the merge cart operation.
	 * @param guestCartId
	 * @param customerId
	 * @param token
	 * @param updateCartServiceBean
	 * @param updatedCartResponse
	 * @param channel 
	 * @param cartRequestBean 
	 * @return String
	 */
	private String doCartMergeOperation(String guestCartId, String customerId, String token, UpdateCartServiceBean updateCartServiceBean, String updatedCartResponse, String channel, CartRequestBean cartRequestBean) {
		String currency = CountryCodeEnum.valueOf(cartRequestBean.getCountry()).getCurrency();
		// guest cart response by guest cart id
		String guestCartResponse = getCartById(guestCartId);
		JsonObject guestCartJsonObject = MolekuleUtility.parseJsonObject(guestCartResponse);
		JsonArray guestLineItemArray = guestCartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		// get CustomercartDetails based on customer id
		JsonObject customerCartJsonObject = null;
		JsonObject activeCustomerCartObject = null;
		String customerCartId =null;
		//		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/carts?where=customerId=\"" + customerId + "\"" ;
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
				+ "?where=customerId=\"" + customerId + "\""
				+ "&where=custom(fields(parentOrderReferenceId is not defined))"
				+ "&where=custom(fields(Quote is not defined))" + "&where=cartState=\"Active\"";
		String customerCartResponse = netConnectionHelper.sendGetWithoutBody(token, baseUrl);
		customerCartJsonObject = MolekuleUtility.parseJsonObject(customerCartResponse);
		if(customerCartJsonObject.get("count").getAsInt() == 0) {
			// create cart
			CreateCartRequestBean createCartRequestBean = new CreateCartRequestBean();
			createCartRequestBean.setCustomerId(customerId);
			createCartRequestBean.setCountry(cartRequestBean.getCountry());
			customerCartResponse = createCart(createCartRequestBean);
			customerCartJsonObject = MolekuleUtility.parseJsonObject(customerCartResponse);
			activeCustomerCartObject = customerCartJsonObject;
			customerCartId = activeCustomerCartObject.has("id") ? activeCustomerCartObject.get("id").getAsString() : null;
		} else {
			JsonArray resultArray = customerCartJsonObject.get(RESULTS).getAsJsonArray();
			activeCustomerCartObject = getActiveCartResponse(resultArray);
			customerCartId = activeCustomerCartObject.has("id") ? activeCustomerCartObject.get("id").getAsString() : null;
		}
		String cartBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + customerCartId;
		JsonArray customerlineItemsArray = activeCustomerCartObject.get(LINE_ITEMS).getAsJsonArray();
		if(guestLineItemArray.size() > 0 && customerlineItemsArray.size() > 0) {
			updatedCartResponse = joinCartOperation(token,guestLineItemArray, customerCartId, cartBaseUrl, customerlineItemsArray,currency);
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
			updatedCartResponse = setChannelToCart(jsonObject,channel, customerCartId, baseUrl, token, updatedCartResponse);
			updatedCartResponse = doCustomFieldAndCustomLineItemUpdation(token, updatedCartResponse, customerCartId,
					cartBaseUrl, channel,currency);
		} else if(guestLineItemArray.size() > 0) {
			List<String> guestProductList = new ArrayList<>();
			for(int guestlineItemCount=0;guestlineItemCount<guestLineItemArray.size();guestlineItemCount++) {
				guestProductList.add(guestLineItemArray.get(guestlineItemCount).getAsJsonObject().get(PRODUCT_ID).getAsString());
			}
			for(int guestlineItemCount=0;guestlineItemCount<guestLineItemArray.size();guestlineItemCount++) {
				updateCartServiceBean.setVersion(getCartCurrentVersionByCartId(customerCartId));
				UpdateCartServiceBean.Action updateAction = new UpdateCartServiceBean.Action();
				updateAction.setActionName(ADD_LINE_ITEM);
				updateAction.setProductId(guestLineItemArray.get(guestlineItemCount).getAsJsonObject().get(PRODUCT_ID).getAsString());
				updateAction.setQuantity(guestLineItemArray.get(guestlineItemCount).getAsJsonObject().get(QUANTITY).getAsInt());
				updateAction.setVariantId(guestLineItemArray.get(guestlineItemCount).getAsJsonObject().get(VARIANT).getAsJsonObject().get("id").getAsInt());
				ArrayList<Action> actions = new ArrayList<>();
				actions.add(updateAction);
				updateCartServiceBean.setActions(actions);
				updatedCartResponse = (String) netConnectionHelper.sendPostRequest(cartBaseUrl, token, updateCartServiceBean);
				addSubscriptionProduct(guestLineItemArray.get(guestlineItemCount).getAsJsonObject(),token,customerCartId,cartBaseUrl,guestProductList,currency);
			}
			JsonObject jsonObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
			updatedCartResponse = setChannelToCart(jsonObject,channel, customerCartId, baseUrl, token, updatedCartResponse);
			updatedCartResponse = doCustomFieldAndCustomLineItemUpdation(token, updatedCartResponse, customerCartId,
					cartBaseUrl, channel,currency);
		} else {
			updatedCartResponse = getCartById(customerCartId);
		}
		// Delete guest cart
		long version = getCartCurrentVersionByCartId(guestCartId);
		String cartDeleteBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/carts/" + guestCartId +"?version=" + version;
		netConnectionHelper.deleteRequest(cartDeleteBaseUrl, token);
		return updatedCartResponse;
	}

	/**
	 * createCart method is used to create a by customer id.
	 * 
	 * @param createCartRequestBean
	 * @return String
	 */
	public String createCart(CreateCartRequestBean createCartRequestBean) {
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/carts";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		CreateCartDTO cartDto = new CreateCartDTO();
		String storeId = ctEnvProperties.getUsStoreId();
		CustomerStoreEnum customerStoreEnum = CustomerStoreEnum.values()[0];
		if (createCartRequestBean.getCustomerId() != null) {
			CustomerResponseBean customerResponseBean = null;
			try {
				customerResponseBean = registrationHelperService.getCustomerById(createCartRequestBean.getCustomerId());
			} catch (Exception ex) {
				logger.error("Error occured when createCart ", ex);
				ErrorResponse errorResponse = new ErrorResponse(400,
						"Unable to find customer group for given customer id");
				return errorResponse.toString();
			}
			cartDto.setCustomerEmail(customerResponseBean.getEmail());
			cartDto.setCustomerId(createCartRequestBean.getCustomerId());
			CreateCartDTO.Custom.Type createCartDTOCustomType = new CreateCartDTO.Custom.Type();
			createCartDTOCustomType.setKey(cartTypeKey);
			CreateCartDTO.Custom custom = new CreateCartDTO.Custom();
			custom.setType(createCartDTOCustomType);
			if(null!=customerResponseBean.getCustom() && null!=customerResponseBean.getCustom().getFields().getAccountId()) {
				CreateCartDTO.Custom.Fields fields = new CreateCartDTO.Custom.Fields();
				fields.setAccountId(customerResponseBean.getCustom().getFields().getAccountId());
				custom.setFields(fields);
			}
			cartDto.setCustom(custom);

			if("US".equals(createCartRequestBean.getCountry()) || createCartRequestBean.getCountry() == null){
				customerStoreEnum = CustomerStoreEnum.values()[0];
				storeId = ctEnvProperties.getUsStoreId();
			} else if("CA".equals(createCartRequestBean.getCountry())){
				customerStoreEnum = CustomerStoreEnum.values()[0];
				storeId = ctEnvProperties.getCaStoreId();
			} else if("GB".equals(createCartRequestBean.getCountry())){
				customerStoreEnum = CustomerStoreEnum.values()[1];
				storeId = ctEnvProperties.getUkStoreId();
			}else {
				customerStoreEnum = CustomerStoreEnum.values()[2];
				storeId = ctEnvProperties.getEuStoreId();
			}
		} else {
			String anonymousId = MolekuleUtility.createUuidValue();
			cartDto.setAnonymousId(anonymousId);
			/*
			 * CreateCartDTO.CustomerGroup customerGroupObj = new
			 * CreateCartDTO.CustomerGroup();
			 * customerGroupObj.setId(ctEnvProperties.getGuestCustomerGroupId());
			 * customerGroupObj.setName("Any"); cartDto.setCustomerGroup(customerGroupObj);
			 */
		}
		cartDto.setTaxMode(ctEnvProperties.getTaxMode());
		CreateCartDTO.Store store = new CreateCartDTO.Store();
		//if(StringUtils.hasText(createCartRequestBean.getStore())) {
		if("US".equals(createCartRequestBean.getCountry()) || createCartRequestBean.getCountry() == null){
			customerStoreEnum = CustomerStoreEnum.values()[0];
			storeId = ctEnvProperties.getUsStoreId();
		}else if("CA".equals(createCartRequestBean.getCountry())){
			customerStoreEnum = CustomerStoreEnum.values()[0];
			storeId = ctEnvProperties.getCaStoreId();
		} else if("GB".equals(createCartRequestBean.getCountry())){
			customerStoreEnum = CustomerStoreEnum.values()[1];
			storeId = ctEnvProperties.getUkStoreId();
		}else {
			customerStoreEnum = CustomerStoreEnum.values()[2];
			storeId = ctEnvProperties.getEuStoreId();
		}
		//}
		store.setId(storeId);
		cartDto.setStore(store);
		cartDto.setCurrency(customerStoreEnum.getCurrency());
		cartDto.setCountry(customerStoreEnum.getCountryCode());
		String response = null;
		try {
			response = (String) netConnectionHelper.sendPostRequest(baseUrl, token, cartDto);
		} catch (Exception e) {
			logger.error("Error -occured when creating a cart", e);
			ErrorResponse errorResponse = new ErrorResponse(400, "Unable to create cart");
			return errorResponse.toString();
		}
		return response;
	}
	/**
	 * doCustomFieldAndCustomLineItemUpdation method is used to update the custom field and custom line item.
	 * @param token
	 * @param updatedCartResponse
	 * @param customerCartId
	 * @param cartBaseUrl
	 * @param channel 
	 * @param currency 
	 * @return String
	 */
	private String doCustomFieldAndCustomLineItemUpdation(String token, String updatedCartResponse,
			String customerCartId, String cartBaseUrl, String channel, String currency) {
		// guest custom line item addition in customer cart and calculating new handling cost for merged cart
		long totalHandlingCharge = 0;
		UpdateCartServiceBean updateCustomLineItemServiceBean = new UpdateCartServiceBean();
		if(B2B.equals(channel)) {
			totalHandlingCharge = handlingCostHelperService.calculateHandlingCostForParcelAndFreight(customerCartId,token);
			addCustomLineItemToCart(customerCartId, updateCustomLineItemServiceBean, totalHandlingCharge, updatedCartResponse,currency);
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(cartBaseUrl, token, updateCustomLineItemServiceBean);
		}
		// update guest cart custom fields with customer cart custom fields
		updatedCartResponse = updateCustomField(updatedCartResponse,customerCartId,cartBaseUrl,token);
		return updatedCartResponse;
	}

	/**
	 * joinCartOperation method is used to join the guest and customer cart details
	 * @param token
	 * @param guestLineItemArray
	 * @param customerCartId
	 * @param cartBaseUrl
	 * @param customerlineItemsArray
	 * @param currency 
	 * @return String
	 */
	private String joinCartOperation(String token, JsonArray guestLineItemArray, String customerCartId, String cartBaseUrl,
			JsonArray customerlineItemsArray, String currency) {
		UpdateCartServiceBean updateCartServiceBean = new UpdateCartServiceBean();
		Map<String,JsonObject> guestproductMap = new HashMap<>();
		JsonArray updatedCustomerLineItemArray = new JsonArray();
		List<String>guestProductList = new ArrayList<>();

		Map<String, String> values = new HashMap<>();
		values.put(TOKEN, token);
		values.put(CUSTOMER_CART_ID, customerCartId);
		values.put(CART_BASE_URL, cartBaseUrl);
		values.put("currency", currency);

		getGuestProductDetails(values, guestLineItemArray, customerlineItemsArray,
				guestproductMap, updatedCustomerLineItemArray, guestProductList);
		guestproductMap.entrySet().stream().forEach(guestcart ->
		guestCartProcessDetails(values, customerlineItemsArray, updateCartServiceBean,
				guestproductMap, updatedCustomerLineItemArray, guestProductList, guestcart)
				);
		return getCartById(customerCartId);
	}
	private void guestCartProcessDetails(Map<String, String> values,
			JsonArray customerlineItemsArray, UpdateCartServiceBean updateCartServiceBean,
			Map<String, JsonObject> guestproductMap, JsonArray updatedCustomerLineItemArray,
			List<String> guestProductList, Entry<String, JsonObject> guestcart) {

		String token = values.get(TOKEN);
		String customerCartId = values.get(CUSTOMER_CART_ID);
		String cartBaseUrl = values.get(CART_BASE_URL);
		String currency = values.get("currency");

		boolean commonItemFlag = false;
		if(customerlineItemsArray.size() == 0) {
			addLineItemToMergeCart(token, customerCartId, cartBaseUrl, updateCartServiceBean, guestcart);
		} else {
			if(guestproductMap.size()>1) {
				for(int customerLineItemCount=0;customerLineItemCount<updatedCustomerLineItemArray.size();customerLineItemCount++) {
					if(updatedCustomerLineItemArray.get(customerLineItemCount).getAsJsonObject().get(PRODUCT_ID).getAsString().equals(guestcart.getKey())) {
						commonItemFlag = changeLineItemQuantity(token, customerCartId, cartBaseUrl,updateCartServiceBean, updatedCustomerLineItemArray,
								guestcart,customerLineItemCount);
					}
				}
				if(!commonItemFlag) {
					addLineItemToMergeCart(token, customerCartId, cartBaseUrl, updateCartServiceBean, guestcart);
				}
			} else {
				addLineItemToMergeCart(token, customerCartId, cartBaseUrl, updateCartServiceBean, guestcart);
			}
		}
		addSubscriptionProduct(guestcart.getValue(),token,customerCartId,cartBaseUrl,guestProductList,currency);
	}

	/**
	 * getGuestProductDetails method is used to get the guest product map, guest product list and customer product list.
	 * @param token
	 * @param guestLineItemArray
	 * @param customerCartId
	 * @param cartBaseUrl
	 * @param customerlineItemsArray
	 * @param guestproductMap
	 * @param updatedCustomerLineItemArray
	 * @param guestProductList
	 */
	private void getGuestProductDetails(Map<String, String> values, JsonArray guestLineItemArray, JsonArray customerlineItemsArray, Map<String, JsonObject> guestproductMap,
			JsonArray updatedCustomerLineItemArray, List<String> guestProductList) {
		for(int i=0;i<guestLineItemArray.size();i++) {
			processGuestLitemArray(guestLineItemArray, customerlineItemsArray, guestproductMap,
					updatedCustomerLineItemArray, guestProductList, values, i);
		}
	}
	private void processGuestLitemArray(JsonArray guestLineItemArray, JsonArray customerlineItemsArray,
			Map<String, JsonObject> guestproductMap, JsonArray updatedCustomerLineItemArray,
			List<String> guestProductList, Map<String, String> values, int i) {

		String token = values.get(TOKEN);
		String customerCartId = values.get(CUSTOMER_CART_ID);
		String cartBaseUrl = values.get(CART_BASE_URL);
		String currency = values.get("currency");

		String subscriptionProductId = null;
		String productId = guestLineItemArray.get(i).getAsJsonObject().get(PRODUCT_ID).getAsString();
		String productBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_PRODUCTS;
		// Get Subscription By ProductId
		String getSubIdUrl = productBaseUrl + "/" + productId
				+ SUB_URL;
		String subProductResponse = netConnectionHelper.sendGetWithoutBody(token, getSubIdUrl);
		if (subProductResponse != null) {
			subscriptionProductId = CartServiceUtility.getSubscriptionProductId(subProductResponse);
			if(subscriptionProductId!=null) {
				guestProductList.add(productId);
				guestproductMap.put(productId, guestLineItemArray.get(i).getAsJsonObject());
				for(int customerLineItemCount=0;customerLineItemCount<customerlineItemsArray.size();customerLineItemCount++) {
					if(customerlineItemsArray.get(customerLineItemCount).getAsJsonObject().get(PRODUCT_ID).getAsString().equals(subscriptionProductId)) {
						handleSubscriptionInCustomerCart(token, customerCartId, cartBaseUrl, customerlineItemsArray,
								subscriptionProductId, customerLineItemCount,currency);
					}else {
						updatedCustomerLineItemArray.add(customerlineItemsArray.get(customerLineItemCount));						
					}
				}
			} else {
				guestProductList.add(productId);
				guestproductMap.put(productId, guestLineItemArray.get(i).getAsJsonObject());
			}	
		}
	}
	/**
	 * changeLineItemQuantity method is used to change the line item quantity in customer cart.
	 * @param token
	 * @param customerCartId
	 * @param cartBaseUrl
	 * @param updateCartServiceBean
	 * @param updatedCustomerLineItemArray
	 * @param guestcart
	 * @param customerLineItemCount
	 * @return boolean
	 */
	private boolean changeLineItemQuantity(String token, String customerCartId, String cartBaseUrl,
			UpdateCartServiceBean updateCartServiceBean, JsonArray updatedCustomerLineItemArray,
			Entry<String, JsonObject> guestcart, int customerLineItemCount) {
		boolean commonItemFlag;
		updateCartServiceBean.setVersion(getCartCurrentVersionByCartId(customerCartId));
		UpdateCartServiceBean.Action updateAction = new UpdateCartServiceBean.Action();
		updateAction.setActionName(CHANGE_LINE_ITEM_QUANTITY);
		updateAction.setLineItemId(updatedCustomerLineItemArray.get(customerLineItemCount).getAsJsonObject().get("id").getAsString());
		updateAction.setProductId(updatedCustomerLineItemArray.get(customerLineItemCount).getAsJsonObject().get(PRODUCT_ID).getAsString());
		int quantity = updatedCustomerLineItemArray.get(customerLineItemCount).getAsJsonObject().get(QUANTITY).getAsInt() + guestcart.getValue().get(QUANTITY).getAsInt();
		updateAction.setQuantity(quantity);
		updateAction.setVariantId(updatedCustomerLineItemArray.get(customerLineItemCount).getAsJsonObject().get(VARIANT).getAsJsonObject().get("id").getAsInt());
		ArrayList<Action> actions = new ArrayList<>();
		actions.add(updateAction);
		updateCartServiceBean.setActions(actions);
		netConnectionHelper.sendPostRequest(cartBaseUrl, token, updateCartServiceBean);
		updatedCustomerLineItemArray.remove(customerLineItemCount);
		commonItemFlag = true;
		return commonItemFlag;
	}
	/**
	 * handleSubscriptionInCustomerCart method is used to remove the subscription in customer cart.
	 * @param token
	 * @param customerCartId
	 * @param cartBaseUrl
	 * @param customerlineItemsArray
	 * @param subscriptionProductId
	 * @param customerLineItemCount
	 * @param currency 
	 */
	private void handleSubscriptionInCustomerCart(String token, String customerCartId, String cartBaseUrl,
			JsonArray customerlineItemsArray, String subscriptionProductId, int customerLineItemCount, String currency) {
		String subcriptionLineItemId = customerlineItemsArray.get(customerLineItemCount).getAsJsonObject().get("id").getAsString();
		CartRequestBean subCartRequestBean = new CartRequestBean();
		subCartRequestBean.setAction(CartServiceUtility.CartActions.REMOVE_LINE_ITEM.name());
		subCartRequestBean.setProductId(subscriptionProductId);
		subCartRequestBean.setVariantId(customerlineItemsArray.get(customerLineItemCount).getAsJsonObject().get(VARIANT).getAsJsonObject().get("id").getAsLong());
		subCartRequestBean.setLineItemId(subcriptionLineItemId);
		UpdateCartServiceBean upaCartServiceBean = CartServiceUtility.getSubscriptionRequestBean(
				subCartRequestBean, getCartCurrentVersionByCartId(customerCartId),
				customerlineItemsArray.get(customerLineItemCount).getAsJsonObject().get(VARIANT).getAsJsonObject().get("id").getAsLong(), currency);
		netConnectionHelper.sendPostRequest(cartBaseUrl, token,upaCartServiceBean);
	}
	/**
	 * addLineItemToMergeCart is used to add the new line item to the customer cart from guest cart.
	 * @param token
	 * @param customerCartId
	 * @param cartBaseUrl
	 * @param updateCartServiceBean
	 * @param guestcart
	 */
	private void addLineItemToMergeCart(String token, String customerCartId, String cartBaseUrl,
			UpdateCartServiceBean updateCartServiceBean, Entry<String, JsonObject> guestcart) {
		updateCartServiceBean.setVersion(getCartCurrentVersionByCartId(customerCartId));
		UpdateCartServiceBean.Action updateAction = new UpdateCartServiceBean.Action();
		updateAction.setActionName(ADD_LINE_ITEM);
		updateAction.setProductId(guestcart.getValue().get(PRODUCT_ID).getAsString());
		updateAction.setQuantity(guestcart.getValue().get(QUANTITY).getAsInt());
		updateAction.setVariantId(guestcart.getValue().get(VARIANT).getAsJsonObject().get("id").getAsInt());
		ArrayList<Action> actions = new ArrayList<>();
		actions.add(updateAction);
		updateCartServiceBean.setActions(actions);
		netConnectionHelper.sendPostRequest(cartBaseUrl, token, updateCartServiceBean);
	}
	/**
	 * getActiveCartResponse method is used to get the active cart from the customer id.
	 * @param resultArray
	 * @return JsonObject
	 */
	private JsonObject getActiveCartResponse(JsonArray resultArray) {
		for(int i=0;i<resultArray.size();i++) {
			if(resultArray.get(i).getAsJsonObject().get("cartState").getAsString().equals("Active")) {
				return resultArray.get(i).getAsJsonObject();
			}
		}
		return resultArray.get(0).getAsJsonObject();
	}

	/**
	 * getTaxExempt method is used to get getTaxExempt flag from account object using cart object
	 * 
	 * @param token
	 * @param jsonObject
	 * @return
	 */
	private boolean getTaxExempt(String token, JsonObject jsonObject) {
		boolean taxExemptFlag = false;
		String accountId = EMPTY;
		if(jsonObject.has(CUSTOMER_ID)) {
			String customerId = jsonObject.get(CUSTOMER_ID).getAsString();
			String customerUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
					+ customerId + "/?";
			String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerUrl);
			JsonObject jsonCustomerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(jsonCustomerObject.has(CUSTOM)) {
				accountId = jsonCustomerObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get("accountId").getAsString();
			}

			//get accountObject
			String accountBaseUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
					.append("/custom-objects?where=id=\"")
					.append(accountId).append("\"").toString();
			String accountResponse = netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
			JsonObject jsonAccountObject = MolekuleUtility.parseJsonObject(accountResponse);
			if(jsonAccountObject.has(RESULTS)) {
				JsonArray resultArray = jsonAccountObject.get(RESULTS).getAsJsonArray();
				for(int i=0; i<resultArray.size();i++) {
					JsonObject valueObject = resultArray.get(i).getAsJsonObject().get(VALUE).getAsJsonObject();
					if(valueObject.has("taxExemptFlag")) {
						taxExemptFlag = valueObject.get("taxExemptFlag").getAsBoolean();
					}
				}
			}
		}
		return taxExemptFlag;
	}
	private boolean checkIsSubscriptionProductOrNot(String productBaseUrl, String subscriptionProductId,String token) {
		String productTypeUrl = productBaseUrl+"/"+subscriptionProductId+"?expand=productType.id";
		String subProductTypeRes = netConnectionHelper.sendGetWithoutBody(token, productTypeUrl);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(subProductTypeRes);
		boolean hasProductType = jsonObject.has(PRODUCT_TYPE);
		if(!hasProductType) {
			return false;
		}
		boolean hasSubName = jsonObject.get(PRODUCT_TYPE).getAsJsonObject().get("obj").getAsJsonObject().has("name");
		if(!hasSubName) {
			return false;
		}
		String subNmae =jsonObject.get(PRODUCT_TYPE).getAsJsonObject().get("obj").getAsJsonObject().get("name").getAsString();
		return "subscription".equals(subNmae);

	}
	public Long getCartCurrentVersionByCartId(String cartId) {
		String cart = getCartObj(cartId);
		JsonObject jsonAccountObj = MolekuleUtility.parseJsonObject(cart);
		if (jsonAccountObj.get(STATUS_CODE) != null) {
			return 0l;
		}
		return jsonAccountObj.get(VERSION).getAsLong();
	}

	/**
	 * updateCustomField method is used to update the custom object fields in cart object
	 * @param updatedCartResponse
	 * @param cartId
	 * @param baseUrl
	 * @param token
	 * @return String - cart response
	 */
	public String updateCustomField(String updatedCartResponse, String cartId, String baseUrl, String token) {
		UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
		JsonObject jsonCartObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		if(jsonCartObject.has(CUSTOM_LINE_ITEMS) && jsonCartObject.get(LINE_ITEMS).getAsJsonArray().size()!=0) {
			updatedCartResponse = setCustomerLineItems(cartId, baseUrl, token, updateCartCustomServiceBean,
					jsonCartObject);
		} else {
			updateCartCustomServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			List<UpdateCartCustomServiceBean.Action> actionList = new ArrayList<>();
			// update handling cost if there is no line item in cart
			actionList.add(setCustomServiceBeanAction(0,HANDLING_COST));
			// update subtotal if there is no line item in cart
			actionList.add(setCustomServiceBeanAction(0,SUB_TOTAL));
			// update shipping cost if there is no line item in cart
			actionList.add(setCustomServiceBeanAction(0,"shippingCost"));
			// update order total if there is no line item in cart
			actionList.add(setCustomServiceBeanAction(0,ORDER_TOTAL));
			updateCartCustomServiceBean.setActions(actionList);			
			updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, updateCartCustomServiceBean);
		}
		return updatedCartResponse;
	}
	private String setCustomerLineItems(String cartId, String baseUrl, String token,
			UpdateCartCustomServiceBean updateCartCustomServiceBean, JsonObject jsonCartObject) {
		String updatedCartResponse;
		JsonArray jsonCustomLineItemArray = jsonCartObject.get(CUSTOM_LINE_ITEMS).getAsJsonArray();
		long handlingCost = 0;
		long totalTax =0;
		long freeShippingCost =0;
		long totaldiscountedPrice = 0;
		long promoDiscount = 0;
		handlingCost = getHandlingCost(jsonCustomLineItemArray, handlingCost);
		JsonObject fieldObjectJson = null;
		String channel = null;
		if(jsonCartObject.has(CUSTOM)) {
			fieldObjectJson = jsonCartObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			channel = fieldObjectJson.has(CHANNEL) ? fieldObjectJson.get(CHANNEL).getAsString() : EMPTY;
			totalTax = fieldObjectJson.has(TOTAL_TAX) ? fieldObjectJson.get(TOTAL_TAX).getAsLong() : 0;
			freeShippingCost = fieldObjectJson.has(FREE_SHIPPING_DISCOUNT) ? fieldObjectJson.get(FREE_SHIPPING_DISCOUNT).getAsLong() : 0;
			if(fieldObjectJson.has(TOTAL_DISCOUNT_PRICE)&&channel.equals("B2B")) {
				totaldiscountedPrice = fieldObjectJson.get(TOTAL_DISCOUNT_PRICE).getAsLong();
			} else if(channel.equals("D2C")) {
				totaldiscountedPrice = getCustomDiscountValue(String.valueOf(jsonCartObject), "");
			} else {
				totaldiscountedPrice = 0;
			}
			promoDiscount = totaldiscountedPrice-freeShippingCost;
		} else {
			totaldiscountedPrice = getCustomDiscountValue(String.valueOf(jsonCartObject), "");
			promoDiscount = totaldiscountedPrice-freeShippingCost;
		}
		long subTotal = jsonCartObject.get(TOTAL_PRICE).getAsJsonObject().get(CENT_AMOUNT).getAsLong();
		long shippingCost = 0;
		if(jsonCartObject.has(SHIPPING_INFO)) {
			shippingCost = jsonCartObject.get(SHIPPING_INFO).getAsJsonObject().get("price").getAsJsonObject().get(CENT_AMOUNT).getAsLong();
		}
		if(freeShippingCost == 0) {
			subTotal = (subTotal +promoDiscount)-(handlingCost + shippingCost);
		} else {
			subTotal = (subTotal +promoDiscount)-(handlingCost);
		}
		long orderTotal = (handlingCost + subTotal + shippingCost + totalTax)-totaldiscountedPrice;

		if(jsonCartObject.has(ANONYMOUSID)&&(channel!=null)&&channel.equals("B2B")) {
			orderTotal = subTotal;
		}

		if(!jsonCartObject.has(CUSTOM)) {
			List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
			UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
			UpdateCartCustomServiceBean.Action.Type updateCartCustomServiceType = new UpdateCartCustomServiceBean.Action.Type();
			UpdateCartCustomServiceBean.Action.Fields field = new UpdateCartCustomServiceBean.Action.Fields();
			action.setActionName(SET_CUSTOM_TYPE);
			updateCartCustomServiceType.setId(ctEnvProperties.getOrderTypeId());
			updateCartCustomServiceType.setTypeId("type");
			field.setHandlingCost(String.valueOf(handlingCost));
			field.setSubTotal(String.valueOf(subTotal));
			field.setShippingCost(String.valueOf(shippingCost));
			field.setOrderTotal(String.valueOf(orderTotal));
			field.setTotalDiscountPrice(String.valueOf(totaldiscountedPrice));
			action.setType(updateCartCustomServiceType);
			action.setFields(field);
			updateActionList.add(action);
			updateCartCustomServiceBean.setActions(updateActionList);
			updateCartCustomServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, updateCartCustomServiceBean);
		} else {
			Map<String, String> values = new HashMap<>();
			values.put("cartId", cartId);
			values.put("baseUrl", baseUrl);
			values.put("token", token);
			updatedCartResponse = setJsonCustomCartObject(values, updateCartCustomServiceBean,
					handlingCost, subTotal, shippingCost, orderTotal, totaldiscountedPrice);
		}
		return updatedCartResponse;
	}
	private long getHandlingCost(JsonArray jsonCustomLineItemArray, long handlingCost) {
		for(int i=0;i<jsonCustomLineItemArray.size();i++) {
			if(jsonCustomLineItemArray.get(i).getAsJsonObject().get("slug").getAsString().equals(handlingCostSlugName)) {
				handlingCost = jsonCustomLineItemArray.get(i).getAsJsonObject().get(MONEY).getAsJsonObject().get(CENT_AMOUNT).getAsLong();
			}
		}
		return handlingCost;
	}
	private String setJsonCustomCartObject(Map<String, String> values,
			UpdateCartCustomServiceBean updateCartCustomServiceBean, long handlingCost, long subTotal,
			long shippingCost, long orderTotal, long totaldiscountedPrice) {

		String cartId = values.get("cartId");
		String baseUrl =  values.get("baseUrl");
		String token = values.get("token");

		String updatedCartResponse;
		updateCartCustomServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
		List<UpdateCartCustomServiceBean.Action> actionList = new ArrayList<>();
		// update handling cost
		actionList.add(setCustomServiceBeanAction(handlingCost,HANDLING_COST));
		// update subtotal
		actionList.add(setCustomServiceBeanAction(subTotal,SUB_TOTAL));
		// update shipping cost
		actionList.add(setCustomServiceBeanAction(shippingCost,"shippingCost"));
		// update order total
		actionList.add(setCustomServiceBeanAction(orderTotal,ORDER_TOTAL));
		// update Total Discount price
		actionList.add(setCustomServiceBeanAction(totaldiscountedPrice,TOTAL_DISCOUNT_PRICE));
		updateCartCustomServiceBean.setActions(actionList);			
		updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, updateCartCustomServiceBean);
		return updatedCartResponse;
	}

	/**
	 * setCustomServiceBeanAction method is used to set the action for UpdateCartCustomServiceBean.
	 * @param value
	 * @param fieldName
	 * @return action
	 */
	private UpdateCartCustomServiceBean.Action setCustomServiceBeanAction(long value,String fieldName ) {
		UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(fieldName);
		action.setValue(String.valueOf(Long.parseLong(String.valueOf(value))));
		return action;
	}

	/**
	 * addCustomLineItemToCart method is used to add the custom line item to cart object.
	 * @param cartId
	 * @param upaCartServiceBean
	 * @param totalHandlingCharge
	 * @param updatedCartResponse
	 * @param currency 
	 */
	private void addCustomLineItemToCart(String cartId, UpdateCartServiceBean upaCartServiceBean,
			long totalHandlingCharge, String updatedCartResponse, String currency) {
		JsonObject jsonCartObj = MolekuleUtility.parseJsonObject(updatedCartResponse);
		List<Action> updateActionList = new ArrayList<>();
		if(jsonCartObj.has(CUSTOM_LINE_ITEMS)) {
			JsonArray customLineItemJsonArray = jsonCartObj.get(CUSTOM_LINE_ITEMS).getAsJsonArray();
			boolean checkHandlingCostFlag = checkForHandlingCostSlugName(customLineItemJsonArray);
			if(!checkHandlingCostFlag) {
				Action action = new Action();
				action.setActionName("addCustomLineItem");
				Name name = new Name();
				name.setEn(handlingCostName);
				Money money = new Money();
				money.setCurrencyCode(currency);
				money.setCentAmount(totalHandlingCharge * 100);
				action.setSlug(handlingCostSlugName);
				action.setName(name);
				action.setMoney(money);
				action.setQuantity(1);
				updateActionList.add(action);
				upaCartServiceBean.setActions(updateActionList);
				upaCartServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			} else {
				String customLineItemId = null;
				for(int i=0;i<customLineItemJsonArray.size();i++) {
					if(customLineItemJsonArray.get(i).getAsJsonObject().get("slug").getAsString().equals(handlingCostSlugName)) {
						customLineItemId = customLineItemJsonArray.get(i).getAsJsonObject().get("id").getAsString();
					}
				}
				Action action = new Action();
				action.setActionName("changeCustomLineItemMoney");
				action.setCustomLineItemId(customLineItemId);
				Money money = new Money();
				money.setCurrencyCode(currency);
				money.setCentAmount(totalHandlingCharge * 100);
				action.setMoney(money);
				updateActionList.add(action);
				upaCartServiceBean.setActions(updateActionList);
				upaCartServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			}
		}
	}

	/**
	 * checkForHandlingCostSlugName method is used to check for the handling cost line item in custom line object of cart.
	 * @param customLineItemJsonArray
	 * @return boolean
	 */
	private Boolean checkForHandlingCostSlugName(JsonArray customLineItemJsonArray) {
		for(int i=0;i<customLineItemJsonArray.size();i++) {
			if(customLineItemJsonArray.get(i).getAsJsonObject().get("slug").getAsString().equals(handlingCostSlugName)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * getCartById method is used to get the cart information by passing cartId.
	 * 
	 * @param cartId
	 * @return String
	 */
	public String getCartObj(String cartId) {
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append(CART).append(cartId).toString();
		return netConnectionHelper.sendGetWithoutBody(token, url);
	}

	/**
	 * calculateTax method will connect to tax service for tax calculation
	 * 
	 * @param response
	 * @return String
	 */
	public Object calculateTax(String response) {
		String taxResponse = null;
		try {
			AvalaraRequestBean avalaraRequestBean = CartServiceUtility.getAvalaraRequestBean(response);
			avalaraRequestBean.setCompanyCode(companyCode);
			avalaraRequestBean.setType(type);
			taxResponse = taxService.calculateTaxByAvalara(avalaraRequestBean);
			JsonObject avalaraJsonObject = MolekuleUtility.parseJsonObject(taxResponse);
			if(avalaraJsonObject.has("error")) {
				return taxResponse;
			}
		}catch (Exception ex) {
			logger.error("Error occured wheno Calculate the tax.Got", ex);
			ErrorResponse errorResponse = new ErrorResponse(400,
					"Unable to Calculate the tax.Got Exception-" + ex.getMessage());
			return errorResponse.toString();
		}
		return CartServiceUtility.updateTaxToCart(taxResponse, response,false);
	}
	
	public String getCartById(String cartId) {
		return getCartById(cartId, null);
	}

	/**
	 * getCartById method is used to get the cart information by passing cartId.
	 * 
	 * @param cartId
	 * @return String
	 */
	public String getCartById(String cartId,String ctToken) {
		String token = null;
		if(ctToken==null) {
			token  = ctServerHelperService.getStringAccessToken();
		}else {
			token = ctToken;
		}				
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append(CART).append(cartId)
				.append("?expand=lineItems[*].variant.attributes[*].value.typeId&expand=lineItems.discountedPrice&expand=lineItems[*].productType.typeId&where=\"")
				.append("custom(fields(parentOrderReferenceId is not defined))\"")
				.append("&where=\"")
				.append("custom(fields(Quote is not defined))\"")
				.append("&expand=lineItems[*].discountedPricePerQuantity[*].discountedPrice.includedDiscounts[*].discount.id")
				.append("&expand=discountCodes[*].discountCode.typeId")
				.append("&expand=custom.fields.promotionProduct.id").toString();
		return netConnectionHelper.sendGetWithoutBody(token, url);
	}
	/**
	 * updatePoNumberCustomFieldIncart method is used to update the custom fields in cart object.
	 * @param baseUrl
	 * @param token
	 * @param poNumber
	 * @param fieldName
	 * @param version
	 * @return String - the cart object
	 */
	private String updatePoNumberCustomFieldIncart(String baseUrl,
			String token, String poNumber, String fieldName ,long version) {

		UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
		List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
		UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(fieldName);
		action.setValue(poNumber);
		updateActionList.add(action);
		updateCartCustomServiceBean.setActions(updateActionList);
		updateCartCustomServiceBean.setVersion(version);
		return (String)netConnectionHelper.sendPostRequest(baseUrl, token, updateCartCustomServiceBean);
	}

	/**
	 * setCustomerForQuote methos is used to set the quote customer details in cart object.
	 * @param cartId
	 * @param baseUrl
	 * @param token
	 * @param cartRequestBean
	 * @param quoteCustomerDetailServiceBean 
	 * @return String
	 */
	private String setCustomerForQuote(String cartId, String baseUrl, String token,
			CartRequestBean cartRequestBean, QuoteCustomerDetailServiceBean quoteCustomerDetailServiceBean) {
		String updatedCartResponse;
		updatedCartResponse = getCartById(cartId);
		JsonObject jsonCartObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		QuoteCustomerDetail quoteCustomerDetail = cartRequestBean.getQuoteCustomerDetail();
		String firstname = quoteCustomerDetail.getFirstName();
		String lastName = quoteCustomerDetail.getLastName();
		String email = quoteCustomerDetail.getEmail();
		String phone = quoteCustomerDetail.getPhone();
		String comments = Objects.nonNull(quoteCustomerDetail.getComments()) ? quoteCustomerDetail.getComments() : EMPTY;
		if(!jsonCartObject.has(CUSTOM)) {
			List<QuoteCustomerDetailServiceBean.Action> updateActionList = new ArrayList<>();
			QuoteCustomerDetailServiceBean.Action action = new QuoteCustomerDetailServiceBean.Action();
			QuoteCustomerDetailServiceBean.Action.Type quoteCartCustomServiceType = new QuoteCustomerDetailServiceBean.Action.Type();
			QuoteCustomerDetailServiceBean.Action.Fields field = new QuoteCustomerDetailServiceBean.Action.Fields();
			action.setActionName(SET_CUSTOM_TYPE);
			quoteCartCustomServiceType.setId(ctEnvProperties.getOrderTypeId());
			quoteCartCustomServiceType.setTypeId("type");
			field.setFirstName(firstname);
			field.setLastName(lastName);
			field.setEmail(email);
			field.setPhone(phone);
			field.setComments(comments);
			action.setType(quoteCartCustomServiceType);
			action.setFields(field);
			updateActionList.add(action);
			quoteCustomerDetailServiceBean.setActions(updateActionList);
			quoteCustomerDetailServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, quoteCustomerDetailServiceBean);
		} else {
			List<QuoteCustomerDetailServiceBean.Action> actionList = new ArrayList<>();
			// updating first name
			actionList.add(setQuoteCustomerBeanAction(firstname ,FIRST_NAME));
			// updating last name
			actionList.add(setQuoteCustomerBeanAction(lastName,LAST_NAME));
			// updating email
			actionList.add(setQuoteCustomerBeanAction(email,EMAIL));
			// updating phone
			actionList.add(setQuoteCustomerBeanAction(phone,PHONE));
			if(jsonCartObject.has(CUSTOMER_ID)) {
				String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
						+ jsonCartObject.get(CUSTOMER_ID).getAsString();
				String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
				JsonObject jsonObject = MolekuleUtility.parseJsonObject(customerResponse);
				if(jsonObject.has("companyName")) {
					String companyName = jsonObject.get("companyName").getAsString();
					// updating company
					actionList.add(setQuoteCustomerBeanAction(companyName,"company"));
				}
			}
			//update Comments
			actionList.add(setQuoteCustomerBeanAction(comments,"comments"));
			quoteCustomerDetailServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			quoteCustomerDetailServiceBean.setActions(actionList);
			updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, quoteCustomerDetailServiceBean);
		}

		jsonCartObject = MolekuleUtility.parseJsonObject(updatedCartResponse);

		SendGridModel sendGridModel = new SendGridModel();
		if(jsonCartObject.has(ANONYMOUSID) && jsonCartObject.has(CUSTOM)) {
			JsonObject responseFieldObject = jsonCartObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			sendGridModel.setQuoteRequestNumber(responseFieldObject.get("quoteNumber").getAsString());
			sendGridModel.setCartId(jsonCartObject.get("id").getAsString());
			sendGridModel.setEmail(responseFieldObject.get("email").getAsString());
			sendGridModel.setFirstName(responseFieldObject.get(FIRST_NAME).getAsString());
			sendGridModel.setLastName(responseFieldObject.get("lastName").getAsString());
			sendGridModel.setPhoneNumber(responseFieldObject.get(PHONE).getAsString());
			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			sendGridModel.setRequestedDate(formatter.format(date));
			String ctCategoryResponse = auroraHelperService.getCompanyCategories();
			List<CategoryDto> categoryDtoList = MolekuleUtility.getCategoryListDto(ctCategoryResponse);
			List<CategoryDto> othersCategoryList = categoryDtoList.stream().filter(filter->filter.getName().equalsIgnoreCase("Other")).collect(Collectors.toList());
			String salesRepId = auroraHelperService.getSalesRepValueFromCategoryList(categoryDtoList,othersCategoryList.get(0).getId());
			String ctSalesRepResponse = auroraHelperService.getListOfSalesRep();
			List<SalesRepresentativeDto> salesRepresentativeDtoList = MolekuleUtility.getSalesRepresentativeDtoList(ctSalesRepResponse);
			SalesRepresentativeDto salesRespresentativeDto = auroraHelperService.getSalesRepDetails(salesRepresentativeDtoList,salesRepId);
			sendGridModel.setSalesRepFirstName(salesRespresentativeDto.getFirstName());
			sendGridModel.setSalesRepLastName(salesRespresentativeDto.getLastName());
			sendGridModel.setSalesRepEmail(salesRespresentativeDto.getEmail());
			sendGridModel.setSalesRepPhoneNumber(salesRespresentativeDto.getPhone());
			sendGridModel.setSalesRepCalendyLink(salesRespresentativeDto.getCalendlyLink());
			quoteRequestConfirmationMailSender.sendMail(sendGridModel);
			quoteRequestPendingApprovalMailSender.sendMail(sendGridModel);
		}
		return updatedCartResponse;
	}
	/**
	 * assignCartOrQuote method is used to set the Quote value in the custom field.
	 * @param cartId
	 * @param baseUrl
	 * @param token
	 * @param quoteValue
	 * @return String
	 */
	private String assignCartOrQuote(String cartId, String baseUrl, String token, boolean quoteValue) {
		String updatedCartResponse;
		QuoteCustomFieldCartServiceBean quoteCustomFieldCartServiceBean = new QuoteCustomFieldCartServiceBean();
		updatedCartResponse = getCartById(cartId);
		JsonObject jsonCartObject = MolekuleUtility.parseJsonObject(updatedCartResponse);
		if(!jsonCartObject.has(CUSTOM)) {
			List<QuoteCustomFieldCartServiceBean.Action> updateActionList = new ArrayList<>();
			QuoteCustomFieldCartServiceBean.Action action = new QuoteCustomFieldCartServiceBean.Action();
			QuoteCustomFieldCartServiceBean.Action.Type quoteCartCustomServiceType = new QuoteCustomFieldCartServiceBean.Action.Type();
			QuoteCustomFieldCartServiceBean.Action.Fields field = new QuoteCustomFieldCartServiceBean.Action.Fields();
			action.setActionName(SET_CUSTOM_TYPE);
			quoteCartCustomServiceType.setId(ctEnvProperties.getOrderTypeId());
			quoteCartCustomServiceType.setTypeId("type");
			field.setQuote(quoteValue);
			action.setType(quoteCartCustomServiceType);
			action.setFields(field);
			updateActionList.add(action);
			quoteCustomFieldCartServiceBean.setActions(updateActionList);
			quoteCustomFieldCartServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			updatedCartResponse = (String)netConnectionHelper.sendPostRequest(baseUrl, token, quoteCustomFieldCartServiceBean);
		} else {
			List<QuoteCustomFieldCartServiceBean.Action> updateActionList = new ArrayList<>();
			QuoteCustomFieldCartServiceBean.Action action = new QuoteCustomFieldCartServiceBean.Action();
			action.setActionName(SET_CUSTOM_FIELD);
			action.setName(QUOTE);
			action.setValue(quoteValue);
			updateActionList.add(action);

			QuoteCustomFieldCartServiceBean.Action quoteNumberAction = new QuoteCustomFieldCartServiceBean.Action();
			String quoteNumber = CartServiceUtility.createQuoteNumber();
			quoteNumberAction.setActionName(SET_CUSTOM_FIELD);
			quoteNumberAction.setName("quoteNumber");
			quoteNumberAction.setValue(quoteNumber);
			updateActionList.add(quoteNumberAction);

			quoteCustomFieldCartServiceBean.setActions(updateActionList);
			quoteCustomFieldCartServiceBean.setVersion(getCartCurrentVersionByCartId(cartId));
			updatedCartResponse =  (String)netConnectionHelper.sendPostRequest(baseUrl, token, quoteCustomFieldCartServiceBean);
		}
		return updatedCartResponse;
	}

	/**
	 * getExpandCartDataById method is used to get expanded cart data on lineItems ProductType.
	 * 
	 * @param cartId
	 * @return String
	 */
	public String getExpandCartDataById(String cartId) {
		String token = ctServerHelperService.getStringAccessToken();
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/carts/").append(cartId)
				.append("?expand=lineItems[*].productType.id").toString();
		return netConnectionHelper.sendGetWithoutBody(token, url);
	}

	/**
	 * getDeliveryType method is used to get deliveryType.
	 * 
	 * @param lineItemsArray
	 * @param value
	 * @return String
	 */
	public Map<String, String> getDeliveryType(JsonArray lineItemsArray, ShippingTableValue value) {
		Map<String, String> map = new HashMap<>();
		int deviceCount = 0;
		int filterCount = 0;
		int quantity = 0;
		Integer rxPalletCount = 0;
		Boolean parcelFlag = Boolean.TRUE;
		Boolean rxProduct = Boolean.FALSE;
		for (int i = 0; i < lineItemsArray.size(); i++) {
			String key = lineItemsArray.get(i).getAsJsonObject().get(PRODUCT_TYPE).getAsJsonObject().get("obj")
					.getAsJsonObject().get("key").getAsString();
			if (key.equals("purifier")) {
				quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
				deviceCount = deviceCount + quantity;
			} else if (key.equals("filter")) {
				quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
				filterCount = filterCount + quantity;
			}
			if (!key.equals("subscription") 
					&& lineItemsArray.get(i).getAsJsonObject().get(VARIANT).getAsJsonObject().get("sku").getAsString()
					.equals(ctEnvProperties.getMolekuleAirProSku())) {
				parcelFlag = Boolean.FALSE;
				rxProduct = Boolean.TRUE;
				rxPalletCount = rxPalletCount + quantity;
			}
		}
		int freightThresholdDevice = value.getFreight().getThreshold().getDevice();
		int freightThresholdFilter = value.getFreight().getThreshold().getFilter();
		Boolean mixedCart = (deviceCount != 0 && filterCount != 0) ? Boolean.TRUE : Boolean.FALSE;
		if(deviceCount >= rxPalletCount) {
			deviceCount = deviceCount - rxPalletCount;			
		}
		if (Boolean.TRUE.equals(mixedCart)) {
			setMixedCartValue(map, deviceCount, filterCount, parcelFlag, freightThresholdDevice,
					freightThresholdFilter);
		} else {
			setParcelFlageValue(map, deviceCount, filterCount, parcelFlag, freightThresholdDevice,
					freightThresholdFilter);
		}
		map.put("rxProduct", rxProduct.toString());
		map.put("rxPalletCount", rxPalletCount.toString());
		return map;
	}
	private void setParcelFlageValue(Map<String, String> map, int deviceCount, int filterCount, Boolean parcelFlag,
			int freightThresholdDevice, int freightThresholdFilter) {
		if (Boolean.TRUE.equals(parcelFlag)) {
			if(deviceCount < freightThresholdDevice && filterCount < freightThresholdFilter) {
				map.put(DELIVERY_TYPE, PARCEL_DELIVERY_TYPE);
			} else {
				map.put(DELIVERY_TYPE, FREIGHT_DELIVERY_TYPE);
				getPalletCount(map, deviceCount, filterCount, 
						freightThresholdFilter, freightThresholdDevice);
			}
		} else { // freight
			map.put(DELIVERY_TYPE, FREIGHT_DELIVERY_TYPE);
			getPalletCount(map, deviceCount, filterCount, 
					freightThresholdFilter, freightThresholdDevice);
		}
	}
	private void setMixedCartValue(Map<String, String> map, int deviceCount, int filterCount, Boolean parcelFlag,
			int freightThresholdDevice, int freightThresholdFilter) {
		if ((parcelFlag && deviceCount < freightThresholdDevice)
				&& (filterCount < freightThresholdFilter)) { // parcel
			map.put(DELIVERY_TYPE, PARCEL_DELIVERY_TYPE);
		} else if ((deviceCount >= freightThresholdDevice) || (filterCount >= freightThresholdFilter)) { // freight
			map.put(DELIVERY_TYPE, FREIGHT_DELIVERY_TYPE);
			getPalletCount(map, deviceCount, filterCount, 
					freightThresholdFilter, freightThresholdDevice);
		}
	}

	/**
	 * getPalletCount method is used to get pallet count.
	 * 
	 * @param map
	 * @param deviceCount
	 * @param filterCount
	 * @param freightThresholdFilter
	 * @param freightThresholdDevice
	 */
	private void getPalletCount(Map<String, String> map, int deviceCount, int filterCount, 
			int freightThresholdFilter, int freightThresholdDevice) {
		BigDecimal palletCount = BigDecimal.ZERO;
		int productTypeCount = (deviceCount != 0 && filterCount != 0) ? 2 : 1;

		// calculate the pallet count based on product type
		if (productTypeCount == 1 && deviceCount != 0 && filterCount == 0) {
			palletCount = new BigDecimal(deviceCount / freightThresholdDevice).add(BigDecimal.ONE);
		} else if (productTypeCount == 1 && filterCount != 0 && deviceCount == 0) {
			palletCount = new BigDecimal(filterCount / freightThresholdFilter).add(BigDecimal.ONE);
		} else if (productTypeCount == 2) {
			int devicePalletCount = (deviceCount / freightThresholdDevice) + 1;
			int filterPalletCount = (filterCount / freightThresholdFilter) + 1;
			palletCount = new BigDecimal(devicePalletCount + filterPalletCount);
		}
		map.put("palletCount", palletCount.toString());
	}

	/**
	 * getAllQuotes method is used to get all the quotes object from CT.
	 * @param offset 
	 * @param limit 
	 * @param email 
	 * @param quoteNumber 
	 * @return ResponseEntity<String>
	 */
	public ResponseEntity<String> getAllQuotes(int limit, int offset, String quoteNumber, String email) {
		String token = ctServerHelperService.getStringAccessToken();
		String quotesUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey() + CART + "?limit=" + limit + "&offset=" + offset 
				+ WHERE;
		String response = null;
		if(StringUtils.hasText(quoteNumber) || StringUtils.hasText(email)) {
			quotesUrl = enocodeUrlParameters(quoteNumber, email, quotesUrl);
			try {
				response = netConnectionHelper.httpConnectionHelper(quotesUrl,token);
			} catch (IOException e) {
				logger.error("IOException Occured", e);
			}
		}else {
			quotesUrl= quotesUrl + "custom(fields(Quote=true))&sort=createdAt desc";
			response = netConnectionHelper.sendGetWithoutBody(token, quotesUrl);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * enocodeUrlParameters is used to enocode the url parameters.
	 * @param companyName
	 * @param email
	 * @param customerBaseUrl
	 * @return String
	 */
	private String enocodeUrlParameters(String quoteNumber, String email, String quotesUrl) {
		String customString ="custom(fields(Quote=true))"; 
		quotesUrl = quotesUrl + MolekuleUtility.convertToEncodeUri(customString);
		String sortString = "sort=\"lastModifiedAt desc\"";
		quotesUrl = quotesUrl +"&"+ MolekuleUtility.convertToEncodeUri(sortString);
		if (StringUtils.hasText(quoteNumber)) {
			String quoteNumberStr = "custom(fields(quoteNumber=\""+ quoteNumber + "\"))";
			quotesUrl = quotesUrl + WHERE + MolekuleUtility.convertToEncodeUri(quoteNumberStr);
		} 
		if (StringUtils.hasText(email)) {
			String emailStr = "custom(fields(email=\""+ email + "\"))";
			quotesUrl = quotesUrl + WHERE + MolekuleUtility.convertToEncodeUri(emailStr);
		}
		return quotesUrl;
	}

	/**
	 * getQuotesById method is used to get the specific quotes object from CT.
	 * 
	 * @return ResponseEntity<String>
	 */
	public ResponseEntity<String> getAllQuotes(String quoteId) {
		String response = getCartById(quoteId);
		JsonObject jsonCartObject = MolekuleUtility.parseJsonObject(response);
		if(jsonCartObject.has(CUSTOM) && jsonCartObject.get(CUSTOM).getAsJsonObject().has(FIELDS) 
				&& jsonCartObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(QUOTE)) {
			boolean quote = jsonCartObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(QUOTE).getAsBoolean();
			if(!quote) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}else {
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		}else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

}
