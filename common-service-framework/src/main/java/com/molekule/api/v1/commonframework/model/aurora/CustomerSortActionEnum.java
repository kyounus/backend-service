package com.molekule.api.v1.commonframework.model.aurora;

public enum CustomerSortActionEnum {
	
	CUSTOMER_NUMBER,
	
	FIRST_NAME,
	
	LAST_NAME,
	
	COMPANY_NAME,
	
	EMAIL,
	
	DATE_CREATED,

	DATE_MODIFIED

}
