package com.molekule.api.v1.commonframework.model.checkout;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AffirmCheckoutBean {

	@JsonProperty("checkout_token")
	private String checkoutToken;

	@JsonProperty("order_id")
	private String orderId;

	public AffirmCheckoutBean(String checkoutToken, String orderId) {
		this.checkoutToken = checkoutToken;
		this.orderId = orderId;
	}
	
	

}
