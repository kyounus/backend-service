package com.molekule.api.v1.commonframework.model.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class DiscountedLineItemPortion {

	@JsonProperty("discountedAmount")
	private TypedMoney discountedAmount;

	public TypedMoney getDiscountedAmount() {
		return discountedAmount;
	}

	public void setDiscountedAmount(TypedMoney discountedAmount) {
		this.discountedAmount = discountedAmount;
	}


}
