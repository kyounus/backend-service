package com.molekule.api.v1.commonframework.service.sns;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM_OBJECTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FREIGHT_HOLD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.HOLDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.NEXT_ORDER_DATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.OBJECT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("notificationHelperService")
public class NotificationHelperService {

	Logger logger = LoggerFactory.getLogger(NotificationHelperService.class);

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("subscriptionSignupConfirmationSender")
	TemplateMailRequestHelper subscriptionSignupConfirmationSender;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("paymentReceivedConfirmationSender")
	TemplateMailRequestHelper paymentReceivedConfirmationSender;

	@Autowired
	@Qualifier(value = "orderUpdatedMailSender")
	TemplateMailRequestHelper orderUpdatedMailSender;

	@Autowired
	@Qualifier("orderShippedMailSender")
	TemplateMailRequestHelper orderShippedMailSender;

	@Autowired
	@Qualifier("autoRefillConfirmationMailSender")
	TemplateMailRequestHelper autoRefillConfirmationMailSender;

	@Autowired
	@Qualifier("subscriptionPaymentFailedMailSender")
	TemplateMailRequestHelper subscriptionPaymentFailedMailSender;

	@Autowired
	@Qualifier("orderProcessedMailSender")
	TemplateMailRequestHelper orderProcessedMailSender;

	@Autowired
	@Qualifier("orderCancelMailSender")
	TemplateMailRequestHelper orderCancelMailSender;

	public String getSNSResponse(HttpServletRequest serverHttpRequest) {
		String response = null;
		logger.info("Get SNS Response Method Starts..");
		try {
			response = new String(StreamUtils.copyToByteArray(serverHttpRequest.getInputStream()));
			JsonObject responseObject = MolekuleUtility.parseJsonObject(response);
			String type = responseObject.get("TopicArn").getAsString();
			if (responseObject.get("Type").getAsString().equals("SubscriptionConfirmation")) {
				logger.info("responseObject : {}", response);
			} else {
				processUnscbscriptionConfirmation(serverHttpRequest, responseObject, type);
			}
		} catch (IOException e) {
			logger.error("IOExcetion occured getSNSResponse", e);
		}
		return response;
	}

	private void processUnscbscriptionConfirmation(HttpServletRequest serverHttpRequest, JsonObject responseObject,
			String type) {
		JsonObject messageObject = MolekuleUtility.parseJsonObject(responseObject.get("Message").getAsString());
		if (type.contains(ctEnvProperties.getAnalyticsSubscriptionArn())) {
			if(messageObject.get(ACTION).getAsString().equals("create")) {
				subscriptionSignupMailConfirmation(messageObject);
				tealiumHelperService.getActivateAutoRefills(messageObject.get(OBJECT).getAsJsonObject());

			}
			if(messageObject.get(ACTION).getAsString().equals("update")) {
				autoRefilConfirmationMail(messageObject);
				tealiumHelperService.getAutorefillRenewalstate(messageObject.get(OBJECT).getAsJsonObject());
			}
		} else if (type.contains(ctEnvProperties.getAnalyticsAchPaidArn())
				&& messageObject.has(MolekuleConstant.PAYMENT_STATE)
				&& messageObject.get(MolekuleConstant.PAYMENT_STATE).getAsString().equals("Paid")) {
			paymentReceivedMailOperation(messageObject.get(OBJECT).getAsJsonObject());
		} else if (type.contains(ctEnvProperties.getAnalyticsOrderArn())) {
			JsonObject resultObject = messageObject.get(OBJECT).getAsJsonObject();
			if(messageObject.get(ACTION).getAsString().equals("create")) {
				tealiumHelperService.setTransactionSuccess(resultObject,serverHttpRequest);
				tealiumHelperService.setOrderCompeltion(resultObject,serverHttpRequest);
			}
			if(messageObject.has(MolekuleConstant.ACTION_TYPE) && MolekuleConstant.LINE_ITEM_STATE_TRANSITION.equals(messageObject.get(MolekuleConstant.ACTION_TYPE).getAsString())&&
					MolekuleConstant.UPDATE.equals(messageObject.get(MolekuleConstant.ACTION).getAsString())){
				if(messageObject.has("toState")) {
					setLineItemStateTransition(messageObject, resultObject);
				}else {
					orderShippedMailOperation(resultObject);
				}
			}
			paymentSuccessOperation(resultObject,serverHttpRequest);
			orderCancelMailOperation(resultObject);
		}
	}

	private void setLineItemStateTransition(JsonObject messageObject, JsonObject resultObject) {
		String stateId = messageObject.get("toState").getAsJsonObject().get("id").getAsString();
		String statesUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/states/" + stateId;
		String stateResponse = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(),
				statesUrl);
		JsonObject statesJsonObject = MolekuleUtility.parseJsonObject(stateResponse);
		if (statesJsonObject.has("key") && statesJsonObject.has("name")
				&& statesJsonObject.get("name").getAsJsonObject().has("en")
				&& statesJsonObject.get("key").getAsString().equals("lineitem-accepted")
				&& statesJsonObject.get("name").getAsJsonObject().get("en").getAsString().equals("Accepted")){
			SendGridModel sendGridModel = registrationHelperService
					.createEmailDynamicInfo(resultObject.get(MolekuleConstant.CUSTOMER_ID).getAsString());
			sendGridModel.setOrderNumber(resultObject.get(ORDER_NUMBER).getAsString());
			orderProcessedMailSender.sendMail(sendGridModel);
		}
	}

	private void paymentSuccessOperation(JsonObject resultObject, HttpServletRequest serverHttpRequest) {
		if (resultObject.has(MolekuleConstant.PAYMENT_STATE)
				&& resultObject.get(MolekuleConstant.PAYMENT_STATE).getAsString().equals("Paid")) {
			tealiumHelperService.setTransactionSuccess(resultObject, serverHttpRequest);

		}
	}

	private void orderShippedMailOperation(JsonObject resultObject) {
		if (resultObject.has(MolekuleConstant.STATE)
				&& resultObject.get(MolekuleConstant.STATE).getAsJsonObject().has("id")) {
			String stateId = resultObject.get(MolekuleConstant.STATE).getAsJsonObject().get("id").getAsString();
			String statesUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/states/" + stateId;
			String response = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(),
					statesUrl);
			JsonObject statesJsonObject = MolekuleUtility.parseJsonObject(response);
			if (statesJsonObject.has("key") && statesJsonObject.has("name")
					&& statesJsonObject.get("name").getAsJsonObject().has("en")
					&& statesJsonObject.get("key").getAsString().equals("order-shipped")
					&& statesJsonObject.get("name").getAsJsonObject().get("en").getAsString().equals("Shipped")) {
				SendGridModel sendGridModel = registrationHelperService
						.createEmailDynamicInfo(resultObject.get(MolekuleConstant.CUSTOMER_ID).getAsString());
				sendGridModel.setOrderNumber(resultObject.get(ORDER_NUMBER).getAsString());
				if (!resultObject.get(MolekuleConstant.SHIPPING_INFO).getAsJsonObject().get("shippingMethodName")
						.getAsString().equals("Contact Carrier") && resultObject.has(MolekuleConstant.SHIPPING_INFO)) {
					sendGridModel.setOrderFedExTrackingInfo(resultObject.get(MolekuleConstant.SHIPPING_INFO)
							.getAsJsonObject().get("deliveries").getAsJsonArray().get(0).getAsJsonObject()
							.get("parcels").getAsJsonArray().get(0).getAsJsonObject().get("trackingData")
							.getAsJsonObject().get("trackingId").getAsString());
				} else if (resultObject.get(MolekuleConstant.CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS)
						.getAsJsonObject().has(MolekuleConstant.OWN_CARRIER)) {
					JsonObject ownCarrierObject = MolekuleUtility.parseJsonObject(
							resultObject.get(MolekuleConstant.CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS)
							.getAsJsonObject().get(MolekuleConstant.OWN_CARRIER).getAsString());
					if (ownCarrierObject.has("carrierName") && ownCarrierObject.has("carrierAccountNumber")) {
						sendGridModel.setCustomerCarrierName(ownCarrierObject.get("carrierName").getAsString());
						sendGridModel.setCustomerCarrierAccountNumber(
								ownCarrierObject.get("carrierAccountNumber").getAsString());
					}
				}
				orderShippedMailSender.sendMail(sendGridModel);
			}
		}
	}

	private void subscriptionSignupMailConfirmation(JsonObject messageObject) {
		JsonObject valueResponse = messageObject.get(OBJECT).getAsJsonObject().get(VALUE).getAsJsonObject();
		SendGridModel sendGridModel = registrationHelperService
				.createEmailDynamicInfo(valueResponse.get(CUSTOMER).getAsJsonObject().get("id").getAsString());
		if (valueResponse.get(NEXT_ORDER_DATE).getAsString() != null) {
			sendGridModel.setNextRefillDate(valueResponse.get(NEXT_ORDER_DATE).getAsString());
		}
		if (valueResponse.getAsJsonObject("cart") != null) {
			JsonObject cartResponse = valueResponse.get("cart").getAsJsonObject();
			JsonObject priceValueResponse = cartResponse.get("obj").getAsJsonObject().get("totalPrice")
					.getAsJsonObject();
			sendGridModel
			.setSubscriptionTotal(centToDollarConversion(priceValueResponse.get("centAmount").getAsLong()));
		}
		subscriptionSignupConfirmationSender.sendMail(sendGridModel);
	}

	private void autoRefilConfirmationMail(JsonObject messageObject) {
		JsonObject valueResponse = messageObject.get(OBJECT).getAsJsonObject().get(VALUE).getAsJsonObject();
		String cartId = valueResponse.get("cart").getAsJsonObject().get("obj").getAsJsonObject().get("id")
				.getAsString();
		String customerId = valueResponse.get(CUSTOMER).getAsJsonObject().get("id").getAsString();
		String subscriptionUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=value(customer(id=\"" + customerId + "\"))" + "&expand=value.cart.id";
		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, subscriptionUrl);
		JsonObject josonObject = MolekuleUtility.parseJsonObject(response);
		List<String> nextRefilDate = new ArrayList<>();
		josonObject.get(RESULTS).getAsJsonArray().forEach(subscription -> {
			if (subscription.getAsJsonObject().get(VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("id")
					.getAsString().equals(cartId)
					&& subscription.getAsJsonObject().get(VALUE).getAsJsonObject().get(CUSTOMER).getAsJsonObject()
					.get("id").getAsString().equals(customerId)
					&& subscription.getAsJsonObject().get(VALUE).getAsJsonObject().get("state").getAsString()
					.equals("Processed")) {
				SendGridModel sendGridModel = registrationHelperService.createEmailDynamicInfo(customerId);
				if (valueResponse.get(NEXT_ORDER_DATE).getAsString() != null) {
					nextRefilDate.add(valueResponse.get(NEXT_ORDER_DATE).getAsString());
				}
				sendGridModel.setNextRefilterDate(nextRefilDate);
				autoRefillConfirmationMailSender.sendMail(sendGridModel);
			}
			manupulateValue(cartId, customerId, subscription);
		});

	}

	private void manupulateValue(String cartId, String customerId, JsonElement subscription) {
		if(subscription.getAsJsonObject().get(VALUE).getAsJsonObject().
				get("cart").getAsJsonObject().get("id").getAsString().equals(cartId) &&
				subscription.getAsJsonObject().get(VALUE).getAsJsonObject().
				get("customer").getAsJsonObject().get("id").getAsString().equals(customerId) &&
				(subscription.getAsJsonObject().get(VALUE).getAsJsonObject().get("state").
						getAsString().equals("Failed") || 
						subscription.getAsJsonObject().get(VALUE).getAsJsonObject().get("state").
						getAsString().equals("Retry-Scheduled"))){
			SendGridModel sendGridModel = registrationHelperService
					.createEmailDynamicInfo(customerId);
			JsonArray transactionArray = subscription.getAsJsonObject().get(VALUE).getAsJsonObject().get("transactions").getAsJsonArray();
			int failedRetries=0;
			for(int i=0;i<transactionArray.size();i++) {
				String type = transactionArray.get(i).getAsJsonObject().get("type").getAsString();
				String message = transactionArray.get(i).getAsJsonObject().get("messages").getAsString();
				if(type.equals("failed") && message.contains("Payment")) {
					failedRetries++;
				}
			}
			if(failedRetries>0) {
				sendGridModel.setFailedRetries(failedRetries);
				subscriptionPaymentFailedMailSender.sendMail(sendGridModel);
			}
		}
	}

	private void paymentReceivedMailOperation(JsonObject messageObject) {
		String paymentType = messageObject.get("paymentInfo").getAsJsonObject().get("payments").getAsJsonArray().get(0)
				.getAsJsonObject().get("obj").getAsJsonObject().get("paymentMethodInfo").getAsJsonObject().get("method")
				.getAsString();
		if (messageObject.get(MolekuleConstant.PAYMENT_STATE).getAsString().equals("Paid")
				&& paymentType.equals("ACH")) {
			SendGridModel sendGridModel = registrationHelperService
					.createEmailDynamicInfo(messageObject.get("customerId").getAsString());
			sendGridModel.setOrderNumber(messageObject.get(ORDER_NUMBER).getAsString());
			paymentReceivedConfirmationSender.sendMail(sendGridModel);
			sendGridModel.setNonCreditCardPayment(false);
			if (null != messageObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(HOLDS)) {
				JsonArray holdsArray = messageObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
						.get(HOLDS).getAsJsonArray();
				for (int i = 0; i < holdsArray.size(); i++) {
					if ((FREIGHT_HOLD).equals(holdsArray.get(i).getAsString())) {
						sendGridModel.setMissingFreightInformation(true);
						orderUpdatedMailSender.sendMail(sendGridModel);
						break;
					}
				}
			}
		}
	}

	public static String centToDollarConversion(long centPayment) {
		NumberFormat numberFormat = null;
		if(Locale.getDefault().getCountry().equals("US")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		}else if(Locale.getDefault().getCountry().equals("GB")) {
			numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		}
		return numberFormat.format(centPayment / 100.0);
	}

	private void orderCancelMailOperation(JsonObject resultObject) {
		if (resultObject.has(MolekuleConstant.STATE)
				&& resultObject.get(MolekuleConstant.STATE).getAsJsonObject().has("id")) {
			String stateId = resultObject.get(MolekuleConstant.STATE).getAsJsonObject().get("id").getAsString();
			String statesUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/states/" + stateId;
			String response = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(),
					statesUrl);
			JsonObject statesJsonObject = MolekuleUtility.parseJsonObject(response);
			if (statesJsonObject.has("key") && statesJsonObject.has("name")
					&& statesJsonObject.get("name").getAsJsonObject().has("en")
					&& statesJsonObject.get("key").getAsString().equals("order-cancelled")
					&& statesJsonObject.get("name").getAsJsonObject().get("en").getAsString().equals("Cancelled")) {
				SendGridModel sendGridModel = registrationHelperService
						.createEmailDynamicInfo(resultObject.get(MolekuleConstant.CUSTOMER_ID).getAsString());
				sendGridModel.setOrderId(resultObject.get("id").getAsString());
				sendGridModel.setOrderNumber(resultObject.get(ORDER_NUMBER).getAsString());
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
				SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date date = format1.parse(resultObject.get("createdAt").getAsString());
					sendGridModel.setOrderDate(format2.format(date));
				} catch (ParseException e) {
					logger.error("Date Parsing Exception", e);
				}
				orderCancelMailSender.sendMail(sendGridModel);
			}
		}
	}
}
