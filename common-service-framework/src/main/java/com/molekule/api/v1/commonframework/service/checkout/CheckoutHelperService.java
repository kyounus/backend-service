package com.molekule.api.v1.commonframework.service.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.API_STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CART;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SET_CUSTOM_FIELD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_DISCOUNT_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNT_CODES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNT_MESSAGE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FREE_SHIPPING_DISCOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.NO_CART_DISCOUNTS_APPLIED;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartCustomServiceBean;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.model.checkout.DiscountCodeRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.SetTaxAmount;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import reactor.core.publisher.Mono;

@Service("checkoutHelperService")
public class CheckoutHelperService {

	Logger logger = LoggerFactory.getLogger(CheckoutHelperService.class);

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;
	
	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("cartHelperService")
	CartHelperService cartHelperService;
	
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	/**
	 * getAccountCustomObjectApi method is to retrieve AccountCustomObject data by
	 * passing account id.
	 * 
	 * @param accountId
	 * @param customerId 
	 * @return ResponseEntity<AccountCustomObject>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<AccountCustomObject> getAccountCustomObjectApi(String accountId, String customerId) {
		AccountCustomObject accountCustomObject = new AccountCustomObject();

		ObjectMapper mapper = new ObjectMapper();

		WebClient client = WebClient.create(registrationHelperService.getCTCustomObjectsByAccountIdUrl(accountId));
		Mono<String> monoResponse = client.get()
				.header(HttpHeaders.AUTHORIZATION, ctServerHelperService.getStringAccessToken())
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(String.class);
				});
		String accountStringResponse = monoResponse.block();

		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
		if (accountJsonObject.get(STATUS_CODE) != null) {
			return new ResponseEntity(HttpStatus.valueOf(accountJsonObject.get(STATUS_CODE).getAsInt()));
		}
		JsonArray resultsArray = accountJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		if (resultsArray.size() != 0) {
			JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
			if (resultsObject.getAsJsonObject(VALUE) != null) {
				JsonObject valueObject = resultsObject.getAsJsonObject(VALUE).getAsJsonObject();
				String container = resultsObject.get(CONTAINER).getAsString();
				String key = resultsObject.get("key").getAsString();
				try {
					Value value = mapper.readValue(valueObject.toString(), Value.class);
					List<Payments> paymentList = registrationHelperService.assignCardObjectToPayments(value, customerId);
					value.setPayments(paymentList);
					accountCustomObject.setValue(value);
				} catch (JsonProcessingException e) {
					logger.error("JsonProcessingException occured in getAccountCustomObjectApi ", e);
				}
				Integer version = resultsObject.get(VERSION).getAsInt();
				accountCustomObject.setContainer(container);
				accountCustomObject.setKey(key);
				accountCustomObject.setVersion(version);
			}
		}
		return new ResponseEntity(accountCustomObject, HttpStatus.OK);
	}
	
	public String applyPromoCode(String cartId, String promoCode) {
		long cartVersion = cartHelperService.getCartCurrentVersionByCartId(cartId);
		String url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId +"?expand=discountCodes[*].discountCode.typeId";
		String response = null;
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		// Get cart Response
		String cartResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		// Check already Promo applied or not
		boolean isPromoAvalible = false;
		isPromoAvalible = checkIsPromoAvalible(cartResponse);
		if (isPromoAvalible) {
			ErrorResponse errorResponse = new ErrorResponse(400,
					"Unable to Apply Multiple Promo's.Already Promo setted.");
			return errorResponse.toString();
		}
		DiscountCodeRequestBean disCodeRequestBean = new DiscountCodeRequestBean();
		disCodeRequestBean.setVersion(cartVersion);
		DiscountCodeRequestBean.Action action = new DiscountCodeRequestBean.Action();
		action.setActionName("addDiscountCode");
		action.setCode(promoCode);
		List<DiscountCodeRequestBean.Action> actions = new ArrayList<>();
		actions.add(action);
		disCodeRequestBean.setActions(actions);
		response = (String) netConnectionHelper.sendPostRequest(url, token, disCodeRequestBean);
		if(MolekuleUtility.parseJsonObject(response).has(STATUS_CODE)) {
			ErrorResponse errorResponse = new ErrorResponse(400,
					"Invalid Promocode.");
			return errorResponse.toString();
		}
		long discountedAmount = cartHelperService.getCustomDiscountValue(response, "addPromo");
		BigDecimal decimalDiscountedVal = BigDecimal.valueOf(discountedAmount).movePointLeft(2);
		long roundDiscountedValue = Math.round(decimalDiscountedVal.floatValue());
		if (roundDiscountedValue != 0) {
			roundDiscountedValue = roundDiscountedValue * 100;
		}
		DiscountCodeRequestBean discountCodeRequestBean = new DiscountCodeRequestBean();
		List<DiscountCodeRequestBean.Action> customActions = new ArrayList<>();
		long currentVersion = cartHelperService.getCartCurrentVersionByCartId(cartId);
		discountCodeRequestBean.setVersion(currentVersion);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		boolean isJsonObject = jsonObject.has(CUSTOM);
		setCustomValue(discountedAmount, roundDiscountedValue, customActions, isJsonObject);
		discountCodeRequestBean.setActions(customActions);
		response = (String) netConnectionHelper.sendPostRequest(url, token, discountCodeRequestBean);
		jsonObject = MolekuleUtility.parseJsonObject(response);
		if(jsonObject.has(CUSTOM)) {
			JsonObject customJsonObject = jsonObject.get(CUSTOM).getAsJsonObject();
			JsonObject fieldsJsonObject = customJsonObject.get(FIELDS).getAsJsonObject();
			if(fieldsJsonObject.has(FREE_SHIPPING_DISCOUNT)) {
				UpdateCartCustomServiceBean updateCartCustomServiceBean = getUpdateCartCustomerServiceBean(jsonObject,
						fieldsJsonObject);
				response = (String)netConnectionHelper.sendPostRequest(url, token, updateCartCustomServiceBean);
				jsonObject = MolekuleUtility.parseJsonObject(response);
			}
		}
		if(validateDiscountCodes(jsonObject)) {
			return setDiscountMessage(jsonObject, url, token, DISCOUNT_MESSAGE, NO_CART_DISCOUNTS_APPLIED);
		}
		boolean isShippingAddressAvailable = jsonObject.has(SHIPPING_ADDRESS);
		if (isShippingAddressAvailable) {
			Object taxResponse = cartHelperService.calculateTax(response);
			if (taxResponse instanceof String) {
				return (String) taxResponse;
			}
			SetTaxAmount setTaxAmount = (SetTaxAmount) taxResponse;
			response = (String) netConnectionHelper.sendPostRequest(url, token, setTaxAmount);
		}
		response = cartHelperService.updateCustomField(response, cartId, url, token);
		logger.trace("Add Discount to Cart Response");
		return response;
	}

	private boolean validateDiscountCodes(JsonObject jsonObject) {
		JsonArray discountCodes = jsonObject.get(DISCOUNT_CODES).getAsJsonArray();
		if (discountCodes.size() <=0) {
			return false;
		}
		
		for (int i = 0; i < discountCodes.size(); i++) {
			String state = discountCodes.get(i).getAsJsonObject().get(STATE).getAsString();
			if ("DoesNotMatchCart".equals(state)) {
				return true;
			} else {
				if(validateMatchCartValues(jsonObject)) {
					return true;
				}
			}
		}
	
		return false;
	}

	private boolean validateMatchCartValues(JsonObject jsonObject) {
		if (jsonObject.has(CUSTOM)) {
			JsonObject customObject = jsonObject.get(CUSTOM).getAsJsonObject();
			JsonObject fieldsJsonObject = customObject.get(FIELDS).getAsJsonObject();
			String totalDiscountPrice = fieldsJsonObject.get(TOTAL_DISCOUNT_PRICE).getAsString();
			if ("0".equals(totalDiscountPrice)) {
				return true;
			}
			if (fieldsJsonObject.has(FREE_SHIPPING_DISCOUNT)) {
				String freeShippingDiscount = fieldsJsonObject.get(FREE_SHIPPING_DISCOUNT).getAsString();
				if (freeShippingDiscount.equals(totalDiscountPrice)) {
					return true;
				}
			}
		}
		return false;
	}

	public String setDiscountMessage(JsonObject jsonObject, String url, String token, String name, String value) {
		Long version = jsonObject.get(VERSION).getAsLong();
		UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
		List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
		UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(name);
		action.setValue(value);
		updateActionList.add(action);
		updateCartCustomServiceBean.setActions(updateActionList);
		updateCartCustomServiceBean.setVersion(version);
		return (String)netConnectionHelper.sendPostRequest(url, token, updateCartCustomServiceBean);
	}

	private boolean checkIsPromoAvalible(String cartResponse) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonObject fieldObjectJson = null;
		if (jsonObject.has(CUSTOM)) {
			fieldObjectJson = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			long freeShippingCost = fieldObjectJson.has(FREE_SHIPPING_DISCOUNT)
					? fieldObjectJson.get(FREE_SHIPPING_DISCOUNT).getAsLong()
					: 0;
			long totalDiscountPrice = fieldObjectJson.has(TOTAL_DISCOUNT_PRICE)
					? fieldObjectJson.get(TOTAL_DISCOUNT_PRICE).getAsLong()
					: 0;
			long promoDiscount = totalDiscountPrice - freeShippingCost;
			return promoDiscount > 0;
		}
		return false;
	}

	private void setCustomValue(long discountedAmount, long roundDiscountedValue,
			List<DiscountCodeRequestBean.Action> customActions, boolean isJsonObject) {
		if (isJsonObject) {
			DiscountCodeRequestBean.Action customAction = new DiscountCodeRequestBean.Action();
			customAction.setActionName(SET_CUSTOM_FIELD);
			customAction.setName(TOTAL_DISCOUNT_PRICE);
			customAction.setValue(String.valueOf(roundDiscountedValue));
			customActions.add(customAction);
		} else {
			DiscountCodeRequestBean.Action customAction = new DiscountCodeRequestBean.Action();
			DiscountCodeRequestBean.Action.Type discountCodeRequestBeanActionType = new DiscountCodeRequestBean.Action.Type();
			customAction.setActionName("setCustomType");
			discountCodeRequestBeanActionType.setId("937cb0c5-55a8-4c88-af52-3bcb8c53a04c");
			discountCodeRequestBeanActionType.setTypeId("type");
			customAction.setType(discountCodeRequestBeanActionType);
			DiscountCodeRequestBean.Action.Fields fields = new DiscountCodeRequestBean.Action.Fields();
			fields.setTotalDiscountPrice(String.valueOf(discountedAmount));
			customAction.setFields(fields);
			customActions.add(customAction);
		}
	}

	private UpdateCartCustomServiceBean getUpdateCartCustomerServiceBean(JsonObject jsonObject,
			JsonObject fieldsJsonObject) {
		Long freeShippingDiscount = Long.parseLong(fieldsJsonObject.get(FREE_SHIPPING_DISCOUNT).getAsString()); 
		BigDecimal decimalFreeShippingVal = BigDecimal.valueOf(freeShippingDiscount).movePointLeft(2);
		long roundFreeShippingValue = Math.round(decimalFreeShippingVal.floatValue());
		if (roundFreeShippingValue != 0) {
			roundFreeShippingValue = roundFreeShippingValue * 100;
		}
		UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
		List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
		UpdateCartCustomServiceBean.Action freeShippingction = new UpdateCartCustomServiceBean.Action();
		freeShippingction.setActionName(SET_CUSTOM_FIELD);
		freeShippingction.setName(FREE_SHIPPING_DISCOUNT);
		freeShippingction.setValue(String.valueOf(roundFreeShippingValue));
		updateActionList.add(freeShippingction);
		updateCartCustomServiceBean.setActions(updateActionList);
		updateCartCustomServiceBean.setVersion(jsonObject.get(VERSION).getAsLong());
		return updateCartCustomServiceBean;
	}

}
