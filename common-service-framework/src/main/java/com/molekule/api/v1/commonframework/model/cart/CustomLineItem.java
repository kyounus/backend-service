package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;

public class CustomLineItem {

	@JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private Map<String, String> name;

    @JsonProperty("money")
    private TypedMoney money;

    @JsonProperty("taxedPrice")
    private TaxedItemPrice taxedPrice;

    @JsonProperty(TOTAL_PRICE)
    private TypedMoney totalPrice;

    @JsonProperty("slug")
    private String slug;

    @JsonProperty(QUANTITY)
    private Long quantity;
    
    @JsonProperty(STATE)
    private List<ItemState> state;

    @JsonProperty("taxRate")
    private TaxRate taxRate;
    
    @JsonProperty("discountedPricePerQuantity")
    private List<DiscountedLineItemPriceForQuantity> discountedPricePerQuantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, String> getName() {
		return name;
	}

	public void setName(Map<String, String> name) {
		this.name = name;
	}

	public TypedMoney getMoney() {
		return money;
	}

	public void setMoney(TypedMoney money) {
		this.money = money;
	}

	public TaxedItemPrice getTaxedPrice() {
		return taxedPrice;
	}

	public void setTaxedPrice(TaxedItemPrice taxedPrice) {
		this.taxedPrice = taxedPrice;
	}

	public TypedMoney getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(TypedMoney totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public List<ItemState> getState() {
		return state;
	}

	public void setState(List<ItemState> state) {
		this.state = state;
	}

	public TaxRate getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(TaxRate taxRate) {
		this.taxRate = taxRate;
	}

	public List<DiscountedLineItemPriceForQuantity> getDiscountedPricePerQuantity() {
		return discountedPricePerQuantity;
	}

	public void setDiscountedPricePerQuantity(List<DiscountedLineItemPriceForQuantity> discountedPricePerQuantity) {
		this.discountedPricePerQuantity = discountedPricePerQuantity;
	}
    
    
}
