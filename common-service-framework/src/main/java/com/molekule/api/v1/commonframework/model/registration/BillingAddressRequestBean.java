package com.molekule.api.v1.commonframework.model.registration;

import io.swagger.annotations.ApiModelProperty;

/**
 * The class BillingAddressRequestBean is sued to hold the request data of billing address.
 */
public class BillingAddressRequestBean {

	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String firstName;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String lastName;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String streetAddress1;

	private String streetAddress2;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String city;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String state;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String postalCode;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String country;
	@ApiModelProperty(value = "If myCarrierInfo enabled.This required", required = true)
	private String phoneNumber;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
