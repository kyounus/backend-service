package com.molekule.api.v1.commonframework.service.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACCESS_TOKEN;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.API_STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CLIENT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM_OBJECTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMAIL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMPTY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FREIGHT_HOLD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.HOLDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SECRET_HASH;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SET_CUSTOM_FIELD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SLASH_CUSTOMERS_SLASH;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.__TYPE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartCustomServiceBean;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountDto;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO.Action;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.checkout.ClearHoldState;
import com.molekule.api.v1.commonframework.model.registration.AdditionalFreightInfo;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;
import com.molekule.api.v1.commonframework.model.registration.ForgotPasswordRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import reactor.core.publisher.Mono;
@Service("registrationHelperService")
public class RegistrationHelperService {
	Logger logger = LoggerFactory.getLogger(RegistrationHelperService.class);

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("checkoutHelperService")
	CheckoutHelperService checkoutHelperService;

	@Autowired
	@Qualifier(value = "orderUpdatedMailSender")
	TemplateMailRequestHelper orderUpdatedMailSender;

	@Autowired(required = true)
	@Qualifier(value = "freightInformationConfirmationSender")
	TemplateMailRequestHelper freightInformationConfirmationSender;

	@Autowired
	@Qualifier("authenticationHelperService")
	AuthenticationHelperService authenticationHelperService;

	@org.springframework.beans.factory.annotation.Value("${stripeApiKey}")
	private String stripeAuthKey;

	@org.springframework.beans.factory.annotation.Value("${paymentMethodUrl}")
	private String paymentMethodUrl;

	@org.springframework.beans.factory.annotation.Value("${setupPaymentIntentUrl}")
	private String setupPaymentIntentUrl;

	public CustomerResponseBean getCustomerById(String customerId) {
		return  getCustomerById(customerId,null);
	}

	public CustomerResponseBean getCustomerById(String customerId,String token) {
		String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_CUSTOMERS_SLASH + customerId;
		if(token == null) {
			token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		}
		return netConnectionHelper.sendGet(token, customerByIdUrl);
	}

	/**
	 * getCTCustomObjectsByAccountIdUrl method is used to form url for
	 * custom-objects where condition by id.
	 * 
	 * @param accountId
	 * @return String
	 */
	public String getCTCustomObjectsByAccountIdUrl(String accountId) {
		return new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
				.append("/custom-objects?where=id=\"").append(accountId).append("\"").toString();
	}

	/**
	 * assignCardObjectToPayments method is used to assign Card Object to payments
	 * list.
	 * 
	 * @param value
	 * @param customerId 
	 * @return List<Payments>
	 */
	public List<Payments> assignCardObjectToPayments(Value value, String customerId) {
		List<Payments> paymentList = new ArrayList<>();
		if (value.getPayments() != null) {
			for (Payments payment : value.getPayments()) {

				if(StringUtils.hasText(customerId) ) {
					setCardValues(customerId, paymentList, payment);
				}else {
					setPaymentType(paymentList, payment);
				}
			}
		}
		return paymentList;
	}

	private void setCardValues(String customerId, List<Payments> paymentList, Payments payment) {
		if(payment.getCustomerId().equals(customerId) && payment.getPaymentType().equals(PaymentRequestBean.Payment.CREDITCARD.toString()) && payment.getPaymentToken() != null ) {
			payment.setCardObject(getPaymentCardDetails(payment.getPaymentToken()));
			paymentList.add(payment);
		}else if(payment.getPaymentType().equals(PaymentRequestBean.Payment.ACH.toString()) || payment.getPaymentType().equals(PaymentRequestBean.Payment.PAYMENTTERMS.toString())) {
			paymentList.add(payment);
		}
	}

	private void setPaymentType(List<Payments> paymentList, Payments payment) {
		if (payment.getPaymentType().equals(PaymentRequestBean.Payment.CREDITCARD.toString())
				&& payment.getPaymentToken() != null) {
			// Stripe implementation
			payment.setCardObject(getPaymentCardDetails(payment.getPaymentToken()));
		}
		paymentList.add(payment);
	}

	/**
	 * getPaymentCardDetails method is used to fetch card details from Stripe Api.
	 * 
	 * @param paymentToken
	 * @return
	 */
	public CreditCard getPaymentCardDetails(String paymentToken) {
		String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
		String paymentIntentBaseUrl = setupPaymentIntentUrl + "/" + paymentToken;
		String setupPaymentIntentResponse = netConnectionHelper.sendGetWithoutBody(authToken, paymentIntentBaseUrl);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(setupPaymentIntentResponse);
		String paymentMethodId = jsonObject.get("payment_method").getAsString();

		String paymentMethodBaseUrl = paymentMethodUrl + "/" + paymentMethodId;
		String paymentMethodResponse = netConnectionHelper.sendGetWithoutBody(authToken, paymentMethodBaseUrl);
		JsonObject methodJsonObject = MolekuleUtility.parseJsonObject(paymentMethodResponse);
		JsonObject cardObject = methodJsonObject.get("card").getAsJsonObject();
		CreditCard newCard = new CreditCard();

		newCard.setBrand(cardObject.get("brand").getAsString());
		newCard.setExpMonth(cardObject.get("exp_month").getAsString());
		newCard.setExpYear(cardObject.get("exp_year").getAsString());
		newCard.setLast4(cardObject.get("last4").getAsString());

		return newCard;
	}

	public String getAccountKeyById(String accountId) {
		String url = getCTCustomObjectsByAccountIdUrl(accountId);
		String token = ctServerHelperService.getStringAccessToken();
		String accountStringResponse = netConnectionHelper.sendGetWithoutBody(token, url);
		JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
		if (accountJsonObject.get(STATUS_CODE) != null) {
			return null;
		}
		JsonArray resultsArray = accountJsonObject.getAsJsonArray(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		return resultsObject.get("key").getAsString();
	}

	/**
	 * getAccountCustomObject method is used to get the account custom object
	 * information.
	 * 
	 * @param key
	 * @return Object
	 */
	public Object getAccountCustomObject(String key) {
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/").append("custom-objects").append("/").append("b2b")
				.append("/").append(key).toString();
		return netConnectionHelper.sendPostWithoutBody(ctServerHelperService.getStringAccessToken(), url);
	}

	/**
	 * getCustomerGroup method is used to get the customer group information.
	 * 
	 * @param customerGroupId
	 * @return
	 */
	public Object getCustomerGroup(String customerGroupId) {
		String url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customer-groups/"
				+ customerGroupId;

		return netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(), url);

	}

	/**
	 * getAccountIdWithCustomerId method is used to get accountid with customerid in
	 * customers api
	 * 
	 * @param paymentOptionRequestBean
	 */
	public String getAccountIdWithCustomerId(String customerId) {
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customers/"
				+ customerId + "/?";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerBaseUrl);
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
		JsonObject fieldObject = customerJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		return fieldObject.has("accountId") ? fieldObject.get("accountId").getAsString() : null;

	}

	public SendGridModel createEmailDynamicInfo(String customerId) {
		SendGridModel sendGridModel = new SendGridModel();
		CustomerResponseBean customer = getCustomerById(customerId);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		sendGridModel.setCustomerId(customerId);
		sendGridModel.setRequestedDate(formatter.format(date));
		sendGridModel.setEmail(customer.getEmail());
		sendGridModel.setFirstName(customer.getFirstName());
		sendGridModel.setLastName(customer.getLastName());
		sendGridModel.setCustomerCompanyName(customer.getCompanyName());
		sendGridModel.setPhoneNumber(customer.getCustom().getFields().getPhone());
		sendGridModel.setChannel(customer.getCustom().getFields().getChannel());
		if(customer.getCustom().getFields().getChannel().equals("B2B")) {
			String accountId = getAccountIdWithCustomerId(customerId);
			String accountStringResponse = netConnectionHelper.sendGetWithoutBody(
					ctServerHelperService.getStringAccessToken(), getCTCustomObjectsByAccountIdUrl(accountId));
			JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountStringResponse);
			if (accountJsonObject.get(STATUS_CODE) != null) {
				return null;
			}
			if (accountJsonObject.getAsJsonArray(RESULTS) != null) {
				processResultValue(sendGridModel, accountJsonObject);
			}
		}
		return sendGridModel;
	}

	private void processResultValue(SendGridModel sendGridModel, JsonObject accountJsonObject) {
		JsonArray resultsArray = accountJsonObject.get(RESULTS).getAsJsonArray();
		JsonObject resultsObject = resultsArray.get(0).getAsJsonObject();
		if (resultsObject.getAsJsonObject(VALUE) != null) {
			JsonObject valueJsonObject = resultsObject.get(VALUE).getAsJsonObject();
			if (valueJsonObject.getAsJsonObject("companyCategory") != null) {
				JsonObject companyCategory = valueJsonObject.get("companyCategory").getAsJsonObject();
				sendGridModel.setCompanyIndustry(companyCategory.get("name").getAsString());
				if (valueJsonObject.getAsJsonObject("salesRepresentative") != null) {
					JsonObject salesRep = valueJsonObject.get("salesRepresentative").getAsJsonObject();
					if (salesRep.get("name").getAsString().trim().split(" ").length > 1) {
						sendGridModel.setSalesRepFirstName(salesRep.get("name").getAsString().trim().split(" ")[0]);
						sendGridModel.setSalesRepLastName(salesRep.get("name").getAsString().trim().split(" ")[1]);
					} else {
						sendGridModel.setSalesRepFirstName(salesRep.get("name").getAsString().trim().split(" ")[0]);
						sendGridModel.setSalesRepLastName(EMPTY);
					}
					sendGridModel.setSalesRepPhoneNumber(salesRep.get(PHONE).getAsString());
					sendGridModel.setSalesRepEmail(salesRep.get(EMAIL).getAsString());
					sendGridModel.setSalesRepCalendyLink("Calendy Link");
				}
			}
		}
	}

	/**
	 * getAccountByCustomerId method is used to get custom objects by customerId
	 * 
	 * @param customerId
	 * @param token2 
	 * @return AccountCustomObject
	 */
	public AccountCustomObject getAccountByCustomerId(String customerId, String token) {
		String accountUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=value(employeeIds=\"" + customerId + "\")";
		AccountDto accountResult = netConnectionHelper.sendGetAccountObject(token, accountUrl);
		AccountCustomObject accountCustomObject = null;
		if(accountResult.getResults()!=null &&!accountResult.getResults().isEmpty()) {
			accountCustomObject = accountResult.getResults().get(0);
		}
		return accountCustomObject;
	}

	/**
	 * getAccountByAccountId method is used to get custom objects by accountId
	 * 
	 * @param accountId
	 * @return AccountCustomObject
	 */
	public AccountCustomObject getAccountByAccountId(String accountId) {
		String accountUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=id=\"" + accountId + "\"";
		String token = ctServerHelperService.getStringAccessToken();
		AccountDto accountResult = netConnectionHelper.sendGetAccountObject(token, accountUrl);
		AccountCustomObject accountCustomObject = null;
		if(!accountResult.getResults().isEmpty()) {
			accountCustomObject = accountResult.getResults().get(0);
		}
		return accountCustomObject;
	}

	public void processOrder(String customerId, String holdType, String accountId)   {

		String orderUrl = EMPTY;
		if (Objects.nonNull(customerId)) {
			orderUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
					.append(ctEnvProperties.getProjectKey()).append("/orders?where=customerId=\"").append(customerId)
					.append("\"").toString();
		} else if (Objects.nonNull(accountId)) {
			orderUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
					.append(ctEnvProperties.getProjectKey()).append("/orders?where=custom(fields(accountId=\"")
					.append(accountId).append("\"))").toString();
		}
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResultResponse = netConnectionHelper.sendGetWithoutBody(token, orderUrl);
		JsonObject orderResultJson = MolekuleUtility.parseJsonObject(orderResultResponse);
		if (orderResultJson.get(STATUS_CODE) != null || orderResultJson.get(RESULTS).getAsJsonArray().size() == 0) {
			return;
		}
		JsonArray orders = orderResultJson.get(RESULTS).getAsJsonArray();
		JsonObject jsonObject = null;
		String orderId = null;
		String paymentId = null;
		ResponseEntity<AccountCustomObject> responseEntityAccountCustomObject = checkoutHelperService
				.getAccountCustomObjectApi(accountId, null);
		AccountCustomObject accountCustomObject = responseEntityAccountCustomObject.getBody();
		Value value = accountCustomObject.getValue();
		Boolean isTaxEemptFlag = value.isTaxExemptFlag();
		List<String> shippingAddressIds = new ArrayList<>();
		if (value.getAdditionalFreightInfos() != null) {
			shippingAddressIds = value.getAdditionalFreightInfos().stream()
					.map(AdditionalFreightInfo::getShippingAddressId).collect(Collectors.toList());
		}

		String orderStateUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/states?where=key=\"order-processing\"").toString();
		String orderStateResponse = netConnectionHelper.sendGetWithoutBody(token, orderStateUrl);
		String stateId = null;
		JsonObject orderStateJsonObject = MolekuleUtility.parseJsonObject(orderStateResponse);
		JsonArray orderStateResultsJsonObejct = orderStateJsonObject.get(RESULTS).getAsJsonArray();
		if(orderStateResultsJsonObejct.size() > 0) {
			stateId = orderStateResultsJsonObejct.get(0).getAsJsonObject().get("id").getAsString();
		}

		for (int i = 0; i < orders.size(); i++) {
			String updatedOrderResponse = null;
			jsonObject = orders.get(i).getAsJsonObject();
			orderId = jsonObject.get("id").getAsString();
			paymentId = jsonObject.get("paymentInfo").getAsJsonObject().get("payments").getAsJsonArray().get(0)
					.getAsJsonObject().get("id").getAsString();
			String transitionStateUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()+ "/orders/" + orderId;
			if (jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS) != null
					&& jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(HOLDS)
					&& jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(HOLDS).getAsJsonArray().size() > 0) {
				Map<String, String> values = new HashMap<>();
				values.put(CUSTOMER_ID,customerId );
				values.put("holdType", holdType);
				values.put("token", token);
				updatedOrderResponse = processCustomValue(values, jsonObject, paymentId,
						isTaxEemptFlag, shippingAddressIds, updatedOrderResponse, transitionStateUrl);
			} 
			if(updatedOrderResponse != null) {
				setOrderResponse(token, stateId, updatedOrderResponse, transitionStateUrl);
			}
		}

	}

	private void setOrderResponse(String token, String stateId, String updatedOrderResponse,
			String transitionStateUrl) {
		JsonObject updatedJsonObject = MolekuleUtility.parseJsonObject(updatedOrderResponse);
		if (updatedJsonObject.get(STATUS_CODE) == null && updatedJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS) != null
				&& updatedJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(HOLDS)
				&& updatedJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(HOLDS).getAsJsonArray().size() == 0) {
			String orderState = updatedJsonObject.get("orderState").getAsString();
			if ("Open".equals(orderState)) {
				// set OrderState and orderWorkFlowStatus
				UpdateCartCustomServiceBean updateCartCustomServiceBean = new UpdateCartCustomServiceBean();
				List<UpdateCartCustomServiceBean.Action> updateActionList = new ArrayList<>();
				UpdateCartCustomServiceBean.Action action = new UpdateCartCustomServiceBean.Action();
				action.setActionName("changeOrderState");
				action.setOrderState("Confirmed");
				updateActionList.add(action);
				updateCartCustomServiceBean.setActions(updateActionList);
				updateCartCustomServiceBean.setVersion(updatedJsonObject.get(VERSION).getAsLong());
				String response = (String) netConnectionHelper.sendPostRequest(transitionStateUrl, token,
						updateCartCustomServiceBean);
				updatedJsonObject = MolekuleUtility.parseJsonObject(response);
				if (updatedJsonObject.get(STATUS_CODE) == null && stateId != null) {
					UpdateCartCustomServiceBean updateCartCustomService = new UpdateCartCustomServiceBean();
					List<UpdateCartCustomServiceBean.Action> updateActions = new ArrayList<>();
					UpdateCartCustomServiceBean.Action stateAction = new UpdateCartCustomServiceBean.Action();
					UpdateCartCustomServiceBean.Action.State state = new UpdateCartCustomServiceBean.Action.State();
					state.setId(stateId);
					state.setTypeId("state");
					stateAction.setActionName("transitionState");
					stateAction.setState(state);
					updateActions.add(stateAction);
					updateCartCustomService.setActions(updateActions);
					updateCartCustomService.setVersion(updatedJsonObject.get(VERSION).getAsLong());
					netConnectionHelper.sendPostRequest(transitionStateUrl, token, updateCartCustomService);
				}
			}
		}
	}

	private String processCustomValue(Map<String, String> values, JsonObject jsonObject,
			String paymentId, Boolean isTaxEemptFlag, List<String> shippingAddressIds, String updatedOrderResponse,
			String transitionStateUrl) {
		String customerId =values.get(CUSTOMER_ID);
		String holdType = values.get("holdType");
		String token = values.get("token");

		long version = jsonObject.get(VERSION).getAsLong();
		String paymentUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/payments/"
				+ paymentId;
		String paymentResponse = netConnectionHelper.sendGetWithoutBody(token, paymentUrl);
		JsonObject paymentJsonObject = MolekuleUtility.parseJsonObject(paymentResponse);
		String shippingAddressId = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
		boolean isPaymentPending = false;
		if (jsonObject.has("paymentState"))
			isPaymentPending = ("Pending").equals(jsonObject.get("paymentState").getAsString());
		boolean isACH = ("ACH").equals(
				paymentJsonObject.get("paymentMethodInfo").getAsJsonObject().get("method").getAsString());
		ClearHoldState clearHoldState;
		JsonArray holdsArray = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject()
				.get(HOLDS).getAsJsonArray();
		Set<String> holds = new HashSet<>();
		for (int k = 0; k < holdsArray.size(); k++) {
			holds.add(holdsArray.get(k).getAsString());
		}
		if (FREIGHT_HOLD.equals(holdType) && holds.contains(FREIGHT_HOLD)) {
			if (shippingAddressIds != null && shippingAddressIds.contains(shippingAddressId)) {
				clearHoldState = CtServerHelperService.getTransitionStateObj(version, jsonObject, holdType);
				updatedOrderResponse = (String)netConnectionHelper.sendPostRequest(transitionStateUrl, token, clearHoldState);
				SendGridModel sendGridModel = createEmailDynamicInfo(customerId);
				sendGridModel.setOrderNumber(jsonObject.get(ORDER_NUMBER).getAsString());
				freightInformationConfirmationSender.sendMail(sendGridModel);
				if (isACH && isPaymentPending) {
					sendGridModel.setNonCreditCardPayment(true);
					sendGridModel.setMissingFreightInformation(false);
					orderUpdatedMailSender.sendMail(sendGridModel);
				}
			}
		} else if ("TaxHold".equals(holdType)) {
			if (Boolean.TRUE.equals(isTaxEemptFlag)) {
				clearHoldState = CtServerHelperService.getTransitionStateObj(version, jsonObject, holdType);
				updatedOrderResponse = (String)netConnectionHelper.sendPostRequest(transitionStateUrl, token, clearHoldState);
			}
		} else if ("CreditHold".equals(holdType)) {
			clearHoldState = CtServerHelperService.getTransitionStateObj(version, jsonObject, holdType);
			updatedOrderResponse = (String)netConnectionHelper.sendPostRequest(transitionStateUrl, token, clearHoldState);
		}
		return updatedOrderResponse;
	}

	/**
	 * mappingStringResponseToCustomObject method is used to map string response
	 * into custom object.
	 * 
	 * @param response
	 * @param paymentRequestBean
	 * @return
	 */
	public AccountCustomObject mappingStringResponseToCustomObject(String response) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		AccountCustomObject accountObject = new AccountCustomObject();
		ObjectMapper mapper = new ObjectMapper();
		Value value = new Value();
		try {
			value = mapper.readValue(valueObject.toString(), Value.class);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}

		List<Payments> paymentList = assignCardObjectToPayments(value, null);
		value.setPayments(paymentList);
		accountObject.setValue(value);
		accountObject.setContainer(jsonObject.get(CONTAINER).getAsString());
		accountObject.setKey(jsonObject.get("key").getAsString());
		accountObject.setVersion(jsonObject.get(VERSION).getAsInt());
		return accountObject;
	}

	public String cognitoApiCall(String body, String target) {
		String cognitoBaseUrl = ctEnvProperties.getCognitoDomain();
		WebClient webClient = WebClient.builder().baseUrl(cognitoBaseUrl).build();
		Mono<String> cognitoResponse = webClient.post().accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.parseMediaType(ctEnvProperties.getCognitoHeaderContentType()))
				.header("x-amz-target", target)
				.body(BodyInserters.fromValue(body))
				.exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(String.class);
				});
		return cognitoResponse.block();
	}

	public String setCognitoResponseDetails(CustomerDTO customerDTO, String channel, String password, String accountId, String token,
			String customerId, Long version) {
		ObjectMapper mapper = new ObjectMapper();
		String cognitoStringResponse = null;
		String countryCode = customerDTO.getCountryCode();
		if(null==countryCode || "".equals(countryCode)) {
			countryCode = "US";
		}
		if(null==password) {
			password = ctEnvProperties.getPw0rd();
		}
		List<Map<String, String>> userAttributesList = new ArrayList<>();
		setCognitoUserAttributes(userAttributesList, "email", customerDTO.getEmail());
		setCognitoUserAttributes(userAttributesList, "given_name", customerDTO.getFirstName());
		setCognitoUserAttributes(userAttributesList, "family_name", customerDTO.getLastName());
		if(null != accountId && "" != accountId) {
			setCognitoUserAttributes(userAttributesList, "custom:accountId", accountId);			
		}
		setCognitoUserAttributes(userAttributesList, "custom:customerId", customerId);
		setCognitoUserAttributes(userAttributesList, "custom:country_code", countryCode);
		setCognitoUserAttributes(userAttributesList, "custom:channel", channel);
		try {
			String email = customerDTO.getEmail();
			String clientId = ctEnvProperties.getCognitoClientId();
			Map<String, Object> cognitoSignUpRequest = new HashMap<>();
			cognitoSignUpRequest.put(CLIENT_ID, clientId);
			cognitoSignUpRequest.put("Username", email);
			cognitoSignUpRequest.put("Password", password);
			cognitoSignUpRequest.put("UserAttributes", userAttributesList);
			if(!com.amazonaws.util.StringUtils.isNullOrEmpty(ctEnvProperties.getCognitoClientSecret())) {
				String secretHash = authenticationHelperService.calculateSecretHash(
						clientId, ctEnvProperties.getCognitoClientSecret(), email);
				cognitoSignUpRequest.put(SECRET_HASH, secretHash);
			}
			String jsonString = mapper.writeValueAsString(cognitoSignUpRequest);
			cognitoStringResponse = cognitoApiCall(jsonString, ctEnvProperties.getCognitoHeaderAmzTarget());
		} catch (JsonProcessingException e) {
			logger.error("Error occured when parsing the congnito", e);
			return cognitoStringResponse;
		}
		if (cognitoStringResponse != null) {
			JsonObject cognitoJsonObject = MolekuleUtility.parseJsonObject(cognitoStringResponse);
			if (!cognitoJsonObject.has(__TYPE)) {
				CustomerDTO cognitoCustomerDTO = new CustomerDTO();
				List<CustomerDTO.Action> actions = new ArrayList<>();
				actions.add(setCustomAttributes("cognitoId", cognitoJsonObject.get("UserSub").getAsString()));
				actions.add(setCustomAttributes(CHANNEL, channel));
				cognitoCustomerDTO.setVersion(version);
				cognitoCustomerDTO.setActions(actions);
				String customerUpdateBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
						+ customerId + "/?";
				return (String) netConnectionHelper.sendPostRequest(customerUpdateBaseUrl, token, cognitoCustomerDTO);
			}
		}
		return cognitoStringResponse;
	}

	/**
	 * setCognitoUserAttributes method is used to set user attributes for Cognito.
	 * 
	 * @param userAttributesList
	 * @param name
	 * @param value
	 * @return Map<String, String>
	 */
	private Map<String, String> setCognitoUserAttributes(List<Map<String, String>> userAttributesList, String name,
			String value) {
		Map<String, String> userAttributesMultiValueMap = new HashMap<>();
		userAttributesMultiValueMap.put("Name", name);
		userAttributesMultiValueMap.put("Value", value);
		userAttributesList.add(userAttributesMultiValueMap);
		return userAttributesMultiValueMap;
	}

	private Action setCustomAttributes(String name, Object value) {
		CustomerDTO.Action cognitoIdReference = new CustomerDTO.Action();
		cognitoIdReference.setActionName(SET_CUSTOM_FIELD);
		cognitoIdReference.setName(name);
		cognitoIdReference.setValue(value);
		return cognitoIdReference;
	}

	public String deleteCognitoUser(String accessToken) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> params = new HashMap<>();
		params.put(ACCESS_TOKEN, accessToken);
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			return cognitoApiCall(jsonString, ctEnvProperties.getCognitoHeaderAmzDeleteUser());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			message = new Gson().toJson(e.getMessage());
		}
		ErrorResponse errorResponse = new ErrorResponse(400, message);
		return errorResponse.toString();
	}

	public ResponseEntity<String> forgotPassword(ForgotPasswordRequestBean forgotPasswordRequestBean, String passwordLayout) {
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> params = new HashMap<>();
		params.put(CLIENT_ID, ctEnvProperties.getCognitoClientId());
		params.put("Username", forgotPasswordRequestBean.getEmail());
		if ("createPassword".equals(passwordLayout)) {
			Map<String, Object> clientMetaDataParams = new HashMap<>();
			clientMetaDataParams.put("welcome", "1");
			params.put("ClientMetadata", clientMetaDataParams);
		}
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(params);
			String response = cognitoApiCall(jsonString,
					ctEnvProperties.getCognitoHeaderAmzForgotPasswordTarget());
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			message = new Gson().toJson(e.getMessage());
		}
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);}
}
