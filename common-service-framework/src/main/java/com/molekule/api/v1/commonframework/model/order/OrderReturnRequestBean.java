package com.molekule.api.v1.commonframework.model.order;

import lombok.Data;

@Data
public class OrderReturnRequestBean {
	private String lineItemId;
	private String quantity;
	private String comment;
	private String resolution; 
	private String functionalIssue;
	private String packageCondition;
	private String returnReasons;
	private String returnLocation;
	private String failureSymptoms;
	private String serialNumber;
}
