package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class TaxExemptReview is used to hold tax exempt review data.
 */
public class TaxExemptReview {
	
	private String certificateId;
	private String state;
	private String country;
	private String expiryDate;
	private String taxCertificationId;
	
	public String getCertificateId() {
		return certificateId;
	}
	public void setCertificateId(String certificateId) {
		this.certificateId = certificateId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getTaxCertificationId() {
		return taxCertificationId;
	}
	public void setTaxCertificationId(String taxCertificationId) {
		this.taxCertificationId = taxCertificationId;
	}
	
}
