package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("orderConfirmD2CMailSender")
public class OrderConfirmD2CMailSender extends TemplateMailRequestHelper {

	@Value("${orderConfirmedD2CTemplateId}")
	private String templateId;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setFirstName(sendGridModel.getFirstName());
		dynamicTemplateData.setGift(sendGridModel.isGift());
		dynamicTemplateData.setBillingAddress(true);
		dynamicTemplateData.setShippingDesc(sendGridModel.getOrderShippingMethod());
		dynamicTemplateData.setPromo(true);
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setProductlist(sendGridModel.getProductlist());
		dynamicTemplateData.setSubTotal(sendGridModel.getOrderSummarySubtotal());
		dynamicTemplateData.setDiscount(sendGridModel.getOrderSummaryDiscount());
		dynamicTemplateData.setTax(sendGridModel.getOrderSummaryTax());
		dynamicTemplateData.setTaxNote(sendGridModel.getTaxNote());
		dynamicTemplateData.setShippingCost(sendGridModel.getOrderSummaryShipping());
		dynamicTemplateData.setOrderTotal(sendGridModel.getOrderSummaryTotal());
		dynamicTemplateData.setOrderPaymentMethod(sendGridModel.getOrderPaymentMethod());
		dynamicTemplateData.setOrderShippingMethod(sendGridModel.getOrderShippingMethod());
		dynamicTemplateData.setOrderShippingAddressContactFirstName(sendGridModel.getOrderShippingAddressContactFirstName());
		dynamicTemplateData.setOrderShippingAddressContactLastName(sendGridModel.getOrderShippingAddressContactLastName());
		dynamicTemplateData.setOrderShippingAddressStreet1(sendGridModel.getOrderShippingAddressStreet1());
		dynamicTemplateData.setOrderShippingAddressStreet2(sendGridModel.getOrderShippingAddressStreet2());
		dynamicTemplateData.setOrderShippingAddressCity(sendGridModel.getOrderShippingAddressCity());
		dynamicTemplateData.setOrderShippingAddressState(sendGridModel.getOrderShippingAddressState());
		dynamicTemplateData.setOrderShippingAddressZip(sendGridModel.getOrderShippingAddressZip());
		if(sendGridModel.getOrderShippingAddressPhoneNumber() != null) {
			dynamicTemplateData.setOrderShippingAddressPhoneNumber(sendGridModel.getOrderShippingAddressPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		}
		dynamicTemplateData.setOrderBillingAddressFirstName(sendGridModel.getOrderBillingAddressFirstName());
		dynamicTemplateData.setOrderBillingAddressLastName(sendGridModel.getOrderBillingAddressLastName());
		dynamicTemplateData.setOrderBillingAddressStreet1(sendGridModel.getOrderBillingAddressStreet1());
		dynamicTemplateData.setOrderBillingAddressStreet2(sendGridModel.getOrderBillingAddressStreet2());
		dynamicTemplateData.setOrderBillingAddressCity(sendGridModel.getOrderBillingAddressCity());
		dynamicTemplateData.setOrderBillingAddressState(sendGridModel.getOrderBillingAddressState());
		dynamicTemplateData.setOrderBillingAddressZip(sendGridModel.getOrderBillingAddressZip());
		if(sendGridModel.getOrderBillingAddressPhoneNumber() != null) {
			dynamicTemplateData.setOrderBillingAddressPhoneNumber(sendGridModel.getOrderBillingAddressPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		}
		return dynamicTemplateData;
	}
}
