package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENTS;

public class PaymentInfo {

	@JsonProperty(PAYMENTS)
	private List<String> payments;

	public List<String> getPayments() {
		return payments;
	}

	public void setPayments(List<String> payments) {
		this.payments = payments;
	}

}
