package com.molekule.api.v1.commonframework.model.order;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import lombok.Data;

@Data
public class OrderSummaryResponseBean {
	
	private long limit;
	private long offset;
	private long count;
	private long total;
	@JsonProperty("results")
	private List<OrderSummary> orderSummaryList;
	
	@Data
	public static class OrderSummary{
	private String orderId;
	private String orderNumber;
	private String orderDate;
	private String orderStatus;
	private String orderTotal;
	private Boolean readFlag;
	private String orderState;
	private String email;
	private int lineOfItems;
	private String paymentStatus;
	private String paymentMethod;
	private String shipmentStatus;
	private List<String> holds;
	private String isGiftOrder;
	private String recipientsEmail;
	private AddressRequestBean shippingAddress;
	private String channel;
	private String store;
	private String trackingId;
	}
}
