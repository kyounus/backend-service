package com.molekule.api.v1.commonframework.dto.registration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StoreKeyReference {

	@JsonProperty("typeId")
	private String typeId;

	@JsonProperty("key")
	private String key;

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
