package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * The class CompanyInfoResponseBean is used to hold the response of company information.
 */
@ApiModel(description = "Company info api response model")
public class CompanyInfoResponseBean {
	private String numberOfEmployees;
	private String numberOfOfficeSpaces;
	private Boolean taxExempt;
	private List<String> taxCertificateList;
	private String accountId;
	private String customerId;
	private Integer customerNumber;
	
	public String getNumberOfEmployees() {
		return numberOfEmployees;
	}
	public void setNumberOfEmployees(String numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}
	public String getNumberOfOfficeSpaces() {
		return numberOfOfficeSpaces;
	}
	public void setNumberOfOfficeSpaces(String numberOfOfficeSpaces) {
		this.numberOfOfficeSpaces = numberOfOfficeSpaces;
	}
	public Boolean getTaxExempt() {
		return taxExempt;
	}
	public void setTaxExempt(Boolean taxExempt) {
		this.taxExempt = taxExempt;
	}
	public List<String> getTaxCertificateList() {
		return taxCertificateList;
	}
	public void setTaxCertificateList(List<String> taxCertificateList) {
		this.taxCertificateList = taxCertificateList;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public Integer getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(Integer customerNumber) {
		this.customerNumber = customerNumber;
	}
}
