package com.molekule.api.v1.commonframework.util;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public class CustomRunTimeException extends RuntimeException {

	private static final long serialVersionUID = 5888935795556837224L;

	public CustomRunTimeException(String errorMessage) {
		super(errorMessage);
	}

	public CustomRunTimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomRunTimeException(Throwable cause) {
		super(cause);
	}

}
