package com.molekule.api.v1.commonframework.model.registration;

import io.swagger.annotations.ApiModelProperty;

/**
 * The class MolekuleCarrier is used to hold the Molekule carrier while doing registration for shipping.
 */
public class MolekuleCarrier {
	@ApiModelProperty(required = true)
	private String deliveryType;

	@ApiModelProperty(required = true)
	private String carrierName;

	@ApiModelProperty(required = true)
	private String carrierAccountNumber;

	@ApiModelProperty(required = true)
	private BillingAddressRequestBean billingAddress;

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public BillingAddressRequestBean getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddressRequestBean billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getCarrierAccountNumber() {
		return carrierAccountNumber;
	}

	public void setCarrierAccountNumber(String carrierAccountNumber) {
		this.carrierAccountNumber = carrierAccountNumber;
	}
}
