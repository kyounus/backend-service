package com.molekule.api.v1.commonframework.dto.registration;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
/**
 * NonProfitDocumentProofDto is contains the details of non profit document.
 *
 */
@Data
public class NonProfitDocumentProofDto {
	@JsonProperty("id")
	private String id;

	@JsonProperty("location")
	private String location;
}
