package com.molekule.api.v1.commonframework.model.registration;

import java.util.ArrayList;
import java.util.List;

public class ShippingCarrierRelationShip {
	
	private String shippingAddressId;
	private List<String> carrierIds;
	public String getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(String shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	public List<String> getCarrierIds() {
		if(null == carrierIds) {
			return new ArrayList<>();
		}
		return carrierIds;
	}
	public void setCarrierIds(List<String> carrierIds) {
		this.carrierIds = carrierIds;
	}
}
