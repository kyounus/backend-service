package com.molekule.api.v1.commonframework.dto.cart;

import lombok.Data;

@Data
public class CreateCartDTO {

	private String customerId;
	private String tealiumVisitorId;
	private String customerEmail;
	private String taxMode;
	private String currency;
	private String anonymousId;
	private Custom custom;
    private Store store;
    private String country;
       private CustomerGroup customerGroup;
   @Data
    public static class Store{
    	private String id;
    }
   @Data
    public static class Custom{
    	private Type type;
    	private Fields fields;
    	@Data
    	public static class Type{
    		private String key;
    	}
    	@Data
    	public static class Fields{
    		private String accountId;
    	}
    	
    }
	@Data
	public static class CustomerGroup {
		private String id;
		private String name;

	}

	
}
