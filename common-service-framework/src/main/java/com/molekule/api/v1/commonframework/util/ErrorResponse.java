package com.molekule.api.v1.commonframework.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ErrorResponse {

	
	private int statusCode;
	private String errorMessage;

	@Override
	public String toString() {
		return "{\r\n"
				+ "    \"statusCode\":"+statusCode+",\r\n"
				+ "    \"errorMessage\":\""+errorMessage+"\"\r\n"
				+ "\r\n"
				+ "}";
	}

	
}
