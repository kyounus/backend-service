package com.molekule.api.v1.commonframework.service.tealium;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ADDRESSES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ANONYMOUSID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.B2B;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM_OBJECTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNT_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNT_CODES;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EN_GB;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EN_US;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.HANDLING_COST;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.NEXT_ORDER_DATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_NUMBER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ORDER_TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PARENT_ORDER_REFERENCE_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PERIOD;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_ADDRESS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SHIPPING_INFO;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SUBSCRIPTION_ENABLED;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SUB_TOTAL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.TOTAL_DISCOUNT_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VARIANT;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.service.cart.CartHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.commonframework.util.TealiumDataBean;
import com.molekule.api.v1.commonframework.util.TealiumUtility;
@Service("tealiumHelperService")
public class TealiumHelperService {

	Logger logger = LoggerFactory.getLogger(TealiumHelperService.class);

	@Autowired
	CTEnvProperties ctEnvProperties;

	@org.springframework.beans.factory.annotation.Value("${stripeApiKey}")
	private String stripeAuthKey;

	@org.springframework.beans.factory.annotation.Value("${paymentIntentHost}")
	private String paymentIntentUrl;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("cartHelperService")
	CartHelperService cartHelperService;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;


	public static final String E_COMMERCE = "Ecommerce";

	public static final String MOLEKULE = "Molekule";

	public static final String CUSTOMER_MAIL = "customerEmail";

	public static final String LINE_ITEMS = "lineItems";

	public static final String CENTAMOUNT = "centAmount";
	
	@Async("TealiumThreadPoolTaskExecutorBean")
	public void setCustomerInfo(String cartResponse) {
		
		JsonObject cartObject = MolekuleUtility.parseJsonObject(cartResponse);

		if(cartObject.has("error") || cartObject.has("statusCode") || cartObject.has("status")) {
			return;
		}

		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("checkout_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Customer Info Step");
		tealiumData.setPageType("Checkout");
		tealiumData.setPageUrl("/D2C/checkout/");
		if(cartObject.has(CUSTOMER_ID)) {
			tealiumData.setCustomerUid(cartObject.get(CUSTOMER_ID).getAsString());
			tealiumData.setCustomerId(cartObject.get(CUSTOMER_ID).getAsString());
			tealiumData.setCustomerEmail(cartObject.get(CUSTOMER_MAIL).getAsString());
		}else {
			tealiumData.setCustomerUid(cartObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
			tealiumData.setCustomerId(cartObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
			tealiumData.setCustomerEmail("Guest user");			
		}
		tealiumData.setCustomerEmailPreference(Boolean.TRUE);
		JsonArray jsonArray = cartObject.get(LINE_ITEMS).getAsJsonArray();
		int lineItems=jsonArray.size();
		List<String> productIdList = new ArrayList<>();
		List<Integer> productQuantity = new ArrayList<>();
		List<String> productSkuList = new ArrayList<>();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> productTypeList = new ArrayList<>();
		List<String> productBrandList = new ArrayList<>();
		List<String> productPromoCodeList = new ArrayList<>();
		BigDecimal[] productDiscount = new BigDecimal[lineItems];
		int totalItems =0;
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			productIdList.add(lineItemObject.get(PRODUCT_ID).getAsString());
			if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("sku")) {
				productSkuList.add(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());				
			}
			productQuantity.add(lineItemObject.get(QUANTITY).getAsInt());
			totalItems = totalItems + productQuantity.get(i);
			
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			productBrandList.add(MOLEKULE);
			productTypeList.add(getProductType(lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("id").getAsString()));
			JsonObject priceObject = lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject();
			productPrice[i]=TealiumUtility.convertToBigDecimal(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("prices")&&lineItemObject.get(VARIANT).getAsJsonObject().get("prices").getAsJsonArray().size()>0) {
				priceObject = lineItemObject.get(VARIANT).getAsJsonObject().get("prices").getAsJsonArray().get(0).getAsJsonObject();	
			}
			if(priceObject.has(DISCOUNTED)) {
				String id =priceObject.get(DISCOUNTED).getAsJsonObject().get("discount").getAsJsonObject().get("id").getAsString();
				String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/product-discounts/"+ id;
				JsonObject productObject = MolekuleUtility.parseJsonObject(getResponse(productUrl));
				if(productObject.get("name").getAsJsonObject().get("en-GB") != null) {
					productPromoCodeList.add(productObject.get("name").getAsJsonObject().get("en-GB").getAsString());
				} else if(productObject.get("name").getAsJsonObject().get(EN_US) != null) {
					productPromoCodeList.add(productObject.get("name").getAsJsonObject().get(EN_US).getAsString());
				}
				productDiscount[i] = TealiumUtility.convertToBigDecimal(priceObject.get(DISCOUNTED).getAsJsonObject().get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			}
		}
		tealiumData.setProductBrand(productBrandList);
		tealiumData.setProductName(productNameList);
		tealiumData.setProductCategory(productTypeList);
		tealiumData.setProductId(productIdList);
		tealiumData.setProductSku(productSkuList);
		tealiumData.setProductQuantity(productQuantity);
		tealiumData.setProductPrice(productPrice);
		tealiumData.setProductListPrice(productPrice);
		tealiumData.setProductPromoCode(productPromoCodeList);
		tealiumData.setProductDiscountAmount(productDiscount);
		tealiumData.setProductImageUrl(new ArrayList<String>());
		tealiumData.setCartProductId(productIdList);
		tealiumData.setCartProductQuantity(productQuantity);
		tealiumData.setCartProductSku(productSkuList);
		tealiumData.setCartProductListPrice(productPrice);
		tealiumData.setCartProductPrice(productPrice);
		JsonObject customObject =cartObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		tealiumData.setCartTotalValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setCartTotalItems(totalItems);
		tealiumData.setCartUrl(new ArrayList<String>());
		tealiumData.setOfferName("");
		tealiumData.setSessionId("");
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in quickRegister", e);
		}
	}

	/**
	 * getProductType is used to get product type by passing productId
	 * 	
	 * @param productId
	 * @return
	 */
	private String getProductType(String productId) {
		String token = ctServerHelperService.getStringAccessToken();
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/product-types/").append(productId).toString();
		JsonObject jsonObject= MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(token, url));

		return jsonObject.get("name").getAsString();
	}

	/**
	 * getProductPromoCode is used to get the product promocode
	 * 
	 * @param discountId
	 * @return
	 */
	private String getProductPromoCode(String discountId) {
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/discount-codes/").append(discountId).toString();
		JsonObject jsonObject= MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(), url));
		return jsonObject.get("code").getAsString();
	}


	/**
	 * getResponse method is used to get response for given Url.
	 * 
	 * @return ResponseEntity<String>
	 */
	public String getResponse(String baseUrl) {
		String token = ctServerHelperService.getStringAccessToken();
		return netConnectionHelper.sendGetWithoutBody(token, baseUrl);

	}

	/**
	 * setProductDetails is to set product details for the event
	 * 
	 * @param orderResponse
	 * @param tealiumData
	 * @return
	 */
	public void setProductDetails(JsonObject jsonObject, TealiumDataBean tealiumData) {
		tealiumData.setCustomerId(jsonObject.has(CUSTOMER_ID) ? jsonObject.get(CUSTOMER_ID).getAsString() : jsonObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
		tealiumData.setCustomerUid(tealiumData.getCustomerId());
		tealiumData.setCustomerEmail(jsonObject.get(CUSTOMER_MAIL).getAsString());
		tealiumData.setOrderId(jsonObject.get(ID).getAsString());
		tealiumData.setOrderNumber(jsonObject.get(ORDER_NUMBER).getAsString());
		tealiumData.setOrderIdIncrement(jsonObject.get(ORDER_NUMBER).getAsString());
		tealiumData.setOrderCurrencyCode(jsonObject.get(MolekuleConstant.TOTAL_PRICE).getAsJsonObject()
				.get(MolekuleConstant.CURRENCY_CODE).getAsString());
		if(jsonObject.has(SHIPPING_INFO))
			tealiumData.setOrderShippingType(jsonObject.get(SHIPPING_INFO).getAsJsonObject()
					.get(MolekuleConstant.SHIPPING_METHOD_NAME).getAsString());

		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		tealiumData.setOrderShippingAmount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get("shippingCost").getAsString())));
		tealiumData.setEventValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
		tealiumData.setOrderGrandTotal(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setOrderSubtotal(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
		tealiumData.setCartTotalValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setOrderPromoAmount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(MolekuleConstant.TOTAL_DISCOUNT_PRICE).getAsString())));
		tealiumData.setOrderSubtotalAfterPromo(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setEmailPreferences(true);
		tealiumData.setGiftPurchase(false);
		if(customObject.has("gift")) {
			tealiumData.setGiftPurchase(customObject.get("gift").getAsBoolean());
		}
		String cartid=jsonObject.get("cart").getAsJsonObject().get(ID).getAsString();
		tealiumData.setCartId(cartid);
		setAddressess(tealiumData, jsonObject);
		getProductInformation(jsonObject, tealiumData);
		tealiumData.setOfferName(tealiumData.getProductPromoCode().isEmpty() ? "" : tealiumData.getProductPromoCode().get(0));
		
	}

	public void processCustomLineItemObject(TealiumDataBean tealiumData, boolean isPurchaseData, boolean[] autofill,
			int i, JsonObject lineItemObject, List<Integer> filterPeriod) {
		if(lineItemObject.has(CUSTOM)&&(lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has(SUBSCRIPTION_ENABLED))) {
			autofill[i] = lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(SUBSCRIPTION_ENABLED).getAsBoolean();
			lineItemObject.get(VARIANT).getAsJsonObject().get("attributes").getAsJsonArray().forEach(attribute->{
				String attributeName = attribute.getAsJsonObject().get("name").getAsString();
				if(attributeName.equals(PERIOD)) {
					filterPeriod.add(attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsInt());
				}
			});
			tealiumData.setFilterFrequency(filterPeriod);
			if(isPurchaseData) {
				validatePurchaseDate(tealiumData);
			}
		}
	}

	private void validatePurchaseDate(TealiumDataBean tealiumData) {
		String baseUrl = ctEnvProperties.getHost()+ "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS 
				+ "?expand=value.cart.id&expand=value.customer.id&where=value(customer(id=\"" +tealiumData.getCustomerId()+ "\"))";
		String subscriptionResponse = getResponse(baseUrl);
		JsonObject subscriptionObject = MolekuleUtility.parseJsonObject(subscriptionResponse);
		if(subscriptionObject.getAsJsonArray(RESULTS) != null) {
			JsonArray resultsArray = subscriptionObject.get(RESULTS).getAsJsonArray();
			for(int j =0;j<resultsArray.size();j++) {
				JsonObject valueJsonObject =  resultsArray.get(j).getAsJsonObject().get(VALUE).getAsJsonObject();
				String orderId = valueJsonObject.get("cart").getAsJsonObject().get("obj").getAsJsonObject()
						.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(PARENT_ORDER_REFERENCE_ID).getAsJsonObject().get("id").getAsString();					
				if(orderId.equals(tealiumData.getOrderId())) {
					JsonObject cartObject = valueJsonObject.getAsJsonObject().get("cart").getAsJsonObject().get("obj").getAsJsonObject();
					JsonArray cartArray = cartObject.get(LINE_ITEMS).getAsJsonArray();
					List<BigDecimal> filterPrice = new ArrayList<>();
					filterPrice.add(TealiumUtility.convertToBigDecimal(cartArray.get(0).getAsJsonObject().get(MolekuleConstant.PRICE).getAsJsonObject()
							.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt()));	
					List<String> refillDate = new ArrayList<>();
					refillDate.add(valueJsonObject.get(NEXT_ORDER_DATE).getAsString());
				}
			}
			List<String> subscriptionList = new ArrayList<>();
			subscriptionList.add(subscriptionObject.get("id").getAsString());
			tealiumData.setSubscriptionId(subscriptionList);	
		}
	}

	public TealiumDataBean setAddressess(TealiumDataBean tealiumData, JsonObject jsonObject) {
		//Save shipping address to tealium data
		JsonObject addressObject; 
		if(jsonObject.has(SHIPPING_ADDRESS)) {
			addressObject =jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject();
			tealiumData.setCustomerFirstNameShipping(addressObject.get(MolekuleConstant.FIRST_NAME).getAsString());
			tealiumData.setCustomerLastNameShipping(addressObject.get(MolekuleConstant.LAST_NAME).getAsString());
			tealiumData.setCustomerCityShipping(addressObject.get("city").getAsString());
			tealiumData.setCustomerCountryShipping(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
			tealiumData.setCustomerCountryCodeShipping(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
			tealiumData.setCustomerStateShipping(addressObject.get(MolekuleConstant.STATE).getAsString());
			tealiumData.setCustomerZipShipping(addressObject.get(MolekuleConstant.POSTAL_CODE).getAsString());
			tealiumData.setCustomerAddress1Shipping(addressObject.get("streetName").getAsString());	
		}
		//Save billing Address to the tealium data
		if(jsonObject.has("billingAddress")) {
			addressObject =jsonObject.get("billingAddress").getAsJsonObject();
			tealiumData.setCustomerFirstNameBilling(addressObject.get(MolekuleConstant.FIRST_NAME).getAsString());
			tealiumData.setCustomerLastNameBilling(addressObject.get(MolekuleConstant.LAST_NAME).getAsString());
			tealiumData.setCustomerCityBilling(addressObject.get("city").getAsString());
			tealiumData.setCustomerCountryBilling(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
			tealiumData.setCustomerCountryCodeBilling(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
			tealiumData.setCustomerStateBilling(addressObject.get(MolekuleConstant.STATE).getAsString());
			tealiumData.setCustomerZipBilling(addressObject.get(MolekuleConstant.POSTAL_CODE).getAsString());
			tealiumData.setCustomerAddress1Billing(addressObject.get("streetName").getAsString());
			
		}
		return tealiumData;

	}


	/**
	 * setTurnOffAutorenewal is to send Turn-off Refill event to tealium
	 * 
	 * @param questionId
	 * @param subscriptionUrl
	 * @return
	 */
	public void setTurnOffAutorenewal(String questionId,String subscriptionUrl) {
		
	TealiumDataBean tealiumData = new TealiumDataBean();
	tealiumData.setTealiumEvent("auto_renew_off_api");
	tealiumData.setEventCategory("Account");
	tealiumData.setEventAction("Turned off Auto-Renew");
	tealiumData.setEventPlatform("Account");
	tealiumData.setPageType("Turn-off AutoRefill Subscription");
	
	String subscriptionResponse = getResponse(subscriptionUrl);
	JsonObject jsonObject = MolekuleUtility.parseJsonObject(subscriptionResponse);
	JsonArray reasonArray=jsonObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("transactions").getAsJsonArray();
	for(int i=0;i<reasonArray.size();i++) {			
		JsonObject resultObject = reasonArray.get(i).getAsJsonObject();
		if(resultObject.get("type").getAsString().equals("cancelled")) {
			tealiumData.setEventLabel(resultObject.get("messages").getAsString());
			break;
		}
	}
	List<String> subscription = new ArrayList<>();
	List<String> refillDate = new ArrayList<>();
	subscription.add(jsonObject.get("id").getAsString());
	JsonObject cartObject = jsonObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("obj").getAsJsonObject();
	refillDate.add(jsonObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(NEXT_ORDER_DATE).getAsString());
	setCustomerDetails(cartObject,tealiumData);
	setAddressess(tealiumData, cartObject);
	setRefillCartDetails(tealiumData, cartObject);
	tealiumData.setFilterRefillEndDate(refillDate.get(0));
	tealiumData.setFilterRefillEndDatePrevious(refillDate);
	tealiumData.setSubscriptionId(subscription);
	tealiumData.setAutoRefill(new boolean[] {false});
	tealiumData.setPageUrl("/users/"+tealiumData.getCustomerId()+"/subscriptions");
	try {
		 TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
	} catch (JsonProcessingException e) {
		logger.error("Error occured when JSON Processing in setTurnOffAutorenewal", e);
	}
	
}
	private void setRefillCartDetails(TealiumDataBean tealiumData, JsonObject cartObject) {
		JsonArray jsonArray = cartObject.get(LINE_ITEMS).getAsJsonArray();
		int lineItems=jsonArray.size();
		List<String> productIdList = new ArrayList<>();
		List<Integer> productQuantity = new ArrayList<>();
		List<String> productSkuList = new ArrayList<>();
		List<String> filterPrice = new ArrayList<>();
		List<String> serialList = new ArrayList<>();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> productTypeList = new ArrayList<>();
		List<String> productBrandList = new ArrayList<>();
		int totalItems =0;
		int totalValue = 0;
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			productIdList.add(lineItemObject.get(PRODUCT_ID).getAsString());
			if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("sku")) {
				productSkuList.add(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());				
			}
			productQuantity.add(lineItemObject.get(QUANTITY).getAsInt());
			totalItems = totalItems + productQuantity.get(i);
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			productBrandList.add(MOLEKULE);
			productTypeList.add(getProductType(lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("id").getAsString()));
			JsonObject priceObject = lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject();
			productPrice[i]=TealiumUtility.convertToBigDecimal(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			tealiumData.setOrderCurrencyCode(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(MolekuleConstant.CURRENCY_CODE).getAsString());
			totalValue = totalItems * priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt();
			filterPrice.add(productPrice[i].toString());
			if(lineItemObject.has(CUSTOM)&&lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has("serialNumbers")) {
				serialList.add(lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject()
						.get("serialNumbers").getAsJsonArray().get(0).getAsString());
			}	
		}
		tealiumData.setSerialNumber(serialList);
		tealiumData.setCartProductId(productIdList);
		tealiumData.setCartProductQuantity(productQuantity);
		tealiumData.setCartProductSku(productSkuList);
		tealiumData.setCartProductListPrice(productPrice);
		tealiumData.setCartProductPrice(productPrice);
		if(productPrice.length > 0) {
			tealiumData.setFilterPlanPrice(productPrice[0].toString());
		}
		tealiumData.setFilterNextChargeAmount(filterPrice);
		JsonObject customObject =cartObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		List<Integer> filterPeriod = new ArrayList<>();
		filterPeriod.add(customObject.get("period").getAsInt());
		tealiumData.setFilterFrequency(filterPeriod);
		
		tealiumData.setCartTotalValue(TealiumUtility.convertToBigDecimal(totalValue));
		tealiumData.setCartTotalItems(totalItems);
		tealiumData.setCartUrl(new ArrayList<String>());
		tealiumData.setOfferName("");
		tealiumData.setSessionId("");
		tealiumData.setDeviceId("Neet to set");
		tealiumData.setDeviceName("Neet to set");
	}


	private void setCustomerDetails(JsonObject cartObject, TealiumDataBean tealiumData) {
		if(cartObject.has(CUSTOMER_ID)) {
			tealiumData.setCustomerUid(cartObject.get(CUSTOMER_ID).getAsString());
			tealiumData.setCustomerId(cartObject.get(CUSTOMER_ID).getAsString());
			tealiumData.setCustomerEmail(cartObject.get(CUSTOMER_MAIL).getAsString());
		}else {
			tealiumData.setCustomerUid(cartObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
			tealiumData.setCustomerId(cartObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
			tealiumData.setCustomerEmail("Guest user");			
		}
	}

	/**
	 * getActivateAutoRefills is to track subscription object creation success
	 * 
	 * @param messageObject
	 */

	public void getActivateAutoRefills(JsonObject messageObject) {
		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("activate_auto_refills_success_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Activated Auto-Refills");
		List<String> subscriptionList = new ArrayList<>();
		subscriptionList.add(messageObject.get("id").getAsString());
		tealiumData.setSubscriptionId(subscriptionList);
		if(messageObject.has(MolekuleConstant.VALUE)) {
			JsonObject cartObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("obj").getAsJsonObject();
			setAddressess(tealiumData, cartObject);
			//add line items to the tealium data
			setRefillCartDetails(tealiumData, cartObject);		
			int filterPeriod =cartObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(PERIOD).getAsInt();
			tealiumData.setFilterFrequency(Arrays.asList(filterPeriod));
			List<String> refillDate = new ArrayList<>();

			refillDate.add(messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(NEXT_ORDER_DATE).getAsString());
			tealiumData.setNextShipmentDate(refillDate);
			tealiumData.setAutoRefill(new boolean[] {messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("subscription").getAsBoolean()});
			tealiumData.setFilterRefillEndDatePrevious(new ArrayList<>());
			tealiumData.setFreeRefillEligible(true);
			JsonObject customerObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CUSTOMER).getAsJsonObject().get("obj").getAsJsonObject();
			tealiumData.setCustomerUid(customerObject.get("id").getAsString());
			tealiumData.setCustomerId(customerObject.get("id").getAsString());
			tealiumData.setCustomerEmail(customerObject.get(MolekuleConstant.EMAIL).getAsString());
		}
		tealiumData.setSessionId(MolekuleUtility.createUuidValue());

		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Error occured when JSON Processing in getActivateAutoRefills", e);

		}

	}
	
	public void getAutorefillRenewalstate(JsonObject messageObject) {
		if(messageObject.has(MolekuleConstant.VALUE)) {
		String state = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("state").getAsString();	
		JsonArray reasonArray=messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("transactions").getAsJsonArray();
		int attempt = 1;
		String errorMessage = null;
		for(int i=0;i<reasonArray.size();i++) {			
			JsonObject resultObject = reasonArray.get(i).getAsJsonObject();
			if(resultObject.get("type").getAsString().equals("failed")) {
				attempt = attempt + 1;
				errorMessage = resultObject.get("messages").getAsString();
			}
		}
		if(state.equals("Processing")) {
			setAutoRefillrenewalAttempts(messageObject, String.valueOf(attempt));
		}else if(state.equals("Processed")) {
			setAutoRefillrenewalSuccess(messageObject, String.valueOf(attempt));
		}else if(state.equals("Retry-Scheduled")) {
			setAutoRefillrenewalFailure(messageObject, String.valueOf(attempt), errorMessage);
		}
		}
	}
	
	public void setAutoRefillrenewalAttempts(JsonObject messageObject, String attemptNumber) {
		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("auto_renewal_charge_attempt_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Auto Renewal Charge Attempt");
		tealiumData.setAttemptNumber(attemptNumber);
		List<String> subscriptionList = new ArrayList<>();
		subscriptionList.add(messageObject.get("id").getAsString());
		tealiumData.setSubscriptionId(subscriptionList);
		if(messageObject.has(MolekuleConstant.VALUE)) {
			JsonObject cartObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("obj").getAsJsonObject();
			setAddressess(tealiumData, cartObject);
			//add line items to the tealium data
			setRefillCartDetails(tealiumData, cartObject);		
					tealiumData.setAutoRefill(new boolean[] {messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("subscription").getAsBoolean()});
			tealiumData.setFreeRefillEligible(true);
			JsonObject customerObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CUSTOMER).getAsJsonObject().get("obj").getAsJsonObject();
			tealiumData.setCustomerUid(customerObject.get("id").getAsString());
			tealiumData.setCustomerId(customerObject.get("id").getAsString());
			tealiumData.setCustomerEmail(customerObject.get(MolekuleConstant.EMAIL).getAsString());
		}
		tealiumData.setSessionId(MolekuleUtility.createUuidValue());

		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Error occured when JSON Processing in getActivateAutoRefills", e);

		}

	}
	
	public void setAutoRefillrenewalFailure(JsonObject messageObject, String attemptNumber, String errorMessage) {
		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("purchase_failed_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Auto Renewal Charge Failed");
		tealiumData.setAttemptNumber(attemptNumber);
		List<String> subscriptionList = new ArrayList<>();
		subscriptionList.add(messageObject.get("id").getAsString());
		tealiumData.setSubscriptionId(subscriptionList);
		if(messageObject.has(MolekuleConstant.VALUE)) {
			JsonObject cartObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("obj").getAsJsonObject();
			String orderId = cartObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(PARENT_ORDER_REFERENCE_ID).getAsJsonObject().get("id").getAsString();		
			setOrderDetails(orderId, tealiumData, false);
			tealiumData.setAutoRefill(new boolean[] {messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("subscription").getAsBoolean()});
			tealiumData.setFreeRefillEligible(true);
			JsonObject customerObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CUSTOMER).getAsJsonObject().get("obj").getAsJsonObject();
			tealiumData.setCustomerUid(customerObject.get("id").getAsString());
			tealiumData.setCustomerId(customerObject.get("id").getAsString());
			tealiumData.setCustomerEmail(customerObject.get(MolekuleConstant.EMAIL).getAsString());
		}
		tealiumData.setSessionId(MolekuleUtility.createUuidValue());
		tealiumData.setEventLabel(errorMessage);
		tealiumData.setErrorMessage(errorMessage);
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Error occured when JSON Processing in getActivateAutoRefills", e);

		}

	}


	public void setAutoRefillrenewalSuccess(JsonObject messageObject, String attemptNumber) {
		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("purchase_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Auto Renewal Charge Success");
		tealiumData.setAttemptNumber(attemptNumber);
		List<String> subscriptionList = new ArrayList<>();
		subscriptionList.add(messageObject.get("id").getAsString());
		tealiumData.setSubscriptionId(subscriptionList);
		if(messageObject.has(MolekuleConstant.VALUE)) {
			JsonObject cartObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("cart").getAsJsonObject().get("obj").getAsJsonObject();
			String orderId = cartObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(PARENT_ORDER_REFERENCE_ID).getAsJsonObject().get("id").getAsString();		
			setOrderDetails(orderId, tealiumData, true);
			tealiumData.setAutoRefill(new boolean[] {messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("subscription").getAsBoolean()});
			tealiumData.setFreeRefillEligible(true);
			JsonObject customerObject = messageObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CUSTOMER).getAsJsonObject().get("obj").getAsJsonObject();
			tealiumData.setCustomerUid(customerObject.get("id").getAsString());
			tealiumData.setCustomerId(customerObject.get("id").getAsString());
			tealiumData.setCustomerEmail(customerObject.get(MolekuleConstant.EMAIL).getAsString());
		}
		tealiumData.setSessionId(MolekuleUtility.createUuidValue());

		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Error occured when JSON Processing in getActivateAutoRefills", e);

		}

	}

	public void setOrderDetails(String orderId, TealiumDataBean tealiumData, boolean flag) {
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders/"+orderId;
		String response = getResponse(baseUrl);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		tealiumData.setOrderId(jsonObject.get("id").getAsString());
		tealiumData.setOrderNumber(jsonObject.get(ORDER_NUMBER).getAsString());
		tealiumData.setOrderCurrencyCode(jsonObject.get(MolekuleConstant.TOTAL_PRICE).getAsJsonObject()
				.get(MolekuleConstant.CURRENCY_CODE).getAsString());
		tealiumData.setOrderShippingType(jsonObject.get(SHIPPING_INFO).getAsJsonObject()
				.get(MolekuleConstant.SHIPPING_METHOD_NAME).getAsString());
		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		tealiumData.setOrderShippingAmount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get("shippingCost").getAsString())));
		tealiumData.setOrderGrandTotal(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setOrderSubtotal(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
		if(flag) {
			tealiumData.setEventValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
			if(customObject.has("recipientsEmail"))	{
				tealiumData.setGiftEmailAddress(customObject.get("recipientsEmail").getAsString());
			}
		}
		tealiumData.setCartTotalValue(tealiumData.getOrderSubtotal());
		setAddressess(tealiumData, jsonObject);
		tealiumData.setOrderPromoCode("");
		tealiumData.setOrderDiscount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get("totalDiscountPrice").getAsString())));
		tealiumData.setPaymentOnFile(Arrays.asList(true));
		
		setPaymentType(jsonObject, tealiumData);
		getProductInformation(jsonObject,tealiumData);
		
		tealiumData.setCartProductId(tealiumData.getProductId());
		tealiumData.setCartProductQuantity(tealiumData.getProductQuantity());
		tealiumData.setCartProductSku(tealiumData.getProductSku());
		tealiumData.setCartProductListPrice(tealiumData.getProductPrice());
		tealiumData.setCartProductPrice(tealiumData.getProductPrice());
	}
	public void setPaymentType(JsonObject jsonObject ,TealiumDataBean tealiumData ) {
		String paymentId = jsonObject.get("paymentInfo").getAsJsonObject().get("payments").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString();
		String paymentUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/payments/" + paymentId;
		String paymentResponse = getResponse(paymentUrl);
		JsonObject paymentJsonObject = MolekuleUtility.parseJsonObject(paymentResponse);
		String paymentType = paymentJsonObject.get("paymentMethodInfo").getAsJsonObject().get("method").getAsString();
		tealiumData.setPaymentMethod(paymentType);
		if(paymentType.equals("CREDIT_CARD")) {
			String authToken = new StringBuilder(BEARER).append(stripeAuthKey).toString();
			String paymentIntentBaseUrl = paymentIntentUrl + "/" + paymentJsonObject.get("interfaceId").getAsString();
			String paymentIntentResponse =netConnectionHelper.sendGetWithoutBody(authToken, paymentIntentBaseUrl);
			JsonObject paymentIntentObject = MolekuleUtility.parseJsonObject(paymentIntentResponse);
			tealiumData.setCcLast4Digits(paymentIntentObject.get("charges").getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject()
					.get("payment_method_details").getAsJsonObject().get("card").getAsJsonObject().get("last4").getAsString());
		}
	}
	
	public String setTransactionSuccess(JsonObject jsonObject,HttpServletRequest request) {
		TealiumDataBean tealiumData= new TealiumDataBean();
		tealiumData.setTealiumEvent("purchase_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Completed Transaction");
		setProductDetails(jsonObject, tealiumData);
		setPaymentType(jsonObject, tealiumData);
		tealiumData.setSessionId(request.getRequestedSessionId());
		try {
			return TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Error occured when JSON Processing in setTransactionSuccess", e);
		}

		return null;
	}

	public void setQuoteInformation(String updatedCartResponse) {
		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("request_quote_completed_api");
		tealiumData.setEventCategory("B2B Request Quote");
		tealiumData.setEventAction("Completed Request Quote");
		JsonObject cartJsonObject= MolekuleUtility.parseJsonObject(updatedCartResponse);
		JsonArray cartLineItemJsonArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		int lineItems=cartLineItemJsonArray.size();
		List<String> productIdList = new ArrayList<>();
		List<Integer> productQuantityList = new ArrayList<>();
		List<String> productSkuList = new ArrayList<>();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> productType = new ArrayList<>();
		List<String> productBrand = new ArrayList<>();
		BigDecimal[] discountAmount= new BigDecimal[lineItems];
		List<String> promoCodesList=new ArrayList<>();
		JsonArray discountArray = cartJsonObject.get(DISCOUNT_CODES).getAsJsonArray();
		for(int i=0;i<discountArray.size();i++) {
			String code=discountArray.get(i).getAsJsonObject().get(DISCOUNT_CODE).getAsJsonObject().get("id").getAsString();
			promoCodesList.add(getProductPromoCode(code));
		}
		int totalItems =0;
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =cartLineItemJsonArray.get(i).getAsJsonObject();
			productIdList.add(lineItemObject.get(PRODUCT_ID).getAsString());
			if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("sku")) {
				productSkuList.add(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());				
			}
			productQuantityList.add(lineItemObject.get(QUANTITY).getAsInt());
			totalItems = totalItems + productQuantityList.get(i);
			productPrice[i]=TealiumUtility.convertToBigDecimal(lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject()
					.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(EN_US).getAsString());
			productBrand.add(MOLEKULE);
			productType.add(getProductType(lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("id").getAsString()));
			if(lineItemObject.has(DISCOUNTED_PRICE)) {
				discountAmount[i]=TealiumUtility.convertToBigDecimal(lineItemObject.get(DISCOUNTED_PRICE).getAsJsonObject().get("includedDiscounts")
						.getAsJsonArray().get(0).getAsJsonObject().get("discountedAmount").getAsJsonObject().get(CENTAMOUNT).getAsInt());
			}

		}
		//Need to set
		tealiumData.setSessionId("1");
		//Need to set
		tealiumData.setPageType("PageType");
		//Need to set
		tealiumData.setPageUrl("PageURL");
		//Need to set
		tealiumData.setEventLabel("EventLabel");
		tealiumData.setCartId(cartJsonObject.get("id").getAsString());
		tealiumData.setCartProductId(productIdList);
		tealiumData.setCartProductPrice(productPrice);
		//Need to set
		tealiumData.setProductListPrice(new BigDecimal[lineItems]);
		tealiumData.setCartProductQuantity(productQuantityList);
		tealiumData.setCartProductSku(productSkuList);
		tealiumData.setCartTotalItems(totalItems);
		JsonObject customObject = cartJsonObject.get(CUSTOM).getAsJsonObject().get("fields").getAsJsonObject();
		if(customObject.has(TOTAL_DISCOUNT_PRICE)) {
			tealiumData.setCartDiscount(Arrays.asList(customObject.get(TOTAL_DISCOUNT_PRICE).getAsString()));
		}else {
			tealiumData.setCartDiscount(Arrays.asList("0"));
		}
		tealiumData.setCartTotalValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setEventValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
		if(cartJsonObject.has("customerId")) {
			String customerId = cartJsonObject.get("customerId").getAsString();
			CustomerResponseBean customerResponse = registrationHelperService.getCustomerById(customerId);
			tealiumData.setCustomerId(customerId);
			tealiumData.setCustomerUid(customerId);
			tealiumData.setCustomerEmail(customerResponse.getEmail());
			tealiumData.setCustomerPhone(customerResponse.getCustom().getFields().getPhone());
			tealiumData.setCustomerFirstName(customerResponse.getFirstName());
			tealiumData.setCustomerLastName(customerResponse.getLastName());
			tealiumData.setCustomerBusinessName(customerResponse.getCompanyName());	
		}
		if(cartJsonObject.has(MolekuleConstant.ANONYMOUSID)) {
			tealiumData.setCustomerId(cartJsonObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
			tealiumData.setCustomerUid(tealiumData.getCustomerId());
			tealiumData.setCustomerEmail(customObject.get(MolekuleConstant.EMAIL).getAsString());
			tealiumData.setCustomerPhone(customObject.get(PHONE).getAsString());
			tealiumData.setCustomerFirstName(customObject.get(MolekuleConstant.LAST_NAME).getAsString());
			tealiumData.setCustomerLastName(customObject.get(MolekuleConstant.FIRST_NAME).getAsString());
			tealiumData.setCustomerBusinessName("Anonymous");
		}//Need to confirm the value
		tealiumData.setCustomerEmailPreference(true);
		//Need to set
		tealiumData.setCustomerType("customerType");
		//Need to set
		tealiumData.setCustomerIndustryCategory("Category");
		//Need to set
		tealiumData.setCustomerMarketingOptIn("MarketOptin");
		//Need to set
		tealiumData.setCustomerTosOptIn("tosoptin");
		//Need to set
		tealiumData.setCustomerRefillOptIn("refill");
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Error occured when JSON Processing in setQuoteInformation", e);			
		}
	}

	/**
	 * setOrderComplte is used to call after order creation
	 * 
	 * @param jsonObject
	 */
	public void setOrderCompeltion(JsonObject jsonObject, HttpServletRequest request) {
		TealiumDataBean tealiumData= new TealiumDataBean();
		tealiumData.setTealiumEvent("purchase_completed_view_api");
		tealiumData.setEventCategory(E_COMMERCE);
		tealiumData.setEventAction("Order Successfully Placed");
		tealiumData.setEventLabel("Place Order");
		tealiumData.setEventPlatform("web");
		tealiumData.setSessionId("");
		tealiumData.setPageUrl("");
		tealiumData.setPageType("Checkout");
		tealiumData.setAbTestGroup("");
		tealiumData.setCustomerUid(jsonObject.get(CUSTOMER_ID).getAsString());
		tealiumData.setCustomerId(jsonObject.get(CUSTOMER_ID).getAsString());
		tealiumData.setCustomerEmail(jsonObject.get(CUSTOMER_MAIL).getAsString());

		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/customers/"+tealiumData.getCustomerId();
		JsonObject customerObject = MolekuleUtility.parseJsonObject(getResponse(customerBaseUrl));
		tealiumData.setCustomerFirstName(customerObject.get(MolekuleConstant.FIRST_NAME).getAsString());
		tealiumData.setCustomerLastName(customerObject.get(MolekuleConstant.LAST_NAME).getAsString());
		tealiumData.setCustomerPhone(customerObject.get(CUSTOM).getAsJsonObject()
				.get(MolekuleConstant.FIELDS).getAsJsonObject().get(PHONE).getAsString());

		tealiumData.setOrderId(jsonObject.get("id").getAsString());
		tealiumData.setOrderNumber(jsonObject.get(ORDER_NUMBER).getAsString());
		tealiumData.setOrderCurrencyCode(jsonObject.get(MolekuleConstant.TOTAL_PRICE).getAsJsonObject()
				.get(MolekuleConstant.CURRENCY_CODE).getAsString());
		tealiumData.setOrderShippingType(jsonObject.get(SHIPPING_INFO).getAsJsonObject()
				.get(MolekuleConstant.SHIPPING_METHOD_NAME).getAsString());
		tealiumData.setCustomerBusinessName("");
		tealiumData.setCustomerIndustryCategory("");
		tealiumData.setCustomerTosOptIn("");
		tealiumData.setCustomerMarketingOptIn("");
		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		tealiumData.setOrderShippingAmount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get("shippingCost").getAsString())));
		tealiumData.setOrderGrandTotal(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(ORDER_TOTAL).getAsString())));
		tealiumData.setOrderSubtotal(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
		tealiumData.setOrderHandlingAmount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(HANDLING_COST).getAsString())));
		tealiumData.setOrderTaxAmount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get("totalTax").getAsString())));
		tealiumData.setCustomerRefillOptIn(Boolean.FALSE.toString());
		setAddressess(tealiumData, jsonObject);
		tealiumData.setOrderPromoCode("");
		tealiumData.setOrderDiscount(new BigDecimal(0));
		tealiumData.setCustomerEmailPreference(true);
		tealiumData.setOrderStatus(jsonObject.get("orderState").getAsString());
		tealiumData.setPaymentOnFile(Arrays.asList(true));
		if(request.getHeader("User-Agent").contains("Mobi")) {
			tealiumData.setDeviceName("Mobile");			
		  } else {
			  tealiumData.setDeviceName("desktop");
		  }
		tealiumData.setDeviceId(request.getHeader("User-Agent"));
		if(customObject.has(TOTAL_DISCOUNT_PRICE)) {
			tealiumData.setOrderDiscount(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(TOTAL_DISCOUNT_PRICE).getAsString())));
			JsonArray discountArray = jsonObject.get(DISCOUNT_CODES).getAsJsonArray();
			if(discountArray.size()>0) {
				String code=discountArray.get(0).getAsJsonObject().get(DISCOUNT_CODE).getAsJsonObject().get("id").getAsString();		
				tealiumData.setOrderPromoCode(getProductPromoCode(code));
			}
		}

		String paymentType =jsonObject.get("paymentInfo").getAsJsonObject().get("payments").getAsJsonArray().get(0).getAsJsonObject()
				.get("obj").getAsJsonObject().get("paymentMethodInfo").getAsJsonObject().get("method").getAsString();		

		tealiumData.setOrderPaymentType(paymentType);
		getProductInformation(jsonObject,tealiumData);
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in setOrderCompeltion method", e);
		}
	}

	public void getProductInformation(JsonObject jsonObject, TealiumDataBean tealiumData ) {
		JsonArray jsonArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();

		int lineItems=jsonArray.size();
		List<String> productIdList = new ArrayList<>();
		List<Integer> productQuantity = new ArrayList<>();
		List<String> productSkuList = new ArrayList<>();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> productTypeList = new ArrayList<>();
		List<String> productBrandList = new ArrayList<>();
		List<String> productPromoCodeList = new ArrayList<>();
		String orderPromo = "";
		BigDecimal[] productDiscount = new BigDecimal[lineItems];
		List<Integer> filterList = new ArrayList<>();
		List<String> filterPrice = new ArrayList<>();
		List<String> refillDate = new ArrayList<>();
		tealiumData.setFilterPlanPrice("");
		tealiumData.setFilterRefillEndDate("");
		boolean[] autofill = new boolean[lineItems];
		List<String> serialList = new ArrayList<>();
		int totalItems =0;
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			productIdList.add(lineItemObject.get(PRODUCT_ID).getAsString());
			if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("sku")) {
				productSkuList.add(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());				
			}
			productQuantity.add(lineItemObject.get(QUANTITY).getAsInt());
			totalItems = totalItems + productQuantity.get(i);
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			productBrandList.add(MOLEKULE);
			productTypeList.add(getProductType(lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("id").getAsString()));
			JsonObject priceObject = lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject();
			productPrice[i]=TealiumUtility.convertToBigDecimal(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());

			if(priceObject.has(DISCOUNTED)) {
				String id =priceObject.get(DISCOUNTED).getAsJsonObject().get("discount").getAsJsonObject().get("id").getAsString();
				String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/product-discounts/"+ id;
				JsonObject productObject = MolekuleUtility.parseJsonObject(getResponse(productUrl));
				productPromoCodeList.add(productObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
				productDiscount[i] = TealiumUtility.convertToBigDecimal(priceObject.get(DISCOUNTED).getAsJsonObject().get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			}
			if(lineItemObject.has("discountedPrice")) {
				String id  = lineItemObject.get("discountedPrice").getAsJsonObject().get("includedDiscounts").getAsJsonArray().get(0).getAsJsonObject().get("discount").getAsJsonObject().get("id").getAsString();
				String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
				+ "/cart-discounts/"+ id;
				JsonObject productObject = MolekuleUtility.parseJsonObject(getResponse(productUrl));
				orderPromo = (productObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			}
			if(lineItemObject.has(CUSTOM)&&lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has("serialNumbers")) {
				serialList.add(lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject()
						.get("serialNumbers").getAsJsonArray().get(0).getAsString());
			}
			autofill[i]=false;
			if(lineItemObject.has(CUSTOM)&&lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has(SUBSCRIPTION_ENABLED)) {	
				tealiumData.setCustomerRefillOptIn("true");
				autofill[i] = lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(SUBSCRIPTION_ENABLED).getAsBoolean();
				lineItemObject.get(VARIANT).getAsJsonObject().get("attributes").getAsJsonArray().forEach(attribute->{
					String attributeName = attribute.getAsJsonObject().get("name").getAsString();
					if(attributeName.equals(PERIOD)) {
						filterList.add(attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsInt());
					}else if(attributeName.equals("filterProduct")) {
						String productId= attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsJsonObject().get("id").getAsString();
						String filterUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
						+ "/products/"+ productId;
						JsonObject filterObject = MolekuleUtility.parseJsonObject(getResponse(filterUrl));
						if(!filterObject.has("statusCode")) {
							int price = filterObject.get("masterData").getAsJsonObject().get("current").getAsJsonObject().get("masterVariant").getAsJsonObject().get("prices").getAsJsonArray()
									.get(0).getAsJsonObject().get("value").getAsJsonObject().get(CENTAMOUNT).getAsInt();
							filterPrice.add((TealiumUtility.convertToBigDecimal(price)).toString());	
							tealiumData.setFilterPlanPrice((TealiumUtility.convertToBigDecimal(price)).toString());
						
						}
					}
					
				});
				tealiumData.setFreeRefillEligible(true);
				LocalDate futureDate = LocalDate.now().plusMonths(filterList.get(0));
				tealiumData.setFilterRefillEndDate(futureDate.toString());
				refillDate.add(futureDate.toString());					
			}
		}
		
		tealiumData.setFilterNextChargeAmount(filterPrice);
		tealiumData.setFilterFrequency(filterList);
		tealiumData.setAutoRefill(autofill);
		tealiumData.setSerialNumber(serialList);
		tealiumData.setProductRenewalDate(tealiumData.getFilterRefillEndDate());
		tealiumData.setProductBrand(productBrandList);
		tealiumData.setProductName(productNameList);
		tealiumData.setProductCategory(productTypeList);
		tealiumData.setProductId(productIdList);
		tealiumData.setProductSku(productSkuList);
		tealiumData.setProductQuantity(productQuantity);
		tealiumData.setProductPrice(productPrice);
		tealiumData.setProductListPrice(productPrice);
		tealiumData.setProductPromoCode(productPromoCodeList);
		tealiumData.setProductDiscountAmount(productDiscount);
		tealiumData.setOrderPromoCode(orderPromo);
		tealiumData.setProductReview(new ArrayList<String>());
		tealiumData.setProductStock(new ArrayList<String>());
		tealiumData.setProductImageUrl(new ArrayList<String>());
		tealiumData.setCartTotalItems(totalItems);
	}

	/**
	 * setAccountInfo is used to set accountInfo 
	 * 
	 * @param response
	 */
	public void setAccountInfo(String response) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		TealiumDataBean customerData= new TealiumDataBean();
		customerData.setTealiumEvent("account_completed_view_api");
		customerData.setEventCategory("B2B Full Register");
		customerData.setEventAction("Completed account Register");

		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		customerData.setSessionId("");
		customerData.setPageType("register");
		customerData.setPageUrl("");
		customerData.setAbTestGroup("");
		customerData.setCustomerId(valueObject.get("employeeIds").getAsJsonArray().get(0).getAsString());
		customerData.setCustomerBusinessName(valueObject.get("companyName").getAsString());
		customerData.setCustomerIndustryCategory(valueObject.get("companyCategory").getAsJsonObject().get("name").getAsString());
		//Save shipping address to tealium data

		JsonObject addressObject =valueObject.get(ADDRESSES).getAsJsonObject().get("shippingAddresses").getAsJsonArray().get(0).getAsJsonObject();
		customerData.setCustomerFirstNameShipping(addressObject.get(MolekuleConstant.FIRST_NAME).getAsString());
		customerData.setCustomerLastNameShipping(addressObject.get(MolekuleConstant.LAST_NAME).getAsString());
		customerData.setCustomerCityShipping(addressObject.get("city").getAsString());
		customerData.setCustomerCountryShipping(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
		customerData.setCustomerCountryCodeShipping(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
		customerData.setCustomerStateShipping(addressObject.get(MolekuleConstant.STATE).getAsString());
		customerData.setCustomerZipShipping(addressObject.get(MolekuleConstant.POSTAL_CODE).getAsString());
		customerData.setCustomerAddress1Shipping(addressObject.get("streetAddress1").getAsString());
		//Save billing Address to the tealium data
		if(valueObject.get(ADDRESSES).getAsJsonObject().get("billingAddresses").getAsJsonArray().size() > 0) {
			addressObject =valueObject.get(ADDRESSES).getAsJsonObject().get("billingAddresses").getAsJsonArray().get(0).getAsJsonObject();
		}
		customerData.setCustomerFirstNameBilling(addressObject.get(MolekuleConstant.FIRST_NAME).getAsString());
		customerData.setCustomerLastNameBilling(addressObject.get(MolekuleConstant.LAST_NAME).getAsString());
		customerData.setCustomerCityBilling(addressObject.get("city").getAsString());
		customerData.setCustomerCountryBilling(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
		customerData.setCustomerCountryCodeBilling(addressObject.get(MolekuleConstant.COUNTRY).getAsString());
		customerData.setCustomerStateBilling(addressObject.get(MolekuleConstant.STATE).getAsString());
		customerData.setCustomerZipBilling(addressObject.get(MolekuleConstant.POSTAL_CODE).getAsString());
		customerData.setCustomerAddress1Billing(addressObject.get("streetAddress1").getAsString());

		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(customerData));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in setAccountInfo", e);
		}

	}

	public void quickRegister(String response,CustomerDTO customerDTO) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		TealiumDataBean customerData= new TealiumDataBean();
		customerData.setTealiumEvent("quick_register_completed_api");
		customerData.setEventCategory("B2B Quick Register");
		customerData.setEventAction("Completed Quick Register");
		customerData.setEventLabel("Register");
		customerData.setEventPlatform("web");
		customerData.setSessionId("");
		customerData.setPageType("Quick register");
		customerData.setPageUrl("");
		customerData.setAbTestGroup("");
		customerData.setEventValue(new BigDecimal(0));
		customerData.setCustomerMarketingOptIn(Boolean.TRUE.toString());
		customerData.setCustomerId(jsonObject.get(CUSTOMER_ID).getAsString());
		customerData.setCustomerUid(jsonObject.get(CUSTOMER_ID).getAsString());
		customerData.setCustomerEmail(jsonObject.get(MolekuleConstant.EMAIL).getAsString());
		customerData.setCustomerFirstName(jsonObject.get(MolekuleConstant.FIRST_NAME).getAsString());
		customerData.setCustomerLastName(jsonObject.get(MolekuleConstant.LAST_NAME).getAsString());
		if(jsonObject.has(CUSTOM)) {
			JsonObject fields = jsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			if(fields.has(CHANNEL) && B2B.equals(fields.get(CHANNEL).getAsString())) {
				customerData.setCustomerType(jsonObject.get("customerGroup").getAsJsonObject().get("customerGroupName").getAsString());				
			}
		}
		customerData.setCustomerPhone(customerDTO.getPhone());
		customerData.setCustomerBusinessName(customerDTO.getCompanyName());
		customerData.setCustomerIndustryCategory(customerDTO.getCompanyCategoryId());
		customerData.setCustomerTosOptIn(Boolean.TRUE.toString());
		customerData.setCustomerRefillOptIn(Boolean.TRUE.toString());
		customerData.setCustomerEmailPreference(true);

		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(customerData));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in quickRegister", e);
		}

	}
	
	public void setPurchaseSubData(String orderResponse) {
		TealiumDataBean tealiumdata= new TealiumDataBean();
		tealiumdata.setTealiumEvent("purchase_subdata_api");
		tealiumdata.setEventCategory(E_COMMERCE);
		tealiumdata.setEventAction("Completed Transaction");
		
		JsonObject jsonObject= MolekuleUtility.parseJsonObject(orderResponse);
		if(jsonObject.has("error") || jsonObject.has("statusCode") || jsonObject.has("status")) {
			return;
		}
		tealiumdata.setCustomerUid(jsonObject.has(CUSTOMER_ID) ? jsonObject.get(CUSTOMER_ID).getAsString() : jsonObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
		if(jsonObject.has(CUSTOMER_MAIL)) {
		tealiumdata.setCustomerEmail(jsonObject.get(CUSTOMER_MAIL).getAsString());
		}
		JsonArray jsonArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
		int lineItems=jsonArray.size();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> refillDate = new ArrayList<>();
		List<String> filterPrice = new ArrayList<>();
		List<Integer> filterPeriod = new ArrayList<>();
	
		boolean[] autofill= new boolean[lineItems];
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			JsonObject priceObject = lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject();
			productPrice[i]=TealiumUtility.convertToBigDecimal(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			if(lineItemObject.has(CUSTOM)&&lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has(SUBSCRIPTION_ENABLED)) {
				autofill[i] = lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(SUBSCRIPTION_ENABLED).getAsBoolean();
				lineItemObject.get(VARIANT).getAsJsonObject().get("attributes").getAsJsonArray().forEach(attribute->{
					String attributeName = attribute.getAsJsonObject().get("name").getAsString();
					if(attributeName.equals(PERIOD)) {
						filterPeriod.add(attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsInt());
						LocalDate futureDate = LocalDate.now().plusMonths(attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsInt());
						refillDate.add(futureDate.toString());
					}
					if(attributeName.equals("filterProduct")) {
						String id = attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsJsonObject().get("id").getAsString();
						String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
						+ "/products/"+ id;
						
						JsonObject productObject = MolekuleUtility.parseJsonObject(getResponse(productUrl));
						JsonObject filterPriceObject= productObject.get("masterData").getAsJsonObject().get("current").getAsJsonObject().get("masterVariant").getAsJsonObject().get("prices").getAsJsonArray().get(0).getAsJsonObject();
						BigDecimal price =TealiumUtility.convertToBigDecimal(filterPriceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
						filterPrice.add(price.toString());
					}
				});
			}
		}
		tealiumdata.setFilterFrequency(filterPeriod);
		tealiumdata.setFilterPlanPriceList(filterPrice);
		tealiumdata.setFilterRefillEndDateList(refillDate);
		tealiumdata.setAutoRefill(autofill);
		tealiumdata.setDeviceId("Need to set");
		tealiumdata.setSubscriptionId(new ArrayList<>());
		JsonObject customObject =jsonObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject();
		tealiumdata.setEventValue(TealiumUtility.convertToBigDecimal(Integer.valueOf(customObject.get(SUB_TOTAL).getAsString())));
		
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumdata));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in purchase subdata", e);
		}
	}


	public void setAffirmPaymentCapture(String orderResponse) {
		TealiumDataBean tealiumdata= new TealiumDataBean();
		tealiumdata.setTealiumEvent("clicked_affirm_later");
		tealiumdata.setEventCategory(E_COMMERCE);
		tealiumdata.setEventAction("Affirm Payment Capture");
		JsonObject orderReposneJsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		String customerId;
		if(orderReposneJsonObject.get("customerId") != null) {
		 customerId = orderReposneJsonObject.get("customerId").getAsString();
		}else {
		  customerId = orderReposneJsonObject.get(ANONYMOUSID).getAsString();	
		}
		JsonArray jsonArray = orderReposneJsonObject.get(LINE_ITEMS).getAsJsonArray();
		int lineItems=jsonArray.size();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> refillDate = new ArrayList<>();
		List<String> filterPrice = new ArrayList<>();
		List<Integer> filterPeriod = new ArrayList<>();
	
		boolean[] autofill= new boolean[lineItems];
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			JsonObject priceObject = lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject();
			productPrice[i]=TealiumUtility.convertToBigDecimal(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			if(lineItemObject.has(CUSTOM)&&lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has(SUBSCRIPTION_ENABLED)) {
				autofill[i] = lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().get(SUBSCRIPTION_ENABLED).getAsBoolean();
				lineItemObject.get(VARIANT).getAsJsonObject().get("attributes").getAsJsonArray().forEach(attribute->{
					String attributeName = attribute.getAsJsonObject().get("name").getAsString();
					if(attributeName.equals(PERIOD)) {
						filterPeriod.add(attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsInt());
						LocalDate futureDate = LocalDate.now().plusMonths(attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsInt());
						refillDate.add(futureDate.toString());
					}
					if(attributeName.equals("filterProduct")) {
						String id = attribute.getAsJsonObject().get(MolekuleConstant.VALUE).getAsJsonObject().get("id").getAsString();
						String productUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
						+ "/products/"+ id;
						
						JsonObject productObject = MolekuleUtility.parseJsonObject(getResponse(productUrl));
						JsonObject filterPriceObject= productObject.get("masterData").getAsJsonObject().get("current").getAsJsonObject().get("masterVariant").getAsJsonObject().get("prices").getAsJsonArray().get(0).getAsJsonObject();
						BigDecimal price =TealiumUtility.convertToBigDecimal(filterPriceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
						filterPrice.add(price.toString());
					}
				});
			}
		}
		
		JsonObject shippingAddressJsonObject = orderReposneJsonObject.get(SHIPPING_ADDRESS).getAsJsonObject();
		tealiumdata.setCustomerAddress1Shipping(shippingAddressJsonObject.get("streetName").getAsString());
		tealiumdata.setCustomerAddress2Shipping("");
		tealiumdata.setCustomerCityShipping(shippingAddressJsonObject.get("city").getAsString());
		tealiumdata.setCustomerCountryCodeShipping(shippingAddressJsonObject.get("country").getAsString());
		tealiumdata.setCustomerCountryShipping(shippingAddressJsonObject.get("country").getAsString());
		tealiumdata.setCustomerEmail(orderReposneJsonObject.get(CUSTOMER_MAIL).getAsString());
		tealiumdata.setCustomerFirstNameShipping(shippingAddressJsonObject.get("firstName").getAsString());
		tealiumdata.setCustomerId(customerId);
		tealiumdata.setCustomerLastNameShipping(shippingAddressJsonObject.get("lastName").getAsString());
		tealiumdata.setCustomerStateShipping(shippingAddressJsonObject.get("state").getAsString());
		tealiumdata.setCustomerUid(customerId);
		tealiumdata.setCustomerZipShipping(shippingAddressJsonObject.get("postalCode").getAsString());
		tealiumdata.setDeviceId("Need to set");
		tealiumdata.setEventPlatform("web");
		tealiumdata.setFilterFrequency(filterPeriod);
		tealiumdata.setFilterNextChargeAmount(filterPrice);
		tealiumdata.setFilterPlanPriceList(filterPrice);
		tealiumdata.setFilterRefillEndDateList(refillDate);
		tealiumdata.setFreeRefillEligible(true);
		tealiumdata.setNextShipmentDate(refillDate);
		tealiumdata.setPageType("Affrim Payment Capture");
		tealiumdata.setPageUrl("");
		tealiumdata.setSubscriptionId(new ArrayList<>());
		
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumdata));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in purchase subdata", e);
		}
	}
	
	public void triggerRefundEvent(Map<String, String> responseMap) {
		String orderResponse = responseMap.get("orderResponse");
		String refundCustomResponse = responseMap.get("refundCustom");
		JsonObject orderObject = MolekuleUtility.parseJsonObject(orderResponse);
		JsonObject customObject = MolekuleUtility.parseJsonObject(refundCustomResponse);
		if(orderObject.has(MolekuleConstant.STATUS_CODE)||customObject.has(MolekuleConstant.STATUS_CODE)) {
			throw new CustomRunTimeException("Error occured in refund process");
		}
		List<String> lineItemList = new ArrayList<>();
		customObject.get(MolekuleConstant.VALUE).getAsJsonObject().get("lineItemsList").getAsJsonArray().forEach(item->{
			 lineItemList.add(item.getAsJsonObject().get("lineItemId").getAsString());
		 });
		TealiumDataBean tealiumData = new TealiumDataBean();
		tealiumData.setTealiumEvent("return_product_api");
		tealiumData.setEventCategory("Offline Ecommerce");
		tealiumData.setEventAction("Refund");
		tealiumData.setCustomerUid(orderObject.has(CUSTOMER_ID) ? orderObject.get(CUSTOMER_ID).getAsString() : orderObject.get(MolekuleConstant.ANONYMOUSID).getAsString());
		tealiumData.setCustomerEmail(orderObject.get(CUSTOMER_MAIL).getAsString());
		
		List<String> serialList = new ArrayList<>();
		List<Integer> productQuantity = new ArrayList<>();
		List<String> productSkuList = new ArrayList<>();
		List<String> productNameList = new ArrayList<>();
		List<String> productTypeList = new ArrayList<>();
		JsonArray jsonArray = orderObject.get(LINE_ITEMS).getAsJsonArray();
		
		int lineItems= jsonArray.size();
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			if(lineItemList.contains(lineItemObject.get(ID).getAsString())) {
				if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("sku")) {
					productSkuList.add(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());	
					tealiumData.setEventLabel(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());
				}
				productQuantity.add(lineItemObject.get(QUANTITY).getAsInt());
				if(lineItemObject.get("name").getAsJsonObject().has(EN_GB)) {
					productNameList.add(lineItemObject.get("name").getAsJsonObject().get(EN_GB).getAsString());
                } else if(lineItemObject.get("name").getAsJsonObject().has(EN_US)) {
                	productNameList.add(lineItemObject.get("name").getAsJsonObject().get(EN_US).getAsString());
                }
				productTypeList.add(getProductType(lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("id").getAsString()));
				if(lineItemObject.has(CUSTOM)&&lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject().has("serialNumbers")) {
					serialList.add(lineItemObject.get(CUSTOM).getAsJsonObject().get(MolekuleConstant.FIELDS).getAsJsonObject()
							.get("serialNumbers").getAsJsonArray().get(0).getAsString());
				}
			}	
		}
		tealiumData.setProductCategory(productTypeList);
		tealiumData.setProductName(productNameList);
		tealiumData.setProductQuantity(productQuantity);
		tealiumData.setProductSku(productSkuList);
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumData));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in refund ", e);
		}
	}
	
	public void forgotPassword(String customerEmail) {
		TealiumDataBean tealiumdata= new TealiumDataBean();
		tealiumdata.setTealiumEvent("forgot_password_link_created_api");
		tealiumdata.setEventCategory("Account");
		tealiumdata.setEventAction("Forgot Password - Link Created");
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/customers?where=";
		String emailStr = "email=\"" + customerEmail + "\"";
		customerBaseUrl = customerBaseUrl + MolekuleUtility.convertToEncodeUri(emailStr);
		String response = null;
		try {
			response = netConnectionHelper.httpConnectionHelper(customerBaseUrl, ctServerHelperService.getStringAccessToken());
		} catch (IOException e) {
			logger.error("IOException Occured", e);
		}
		JsonObject jsonObject= MolekuleUtility.parseJsonObject(response);
		if(jsonObject.has("error") || jsonObject.has("statusCode") || jsonObject.has("status")) {
			return;
		}
		tealiumdata.setCustomerEmail(customerEmail);
		tealiumdata.setCustomerUid(jsonObject.get(ID).getAsString());
		tealiumdata.setCustomerId(jsonObject.get(ID).getAsString());
		String baseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/carts/customer-id=\""+jsonObject.get(ID).getAsString()+"\"";
		String cartResponse =  getResponse(baseUrl);
		JsonObject cartObject= MolekuleUtility.parseJsonObject(cartResponse);
		
		JsonArray jsonArray = cartObject.get(LINE_ITEMS).getAsJsonArray();
		int lineItems=jsonArray.size();
		List<String> productIdList = new ArrayList<>();
		List<Integer> productQuantity = new ArrayList<>();
		List<String> productSkuList = new ArrayList<>();
		BigDecimal[] productPrice = new BigDecimal[lineItems];
		List<String> productNameList = new ArrayList<>();
		List<String> productTypeList = new ArrayList<>();
		List<String> productBrandList = new ArrayList<>();
		int totalItems =0;
		int totalValue = 0;
		for(int i=0;i<lineItems;i++) {
			JsonObject lineItemObject =jsonArray.get(i).getAsJsonObject();
			productIdList.add(lineItemObject.get(PRODUCT_ID).getAsString());
			if(lineItemObject.has(VARIANT) &&lineItemObject.get(VARIANT).getAsJsonObject().has("sku")) {
				productSkuList.add(lineItemObject.get(VARIANT).getAsJsonObject().get("sku").getAsString());				
			}
			productQuantity.add(lineItemObject.get(QUANTITY).getAsInt());
			totalItems = totalItems + productQuantity.get(i);
			productNameList.add(lineItemObject.get("name").getAsJsonObject().get(ctEnvProperties.getLanguage()).getAsString());
			productBrandList.add(MOLEKULE);
			productTypeList.add(getProductType(lineItemObject.get(PRODUCT_TYPE).getAsJsonObject().get("id").getAsString()));
			JsonObject priceObject = lineItemObject.get(MolekuleConstant.PRICE).getAsJsonObject();
			productPrice[i]=TealiumUtility.convertToBigDecimal(priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt());
			totalValue = totalItems * priceObject.get(MolekuleConstant.VALUE).getAsJsonObject().get(CENTAMOUNT).getAsInt();
				
		}
		tealiumdata.setCartProductId(productIdList);
		tealiumdata.setCartProductQuantity(productQuantity);
		tealiumdata.setCartProductSku(productSkuList);
		tealiumdata.setCartProductListPrice(productPrice);
		tealiumdata.setCartProductPrice(productPrice);
		tealiumdata.setCartTotalValue(TealiumUtility.convertToBigDecimal(totalValue));
		tealiumdata.setCartTotalItems(totalItems);
		tealiumdata.setCartUrl(new ArrayList<String>());
		tealiumdata.setOfferName("");
		tealiumdata.setSessionId("");
		
		try {
			TealiumUtility.sendCurlRequest(new ObjectMapper().writeValueAsString(tealiumdata));
		} catch (JsonProcessingException e) {
			logger.error("Exception occured TealiumDataBean in Forgot Password", e);
		}
	}
}
