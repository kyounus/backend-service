package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNT_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscountCodeInfo {

	@JsonProperty(DISCOUNT_CODE)
	private String discountCode;

	@JsonProperty(STATE)
	private String state;

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
