package com.molekule.api.v1.commonframework.dto.order;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class OrderRefundServiceBean {
	private Long version;
	private List<Action> actions = null;

	@Data
	public static class Action {

		@JsonProperty(ACTION)
		private String actionName;
		private String name;
		private Object value;
		private String returnItemId;
		private String paymentState;

	}
	
}
