package com.molekule.api.v1.commonframework.service.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FREIGHT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
/**
 * FreightHandlingCostCalculationServiceImpl is a class used to handle the freight related handling charges.
 * 
 *
 */
@Component
public class FreightHandlingCostCalculationServiceImpl implements HandlingCostCalculationService{

	@Override
	public String calculateHandlingCharge(JsonObject shippingTableJsonObject, int deviceCount, int filterCount) {
		int productTypeCount = (deviceCount !=0 && filterCount !=0) ? 2: 1;
		int freightThresholdDevice = shippingTableJsonObject.get(VALUE).getAsJsonObject().get(FREIGHT).getAsJsonObject().get("threshold").getAsJsonObject().get("device").getAsInt();
		int freightThresholdFilter = shippingTableJsonObject.get(VALUE).getAsJsonObject().get(FREIGHT).getAsJsonObject().get("threshold").getAsJsonObject().get("filter").getAsInt();
		BigDecimal palletCount = BigDecimal.ZERO;
		
		// calculate the pallet count based on product type
		if(productTypeCount == 1 && deviceCount !=0 && filterCount == 0) {
			palletCount = new BigDecimal(deviceCount / freightThresholdDevice);
		}else if(productTypeCount == 1 && filterCount !=0 && deviceCount ==0) {
			palletCount = new BigDecimal(filterCount / freightThresholdFilter);
		} else if(productTypeCount == 2) {
			int devicePalletCount = deviceCount / freightThresholdDevice;
			int filterPalletCount = filterCount / freightThresholdFilter;
			palletCount = new BigDecimal(devicePalletCount + filterPalletCount);
		}
		JsonObject freightJsonObject = shippingTableJsonObject.get(VALUE).getAsJsonObject().get("handlingCosts").getAsJsonObject()
				.get(FREIGHT).getAsJsonObject();
		// Pallet charge
		BigDecimal perPalletCharge = freightJsonObject.get("perPallet").getAsJsonObject().get(AMOUNT).getAsBigDecimal();
		BigDecimal totalPalletCharge = perPalletCharge.multiply(palletCount);
		
		// sku charge
		BigDecimal perSkuCharge = freightJsonObject.get("perSku").getAsJsonObject().get(AMOUNT).getAsBigDecimal();
		BigDecimal totalSkuCharge = perSkuCharge.multiply(new BigDecimal(productTypeCount));
		
		// order charge
		BigDecimal orderCharge = freightJsonObject.get("orderCharge").getAsJsonObject().get("price").getAsJsonObject().get(AMOUNT).getAsBigDecimal();
		
		// scan charge
		BigDecimal scanCharge = freightJsonObject.get("snScan").getAsJsonObject().get("price").getAsJsonObject().get(AMOUNT).getAsBigDecimal();
		BigDecimal totalScanCharge = scanCharge.multiply(new BigDecimal(deviceCount + filterCount));
		
		// final total handling charge
		BigDecimal finaltotalHandlingCostForFreight = totalPalletCharge.add(totalSkuCharge).add(orderCharge).add(totalScanCharge);
		
		// round of to nearest value
		return String.valueOf(finaltotalHandlingCostForFreight.setScale(0, RoundingMode.HALF_EVEN));
	}

}
