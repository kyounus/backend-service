package com.molekule.api.v1.commonframework.dto.cart;

import java.util.List;

import lombok.Data;
@Data
public class SetShippingAddressRequestBean {

	private Long version;
	List<Action> actions;
}
