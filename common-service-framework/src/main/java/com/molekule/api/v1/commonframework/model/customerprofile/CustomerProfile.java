package com.molekule.api.v1.commonframework.model.customerprofile;

import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean;

import lombok.Data;

@Data
public class CustomerProfile {
	private AccountCustomObject  accountCustomObject;
	private CustomerResponseBean customerResponseBean;
	private OrderSummaryResponseBean orderSummaryResponseBean;
	private MessageCenterBean messageCenterBean;
	private AutoRefillsResponseBean autoRefillsResponseBean;
	private PaymentCustomObject paymentCustomObject;
}
