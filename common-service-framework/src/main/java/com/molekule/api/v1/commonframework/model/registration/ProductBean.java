package com.molekule.api.v1.commonframework.model.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.MASTER_DATA;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.ProductCatalogData;
import com.molekule.api.v1.commonframework.model.products.ProductTypeReference;

import io.swagger.annotations.ApiModel;

/**
 * The class ProductBean is used to hold the request of Product.
 */
@ApiModel(description = "Product model")
public class ProductBean {

	@JsonProperty("id")
	private String id;

	@JsonProperty(VERSION)
	private Long version;

	@JsonProperty("key")
	private String key;

	@JsonProperty(PRODUCT_TYPE)
	private ProductTypeReference productType;

	@JsonProperty(MASTER_DATA)
	private ProductCatalogData masterData;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public ProductTypeReference getProductType() {
		return productType;
	}

	public void setProductType(ProductTypeReference productType) {
		this.productType = productType;
	}

	public ProductCatalogData getMasterData() {
		return masterData;
	}

	public void setMasterData(ProductCatalogData masterData) {
		this.masterData = masterData;
	}

	@Override
	public String toString() {
		return "ProductResponseBean [id=" + id + ", version=" + version + ", key=" + key + ", productType="
				+ productType + ", masterData=" + masterData + "]";
	}

}
