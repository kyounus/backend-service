package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class PaymentOptionResponseBean is used to hold the response of payment option.
 */
public class PaymentOptionResponseBean {

	private String accountId;
	private String customerId;
	private String customerNumber;
	private String paymentPrefernces;
	private String paymentToken;
	private boolean isBillingAddressEnable;
	private BillingAddressRequestBean billingAddress;
	private int version;
	private String downloadUrl;
	private String businessContact;
	private String financeContact;

	public BillingAddressRequestBean getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddressRequestBean billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getPaymentPrefernces() {
		return paymentPrefernces;
	}

	public void setPaymentPrefernces(String paymentPrefernces) {
		this.paymentPrefernces = paymentPrefernces;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	public boolean isBillingAddressEnable() {
		return isBillingAddressEnable;
	}

	public void setBillingAddressEnable(boolean isBillingAddressEnable) {
		this.isBillingAddressEnable = isBillingAddressEnable;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getDownloadUrl() {
		return downloadUrl;
	}
	
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	public String getbusinessContact() {
		return businessContact;
	}
	
	public void setBusinessContact(String businessContact) {
		this.businessContact = businessContact;
	}
	
	public String getFinanceContact() {
		return financeContact;
	}
	
	public void setFinanceContact(String financeContact) {
		this.financeContact = financeContact;
	}
}
