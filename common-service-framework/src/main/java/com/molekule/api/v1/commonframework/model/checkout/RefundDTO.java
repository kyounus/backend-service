package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class RefundDTO {

	private String pspReference;
	private String reference;
	private String amount;
	private String currency;
}
