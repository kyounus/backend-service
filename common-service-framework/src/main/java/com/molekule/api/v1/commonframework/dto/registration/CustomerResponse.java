package com.molekule.api.v1.commonframework.dto.registration;

import lombok.Data;

@Data
public class CustomerResponse {

	private CustomerResponseBean customerResponseBean;
	private String cartId;
}
