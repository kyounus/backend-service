package com.molekule.api.v1.commonframework.mail.sendgrid;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.MAX_RETRY;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;
import com.molekule.api.v1.commonframework.util.SendGridClientThread;
/**
 * TemplateMailRequestHelper class Define a Standard Template for sending
 * mail through SendGrid
 * 
 * @version 1.0
 */
@Component
public abstract class TemplateMailRequestHelper {

	@Value("${sendGridUrl}")
	private String sendGridUrl;

	@Value("${sendGridApiKey}")
	private String sendGridApiKey;

	@Value("${fromEmail}")
	private String fromEmail;
	
	@Value("${fromName}")
	private String fromName;
	
	@Value("${maxThreadPoolSize}")
	private int threadPoolSize;
	
	@Value("${maxRetry}")
	private int maxRetry;

	private String toEmail;
	
	private String toName;

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getSendGridUrl() {
		return sendGridUrl;
	}

	public void setSendGridUrl(String sendGridUrl) {
		this.sendGridUrl = sendGridUrl;
	}

	public String getSendGridApiKey() {
		return sendGridApiKey;
	}

	public void setSendGridApiKey(String sendGridApiKey) {
		this.sendGridApiKey = sendGridApiKey;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public abstract String templateId();
	
	/**
	 * sendMail method will place the request data in Map
	 * and will send that map SendGridClientThread
	 * 
	 * @param Object
	 * 
	 */
	public void sendMail(Object obj) {
		Map<String, Object> requestData = new HashMap<>();
		requestData.put("sendGridUrl", getSendGridUrl());
		requestData.put("sendGridApiKey", getSendGridApiKey());
		requestData.put(MAX_RETRY, getMaxRetry());
		requestData.put("requestBean", prepareRequestBean(obj));
		sendMailThroughSendGrid(requestData);
	}

	/**
	 * sendMailThroughSendGrid method create's the thread for sending 
	 * request to SendGrid Api
	 * 
	 * @param Map<String, Object>
	 * 
	 */
	public void sendMailThroughSendGrid(Map<String, Object> requestData) {
		ExecutorService executorService = Executors.newFixedThreadPool(getMaxThreadPoolSize());
		SendGridClientThread sendGridClientThread = new SendGridClientThread();
		sendGridClientThread.setRequestData(requestData);
		executorService.execute(sendGridClientThread);
		executorService.shutdown();
	}

	public SendGridRequestBean prepareRequestBean(Object obj) {
		SendGridModel sendGridModel = (SendGridModel) obj;
		SendGridRequestBean mailResponseBean = new SendGridRequestBean();
		SendGridRequestBean.Personalizations personalizations = getPersonalizations(sendGridModel);
		SendGridRequestBean.From fromMail = new SendGridRequestBean.From();
		mailResponseBean.setTemplateId(templateId());
		fromMail.setEmail(getFromEmail());
		fromMail.setName(getFromName());
		mailResponseBean.setFrom(fromMail);
		List<SendGridRequestBean.Personalizations> personalizationsList = new ArrayList<>();
		personalizationsList.add(personalizations);
		mailResponseBean.setPersonalizations(personalizationsList);
		return mailResponseBean;

	}

	public SendGridRequestBean.Personalizations getPersonalizations(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations personalizations = new SendGridRequestBean.Personalizations();
		SendGridRequestBean.Personalizations.To to = new SendGridRequestBean.Personalizations.To();
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = getDynamicTemplateData(
				sendGridModel);
		to.setEmail(sendGridModel.getEmail());
		to.setName(sendGridModel.getFirstName());
		List<SendGridRequestBean.Personalizations.To> toList = new ArrayList<>();
		toList.add(to);
		personalizations.setTo(toList);
		personalizations.setDynamicTemplateData(dynamicTemplateData);
		return personalizations;

	}

	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		/** Added the below condition for sonarqube error.*/
		if(sendGridModel != null) {
			sendGridModel.getFirstName();
		}
		return null;

	}
	public int getThreadPoolSize() {
		return threadPoolSize;
	}

	public void setThreadPoolSize(int threadPoolSize) {
		this.threadPoolSize = threadPoolSize;
	}

	public int getMaxThreadPoolSize() {
		if (getThreadPoolSize() == 0) {
			return 10;
		}
		return getThreadPoolSize();
	}

	public int getMaxRetry() {
		return maxRetry;
	}

	public void setMaxRetry(int maxRetry) {
		this.maxRetry = maxRetry;
	}

}
