package com.molekule.api.v1.commonframework.dto.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The class PaymentCustomObject is used to hold the payment data of D2C customer.
 */
@Data
public class PaymentCustomObject {
	
	@JsonProperty(CONTAINER)
	private String container;
	
	@JsonProperty("key")
	private String key;
	
	@JsonProperty(VERSION)
	private Integer version;
	
	@JsonProperty(VALUE)
	private Value value;
	
	@Data
	public static class Value {

		@JsonProperty(PAYMENTS)
		private List<Payments> payments;
		private String preferredPaymentId;
		private String recentlyUsedPayment;
	}
}