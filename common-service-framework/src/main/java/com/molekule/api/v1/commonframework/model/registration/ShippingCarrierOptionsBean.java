package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import lombok.Data;

@Data
public class ShippingCarrierOptionsBean {
	
	private ShippingAddresses shippingAddress;
	private boolean primaryShipping;
	private List<CarrierOption> carriers;
	private boolean acceptFreight;
	private AdditionalFreightInfo additionalFreightInfo;

}
