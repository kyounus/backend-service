package com.molekule.api.v1.commonframework.model.customerprofile;

import lombok.Data;

@Data
public class SubscriptionRequestBean {

	private String customerId;
	private String paymentId;
	private String shippingAddressId;
	private String nextOrderDate;
	private String productId;
	private int period;
	private String promoCode;
	private String serialNumber;
	private String purchaseDate;
	private String ShippingMethod;
	private String cost;
}
