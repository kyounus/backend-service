package com.molekule.api.v1.commonframework.model.customerprofile;

import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;
import com.molekule.api.v1.commonframework.util.CustomerChannelEnum;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdateAutoRefillsRequestBean {
	
	public enum AutoRefillsActions {
		UPDATE_QUANTITY, 
		UPDATE_SHIPPING_ADDRESS, 
		UPDATE_RENEWAL_DATE, 
		TURN_OFF, 
		UPDATE_PAYMENT_OPTION, 
		TURN_ON
	}
	
	@ApiModelProperty(example = "UPDATE_QUANTITY/UPDATE_SHIPPING_ADDRESS/UPDATE_RENEWAL_DATE/TURN_OFF/UPDATE_PAYMENT_OPTION/TURN_ON")
	private AutoRefillsActions action;
	private String paymentId;
	@ApiModelProperty(example = "CREDITCARD/PAYMENTTERMS")
	private String paymentType;
	private String surveyQuestionId;
	private String renewalDate;
	private String shippingAddressId;
	private CartRequestBean updateCartRequestBean;
	private ShippingCarrierOptionsBean shippingCarrierOptionsBean;
	private PaymentRequestBean paymentRequestBean;
	private UpdatePaymentRequestBean updatePaymentRequest;
	private String other;
	private D2CPaymentRequestBean d2CPaymentRequestBean;
	private CustomerChannelEnum channel;
}
