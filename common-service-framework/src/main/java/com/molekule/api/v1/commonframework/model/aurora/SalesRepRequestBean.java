package com.molekule.api.v1.commonframework.model.aurora;

import java.util.List;

import lombok.Data;

@Data
public class SalesRepRequestBean {
	
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String calendlyLink;
	private List<String> categoryId;

}
