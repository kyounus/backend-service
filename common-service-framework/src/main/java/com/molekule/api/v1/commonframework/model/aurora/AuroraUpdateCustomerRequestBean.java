package com.molekule.api.v1.commonframework.model.aurora;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AuroraUpdateCustomerRequestBean extends AuroraCustomerRequestBean {

	@ApiModelProperty(required = true)
	private String phone;
	@ApiModelProperty(required = true)
	private String title;
	@ApiModelProperty(required = true)
	private Boolean accountLocked;
	@ApiModelProperty(required = true)
	private String cognitoId;
}
