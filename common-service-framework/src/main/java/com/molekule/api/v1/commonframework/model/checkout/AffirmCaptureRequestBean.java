package com.molekule.api.v1.commonframework.model.checkout;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AffirmCaptureRequestBean {

	@JsonProperty("order_id")
	private String orderId;
	
	@JsonProperty("shipping_carrier")
	private String shippingCarrier;
	
	@JsonProperty("shipping_confirmation")
	private String shippingConfirmation;
}
