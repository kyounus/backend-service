package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class ShippingAddress is used to hold the shipping address.
 */
public class ShippingAddress extends Address {

	private String companyName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
}
