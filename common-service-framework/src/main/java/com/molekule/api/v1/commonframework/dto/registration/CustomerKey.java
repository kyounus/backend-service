package com.molekule.api.v1.commonframework.dto.registration;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
@Data
public class CustomerKey {
	@JsonProperty(VERSION)
	private long version;
	@JsonProperty("actions")
	private List<Actions> actions;
	
	@Data
	public static class Actions{
		@JsonProperty(ACTION)
		private String action;
		@JsonProperty("key")
		private String key;
	}
}
