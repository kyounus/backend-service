package com.molekule.api.v1.commonframework.model.products;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.MASTER_VARIANT;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductData {

	@JsonProperty("name")
	private Map<String, String> name;

	@JsonProperty("categories")
	private List<CategoryReference> categories;

	@JsonProperty("categoryOrderHints")
	private Map<String, String> values;

	@JsonProperty("description")
	private Map<String, String> description;

	@JsonProperty("slug")
	private Map<String, String> slug;

	@JsonProperty("metaTitle")
	private Map<String, String> metaTitle;

	@JsonProperty("metaDescription")
	private Map<String, String> metaDescription;

	@JsonProperty("metaKeywords")
	private Map<String, String> metaKeywords;

	@JsonProperty(MASTER_VARIANT)
	private ProductVariant masterVariant;

	@JsonProperty("variants")
	private List<ProductVariant> variants;

	public Map<String, String> getName() {
		return name;
	}

	public void setName(Map<String, String> name) {
		this.name = name;
	}

	public List<CategoryReference> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryReference> categories) {
		this.categories = categories;
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	public Map<String, String> getDescription() {
		return description;
	}

	public void setDescription(Map<String, String> description) {
		this.description = description;
	}

	public Map<String, String> getSlug() {
		return slug;
	}

	public void setSlug(Map<String, String> slug) {
		this.slug = slug;
	}

	public Map<String, String> getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(Map<String, String> metaTitle) {
		this.metaTitle = metaTitle;
	}

	public Map<String, String> getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(Map<String, String> metaDescription) {
		this.metaDescription = metaDescription;
	}

	public Map<String, String> getMetaKeywords() {
		return metaKeywords;
	}

	public void setMetaKeywords(Map<String, String> metaKeywords) {
		this.metaKeywords = metaKeywords;
	}

	public ProductVariant getMasterVariant() {
		return masterVariant;
	}

	public void setMasterVariant(ProductVariant masterVariant) {
		this.masterVariant = masterVariant;
	}

	public List<ProductVariant> getVariants() {
		return variants;
	}

	public void setVariants(List<ProductVariant> variants) {
		this.variants = variants;
	}

	@Override
	public String toString() {
		return "ProductData [name=" + name + ", categories=" + categories + ", values=" + values + ", description="
				+ description + ", slug=" + slug + ", metaTitle=" + metaTitle + ", metaDescription=" + metaDescription
				+ ", metaKeywords=" + metaKeywords + ", masterVariant=" + masterVariant + ", variants=" + variants
				+ "]";
	}

	
}
