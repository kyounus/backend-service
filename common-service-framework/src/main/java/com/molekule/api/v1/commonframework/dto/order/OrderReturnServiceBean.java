package com.molekule.api.v1.commonframework.dto.order;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class OrderReturnServiceBean {
	private Long version;
	private List<Action> actions = null;

	@Data
	public static class Action {

		@JsonProperty(ACTION)
		private String actionName;
		private Type type;
		private Fields fields;
		private String name;
		private Object value;
		private String lineItemId;
		private String returnDate;
		private String returnTrackingId;
		private List<Items> items;

		@Data
		public static class Type {
			private String id;
			private String typeId;

		}

		@Data
		public static class Items {
			private int quantity;
			private String lineItemId;
			private String comment;
			private String shipmentState;

		}
		@Data
		public static class Fields {
			private String resolution; 
			private String functionalIssue;
			private String packageCondition;
			private String returnReasons; 
			private String returnLocation; 
			private String fedexReturnLable;
			private String failureSymptoms;
			private String returnQuantity;
			private List<String> serialNumbers;

		}
	}
}
