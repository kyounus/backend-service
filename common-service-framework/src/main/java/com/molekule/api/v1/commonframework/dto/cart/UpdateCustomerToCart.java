package com.molekule.api.v1.commonframework.dto.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class UpdateCustomerToCart {

	
	private Long version;
	private List<Action> actions = null;

	public List<Action> getActions() {
		if (actions != null) {
			return actions;
		}
		return new ArrayList<>();
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	
	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		private String customerId;
		private String anonymousId;
		private String email;
		
	}
}
