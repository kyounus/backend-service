package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class StockDetails is used to hold the Stock information.
 */
public class StockDetails {
	private String stockTickerName;
	private String stockExchange;
	
	public String getStockTickerName() {
		return stockTickerName;
	}
	
	public void setStockTickerName(String stockTickerName) {
		this.stockTickerName = stockTickerName;
	}

	public String getStockExchange() {
		return stockExchange;
	}
	
	public void setStockExchange(String stockExchange) {
		this.stockExchange = stockExchange;
	}
}
