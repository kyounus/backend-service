package com.molekule.api.v1.commonframework.util;

public enum CountryCodeEnum {

	GB("GBP"),
	EU("EUR"),
	US("USD"),
	XI("GBP"),
	IE(""),
	DE(""),
	AT(""),
	BE(""),
	HR(""),
	CY(""),
	CZ(""),
	DK(""),
	EE(""),
	FI(""),
	FR(""),
	GR(""),
	HU(""),
	IT(""),
	LV(""),
	LT(""),
	LU(""),
	MT(""),
	NL(""),
	PL(""),
	PT(""),
	RO(""),
	SK(""),
	SI(""),
	ES(""),
	SE("");
	
	private String currency;
	
	private CountryCodeEnum(String currency){
        this.currency = currency;
    }

	public String getCurrency() {
		return currency;
	}
}
