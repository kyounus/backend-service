package com.molekule.api.v1.commonframework.model.checkout;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AffirmPaymentOrderDTO {
	@ApiModelProperty(required = true)
	private String cartId;
	
	@ApiModelProperty(required = true)
	private String checkoutToken;
	
	@ApiModelProperty(required = true)
	private String amount;
	
	@ApiModelProperty(required = true)
	private String currency;

	@ApiModelProperty
	private String customerId;
	
	@ApiModelProperty
	private String paymentType;
}
