package com.molekule.api.v1.commonframework.model.customerprofile;

import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;

import lombok.Data;

@Data
public class GetSubscriptionBean {
	private int limit;
	private int offset;
	private String email;
	private String nextOrderDate;
	private String subscriptionId;
	private String customerId;
	private String channel;
	private SubscriptionSortEnum sortAction;
	private SortOrderEnum sortOrder;
}
