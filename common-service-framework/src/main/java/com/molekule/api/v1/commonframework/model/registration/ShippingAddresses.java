package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

@Data
public class ShippingAddresses {

	private String shippingAddressId;
	private String firstName;
	private String lastName;
	private String streetAddress1;
	private String streetAddress2;
	private String city;
	private String state;
	private String postalCode;
	private String phoneNumber;
	private String country;
	private String companyName;

}
