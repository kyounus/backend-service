package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class SubscriptionEstimate {

	private SubscriptionCustomerEstimateQuery subscriptionCustomerEstimateQuery;
}
