package com.molekule.api.v1.commonframework.util;

public class MolekuleConstant {

	private MolekuleConstant() {
	}

	public static final String CENT_AMOUNT = "centAmount";

	public static final String COUNTRY = "country";

	public static final String CURRENCY_CODE = "currencyCode";

	public static final String LINE_NUMBER = "lineNumber";

	public static final String TOTAL_TAX = "totalTax";

	public static final String SHIPPING_COST = "shippingCost";

	public static final String HANDLING_COST = "handlingCost";

	public static final String TOTAL_PRICE = "totalPrice";

	public static final String TOTAL_AMOUNT = "totalAmount";

	public static final String TAX_CALCULATED = "taxCalculated";

	public static final String OFF_SET = "&offset=";

	public static final String BEARER = "Bearer ";

	public static final String STATUS_CODE = "statusCode";

	public static final String VALUE = "value";

	public static final String TAX_EXEMPT_REVIEW = "taxExemptReview";

	public static final String COMPANY_CATEGORY = "companyCategory";

	public static final String SALES_REP_ID = "sales-rep-id";

	public static final String VERSION = "version";

	public static final String CONTAINER = "container";

	public static final String CUSTOM_OBJECTS = "/custom-objects";

	public static final String RESULTS = "results";

	public static final String FIRST_NAME = "firstName";

	public static final String STREET_ADDRESS_2 = "streetAddress2";

	public static final String STREET_ADDRESS_1 = "streetAddress1";

	public static final String AMOUNT = "amount";

	public static final String PRICE = "price";

	public static final String FREIGHT = "freight";

	public static final String CART = "/carts/";

	public static final String ORDERS = "/orders/";

	public static final String AUTHORIZATION = "Authorization";
	public static final String X_API_KEY="X-API-Key";

	public static final String CUSTOM = "custom";

	public static final String API_STATUS_CODE = "Api Status Code -{}";

	public static final String SET_CUSTOM_FIELD = "setCustomField";

	public static final String SET_CUSTOM_TYPE = "setCustomType";

	public static final String CUSTOMER = "customer";

	public static final String EMPTY = "";

	public static final String UNDEFINED = "undefined";

	public static final String BILLING_ADDRESS_ID = "billingAddressId";

	public static final String PAYMENT_TYPE = "paymentType";

	public static final String BUSINESS_TYPE = "businessType";

	public static final String TOTAL_NET_CHARGE_WITH_DUTIES_AND_TAXES = "TotalNetChargeWithDutiesAndTaxes";

	public static final String ATTRIBUTES = "attributes";

	public static final String EMAIL_VALIDATION = "There is already an existing customer with this email.";

	public static final String CURRENT = "current";

	public static final String CUSTOM_LINE_ITEMS = "customLineItems";

	public static final String EMAIL = "email";

	public static final String EN_US = "en-US";
	
	public static final String EN_GB = "en-GB";

	public static final String ERRORS = "errors";

	public static final String FIELDS = "fields";

	public static final String LAST_NAME = "lastName";

	public static final String LINE_ITEMS = "lineItems";

	public static final String MASTER_DATA = "masterData";

	public static final String MASTER_VARIANT = "masterVariant";

	public static final String MY_TAX_RATE = "myTaxRate";

	public static final String PHONE_NUMBER = "phoneNumber";

	public static final String POSTAL_CODE = "postalCode";

	public static final String SET_LINE_ITEM_TAX_AMOUNT = "setLineItemTaxAmount";

	public static final String SET_SHIPPING_METHOD_TAX_AMOUNT = "setShippingMethodTaxAmount";

	public static final String STATE = "state";

	public static final String SUBSCRIPTION_ENABLED = "subscriptionEnabled";

	public static final String SLASH_CUSTOMERS_SLASH = "/customers/";

	public static final String DELIVERY_TYPE = "deliveryType";

	public static final String SHIPPING_ADDRESS = "shippingAddress";

	public static final String BILLING_ADDRESS = "billingAddress";

	public static final String QUOTE = "Quote";

	public static final String PRODUCT_TYPE = "productType";

	public static final String PAYMENT_INTENT_ID = "paymentIntentId";

	public static final String PENDING = "Pending";

	public static final String PARCEL_DELIVERY_TYPE = "PARCEL";

	public static final String FREIGHT_DELIVERY_TYPE = "FREIGHT";

	public static final String PRODUCT_ID = "productId";

	public static final String QUANTITY = "quantity";

	public static final String VARIANT = "variant";

	public static final String READ_FLAG = "readFlag";

	public static final String PAYMENTTERMS = "PAYMENTTERMS";

	public static final String CREDITCARD = "CREDITCARD";

	public static final String CUSTOMER_ID = "customerId";

	public static final String PAYMENTS = "payments";

	public static final String SHIPPING_INFO = "shippingInfo";

	public static final String DISCOUNTED_PRICE = "discountedPrice";

	public static final String DISCOUNTED_PRICE_PER_QUANTITY = "discountedPricePerQuantity";

	public static final String DISCOUNTED_AMOUNT = "discountedAmount";

	public static final String INCLUDED_DISCOUNTS = "includedDiscounts";

	public static final String DISCOUNT_CODES = "discountCodes";

	public static final String PAYMENT_INFO = "paymentInfo";

	public static final String NEXT_ORDER_DATE_WITH_QUERY = "&where=value(nextOrderDate=\"";

	public static final String LIMIT = "?limit=";

	public static final String OFFSET = "&offset=";

	public static final String INTERFACE_ID = "interfaceId";

	public static final String PAYMENT_METHOD = "payment_method";

	public static final String ADDITIONAL_FREIGHT_INFOS = "additionalFreightInfos";

	public static final String PAYMENT_STATE = "paymentState";

	public static final String SCHEDULED = "Scheduled";

	public static final String CHARGES = "charges";

	public static final String DATA = "data";

	public static final String OWN_CARRIER = "ownCarrier";

	public static final String TOTAL_DISCOUNT_PRICE =  "totalDiscountPrice";

	public static final String DISCOUNT_CODE =  "discountCode";

	public static final String ADDRESSES = "addresses";

	public static final String DISCOUNTED = "discounted";

	public static final String FREE_SHIPPING_DISCOUNT = "freeShippingDiscount";

	public static final String MAX_RETRY = "maxRetry";

	public static final String OBJECT = "object";

	public static final String ORDER_TOTAL = "orderTotal";

	public static final String PARENT_ORDER_REFERENCE_ID = "parentOrderReferenceId";

	public static final String PERIOD = "period";

	public static final String PHONE = "phone";

	public static final String SUB_TOTAL = "subTotal";

	public static final String NEXT_ORDER_DATE = "nextOrderDate";

	public static final String FREIGHT_HOLD = "FreightHold";

	public static final String IMAGES = "images";

	public static final String MOLEKULE_CARRIER = "molekuleCarrier";

	public static final String ORDER = "order";

	public static final String CUSTOMER_STATUS = "customerStatus";

	public static final String PASSW0RD = "password";

	public static final String LINE_ITEM_STATE_TRANSITION = "LineItemStateTransition";

	public static final String ACTION_TYPE = "actionType";

	public static final String ACTION = "action";

	public static final String UPDATE ="update";

	public static final String DISCOUNT_MESSAGE = "discountMessage";

	public static final String NO_CART_DISCOUNTS_APPLIED = "No Cart Discounts are Applied";

	public static final String CANCELLED = "Cancelled";

	public static final String COMPANY_CATEGORIES = "companyCategories";

	public static final String HOLDS =  "holds";

	public static final String ITEMS = "items" ;

	public static final String PAYMENT_TYPE_CREDIT_CARD = "CreditCard";

	public static final String IMAGE_DESKTOP = "image_desktop";

	public static final String RECEIVING_HOURS = "receivingHours";

	public static final String ORDER_NUMBER =  "orderNumber";

	public static final String LOADING_DOCK =  "loadingDock";

	public static final String FORK_LIFT =  "forkLift";

	public static final String DELIVERY_APPOINTMENT_REQUIRED = "deliveryAppointmentRequired";

	public static final String TOKEN = "token";

	public static final String CUSTOMER_CART_ID = "customerCartId";

	public static final String CART_BASE_URL = "cartBaseUrl";

	public static final String ACCOUNT_ID = "accountId";

	public static final String WHERE = "&where=";

	public static final String ADD_LINE_ITEM ="addLineItem";

	public static final String SHIPPING_METHOD_NAME = "shippingMethodName";

	public static final String PAYMENT_METHOD_INFO = "paymentMethodInfo";

	public static final String  PRODUCT = "product";

	public static final String  LAST4 = "last4";

	public static final String  BRAND = "brand";

	public static final String  B2B = "B2B";

	public static final String  D2C = "D2C";

	public static final String  IS_A_GIFT = "gift";

	public static final String  REMOVE_LINE_ITEM = "removeLineItem";

	public static final String  __TYPE = "__type";

	public static final String SECRET_BLOCK = "SECRET_BLOCK";

	public static final String SRP_B = "SRP_B";

	public static final String USERNAME = "USERNAME";

	public static final String USER_ID_FOR_SRP = "USER_ID_FOR_SRP";

	public static final String SALT = "SALT";

	public static final String CLIENT_ID = "ClientId";

	public static final String ACCESS_TOKEN = "AccessToken";

	public static final String REFRESH_TOKEN_UPPERCASE = "REFRESH_TOKEN";

	public static final String SECRET_HASH = "SecretHash";

	public static final String SECRET_HASH_UPPERCASE = "SECRET_HASH";

	public static final String  CHANGE_LINE_ITEM_QUANTITY = "changeLineItemQuantity";

	public static final String PROTECTION_PLAN_LINE_ITEM="protectionPlanLineItem";

	public static final String PROMOTION_MESSAGE="promotionMessage";

	public static final String PROMOTION_PRODUCT="promotionProduct";

	public static final String TIMESTAMP = "TIMESTAMP";

	public static final String HMAC_SHA256 = "HmacSHA256";

	public static final String CHANNEL = "channel";

	public static final String PRODUCTS = "/products/";

	public static final String MONEY = "money";

	public static final String CUSTOMER_ID_WITH_QUERY = "&where=value(customer(id=\"";

	public static final String ANONYMOUSID = "anonymousId";

	public static final String STORE = "store";

	public static final String CREATED_AT = "createdAt";

	public static final String DEVICE_ID = "deviceId";

	public static final String STATUS ="status";

	public static final String LAST_MODIFIED_AT ="lastModifiedAt";

	public static final String ID_WITH_QUERY = "&where=id=\"";

	public static final String D2C_PAYMENT = "/d2c-payment/";

	public static final String COMPANY = "company";

	public static final String IS_BUSINESS = "isBusiness";

	public static final String ID = "id";

	public static final String  TOTAL = "total";
	public static final String  TRACKING_DATA = "trackingData";

	public static final String COMPANY_NAME = "companyName";

	public static final String DATE_OF_BIRTH = "dateOfBirth";

	public static final String CUSTOMER_GROUP = "customerGroup";

	public static final String LIFE_TIME_SALES = "lifeTimeSales";

	public static final String LAST_MODIFIED_DESC = "&sort=lastModifiedAt desc";

	public static final String CART_ID = "cartId";

	public static final String SERIAL_NUMBERS = "serialNumbers";

	public static final String CA = "CA";

	public static final String FEDEX_GROUND = "FEDEX_GROUND";

	public static final String GROUND = "Ground";

	public static final String  PRICE_CURRENCY = "priceCurrency=";

	public static final String  PRICE_COUNTRY = "&priceCountry=";
	
	public static final String UNAUTHORIZED_MESSAGE = "Unauthorized access token ..";
	
	public static final String EXPAND_CUSTOMER_ID = "&expand=value.customer.id";
	
	public static final String STREET_NUMBER = "streetNumber";
	
	public static final String STREET_NAME = "streetName";
	
	public static final String AND_SORT = "&sort=";
	
	public static final String USER_ATTRIBUTES = "userAttributes";
	
	public static final String ACCOUNT_LOCKED = "accountLocked";
	
	public static final String SET_BILLING_ADDRESS = "setBillingAddress";
	
	public static final String CUSTOMER_GROUP_NAME = "customerGroupName";
	
	public static final String ERROR = "error";
	
	public static final String RETURN_INFO = "returnInfo";
	
	public static final String LINE_ITEM_ID = "lineItemId";
	
	public static final String GBR = "GBR";
	
	public static final String CUSTOMER_NUMBER = "customerNumber";
	
}
