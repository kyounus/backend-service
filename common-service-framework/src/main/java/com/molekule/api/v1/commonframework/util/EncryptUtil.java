package com.molekule.api.v1.commonframework.util;

// Java program to demonstrate the creation
// of Encryption and Decryption with Java AES
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptUtil {

	private static final String SECRET_KEY = "24rIRq4B5kxSeSGTYGwRn18MrxNHnlc4LjIxmKZp";

	private static final String SALT = "WewNWx57oQnysZ03NVev2WhMp4SYc0vDHBnSunEE";

	private static final String ALGO = "PBKDF2WithHmacSHA256";

	private static final String PADDING = "AES/CBC/PKCS5PADDING";

	private static final String AES1 = "AES";

	private static final byte[] BYTE_VALUES = { 0, 1, 1, 1, -7, -9, 1, -1, -3, -2, -60, -5, 3, -9, -6, -10 };

	// This method use to encrypt to string
	private static String encrypt(String strToEncrypt) {
		try {

			// Create default byte array
			IvParameterSpec ivspec = new IvParameterSpec(BYTE_VALUES);

			// Create SecretKeyFactory object
			SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGO);

			// Create KeySpec object and assign with
			// constructor
			KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), AES1);

			Cipher cipher = Cipher.getInstance(PADDING);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
			// Return encrypted string
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
		} catch (Exception e) {
			return null;
		}
	}

	// This method use to decrypt to string
	public static String decrypt(String strToDecrypt) {

		try {
			IvParameterSpec ivspec = new IvParameterSpec(BYTE_VALUES);

			// Create SecretKeyFactory Object
			SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGO);

			// Create KeySpec object and assign with
			// constructor
			KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), AES1);

			Cipher cipher = Cipher.getInstance(PADDING);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
			// Return decrypted string
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		encrypt("test");
	}

}
