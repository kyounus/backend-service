package com.molekule.api.v1.commonframework.model.checkout;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReadFlagResponseBean {

	private String orderId;

	private Boolean readStatus;

	@Override
	public String toString() {
		return "{\r\n" + "    \"orderId\":\"" + orderId + "\",\r\n" + "    \"readStatus\":" + readStatus + "\r\n"
				+ "\r\n" + "}";
	}

}