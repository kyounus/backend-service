package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("taxExemptionSuccessConfirmationSender")
public class TaxExemptionSuccessConfirmationSender extends TemplateMailRequestHelper{
	
	@Value("${taxExemptionSuccessConfirmationTemplateId}")
	private String templateId;
	
	@Value("${taxExemptionStatusUrl}")
	private String statusURL;

	@Override
	public String templateId() {
		return templateId;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public void setStatusURL(String statusURL) {
		this.statusURL = statusURL;
	}
}
