package com.molekule.api.v1.commonframework.dto.cart;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;

import lombok.Data;
@Data
public class UpdateCartServiceBean {

	private Long version;
	private List<Action> actions = null;

	public List<Action> getActions() {
		if (actions != null) {
			return actions;
		}
		return new ArrayList<>();
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	@Data
	public static class Action {

		@JsonProperty(ACTION)
		private String actionName;
		private String productId;
		private long variantId;
		private String priceMode;
		private String lineItemId;
		private String customerId;
		private int quantity;
		@JsonProperty("address")
		private AddressRequestBean shippingAddress;
		private Type type;
		private Fields fields;
		private Name name;
		private Money money;
		private String slug;
		private String customLineItemId;
		private String shippingMethodName;
		private ShippingRate shippingRate;
		private ExternalTaxAmount externalTaxAmount;
		private ExternalTotalPrice externalTotalPrice;
		private TaxCategory taxCategory;
		private String sku;
		private String channel;
		private DistributionChannel distributionChannel; 
		
		@Data
		public static class DistributionChannel{
			private String id;
			private String typeId;
		}
		
		@Data
		public static class TaxCategory{
			
			private String id;
			private String typeId;
		}
		@Data
		public static class ExternalTotalPrice{
			private Price price;
			private TotalPrice totalPrice;
			@Data
			public static class Price{
				private String currencyCode;
				private long centAmount;
			}
			@Data
			public static class TotalPrice{
				private String currencyCode;
				private long centAmount;
			}
		}
		@Data
		public static class ExternalTaxAmount{
			private TotalGross totalGross;
			private TaxRate taxRate;
			@Data
			public static class TotalGross{
				private long centAmount;
				private String currencyCode;
			}
			@Data
			public static class TaxRate{
				private String name;
				private double amount;
				private String country;
				private boolean includedInPrice;
			}
		}
		@Data
		public static class ShippingRate{
			private Price price;
			@Data
			public static class Price{
				private String currencyCode;
				private Long centAmount;
			}
		}

		public static class Type {
			private String id;
			private String typeId;

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public String getTypeId() {
				return typeId;
			}

			public void setTypeId(String typeId) {
				this.typeId = typeId;
			}
		}

		public static class Fields {

			private boolean subscriptionEnabled;
			public boolean isSubscriptionEnabled() {
				return subscriptionEnabled;
			}

			public void setSubscriptionEnabled(boolean subscriptionEnabled) {
				this.subscriptionEnabled = subscriptionEnabled;
			}
		}

		public Type getType() {
			return type;
		}

		public void setType(Type type) {
			this.type = type;
		}

		public Fields getFields() {
			return fields;
		}

		public void setFields(Fields fields) {
			this.fields = fields;
		}

		public String getLineItemId() {
			return lineItemId;
		}

		public void setLineItemId(String lineItemId) {
			this.lineItemId = lineItemId;
		}

		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public String getProductId() {
			return productId;
		}

		public long getVariantId() {
			return variantId;
		}

		public void setVariantId(long variantId) {
			this.variantId = variantId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}

		public int getQuantity() {
			return quantity;
		}

		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}

		public AddressRequestBean getShippingAddress() {
			return shippingAddress;
		}

		public void setShippingAddress(AddressRequestBean shippingAddress) {
			this.shippingAddress = shippingAddress;
		}
		@Data
		public static class Name {
			private String en;
		}
		@Data
		public static class Money {
			private String currencyCode;
			private long centAmount;
		}

	}
}
