package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CarrierOptionCustomObject  extends AdditionalFreightInfo{

	private String carrierId;
	
	private CarrierType type;
	
	private String carrierName;
	
	private String carrierAccountNumber;
	
	private String billingAddressId;
	
	private List<String> parcelAndFrieghtIds;

}
