package com.molekule.api.v1.commonframework.model.checkout;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClientSecret {
	
    @JsonProperty("clientSecret")
	private String clientSecretValue;
}
