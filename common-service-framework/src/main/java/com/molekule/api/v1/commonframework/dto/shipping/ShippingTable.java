package com.molekule.api.v1.commonframework.dto.shipping;

import lombok.Data;

/**
 * The class ShippingTable is used to hold the shipping table custom object data.
 *
 */
@Data
public class ShippingTable {

	private String id;
	private String container;
	private String key;
	private ShippingTableValue value;
	
	@Data
	public static class ShippingTableValue{
		private Pallet pallet;
		private HandlingCosts handlingCosts;
		private String freightClass;
		private String markUpPercentage;
		private SourceAddress sourceAddress;
		private SourceAddress sourceAddress2;
		private Freight freight;
		
		@Data
		public static class Pallet{
			private PalletUnitValue length;
			private PalletUnitValue height;
			private PalletUnitValue width;
			private PalletUnitValue weight;
			
			@Data
			public static class PalletUnitValue{
				private String unit;
				private String value;
			}
		}

		@Data
		public static class HandlingCosts{
			private Parcel parcel;
			private Freight freight;
			
			@Data
			public static class Parcel{
				private HandlingCostsUnitPrice pick;
				private HandlingCostsUnitPrice pack;
				private HandlingCostsUnitPrice ship;
				private HandlingCostsUnitPrice orderCharge;
				private HandlingCostsUnitPrice snScan;
			}
			
			@Data
			public static class Freight{
				private AmountCurrency perPallet;
				private AmountCurrency perSku;
				private HandlingCostsUnitPrice orderCharge;
				private HandlingCostsUnitPrice snScan;
				
			}
			
			@Data
			public static class HandlingCostsUnitPrice{
				private String unit;
				private AmountCurrency price;
				
			}
			
			@Data
			public static class AmountCurrency {
				private String amount;
				private String currencyCode;
			}
		}
		
		@Data
		public static class SourceAddress{
			private String streetAddress1;
			private String streetAddress2;
			private String city;
			private String state;
			private String postalCode;
			private String country;
			private String alomAccount;
		}
		
		@Data
		public static class Freight{
			private Threshold threshold;
			
			@Data
			public static class Threshold{
				private Integer device;
				private Integer filter;
			}
		}
	}
}
