package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("orderUpdatedMailSender")
public class OrderUpdatedMailSender extends TemplateMailRequestHelper {

	@Value("${orderUpdatedTemplateId}")
	private String templateId;
	
	@Value("${orderDashboardUrl}")
	private String orderDashboardUrl;
	
	@Value("${paymentDocumentUrl}")
	private String paymentDocumentUrl;
	
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setRequireFreightInformtion(sendGridModel.isMissingFreightInformation());
		dynamicTemplateData.setOrderUrl(orderDashboardUrl);
		dynamicTemplateData.setRequirePaymentInformation(sendGridModel.isNonCreditCardPayment());
	    dynamicTemplateData.setPaymentDocumentUrl(paymentDocumentUrl);
		dynamicTemplateData.setSalesRepresentativeFirstName(sendGridModel.getSalesRepFirstName());
		dynamicTemplateData.setSalesRepresentativeLastName(sendGridModel.getSalesRepLastName());
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		dynamicTemplateData.setSalesRepresentativeCalendlyLink(sendGridModel.getSalesRepCalendyLink());
		return dynamicTemplateData;
	}

}
