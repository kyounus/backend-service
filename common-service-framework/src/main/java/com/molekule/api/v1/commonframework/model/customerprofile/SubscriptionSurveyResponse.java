package com.molekule.api.v1.commonframework.model.customerprofile;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class SubscriptionSurveyResponse {
	
	@JsonProperty("results")
	private List<SurveyQuestion> surveyQuestions;

	@Data
	public static class SurveyQuestion {
		
		@JsonProperty("id")
		private String id;
		
		@JsonProperty("question")
		private String question;
		
		@JsonProperty(CHANNEL)
		private String channel;
		
	}
}
