package com.molekule.api.v1.commonframework.model.order;

import lombok.Data;

@Data
public class ReprocessOrderDTO {
	
	String orderNumber;
	String payload;
	String status;
	String createdAt;
	String lastModifiedAt;
	String error;
}
