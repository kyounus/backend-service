package com.molekule.api.v1.commonframework.model.checkout;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ReccurringDetails {

	@JsonProperty("recurringDetailReference")
	private String recurringDetailReference;
}

