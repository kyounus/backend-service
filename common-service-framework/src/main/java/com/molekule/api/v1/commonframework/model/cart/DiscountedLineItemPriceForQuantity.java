package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.DISCOUNTED_PRICE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscountedLineItemPriceForQuantity {

    @JsonProperty(QUANTITY)
    private Double quantity;

    @JsonProperty(DISCOUNTED_PRICE)
    private DiscountedLineItemPrice discountedPrice;

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public DiscountedLineItemPrice getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(DiscountedLineItemPrice discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
    
    
}
