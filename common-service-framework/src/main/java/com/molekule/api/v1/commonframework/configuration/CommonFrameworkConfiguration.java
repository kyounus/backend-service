package com.molekule.api.v1.commonframework.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.molekule.api.v1.commonframework.util.EncryptUtil;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The class MolekuleBeanConfiguration is used to configure the beans for the
 * application.
 */
@Configuration
@EnableSwagger2
public class CommonFrameworkConfiguration {

	@Value("${ct.aws.access.key.id}")
	private String awsAccessKeyId;

	@Value("${ct.aws.access.secret.key}")
	private String awsSecretKey;

	@Value("${ct.aws.region}")
	private String awsRegion;
	
	@Value("${ct.cognitoUserPool}")
	private String cognitoUserPool;
    
    @Value("${ct.cognitoClientId}")
	private String cognitoClientId;
    
    @Value("${ct.cognitoClientSecret}")
	private String cognitoClientSecret;

	/**
	 * getDocket bean is written to get all apis in the swagger ui.
	 * 
	 * @return Docket
	 */
	@Bean
	public Docket getDocket() {
		Docket swaggerDoc = new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).build();
		return swaggerDoc.apiInfo(getApiInfo());
	}

	/**
	 * crosConfigure bean is written to configure the cross platform applications.
	 * 
	 * @return WebMvcConfigurer
	 */
	@Bean
	public WebMvcConfigurer crosConfigure() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST", "DELETE").allowedOrigins("*")
						.allowedHeaders("*");
				WebMvcConfigurer.super.addCorsMappings(registry);
			}

		};
	}

	/**
	 * getApiInfo method is used to get the api information.
	 * 
	 * @return ApiInfo
	 */
	public ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("Molekule Integration Api's")
				.description("exposed integration api's from molekule to ct").build();
	}

	@Bean(name = "awsCredentialsProvider")
	public AWSCredentialsProvider getAWSCredentials() {
		return new AWSStaticCredentialsProvider(new BasicAWSCredentials(EncryptUtil.decrypt(this.awsAccessKeyId), EncryptUtil.decrypt(this.awsSecretKey)));
	}

	@Bean(name = "dynamoDB")
	public DynamoDB getDynamoDB() {
		return new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(awsRegion)
				.withCredentials(getAWSCredentials()).build());
	}
}
