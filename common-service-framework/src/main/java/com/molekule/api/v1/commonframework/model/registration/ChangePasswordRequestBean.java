package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

@Data
public class ChangePasswordRequestBean {
	private String accessToken;
	private String previousPassword;
	private String proposedPassword;
}
