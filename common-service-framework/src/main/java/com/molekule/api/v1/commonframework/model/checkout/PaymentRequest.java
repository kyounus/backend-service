package com.molekule.api.v1.commonframework.model.checkout;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(description = "Payment Model")
@Data
public class PaymentRequest {
	@ApiModelProperty
	private String amount;
	@ApiModelProperty
	private String customerId;
	@ApiModelProperty
    private String paymentToken;
	@ApiModelProperty
	private boolean paymentKeyOnly;
	@ApiModelProperty
	private String cartId;
	@ApiModelProperty
	private String encryptedCardNumber;
	@ApiModelProperty
    private String encryptedExpiryMonth;
	@ApiModelProperty
	private String encryptedExpiryYear;
	@ApiModelProperty
	private String encryptedSecurityCode;
}

