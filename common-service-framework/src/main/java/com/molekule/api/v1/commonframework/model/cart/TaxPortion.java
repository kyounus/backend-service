package com.molekule.api.v1.commonframework.model.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class TaxPortion {

	@JsonProperty("name")
    private String name;

    @JsonProperty("rate")
    private Double rate;

    @JsonProperty("amount")
    private TypedMoney amount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public TypedMoney getAmount() {
		return amount;
	}

	public void setAmount(TypedMoney amount) {
		this.amount = amount;
	}
    
    
}
