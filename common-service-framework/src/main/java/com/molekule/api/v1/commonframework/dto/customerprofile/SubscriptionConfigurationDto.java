package com.molekule.api.v1.commonframework.dto.customerprofile;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SubscriptionConfigurationDto {
	@JsonProperty(CONTAINER)
	private String container;
	
	@JsonProperty("key")
	private String key;
	
	@JsonProperty(VERSION)
	private long version;
	
	@JsonProperty(VALUE)
	private Value value;
	
	@Data
	public static class Value {
		@JsonProperty("blackout")
		private Blackout blackout;
		@JsonProperty("retry-delay")
		private RetryDelay retryDelay;
		@JsonProperty("shipping")
		private Shipping shipping;
		@JsonProperty("trigger-date")
		private TriggerDate triggerDate;
		
		@Data
		public static class Blackout {
			private String startTime;
			private String endTime;
			
		}
		@Data
		public static class RetryDelay {
			private int first;
			private int second;
			private int third;
		}
		@Data
		public static class Shipping {
			@JsonProperty("option")
			private Option option;
			@Data
			public static class Option {
				@JsonProperty("name")
				private String name;
				@JsonProperty("flag")
				private boolean flag;
			}
		}
		@Data
		public static class TriggerDate {
			private boolean enabled;
			private String date;
		}
		
	}

}
