package com.molekule.api.v1.commonframework.model.products;

import java.util.List;

import com.molekule.api.v1.commonframework.model.registration.ProductResponse;

import lombok.Data;

@Data
public class ProductListResponse {

	private int limit;
	private int offset;
	private int count;
	private int total;
	private List<ProductResponse> productResponseList;
}
