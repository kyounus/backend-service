package com.molekule.api.v1.commonframework.model.customerprofile;

import lombok.Data;

@Data
public class DeviceRequestBean {
	private String serialNumber;
	private String sku;
	private String purchaseDate;
	private String channel;
	private boolean gift;
	private String retailSeller;
	private String registraionDate; 
	private String comments;
	private String customerId;
	private String customerNumber;
}
