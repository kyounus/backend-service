package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

@Data
public class CreditCard {

	private String expMonth;
	private String expYear;
	private String last4;
	private String brand;
}
