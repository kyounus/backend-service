package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;
/**
 * AccountCustomResponseBean is used to hold the  request data of company information.
 * 
 */
@Data
public class AccountCustomResponseBean {
	private String numberOfEmployees;
	private String numberOfOfficeSpaces;
	private String companyName;
	private String companyCategoryId;
}
