package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class OrderDTO {

	private String id;
	private long version;
	private String orderNumber;
	private State state;
	@Data
	public static  class State{
		private String id;
		private String typeId;
	}
}
