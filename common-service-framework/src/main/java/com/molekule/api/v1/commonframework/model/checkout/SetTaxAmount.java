package com.molekule.api.v1.commonframework.model.checkout;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

@Data
public class SetTaxAmount {

	private long version;
	private List<Action> actions;
	private String key;
	private Resource resource;
	@Data
	public static class Resource{
		private String typeId;
		private String id;
	}
	private List<Action> stagedActions;
	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		private String lineItemId;
		private Type type;
		private Fields fields;
		private String name;
		private String value;
		private String customLineItemId;
		private ExternalTaxAmount externalTaxAmount;
		private ExternalTotalGross externalTotalGross;
		private List<SetTaxAmount.Action.TaxPortion> externalTaxPortions;
		@Data
		public static class TaxPortion{
			private String name;
			private double rate;
			private Money amount;
			@Data
			public static class Money{
				private String currencyCode;
				private long centAmount;
			}
		}
        @Data
		public static class Fields{
		   private String totalTax;
		}
		@Data
		public static class Type{
			private String id;
			private String typeId;
		}
		@Data
		public static class ExternalTotalGross{
			private String currencyCode;
			private long centAmount;
		}
		@Data
		public static class ExternalTaxAmount {
			private TotalGross totalGross;
			private TaxRate taxRate;

			@Data
			public static class TotalGross {
				private String currencyCode;
				private long centAmount;
			}

			@Data
			public static class TaxRate {
				private String name;
				private double amount;
				private String country;
			}
		}
	}
}
