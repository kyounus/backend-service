package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;

import com.fasterxml.jackson.annotation.JsonProperty;
public class ItemState {

    @JsonProperty(QUANTITY)
    private Double quantity;

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
    
}
