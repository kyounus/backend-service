package com.molekule.api.v1.commonframework.util;

public class OrderServiceUtility {
	public enum ORDER_CANCELLABLE_STATE
	{
		BACKORDERED("Backordered"),
		INITIAL("Initial"),
		ACCEPTED("Accepted");

		private String state;

		public String getValue()
		{
			return this.state;
		}

		private ORDER_CANCELLABLE_STATE(String state)
		{
			this.state = state;
		}
	}

	public enum RMASTATUS
	{
		APPROVED("Approved"),
		PENDINGAPPROVAL("PendingApproval"),
		CLOSED("Closed");

		private String status;

		public String getValue()
		{
			return this.status;
		}

		private RMASTATUS(String status)
		{
			this.status = status;
		}
	}
}
