package com.molekule.api.v1.commonframework.mail.sendgrid;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("subscriptionPaymentFailedMailSender")
public class SubscriptionPaymentFailedMailSender extends TemplateMailRequestHelper {
	
	@Value("${subscriptionPaymentFailedTemplateId}")
	private String templateId;
	
	@Value("${refillFilterb2bAccountDashBoardURL}")
	private String filterAutoRefillsUrl;
	
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setFilterAutoRefillsUrl(filterAutoRefillsUrl);
		if(sendGridModel.getFailedRetries()==1) {
			dynamicTemplateData.setEmailHeader("There was an issue processing your auto-refill payment.");
			dynamicTemplateData.setEmailBody("Your credit card was declined, and we were unable to renew your filter subscription. Please update your account payment information, and we’ll get your auto-refill subscription renewed and replacement filters sent out on schedule.");
		}else if(sendGridModel.getFailedRetries()==2) {
			dynamicTemplateData.setEmailHeader("There’s still an an issue processing your auto-refill payment.");
			dynamicTemplateData.setEmailBody("Your credit card was declined a second time, and we were unable to renew your filter subscription. To continue your auto-refill subscription and receive your replacement filters, please update your account payment information.");
		}else if(sendGridModel.getFailedRetries()==3) {
			dynamicTemplateData.setEmailHeader("Your auto-refill payment has failed for a third time.");
			dynamicTemplateData.setEmailBody("Your credit card was declined a third time. We’ll make one final attempt to charge your card and renew your filter subscription. To continue your auto-refill subscription and receive your replacement filters, please update your account payment information. Thanks."); 
		}else if(sendGridModel.getFailedRetries()==4) {
			dynamicTemplateData.setEmailHeader("This was our final attempt to charge your card.");
			dynamicTemplateData.setEmailBody("Your credit card was declined again. We’ll no longer attempt to try this card. Your auto-refill subscription will not be renewed, and you will no not receive filters until your payment information has been updated in your account.");
		}
		return dynamicTemplateData;
	}
}
