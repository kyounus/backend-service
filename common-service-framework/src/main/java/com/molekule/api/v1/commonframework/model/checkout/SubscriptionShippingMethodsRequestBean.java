package com.molekule.api.v1.commonframework.model.checkout;

import java.time.LocalDate;

import lombok.Data;

@Data
public class SubscriptionShippingMethodsRequestBean {

	private LocalDate orderDate;
	private AddressRequestBean shippingAddress;
}
