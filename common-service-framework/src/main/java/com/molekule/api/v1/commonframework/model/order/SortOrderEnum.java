package com.molekule.api.v1.commonframework.model.order;

public enum SortOrderEnum {

	ASC,
	
	DESC
}
