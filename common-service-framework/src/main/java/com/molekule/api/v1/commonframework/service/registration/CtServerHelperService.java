package com.molekule.api.v1.commonframework.service.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.HOLDS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.SET_CUSTOM_FIELD;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccessTokenDTO;
import com.molekule.api.v1.commonframework.model.checkout.ClearHoldState;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Service("ctServerHelperService")
public class CtServerHelperService {

	Logger logger = LoggerFactory.getLogger(CtServerHelperService.class);

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	/**
	 * getAccessToken method is used to get the authorized tokens from the CT
	 * authorization.
	 * 
	 * @return String - it contains the response of the AccessTokenDTO.
	 */
	public String getAccessToken() {
		String baseUrl = ctEnvProperties.getAuthUrl() + "/" + ctEnvProperties.getAccessTokenUrl() + "?grant_type="
				+ ctEnvProperties.getGranttype();
		logger.trace("Url...{}", baseUrl);
		String token = new StringBuilder().append("Basic ").append(ctEnvProperties.getBasicAuthValue()).toString();

		WebClient webClient = fetchWebclient(baseUrl);
		Mono<Object> accessToken = webClient.post().accept(MediaType.APPLICATION_JSON)
				.header("Authorization", token).exchangeToMono(clientResponse -> {
					logger.trace("Access Token Api Status Code - {}", clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						throw new CustomRunTimeException("Unable to get Access Token From the CT Server");
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty()));
				    }
					return clientResponse.bodyToMono(AccessTokenDTO.class);
				});
		AccessTokenDTO accessTokenResponse = (AccessTokenDTO) accessToken.block();
		if(accessTokenResponse != null) {
			return accessTokenResponse.getAccessToken();
		}
		return null;
	}

	/**
	 * fetchWebclient method is used to fetch the web client.
	 * 
	 * @param url
	 * @return WebClient
	 */
	public WebClient fetchWebclient(String url) {
		if(ctEnvProperties.isHttpclientEnableFlag()) {
			HttpClient client = HttpClient.create().doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(ctEnvProperties.getHttpClientResponseTimeout())).addHandlerLast(new WriteTimeoutHandler(ctEnvProperties.getHttpClientResponseTimeout())));
			client.disableRetry(true);
			ClientHttpConnector connector = new ReactorClientHttpConnector(client.wiretap(true));
			return WebClient.builder().clientConnector(connector).baseUrl(url).build();
		}
		return WebClient.builder().baseUrl(url).build();
	}

	/**
	 * getStringAccessToken method is used to get the bearer token string to pass
	 * this to the api call for authorization.
	 * 
	 * @return String
	 */
	public String getStringAccessToken() {
		String accessToken = getAccessToken();
		return new StringBuilder().append(BEARER).append(accessToken).toString();

	}

	public static ClearHoldState getTransitionStateObj(long version, JsonObject orderResponse, String holdType) {
		ClearHoldState clearHoldState = new ClearHoldState();
		clearHoldState.setVersion(version);
		List<ClearHoldState.Action> actions = new ArrayList<>();
		ClearHoldState.Action customAction = new ClearHoldState.Action();
		customAction.setActionName(SET_CUSTOM_FIELD);
		customAction.setName(HOLDS);
		JsonArray holdsArray = orderResponse.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get(HOLDS)
				.getAsJsonArray();
		Set<String> holds = new HashSet<>();
		for (int i = 0; i < holdsArray.size(); i++) {
			if (!holdType.equals(holdsArray.get(i).getAsString())) {
				holds.add(holdsArray.get(i).getAsString());
			}
		}
		customAction.setValue(holds);
		actions.add(customAction);
		clearHoldState.setActions(actions);
		return clearHoldState;
	}
}
