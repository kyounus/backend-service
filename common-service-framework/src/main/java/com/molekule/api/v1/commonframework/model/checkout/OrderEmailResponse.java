package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class OrderEmailResponse {

	private String status;
}
