package com.molekule.api.v1.commonframework.model.registration;

import com.molekule.api.v1.commonframework.util.CustomerChannelEnum;
import com.molekule.api.v1.commonframework.util.CustomerSourceEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * The class RegistrationBean is used to hold the request of registration.
 */
@ApiModel(description = "Registration Model")
@Data
public class RegistrationBean {
	@ApiModelProperty(required = true)
	private String firstName;
	@ApiModelProperty(required = true)
	private String lastName;
	@ApiModelProperty(required = true)
	private String email;
	@ApiModelProperty(required = true)
	private String password;
	@ApiModelProperty(required = true)
	private String companyName;
	private String companyCategoryId;
	@ApiModelProperty(required = true)
	private String mobileNumber;
	@ApiModelProperty(required = false)
	private String accountId;
	@ApiModelProperty(required = false)
	private String customerGroupId;
	private CustomerChannelEnum channel;
	@ApiModelProperty(example = "US")
	private String countryCode;
	private String store;
	private String registeredChannel;
	private String comments;
	
	private CustomerSourceEnum customerSource;
	
	private boolean termsOfService;
	private boolean marketingOffers;
	
	

}
