package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

/**
 * CustomerProfileDetails is used for updating profile informations.
 */
@Data
public class CustomerProfileDetails {

	private String firstName;
	private String lastName;
	private String registeredChannel;
}
