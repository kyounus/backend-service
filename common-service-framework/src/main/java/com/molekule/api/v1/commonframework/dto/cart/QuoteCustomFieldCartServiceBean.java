package com.molekule.api.v1.commonframework.dto.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUOTE;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class QuoteCustomFieldCartServiceBean {
	private Long version;
	private List<Action> actions = null;
	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		
		private Type type;
		private Fields fields;
		private String name;
		private Object value;
		@Data
		public static class Type {
			private String id;
			private String typeId;
		}
		@Data
		public static class Fields {
			@JsonProperty(QUOTE)
			private boolean quote;
			private String quoteNumber;
		}
	}
}
