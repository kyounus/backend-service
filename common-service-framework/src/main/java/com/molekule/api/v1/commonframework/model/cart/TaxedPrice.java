package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class TaxedPrice {

	@JsonProperty("totalNet")
    private TypedMoney totalNet;

    @JsonProperty("totalGross")
    private TypedMoney totalGross;

    @JsonProperty("taxPortions")
    private List<TaxPortion> taxPortions;

	public TypedMoney getTotalNet() {
		return totalNet;
	}

	public void setTotalNet(TypedMoney totalNet) {
		this.totalNet = totalNet;
	}

	public TypedMoney getTotalGross() {
		return totalGross;
	}

	public void setTotalGross(TypedMoney totalGross) {
		this.totalGross = totalGross;
	}

	public List<TaxPortion> getTaxPortions() {
		return taxPortions;
	}

	public void setTaxPortions(List<TaxPortion> taxPortions) {
		this.taxPortions = taxPortions;
	}
    
    
}
