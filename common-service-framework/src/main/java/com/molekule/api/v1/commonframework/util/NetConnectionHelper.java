package com.molekule.api.v1.commonframework.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.API_STATUS_CODE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.AUTHORIZATION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.X_API_KEY;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import com.google.common.net.HttpHeaders;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountDto;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;

import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Service("netConnectionHelper")
public class NetConnectionHelper {

	Logger logger = LoggerFactory.getLogger(NetConnectionHelper.class);

	@Autowired
	CTEnvProperties ctEnvProperties;
	
	public WebClient getWebClient(String baseUrl) {
		if(ctEnvProperties.isHttpclientEnableFlag()) {
			HttpClient client = HttpClient.create().doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(ctEnvProperties.getHttpClientResponseTimeout())).addHandlerLast(new WriteTimeoutHandler(ctEnvProperties.getHttpClientResponseTimeout())));
			client.disableRetry(true);
			ClientHttpConnector connector = new ReactorClientHttpConnector(client.wiretap(true));
			return WebClient.builder().clientConnector(connector).baseUrl(baseUrl).build();
		}
		return WebClient.builder().baseUrl(baseUrl).build();
	}
	
	public Object sendPostRequest(String baseUrl, String token, Object requestData) {
		WebClient webClient = getWebClient(baseUrl);
		Mono<String> response = null;
		try {
		response = webClient.post().header(AUTHORIZATION, token).accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(requestData), Object.class).exchangeToMono(clientResponse -> {
					logger.info(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendPostRequest Method- "+ex);
			}
		return response.block();
	}
	
	public Object sendPostWithXApiKey(String baseUrl, String token, Object requestData) {
		WebClient webClient = getWebClient(baseUrl);
		Mono<String> response = null;
		try {
		response = webClient.post().header(X_API_KEY, token).accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(requestData), Object.class).exchangeToMono(clientResponse -> {
					logger.info(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendPostRequest Method- "+ex);
			}
		return response.block();
	}

	public Object sendFormUrlEncoded(String baseUrl, String token, MultiValueMap<String, String> formData) {
		WebClient webClient = getWebClient(baseUrl);
		Mono<String> response = null;
		try {
		response = webClient.post().header(AUTHORIZATION, token).accept(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromFormData(formData)).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		}catch (Exception ex) {
			logger.error("Error In sendFormUrlEncoded Method -"+ex);
		}
		return response.block();
	}

	/**
	 * sendGetWithoutBody method is used to send the get request through webclient
	 * to the CT api calls without the body.
	 * 
	 * @param token
	 * @param url
	 * @return String
	 */
	public String sendGetWithoutBody(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<String> response = null;
		try {
			response = client.get().header(HttpHeaders.AUTHORIZATION, token).accept(MediaType.APPLICATION_JSON)
					.exchangeToMono(clientResponse -> {
						logger.trace(API_STATUS_CODE, clientResponse.statusCode());
						if (clientResponse.statusCode().value() != 200) {
							/** Need to handle the error response */
						} else if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
							return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
						}
						return clientResponse.bodyToMono(String.class);
					});
		} catch (Exception ex) {
			logger.error("Error In sendGetWithoutBody Method-" + ex);
		}
		return response.block();
	}

	/**
	 * sendGetWithoutBody method is used to send the get request through webclient
	 * to the CT api calls without the body.
	 * 
	 * @param token
	 * @param url
	 * @return String
	 */
	public CustomerResponseBean sendGet(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<CustomerResponseBean> response = null;
		try {
		response = client.get().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(CustomerResponseBean.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendGet Method -"+ex);
		}
		return response.block();
	}

	/**
	 * sendGetProductDataWithoutBody method is used to send the get request through
	 * webclient to the CT api calls without the body.
	 * 
	 * @param token
	 * @param url
	 * @return ProductBean
	 */
	public ProductBean sendGetProductDataWithoutBody(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<ProductBean> response = null;
		try {
		response = client.get().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(ProductBean.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendGetProductDataWithoutBody Method -"+ex);
		}
		return response.block();
	}

	/**
	 * sendPostWithoutBody method is dummy method, deprecated in near future.
	 * 
	 * @param token
	 * @param url
	 * @param obj
	 * @return Object
	 */
	public AccountCustomObject sendPostWithoutBody(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<AccountCustomObject> response = null;
		try {
		response = client.get().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(AccountCustomObject.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendPostWithoutBody Method -"+ex);
		}
		return response.block();
	}

	/**
	 * sendGetForShippingTables method is used to get ShippingTable CUstom Object.
	 * 
	 * @param token
	 * @param url
	 * @return ShippingTable
	 */
	public ShippingTable sendGetForShippingTables(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<ShippingTable> response = null;
		try {
		response =  client.get().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());

					return clientResponse.bodyToMono(ShippingTable.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendGetForShippingTables Method - "+ex);
		}
		return response.block();
	}

	/**
	 * deleteRequest is used to make the delete request in CT.
	 * 
	 * @param baseUrl
	 * @param token
	 */
	public String deleteRequest(String baseUrl, String token) {
		WebClient client = getWebClient(baseUrl);
		Mono<String> response = null;
		try {
		response = client.delete().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		}catch(Exception ex) {
			logger.error("Error In deleteRequest Method -"+ex);
		}
		return response.block();

	}
	
	public AccountDto sendGetAccountObject(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<AccountDto> response = null;
		try {
		response = client.get().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(AccountDto.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendGetAccountObject Method -"+ex);
		}
		return response.block();
	}
	
	/**
	 * httpConnectionHelper is used to get the data from the CT server.
	 * @param customerBaseUrl
	 * @param token
	 * @return String
	 * @throws IOException
	 */
	public String httpConnectionHelper(String customerBaseUrl, String token) throws IOException {
		URL url = new URL(customerBaseUrl);
		HttpsURLConnection httpConnection = (HttpsURLConnection) url.openConnection();
		httpConnection.setRequestProperty (AUTHORIZATION, token);
		HttpURLConnection.setFollowRedirects(true);
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
		StringBuilder responseString = new StringBuilder();
		String line;
		while ((line = bufferReader.readLine()) != null) {
			responseString.append(line);
		}
		bufferReader.close();
		httpConnection.disconnect();
		return String.valueOf(responseString);
	}
	
	/**
	 * sendGetWithLimitSize method is used to send the get request through webclient
	 * to the CT api calls without the body.
	 * 
	 * @param token
	 * @param url
	 * @return String
	 */
	public String sendGetWithLimitSize(String token, String url) {
		ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder().codecs(configurer -> configurer
	                      .defaultCodecs().maxInMemorySize(1 * 1024 * 1024)).build();
		WebClient client = WebClient.builder().exchangeStrategies(exchangeStrategies).build();
		Mono<String> response = null;
		try {
		response  = client.get().uri(url).header(HttpHeaders.AUTHORIZATION, token).accept(MediaType.APPLICATION_JSON)
				.exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		}catch(Exception ex) {
			logger.error("Error In sendGetWithLimitSize Method-"+ex);
		}
		return response.block();
	}
	
	public Object sendPostRequestWithLimitSize(String baseUrl, String token, Object requestData) {
		ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder().codecs(configurer -> configurer
                .defaultCodecs().maxInMemorySize(1 * 1024 * 1024)).build();
		WebClient webClient = WebClient.builder().exchangeStrategies(exchangeStrategies).baseUrl(baseUrl).build();

		Mono<String> response = webClient.post().header(AUTHORIZATION, token).accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(requestData), Object.class).exchangeToMono(clientResponse -> {
					logger.info(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		return response.block();
	}
	
	public String sendCognitoPostRequest(String url,String request,CTEnvProperties ctEnvProperties,String target) {
		String cognitoStringResponse = null;
		WebClient webClient = getWebClient(url);
		Mono<String> cognitoResponse = null;
		try {
		cognitoResponse = webClient.post().accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.parseMediaType(ctEnvProperties.getCognitoHeaderContentType()))
				.header("x-amz-target", target)
				.body(BodyInserters.fromValue(request))
				.exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		}catch (Exception ex) {
			logger.error("Error In sendCognitoPostRequest Method -"+ex);
		}
		cognitoStringResponse = cognitoResponse.block();
		return cognitoStringResponse;
	}

	public PaymentCustomObject getPaymentCustomObject(String token, String url) {
		WebClient client = getWebClient(url);
		Mono<PaymentCustomObject> response = null; 
		try {
		response = client.get().header(HttpHeaders.AUTHORIZATION, token)
				.accept(MediaType.APPLICATION_JSON).exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					return clientResponse.bodyToMono(PaymentCustomObject.class);
				});
		}catch(Exception ex) {
			logger.error("Exception In getPaymentCustomObject Method -"+ex);
		}
		return response.block();
	}
	
	public String postNetSuitAPI(String url,Object request) {
		WebClient webClient = getWebClient(url);
		String nonce = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(5);
		String timeStamp =String.valueOf(System.currentTimeMillis() / 1000);
		String httpMethod = "POST";
		String signature = generateSignature(nonce, timeStamp,httpMethod,null);
		String token =  getToken(nonce, timeStamp, MolekuleUtility.convertToEncodeUri(signature));
		
		Mono<String> nsResponse = webClient.post().accept(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, token)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
				.body(BodyInserters.fromValue(request))
				.exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		return nsResponse.block();
	}

	public String getToken(String nonce, String timeStamp, String signature) {
		return new StringBuilder().append("OAuth ")
		.append("realm=\"").append(ctEnvProperties.getOAuthRealm()).append("\"")
		.append(",oauth_consumer_key=\"").append(ctEnvProperties.getOAuthConsumerKey()).append("\"")
		.append(",oauth_token=\"").append(ctEnvProperties.getOAuthToken()).append("\"")
		.append(",oauth_signature_method=\"").append(ctEnvProperties.getOAuthSignatureMethod()).append("\"")
		.append(",oauth_timestamp=\"").append(timeStamp).append("\"")
		.append(",oauth_nonce=\"").append(nonce).append("\"")
		.append(",oauth_version=\"").append(ctEnvProperties.getOAuthVersion()).append("\"")
		.append(",oauth_signature=\"").append(signature).append("\"").toString();
	}

	private String generateSignature(String nonce, String timeStamp, String httpMethod, String orderNumber) {
		String baseUrl = ctEnvProperties.getOAuthBaseUrl();
		String parameterString = null;
		if(orderNumber == null) {
			 parameterString = "deploy="+ ctEnvProperties.getOAuthSerialNumberValidationDeploymentId() +
					"&oauth_consumer_key="+ ctEnvProperties.getOAuthConsumerKey() +
					"&oauth_nonce="+ nonce +
					"&oauth_signature_method="+ ctEnvProperties.getOAuthSignatureMethod() +
					"&oauth_timestamp=" + timeStamp +
					"&oauth_token=" + ctEnvProperties.getOAuthToken() +
					"&oauth_version=" + ctEnvProperties.getOAuthVersion() +
					"&script=" + ctEnvProperties.getOAuthSerialNumberValidationScriptId();
		} else {
			 parameterString = "deploy="+ ctEnvProperties.getOAuthCancellationApiValidationDeploymentId() +
					"&oauth_consumer_key="+ ctEnvProperties.getOAuthConsumerKey() +
					"&oauth_nonce="+ nonce +
					"&oauth_signature_method="+ ctEnvProperties.getOAuthSignatureMethod() +
					"&oauth_timestamp=" + timeStamp +
					"&oauth_token=" + ctEnvProperties.getOAuthToken() +
					"&oauth_version=" + ctEnvProperties.getOAuthVersion() +
				 	"&orderid="+ orderNumber +
					"&script=" + ctEnvProperties.getOAuthCancellationApiValidationScriptId();
					 
		}
		
		String signatueBaseStr = httpMethod
				+ "&"
				+ MolekuleUtility.convertToEncodeUri(baseUrl)
				+ "&"
				+ MolekuleUtility.convertToEncodeUri(parameterString) ;
		
		String signingKey = MolekuleUtility.convertToEncodeUri(ctEnvProperties.getOAuthConsumerSecret()) +
				"&" + MolekuleUtility.convertToEncodeUri(ctEnvProperties.getOAuthTokenSecret());
		
		byte[] byteHMAC = null;  
	     try {  
	       Mac mac = Mac.getInstance("HmacSHA256");  
	       SecretKeySpec spec = new SecretKeySpec(signingKey.getBytes(), "HmacSHA256"); 
	       mac.init(spec);  
	       byteHMAC = mac.doFinal(signatueBaseStr.getBytes());  
	     } catch (Exception e) {  
	       e.printStackTrace();  
	     }  
	     return Base64.encodeBase64String(byteHMAC);  
	     
	}

	public String getNetSuitAPI(String nsUrl, String orderNumber) {
		WebClient webClient = getWebClient(nsUrl);
		String nonce = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(5);
		String timeStamp =String.valueOf(System.currentTimeMillis() / 1000);
		String httpMethod = "GET";
		String signature = generateSignature(nonce, timeStamp,httpMethod,orderNumber);
		String token =  getToken(nonce, timeStamp, MolekuleUtility.convertToEncodeUri(signature));
		
		Mono<String> nsResponse = webClient.get().accept(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, token)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
				.exchangeToMono(clientResponse -> {
					logger.trace(API_STATUS_CODE, clientResponse.statusCode());
					if (clientResponse.statusCode().value() != 200) {
						/** Need to handle the error response */
					}else  if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
					      return clientResponse.bodyToMono(Void.class).thenReturn((Optional.empty().toString()));
				    }
					return clientResponse.bodyToMono(String.class);
				});
		return nsResponse.block();
	}
	
}
