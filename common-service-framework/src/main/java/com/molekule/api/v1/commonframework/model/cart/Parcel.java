package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ITEMS;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Parcel {

	@JsonProperty("id")
	private String id;

	@JsonProperty("createdAt")
	private ZonedDateTime createdAt;

	@JsonProperty("measurements")
	private ParcelMeasurements measurements;

	@JsonProperty("trackingData")
	private TrackingData trackingData;

	@JsonProperty(ITEMS)
	private List<DeliveryItem> items;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public ParcelMeasurements getMeasurements() {
		return measurements;
	}

	public void setMeasurements(ParcelMeasurements measurements) {
		this.measurements = measurements;
	}

	public TrackingData getTrackingData() {
		return trackingData;
	}

	public void setTrackingData(TrackingData trackingData) {
		this.trackingData = trackingData;
	}

	public List<DeliveryItem> getItems() {
		return items;
	}

	public void setItems(List<DeliveryItem> items) {
		this.items = items;
	}

}
