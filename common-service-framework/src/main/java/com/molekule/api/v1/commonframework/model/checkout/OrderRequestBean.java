package com.molekule.api.v1.commonframework.model.checkout;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderRequestBean {
	@ApiModelProperty(required = true)
	private String cartId;	
	@ApiModelProperty
	private String paymentToken;
	@ApiModelProperty
	private String paymentType;
	private AddressRequestBean billingAddress;
	private String shopperReference;
	
	private boolean termsOfService;
	private boolean marketingOffers;
	@ApiModelProperty
	private String encryptedCardNumber;
	@ApiModelProperty
    private String encryptedExpiryMonth;
	@ApiModelProperty
	private String encryptedExpiryYear;
	@ApiModelProperty
	private String encryptedSecurityCode;
}
