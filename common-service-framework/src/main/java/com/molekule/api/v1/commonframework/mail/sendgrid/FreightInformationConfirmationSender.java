package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("freightInformationConfirmationSender")
public class FreightInformationConfirmationSender extends TemplateMailRequestHelper {
	@Value("${freightInformationConfirmationTemplateId}")
	private String templateId;

	@Value("${freightInfoDashBoardURL}")
	private String accountDashBoardURL;

	@Override
	public String templateId() {
		return templateId;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setAccountDashboardUrl(accountDashBoardURL);
		return dynamicTemplateData;

	}
}
