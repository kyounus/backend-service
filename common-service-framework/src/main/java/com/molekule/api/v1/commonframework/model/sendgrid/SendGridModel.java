package com.molekule.api.v1.commonframework.model.sendgrid;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SendGridModel {

	private String toMail;
	private String toName;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String orderDate;
	private String customerId;
	private String accountId;
	private String customerCompanyName;
	@JsonProperty("CompanyIndustry")
	private String companyIndustry;
	private String customerRecordUrl;
	private String approvedStates;
	private String salesRepFirstName;
	private String salesRepLastName;
	private String salesRepPhoneNumber;
	private String salesRepEmail;
	private String salesRepCalendyLink;
	private String orderNumber;
	private String orderId;
	private List<Product> products;
	private List<Map<String,Object>> productlist;
	private String orderSummarySubtotal;
	private String orderSummaryShipping;
	private String orderSummaryHandling;
	private String orderSummaryTax;
	private String orderSummaryDiscount;
	private String orderSummaryTotal;
	private String orderPaymentMethod;
	private String orderShippingMethod;
	private String orderShippingAddressCompanyName;
	private String orderShippingAddressContactFirstName;
	private String orderShippingAddressContactLastName;
	private String orderShippingAddressStreet1;
	private String orderShippingAddressStreet2;
	private String orderShippingAddressCity;
	private String orderShippingAddressState;
	private String orderShippingAddressZip;
	private String orderShippingAddressCountry;
	private String orderShippingAddressPhoneNumber;
	private String orderBillingAddressFirstName;
	private String orderBillingAddressLastName;
	private String orderBillingAddressStreet1;
	private String orderBillingAddressStreet2;
	private String orderBillingAddressCity;
	private String orderBillingAddressState;
	private String orderBillingAddressZip;
	private String orderBillingAddressPhoneNumber;
	@JsonProperty("requestedDate")
	private String requestedDate;
	private boolean nonCreditCardPayment;
	private boolean paymentTermsApplication;
	private boolean taxExemptionRequest;
	private boolean missingFreightInformation;
	private boolean moreFreightInformation;
	private String orderMerchantCenterUrl;
	private String orderUrl;
	private String paymentDocumentUrl;
	private String paymentTermsStatusUrl;
	private String accountDashboardUrl;
	private String nextRefillDate;
	private List<String> nextRefilterDate;
	private String subscriptionTotal;
	private String quoteRequestNumber;
	private String cartId;
	private String orderFedExTrackingInfo;
	private String customerCarrierName;
	private String customerCarrierAccountNumber;
	private int failedRetries;
	private String invoiceUrl;
	private List<String> invoiceEmailAddresses;
	private boolean gift;
	private String returnNumber;
	private String returnLabelUrl;
	private String updatedEmail;
	private String customerPassword;
	private String subscriptionPlanName;
	private String subscriptionRenewalDate;
	private String subscriptionDeviceSerial;
	private String subscriptionPrice;
	private String channel;
	private boolean creditToCC;
	private String returnTotal;
	private String taxNote;
}
