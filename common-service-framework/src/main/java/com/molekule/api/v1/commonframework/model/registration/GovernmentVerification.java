package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

/**
 * The class GovernmentVerification is used to hold the government information.
 */
@Data
public class GovernmentVerification {

	private String governmentEmail;
	private String deptPhoneNumber;
	
}
