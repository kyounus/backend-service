package com.molekule.api.v1.commonframework.dto.registration;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
/**
 * CategoryDto class is used to hold the category details.
 *
 */
@Data
public class CategoryDto {

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("sales-rep-id")
	private String salesRepId;
}
