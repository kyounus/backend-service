package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

/**
 * 
 * 
 * This class used for return the shipping address when create a new shipping
 * address
 */

@Data
public class ShippingAddressResponse {

	private ShippingAddresses shippingAddresses;
	private AdditionalFreightInfo additionalFreightInfo;

}
