package com.molekule.api.v1.commonframework.service.order;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.ReadFlagRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.ReadFlagResponseBean;
import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean;
import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean.OrderSummary;
import com.molekule.api.v1.commonframework.model.order.SortActionEnum;
import com.molekule.api.v1.commonframework.model.order.SortOrderEnum;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.EncryptUtil;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
import com.molekule.api.v1.commonframework.util.StoreLocationType;


@Service("orderHelperService")
public class OrderHelperService {

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("dynamoDB")
	DynamoDB dynamoDB;
	
	@Value("${ct.aws.region}")
	private String awsRegion;
	
	@Value("${ct.awsSQSaccessKey}")
	private String awsSQSAccessKey;

	@Value("${ct.awsSQSSecretKey}")
	private String awsSQSSecretKey;

	private static final String ORDER_ID_ERROR_MESSAGE = "Getting Failed when getting order number";
	private static final String TRACKING_NUMBER_ERROR_MESSAGE = "Getting Failed when getting tracking number";

	private static final String COLON_START= ":start";
	
	Logger logger = LoggerFactory.getLogger(OrderHelperService.class);

	/**
	 * getAllOrders method is used to get all list of Order Data sorted by desc.
	 * @param customerId
	 * @param limit
	 * @param offset
	 * @param email 
	 * @param orderNumber 
	 * @param sortOrder 
	 * @param sortAction 
	 * 
	 * @return OrderSummaryResponseBean.
	 */

	public OrderSummaryResponseBean getAllOrders(String customerId,int limit ,int offset, String email, String orderNumber,Boolean isGift,String channel, SortActionEnum sortAction, SortOrderEnum sortOrder,String giftReceiptMail) {
		if(StringUtils.hasText(orderNumber)) {
			orderNumber = orderNumber.toUpperCase();
		}else if(StringUtils.hasText(email)) {
			email = email.toLowerCase();
		}
		limit = (limit == 0)?5:limit;
		//setting limit value to 5 if it is 0
		String orderResponse = getOrderByCustomerId(customerId,limit,offset, email, orderNumber, isGift, channel, sortAction, sortOrder,giftReceiptMail);
		JsonObject ordersResultObject = MolekuleUtility.parseJsonObject(orderResponse);
		int count = ordersResultObject.get("count").getAsInt();
		OrderSummaryResponseBean orderSummaryResponseBean = new OrderSummaryResponseBean();
		orderSummaryResponseBean.setLimit(ordersResultObject.get("limit").getAsInt());
		orderSummaryResponseBean.setOffset(ordersResultObject.get("offset").getAsInt());
		orderSummaryResponseBean.setCount(count);
		orderSummaryResponseBean.setTotal(ordersResultObject.get("total").getAsInt());

		List<OrderSummary> orderSummaryList =new ArrayList<>();
		if(ordersResultObject.get(RESULTS).getAsJsonArray().size() != 0) {
			processResultsValue(ordersResultObject, count, orderSummaryList);
		}

		orderSummaryResponseBean.setOrderSummaryList(orderSummaryList);
		return orderSummaryResponseBean;
	}

	public ResponseEntity<Object> getOrderByOrderId(String orderId) {
		//expend by payment query
		String ordersBaseUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/orders/").append(orderId).append("?expand=paymentInfo.payments[*].id&expand=state.id")
				.append("&expand=customerGroup.id")
				.append("&expand=lineItems[*].productType.typeId")
				.append("&expand=lineItems[*].state[*].state.typeId")
				.append("&expand=lineItems[*].variant.attributes[*].value.typeId").toString();
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		String orderResponse = netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
		if(orderResponse == null) {
			return new ResponseEntity<>(new ErrorResponse(400, "Resources does not have a valid Order Id"),HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderResponseWithCustomerName(orderResponse, token), HttpStatus.OK);
	}

	/**
	 * orderResponseWithCustomerName method is used to add customer's name to order Response.
	 * 
	 * @param orderResponse
	 * @return String
	 */
	public String orderResponseWithCustomerName(String orderResponse, String token) {
		JsonObject orderObject = MolekuleUtility.parseJsonObject(orderResponse);
		if(orderObject.get(STATUS_CODE) != null) {
			return orderResponse;
		}
		String orderShippingAddressId = EMPTY;
		if(orderObject.has(SHIPPING_ADDRESS) && orderObject.get(SHIPPING_ADDRESS).getAsJsonObject().has("id")) {
			orderShippingAddressId = orderObject.get(SHIPPING_ADDRESS).getAsJsonObject().get("id").getAsString();
		}
		JsonObject jsonCustomerObject = new JsonObject();
		if(orderObject.has(CUSTOMER_ID)) {

			String customerId = orderObject.get(CUSTOMER_ID).getAsString();
			String customerUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + SLASH_CUSTOMERS_SLASH
					+ customerId;
			String customerResponse = netConnectionHelper.sendGetWithoutBody(token ,customerUrl);
			jsonCustomerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(jsonCustomerObject.get(STATUS_CODE) != null) {
				return customerResponse;
			}
		}
		if(jsonCustomerObject.has(CUSTOM)
				&& jsonCustomerObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			manupulateOrderCustomField(orderObject, orderShippingAddressId, jsonCustomerObject, token);
		}
		if(jsonCustomerObject.has(FIRST_NAME)) {
			String firstName = jsonCustomerObject.get(FIRST_NAME).getAsString();
			orderObject.addProperty("customerFirstName", firstName);
		}
		if(jsonCustomerObject.has("lastName")) {
			String lastName = jsonCustomerObject.get("lastName").getAsString();
			orderObject.addProperty("customerLastName", lastName);
		}
		return orderObject.toString();
	}
	
	private void manupulateOrderCustomField(JsonObject orderObject, String orderShippingAddressId,
			JsonObject jsonCustomerObject, String token) {
		if(jsonCustomerObject.has(CUSTOM) && jsonCustomerObject.get(CUSTOM).getAsJsonObject().has(FIELDS) && jsonCustomerObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has("accountId")) {

			String accountId = jsonCustomerObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().get("accountId").getAsString();

			String accountBaseUrl = new StringBuilder().append(ctEnvProperties.getHost()).append("/").append(ctEnvProperties.getProjectKey())
					.append("/custom-objects?where=id=\"")
					.append(accountId).append("\"").toString();
			String accountResponse = netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
			JsonObject jsonAccountObject = MolekuleUtility.parseJsonObject(accountResponse);
			if(jsonAccountObject.has(RESULTS)) {
				JsonArray resultArray = jsonAccountObject.get(RESULTS).getAsJsonArray();
				for(int i=0; i<resultArray.size();i++) {
					JsonObject resultObject = resultArray.get(i).getAsJsonObject();
					JsonObject valueObject = new JsonObject();   
					if(resultObject.has(VALUE)) {
						valueObject = resultObject.get(VALUE).getAsJsonObject();
					}
					if(valueObject.has(ADDITIONAL_FREIGHT_INFOS)) {
						setOrderFreightDetails(orderObject, orderShippingAddressId, valueObject);
					}
				}
			}
		}
	}

	private void setOrderFreightDetails(JsonObject orderObject, String orderShippingAddressId, JsonObject valueObject) {
		JsonArray additionFreightInfosArray = valueObject.get(ADDITIONAL_FREIGHT_INFOS).getAsJsonArray();
		for(int j=0; j<additionFreightInfosArray.size();j++) {
			JsonObject additionFreightInfosObject = additionFreightInfosArray.get(j).getAsJsonObject();
			if(additionFreightInfosObject.has("shippingAddressId") && orderShippingAddressId.equals(additionFreightInfosObject.get("shippingAddressId").getAsString())) {
				orderObject.add(ADDITIONAL_FREIGHT_INFOS, additionFreightInfosObject);
			}
		}
	}
	private void processResultsValue(JsonObject ordersResultObject, int count, List<OrderSummary> orderSummaryList) {
		for(int i=0; i<count;i++) {
			OrderSummary orderSummary= new OrderSummary();
			JsonObject orderResultObject = ordersResultObject.get(RESULTS).getAsJsonArray().get(i).getAsJsonObject();
			if(orderResultObject.has("id")) {
				orderSummary.setOrderId(orderResultObject.get("id").getAsString());
			}
			if(orderResultObject.has("customerEmail")) {
				orderSummary.setEmail(orderResultObject.get("customerEmail").getAsString());
			}
			if(orderResultObject.has("lineItems")) {
				orderSummary.setLineOfItems(orderResultObject.get("lineItems").getAsJsonArray().size());
			}
			if(orderResultObject.has(PAYMENT_INFO)) {
				processPaymentInfo(orderSummary, orderResultObject);
			}
			if(orderResultObject.has(ORDER_NUMBER)) {
				orderSummary.setOrderNumber(orderResultObject.get(ORDER_NUMBER).getAsString());
			}
			orderSummary.setOrderDate(orderResultObject.get("createdAt").getAsString());
			if(orderResultObject.has("orderState")) {
				orderSummary.setOrderStatus(orderResultObject.get("orderState").getAsString());
			}
			if(orderResultObject.has(CUSTOM)) {
				processCustomOrderResults(orderSummary, orderResultObject);
			}
			setStoreValue(orderSummary, orderResultObject);
			populateChannelAndRecipientAddress(orderSummary, orderResultObject);
			processStateValue(orderSummary, orderResultObject);
			setTrackingValue(orderSummary, orderResultObject);
			orderSummaryList.add(orderSummary);
		}
	}

	private void setTrackingValue(OrderSummary orderSummary, JsonObject orderResultObject) {
		String trackingId = "";
		if(orderResultObject.has(SHIPPING_INFO)&&orderResultObject.get(SHIPPING_INFO).getAsJsonObject().has("deliveries")) {
			JsonArray deliveriesArray = orderResultObject.get(SHIPPING_INFO).getAsJsonObject().get("deliveries").getAsJsonArray();
			for(int i=0;i<deliveriesArray.size();i++) {
				if(deliveriesArray.get(i).getAsJsonObject().has("parcels")) {
					JsonArray parcelsArray = deliveriesArray.get(i).getAsJsonObject().get("parcels").getAsJsonArray();
					for(int j=0;j<parcelsArray.size();j++) {
						if(parcelsArray.get(j).getAsJsonObject().has(TRACKING_DATA)&& parcelsArray.get(j).getAsJsonObject().get(TRACKING_DATA).getAsJsonObject().has("trackingId")) {
							trackingId = parcelsArray.get(j).getAsJsonObject().get(TRACKING_DATA).getAsJsonObject().get("trackingId").getAsString();
						}
					}
				}
			}
		}
		orderSummary.setTrackingId(trackingId);
	}

	private void setStoreValue(OrderSummary orderSummary, JsonObject orderResultObject) {
		if(orderResultObject.has(STORE)) {
			orderSummary.setStore(orderResultObject.get(STORE).getAsJsonObject().has("key")?orderResultObject.get(STORE).getAsJsonObject().get("key").getAsString():null);
		}
	}

	private void populateChannelAndRecipientAddress(OrderSummary orderSummary, JsonObject orderResultObject) {
		if(orderResultObject.has(SHIPPING_ADDRESS)) {
			AddressRequestBean shippingAddress = new Gson().fromJson(orderResultObject.get(SHIPPING_ADDRESS).getAsJsonObject(), AddressRequestBean.class);
			orderSummary.setShippingAddress(shippingAddress);
		}
		JsonObject fieldsObject = new JsonObject();
		if(orderResultObject.has(CUSTOM) && orderResultObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			fieldsObject = orderResultObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}
		if(fieldsObject != null && fieldsObject.has(CHANNEL)) {
			orderSummary.setChannel(fieldsObject.get(CHANNEL).getAsString());
		}	
	}

	private void processPaymentInfo(OrderSummary orderSummary, JsonObject orderResultObject) {
		JsonObject paymentInfoObject = orderResultObject.get(PAYMENT_INFO).getAsJsonObject();
		JsonArray paymentsArray = new JsonArray();
		if(paymentInfoObject.has(PAYMENTS)) {
			paymentsArray = paymentInfoObject.get(PAYMENTS).getAsJsonArray();
		}
		for(int j=0; j< paymentsArray.size(); j++) {
			if(paymentsArray.get(j).getAsJsonObject().has("obj")) {
				JsonObject paymentObject = paymentsArray.get(j).getAsJsonObject().get("obj").getAsJsonObject();
				if(paymentObject.has(PAYMENT_METHOD_INFO) && paymentObject.get(PAYMENT_METHOD_INFO).getAsJsonObject().has("method")) {
					orderSummary.setPaymentMethod( paymentObject.get(PAYMENT_METHOD_INFO).getAsJsonObject().get("method").getAsString());
				}
				JsonObject transactionsObject = new JsonObject();
				if(paymentObject.get("transactions").getAsJsonArray().size()>0) {
					transactionsObject = paymentObject.get("transactions").getAsJsonArray().get(0).getAsJsonObject();
				}
				if(transactionsObject.has("state")) {
					orderSummary.setPaymentStatus(transactionsObject.get("state").getAsString());
				}
			}
		}
	}

	private void processStateValue(OrderSummary orderSummary, JsonObject orderResultObject) {
		if(orderResultObject.has(STATE) && orderResultObject.get(STATE).getAsJsonObject().has("obj")) {
			JsonObject stateObject = orderResultObject.get(STATE).getAsJsonObject().get("obj").getAsJsonObject();
			String state = EMPTY;
			if(stateObject.has("name")) {

				state = stateObject.get("name").getAsJsonObject().get("en").getAsString();
				orderSummary.setOrderState(state);
			}
			if(stateObject.has("key") && stateObject.get("key").getAsString().equals("order-shipped")) {
				orderSummary.setShipmentStatus(state);
			}
		}
	}

	private void processCustomOrderResults(OrderSummary orderSummary, JsonObject orderResultObject) {
		JsonObject fieldsObject = orderResultObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		if(fieldsObject != null) {
			if(fieldsObject.has(ORDER_TOTAL)) {
				orderSummary.setOrderTotal(fieldsObject.get(ORDER_TOTAL).getAsString());
			}
			if(fieldsObject.has(READ_FLAG)) {
				orderSummary.setReadFlag(fieldsObject.get(READ_FLAG).getAsBoolean());
			} else {
				orderSummary.setReadFlag(false);
			}
			if(fieldsObject.has(HOLDS)) {
				JsonArray holdsArray = fieldsObject.get(HOLDS).getAsJsonArray();
				List<String> holdsList = new ArrayList<>();
				for(int j=0;j<holdsArray.size();j++) {
					holdsList.add(holdsArray.get(j).getAsString());
				}
				orderSummary.setHolds(holdsList);
			}
			if(fieldsObject.has("gift")) {
				orderSummary.setIsGiftOrder(fieldsObject.get("gift").getAsString());
			}
			if(fieldsObject.has("recipientsEmail")) {
				orderSummary.setRecipientsEmail(fieldsObject.get("recipientsEmail").getAsString());
			}
		}
	}

	/**
	 * getOrderByCustomerId method is used to get sorted list of Order Data sorted by desc.
	 * @param customerId
	 * @param limit
	 * @param offset
	 * @param email 
	 * @param orderNumber 
	 * @param sortOrder 
	 * @param sortAction 
	 * 
	 * @return String- it contain all Order related values.
	 */
	public String getOrderByCustomerId(String customerId, int limit , int offset, String email, String orderNumber,Boolean isGift, String channel, SortActionEnum sortAction, SortOrderEnum sortOrder, String giftReceiptMail) {
		String token = ctServerHelperService.getStringAccessToken();
		String response = null;
		String ordersBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders"
				+ LIMIT + limit + OFF_SET + offset + "&expand=paymentInfo.payments[*].typeId&expand=state.id";

		ordersBaseUrl = sortOrderUrl(sortAction, sortOrder, ordersBaseUrl);
		if(StringUtils.hasText(customerId)) {
			ordersBaseUrl = ordersBaseUrl + "&sort=createdAt desc&where=customerId=\"" + customerId + "\"";
			response = netConnectionHelper.sendGetWithLimitSize(token, ordersBaseUrl);
		}else if(StringUtils.hasText(channel)) {
			ordersBaseUrl = ordersBaseUrl + "&sort=createdAt desc&where=custom(fields(channel=\"" + channel + "\"))";
			response = netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
		}else if(isGift != null) {
			ordersBaseUrl = ordersBaseUrl + "&sort=createdAt desc&where=custom(fields(gift=" + isGift + "))";
			response = netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
		}else if(StringUtils.hasText(orderNumber) || StringUtils.hasText(email) || StringUtils.hasText(giftReceiptMail)){
			response = filterEmail(email, orderNumber, token, response, ordersBaseUrl, giftReceiptMail);
		}else {
			response = netConnectionHelper.sendGetWithLimitSize(token, ordersBaseUrl);
		}
		return response;
	}

	private String filterEmail(String email, String orderNumber, String token, String response, String ordersBaseUrl, String giftReceiptMail) {
		String sortString = "sort=\"createdAt desc\"";
		ordersBaseUrl = ordersBaseUrl +"&"+ MolekuleUtility.convertToEncodeUri(sortString);
		if (StringUtils.hasText(orderNumber)) {
			String companyNameStr = "orderNumber=\""+ orderNumber + "\"";
			ordersBaseUrl = ordersBaseUrl + "&where=" + MolekuleUtility.convertToEncodeUri(companyNameStr);
		} 
		if (StringUtils.hasText(email)) {
			String emailStr = "customerEmail=\""+ email + "\"";
			ordersBaseUrl = ordersBaseUrl + "&where=" + MolekuleUtility.convertToEncodeUri(emailStr);
		}
		if (StringUtils.hasText(giftReceiptMail)) {
			String giftEmailStr = "custom(fields(recipientsEmail=\""+ giftReceiptMail + "\"))";
			ordersBaseUrl = ordersBaseUrl + "&where=" + MolekuleUtility.convertToEncodeUri(giftEmailStr);
		}
		
		try {
			response = netConnectionHelper.httpConnectionHelper(ordersBaseUrl,token);
		} catch (IOException e) {
			logger.error("IOException Occured", e);
		}
		return response;
	}

	private String sortOrderUrl(SortActionEnum sortAction, SortOrderEnum sortOrder, String ordersBaseUrl) {
		if(sortAction!= null && sortOrder != null) {
			if(sortAction.name().equals(SortActionEnum.DATE_CREATED.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=createdAt asc";
			}else if(sortAction.name().equals(SortActionEnum.DATE_CREATED.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=createdAt desc";
			}else if(sortAction.name().equals(SortActionEnum.DATE_MODIFIED.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=lastModifiedAt asc";
			}else if(sortAction.name().equals(SortActionEnum.DATE_MODIFIED.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=lastModifiedAt desc";
			}else if(sortAction.name().equals(SortActionEnum.ORDER_STATUS.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=orderState asc";
			}else if(sortAction.name().equals(SortActionEnum.ORDER_STATUS.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=orderState desc";
			}else if(sortAction.name().equals(SortActionEnum.ORDER_NUMBER.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=orderNumber asc";
			}else if(sortAction.name().equals(SortActionEnum.ORDER_NUMBER.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=orderNumber desc";
			}else if(sortAction.name().equals(SortActionEnum.EMAIL.name()) && sortOrder.name().equals(SortOrderEnum.ASC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=customerEmail asc";
			}else if(sortAction.name().equals(SortActionEnum.EMAIL.name()) && sortOrder.name().equals(SortOrderEnum.DESC.name())) {
				ordersBaseUrl = ordersBaseUrl + "&sort=customerEmail desc";
			}else {
				ordersBaseUrl = ordersBaseUrl + "&sort=createdAt desc";
			}
			
		}
		return ordersBaseUrl;
	}

	public String getOrderNumber(String locationType) {
		if (StoreLocationType.MLK_US.name().equals(locationType)) {
			return getUSOrderNumber();
		} else if (StoreLocationType.MLK_CA.name().equals(locationType)) {
			return getCAOrderNumber();
		} 
		return new ErrorResponse(400, "Invalid Store Location Type").toString();
	}

	private synchronized String getUSOrderNumber() {
		try {
			Table table = dynamoDB.getTable(ctEnvProperties.getUSOrdersTableName());

			UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", 1)
					.withUpdateExpression("set order_id = if_not_exists(order_id, :start) + :inc")
					.withValueMap(new ValueMap().withNumber(COLON_START, ctEnvProperties.getUSOrdersStartingIndex()).withNumber(":inc", 1))
					.withReturnValues(ReturnValue.UPDATED_NEW);

			UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
			if (outcome != null && outcome.getItem() != null) {
				return "{\"orderId\": \"" + outcome.getItem().get("order_id") +"\"}";
			}

			return new ErrorResponse(400, ORDER_ID_ERROR_MESSAGE).toString();

		} catch (Exception exception) {
			logger.error("exception occured when getUSOrderNumber", exception);
			return new ErrorResponse(400, ORDER_ID_ERROR_MESSAGE).toString();
		}

	}


	private synchronized String getCAOrderNumber() {
		try {
			Table table = dynamoDB.getTable(ctEnvProperties.getCAOrdersTableName());

			UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", 1)
					.withUpdateExpression("set order_id = if_not_exists(order_id, :start) + :inc")
					.withValueMap(new ValueMap().withNumber(COLON_START, ctEnvProperties.getCAOrdersStartingIndex()).withNumber(":inc", 1))
					.withReturnValues(ReturnValue.UPDATED_NEW);


			UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
			if (outcome != null && outcome.getItem() != null) {
				return "{\"orderId\": \"" + outcome.getItem().get("order_id") +"\"}";
			}

			return new ErrorResponse(400, ORDER_ID_ERROR_MESSAGE).toString();

		} catch (Exception exception) {
			logger.error("exception occured when getCAOrderNumber", exception);
			return new ErrorResponse(400, ORDER_ID_ERROR_MESSAGE).toString();

		}

	}	
	public synchronized String getTrackingNumber() {
		try {
			Table table = dynamoDB.getTable(ctEnvProperties.getTrackingNumberTableName());

			UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", 1)
					.withUpdateExpression("set tracking_number = if_not_exists(tracking_number, :start) + :inc")
					.withValueMap(new ValueMap().withNumber(COLON_START, Long.parseLong(ctEnvProperties.getTrackingNumberStartingIndex())).withNumber(":inc", 1))
					.withReturnValues(ReturnValue.UPDATED_NEW);

			UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
			if (outcome != null && outcome.getItem() != null) {
				BigDecimal trackingNumber = new BigDecimal(outcome.getItem().get("tracking_number").toString());
				return String.format("%09d", trackingNumber.intValue());
			}

			return new ErrorResponse(400, TRACKING_NUMBER_ERROR_MESSAGE).toString();

		} catch (Exception exception) {
			logger.error("exception occured when getTrackingNumber", exception);
			return new ErrorResponse(400, TRACKING_NUMBER_ERROR_MESSAGE).toString();
		}

	}
	public String updateReadFlag(String orderId, boolean readFlagValue, String token) {
		ReadFlagRequestBean readFlagRequestBean = new ReadFlagRequestBean();
		ReadFlagRequestBean.Action customAction = new ReadFlagRequestBean.Action();
		List<ReadFlagRequestBean.Action> readFlagRequestActions = new ArrayList<>();
		customAction.setActionType(SET_CUSTOM_FIELD);
		customAction.setName(READ_FLAG);
		customAction.setValue(readFlagValue);
		readFlagRequestActions.add(customAction);
		readFlagRequestBean.setVersion(getCurrentVersionByOrderId(orderId, token));
		readFlagRequestBean.setActions(readFlagRequestActions);
		String updateURL = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + ORDERS + orderId;
		return (String) netConnectionHelper.sendPostRequest(updateURL, token, readFlagRequestBean);
	}

	public Long getCurrentVersionByOrderId(String orderId, String token) {
		String order = getOrderObj(orderId, token);
		JsonObject jsonAccountObj = MolekuleUtility.parseJsonObject(order);
		if (jsonAccountObj.get(STATUS_CODE) != null) {
			return 0l;
		}
		return jsonAccountObj.get(VERSION).getAsLong();
	}

	public String getOrderByNumber(String orderNumber,String token) {
		String getOrderByNumberUrl = ctEnvProperties.getHost()+"/"+ctEnvProperties.getProjectKey()+"/orders/order-number="+orderNumber;
		return netConnectionHelper.sendGetWithoutBody(token, getOrderByNumberUrl);
	}

	public String getOrderObj(String orderId, String token) {
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append(ORDERS).append(orderId).toString();
		return netConnectionHelper.sendGetWithoutBody(token, url);
	}

	public String getReadFlagResponseBean(String response) {
		if (response == null) {
			return response;
		}
		Boolean readStatus = false;
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		if (jsonObject.has(CUSTOM) && jsonObject.get(CUSTOM) != null) {
			JsonObject customObject = (JsonObject) jsonObject.get(CUSTOM);
			if (customObject.get(FIELDS) != null) {
				JsonObject fieldsObject = (JsonObject) customObject.get(FIELDS);
				if (fieldsObject.get(READ_FLAG) != null) {
					readStatus = Boolean.valueOf(fieldsObject.get(READ_FLAG).getAsString());
				}
			}

		}
		return new ReadFlagResponseBean(jsonObject.get("id").getAsString(), readStatus).toString();

	}

	public void sendAmazonSqsToNS(Map<String, Object> refundNetsuiteMap) {
		AmazonSQS sqs = new AmazonSQSClient(new BasicAWSCredentials(EncryptUtil.decrypt(this.awsSQSAccessKey), EncryptUtil.decrypt(this.awsSQSSecretKey)));
		sqs.setRegion(Region.getRegion(Regions.fromName(awsRegion)));
		String queueName = "NETSUITE_ORDER_"+ctEnvProperties.getEnvironment();
		String queueUrl = ctEnvProperties.getAwsSQSLambdaUrl()+queueName;
		logger.info("queueName : {}" , queueName);
		logger.info("SQS Message : {}" , MolekuleUtility.parseJsonObject(refundNetsuiteMap.toString()).toString());
		sqs.sendMessage(queueUrl, MolekuleUtility.parseJsonObject(refundNetsuiteMap.toString()).toString());
		logger.info("Successfully sent");
	}

	public List<String> getProductImage(String productId, String country, String channel) {
		String sku = null;
		List<String> productDetail = new ArrayList<String>();
		String prismicImageResponse = null;
		String prismicImageUrl = null;
		try {
			String token = ctServerHelperService.getStringAccessToken();
			String url = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/products/" + productId+"?country="+country;
			ProductBean productBeanResponseData = netConnectionHelper.sendGetProductDataWithoutBody(token, url);
			ProductData current = productBeanResponseData.getMasterData().getCurrent();
			/**slug = current.getSlug().get(EN_US);*/
			sku = current.getMasterVariant().getSku();
			if(country.equals("GB")) {
				prismicImageUrl = ctEnvProperties.getPrismicApiUrl()+sku+"?country="+country;
			}else if(country.equals("US")) {
				if(channel.equals("D2C")) {
					prismicImageUrl = ctEnvProperties.getPrismicApiD2CUrl()+sku+"?country="+country;
				}else if(channel.equals("B2B")) {
					prismicImageUrl = ctEnvProperties.getPrismicApiB2BUrl()+sku+"?country="+country;
			}
						}
			prismicImageResponse = netConnectionHelper.sendGetWithoutBody(null, prismicImageUrl);
			prismicImageResponse = prismicImageResponse.replace("prismic-images", "images.prismic.io");
			prismicImageResponse = "https:/"+prismicImageResponse;
			productDetail.add(0, prismicImageResponse);
						if(country.equals("US")) {
							productDetail.add(1,current.getDescription().get("en-US"));
						}else if(country.equals("GB")) {
							productDetail.add(1,current.getDescription().get("en-GB"));
						}
		}catch (Exception e) {
			logger.error("Error occured in getProductImage - ", e);
		}
		return productDetail;
	}

}
