package com.molekule.api.v1.commonframework.service.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRICE;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
/**
 * ParcelHandlingCostCalculationServieImpl is a class used to handle the parcel related handling charge.
 * 
 *
 */
@Component
@Primary
public class ParcelHandlingCostCalculationServieImpl implements HandlingCostCalculationService{

	@Override
	public String calculateHandlingCharge(JsonObject shippingTableJsonObject, int deviceCount, int filterCount) {
		int totalCount =deviceCount + filterCount;
		JsonObject parcelChargeJsonObject = shippingTableJsonObject.get(VALUE).getAsJsonObject().get("handlingCosts").getAsJsonObject()
				.get("parcel").getAsJsonObject();
		// pick  charge
		BigDecimal pickCharge = parcelChargeJsonObject.get("pick").getAsJsonObject().get(PRICE).getAsJsonObject()
				.get(AMOUNT).getAsBigDecimal();
		// pack charge
		BigDecimal packCharge = parcelChargeJsonObject.get("pack").getAsJsonObject().get(PRICE).getAsJsonObject()
				.get(AMOUNT).getAsBigDecimal();
		// ship charge
		BigDecimal shipCharge = parcelChargeJsonObject.get("ship").getAsJsonObject().get(PRICE).getAsJsonObject()
				.get(AMOUNT).getAsBigDecimal();
		// adding pick + pack + ship charge 
		BigDecimal pickPackShipCharge = pickCharge.add(packCharge).add(shipCharge);
		BigDecimal totalPickPackShipCharge = pickPackShipCharge.multiply(new BigDecimal(totalCount));
		// order charge
		BigDecimal orderCharge = parcelChargeJsonObject.get("orderCharge").getAsJsonObject().get(PRICE).getAsJsonObject().get(AMOUNT).getAsBigDecimal();
		// Scan charge
		BigDecimal serialNumberScanCharge = parcelChargeJsonObject.get("snScan").getAsJsonObject().get(PRICE).getAsJsonObject().get(AMOUNT).getAsBigDecimal();
		BigDecimal totalScanCharge = serialNumberScanCharge.multiply(new BigDecimal(totalCount));
		// markup percentage 
		BigDecimal markupPercentage;
		String markupPercentageString = shippingTableJsonObject.get(VALUE).getAsJsonObject().get("markUpPercentage").getAsString();
		if(markupPercentageString == null || markupPercentageString.equals("")) {
			markupPercentage = new BigDecimal(0);
		} else {
			markupPercentage = new BigDecimal(markupPercentageString);
		}
		BigDecimal totalHandlingChargeForParcel = totalPickPackShipCharge.add(orderCharge).add(totalScanCharge);
		BigDecimal markupPercentageValue = totalHandlingChargeForParcel.multiply(markupPercentage.divide(BigDecimal.valueOf(100)));
		// final total handling cost
		BigDecimal finaltotalHandlingCostForParcel = totalHandlingChargeForParcel.add(markupPercentageValue);
		// round of to nearest value
		return String.valueOf(finaltotalHandlingCostForParcel.setScale(0, RoundingMode.HALF_EVEN));
	}

}
