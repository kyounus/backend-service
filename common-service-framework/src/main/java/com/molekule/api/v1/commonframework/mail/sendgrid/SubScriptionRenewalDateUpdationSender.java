package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("subScriptionRenewalDateUpdationSender")
public class SubScriptionRenewalDateUpdationSender extends TemplateMailRequestHelper{

	@Value("${subscriptionRenewalDateChangeTemplateId}")
	private String templateId;

	@Value("${refillFilterb2bAccountDashBoardURL}")
	private String b2bAccountDashboardURL;

	@Value("${refillFilterd2cAccountDashBoardURL}")
	private String d2cAccountDashboardURL;

	@Override
	public String templateId() {
		return templateId ;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		if(sendGridModel.getChannel().equals("B2B"))
			dynamicTemplateData.setAccountDashboardUrl(b2bAccountDashboardURL);
		else
			dynamicTemplateData.setAccountDashboardUrl(d2cAccountDashboardURL);
		return dynamicTemplateData;
	}

}
