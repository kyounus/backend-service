package com.molekule.api.v1.commonframework.model.mobileapp;

import java.util.List;

import lombok.Data;
@Data
public class CustomerResponse {

	private String id;
	private String group_id;
	private String default_billing;
	private String default_shipping;
	private String created_at;
	private String updated_at;
	private String created_in;
	private String email;
	private String firstname;
	private String lastname;
	private String store_id;
	private String website_id;
	private List<Address> addresses;
	private int disable_auto_group_change;
	private ExtensionAttributes extension_attributes;
	@Data
	public static class ExtensionAttributes{
		private boolean is_subscribed;
	}
	private List<CustomAttribute> custom_attributes;
	@Data
	public static class CustomAttribute{
		private String attribute_code;
		private String value;
	}
}
