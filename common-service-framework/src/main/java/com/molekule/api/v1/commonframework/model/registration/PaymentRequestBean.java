package com.molekule.api.v1.commonframework.model.registration;

import java.util.List;

import com.molekule.api.v1.commonframework.dto.registration.NonProfitDocumentProofDto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PaymentRequestBean {

	public enum Payment{
		CREDITCARD,ACH,PAYMENTTERMS,AFFIRM
	}
	public enum BusinessType{
		PUBLIC,PRIVATE,GOVERNMENT,NONPROFIT
	}
	@ApiModelProperty(example = "CREDITCARD or ACH or PAYMENTTERMS")
	private String paymentPreferences;
	
	@ApiModelProperty(required = false)
	private String paymentToken;
	
	@ApiModelProperty(required = false)
	private boolean isShippingAsBillingAddress;
	
	@ApiModelProperty(required = false)
	private BillingAddresses billingAddress;
	
	@ApiModelProperty(required = false, example = "PUBLIC or PRIVATE or GOVERNMENT or NONPROFIT")
	private String businessType;
	
	@ApiModelProperty(required = false)
	private String stockTickerName;
	
	@ApiModelProperty(required = false)
	private String stockExchange;
	
	@ApiModelProperty(required = false)
	private BusinessReferences businessReferences1;
	
	@ApiModelProperty(required = false)
	private BusinessReferences businessReferences2;
	
	@ApiModelProperty(required = false)
	private BankInformation bankInformation;
	
	@ApiModelProperty(required = false)
	private GovernmentVerification govtVerification; 
	
	@ApiModelProperty(required = false)
	private List<NonProfitDocumentProofDto> documentProof;
	
	@ApiModelProperty(required = false)
	private boolean isDefaultPayment;
	
}
