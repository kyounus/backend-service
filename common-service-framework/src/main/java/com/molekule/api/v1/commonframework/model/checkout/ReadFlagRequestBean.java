package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ReadFlagRequestBean {

	private long version;
	private List<Action> actions;

	@Data
	public static class Action {
		
		@JsonProperty(ACTION)
		private String actionType;
		
		private String name;
		
		private Boolean value;
	}

}