package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;

import lombok.Data;

@Data
public class AvalaraRequestBean {

	private List<Line> lines;
	private String type;
	private String companyCode;
	private String customerCode;
	private String purchaseOrderNo;
	private boolean commit;
	private String currencyCode;
	private String date;
	private String description;
	private Address addresses;

	@Data
	public static class Line {

		private String number;
		private int quantity;
		private double amount;
		private String taxCode;
		private String itemCode;
		private String description;

	}

    @Data
	public static class Address {
		private SingleLocation singleLocation;
        
		@Data
		public static class SingleLocation {
			private String line1;
			private String city;
			private String region;
			private String country;
			private String postalCode;
		}
	}
}
