package com.molekule.api.v1.commonframework.model.mobileapp;

import java.util.List;

import lombok.Data;
@Data
public class SubscriptionCustomerEstimateQuery {
	private String subscription_id;
	private String shipping_address_id;
	private List<String> coupon_codes;

}
