package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
public class DiscountedLineItemPrice {

	@JsonProperty(VALUE)
	private TypedMoney value;

	@JsonProperty("includedDiscounts")
	private List<DiscountedLineItemPortion> includedDiscounts;

	public TypedMoney getValue() {
		return value;
	}

	public void setValue(TypedMoney value) {
		this.value = value;
	}

	public List<DiscountedLineItemPortion> getIncludedDiscounts() {
		return includedDiscounts;
	}

	public void setIncludedDiscounts(List<DiscountedLineItemPortion> includedDiscounts) {
		this.includedDiscounts = includedDiscounts;
	}

}
