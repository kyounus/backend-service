package com.molekule.api.v1.commonframework.dto.order;

import java.util.List;

import lombok.Data;

@Data
public class OrderRefundRequestBean {
	private String invoiceNumber;
	private List<LineItems> lineItemsList;
	private String refundShipping;
	private String adjustmentRefund;
	private String adjustmentFee;
	private String comments;
	
}
