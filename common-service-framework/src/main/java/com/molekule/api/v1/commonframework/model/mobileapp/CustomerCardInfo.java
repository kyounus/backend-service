package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class CustomerCardInfo {

	private String id;
	private int exp_month;
	private int exp_year;
	private String last4;
	private String brand;
	
}
