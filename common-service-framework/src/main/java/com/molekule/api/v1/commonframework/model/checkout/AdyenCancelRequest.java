package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class AdyenCancelRequest {

	private String reference;
	private String merchantAccount;
	
}
