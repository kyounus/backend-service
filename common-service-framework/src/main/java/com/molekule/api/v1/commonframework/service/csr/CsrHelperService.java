package com.molekule.api.v1.commonframework.service.csr;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;
import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean.OrderSummary;
import com.molekule.api.v1.commonframework.model.products.CustomerGroup;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.CustomRunTimeException;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("csrHelperService")
public class CsrHelperService {
	Logger logger = LoggerFactory.getLogger(CsrHelperService.class);

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	/**
	 * getCustomerData method is used to display customer details.
	 * 
	 * @param customerId
	 * @param email
	 * @param limit
	 * @return CustomerDetailBean
	 * @throws IOException 
	 */
	public CustomerDetailBean getCustomerData(String customerId, String email, int limit, String customerSource) {

		customerDataRequestValidation(customerId, email, customerSource);
		if(StringUtils.hasText(email)) {
			email = email.toLowerCase();
		}
		String customerBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/customers?expand=customerGroup.typeId&";
		customerBaseUrl = getCustomerBaseURL(customerId, email, customerSource, customerBaseUrl);
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();

		String customerResponseData = null;
		try {
			customerResponseData = netConnectionHelper.httpConnectionHelper(customerBaseUrl, token);
		} catch (IOException e1) {
			logger.error("Exception occured when getting customer Data", e1);
		}
		JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponseData);
		if (0 == customerJsonObject.get(TOTAL).getAsInt()) {
			throw new CustomRunTimeException("User Not Found");
		}
		JsonObject resultObject = customerJsonObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
		JsonObject fieldsCustomObject = new JsonObject();
		if(resultObject.has(CUSTOM) && resultObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			fieldsCustomObject = resultObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}
		if(fieldsCustomObject.has(CHANNEL) && B2B.equals(fieldsCustomObject.get(CHANNEL).getAsString())) {
			return b2bCustomer(customerJsonObject, customerId, token, limit);
		} else {
			return d2cCustomer(customerJsonObject, customerId, token, limit);
		}
	}

	private CustomerDetailBean d2cCustomer(JsonObject customerJsonObject, String customerId, String token, int limit) {
		CustomerDetailBean customerDetails = new CustomerDetailBean();
		JsonObject resultObject = customerJsonObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
		customerId = (customerId == null) ? resultObject.get("id").getAsString() : customerId;
		if (resultObject.has(CUSTOM)) {
			JsonObject fieldsObject = resultObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			setCustomfields(customerDetails, fieldsObject);
		}
		customerDetails.setCustomerId(customerId);
		customerDetails.setFirstName(resultObject.get(FIRST_NAME).getAsString());
		customerDetails.setLastName(resultObject.get("lastName").getAsString());
		customerDetails.setEmail(resultObject.get(EMAIL).getAsString());
		customerDetails
				.setDateOfBirth(resultObject.has(DATE_OF_BIRTH) ? resultObject.get(DATE_OF_BIRTH).getAsString() : null);
		customerDetails.setCustomerSince(resultObject.get("createdAt").getAsString());
		if (resultObject.has(CUSTOMER_GROUP)) {
			processCustomerGroup(customerDetails, resultObject);
		}
		setAddressList(customerDetails, resultObject);
		List<AddressRequestBean> addressesList = customerDetails.getAddressList();
		customerDetails.setTotalTickets(EMPTY);
		JsonArray billingAddressIdsArray = resultObject.get("billingAddressIds").getAsJsonArray();
		JsonArray shippingAddressIdsArray = resultObject.get("shippingAddressIds").getAsJsonArray();
		List<String> billingAddressIds = new ArrayList<>();
		for(int i=0;i<billingAddressIdsArray.size();i++) {
			billingAddressIds.add(billingAddressIdsArray.get(i).getAsString());
		}
		List<String> shippingAddressIds = new ArrayList<>();
		for(int i=0;i<shippingAddressIdsArray.size();i++) {
			shippingAddressIds.add(shippingAddressIdsArray.get(i).getAsString());
		}
		if(!billingAddressIds.isEmpty()) {
			List<AddressRequestBean> billingAddressList = setShippingOrBillingAddress(billingAddressIds, addressesList);
			customerDetails.setBillingAddressList(billingAddressList);
		}
		if(!shippingAddressIds.isEmpty()) {
			List<AddressRequestBean> shippingAddressList = setShippingOrBillingAddress(shippingAddressIds, addressesList);
			customerDetails.setShippingAddressList(shippingAddressList);
		}
		customerDetails.setBillingAddressIds(billingAddressIds);
		customerDetails.setShippingAddressIds(shippingAddressIds);
		if(resultObject.has("defaultBillingAddressId")) {
			customerDetails.setDefaultBillingAddressId(resultObject.get("defaultBillingAddressId").getAsString());			
		}
		if(resultObject.has("defaultShippingAddressId")) {
			customerDetails.setDefaultShippingAddressId(resultObject.get("defaultShippingAddressId").getAsString());			
		}
		if (resultObject.has(COMPANY_NAME)) {
			customerDetails.setCompanyName(resultObject.get(COMPANY_NAME).getAsString());			
		}
		customerDetails.setCompanyPhoneNumber(EMPTY);
		customerDetails.setCompanyAddress(EMPTY);
		
		// Adding payment in response
		String customPaymentBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects" + D2C_PAYMENT
				+ customerId;
		PaymentCustomObject customPaymentResponse = netConnectionHelper.getPaymentCustomObject(token, customPaymentBaseUrl);
		if(null != customPaymentResponse) {
			com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject.Value value = customPaymentResponse.getValue();
			if(null != value) {
				customerDetails.setPayments(value.getPayments());
				customerDetails.setPreferredPaymentId(value.getPreferredPaymentId());
				customerDetails.setRecentlyUsedPayment(value.getRecentlyUsedPayment());
			}
		}

		// Adding order summary in response
		String orderResponse = getSortedOrderByCustomerId(customerId, limit);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		int count = orderJsonObject.get("count").getAsInt();
		List<OrderSummary> orderSummaryList = new ArrayList<>();
		if (orderJsonObject.get(RESULTS).getAsJsonArray().size() != 0) {
			processOrderSummary(count, orderJsonObject, orderSummaryList);
		}
		//		String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + "customer-id="
		//		+ customerId;
		String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
				+ "?where=customerId=\"" + customerId + "\""
				+ "&where=custom(fields(parentOrderReferenceId is not defined))"
				+ "&where=custom(fields(Quote is not defined))" + "&where=cartState=\"Active\"";
		JsonObject cartObject = MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(token, cartUrl));
		//if (cartObject.has("id")) {
		//	customerDetails.setCartId(cartObject.get("id").getAsString());
		//}
		if(cartObject.get("results").getAsJsonArray().size() > 0) {
			JsonObject cartResultObject = cartObject.get("results").getAsJsonArray().get(0).getAsJsonObject();
			if(resultObject.has("id")) {
				customerDetails.setCartId(cartResultObject.get("id").getAsString());
			}
		}
		customerDetails.setOrderSummaryList(orderSummaryList);
		return customerDetails;
	}

	private List<AddressRequestBean> setShippingOrBillingAddress(List<String> shippingAddressIds, List<AddressRequestBean> addressesList) {
		List<AddressRequestBean> shippingOrBillingAddress = new ArrayList<>();
		addressesList.stream().forEach(address -> {
			shippingAddressIds.stream().forEach(shippingAddressId -> {
				if(address.getId().equals(shippingAddressId)) {
					shippingOrBillingAddress.add(address);
				}
			});
		});
		return shippingOrBillingAddress;
	}

	private void setAddressList(CustomerDetailBean customerDetails, JsonObject resultObject) {
		List<AddressRequestBean> addressList = new ArrayList<>();
		JsonArray addressJsonArray = resultObject.get("addresses").getAsJsonArray();
		for(int i=0;i<addressJsonArray.size();i++) {
			JsonObject addressJsonObject = addressJsonArray.get(i).getAsJsonObject();
			AddressRequestBean address = new AddressRequestBean();
			address.setCity(addressJsonObject.get("city").getAsString());
			if(addressJsonObject.has(COMPANY)) {
				address.setCompany(addressJsonObject.get(COMPANY).getAsString());				
			}
			address.setCountry(addressJsonObject.get(COUNTRY).getAsString());
			address.setFirstName(addressJsonObject.get(FIRST_NAME).getAsString());
			address.setId(addressJsonObject.get(ID).getAsString());
			if(addressJsonObject.has(CUSTOM)) {
				JsonObject fieldsJsonObject = addressJsonObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
				if(fieldsJsonObject.has(IS_BUSINESS)) {
					address.setIsBusiness(fieldsJsonObject.get(IS_BUSINESS).getAsBoolean());
				}
			}
			
			address.setState(getAddressFields(addressJsonObject, STATE));
			address.setLastName(getAddressFields(addressJsonObject, LAST_NAME));
			address.setPostalCode(getAddressFields(addressJsonObject, POSTAL_CODE));
			address.setPhone(getAddressFields(addressJsonObject, PHONE));
			address.setRegion(getAddressFields(addressJsonObject, "region"));
			address.setStreetName(getAddressFields(addressJsonObject, "streetName"));
			address.setStreetNumber(getAddressFields(addressJsonObject, "streetNumber"));
			addressList.add(address);
		}
		customerDetails.setAddressList(addressList);
	}
	
	private String getAddressFields(JsonObject addressJsonObject, String attribute) {
		if(addressJsonObject.has(attribute)) {
			return addressJsonObject.get(attribute).getAsString();				
		}
		return null;
	}

	private CustomerDetailBean b2bCustomer(JsonObject customerJsonObject, String customerId, 
			String token, int limit) {
		CustomerDetailBean customerDetails = new CustomerDetailBean();
		ShippingAddresses defaultShippingAddresses = new ShippingAddresses();
		BillingAddresses defaultBillingAddresses = new BillingAddresses();
		List<BillingAddresses> billingAddressesList = new ArrayList<>();
		JsonObject resultObject = customerJsonObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
		customerId = (customerId == null) ? resultObject.get("id").getAsString() : customerId;
		if (resultObject.has(CUSTOM)) {

			JsonObject fieldsObject = resultObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
			String accountId = fieldsObject.get("accountId").getAsString();

			setCustomfields(customerDetails, fieldsObject);

			String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
					+ "/custom-objects?" + "where=id=\"" + accountId + "\"";
			String accountResponseData = netConnectionHelper.sendGetWithoutBody(token, accountBaseUrl);
			JsonObject accountJsonObject = MolekuleUtility.parseJsonObject(accountResponseData);
			JsonObject accountResultObject = accountJsonObject.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
			JsonObject valueObject = accountResultObject.get(VALUE).getAsJsonObject();
			Value value = new Value();
			ObjectMapper mapper = new ObjectMapper();
			try {
				value = mapper.readValue(valueObject.toString(), Value.class);
			} catch (JsonProcessingException e) {
				logger.error("JsonProcessingException occured in getCustomerData ", e);
			}
			defaultShippingAddresses = processDefaultShippingAddressId(defaultShippingAddresses, value);
			defaultBillingAddresses = processDefaultBillingAddress(defaultBillingAddresses, value);
			setTermRelatedDetails(customerId, customerDetails, billingAddressesList, accountResultObject, value);
		}

		customerDetails.setCustomerId(resultObject.get("id").getAsString());
		customerDetails.setFirstName(resultObject.get(FIRST_NAME).getAsString());
		customerDetails.setLastName(resultObject.get("lastName").getAsString());
		customerDetails.setEmail(resultObject.get(EMAIL).getAsString());
		customerDetails
				.setDateOfBirth(resultObject.has(DATE_OF_BIRTH) ? resultObject.get(DATE_OF_BIRTH).getAsString() : null);
		customerDetails.setCustomerSince(resultObject.get("createdAt").getAsString());
		if (resultObject.has(CUSTOMER_GROUP)) {
			processCustomerGroup(customerDetails, resultObject);
		}
		customerDetails.setTotalTickets(EMPTY);
		customerDetails.setDefaultBillingAddress(defaultBillingAddresses);
		customerDetails.setDefaultShippingAddress(defaultShippingAddresses);
		if (resultObject.has(COMPANY_NAME))
			customerDetails.setCompanyName(resultObject.get(COMPANY_NAME).getAsString());
		customerDetails.setCompanyPhoneNumber(EMPTY);
		customerDetails.setCompanyAddress(EMPTY);

		// Adding order summary in response
		String orderResponse = getSortedOrderByCustomerId(customerId, limit);
		JsonObject orderJsonObject = MolekuleUtility.parseJsonObject(orderResponse);
		int count = orderJsonObject.get("count").getAsInt();
		List<OrderSummary> orderSummaryList = new ArrayList<>();
		if (orderJsonObject.get(RESULTS).getAsJsonArray().size() != 0) {
			processOrderSummary(count, orderJsonObject, orderSummaryList);
		}
		String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + "customer-id="
				+ customerId;

		JsonObject cartObject = MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(token, cartUrl));
		if (cartObject.has("id")) {
			customerDetails.setCartId(cartObject.get("id").getAsString());
		}

		customerDetails.setOrderSummaryList(orderSummaryList);
		return customerDetails;
	}

	private void setCustomfields(CustomerDetailBean customerDetails, JsonObject fieldsObject) {
		if(fieldsObject.has("customerSource")) {
			customerDetails.setCustomerSource(fieldsObject.get("customerSource").getAsString());
		}
		customerDetails.setLifeTimeSales(fieldsObject.has(LIFE_TIME_SALES)?fieldsObject.get(LIFE_TIME_SALES).getAsString():null);
		customerDetails.setChannel(fieldsObject.has(CHANNEL)?fieldsObject.get(CHANNEL).getAsString():null);
		customerDetails.setComments(fieldsObject.has("comments")?fieldsObject.get("comments").getAsString():null);
		customerDetails.setRegisteredChannel(fieldsObject.has("registeredChannel")?fieldsObject.get("registeredChannel").getAsString():null);
		if(fieldsObject.has("recentlyUsedAddress")) {
			customerDetails.setRecentlyUsedAddressId(fieldsObject.get("recentlyUsedAddress").getAsString());			
		}
		if(fieldsObject.has("accountLocked")) {
			customerDetails.setAccountLocked(fieldsObject.get("accountLocked").getAsBoolean());			
		}
		if(fieldsObject.has(PHONE))
			customerDetails.setPhoneNumber(fieldsObject.get(PHONE).getAsString());
		
		if(fieldsObject.has("store")) {
			String store = fieldsObject.get("store").getAsString();
			customerDetails.setStore(store);
			if(store.contains("US")) {
				customerDetails.setCountry("US");
			} else if(store.contains("CA")) {
				customerDetails.setCountry("CA");
			} else if(store.contains("UK")) {
				customerDetails.setCountry("GB");
			}
		}
		if(fieldsObject.has("termsOfService")) {
			customerDetails.setTermsOfService(fieldsObject.get("termsOfService").getAsBoolean());
		}
		if(fieldsObject.has("marketingOffers")) {
			customerDetails.setMarketingOffers(fieldsObject.get("marketingOffers").getAsBoolean());
		}
	}

	private void customerDataRequestValidation(String customerId, String email, String customerSource) {
		/* We are throwing error when no filters in the request. */

		if (!StringUtils.hasText(customerId) && !StringUtils.hasText(email) && !StringUtils.hasText(customerSource)) {
			throw new CustomRunTimeException("Please fill CustomerId/Email/customerSource");
		}

		/* We are throwing error when received only customer source. */

		if (!StringUtils.hasText(customerId) && !StringUtils.hasText(email) && StringUtils.hasText(customerSource)) {
			throw new CustomRunTimeException("Please fill CustomerId/Email");

		}
	}

	private void setTermRelatedDetails(String customerId, CustomerDetailBean customerDetails,
			List<BillingAddresses> billingAddressesList, JsonObject accountResultObject, Value value) {
		List<String> billingAddressIdList = new ArrayList<>();
		boolean paymentTerm = false;
		List<Payments> customerResponsePayment = new ArrayList<>();
		if (value.getPayments() != null) {
			paymentTerm = paymentProcessing(customerId, value, billingAddressIdList, paymentTerm,
					customerResponsePayment);
		}
		if (!paymentTerm) {
			nonPaymentTermProcess(billingAddressesList, value, billingAddressIdList);
		} else {
			billingAddressesList = value.getAddresses().getBillingAddresses();
		}
		customerDetails.setBillingAddresses(billingAddressesList);
		customerDetails.setPayments(customerResponsePayment);
		customerDetails.setCustomerType(accountResultObject.get(CONTAINER).getAsString());
	}

	private BillingAddresses processDefaultBillingAddress(BillingAddresses defaultBillingAddresses, Value value) {
		if (value.getDefaultBillingAddressId() != null) {

			String defaultBillingAddressId = value.getDefaultBillingAddressId();
			List<BillingAddresses> billingAddresses = value.getAddresses().getBillingAddresses();
			billingAddresses = billingAddresses.stream()
					.filter(action -> action.getBillingAddressId().equals(defaultBillingAddressId))
					.collect(Collectors.toList());
			if (!billingAddresses.isEmpty())
				defaultBillingAddresses = billingAddresses.get(0);
		}
		return defaultBillingAddresses;
	}

	private ShippingAddresses processDefaultShippingAddressId(ShippingAddresses defaultShippingAddresses, Value value) {
		if (value.getDefaultShippingAddressId() != null) {

			String defaultShippingAddressId = value.getDefaultShippingAddressId();
			List<ShippingAddresses> shippingAddresses = value.getAddresses().getShippingAddresses();
			shippingAddresses = shippingAddresses.stream()
					.filter(action -> action.getShippingAddressId().equals(defaultShippingAddressId))
					.collect(Collectors.toList());
			if (!shippingAddresses.isEmpty())
				defaultShippingAddresses = shippingAddresses.get(0);
		}
		return defaultShippingAddresses;
	}

	private void processOrderSummary(int count, JsonObject orderJsonObject, List<OrderSummary> orderSummaryList) {
		for (int i = 0; i < count; i++) {
			OrderSummary orderSummary = new OrderSummary();
			JsonObject ordersResultObject = orderJsonObject.get(RESULTS).getAsJsonArray().get(i).getAsJsonObject();
			if (ordersResultObject.has("id"))
				orderSummary.setOrderId(ordersResultObject.get("id").getAsString());
			if (ordersResultObject.has(ORDER_NUMBER))
				orderSummary.setOrderNumber(ordersResultObject.get(ORDER_NUMBER).getAsString());
			if (ordersResultObject.has("orderState"))
				orderSummary.setOrderStatus(ordersResultObject.get("orderState").getAsString());
			if (ordersResultObject.has(CUSTOM) 
					&& ordersResultObject.get(CUSTOM).getAsJsonObject().has(FIELDS) 
					&& ordersResultObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(ORDER_TOTAL))
				orderSummary.setOrderTotal(ordersResultObject.get(CUSTOM).getAsJsonObject().get(FIELDS)
						.getAsJsonObject().get(ORDER_TOTAL).getAsString());
			if(ordersResultObject.has(STORE))
				orderSummary.setStore(ordersResultObject.get(STORE).getAsJsonObject().get("key").getAsString());
			orderSummaryList.add(orderSummary);
		}
	}

	private void processCustomerGroup(CustomerDetailBean customerDetails, JsonObject resultObject) {
		JsonObject customerGroupObject = resultObject.get(CUSTOMER_GROUP).getAsJsonObject();
		CustomerGroup customerGroup = new CustomerGroup();
		customerGroup.setTypeId(customerGroupObject.get("typeId").getAsString());
		customerGroup.setCustomerGroupId(customerGroupObject.get("id").getAsString());
		customerDetails.setCustomerGroup(customerGroup);
		if(customerGroupObject.has("obj")) {
			customerDetails.setCustomerGroupName(customerGroupObject.get("obj").getAsJsonObject().get("name").getAsString());
		}
	}

	private String getCustomerBaseURL(String customerId, String email, String customerSource, String customerBaseUrl) {

		if (StringUtils.hasText(email)) {
			customerBaseUrl = customerBaseUrl + WHERE + MolekuleUtility.convertToEncodeUri("email=\"" + email + "\"");
		}

		if (StringUtils.hasText(customerId)) {
			customerBaseUrl = customerBaseUrl + WHERE+ MolekuleUtility.convertToEncodeUri("id=\"" + customerId + "\"") ;
		}

		if (StringUtils.hasText(customerSource)) {
			customerBaseUrl = customerBaseUrl + WHERE+ MolekuleUtility.convertToEncodeUri("custom(fields(customerSource=\"" + customerSource + "\"))");
		}

		return customerBaseUrl;
	}

	private void nonPaymentTermProcess(List<BillingAddresses> billingAddressesList, Value value,
			List<String> billingAddressIdList) {
		for (int i = 0; i < billingAddressIdList.size(); i++) {
			String id = billingAddressIdList.get(i);
			List<BillingAddresses> addressList = value.getAddresses().getBillingAddresses().stream()
					.filter(action -> action.getBillingAddressId().equals(id)).collect(Collectors.toList());
			if (!addressList.isEmpty())
				billingAddressesList.add(addressList.get(0));
		}
	}

	private boolean paymentProcessing(String customerId, Value value, List<String> billingAddressIdList,
			boolean paymentTerm, List<Payments> customerResponsePayment) {
		List<Payments> paymentList = value.getPayments();
		for (int i = 0; i < paymentList.size(); i++) {
			Payments payments = paymentList.get(i);

			if (payments.getCustomerId().equals(customerId) && "CREDITCARD".equals(payments.getPaymentType())) {
				billingAddressIdList.add(payments.getBillingAddressId());
				customerResponsePayment.add(payments);
			} else if ("PAYMENTTERMS".equals(payments.getPaymentType())) {
				paymentTerm = true;
				customerResponsePayment.add(payments);
			} else if ("ACH".equals(payments.getPaymentType())) {
				customerResponsePayment.add(payments);
			}
		}
		return paymentTerm;
	}

	/**
	 * getSortedOrderByCustomerId method is used to get sorted list of Order Data
	 * sorted by desc.
	 * 
	 * @return String- it contain all Order related values.
	 */
	public String getSortedOrderByCustomerId(String customerId, int limit) {
		String ordersBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/orders?sort=createdAt desc&limit=" + limit + "&where=customerId=\"" + customerId + "\"";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		return netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
	}
}
