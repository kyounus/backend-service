package com.molekule.api.v1.commonframework.model.checkout;

import java.util.List;

import lombok.Data;
@Data
public class CTSetBillingAddress {

	private long version;
	List<Action> actions;
	@Data
	public static class Action{

		private Address address;
		@Data
		public static class Address{
			 private String id;
			 private String key;
             private String title;
             private String salutation;
             private String firstName;
             private String lastName;
             private String streetName;
             private String streetNumber;
             private String additionalStreetInfo;
             private String postalCode;
             private String city;
             private String region;
             private String state;
             private String country;
             private String company;
             private String department;
             private String building;
             private String apartment;
             private String pOBox;
             private String phone;
             private String mobile;
             private String email;
             private String fax;
             private String additionalAddressInfo;
             private String externalId;
		}
	}
}
