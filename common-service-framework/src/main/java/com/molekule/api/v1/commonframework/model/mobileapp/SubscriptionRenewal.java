package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class SubscriptionRenewal {

	private String id;
	private String customer_id;
	private String next_order;
}
