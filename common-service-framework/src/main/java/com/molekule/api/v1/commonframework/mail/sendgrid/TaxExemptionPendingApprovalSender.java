package com.molekule.api.v1.commonframework.mail.sendgrid;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;
/**
 * TaxExemptionPendingApprovalSender is a helper class.it will Build SendGrid
 * Dynamic data
 * 
 * @version 1.0
 */

@Component("taxExemptionPendingApprovalSender")
public class TaxExemptionPendingApprovalSender extends TemplateMailRequestHelper {

	@Value("${taxPendingApprovalTemplateId}")
	private String templateId;

	@Value("${taxApprovalAccountMailID}")
	private String toEmail;
	
	@Value("${taxApprovalToName}")
	private String toName;
	
	@Value("${taxApprovalCustomerRecord}")
	private String statusUrl;
	
	@Value("${taxApprovalCustomerCompanyRecord}")
	private String customerCompanyRecordUrl;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public String getToEmail() {
		return toEmail;
	}

	@Override
	public String getToName() {
		return toName;
	}
	
	public String getCustomerCompanyRecordUrl() {
		return customerCompanyRecordUrl;
	}

	public void setCustomerCompanyRecordUrl(String customerCompanyRecordUrl) {
		this.customerCompanyRecordUrl = customerCompanyRecordUrl;
	}

	@Override
	public SendGridRequestBean prepareRequestBean(Object obj) {
		SendGridModel sendGridModel = (SendGridModel) obj;
		SendGridRequestBean mailResponseBean = new SendGridRequestBean();
		SendGridRequestBean.Personalizations personalizations = getPersonalizations(sendGridModel);
		SendGridRequestBean.From fromMail = new SendGridRequestBean.From();
		mailResponseBean.setTemplateId(templateId());
		fromMail.setEmail(sendGridModel.getEmail());
		fromMail.setName(sendGridModel.getFirstName());
		mailResponseBean.setFrom(fromMail);
		List<SendGridRequestBean.Personalizations> personalizationsList = new ArrayList<>();
		personalizationsList.add(personalizations);
		mailResponseBean.setPersonalizations(personalizationsList);
		return mailResponseBean;
	}

	@Override
	public SendGridRequestBean.Personalizations getPersonalizations(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations personalizations = new SendGridRequestBean.Personalizations();
		SendGridRequestBean.Personalizations.To to = new SendGridRequestBean.Personalizations.To();
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = getDynamicTemplateData(
				sendGridModel);
		to.setEmail(getToEmail());
		to.setName(getToName());
		List<SendGridRequestBean.Personalizations.To> toList = new ArrayList<>();
		toList.add(to);
		personalizations.setTo(toList);
		personalizations.setDynamicTemplateData(dynamicTemplateData);
		return personalizations;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setRequestedDate(sendGridModel.getRequestedDate());
		dynamicTemplateData.setCustomerFirstName(sendGridModel.getFirstName());
		dynamicTemplateData.setCustomerLastName(sendGridModel.getLastName());
		dynamicTemplateData.setCustomerCompanyName(sendGridModel.getCustomerCompanyName());
		dynamicTemplateData.setCustomerCompanyIndustry(sendGridModel.getCompanyIndustry());
		dynamicTemplateData.setCustomerEmail(sendGridModel.getEmail());
		dynamicTemplateData.setCustomerPhoneNumber(sendGridModel.getPhoneNumber());
		dynamicTemplateData.setSalesRepresentativeFirstName(sendGridModel.getSalesRepFirstName());
		dynamicTemplateData.setSalesRepresentativeLastName(sendGridModel.getSalesRepLastName());
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber());
		dynamicTemplateData.setSalesRepresentativeCalendlyLink(sendGridModel.getSalesRepCalendyLink());
		return dynamicTemplateData;
	}
}
