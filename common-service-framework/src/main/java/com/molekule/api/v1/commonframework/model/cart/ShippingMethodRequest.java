package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;

import lombok.Data;

@Data
public class ShippingMethodRequest {

	private String name;
	private TaxCategory taxCategory;
	private List<ZoneRates> zoneRates;

	@Data
	public static class TaxCategory {
		private String typeId;
		private String id;
	}

	@Data
	public static class ZoneRates {
		private Zone zone;
		private List<ShippingRates> shippingRates;

		@Data
		public static class Zone {
			private String typeId;
			private String id;
		}

		@Data
		public static class ShippingRates {
			private Price price;

			@Data
			public static class Price {
				private String currencyCode;
				private long centAmount;
			}

		}
	}
}
