package com.molekule.api.v1.commonframework.dto.order;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class WarrentyOrderServiceBean {
	private Long version;
	private List<Action> actions = null;
	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		private String lineItemId;
		private Type type;
		private Fields fields;
		private String name;
		private Object value;
		@Data
		public static class Type {
			private String id;
			private String typeId;
		}
		@Data
		public static class Fields {
			private List<String> warrantyOrderNumbers;
		}
	}
}
