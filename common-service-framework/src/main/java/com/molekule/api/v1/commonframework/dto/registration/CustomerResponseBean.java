package com.molekule.api.v1.commonframework.dto.registration;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean.Action.CustomerGroup;
import com.molekule.api.v1.commonframework.model.registration.Address;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMAIL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIRST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ADDRESSES;

public class CustomerResponseBean {

	@JsonProperty("id")
	private String id;

	@JsonProperty(VERSION)
	private Long version;

	@JsonProperty("createdAt")
	private ZonedDateTime createdAt;

	@JsonProperty("lastModifiedAt")
	private ZonedDateTime lastModifiedAt;

	@JsonProperty("customerNumber")
	private String customerNumber;

	@JsonProperty(EMAIL)
	private String email;

	@JsonProperty(value = "password", access = Access.WRITE_ONLY)
	private String password;

	@JsonProperty(FIRST_NAME)
	private String firstName;

	@JsonProperty(LAST_NAME)
	private String lastName;

	@JsonProperty("middleName")
	private String middleName;

	@JsonProperty("title")
	private String title;

	@JsonProperty("dateOfBirth")
	private LocalDate dateOfBirth;

	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("vatId")
	private String vatId;

	@JsonProperty(ADDRESSES)
	private List<Address> addresses;

	@JsonProperty("defaultShippingAddressId")
	private String defaultShippingAddressId;

	@JsonProperty("shippingAddressIds")
	private List<String> shippingAddressIds;

	@JsonProperty("defaultBillingAddressId")
	private String defaultBillingAddressId;

	@JsonProperty("billingAddressIds")
	private List<String> billingAddressIds;

	@JsonProperty("isEmailVerified")
	private Boolean isEmailVerified;

	@JsonProperty("externalId")
	private String externalId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("stores")
	private List<StoreKeyReference> stores;
	
	@JsonProperty("customerGroup")
	private CustomerGroupRequestBean.Action.CustomerGroup customerGroup;
	
	@JsonProperty(CUSTOM)
	private Custom custom;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public ZonedDateTime getLastModifiedAt() {
		return lastModifiedAt;
	}

	public void setLastModifiedAt(ZonedDateTime lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getVatId() {
		return vatId;
	}

	public void setVatId(String vatId) {
		this.vatId = vatId;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public String getDefaultShippingAddressId() {
		return defaultShippingAddressId;
	}

	public void setDefaultShippingAddressId(String defaultShippingAddressId) {
		this.defaultShippingAddressId = defaultShippingAddressId;
	}

	public List<String> getShippingAddressIds() {
		return shippingAddressIds;
	}

	public void setShippingAddressIds(List<String> shippingAddressIds) {
		this.shippingAddressIds = shippingAddressIds;
	}

	public String getDefaultBillingAddressId() {
		return defaultBillingAddressId;
	}

	public void setDefaultBillingAddressId(String defaultBillingAddressId) {
		this.defaultBillingAddressId = defaultBillingAddressId;
	}

	public List<String> getBillingAddressIds() {
		return billingAddressIds;
	}

	public void setBillingAddressIds(List<String> billingAddressIds) {
		this.billingAddressIds = billingAddressIds;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public List<StoreKeyReference> getStores() {
		return stores;
	}

	public void setStores(List<StoreKeyReference> stores) {
		this.stores = stores;
	}
	
	public CustomerGroup getCustomerGroup() {
		return customerGroup;
	}

	public void setCustomerGroup(CustomerGroup customerGroup) {
		this.customerGroup = customerGroup;
	}

	public Custom getCustom() {
		return custom;
	}

	public void setCustom(Custom custom) {
		this.custom = custom;
	}
	
	

}
