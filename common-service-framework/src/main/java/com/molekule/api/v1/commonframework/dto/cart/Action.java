package com.molekule.api.v1.commonframework.dto.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;

import lombok.Data;

@Data
public class Action {

	private String action;
	@JsonProperty("address")
	private AddressRequestBean shippingAddress;
	private String name;
	private Object value;
}