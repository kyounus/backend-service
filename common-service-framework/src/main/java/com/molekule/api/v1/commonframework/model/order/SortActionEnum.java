package com.molekule.api.v1.commonframework.model.order;

public enum SortActionEnum {
	
	ORDER_NUMBER,

	ORDER_STATUS,

	EMAIL,

	DATE_CREATED,

	DATE_MODIFIED
}
