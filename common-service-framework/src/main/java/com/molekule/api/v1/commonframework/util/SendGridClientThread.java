package com.molekule.api.v1.commonframework.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.MAX_RETRY;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

import reactor.core.publisher.Mono;

/**
 * SendGridClientThread is used to write Send Grid Api call for quick
 * registration welcome mail.
 * 
 * @version 1.0
 */
public class SendGridClientThread implements Runnable {
	Logger logger = LoggerFactory.getLogger(SendGridClientThread.class);
	private Map<String, Object> requestData;

	@Override
	public void run() {
		logger.trace("Sending mail to customer on Registration confirmation....");
		SendGridRequestBean mailResponseBean = null;
		Map<String, Object> requestDataLocal = getRequestData();
		String sendGridBaseUrl = (String) requestDataLocal.get("sendGridUrl");
		String token = new StringBuilder().append(BEARER).append((String) requestDataLocal.get("sendGridApiKey"))
				.toString();
		logger.trace("Send Grid Url {}", sendGridBaseUrl);
		mailResponseBean = (SendGridRequestBean) requestDataLocal.get("requestBean");
		int maxRetry = 2;
		if (requestDataLocal.get(MAX_RETRY) != null && ((Integer) requestDataLocal.get(MAX_RETRY)) != 0) {
			maxRetry = (Integer) requestDataLocal.get(MAX_RETRY);
		}
		String responseData = null;

		for (int i = 0; i < maxRetry; i++) {
			boolean status = sendPostRequest(sendGridBaseUrl, token, mailResponseBean);
			if (status) {
				break;
			}
		}

		logger.debug("Customer Mail Respose Data - {}", responseData);
	}

	public boolean sendPostRequest(String baseUrl, String token, Object requestData) {
		WebClient webClient = WebClient.builder().baseUrl(baseUrl).build();
		ResponseEntity<String> response = webClient.post().header("Authorization", token)
				.accept(MediaType.APPLICATION_JSON).body(Mono.just(requestData), Object.class).retrieve()
				.toEntity(String.class).block();
		logger.trace("SendGrid Response - {}", response.getBody());
		return response.getStatusCode().is2xxSuccessful();
	}

	public Map<String, Object> getRequestData() {
		return requestData;
	}

	public void setRequestData(Map<String, Object> requestData) {
		this.requestData = requestData;
	}

}
