package com.molekule.api.v1.commonframework.util;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("tealiumUtility")
public class TealiumUtility {
	static Logger logger = LoggerFactory.getLogger(TealiumUtility.class);
	
	private static final String TEALIUM_URL="https://collect.tealiumiq.com/event?tealium_account=molekule&tealium_profile=enterprise";
		
	private TealiumUtility() {
	}
	
	/**
	 * sendCurlRequest is to send curl request to tealium dashboard
	 * 
	 * @param requestBean
	 * @return
	 */
	public static String sendCurlRequest(String requestBean) {
		String response = null;
		try {
			URL url = new URL(TEALIUM_URL);
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			http.setRequestProperty("Content-type", "application/json");
			byte[] out = requestBean.getBytes(StandardCharsets.UTF_8);
			OutputStream stream = http.getOutputStream();
			stream.write(out);
			response = http.getResponseCode() + " " + http.getResponseMessage();
			http.disconnect();

		} catch (Exception e) {
			 logger.error("Exception occured sending curl request", e);
		}
		return response;
	
	}
	
	/**
	 * convertToBigDecimal is to convert price to big decimal
	 * 
	 * @param price
	 * @return
	 */
	public static BigDecimal convertToBigDecimal(int price) {
		return new BigDecimal(price).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_EVEN);
	}


}
