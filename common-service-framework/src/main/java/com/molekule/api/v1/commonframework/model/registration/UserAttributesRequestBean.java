package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

@Data
public class UserAttributesRequestBean {

	private String accessToken;
}
