package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class CustomerStripePayment {
    private int id;
    private String customer_id;
    private String stripe_id;
    private String last_retrieved;
    private String customer_email;
    private String session_id;
}
