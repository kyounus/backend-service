package com.molekule.api.v1.commonframework.model.products;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CURRENT;

public class ProductCatalogData {

	@JsonProperty("published")
	private Boolean published;

	@JsonProperty(CURRENT)
	private ProductData current;

	@JsonProperty("staged")
	private ProductData staged;

	@JsonProperty("hasStagedChanges")
	private Boolean gehasStagedChanges;

	public Boolean getPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public ProductData getCurrent() {
		return current;
	}

	public void setCurrent(ProductData current) {
		this.current = current;
	}

	public ProductData getStaged() {
		return staged;
	}

	public void setStaged(ProductData staged) {
		this.staged = staged;
	}

	public Boolean getGehasStagedChanges() {
		return gehasStagedChanges;
	}

	public void setGehasStagedChanges(Boolean gehasStagedChanges) {
		this.gehasStagedChanges = gehasStagedChanges;
	}

	@Override
	public String toString() {
		return "ProductCatalogData [published=" + published + ", current=" + current + ", staged=" + staged
				+ ", gehasStagedChanges=" + gehasStagedChanges + "]";
	}
	
	

}
