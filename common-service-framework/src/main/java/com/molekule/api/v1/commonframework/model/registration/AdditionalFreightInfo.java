package com.molekule.api.v1.commonframework.model.registration;

import com.molekule.api.v1.commonframework.model.registration.CarrierOption.ReceivingHours;

import lombok.Data;

@Data
public class AdditionalFreightInfo {

	private boolean loadingDock;
	
	private boolean forkLift;
	
	private boolean deliveryAppointmentRequired;
	
	private ReceivingHours receivingHours;
	
	private String instructions;
	
	private String shippingAddressId;
}
