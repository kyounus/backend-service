package com.molekule.api.v1.commonframework.dto.registration;

import java.util.List;

import lombok.Data;

@Data
public class AccountDto {
	private int limit;
	private int offset;
	private int count;
	private int total;
	private List<AccountCustomObject> results;
}
