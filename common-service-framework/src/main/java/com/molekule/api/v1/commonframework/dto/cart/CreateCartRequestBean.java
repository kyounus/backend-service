package com.molekule.api.v1.commonframework.dto.cart;

import lombok.Data;

@Data
public class CreateCartRequestBean {

	private String customerId;
	private String tealiumVisitorId;
	private String currency;
	private String store;
	private String country;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getTealiumVisitorId() {
		return tealiumVisitorId;
	}

	public void setTealiumVisitorId(String tealiumVisitorId) {
		this.tealiumVisitorId = tealiumVisitorId;
	}

}
