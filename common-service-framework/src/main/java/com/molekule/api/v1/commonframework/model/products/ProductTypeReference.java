package com.molekule.api.v1.commonframework.model.products;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ATTRIBUTES;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProductTypeReference {

	@JsonProperty("id")
	private String id;
	@JsonProperty("typeId")
	private String typeId;
	
	@JsonProperty(ATTRIBUTES)
	private List<AttributeDefinition> attributes;

	@JsonProperty("obj")
	private Obj obj;
	
	@Data
	public static class Obj {
		@JsonProperty("key")
		private String key;

		@JsonProperty("name")
		private String name;
		
		@JsonProperty("classifier")
		private String classifier;
		
		@JsonProperty("description")
		private String description;
		
	}

}
