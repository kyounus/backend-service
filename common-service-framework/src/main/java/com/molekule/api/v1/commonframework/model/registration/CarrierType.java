package com.molekule.api.v1.commonframework.model.registration;

public enum CarrierType {
	PARCEL,FREIGHT,BOTH
}
