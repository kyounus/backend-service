package com.molekule.api.v1.commonframework.model.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.STATE;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TaxRate {

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("amount")
	private Double amount;

	@JsonProperty("includedInPrice")
	private Boolean includedInPrice;

	@JsonProperty(COUNTRY)
	private String country;

	@JsonProperty(STATE)
	private String state;

	@JsonProperty("subRates")
	private List<SubRate> subRates;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getIncludedInPrice() {
		return includedInPrice;
	}

	public void setIncludedInPrice(Boolean includedInPrice) {
		this.includedInPrice = includedInPrice;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<SubRate> getSubRates() {
		return subRates;
	}

	public void setSubRates(List<SubRate> subRates) {
		this.subRates = subRates;
	}

}
