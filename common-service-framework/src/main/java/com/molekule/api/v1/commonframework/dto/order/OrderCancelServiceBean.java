package com.molekule.api.v1.commonframework.dto.order;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class OrderCancelServiceBean {
	private Long version;
	private List<Action> actions = null;

	@Data
	public static class Action {

		@JsonProperty(ACTION)
		private String actionName;
		private String orderState;
		private State state;

	}
	@Data
	public static class State {
		private String id;
		private String typeId;
	} 
}
