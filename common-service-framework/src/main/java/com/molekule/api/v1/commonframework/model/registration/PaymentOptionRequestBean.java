package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class PaymentOptionRequestBean is used to hold the request of payment option.
 */
public class PaymentOptionRequestBean {

	public enum Payment{
		CREDITCARD,ACH,CREDITAPPROVAL
	}
	public enum BusinessType{
		PUBLIC,PRIVATE,GOVERNMENT,NONPROFIT
	}
	private String accountId;
	private String customerId;
	private String customerNumber;
	private String paymentPreferences;
	private String creditCardNumber;
	private String expiryMonth;
	private Integer expiryYear;
	private String cvv;
	private String paymentToken;
	private boolean isBillingAddressEnable;
	private BillingAddressRequestBean billingAddress;
	private int version;
	private String downloadUrl;
	private String businessContact;
	private String financeContact;
	private String businessType;
	private BusinessReferences businessReference1;
	private BusinessReferences businessReference2;
	private BankInformation bankInformation;
	private GovernmentVerification governmentVerification;
	private StockDetails stockDetails;
	private String proofOfNonProfitStatus;
	
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getPaymentPreferences() {
		return paymentPreferences;
	}

	public void setPaymentPreferences(String paymentPreferences) {
		this.paymentPreferences = paymentPreferences;
	}
	
	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getExpiryMonth() {
		return expiryMonth;
	}

	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public Integer getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(Integer expiryYear) {
		this.expiryYear = expiryYear;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}


	public boolean isBillingAddressEnable() {
		return isBillingAddressEnable;
	}

	public void setBillingAddressEnable(boolean isBillingAddressEnable) {
		this.isBillingAddressEnable = isBillingAddressEnable;
	}

	public BillingAddressRequestBean getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddressRequestBean billingAddress) {
		this.billingAddress = billingAddress;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getDownloadUrl() {
		return downloadUrl;
	}
	
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	public String getbusinessContact() {
		return businessContact;
	}
	
	public void setBusinessContact(String businessContact) {
		this.businessContact = businessContact;
	}
	
	public String getFinanceContact() {
		return financeContact;
	}
	
	public void setFinanceContact(String financeContact) {
		this.financeContact = financeContact;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public BusinessReferences getBusinessReference1() {
		return businessReference1;
	}

	public void setBusinessReference1(BusinessReferences businessReference1) {
		this.businessReference1 = businessReference1;
	}

	public BusinessReferences getBusinessReference2() {
		return businessReference2;
	}

	public void setBusinessReference2(BusinessReferences businessReference2) {
		this.businessReference2 = businessReference2;
	}

	public BankInformation getBankInformation() {
		return bankInformation;
	}

	public void setBankInformation(BankInformation bankInformation) {
		this.bankInformation = bankInformation;
	}
	
	public GovernmentVerification getGovernmentVerification() {
		return governmentVerification;
	}

	public void setGovernmentVerification(GovernmentVerification governmentVerification) {
		this.governmentVerification = governmentVerification;
	}
	
	public StockDetails getStockDetails() {
		return stockDetails;
	}
	
	public void setStockDetails(StockDetails stockDetails) {
		this.stockDetails = stockDetails;
	}

	public String getProofOfNonProfitStatus() {
		return proofOfNonProfitStatus;
	}

	public void setProofOfNonProfitStatus(String proofOfNonProfitStatus) {
		this.proofOfNonProfitStatus = proofOfNonProfitStatus;
	}
	
	
}
