package com.molekule.api.v1.commonframework.model.products;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
public class Attribute {

	@JsonProperty("name")
	private String name;

	/**
	 * <p>
	 * A valid JSON value, based on an AttributeDefinition.
	 * </p>
	 */
	@JsonProperty(VALUE)
	private Object value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
