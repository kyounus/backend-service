package com.molekule.api.v1.commonframework.model.registration;

import io.swagger.annotations.ApiModelProperty;

/**
 * The class ShippingAddressResponseBean is used to hold the response of shipping address.
 */
public class ShippingAddressResponseBean {

	private String accountId;
	private String customerId;
	private String customerNumber;
	
	private String companyName;
	
	@ApiModelProperty(required = true)
	private String firstName;
	
	@ApiModelProperty(required = true)
	private String lastName;
	
	@ApiModelProperty(required = true)
	private StreetLine streetAddress1;
	
	private StreetLine streetAddress2;
	
	@ApiModelProperty(required = true)
	private String city;
	
	@ApiModelProperty(required = true)
	private String state;
	
	@ApiModelProperty(required = true)
	private String postalCode;
	
	private boolean preferredShippingAddress;
	
	private boolean myCarrierInfo;
	
	private MolekuleCarrier ownCarrier;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public StreetLine getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(StreetLine streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public StreetLine getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(StreetLine streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public boolean isPreferredShippingAddress() {
		return preferredShippingAddress;
	}

	public void setPreferredShippingAddress(boolean preferredShippingAddress) {
		this.preferredShippingAddress = preferredShippingAddress;
	}

	public boolean isMyCarrierInfo() {
		return myCarrierInfo;
	}

	public void setMyCarrierInfo(boolean myCarrierInfo) {
		this.myCarrierInfo = myCarrierInfo;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public MolekuleCarrier getOwnCarrier() {
		return ownCarrier;
	}

	public void setOwnCarrier(MolekuleCarrier ownCarrier) {
		this.ownCarrier = ownCarrier;
	}
	
	
}
