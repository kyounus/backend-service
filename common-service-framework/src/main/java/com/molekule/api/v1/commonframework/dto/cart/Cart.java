package com.molekule.api.v1.commonframework.dto.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.cart.CustomLineItem;
import com.molekule.api.v1.commonframework.model.cart.DiscountCodeInfo;
import com.molekule.api.v1.commonframework.model.cart.LineItem;
import com.molekule.api.v1.commonframework.model.cart.PaymentInfo;
import com.molekule.api.v1.commonframework.model.cart.ShippingInfo;
import com.molekule.api.v1.commonframework.model.cart.ShippingRateInput;
import com.molekule.api.v1.commonframework.model.cart.TaxedPrice;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;
import com.molekule.api.v1.commonframework.model.registration.Address;
public class Cart {
	
	@JsonProperty("type")
	private String type;

    @JsonProperty("id")
    private String id;

    @JsonProperty(VERSION)
    private Long version;

    @JsonProperty(CUSTOMER_ID)
    private String customerId;

    @JsonProperty("customerEmail")
    private String customerEmail;

    @JsonProperty(ANONYMOUSID)
    private String anonymousId;

    @JsonProperty(LINE_ITEMS)
    private List<LineItem> lineItems;

    @JsonProperty(CUSTOM_LINE_ITEMS)
    private List<CustomLineItem> customLineItems;

    @JsonProperty(TOTAL_PRICE)
    private TypedMoney totalPrice;

    @JsonProperty("taxedPrice")
    private TaxedPrice taxedPrice;

    @JsonProperty("cartState")
    private String cartState;

    @JsonProperty(SHIPPING_ADDRESS)
    private Address shippingAddress;

    @JsonProperty(BILLING_ADDRESS)
    private Address billingAddress;

    @JsonProperty("inventoryMode")
    private String inventoryMode;

    @JsonProperty("taxMode")
    private String taxMode;

    @JsonProperty("taxRoundingMode")
    private String taxRoundingMode;

    @JsonProperty("taxCalculationMode")
    private String taxCalculationMode;

    @JsonProperty(COUNTRY)
    private String country;

    @JsonProperty(SHIPPING_INFO)
    private ShippingInfo shippingInfo;

    @JsonProperty(DISCOUNT_CODES)
    private List<DiscountCodeInfo> discountCodes;

    @JsonProperty(PAYMENT_INFO)
    private PaymentInfo paymentInfo;

    @JsonProperty("locale")
    private String locale;

    @JsonProperty("deleteDaysAfterLastModification")
    private Integer deleteDaysAfterLastModification;

    @JsonProperty("refusedGifts")
    private List<String> refusedGifts;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("shippingRateInput")
    private ShippingRateInput shippingRateInput;

    @JsonProperty("itemShippingAddresses")
    private List<Address> itemShippingAddresses;
    
    private String tealiumVisitorId;

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getAnonymousId() {
		return anonymousId;
	}

	public void setAnonymousId(String anonymousId) {
		this.anonymousId = anonymousId;
	}

	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	public List<CustomLineItem> getCustomLineItems() {
		return customLineItems;
	}

	public void setCustomLineItems(List<CustomLineItem> customLineItems) {
		this.customLineItems = customLineItems;
	}

	public TypedMoney getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(TypedMoney totalPrice) {
		this.totalPrice = totalPrice;
	}

	public TaxedPrice getTaxedPrice() {
		return taxedPrice;
	}

	public void setTaxedPrice(TaxedPrice taxedPrice) {
		this.taxedPrice = taxedPrice;
	}

	public String getCartState() {
		return cartState;
	}

	public void setCartState(String cartState) {
		this.cartState = cartState;
	}

	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getInventoryMode() {
		return inventoryMode;
	}

	public void setInventoryMode(String inventoryMode) {
		this.inventoryMode = inventoryMode;
	}

	public String getTaxMode() {
		return taxMode;
	}

	public void setTaxMode(String taxMode) {
		this.taxMode = taxMode;
	}

	public String getTaxRoundingMode() {
		return taxRoundingMode;
	}

	public void setTaxRoundingMode(String taxRoundingMode) {
		this.taxRoundingMode = taxRoundingMode;
	}

	public String getTaxCalculationMode() {
		return taxCalculationMode;
	}

	public void setTaxCalculationMode(String taxCalculationMode) {
		this.taxCalculationMode = taxCalculationMode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ShippingInfo getShippingInfo() {
		return shippingInfo;
	}

	public void setShippingInfo(ShippingInfo shippingInfo) {
		this.shippingInfo = shippingInfo;
	}

	public List<DiscountCodeInfo> getDiscountCodes() {
		return discountCodes;
	}

	public void setDiscountCodes(List<DiscountCodeInfo> discountCodes) {
		this.discountCodes = discountCodes;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Integer getDeleteDaysAfterLastModification() {
		return deleteDaysAfterLastModification;
	}

	public void setDeleteDaysAfterLastModification(Integer deleteDaysAfterLastModification) {
		this.deleteDaysAfterLastModification = deleteDaysAfterLastModification;
	}

	public List<String> getRefusedGifts() {
		return refusedGifts;
	}

	public void setRefusedGifts(List<String> refusedGifts) {
		this.refusedGifts = refusedGifts;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public ShippingRateInput getShippingRateInput() {
		return shippingRateInput;
	}

	public void setShippingRateInput(ShippingRateInput shippingRateInput) {
		this.shippingRateInput = shippingRateInput;
	}

	public List<Address> getItemShippingAddresses() {
		return itemShippingAddresses;
	}

	public void setItemShippingAddresses(List<Address> itemShippingAddresses) {
		this.itemShippingAddresses = itemShippingAddresses;
	}
    
	public String getTealiumVisitorId() {
		return tealiumVisitorId;
	}

	public void setTealiumVisitorId(String tealiumVisitorId) {
		this.tealiumVisitorId = tealiumVisitorId;
	}
    
}
