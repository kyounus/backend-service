package com.molekule.api.v1.commonframework.model.order;

import lombok.Data;

@Data
public class SerialNumberValidationBean {

	private String serialNumber;
	
}
