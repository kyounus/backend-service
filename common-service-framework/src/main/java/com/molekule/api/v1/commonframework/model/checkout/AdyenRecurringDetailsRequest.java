package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class AdyenRecurringDetailsRequest {

	private Recurring recurring;
	@Data
	public static class Recurring{
		private String contract;
	}
	private String shopperReference;
	private String merchantAccount;
	
}
