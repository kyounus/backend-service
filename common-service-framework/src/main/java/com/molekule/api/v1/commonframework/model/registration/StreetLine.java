package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class StreetLine is used to hold the street line.
 */
public class StreetLine {

	private String street;
	private String landMark;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLandMark() {
		return landMark;
	}

	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}

}
