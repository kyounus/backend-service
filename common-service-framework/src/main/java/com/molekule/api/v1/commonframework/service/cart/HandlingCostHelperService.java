package com.molekule.api.v1.commonframework.service.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LINE_ITEMS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PRODUCT_TYPE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.QUANTITY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.RESULTS;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VARIANT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
@Service("handlingCostHelperService")
public class HandlingCostHelperService {
	
	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;
	
	@Autowired
	@Qualifier("netConnectionHelper")
	private NetConnectionHelper netConnectionHelper;
	
	@Autowired
	CTEnvProperties ctEnvProperties;
	
	@Autowired
	HandlingCostCalculationService handlingCostCalculationService;
	/**
	 * calculateHandlingCostForParcelAndFreight
	 * @param cartId
	 * @param token 
	 * @return String
	 */
	public long calculateHandlingCostForParcelAndFreight(String cartId, String token) {
		String url = new StringBuilder().append(ctEnvProperties.getHost()).append("/")
				.append(ctEnvProperties.getProjectKey()).append("/carts?").append("where=id=\"")
				.append(cartId).append("\"").append("&expand=lineItems[*].productType.id").toString();
		String cartReponse= netConnectionHelper.sendGetWithoutBody(token,url);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartReponse);
		JsonArray resultArray = jsonObject.get(RESULTS).getAsJsonArray();
		JsonArray lineItemsArray = resultArray.get(0).getAsJsonObject().get(LINE_ITEMS).getAsJsonArray();
		int deviceCount =0;
		int filterCount =0;
		Boolean parcelFlag = Boolean.TRUE;
		String calculatedHandlingCharge = "0";
		return getCalculateHAndlingCharge(token, lineItemsArray, deviceCount, filterCount, parcelFlag,
				calculatedHandlingCharge);
	}

	private long getCalculateHAndlingCharge(String token, JsonArray lineItemsArray, int deviceCount, int filterCount,
			Boolean parcelFlag, String calculatedHandlingCharge) {
		if(lineItemsArray.size()!=0) {
			return processLineItemsArray(token, lineItemsArray, deviceCount, filterCount, parcelFlag,
					calculatedHandlingCharge);
		} else {
			return Long.parseLong(calculatedHandlingCharge);
		}
	}

	private long processLineItemsArray(String token, JsonArray lineItemsArray, int deviceCount, int filterCount,
			Boolean parcelFlag, String calculatedHandlingCharge) {
		int quantity;
		for(int i =0;i<lineItemsArray.size();i++) {
			String key = lineItemsArray.get(i).getAsJsonObject().get(PRODUCT_TYPE).getAsJsonObject().get("obj").getAsJsonObject().get("key").getAsString();
			if(key.equals("purifier")) {
				quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
				deviceCount = deviceCount + quantity;
			} else if(key.equals("filter")) {
				quantity = lineItemsArray.get(i).getAsJsonObject().get(QUANTITY).getAsInt();
				filterCount = filterCount + quantity;
			}
			if(!key.equals("subscription") 
					&& lineItemsArray.get(i).getAsJsonObject().get(VARIANT).getAsJsonObject().get("sku").getAsString().equals(ctEnvProperties.getMolekuleAirProSku())) {
				parcelFlag = Boolean.FALSE;
			}
		}
		// get the value of shipping table from CT
		String shippingTableBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/custom-objects"+ "/" + 
				ctEnvProperties.getShippingTableContainer() +"/" + ctEnvProperties.getShippingTableKey();
		String shippingTableDetailResponse = netConnectionHelper.sendGetWithoutBody(token,shippingTableBaseUrl);
		JsonObject shippingTableJsonObject = MolekuleUtility.parseJsonObject(shippingTableDetailResponse);
		JsonObject thresholdJsonObject = shippingTableJsonObject.get(VALUE).getAsJsonObject().get("freight").getAsJsonObject().get("threshold").getAsJsonObject();
		int freightThresholdDevice = thresholdJsonObject.get("device").getAsInt();
		int freightThresholdFilter = thresholdJsonObject.get("filter").getAsInt();
		Boolean mixedCart = (deviceCount !=0 && filterCount !=0) ? Boolean.TRUE: Boolean.FALSE;
		if(mixedCart) {
			calculatedHandlingCharge = doMixedCartOperation(deviceCount, filterCount, parcelFlag,
					shippingTableJsonObject, freightThresholdDevice, freightThresholdFilter);
		} else {
			calculatedHandlingCharge = doNonMixedCartOperation(deviceCount, filterCount, parcelFlag,
					shippingTableJsonObject, freightThresholdDevice, freightThresholdFilter, calculatedHandlingCharge);
		}
		return Long.parseLong(calculatedHandlingCharge);
	}

	/**
	 * doNonMixedCartOperation method is used check for whether the cart handling cost based on parcel or frieght for Non-Mixed cart items.
	 * @param deviceCount
	 * @param filterCount
	 * @param parcelFlag
	 * @param shippingTableJsonObject
	 * @param freightThresholdDevice
	 * @param freightThresholdFilter
	 * @param calculatedHandlingCharge
	 * @return String
	 */
	private String doNonMixedCartOperation(int deviceCount, int filterCount, Boolean parcelFlag,
			JsonObject shippingTableJsonObject, int freightThresholdDevice, int freightThresholdFilter,
			String calculatedHandlingCharge) {
		if(parcelFlag){
			if((deviceCount < freightThresholdDevice) && ( filterCount < freightThresholdFilter)) {
				handlingCostCalculationService = new ParcelHandlingCostCalculationServieImpl();
				calculatedHandlingCharge = handlingCostCalculationService.calculateHandlingCharge(shippingTableJsonObject,deviceCount,filterCount);
			} else if((deviceCount >= freightThresholdDevice)) {
				handlingCostCalculationService = new FreightHandlingCostCalculationServiceImpl();
				calculatedHandlingCharge = handlingCostCalculationService.calculateHandlingCharge(shippingTableJsonObject,deviceCount,filterCount);
			}
		} else {
			handlingCostCalculationService = new FreightHandlingCostCalculationServiceImpl();
			calculatedHandlingCharge = handlingCostCalculationService.calculateHandlingCharge(shippingTableJsonObject,deviceCount,filterCount);
		}
		return calculatedHandlingCharge;
	}
	
	/**
	 * doMixedCartOperation method is used check for whether the cart handling cost based on parcel or frieght for Mixed cart items
	 * @param deviceCount
	 * @param filterCount
	 * @param parcelFlag
	 * @param shippingTableJsonObject
	 * @param freightThresholdDevice
	 * @param freightThresholdFilter
	 * @return String
	 */
	private String doMixedCartOperation(int deviceCount, int filterCount, Boolean parcelFlag,
			JsonObject shippingTableJsonObject, int freightThresholdDevice, int freightThresholdFilter) {
		String calculatedHandlingCharge;
		if(parcelFlag && deviceCount < freightThresholdDevice && filterCount < freightThresholdFilter) {
			handlingCostCalculationService = new ParcelHandlingCostCalculationServieImpl();
			calculatedHandlingCharge = handlingCostCalculationService.calculateHandlingCharge(shippingTableJsonObject,deviceCount,filterCount);
		}else{
			handlingCostCalculationService = new FreightHandlingCostCalculationServiceImpl();
			calculatedHandlingCharge = handlingCostCalculationService.calculateHandlingCharge(shippingTableJsonObject,deviceCount,filterCount);
		}
		return calculatedHandlingCharge;
	}
}
