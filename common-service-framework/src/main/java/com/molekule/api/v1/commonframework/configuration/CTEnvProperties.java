package com.molekule.api.v1.commonframework.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * The class CTEnvProperties is used to bind the application properties values
 * related to Commerce Tools.
 */
//@ConfigurationProperties(prefix = "ct")
@Data
@Configuration
public class CTEnvProperties {

	@Value("${ct.host}")
	private String host;
	@Value("${ct.cientId}")
	private String cientId;
	@Value("${ct.clientSecret}")
	private String clientSecret;
	@Value("${ct.projectKey}")
	private String projectKey;
	@Value("${ct.accessTokenUrl}")
	private String accessTokenUrl;
	@Value("${ct.granttype}")
	private String granttype;
	@Value("${ct.basicAuthValue}")
	private String basicAuthValue;
	@Value("${ct.authUrl}")
	private String authUrl;
	@Value("${ct.customAttributeId}")
	private String customAttributeId;
	@Value("${ct.customType}")
	private String customType;

	@Value("${ct.stripeHost}")
	private String stripeHost;
	@Value("${ct.stripeBearerToken}")
	private String stripeBearerToken;

	@Value("${ct.powerReviewHost}")
	private String powerReviewHost;
	@Value("${ct.powerReviewMerchantIdTestUS}")
	private String powerReviewMerchantIdTestUS;
	@Value("${ct.powerReviewApiKey}")
	private String powerReviewApiKey;

	@Value("${ct.sendGridUrl}")
	private String sendGridUrl;
	@Value("${ct.apiKey}")
	private String apiKey;
	@Value("${ct.fromEmail}")
	private String fromEmail;
	@Value("${ct.fromName}")
	private String fromName;
	@Value("${ct.validatEmailUrl}")
	private String validatEmailUrl;
	@Value("${ct.salesRepresentativeFirstName}")
	private String salesRepresentativeFirstName;
	@Value("${ct.salesRepresentativeLastName}")
	private String salesRepresentativeLastName;
	@Value("${ct.salesRepresentativeEmail}")
	private String salesRepresentativeEmail;
	@Value("${ct.salesRepresentativePhoneNumber}")
	private String salesRepresentativePhoneNumber;
	@Value("${ct.salesRepresentativeCalendlyLink}")
	private String salesRepresentativeCalendlyLink;
	@Value("${ct.templateId}")
	private String templateId;
	@Value("${ct.customerGroupId}")
	private String customerGroupId;

	@Value("${ct.shippingTableContainer}")
	private String shippingTableContainer;
	@Value("${ct.shippingTableKey}")
	private String shippingTableKey;
	@Value("${ct.d2cShippingTableKey}")
	private String d2cShippingTableKey;

	@Value("${ct.molekuleAirPro.productKey}")
	private String molekuleAirProProductKey;
	@Value("${ct.molekuleAirPro.sku}")
	private String molekuleAirProSku;

	@Value("${ct.avalara.certcapture.host}")
	private String avalaraHost;
	@Value("${ct.avalara.certcapture.authValue}")
	private String avalaraAuthValue;
	@Value("${ct.avalara.certcapture.xClientId}")
	private String avalaraXclientId;

	@Value("${ct.order.type.id}")
	private String orderTypeId;

	@Value("${ct.fedExApiUrl}")
	private String fedExApiUrl;
	@Value("${ct.fedExUser}")
	private String fedExUser;
	@Value("${ct.fedExPassword}")
	private String fedExPassword;
	@Value("${ct.fedExClientAccountNumber}")
	private String fedExClientAccountNumber;
	@Value("${ct.fedExClientMeterNumber}")
	private String fedExClientMeterNumber;

	@Value("${ct.taxCatgeoryId}")
	private String taxCatgeoryId;

	@Value("${ct.orderSubmittedId}")
	private String orderSubmittedId;
	@Value("${ct.lineItemTypeId}")
	private String lineItemTypeId;
	@Value("${ct.pw0rd}")
	private String pw0rd;

	@Value("${ct.guestCustomerGroupId}")
	private String guestCustomerGroupId;

	@Value("${usStoreId}")
	private String usStoreId;

	@Value("${caStoreId}")
	private String caStoreId;

	@Value("${ct.usOrdersTableName}")
	private String uSOrdersTableName;

	@Value("${ct.caOrdersTableName}")
	private String cAOrdersTableName;

	@Value("${ct.usOrdersStartingIndex}")
	private Long uSOrdersStartingIndex;

	@Value("${ct.caOrdersStartingIndex}")
	private Long cAOrdersStartingIndex;

	@Value("${ct.analyticsSubscriptionArn}")
	private String analyticsSubscriptionArn;

	@Value("${ct.analyticsAchPaidArn}")
	private String analyticsAchPaidArn;

	@Value("${ct.analyticsOrderArn}")
	private String analyticsOrderArn;

	@Value("${ct.paymentTypeId}")
	private String paymentTypeId;

	@Value("${ukStoreId}")
	private String ukStoreId;
	@Value("${ct.cognitoDomain}")
	private String cognitoDomain;
	@Value("${ct.cognitoClientId}")
	private String cognitoClientId;
	@Value("${ct.cognitoClientSecret}")
	private String cognitoClientSecret;
	@Value("${ct.cognitoUserPool}")
	private String cognitoUserPool;
	@Value("${ct.cognitoHeaderContentType}")
	private String cognitoHeaderContentType;
	@Value("${ct.cognitoHeaderAmzTarget}")
	private String cognitoHeaderAmzTarget;
	@Value("${ct.cognitoHeaderAmzInitiateAuth}")
	private String cognitoHeaderAmzInitiateAuth;
	@Value("${ct.cognitoHeaderAmzRespondToAuthChallenge}")
	private String cognitoHeaderAmzRespondToAuthChallenge;
	@Value("${ct.cognitoHeaderAmzGlobalSignOut}")
	private String cognitoHeaderAmzGlobalSignOut;
	@Value("${ct.prismicAccessToken}")
	private String prismicAccessToken;
	@Value("${ct.prismicRefUrl}")
	private String prismicRefUrl;
	@Value("${ct.prismicImageSearchUrl}")
	private String prismicImageSearchUrl;
	@Value("${ct.prismicApiUrl}")
	private String prismicApiUrl;
	@Value("${ct.prismicApiB2BUrl}")
	private String prismicApiB2BUrl;
	@Value("${ct.prismicApiD2CUrl}")
	private String prismicApiD2CUrl;

	@Value("${ct.affirmPublicKey}")
	private String affirmPublicKey;

	@Value("${ct.affirmPrivateKey}")
	private String affirmPrivateKey;

	@Value("${ct.affirmServiceBaseUrl}")
	private String affirmServiceBaseUrl;

	@Value("${ct.affirmChargesUrl}")
	private String affirmChargesUrl;

	@Value("${ct.affirmCaptureUrl}")
	private String affirmCaptureUrl;

	@Value("${ct.checkoutSurveyTableKey}")
	private String checkoutSurveyKey;

	@Value("${ct.checkoutSurveyTableContainer}")
	private String checkoutSurveyContainer;
	@Value("${ct.cognitoHeaderAmzGetUserTarget}")
	private String cognitoHeaderAmzGetUserTarget;

	@Value("${ct.cognitoHeaderAmzForgotPasswordTarget}")
	private String cognitoHeaderAmzForgotPasswordTarget;

	@Value("${ct.cognitoHeaderAmzConfirmForgotPasswordTarget}")
	private String cognitoHeaderAmzConfirmForgotPasswordTarget;

	@Value("${ct.cognitoHeaderAmzChangePasswordTarget}")
	private String cognitoHeaderAmzChangePasswordTarget;

	@Value("${ct.cognitoHeaderAmzDeleteUser}")
	private String cognitoHeaderAmzDeleteUser;

	@Value("${ct.httpclientEnableFlag}")
	private boolean httpclientEnableFlag;

	@Value("${ct.httpclientCount}")
	private Integer httpclientCount;

	@Value("${ct.httpClientLifeTime}")
	private Integer httpClientLifeTime;

	@Value("${ct.httpClientIdleTime}")
	private Integer httpClientIdleTime;

	@Value("${ct.httpClientconnectionTimeout}")
	private Integer httpClientconnectionTimeout;

	@Value("${ct.httpClientResponseTimeout}")
	private Integer httpClientResponseTimeout;

	@Value("${ct.bundleProductTypeName}")
	private String bundleProductTypeName;

	@Value("${ct.subscriptionProductTypeId}")
	private String subscriptionProductTypeId;
	@Value("${ct.stripeDomainHost}")
	private String stripeDomainHost;

	@Value("${ct.addressType}")
	private String addressType;

	@Value("${ct.netsuiteOrderStatusTableName}")
	private String netsuiteOrderStatusTableName;
	@Value("${userAccountHost}")
	private String userAccountUrl;

	@Value("${ct.cancelStateId}")
	private String cancelStateId;

	@Value("${ct.replacementPartsCategoryId}")
	private String replacementPartsCategoryId;
	@Value("${ct.productServiceHost}")
	private String productServiceHost;
	
    @Value("${ct.oAuthConsumerKey}")
	private String oAuthConsumerKey;
	@Value("${ct.oAuthToken}")
	private String oAuthToken;
	@Value("${ct.oAuthVersion}")
	private String oAuthVersion;
	@Value("${ct.oAuthBaseUrl}")
	private String oAuthBaseUrl;
	@Value("${ct.oAuthSignatureMethod}")
	private String oAuthSignatureMethod;
	@Value("${ct.oAuthSerialNumberValidationDeploymentId}")
	private String oAuthSerialNumberValidationDeploymentId;
	@Value("${ct.oAuthSerialNumberValidationScriptId}")
	private String oAuthSerialNumberValidationScriptId;
	@Value("${ct.oAuthConsumerSecret}")
	private String oAuthConsumerSecret;
	@Value("${ct.oAuthTokenSecret}")
	private String oAuthTokenSecret;
	@Value("${ct.oAuthRealm}")
	private String oAuthRealm;
	
	@Value("${ct.oAuthCancellationApiValidationDeploymentId}")
	private String oAuthCancellationApiValidationDeploymentId;
	@Value("${ct.oAuthCancellationApiValidationScriptId}")
	private String oAuthCancellationApiValidationScriptId;
	
//	@Value("${ct.currency}")
//	private String currency;
	@Value("${ct.environment}")
	private String environment;
	@Value("${ct.awsSQSLambdaUrl}")
	private String awsSQSLambdaUrl;

	@Value("${language}")
	private String language;
//    @Value("${countryCode}")
//	private String countryCode;
    @Value("${euStoreId}")
    private String euStoreId;
    
    @Value("${ct.trackingNumberTableName}")
    private String trackingNumberTableName;
    @Value("${ct.trackingNumberStartingIndex}")
    private String trackingNumberStartingIndex;
     @Value("${taxMode}")
    private String taxMode;
     
    @Value("${ct.distributionChannel.b2c.id}")
    private String distributionChannelB2CId;
    
    @Value("${ct.distributionChannel.b2b.id}")
    private String distributionChannelB2BId;
    
    @Value("${adyenHost}")
    private String adyenHost;
    @Value("${xAPIKey}")
    private String xAPIKey;
    @Value("${adyenVersion}")
    private String adyenVersion;
    @Value("${adyenPaymentType}")
    private String adyenPaymentType;
    @Value("${merchantAccount}")
    private String merchantAccount;
    @Value("${adyenReccuringHost}")
    private String adyenReccuringHost;
    @Value("${adyenReccuringContract}")
    private String adyenReccuringContract;
    @Value("${adyenShopperInteraction}")
    private String adyenShopperInteraction;
    
	public void setAuthUrl(String authUrl) {
		this.authUrl = authUrl;
	}

	public void setHost(String host) {
		this.host = host;
	}


	public void setCientId(String cientId) {
		this.cientId = cientId;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}


	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}


	public void setAccessTokenUrl(String accessTokenUrl) {
		this.accessTokenUrl = accessTokenUrl;
	}


	public void setGranttype(String granttype) {
		this.granttype = granttype;
	}


	public void setBasicAuthValue(String basicAuthValue) {
		this.basicAuthValue = basicAuthValue;
	}


	public void setCustomAttributeId(String customAttributeId) {
		this.customAttributeId = customAttributeId;
	}


	public void setStripeHost(String stripeHost) {
		this.stripeHost = stripeHost;
	}


	public void setStripeBearerToken(String stripeBearerToken) {
		this.stripeBearerToken = stripeBearerToken;
	}


	public void setPowerReviewHost(String powerReviewHost) {
		this.powerReviewHost = powerReviewHost;
	}


	public void setPowerReviewMerchantIdTestUS(String powerReviewMerchantIdTestUS) {
		this.powerReviewMerchantIdTestUS = powerReviewMerchantIdTestUS;
	}


	public void setPowerReviewApiKey(String powerReviewApiKey) {
		this.powerReviewApiKey = powerReviewApiKey;
	}


	public void setSendGridUrl(String sendGridUrl) {
		this.sendGridUrl = sendGridUrl;
	}


	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}


	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}


	public void setFromName(String fromName) {
		this.fromName = fromName;
	}


	public void setValidatEmailUrl(String validatEmailUrl) {
		this.validatEmailUrl = validatEmailUrl;
	}


	public void setSalesRepresentativeFirstName(String salesRepresentativeFirstName) {
		this.salesRepresentativeFirstName = salesRepresentativeFirstName;
	}


	public void setSalesRepresentativeLastName(String salesRepresentativeLastName) {
		this.salesRepresentativeLastName = salesRepresentativeLastName;
	}


	public void setSalesRepresentativeEmail(String salesRepresentativeEmail) {
		this.salesRepresentativeEmail = salesRepresentativeEmail;
	}


	public void setSalesRepresentativePhoneNumber(String salesRepresentativePhoneNumber) {
		this.salesRepresentativePhoneNumber = salesRepresentativePhoneNumber;
	}


	public void setSalesRepresentativeCalendlyLink(String salesRepresentativeCalendlyLink) {
		this.salesRepresentativeCalendlyLink = salesRepresentativeCalendlyLink;
	}


	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}


	public void setCustomerGroupId(String customerGroupId) {
		this.customerGroupId = customerGroupId;
	}

	public void setFedExApiUrl(String fedExApiUrl) {
		this.fedExApiUrl = fedExApiUrl;
	}

	public void setFedExUser(String fedExUser) {
		this.fedExUser = fedExUser;
	}

	public void setFedExPassword(String fedExPassword) {
		this.fedExPassword = fedExPassword;
	}

	public void setFedExClientAccountNumber(String fedExClientAccountNumber) {
		this.fedExClientAccountNumber = fedExClientAccountNumber;
	}

	public void setFedExClientMeterNumber(String fedExClientMeterNumber) {
		this.fedExClientMeterNumber = fedExClientMeterNumber;
	}

	public void setTaxCatgeoryId(String taxCatgeoryId) {
		this.taxCatgeoryId = taxCatgeoryId;
	}

	public void setOrderSubmittedId(String orderSubmittedId) {
		this.orderSubmittedId = orderSubmittedId;
	}

	public void setLineItemTypeId(String lineItemTypeId) {
		this.lineItemTypeId = lineItemTypeId;
	}

	public void setUsStoreId(String usStoreId) {
		this.usStoreId = usStoreId;
	}
}
