package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class PaymentIntentRequest {

	private String amount;
	private String setUpPaymentIntentId;
	private boolean setupIntentOnly;
	private String cartId;
	private String currency;

}
