package com.molekule.api.v1.commonframework.dto.registration;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
/**
 * The class Fields is used to hold the data for custom fields.
 */
@Data
public class Fields {

	private boolean admin;
	private String salutation;
	private String accountId;
	private List<String> invoiceEmailAddress;
	private String phone;
	private String customerGroupName;
	@JsonProperty("accountLocked")
	private boolean accountLocked;
	@JsonProperty(CHANNEL)
	private String channel;
	@JsonProperty("store")
	private String store;
	/**
//	@JsonProperty("comments")
//	private String comments;
//	@JsonProperty("registeredChannel")
//	private String registeredChannel;
	 */
	private boolean termsOfService;
	private boolean marketingOffers;

}
