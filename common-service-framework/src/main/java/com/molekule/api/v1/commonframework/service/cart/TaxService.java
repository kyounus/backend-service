package com.molekule.api.v1.commonframework.service.cart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.molekule.api.v1.commonframework.model.cart.AvalaraRequestBean;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
/*
 * Tax Service class will connect to Avalara for tax calculation.
 * 
 * 
 */
@Service("taxService")
public class TaxService {

	@Value("${apiKey}")
	private String avalaraApiKey;
	@Value("${targetUrl}")
	private String targetUrl;
	@Autowired
	@Qualifier("netConnectionHelper")
	private NetConnectionHelper netConnectionHelper;

	public String calculateTaxByAvalara(AvalaraRequestBean avalaraRequestBean) {
		String token = new StringBuilder().append("Basic ").append(avalaraApiKey).toString();
		return (String) netConnectionHelper.sendPostRequest(targetUrl, token, avalaraRequestBean);
	}
}
