package com.molekule.api.v1.commonframework.model.checkout;

import java.util.List;

import lombok.Data;

@Data
public class Payment {

	private String interfaceId;
	private AmountPlanned amountPlanned;
	private PaymentMethodInfo paymentMethodInfo;
	private List<Transaction> transactions;
	private Custom custom;
	

	
	@Data
	public static class Custom {
		private Type type;
		private Fields fields;
		
		@Data
		public static class Type {
			private String id;
			private String typeId;
		}

		@Data
		public static class Fields {
			private String paymentId;
			private String expMonth;
			private String expYear;
			private String last4;
			private String brand;
			private String charge;
			private String streetCheck;
			private String zipCheck;
			private String cvcCheck;
			private Boolean captured;
			private Boolean refunded;
			private String radarRisk;
			private String paymentView;
			private String customerView;
		}
	}

	@Data
	public static class Transaction {
		private String timestamp;
		private String type;
		private Amount amount;
		private String state;

		@Data
		public static class Amount {
			private String currencyCode;
			private long centAmount;

		}

	}

	@Data
	public static class AmountPlanned {
		private String currencyCode;
		private long centAmount;

	}

	@Data
	public static class PaymentMethodInfo {
		private String paymentInterface;
		private String method;
		private Name name;

		@Data
		public static class Name {
			private String en;
		}
	}
}
