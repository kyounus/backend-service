package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMAIL;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CheckoutSurveyResponse {
	
	private int limit;
	private int offset;
	private int count;
	private int total;
	private List<CheckoutSurvey> checkoutSurveyList;

	@Data
	public static class CheckoutSurvey {
		@JsonProperty("id")
		private String id;

		@JsonProperty("customerId")
		private String customerId;
		
		@JsonProperty("customerNumber")
		private String customerNumber;

		@JsonProperty(EMAIL)
		private String email;
		
		@JsonProperty("surveyResponse")
	    private String surveyResponse;
	   
	    @JsonProperty("other")
	    private boolean other;

	    @JsonProperty("createdDate")
	    private String createdDate;

	    @JsonProperty("lastModifiedDate")
	    private String lastModifiedDate;

		
	}
	
}
