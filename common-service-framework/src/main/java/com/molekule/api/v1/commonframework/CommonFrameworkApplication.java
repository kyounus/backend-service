package com.molekule.api.v1.commonframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommonFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommonFrameworkApplication.class, args);
	}

}
