package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;
@Data
public class SubscriptionObject{
    private SubscriptionCustomer subscriptionCustomer;
}
