package com.molekule.api.v1.commonframework.model.customerprofile;

import java.util.List;

import lombok.Data;

@Data
public class BulkUpdateRequestBean {

	private int limit;
	private int offset;
	private String nextOrderDate;
	private String nextOrderRenewalDate;
	private List<String> subscriptionIdList;
	private String surveyQuestionId;
	private String other;
}
