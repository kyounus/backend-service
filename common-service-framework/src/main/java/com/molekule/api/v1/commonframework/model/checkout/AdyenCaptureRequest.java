package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;

@Data
public class AdyenCaptureRequest {

	private String reference;
	private String merchantAccount;
	private Amount amount;
	@Data
	public static class Amount{
		private String currency;
		private Long value;
	}
}

