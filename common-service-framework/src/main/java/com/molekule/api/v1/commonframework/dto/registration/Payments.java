package com.molekule.api.v1.commonframework.dto.registration;

import com.molekule.api.v1.commonframework.model.registration.Business;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;

import lombok.Data;

/*
 * Payments class will hold Payments information in array.
 * 
 */
@Data
public class Payments {
	
	public enum TermsStatus{
		NOTAPPLIED, PENDINGREVIEW, APPROVED
	}

	private String paymentId;
	private String paymentType;
	private String paymentToken;
	private String customerId;
	private String billingAddressId;
	private Business business;
	private CreditCard cardObject;
	
	private Boolean transactionNeedApproval;
	private String totalApprovedCredit;
	private String totalUsedCredit;
	private String netTerms;
	private String termsApprovalDate;
	private TermsStatus termsStatus;
}
