package com.molekule.api.v1.commonframework.service.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionCustomObject;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionCustomObject.Value.TransactionObject;
import com.molekule.api.v1.commonframework.dto.customerprofile.DeviceCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.CustomerAction;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.PaymentCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.UpdateCustomerServiceBean;
import com.molekule.api.v1.commonframework.dto.registration.UpdateCustomerServiceBean.Action;
import com.molekule.api.v1.commonframework.mail.sendgrid.TemplateMailRequestHelper;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.AdyenRecurringDetailsRequest;
import com.molekule.api.v1.commonframework.model.customerprofile.BulkUpdateRequestBean;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSurveyResponse;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSurveyResponse.SurveyQuestion;
import com.molekule.api.v1.commonframework.model.customerprofile.DeviceRequestBean;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;
import com.molekule.api.v1.commonframework.model.registration.CustomerProfileDetails;
import com.molekule.api.v1.commonframework.model.registration.CustomerRequestBean;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.tealium.TealiumHelperService;
import com.molekule.api.v1.commonframework.util.CustomerChannelEnum;
import com.molekule.api.v1.commonframework.util.CustomerServiceUtility;
import com.molekule.api.v1.commonframework.util.ErrorResponse;
import com.molekule.api.v1.commonframework.util.LifeTimeSalesEnum;
import com.molekule.api.v1.commonframework.util.MolekuleConstant;
import com.molekule.api.v1.commonframework.util.MolekuleUtility;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;


@Service("customerProfileHelperService")
public class CustomerProfileHelperService {

	Logger logger = LoggerFactory.getLogger(CustomerProfileHelperService.class);

	private static final SecureRandom RANDOM = new SecureRandom();

	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	@Autowired
	@Qualifier("checkoutHelperService")
	CheckoutHelperService checkoutHelperService;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;

	@Autowired
	@Qualifier("customerEmailUpdateMailSender")
	TemplateMailRequestHelper customerEmailUpdateMailSender;

	@Autowired
	@Qualifier("registrationHelperService")
	RegistrationHelperService registrationHelperService;

	@Autowired
	@Qualifier("tealiumHelperService")
	TealiumHelperService tealiumHelperService;

	public String updateCustomerById(String customerId, CustomerRequestBean customerRequestBean,String country,String ctToken,String shopperReference) {
		String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_CUSTOMERS_SLASH + customerId;
		String createCustomObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ "/custom-objects";
		String customerResponse = netConnectionHelper.sendGetWithoutBody(ctToken, customerByIdUrl);
		if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.ADD_SHIPPING_ADDRESS.name())) {
			customerResponse = addShippingAddress(customerResponse, customerRequestBean, customerByIdUrl, ctToken);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.UPDATE_SHIPPING_ADDRESS.name())) {
			customerResponse = changeShippingAddress(customerResponse, customerRequestBean, customerByIdUrl, ctToken);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.REMOVE_SHIPPING_ADDRESS.name())) {
			customerResponse = removeShippingAddress(customerResponse, customerRequestBean, customerByIdUrl, ctToken);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.ADD_PAYMENT.name())) {
			return addPayment(customerResponse, customerRequestBean, createCustomObjectUrl, ctToken, customerId,country,shopperReference);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.UPDATE_PAYMENT.name())) {
			return changePayment(customerResponse, customerRequestBean, createCustomObjectUrl, ctToken, customerId,country);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.REMOVE_PAYMENT.name())) {
			return removePayment(customerResponse, customerRequestBean, createCustomObjectUrl, ctToken, customerId);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.UPDATE_EMAIL.name())) {
			return updateCustomerEmail(customerRequestBean, customerResponse, customerByIdUrl, ctToken, customerId);
		} else if (customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.UPDATE_PROFILE.name())) {
			return updateCustomerProfile(customerRequestBean, customerResponse, customerByIdUrl, ctToken);
		}else if(customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.UPDATE_COMMENTS.name())) {
			return updateComments(customerRequestBean, customerResponse, customerByIdUrl, ctToken);
		}else if(customerRequestBean.getAction().equals(CustomerServiceUtility.CustomerActions.UPDATE_MARKETING_OFFERS.name())) {
			return updateProductNewsExclusiveOffers(customerRequestBean, customerResponse, customerByIdUrl, ctToken);
		}
		return customerResponse;
	}

	protected String updateProductNewsExclusiveOffers(CustomerRequestBean customerRequestBean, String customerResponse,
			String customerByIdUrl, String token) {
		if(customerRequestBean == null) {
			return new ErrorResponse(400, "Invalid input").toString();
		}
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();
		boolean marketingOffers = customerRequestBean.isMarketingOffers();
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName("marketingOffers");
		action.setValue(marketingOffers);
		actionsList.add(action);
		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);
		return (String) netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private String updateComments(CustomerRequestBean customerRequestBean, String customerResponse,
			String customerByIdUrl, String token) {
		if(customerRequestBean == null || customerRequestBean.getComments() == null) {
			return new ErrorResponse(400, "comments field required to update customer details").toString();
		}
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();

		String comments =  customerRequestBean.getComments();
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName("comments");
		action.setValue(comments);
		actionsList.add(action);
		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);

		return (String) netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private String updateCustomerProfile(CustomerRequestBean customerRequestBean, String customerResponse, String customerByIdUrl, String token) {

		if(customerRequestBean == null || customerRequestBean.getCustomerProfileDetails() == null) {
			return new ErrorResponse(400, "customerProfileDetails required for update the profile").toString();
		}

		CustomerProfileDetails customerProfileDetails = customerRequestBean.getCustomerProfileDetails();

		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();

		String firstName = StringUtils.hasText(customerProfileDetails.getFirstName()) ? customerProfileDetails.getFirstName().toUpperCase() : null;
		String lastName =  StringUtils.hasText(customerProfileDetails.getLastName()) ? customerProfileDetails.getLastName().toUpperCase() : null;

		if (StringUtils.hasText(firstName)) {
			UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
			action.setActionName("setFirstName");
			action.setFirstName(firstName);
			actionsList.add(action);
		}

		if (StringUtils.hasText(lastName)) {
			UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
			action.setActionName("setLastName");
			action.setLastName(lastName);
			actionsList.add(action);
		}

		if (StringUtils.hasText(customerProfileDetails.getRegisteredChannel())) {
			actionsList.add(updateCustomerCusotmFields("registeredChannel", customerProfileDetails.getRegisteredChannel()));
		}
		
		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);

		return (String) netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);

	}

	private Action updateCustomerCusotmFields(String name, Object value) {
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName(SET_CUSTOM_FIELD);
		action.setName(name);
		action.setValue(value);
		return action;
	}

	private String updateCustomerEmail(CustomerRequestBean customerRequestBean, String customerResponse, String customerByIdUrl, String token, String customerId) {
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		SendGridModel sendgridModel = new SendGridModel();
		sendgridModel.setEmail(customerObject.get("email").getAsString());
		sendgridModel.setFirstName(customerObject.get("firstName").getAsString());
		String emailId = StringUtils.hasText(customerRequestBean.getEmail()) ? customerRequestBean.getEmail().toLowerCase() : null;
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName("changeEmail");
		action.setEmail(emailId);
		actionsList.add(action);
		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);
		customerResponse = (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
		customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		if(customerObject.has(STATUS_CODE)) {
			return customerResponse;
		}
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setCountryCode(customerRequestBean.getCountryCode());
		customerDTO.setEmail(emailId);
		customerDTO.setFirstName(customerObject.get(FIRST_NAME).getAsString());
		customerDTO.setLastName(customerObject.get(LAST_NAME).getAsString());
		String cognitoStringResponse = registrationHelperService.setCognitoResponseDetails(customerDTO, D2C, 
				customerRequestBean.getPassword(), null, token, customerId, customerObject.get(VERSION).getAsLong());
		if (cognitoStringResponse != null) {
			JsonObject cognitoJsonObject = MolekuleUtility.parseJsonObject(cognitoStringResponse);
			if (!cognitoJsonObject.has(__TYPE) && !cognitoJsonObject.has(STATUS_CODE)) {
				registrationHelperService.deleteCognitoUser(customerRequestBean.getAccessToken());
			}
			if(cognitoJsonObject.has("id")){
				//update active cart email
				String cartUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART
						+ "?where=customerId=\"" + customerId + "\""
						+ "&where=custom(fields(parentOrderReferenceId is not defined))"
						+ "&where=custom(fields(Quote is not defined))" + "&where=cartState=\"Active\"";
				JsonObject cartObject = MolekuleUtility.parseJsonObject(netConnectionHelper.sendGetWithoutBody(token, cartUrl));
				if(cartObject.get("results").getAsJsonArray().size() > 0) {
					JsonObject resultObject = cartObject.get("results").getAsJsonArray().get(0).getAsJsonObject();
					String cartId = resultObject.get("id").getAsString();
					String updateCartUrl = 	ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CART + cartId;
					UpdateCustomerServiceBean updateCartActionBean = new UpdateCustomerServiceBean();
					List<UpdateCustomerServiceBean.Action> updateCartActionsList = new ArrayList<>();
					UpdateCustomerServiceBean.Action updateCartAction = new UpdateCustomerServiceBean.Action();
					updateCartAction.setActionName("setCustomerEmail");
					updateCartAction.setEmail(emailId);
					updateCartActionsList.add(updateCartAction);
					updateCartActionBean.setVersion(resultObject.get(VERSION).getAsLong());
					updateCartActionBean.setActions(updateCartActionsList);
					netConnectionHelper.sendPostRequest(updateCartUrl, token, updateCartActionBean);
				}
			}
		}
		sendgridModel.setUpdatedEmail(emailId);
		customerEmailUpdateMailSender.sendMail(sendgridModel);

		return cognitoStringResponse;
	}

	private String addShippingAddress(String customerResponse, CustomerRequestBean customerRequestBean, String customerByIdUrl, String token) {
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		AddressRequestBean requestShippingAddress = customerRequestBean.getShippingAddress();
		//		first add address then only add custom fields in address. 
		//		If first add custom field in address and then address then we will not have custom fields.
		customerResponse = addCustomerAddress(requestShippingAddress, customerByIdUrl, token, customerObject);
		customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		if(customerObject.has(STATUS_CODE)) {
			return customerResponse;
		}
		JsonArray addressesJsonArray = customerObject.get("addresses").getAsJsonArray();
		Integer addressesJsonArraySize = addressesJsonArray.size();
		JsonObject addressJsonObject = addressesJsonArray.get(addressesJsonArraySize-1).getAsJsonObject();
		String shippingAddressId = addressJsonObject.get("id").getAsString();
		customerResponse = setShippigAddressIds(customerRequestBean, customerByIdUrl, token, customerObject, shippingAddressId);
		customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		if(customerObject.has(STATUS_CODE)) {
			return customerResponse;
		}
		if(Boolean.TRUE.equals(requestShippingAddress.getIsBusiness())) {
			customerResponse = setCustomFieldInAddress(requestShippingAddress, customerByIdUrl, token, customerObject, shippingAddressId);
			customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(customerObject.has(STATUS_CODE)) {
				return customerResponse;
			}
		}
		return customerResponse;
	}

	private String changeShippingAddress(String customerResponse, CustomerRequestBean customerRequestBean,
			String customerByIdUrl, String token) {
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		AddressRequestBean requestShippingAddress = customerRequestBean.getShippingAddress();
		customerResponse = updateCustomerAddress(requestShippingAddress, customerByIdUrl, token, customerObject);
		customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		if(customerObject.has(STATUS_CODE)) {
			return customerResponse;
		}
		return customerResponse;
	}

	private String removeShippingAddress(String customerResponse, CustomerRequestBean customerRequestBean,
			String customerByIdUrl, String token) {
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();
		actionsList.add(setCustomerUpdateActions("removeAddress", customerRequestBean.getShippingAddress().getId()));
		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);
		return (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private String addPayment(String customerResponse, CustomerRequestBean customerRequestBean, String createCustomObjectUrl,
			String token, String customerId,String country,String shopperReference) {
		String paymentCustomObjectUrl = createCustomObjectUrl + D2C_PAYMENT +customerId;
		PaymentCustomObject paymentCustomObjectResponse = netConnectionHelper.getPaymentCustomObject(token, paymentCustomObjectUrl);
		String paymentToken = customerRequestBean.getPaymentToken();
		if(null == paymentCustomObjectResponse.getValue()) {
			paymentCustomObjectResponse = new PaymentCustomObject();
			PaymentCustomObject.Value value = new PaymentCustomObject.Value();
			List<Payments> paymentsList = new ArrayList<>();
			Payments payments = new Payments();
			String paymentId = UUID.randomUUID().toString();
			payments.setPaymentId(paymentId);
			payments.setPaymentType(customerRequestBean.getPaymentType());
			payments.setPaymentToken(paymentToken);
			if(null != customerRequestBean.getBillingAddress()) {
				customerResponse = setBillingAddress(customerResponse, customerRequestBean, payments, token, customerId);				
			}
			if("US".equals(country)|| "CA".equals(country)) {
				payments.setCardObject(registrationHelperService.getPaymentCardDetails(paymentToken));
			}else {
				payments.setCardObject(getPaymentCardDetails(paymentToken,shopperReference));
			}
			
			paymentsList.add(payments);
			value.setPayments(paymentsList);
			if(Boolean.TRUE.equals(customerRequestBean.getIsDefaultPayment())) {
				value.setPreferredPaymentId(paymentId);
			}
			paymentCustomObjectResponse.setContainer("d2c-payment");
			paymentCustomObjectResponse.setKey(customerId);
			paymentCustomObjectResponse.setValue(value);
			paymentCustomObjectResponse.setVersion(0);
		} else {
			List<Payments> paymentsList = paymentCustomObjectResponse.getValue().getPayments();
			Payments payments = new Payments();
			String paymentId = UUID.randomUUID().toString();
			payments.setPaymentId(paymentId);
			payments.setPaymentType(customerRequestBean.getPaymentType());
			payments.setPaymentToken(paymentToken);
			if(null != customerRequestBean.getBillingAddress()) {
				customerResponse = setBillingAddress(customerResponse, customerRequestBean, payments, token, customerId);			
			}
			if("US".equals(country)|| "CA".equals(country)) {
				payments.setCardObject(registrationHelperService.getPaymentCardDetails(paymentToken));
			}else {
				payments.setCardObject(getPaymentCardDetails(paymentToken,shopperReference));
			}
			paymentsList.add(payments);
			paymentCustomObjectResponse.getValue().setPayments(paymentsList);
			if(Boolean.TRUE.equals(customerRequestBean.getIsDefaultPayment())) {
				paymentCustomObjectResponse.getValue().setPreferredPaymentId(paymentId);
			}
		}
		return (String)netConnectionHelper.sendPostRequest(createCustomObjectUrl, token, paymentCustomObjectResponse);
	}
	
	public CreditCard getPaymentCardDetails(String paymentToken,String shopperReference) {
	    String adyenReccuringHost = ctEnvProperties.getAdyenReccuringHost()+"/listRecurringDetails";
	    AdyenRecurringDetailsRequest adyenRecurringDetailsRequest = preparePaymentDetailRequest(shopperReference);
	    String reccuringDetailsResponse  =(String) netConnectionHelper.sendPostWithXApiKey(adyenReccuringHost,ctEnvProperties.getXAPIKey(), adyenRecurringDetailsRequest);
		CreditCard creditCard = prepareCreditCard(reccuringDetailsResponse,paymentToken);
	    return creditCard;
	}

	private AdyenRecurringDetailsRequest preparePaymentDetailRequest(String shopperReference) {
	    AdyenRecurringDetailsRequest adyenRecurringDetailsRequest = new AdyenRecurringDetailsRequest();
	    AdyenRecurringDetailsRequest.Recurring recurring = new AdyenRecurringDetailsRequest.Recurring();
	    recurring.setContract(ctEnvProperties.getAdyenReccuringContract());
	    adyenRecurringDetailsRequest.setRecurring(recurring);
	    adyenRecurringDetailsRequest.setShopperReference(shopperReference);
	    adyenRecurringDetailsRequest.setMerchantAccount(ctEnvProperties.getMerchantAccount());
		return adyenRecurringDetailsRequest;
	}
	private CreditCard prepareCreditCard(String reccuringDetailsResponse,String paymentToken) {
		JsonObject reccuringDetailsJson = MolekuleUtility.parseJsonObject(reccuringDetailsResponse);
		JsonArray jsonArray = reccuringDetailsJson.get("details").getAsJsonArray();
		CreditCard creditCard = new CreditCard();
		if(jsonArray.size() >0) {
			for(int i=0;i<jsonArray.size();i++) {
				JsonObject recurringDetailsJson = jsonArray.get(i).getAsJsonObject().get("RecurringDetail").getAsJsonObject();
				 if(paymentToken.equals(recurringDetailsJson.get("recurringDetailReference").getAsString())) {
					 creditCard.setLast4(recurringDetailsJson.get("card").getAsJsonObject().get("number").getAsString());
					 creditCard.setExpMonth(recurringDetailsJson.get("card").getAsJsonObject().get("expiryMonth").getAsString());
					 creditCard.setExpYear(recurringDetailsJson.get("card").getAsJsonObject().get("expiryYear").getAsString());
					 creditCard.setBrand(recurringDetailsJson.get("paymentMethodVariant").getAsString());
				 }
				

			}
			
		}
		return creditCard;
	}

	private String changePayment(String customerResponse, CustomerRequestBean customerRequestBean,
			String createCustomObjectUrl, String token, String customerId,String country) {
		String paymentCustomObjectUrl = createCustomObjectUrl + D2C_PAYMENT + customerId;
		PaymentCustomObject paymentCustomObjectResponse = netConnectionHelper.getPaymentCustomObject(token,
				paymentCustomObjectUrl);
		String paymentToken = customerRequestBean.getPaymentToken();

		List<Payments> paymentsList = paymentCustomObjectResponse.getValue().getPayments();
		paymentsList.stream().filter(payments -> payments.getPaymentId().equals(customerRequestBean.getPaymentId())).forEach(payments -> {
			payments.setPaymentType(customerRequestBean.getPaymentType());
			payments.setPaymentToken(paymentToken);
			setBillingAddress(customerResponse, customerRequestBean, payments, token, customerId);
			if("US".equals(country)|| "CA".equals(country)) {
				payments.setCardObject(registrationHelperService.getPaymentCardDetails(paymentToken));
			}else {
				payments.setCardObject(getPaymentCardDetails(paymentToken,customerId));
			}
		});
		paymentCustomObjectResponse.getValue().setPayments(paymentsList);
		return (String) netConnectionHelper.sendPostRequest(createCustomObjectUrl, token, paymentCustomObjectResponse);
	}

	private String removePayment(String customerResponse, CustomerRequestBean customerRequestBean,
			String createCustomObjectUrl, String token, String customerId) {
		String paymentCustomObjectUrl = createCustomObjectUrl + D2C_PAYMENT + customerId;
		PaymentCustomObject paymentCustomObjectResponse = netConnectionHelper.getPaymentCustomObject(token,
				paymentCustomObjectUrl);
		List<Payments> paymentsList = paymentCustomObjectResponse.getValue().getPayments();
		paymentsList = paymentsList.stream().filter(payments -> !payments.getPaymentId().equals(customerRequestBean.getPaymentId())).collect(Collectors.toList());
		paymentCustomObjectResponse.getValue().setPayments(paymentsList);
		return (String) netConnectionHelper.sendPostRequest(createCustomObjectUrl, token, paymentCustomObjectResponse);
	}

	private String setBillingAddress(String customerResponse, CustomerRequestBean customerRequestBean, Payments payments, String token, String customerId) {
		String customerByIdUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
		+ SLASH_CUSTOMERS_SLASH + customerId;
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		String billingAddressId;
		if(null != customerRequestBean.getBillingAddress().getId()) {
			billingAddressId = customerRequestBean.getBillingAddress().getId();
			payments.setBillingAddressId(billingAddressId);
			return updateCustomerAddress(customerRequestBean.getBillingAddress(), customerByIdUrl,
					token, customerObject);
		} else {
			customerResponse = addCustomerAddress(customerRequestBean.getBillingAddress(), customerByIdUrl, token, customerObject);
			customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(customerObject.has(STATUS_CODE)) {
				return customerResponse;
			}
			JsonArray addressesJsonArray = customerObject.get("addresses").getAsJsonArray();
			Integer addressesJsonArraySize = addressesJsonArray.size();
			JsonObject addressJsonObject = addressesJsonArray.get(addressesJsonArraySize-1).getAsJsonObject();
			billingAddressId = addressJsonObject.get("id").getAsString();
			payments.setBillingAddressId(billingAddressId);
			customerResponse = setBillingAddressIds(customerRequestBean, customerByIdUrl, token, customerObject, billingAddressId);
			customerObject = MolekuleUtility.parseJsonObject(customerResponse);
			if(customerObject.has(STATUS_CODE)) {
				return customerResponse;
			}
		}
		return customerResponse;
	}

	private String setBillingAddressIds(CustomerRequestBean customerRequestBean, String customerByIdUrl, String token,
			JsonObject customerObject, String billingAddressId) {
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();

		actionsList.add(setCustomerUpdateActions("addBillingAddressId", billingAddressId));
		if(Boolean.TRUE.equals(customerRequestBean.getIsDefaultAddress())) {
			actionsList.add(setCustomerUpdateActions("setDefaultBillingAddress", billingAddressId));
		}

		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);
		return (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private String setShippigAddressIds(CustomerRequestBean customerRequestBean, String customerByIdUrl, String token,
			JsonObject customerObject, String shippingAddressId) {
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();

		actionsList.add(setCustomerUpdateActions("addShippingAddressId", shippingAddressId));
		if(Boolean.TRUE.equals(customerRequestBean.getIsDefaultAddress())) {
			actionsList.add(setCustomerUpdateActions("setDefaultShippingAddress", shippingAddressId));
		}

		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);
		return (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private String addCustomerAddress(AddressRequestBean requestShippingAddress, String customerByIdUrl, String token, JsonObject customerObject) {
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName("addAddress");
		action.setAddress(requestShippingAddress);
		actionsList.add(action);
		updateCustomerServiceBean.setVersion(customerObject.has(VERSION) ? customerObject.get(VERSION).getAsLong() : 0);
		updateCustomerServiceBean.setActions(actionsList);
		return (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private String updateCustomerAddress(AddressRequestBean requestShippingAddress, String customerByIdUrl,
			String token, JsonObject customerObject) {
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName("changeAddress");
		action.setAddress(requestShippingAddress);
		action.setAddressId(requestShippingAddress.getId());
		actionsList.add(action);
		if(Boolean.TRUE.equals(requestShippingAddress.getIsBusiness())) {
			actionsList.add(setAddressCustomField(requestShippingAddress, requestShippingAddress.getId()));
		}
		updateCustomerServiceBean.setVersion(customerObject.has(VERSION) ? customerObject.get(VERSION).getAsLong() : 0);
		updateCustomerServiceBean.setActions(actionsList);
		return (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private Action setCustomerUpdateActions(String actionName, String shippingAddressId) {
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		action.setActionName(actionName);
		action.setAddressId(shippingAddressId);
		return action;
	}

	private String setCustomFieldInAddress(AddressRequestBean requestShippingAddress, String customerByIdUrl, String token, JsonObject customerObject, String shippingAddressId) {
		UpdateCustomerServiceBean updateCustomerServiceBean = new UpdateCustomerServiceBean();
		List<UpdateCustomerServiceBean.Action> actionsList = new ArrayList<>();
		actionsList.add(setAddressCustomField(requestShippingAddress, shippingAddressId));
		updateCustomerServiceBean.setVersion(customerObject.get(VERSION).getAsLong());
		updateCustomerServiceBean.setActions(actionsList);
		return (String)netConnectionHelper.sendPostRequest(customerByIdUrl, token, updateCustomerServiceBean);
	}

	private Action setAddressCustomField(AddressRequestBean requestShippingAddress, String shippingAddressId){
		UpdateCustomerServiceBean.Action action = new UpdateCustomerServiceBean.Action();
		UpdateCustomerServiceBean.Action.Type type = new UpdateCustomerServiceBean.Action.Type();

		action.setActionName("setAddressCustomType");
		type.setId(ctEnvProperties.getAddressType());
		type.setTypeId("type");
		action.setType(type);
		UpdateCustomerServiceBean.Action.Fields fields = new UpdateCustomerServiceBean.Action.Fields();
		fields.setIsBusiness(requestShippingAddress.getIsBusiness());
		action.setFields(fields);
		action.setAddressId(shippingAddressId);
		return action;
	}

	public static void setLifeTimeSales(NetConnectionHelper netConnectionHelper2, String customerResponse, String orderResponse, 
			LifeTimeSalesEnum actionEnum, String customerUrlById, String token, String refundAmount) {
		long orderTotal = 000;
		JsonObject customerObject = MolekuleUtility.parseJsonObject(customerResponse);
		JsonObject customerFieldObject = new JsonObject();
		if(customerObject.has(CUSTOM)) {
			customerFieldObject = customerObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}
		if(customerFieldObject.has(LIFE_TIME_SALES)) {
			orderTotal = customerFieldObject.get(LIFE_TIME_SALES).getAsLong();
		}
		JsonObject orderObject = MolekuleUtility.parseJsonObject(orderResponse);
		JsonObject orderFieldObject = new JsonObject();
		if(orderObject.has(CUSTOM)) {
			orderFieldObject = orderObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}
		if(orderFieldObject.has(ORDER_TOTAL) && actionEnum.equals(LifeTimeSalesEnum.ADD_ORDER_TOTAL)) {
			orderTotal = orderTotal + orderFieldObject.get(ORDER_TOTAL).getAsLong();
		} else if(actionEnum.equals(LifeTimeSalesEnum.SUBTRACT_ORDER_TOTAL)) {
			orderTotal = orderTotal - Long.parseLong(refundAmount);
		}
		//update life time sales in Customer
		if(customerObject.get(VERSION) != null) {
		CustomerAction customerAction = new CustomerAction();
		customerAction.setVersion(customerObject.get(VERSION).getAsLong());
		List<CustomerAction.Actions> list = new ArrayList<>();
		CustomerAction.Actions actionsObj = new CustomerAction.Actions();
		actionsObj.setAction(SET_CUSTOM_FIELD);
		actionsObj.setName(LIFE_TIME_SALES);
		actionsObj.setValue(String.valueOf(orderTotal));
		list.add(actionsObj);
		customerAction.setActions(list);
		netConnectionHelper2.sendPostRequest(customerUrlById, token, customerAction);	
		}
	}

	public String bulkSubscriptionTurnOff(BulkUpdateRequestBean bulkUpdateRequestBean, String token) {
		for (int i = 0; i < bulkUpdateRequestBean.getSubscriptionIdList().size(); i++) {
			String accountbaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey()
			+ CUSTOM_OBJECTS + LIMIT + bulkUpdateRequestBean.getLimit() + OFFSET
			+ bulkUpdateRequestBean.getOffset()
			+ "&where=value((state=\"Scheduled\")or(state=\"Retry-Scheduled\"))" 
			+ "&where=value(subscription=true)" + ID_WITH_QUERY
			+ bulkUpdateRequestBean.getSubscriptionIdList().get(i) + "\"";
			String accountSubscriptionResponse = netConnectionHelper.sendGetWithoutBody(token, accountbaseUrl);
			JsonObject jsonAccountObj = MolekuleUtility.parseJsonObject(accountSubscriptionResponse);	
			if( jsonAccountObj.get(RESULTS).getAsJsonArray().size()>0) {
				JsonObject resultObject = jsonAccountObj.get(RESULTS).getAsJsonArray().get(0).getAsJsonObject();
				String container = resultObject.get(CONTAINER).getAsString();
				String key = resultObject.get("key").getAsString();
				doTurnOffAutoRefills(bulkUpdateRequestBean.getSurveyQuestionId() ,resultObject, container, key, token, bulkUpdateRequestBean.getOther());
				String subscriptionUrl = ctEnvProperties.getHost()+ "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + "/" 
						+ container + "/" + key+ "?expand=value.cart.id&expand=value.customer.id";
				tealiumHelperService.setTurnOffAutorenewal(bulkUpdateRequestBean.getSurveyQuestionId(), subscriptionUrl);
			}
		}
		String responseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ LIMIT + bulkUpdateRequestBean.getLimit() + OFFSET + bulkUpdateRequestBean.getOffset()
				+ "&sort=lastModifiedAt desc&where=value(state=\"Cancelled\")&where=value(subscription=true)";
		return netConnectionHelper.sendGetWithoutBody(token, responseUrl);
	}

	/**
	 * doTurnOffAutoRefills method is used to turn off/ cancel the subscription.
	 * 
	 * @param string
	 * 
	 * @param jsonAccountObj
	 * @param container
	 * @param key
	 * @param token
	 * @return
	 */
	public String doTurnOffAutoRefills(String surveyQuestionId, JsonObject jsonAccountObj, String container,
			String key, String token, String other) {
		SubscriptionSurveyResponse surveyResponse = null;
		if(StringUtils.hasText(surveyQuestionId)) {
			surveyResponse = getSurveysResponseById(surveyQuestionId);
		}
		ObjectMapper mapper = new ObjectMapper();
		String accountBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS;
		JsonObject valueObject = jsonAccountObj.get(VALUE).getAsJsonObject();
		SubscriptionCustomObject customObject = new SubscriptionCustomObject();
		SubscriptionCustomObject.Value value = new SubscriptionCustomObject.Value();
		try {
			value = mapper.readValue(valueObject.toString(), SubscriptionCustomObject.Value.class);

		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		if(value.getState().equals(CANCELLED)) {
			ErrorResponse errorResponse = new ErrorResponse(3000, "Subscription is already cancelled");
			return errorResponse.toString();
		}
		List<TransactionObject> transactionList = new ArrayList<>();
		if(!value.getTransactions().isEmpty() && Objects.nonNull(value.getTransactions()) && value.getTransactions().get(0).getMessage() != null && value.getTransactions().get(0).getType() !=null) {
			transactionList = value.getTransactions();
		}
		TransactionObject transactionObject = new TransactionObject();
		transactionObject.setType("cancelled");
		if(StringUtils.hasText(other)) {
			transactionObject.setMessage(other);
		} else if(surveyResponse!=null && !surveyResponse.getSurveyQuestions().isEmpty()) {
			transactionObject.setMessage(surveyResponse.getSurveyQuestions().get(0).getQuestion());
		} else {
			transactionObject.setMessage("Subscription Turned Off");
		}

		transactionList.add(transactionObject);
		value.setTransactions(transactionList);
		value.setState(CANCELLED);
		customObject.setContainer(container);
		customObject.setKey(key);
		customObject.setVersion(jsonAccountObj.get(MolekuleConstant.VERSION).getAsLong());
		customObject.setValue(value);
		return (String) netConnectionHelper.sendPostRequest(accountBaseUrl, token, customObject);
	}

	/**
	 * getAllSurveysResponse method is to get all subscriptions survey questions.
	 * 
	 * @return ResponseEntity<String>
	 */
	public SubscriptionSurveyResponse getSurveysResponseById(String id) {
		SubscriptionSurveyResponse subscriptionSurveyResponse = getAllSurveysResponse(null).getBody();

		if (Objects.nonNull(subscriptionSurveyResponse)
				&& Objects.nonNull(subscriptionSurveyResponse.getSurveyQuestions())) {
			List<SurveyQuestion> surveyQuestions = subscriptionSurveyResponse.getSurveyQuestions();
			surveyQuestions = surveyQuestions.stream().filter(survey -> survey.getId().equals(id))
					.collect(Collectors.toList());
			subscriptionSurveyResponse.setSurveyQuestions(surveyQuestions);
		}
		return subscriptionSurveyResponse;
	}

	/**
	 * getAllSurveysResponse method is to get all subscriptions survey questions.
	 * @param channel 
	 * 
	 * @return ResponseEntity<String>
	 */
	public ResponseEntity<SubscriptionSurveyResponse> getAllSurveysResponse(CustomerChannelEnum channel) {
		String token = ctServerHelperService.getStringAccessToken();
		String response = netConnectionHelper.sendGetWithoutBody(token, getSubscriptionSurveyResponseUrl());
		SubscriptionSurveyResponse surveyResponse = getSubscriptionSurveyResponse(response);
		if(channel!=null && StringUtils.hasText(channel.name())) {
			List<SurveyQuestion> surveyQuestions = new ArrayList<>();
			for(int i=0; i<surveyResponse.getSurveyQuestions().size(); i++) {
				if(StringUtils.hasText(surveyResponse.getSurveyQuestions().get(i).getChannel())
						&& surveyResponse.getSurveyQuestions().get(i).getChannel().equals(channel.name())) {
					SurveyQuestion surveyQuestion = new SurveyQuestion();
					surveyQuestion.setId(surveyResponse.getSurveyQuestions().get(i).getId());
					surveyQuestion.setChannel(surveyResponse.getSurveyQuestions().get(i).getChannel());
					surveyQuestion.setQuestion(surveyResponse.getSurveyQuestions().get(i).getQuestion());
					surveyQuestions.add(surveyQuestion);
				}
			}
			surveyResponse.setSurveyQuestions(surveyQuestions);
		}

		return new ResponseEntity<>(surveyResponse, HttpStatus.OK);
	}

	/**
	 * getSubscriptionSurveyResponseUrl method is to form the base url for
	 * subscription Surveys Response.
	 * 
	 * @return String
	 */
	public String getSubscriptionSurveyResponseUrl() {
		return ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS + "/b2b" + "/"
				+ "mo_subscription_turnoff_survey";
	}

	/**
	 * getSubscriptionSurveyResponse method is to map SubscriptionSurveyResponse to
	 * response.
	 * 
	 * @return SubscriptionSurveyResponse
	 */

	public SubscriptionSurveyResponse getSubscriptionSurveyResponse(String response) {
		SubscriptionSurveyResponse subscriptionSurveyResponse = new SubscriptionSurveyResponse();
		List<SurveyQuestion> surveyQuestions = new ArrayList<>();

		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		JsonArray resultArrays = valueObject.has("survey-questions")?valueObject.get("survey-questions").getAsJsonArray():new JsonArray();
		for(int i=0;i<resultArrays.size();i++) {
			SubscriptionSurveyResponse.SurveyQuestion surveyQuestion= new SubscriptionSurveyResponse.SurveyQuestion();
			JsonObject resultObject = resultArrays.get(i).getAsJsonObject();
			surveyQuestion.setId(resultObject.get("id").getAsString());
			surveyQuestion.setQuestion(resultObject.get("question").getAsString());
			if(resultObject.has(CHANNEL)) {
				surveyQuestion.setChannel(resultObject.get(CHANNEL).getAsString());
			}
			surveyQuestions.add(surveyQuestion);
		}
		subscriptionSurveyResponse.setSurveyQuestions(surveyQuestions);
		return subscriptionSurveyResponse;
	}

	public String bulkDeleteDevice(List<String> serialNumbersList) {
		String token =  ctServerHelperService.getStringAccessToken();
		String queryDeviceIds = "";
		for(int i=0;i<serialNumbersList.size();i++) {
			queryDeviceIds = queryDeviceIds + "\"" + serialNumbersList.get(i) + "\",";
		}
		queryDeviceIds = queryDeviceIds.substring(0, queryDeviceIds.length() - 1);
		String customObjectDeviceUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS+ "/d2c_device?sort=createdAt desc";
		customObjectDeviceUrl = customObjectDeviceUrl + "&where=value(serialNumber IN (" + queryDeviceIds + "))";

		return netConnectionHelper.deleteRequest(customObjectDeviceUrl, token);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResponseEntity<String> registerDevice(DeviceRequestBean deviceRequestBean, String token,String ctHost,String ctProjectKey, NetConnectionHelper netConnectionHelper) {
		String serialNumber = deviceRequestBean.getSerialNumber();
		String sku = deviceRequestBean.getSku();

		//Checking serial number is already exist or not
		String accountCheckUrl = ctHost + "/" + ctProjectKey + CUSTOM_OBJECTS
				+ "?where=value(serialNumber=\"" + serialNumber + "\")";

		String customResponse = netConnectionHelper.sendGetWithoutBody(token, accountCheckUrl);
		JsonObject customObject = MolekuleUtility.parseJsonObject(customResponse);
		if(customObject.has(RESULTS) && customObject.get(RESULTS).getAsJsonArray().size() >0) {
			ErrorResponse errorResponse = new ErrorResponse(3000,
					"Cannot create device with duplicated SerialNumber.");
			return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		}	
		String customObjectUrl = ctHost + "/" + ctProjectKey + CUSTOM_OBJECTS;
		String container = "d2c_device";
		String key = String.format("%06d", RANDOM.nextInt(999999));
		DeviceCustomObject deviceCustomObject = new DeviceCustomObject();
		deviceCustomObject.setContainer(container);
		deviceCustomObject.setKey(key);
		DeviceCustomObject.Value value = new DeviceCustomObject.Value();
		value.setDeviceId(key);
		value.setSerialNumber(serialNumber);
		value.setSku(sku);
		value.setPurchaseDate(deviceRequestBean.getPurchaseDate());
		value.setRegistraionDate(java.time.LocalDate.now().toString());
		value.setGift(deviceRequestBean.isGift());
		deviceCustomObject.setValue(value);

		setOrderDetails(deviceCustomObject, token, serialNumber, sku, deviceRequestBean,ctHost,ctProjectKey,netConnectionHelper);
		String registeredDeviceResponse = (String) netConnectionHelper.sendPostRequest(customObjectUrl, token, deviceCustomObject);
		return new ResponseEntity<>(registeredDeviceResponse, HttpStatus.OK);
	}

	private static void setOrderDetails(DeviceCustomObject deviceCustomObject, String token, String serialNumber, String sku, DeviceRequestBean deviceRequestBean,String ctHost,String ctProjectKey,NetConnectionHelper netConnectionHelper) {

		String orderUrl = ctHost + "/" + ctProjectKey+ "/orders"
				+ "?sort=createdAt desc"
				+ "&where=lineItems(custom(fields(serialNumbers=\"" + serialNumber + "\")))";
		if(StringUtils.hasText(sku)) {
			orderUrl = orderUrl + "&where=lineItems(variant(sku=\"" + sku + "\"))";
		}

		String response = netConnectionHelper.sendGetWithoutBody(token, orderUrl);
		JsonArray orderResultArray = MolekuleUtility.parseJsonObject(response).get(RESULTS).getAsJsonArray();
		JsonObject orderObject = new JsonObject();
		if(orderResultArray.size()>0) {
			orderObject = orderResultArray.get(0).getAsJsonObject();
		}
		JsonObject fieldsObject = new JsonObject();

		if(orderObject.has(CUSTOM) && orderObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
			fieldsObject = orderObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
		}
		if(fieldsObject.has("gift")) {
			deviceCustomObject.getValue().setGift(fieldsObject.get("gift").getAsBoolean());
		}
		if(StringUtils.hasText(deviceRequestBean.getChannel())) {
			deviceCustomObject.getValue().setChannel(deviceRequestBean.getChannel());
		} else if(fieldsObject.has(CHANNEL)) {
			deviceCustomObject.getValue().setChannel(fieldsObject.get(CHANNEL).getAsString());
		}
		if(StringUtils.hasText(deviceRequestBean.getCustomerId())) {
			deviceCustomObject.getValue().setCustomerId(deviceRequestBean.getCustomerId());
		} else if(orderObject.has(CUSTOMER_ID)) {
			deviceCustomObject.getValue().setCustomerId(orderObject.get(CUSTOMER_ID).getAsString());
		}
		if(StringUtils.hasText(deviceRequestBean.getCustomerNumber())) {
			deviceCustomObject.getValue().setCustomerNumber(deviceRequestBean.getCustomerNumber());
		} else if(StringUtils.hasText(deviceCustomObject.getValue().getCustomerId())) {
			String customerByIdUrl = ctHost+ "/" + ctProjectKey
			+ SLASH_CUSTOMERS_SLASH + deviceCustomObject.getValue().getCustomerId();
			String customerResponse = netConnectionHelper.sendGetWithoutBody(token, customerByIdUrl);
			JsonObject customerJsonObject = MolekuleUtility.parseJsonObject(customerResponse);
			String customerNumber = customerJsonObject.has("customerNumber")? customerJsonObject.get("customerNumber").getAsString():"";
			deviceCustomObject.getValue().setCustomerNumber(customerNumber);
		}
		if(StringUtils.hasText(deviceRequestBean.getRetailSeller())) {
			deviceCustomObject.getValue().setRetailSeller(deviceRequestBean.getRetailSeller());
		} else if(fieldsObject.has("retailSeller")) {
			deviceCustomObject.getValue().setRetailSeller(fieldsObject.get("retailSeller").getAsString());
		}

		if(!StringUtils.hasText(sku) && StringUtils.hasText(serialNumber)) {
			JsonArray lineItemArray =  orderObject.has("lineItems") ? orderObject.get("lineItems").getAsJsonArray() : new JsonArray();
			for(int i=0; i<lineItemArray.size(); i++) {
				JsonObject lineItemObject = lineItemArray.get(i).getAsJsonObject();
				String existingNumber = null;
				if(lineItemObject.has("custom") && lineItemObject.get(CUSTOM).getAsJsonObject().has(FIELDS)) {
					JsonObject customField = lineItemObject.get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject();
					if(customField.has(SERIAL_NUMBERS)) {
						existingNumber = customField.get(SERIAL_NUMBERS).getAsJsonArray().size()>0 ? customField.get(SERIAL_NUMBERS).getAsJsonArray().get(0).getAsString() : null;
					}
				}
				if(serialNumber.equals(existingNumber)) {
					sku = lineItemObject.get("variant").getAsJsonObject().get("sku").getAsString();
					deviceCustomObject.getValue().setSku(sku);
				}
			}
		}
	}
	public List<JsonObject> getSubscriptionContainerAndKey(String customerId, String orderId) {
		String customObjectUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + CUSTOM_OBJECTS
				+ "?where=value(subscription=true)&sort=lastModifiedAt desc&where=value(customer(id=\""
				+ customerId + "\"))&expand=value.cart.typeId";
		String response = netConnectionHelper.sendGetWithoutBody(ctServerHelperService.getStringAccessToken(),
				customObjectUrl);
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		List<JsonObject> subscriptionLists = new ArrayList<>();
		if (0 != jsonObject.get(TOTAL).getAsInt()) {
			JsonArray resultsArray = jsonObject.get(RESULTS).getAsJsonArray();
			for (int i = 0; i < resultsArray.size(); i++) {
				JsonObject resultsObject = resultsArray.get(i).getAsJsonObject();
				JsonObject valueObject = resultsObject.get(VALUE).getAsJsonObject();
				if (valueObject.has("cart")) {
					JsonObject cartObject = valueObject.get("cart").getAsJsonObject();
					if(cartObject.has("obj")) {
						if (cartObject.get("obj").getAsJsonObject().has(CUSTOM)) {
							JsonObject fieldsObject = cartObject.get("obj").getAsJsonObject().get(CUSTOM).getAsJsonObject()
									.get(FIELDS).getAsJsonObject();
							if( fieldsObject.has("parentOrderReferenceId")) {
								String parentOrderReferenceId = fieldsObject.get("parentOrderReferenceId").getAsJsonObject().get(ID)
										.getAsString();
								if (orderId.equals(parentOrderReferenceId)) {
									subscriptionLists.add(resultsObject);
								}
							}
						}
					}
				}
			}
		}
		return subscriptionLists;
	}
}
