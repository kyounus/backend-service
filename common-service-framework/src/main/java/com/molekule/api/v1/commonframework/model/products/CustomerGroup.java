package com.molekule.api.v1.commonframework.model.products;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CustomerGroup {

	@JsonProperty("id")
	private String customerGroupId;
	
	private String typeId;
}
