package com.molekule.api.v1.commonframework.service.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.model.checkout.AdyenRefundRequest;
import com.molekule.api.v1.commonframework.model.checkout.RefundDTO;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@Service("adyenPaymentHeplerService")
public class AdyenPaymentHeplerService {
	
	@Autowired
	CTEnvProperties ctEnvProperties;
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;

	public String refundPayment(Object refundObject) {
		RefundDTO refundDTO = (RefundDTO)refundObject;
		String adyenPaymentRefundUrl = ctEnvProperties.getAdyenHost()+"/checkout/"+ctEnvProperties.getAdyenVersion()+"/payments/"+refundDTO.getPspReference()+"/refunds";
	    AdyenRefundRequest adyenRefundRequest = prepareAdyenRefundRequest(refundDTO);
		return (String)netConnectionHelper.sendPostWithXApiKey(adyenPaymentRefundUrl,ctEnvProperties.getXAPIKey(), adyenRefundRequest);
	}
	
	private AdyenRefundRequest prepareAdyenRefundRequest(RefundDTO refundDTO) {
		AdyenRefundRequest adyenRefundRequest = new AdyenRefundRequest();
		AdyenRefundRequest.Amount amount = new AdyenRefundRequest.Amount();
		amount.setCurrency(refundDTO.getCurrency());
		amount.setValue(Long.parseLong(refundDTO.getAmount()));
		adyenRefundRequest.setAmount(amount);
		adyenRefundRequest.setReference(refundDTO.getReference());
		adyenRefundRequest.setMerchantAccount(ctEnvProperties.getMerchantAccount());
		return adyenRefundRequest;
	}

}
