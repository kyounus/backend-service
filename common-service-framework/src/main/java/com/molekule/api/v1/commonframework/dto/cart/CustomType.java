package com.molekule.api.v1.commonframework.dto.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER_ID;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

@Data
public class CustomType {

	@JsonProperty(VERSION)
	private int version;
	@JsonProperty("actions")
	private List<Actions> actions;
	
	@Data
	public static class Actions {
		@JsonProperty(ACTION)
		private String action;
		@JsonProperty("type")
		private Type type;
		@JsonProperty(FIELDS)
		private Fields fields;
		@JsonProperty(CUSTOMER_ID)
		private String customerId;
		
		@Data
		public static class Type {
			private String id; 
		}
		@Data
		public static class Fields {
			private String accountId; 
		}
	}

}
