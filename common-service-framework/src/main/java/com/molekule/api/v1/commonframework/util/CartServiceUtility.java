package com.molekule.api.v1.commonframework.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean.Action;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.model.cart.AvalaraRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.PaymentIntentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.SetTaxAmount;
import com.molekule.api.v1.commonframework.model.registration.CarrierOptionCustomObject;
public class CartServiceUtility {
	public enum CartActions {
		SET_SHIPPING_ADDRESS, ADD_LINE_ITEM, REMOVE_LINE_ITEM, CHANGE_LINE_ITEM_QTY, SET_PO_NUMBER,SET_PERIOD, SET_CART_AS_QUOTE,
		SET_CUSTOMER_FOR_QUOTE,SET_QUOTE_AS_CART,SET_SHIPPING_METHOD,MERGE_CART, GIFT,SET_EMAIL,WARRANTY
	}
	public enum filterBuyOption{
		FILTER_ONE_TIME,FILTER_AUTO_REFILL
	}
	public static UpdateCartServiceBean getUpdateCartServiceBean(CartRequestBean CartRequestBean,
			Long version, long variantId) {
		UpdateCartServiceBean updateCartServiceBean = new UpdateCartServiceBean();
		updateCartServiceBean.setVersion(version);
		UpdateCartServiceBean.Action upaAction = new UpdateCartServiceBean.Action();
		if (CartRequestBean.getAction().equals(CartActions.ADD_LINE_ITEM.name())) {
			upaAction.setActionName("addLineItem");
		} else if (CartRequestBean.getAction().equals(CartActions.REMOVE_LINE_ITEM.name())) {
			upaAction.setActionName("removeLineItem");
			upaAction.setLineItemId(CartRequestBean.getLineItemId());
		} else if (CartRequestBean.getAction().equals(CartActions.CHANGE_LINE_ITEM_QTY.name())) {
			upaAction.setActionName("changeLineItemQuantity");
			upaAction.setLineItemId(CartRequestBean.getLineItemId());
		}
		upaAction.setProductId(CartRequestBean.getProductId());
		upaAction.setQuantity(CartRequestBean.getQuantity());
		upaAction.setVariantId(variantId);
		ArrayList<Action> actions = new ArrayList<>();
		actions.add(upaAction);
		updateCartServiceBean.setActions(actions);
		return updateCartServiceBean;
	}

	public static AvalaraRequestBean getAvalaraRequestBean(String cartResponse) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonArray jsonArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
		AvalaraRequestBean avalaraRequestBean = new AvalaraRequestBean();
		List<AvalaraRequestBean.Line> lineItems = new ArrayList<>();
		int lineItemLength = jsonArray.size();
		// iterate all line item then set line item amount and quantity to
		// AvalaraRequestBean for tax calculation
		for (int i = 0; i < lineItemLength; i++) {
			if(jsonArray.get(i).getAsJsonObject().has(CUSTOM) && jsonArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS).getAsJsonObject().has(SUBSCRIPTION_ENABLED)){
				continue;
			}
			JsonObject lineItemJson = jsonArray.get(i).getAsJsonObject();
			String lineItemId = lineItemJson.get("id").getAsString();
			int quantity = lineItemJson.get(QUANTITY).getAsInt();
			String productId = lineItemJson.get(PRODUCT_ID).getAsString();
			JsonObject totalPriceJson = lineItemJson.get(TOTAL_PRICE).getAsJsonObject();
			double totalAmount = totalPriceJson.get(CENT_AMOUNT).getAsDouble();
			AvalaraRequestBean.Line line = new AvalaraRequestBean.Line();
			line.setNumber(lineItemId);
			line.setQuantity(quantity);
			line.setAmount(totalAmount);
			line.setItemCode(productId);
			lineItems.add(line);
		}
		// calculate Tax for Custom Line items
		JsonArray customItemsArray = jsonObject.get(CUSTOM_LINE_ITEMS).getAsJsonArray();
		if (customItemsArray.size() > 0) {
			for (int i = 0; i < customItemsArray.size(); i++) {
				JsonObject customJson = customItemsArray.get(i).getAsJsonObject();
				AvalaraRequestBean.Line line = new AvalaraRequestBean.Line();
				line.setNumber(customJson.get("id").getAsString());
				line.setQuantity(customJson.get(QUANTITY).getAsInt());
				line.setAmount(customJson.get("money").getAsJsonObject().get(CENT_AMOUNT).getAsLong());
				lineItems.add(line);
			}
		}
		avalaraRequestBean.setLines(lineItems);
		AvalaraRequestBean.Address address = new AvalaraRequestBean.Address();
		AvalaraRequestBean.Address.SingleLocation singleLocation = new AvalaraRequestBean.Address.SingleLocation();
		JsonObject addressJson = jsonObject.get(SHIPPING_ADDRESS).getAsJsonObject();
		if(addressJson.has("streetName"))
			singleLocation.setLine1(addressJson.get("streetName").getAsString());
		if(addressJson.has("city"))
			singleLocation.setCity(addressJson.get("city").getAsString());
		if(addressJson.has(STATE))
			singleLocation.setRegion(addressJson.get(STATE).getAsString());
		if(addressJson.has(POSTAL_CODE))
			singleLocation.setPostalCode(addressJson.get(POSTAL_CODE).getAsString());
		if(addressJson.has(COUNTRY))
			singleLocation.setCountry(addressJson.get(COUNTRY).getAsString());
		address.setSingleLocation(singleLocation);
		avalaraRequestBean.setAddresses(address);
		avalaraRequestBean.setCommit(true);
		if(jsonObject.has(CUSTOMER_ID)) {
			avalaraRequestBean.setCustomerCode(jsonObject.get(CUSTOMER_ID).getAsString());	
		} else {
			avalaraRequestBean.setCustomerCode(jsonObject.get(ANONYMOUSID).getAsString());
		}
		avalaraRequestBean.setDate(simpleDateFormat.format(new Date()));
		avalaraRequestBean.setCurrencyCode(jsonObject.get("totalPrice").getAsJsonObject().get("currencyCode").getAsString());
		return avalaraRequestBean;
	}

	public static SetTaxAmount updateTaxToCart(String avlararesponse, String cartResponse,boolean isInOrderFlow) {
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonObject avalaraJsonObject = MolekuleUtility.parseJsonObject(avlararesponse);
		JsonArray jsonArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		JsonArray avalaraJsonArray = avalaraJsonObject.get("lines").getAsJsonArray();
		SetTaxAmount setTaxAmount = new SetTaxAmount();
		if(!isInOrderFlow) {
		setTaxAmount.setVersion(cartJsonObject.get(VERSION).getAsLong());
		}
		List<SetTaxAmount.Action> actions = new ArrayList<>();
		// Set Default Tax for subscriptions
		processCustom(avalaraJsonObject, jsonArray, actions);
		processLineItem(avalaraJsonObject, jsonArray, avalaraJsonArray, actions);
		setCustomLineItems(cartJsonObject, avalaraJsonObject, avalaraJsonArray, actions);
		// Set Shipping Method Tax
		if (cartJsonObject.has(SHIPPING_INFO)) {
			setShippingInfo(avalaraJsonObject, actions);
		}
		SetTaxAmount.Action cartTaxAction = new SetTaxAmount.Action();
		if(isInOrderFlow) {
			cartTaxAction.setActionName("setOrderTotalTax");
		}else {
			cartTaxAction.setActionName("setCartTotalTax");
		}
		
		SetTaxAmount.Action.ExternalTotalGross externalTotalGross = new SetTaxAmount.Action.ExternalTotalGross();
		long totalGross = avalaraJsonObject.get(TOTAL_TAX).getAsLong()
				+ avalaraJsonObject.get(TOTAL_AMOUNT).getAsLong();
		externalTotalGross.setCentAmount(totalGross);
		externalTotalGross.setCurrencyCode(avalaraJsonObject.get(CURRENCY_CODE).getAsString());
		cartTaxAction.setExternalTotalGross(externalTotalGross);
		List<SetTaxAmount.Action.TaxPortion> taxPortions = new ArrayList<>();
		JsonArray summaryArray =  avalaraJsonObject.get("summary").getAsJsonArray();
		for(int i=0;i<summaryArray.size();i++) {
			SetTaxAmount.Action.TaxPortion taxPortion = new SetTaxAmount.Action.TaxPortion();
			taxPortion.setRate(summaryArray.get(i).getAsJsonObject().get("rate").getAsDouble());
			taxPortion.setName(summaryArray.get(i).getAsJsonObject().get("taxName").getAsString());
			SetTaxAmount.Action.TaxPortion.Money money = new SetTaxAmount.Action.TaxPortion.Money();
			money.setCentAmount((long)summaryArray.get(i).getAsJsonObject().get(TAX_CALCULATED).getAsDouble());
			money.setCurrencyCode(avalaraJsonObject.get(CURRENCY_CODE).getAsString());
			taxPortion.setAmount(money);
			taxPortions.add(taxPortion);
		}
		cartTaxAction.setExternalTaxPortions(taxPortions);
		actions.add(cartTaxAction);
		boolean isJsonObject = cartJsonObject.has(CUSTOM);
		SetTaxAmount.Action customValue = new SetTaxAmount.Action();
		if (isJsonObject) {
			customValue.setActionName(SET_CUSTOM_FIELD);
			customValue.setName(TOTAL_TAX);
			customValue.setValue(String.valueOf(avalaraJsonObject.get(TOTAL_TAX).getAsLong()));
		} else {
			SetTaxAmount.Action.Type type = new SetTaxAmount.Action.Type();
			customValue.setActionName("setCustomType");
			type.setId("937cb0c5-55a8-4c88-af52-3bcb8c53a04c");
			type.setTypeId("type");
			customValue.setType(type);
			SetTaxAmount.Action.Fields fields = new SetTaxAmount.Action.Fields();
			fields.setTotalTax(String.valueOf(avalaraJsonObject.get(TOTAL_TAX).getAsLong()));
			customValue.setFields(fields);
		}
		actions.add(customValue);
		if(isInOrderFlow) {
			SetTaxAmount.Action taxCalCustomField = new SetTaxAmount.Action();
			taxCalCustomField.setActionName(SET_CUSTOM_FIELD);
			taxCalCustomField.setName("isTaxCalculated");
			taxCalCustomField.setValue("true");
			actions.add(taxCalCustomField);
		}
		setTaxAmount.setActions(actions);
		return setTaxAmount;
	}

	private static void processCustom(JsonObject avalaraJsonObject, JsonArray jsonArray,
			List<SetTaxAmount.Action> actions) {
		for (int i = 0; i < jsonArray.size(); i++) {
			boolean hasCustom = jsonArray.get(i).getAsJsonObject().has(CUSTOM);
			if (hasCustom && jsonArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS)
					.getAsJsonObject().has(SUBSCRIPTION_ENABLED)) {
				SetTaxAmount.Action action = new SetTaxAmount.Action();
				action.setActionName(SET_LINE_ITEM_TAX_AMOUNT);
				action.setLineItemId(jsonArray.get(i).getAsJsonObject().get("id").getAsString());
				SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
				SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
				totalGross.setCentAmount(0);
				totalGross.setCurrencyCode(avalaraJsonObject.get(CURRENCY_CODE).getAsString());
				externalTaxAmount.setTotalGross(totalGross);
				SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
				taxRate.setAmount(0.0);
				taxRate.setCountry(avalaraJsonObject.get(COUNTRY).getAsString());
				taxRate.setName(MY_TAX_RATE);
				externalTaxAmount.setTaxRate(taxRate);
				action.setExternalTaxAmount(externalTaxAmount);
				actions.add(action);
			}
		}
	}

	private static void processLineItem(JsonObject avalaraJsonObject, JsonArray jsonArray, JsonArray avalaraJsonArray,
			List<SetTaxAmount.Action> actions) {
		for (int i = 0; i < jsonArray.size(); i++) {
			String lineItemId = jsonArray.get(i).getAsJsonObject().get("id").getAsString();
			for (int j = 0; j < avalaraJsonArray.size(); j++) {
				String avalaraLine = avalaraJsonArray.get(j).getAsJsonObject().get(LINE_NUMBER).getAsString();
				if (lineItemId.equals(avalaraLine)) {
					SetTaxAmount.Action action = new SetTaxAmount.Action();
					JsonObject taxLineItemJson = avalaraJsonArray.get(j).getAsJsonObject();
					String ctLineItemId = taxLineItemJson.get(LINE_NUMBER).getAsString();
					action.setActionName(SET_LINE_ITEM_TAX_AMOUNT);
					action.setLineItemId(ctLineItemId);
					SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
					SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
					long totalAmount = taxLineItemJson.get(TAX_CALCULATED).getAsLong();
					long totalGrossAmount = totalAmount+taxLineItemJson.get("lineAmount").getAsLong();
					totalGross.setCentAmount(totalGrossAmount);
					totalGross.setCurrencyCode(avalaraJsonObject.get(CURRENCY_CODE).getAsString());
					externalTaxAmount.setTotalGross(totalGross);
					SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
					double taxPercentage = getTaxPercentage(taxLineItemJson);
					taxRate.setAmount(taxPercentage);
					taxRate.setCountry(avalaraJsonObject.get(COUNTRY).getAsString());
					taxRate.setName("tax");
					externalTaxAmount.setTaxRate(taxRate);
					action.setExternalTaxAmount(externalTaxAmount);
					actions.add(action);
					break;
				}

			}

		}
	}

	private static void setCustomLineItems(JsonObject cartJsonObject, JsonObject avalaraJsonObject,
			JsonArray avalaraJsonArray, List<SetTaxAmount.Action> actions) {
		JsonArray customLineArray = cartJsonObject.get(CUSTOM_LINE_ITEMS).getAsJsonArray();
		for (int i = 0; i < customLineArray.size(); i++) {
			String lineItemId = customLineArray.get(i).getAsJsonObject().get("id").getAsString();
			for (int j = 0; j < avalaraJsonArray.size(); j++) {
				String avalaraLine = avalaraJsonArray.get(j).getAsJsonObject().get(LINE_NUMBER).getAsString();
				if (lineItemId.equals(avalaraLine)) {
					SetTaxAmount.Action action = new SetTaxAmount.Action();
					JsonObject taxLineItemJson = avalaraJsonArray.get(j).getAsJsonObject();
					String customLineItemId = taxLineItemJson.get(LINE_NUMBER).getAsString();
					action.setActionName("setCustomLineItemTaxAmount");
					action.setCustomLineItemId(customLineItemId);
					SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
					SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
					long totalAmount = taxLineItemJson.get(TAX_CALCULATED).getAsLong();
					long totalGrossAmount = totalAmount+taxLineItemJson.get("lineAmount").getAsLong();
					totalGross.setCentAmount(totalGrossAmount);
					totalGross.setCurrencyCode(avalaraJsonObject.get(CURRENCY_CODE).getAsString());
					externalTaxAmount.setTotalGross(totalGross);
					SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
					double taxPercentage = getTaxPercentage(taxLineItemJson);
					taxRate.setAmount(taxPercentage);
					taxRate.setCountry(avalaraJsonObject.get(COUNTRY).getAsString());
					taxRate.setName("tax");
					externalTaxAmount.setTaxRate(taxRate);
					action.setExternalTaxAmount(externalTaxAmount);
					actions.add(action);
					break;
				}

			}

		}
	}

	private static void setShippingInfo(JsonObject avalaraJsonObject, List<SetTaxAmount.Action> actions) {
		SetTaxAmount.Action action = new SetTaxAmount.Action();
		action.setActionName(SET_SHIPPING_METHOD_TAX_AMOUNT);
		SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
		SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
		totalGross.setCentAmount(0);
		totalGross.setCurrencyCode(avalaraJsonObject.get(CURRENCY_CODE).getAsString());
		externalTaxAmount.setTotalGross(totalGross);
		SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
		taxRate.setAmount(0.0);
		taxRate.setCountry(avalaraJsonObject.get(COUNTRY).getAsString());
		taxRate.setName("taxRate");
		externalTaxAmount.setTaxRate(taxRate);
		action.setExternalTaxAmount(externalTaxAmount);
		actions.add(action);
	}

	private static double getTaxPercentage(JsonObject taxLineItemJson) {

		JsonArray detailsArray =  taxLineItemJson.get("details").getAsJsonArray();
		double taxPercentage = 0.0;
		for(int i=0;i<detailsArray.size();i++) {
			taxPercentage = taxPercentage+detailsArray.get(i).getAsJsonObject().get("rate").getAsDouble();
		}
		return taxPercentage;
	}

	public static UpdateCartServiceBean getShippingMethodTaxRequestBean(JsonObject jsonObject,String currency) {
		UpdateCartServiceBean serviceBean = new UpdateCartServiceBean();
		serviceBean.setVersion(jsonObject.get(VERSION).getAsLong());
		List<UpdateCartServiceBean.Action> actionsList = new ArrayList<>();
		UpdateCartServiceBean.Action updateTaxAction = new UpdateCartServiceBean.Action();
		updateTaxAction.setActionName(SET_SHIPPING_METHOD_TAX_AMOUNT);
		UpdateCartServiceBean.Action.ExternalTaxAmount externalTaxAmount = new UpdateCartServiceBean.Action.ExternalTaxAmount();
		UpdateCartServiceBean.Action.ExternalTaxAmount.TaxRate taxRate = new UpdateCartServiceBean.Action.ExternalTaxAmount.TaxRate();
		UpdateCartServiceBean.Action.ExternalTaxAmount.TotalGross totalGross = new UpdateCartServiceBean.Action.ExternalTaxAmount.TotalGross();
		totalGross.setCurrencyCode(currency);
		totalGross.setCentAmount(0);
		externalTaxAmount.setTotalGross(totalGross);
		taxRate.setAmount(0.12);
		taxRate.setName(MY_TAX_RATE);
		taxRate.setCountry("US");
		taxRate.setIncludedInPrice(true);
		externalTaxAmount.setTaxRate(taxRate);
		externalTaxAmount.setTotalGross(totalGross);
		updateTaxAction.setExternalTaxAmount(externalTaxAmount);
		actionsList.add(updateTaxAction);
		serviceBean.setActions(actionsList);
		return serviceBean;
	}

	public static String getSubscriptionProductId(String response) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		String subscriptionId = null;
		JsonArray attributes = jsonObject.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		if(attributes == null || attributes.size() <= 0) {
			return null;
		}
		for(int i=0;i<attributes.size();i++) {
			String name = attributes.get(i).getAsJsonObject().get("name").getAsString();
			if("SubscriptionIdentifier".equals(name)) {
				subscriptionId = attributes.get(i).getAsJsonObject().get(VALUE).getAsJsonObject().get("id").getAsString();
				break;
			}
		}
		return subscriptionId;
	}
	public static PaymentIntentRequest createPaymentRequest(PaymentOrderDTO orderRequestBean) {
		PaymentIntentRequest paymentmRequestBean = new PaymentIntentRequest();
		paymentmRequestBean.setAmount(orderRequestBean.getAmount());
		paymentmRequestBean.setSetUpPaymentIntentId(orderRequestBean.getSetupPaymentIntentId());
		return paymentmRequestBean;
	}
	
	public static PaymentIntentRequest createAffirmPaymentRequest(PaymentOrderDTO orderRequestBean) {
		PaymentIntentRequest paymentmRequestBean = new PaymentIntentRequest();
		paymentmRequestBean.setAmount(orderRequestBean.getAmount());
		return paymentmRequestBean;
	}
	
	public static String createQuoteNumber() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
		return sdf.format(new Date());
	}

	public static String getSubLineItemId(JsonObject jsonObject ,String subscriptionProductId) {
		JsonArray jsonArray = jsonObject.get(LINE_ITEMS).getAsJsonArray();
		String lineItemId = null;
		for(int i=0;i<jsonArray.size();i++) {
			String productId = jsonArray.get(i).getAsJsonObject().get(PRODUCT_ID).getAsString();
			if(subscriptionProductId.equals(productId)) {
				lineItemId = jsonArray.get(i).getAsJsonObject().get("id").getAsString();
				break;
			}
		}

		return lineItemId;
	}

	public static UpdateCartServiceBean getSubscriptionRequestBean(CartRequestBean cartRequestBean,
			Long version, long variantId,String currency) {
		UpdateCartServiceBean updateCartServiceBean = new UpdateCartServiceBean();
		updateCartServiceBean.setVersion(version);
		UpdateCartServiceBean.Action upaAction = new UpdateCartServiceBean.Action();
		if (cartRequestBean.getAction().equals(CartServiceUtility.CartActions.ADD_LINE_ITEM.name())) {
			upaAction.setActionName("addLineItem");
			UpdateCartServiceBean.Action.ExternalTotalPrice externalTotalPrice = new UpdateCartServiceBean.Action.ExternalTotalPrice();
			UpdateCartServiceBean.Action.ExternalTotalPrice.Price price  = new UpdateCartServiceBean.Action.ExternalTotalPrice.Price();
			price.setCurrencyCode(currency);
			price.setCentAmount(0);
			externalTotalPrice.setPrice(price);
			UpdateCartServiceBean.Action.ExternalTotalPrice.TotalPrice totalPrice = new UpdateCartServiceBean.Action.ExternalTotalPrice.TotalPrice();
			totalPrice.setCurrencyCode(currency);
			totalPrice.setCentAmount(0);
			externalTotalPrice.setTotalPrice(totalPrice);
			upaAction.setExternalTotalPrice(externalTotalPrice);
		} else if (cartRequestBean.getAction().equals(CartServiceUtility.CartActions.REMOVE_LINE_ITEM.name())) {
			upaAction.setActionName("removeLineItem");
			upaAction.setLineItemId(cartRequestBean.getLineItemId());
		}
		upaAction.setQuantity(1);
		upaAction.setProductId(cartRequestBean.getProductId());
		upaAction.setVariantId(variantId); 
		ArrayList<Action> actions = new ArrayList<>();
		actions.add(upaAction);
		updateCartServiceBean.setActions(actions);
		return updateCartServiceBean;
	}

	public static UpdateCartServiceBean getSubCustomTypeAction(long version,String lineItemId,String lineItemTypeId) {
		UpdateCartServiceBean updateCartServiceBean = new UpdateCartServiceBean();
		updateCartServiceBean.setVersion(version);
		List<UpdateCartServiceBean.Action> actions = new ArrayList<>();
		UpdateCartServiceBean.Action lineItemCustom = new UpdateCartServiceBean.Action();
		lineItemCustom.setActionName("setLineItemCustomType");
		lineItemCustom.setLineItemId(lineItemId);
		UpdateCartServiceBean.Action.Type type = new UpdateCartServiceBean.Action.Type();
		type.setTypeId("type");
		type.setId(lineItemTypeId);
		lineItemCustom.setType(type);
		UpdateCartServiceBean.Action.Fields fields = new UpdateCartServiceBean.Action.Fields();
		fields.setSubscriptionEnabled(true);
		lineItemCustom.setFields(fields);
		actions.add(lineItemCustom);
		updateCartServiceBean.setActions(actions);
		return updateCartServiceBean;
	}

	public static int getSubscriptionPeriod(String response) {
		int period = 0;
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(response);
		JsonArray attributes = jsonObject.get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject().get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
		if(attributes == null || attributes.size() <= 0) {
			return 0;
		}
		return getPeriod(period, attributes);
	}

	private static int getPeriod(int period, JsonArray attributes) {
		for (int i = 0; i < attributes.size(); i++) {
			String name = attributes.get(i).getAsJsonObject().get("name").getAsString();
			if ("SubscriptionIdentifier".equals(name)) {
				JsonArray jsonArray = attributes.get(i).getAsJsonObject().get(VALUE).getAsJsonObject().get("obj")
						.getAsJsonObject().get(MASTER_DATA).getAsJsonObject().get(CURRENT).getAsJsonObject()
						.get(MASTER_VARIANT).getAsJsonObject().get(ATTRIBUTES).getAsJsonArray();
				if (jsonArray == null || jsonArray.size() <= 0) {
					return 0;
				}
				for (int j = 0; j < attributes.size(); j++) {
					String periodName = jsonArray.get(j).getAsJsonObject().get("name").getAsString();
					if (PERIOD.equals(periodName)) {
						period = jsonArray.get(j).getAsJsonObject().get(VALUE).getAsInt();
						break;
					}
				}
				break;
			}
		}
		return period;
	}

	public static UpdateCartServiceBean getCustomLineItemMasterBean(long version,String lineItemId, String lineItemTypeId) {
		UpdateCartServiceBean updateCartServiceBean = new UpdateCartServiceBean();
		updateCartServiceBean.setVersion(version);
		List<UpdateCartServiceBean.Action> actions = new ArrayList<>();
		UpdateCartServiceBean.Action lineItemCustom = new UpdateCartServiceBean.Action();
		lineItemCustom.setActionName("setLineItemCustomType");
		lineItemCustom.setLineItemId(lineItemId);
		UpdateCartServiceBean.Action.Type type = new UpdateCartServiceBean.Action.Type();
		type.setTypeId("type");
		type.setId(lineItemTypeId);
		lineItemCustom.setType(type);
		UpdateCartServiceBean.Action.Fields fields = new UpdateCartServiceBean.Action.Fields();
		fields.setSubscriptionEnabled(false);
		lineItemCustom.setFields(fields);
		actions.add(lineItemCustom);
		updateCartServiceBean.setActions(actions);
		return updateCartServiceBean;
	}

	/**
	 * updateNoTaxToCart method is used when tax-exempt is true.
	 * 
	 * @param cartResponse
	 * @return SetTaxAmount
	 */
	public static SetTaxAmount updateNoTaxToCart(String cartResponse,String currency) {
		JsonObject cartJsonObject = MolekuleUtility.parseJsonObject(cartResponse);
		JsonArray jsonArray = cartJsonObject.get(LINE_ITEMS).getAsJsonArray();
		SetTaxAmount setTaxAmount = new SetTaxAmount();
		setTaxAmount.setVersion(cartJsonObject.get(VERSION).getAsLong());
		List<SetTaxAmount.Action> actions = new ArrayList<>();
		// Set Default Tax for subscriptions
		for (int i = 0; i < jsonArray.size(); i++) {
			boolean hasCustom = jsonArray.get(i).getAsJsonObject().has(CUSTOM);
			if (hasCustom && jsonArray.get(i).getAsJsonObject().get(CUSTOM).getAsJsonObject().get(FIELDS)
					.getAsJsonObject().has(SUBSCRIPTION_ENABLED)) {
				SetTaxAmount.Action action = new SetTaxAmount.Action();
				action.setActionName(SET_LINE_ITEM_TAX_AMOUNT);
				action.setLineItemId(jsonArray.get(i).getAsJsonObject().get("id").getAsString());
				SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
				SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
				totalGross.setCentAmount(0);
				totalGross.setCurrencyCode(currency);
				externalTaxAmount.setTotalGross(totalGross);
				SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
				taxRate.setAmount(0.0);
				taxRate.setCountry("US");
				taxRate.setName(MY_TAX_RATE);
				externalTaxAmount.setTaxRate(taxRate);
				action.setExternalTaxAmount(externalTaxAmount);
				actions.add(action);
			}
		}
		for (int i = 0; i < jsonArray.size(); i++) {
			String lineItemId = jsonArray.get(i).getAsJsonObject().get("id").getAsString();
			SetTaxAmount.Action action = new SetTaxAmount.Action();
			action.setActionName(SET_LINE_ITEM_TAX_AMOUNT);
			action.setLineItemId(lineItemId);
			SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
			SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
			String totalPriceCentAmount = jsonArray.get(i).getAsJsonObject().get("totalPrice").getAsJsonObject().get("centAmount").getAsString();
			totalGross.setCentAmount(Long.parseLong(totalPriceCentAmount));
			totalGross.setCurrencyCode("USD");
			externalTaxAmount.setTotalGross(totalGross);
			SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
			taxRate.setAmount(0);
			taxRate.setCountry("US");
			taxRate.setName("tax");
			externalTaxAmount.setTaxRate(taxRate);
			action.setExternalTaxAmount(externalTaxAmount);
			actions.add(action);
		}
		JsonArray customLineArray = cartJsonObject.get(CUSTOM_LINE_ITEMS).getAsJsonArray();
		if (!Objects.isNull(customLineArray) && customLineArray.size()> 0) {
			String lineItemId = customLineArray.get(0).getAsJsonObject().get("id").getAsString();
			SetTaxAmount.Action action = new SetTaxAmount.Action();
			action.setActionName("setCustomLineItemTaxAmount");
			action.setCustomLineItemId(lineItemId);
			SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
			SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
			String totalPriceCentAmount = customLineArray.get(0).getAsJsonObject().get("totalPrice").getAsJsonObject().get("centAmount").getAsString();
			totalGross.setCentAmount(Long.parseLong(totalPriceCentAmount));
			totalGross.setCurrencyCode("USD");
			externalTaxAmount.setTotalGross(totalGross);
			SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
			taxRate.setAmount(0);
			taxRate.setCountry("US");
			taxRate.setName("tax");
			externalTaxAmount.setTaxRate(taxRate);
			action.setExternalTaxAmount(externalTaxAmount);
			actions.add(action);
		}
		// Set Shipping Method Tax
		if (cartJsonObject.has(SHIPPING_INFO)) {
			SetTaxAmount.Action action = new SetTaxAmount.Action();
			action.setActionName(SET_SHIPPING_METHOD_TAX_AMOUNT);
			SetTaxAmount.Action.ExternalTaxAmount externalTaxAmount = new SetTaxAmount.Action.ExternalTaxAmount();
			SetTaxAmount.Action.ExternalTaxAmount.TotalGross totalGross = new SetTaxAmount.Action.ExternalTaxAmount.TotalGross();
			totalGross.setCentAmount(0);
			totalGross.setCurrencyCode("USD");
			externalTaxAmount.setTotalGross(totalGross);
			SetTaxAmount.Action.ExternalTaxAmount.TaxRate taxRate = new SetTaxAmount.Action.ExternalTaxAmount.TaxRate();
			taxRate.setAmount(0.0);
			taxRate.setCountry("US");
			taxRate.setName("taxRate");
			externalTaxAmount.setTaxRate(taxRate);
			action.setExternalTaxAmount(externalTaxAmount);
			actions.add(action);
		}
		SetTaxAmount.Action cartTaxAction = new SetTaxAmount.Action();
		cartTaxAction.setActionName("setCartTotalTax");
		SetTaxAmount.Action.ExternalTotalGross externalTotalGross = new SetTaxAmount.Action.ExternalTotalGross();
		externalTotalGross.setCentAmount(0);
		externalTotalGross.setCurrencyCode("USD");
		cartTaxAction.setExternalTotalGross(externalTotalGross);
		List<SetTaxAmount.Action.TaxPortion> taxPortions = new ArrayList<>();
		SetTaxAmount.Action.TaxPortion taxPortion = new SetTaxAmount.Action.TaxPortion();
		taxPortion.setRate(0);
		taxPortion.setName("taxName");
		SetTaxAmount.Action.TaxPortion.Money money = new SetTaxAmount.Action.TaxPortion.Money();
		money.setCentAmount(0);
		money.setCurrencyCode("USD");
		taxPortion.setAmount(money);
		taxPortions.add(taxPortion);
		cartTaxAction.setExternalTaxPortions(taxPortions);
		actions.add(cartTaxAction);
		boolean isJsonObject = cartJsonObject.has(CUSTOM);
		SetTaxAmount.Action customValue = new SetTaxAmount.Action();
		if (isJsonObject) {
			customValue.setActionName(SET_CUSTOM_FIELD);
			customValue.setName(TOTAL_TAX);
			customValue.setValue("0");
		} else {
			SetTaxAmount.Action.Type type = new SetTaxAmount.Action.Type();
			customValue.setActionName("setCustomType");
			type.setId("937cb0c5-55a8-4c88-af52-3bcb8c53a04c");
			type.setTypeId("type");
			customValue.setType(type);
			SetTaxAmount.Action.Fields fields = new SetTaxAmount.Action.Fields();
			fields.setTotalTax("0");
			customValue.setFields(fields);
		}
		actions.add(customValue);
		setTaxAmount.setActions(actions);
		return setTaxAmount;
	}
	
	public static CarrierOptionCustomObject getCarrierOptionObjById(String carrierOptionId,AccountCustomObject accountCustomObject) {
		CarrierOptionCustomObject carrierOptionCustomObject = null;
		List<CarrierOptionCustomObject> carrierOptionCustomObjects = accountCustomObject.getValue().getCarriers();
		if(carrierOptionCustomObjects != null) {
			 Iterator<CarrierOptionCustomObject> carrierOptionCustomItr = carrierOptionCustomObjects.iterator();
			 while(carrierOptionCustomItr.hasNext()) {
				 carrierOptionCustomObject = carrierOptionCustomItr.next();
				 if(carrierOptionId.equals(carrierOptionCustomObject.getCarrierId())) {
					 break;
				 }
			 }
		}
		return carrierOptionCustomObject;
	}
}
