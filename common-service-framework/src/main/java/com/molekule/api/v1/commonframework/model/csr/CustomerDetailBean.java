package com.molekule.api.v1.commonframework.model.csr;

import java.util.List;

import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.products.CustomerGroup;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;

import lombok.Data;
/**
 * CustomerDetailBean is used to display customer details.
 */
@Data
public class CustomerDetailBean {

	private String customerId;
	private String email;
	private String customerSince;
	private CustomerGroup customerGroup;
	private String customerGroupName;
	private String lifeTimeSales;
	private String totalTickets;
	private String country;
	private String customerType;
	private List<BillingAddresses> billingAddresses;
	private List<AddressRequestBean> addressList;
	private List<AddressRequestBean> shippingAddressList;
	private List<AddressRequestBean> billingAddressList;
	private List<String> billingAddressIds;
	private List<String> shippingAddressIds;
	private String defaultShippingAddressId;
	private String defaultBillingAddressId;
	private BillingAddresses defaultBillingAddress;
	private ShippingAddresses defaultShippingAddress;
	private String companyName;
	private String companyPhoneNumber;
	private String companyAddress;
	private List<OrderSummary> orderSummaryList;
	
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private List<Payments> payments;
	private String preferredPaymentId;
	private String cartId;
	
	private String customerSource;
	private String channel;
	
	private String store;
	private String comments;
	private String registeredChannel;
	private String dateOfBirth;
	private String recentlyUsedAddressId;
	private Boolean accountLocked;
	private String recentlyUsedPayment;
	private Boolean marketingOffers;
	private Boolean termsOfService;
	
	@Data
	public static class OrderSummary{
		
		private String orderId;
		private String orderNumber;
		private String orderStatus;
		private String orderTotal;
		private String store;
		
	}
}
