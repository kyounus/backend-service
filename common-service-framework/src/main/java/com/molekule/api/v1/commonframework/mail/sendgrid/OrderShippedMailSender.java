package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("orderShippedMailSender")
public class OrderShippedMailSender extends TemplateMailRequestHelper {
	@Value("${orderShippedTemplateId}")
	private String templateId;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(
			SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		if (sendGridModel.getOrderFedExTrackingInfo() != null) {
			dynamicTemplateData.setOrderFedExTrackingInfo(sendGridModel.getOrderFedExTrackingInfo());
		}
		if (sendGridModel.getCustomerCarrierName() != null) {
			dynamicTemplateData.setCustomerCarrierName(sendGridModel.getCustomerCarrierName());
		}
		if (sendGridModel.getCustomerCarrierAccountNumber() != null) {
			dynamicTemplateData.setCustomerCarrierAccountNumber(sendGridModel.getCustomerCarrierAccountNumber());
		}
		return dynamicTemplateData;
	}
}
