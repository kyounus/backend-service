package com.molekule.api.v1.commonframework.model.sendgrid;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SendGridRequestBean {

	@JsonProperty("template_id")
	private String templateId;
	private From from;
	private List<Personalizations> personalizations;
	private List<Attachments> attachments;

	@Data
	public static class From {

		private String email;
		private String name;

	}
	
	@Data
	public static class Attachments {

		private String content;
		private String type;
		private String fileName;
	}

	@Data
	public static class Personalizations {

		private List<To> to;
		@JsonProperty("dynamic_template_data")
		private DynamicTemplateData dynamicTemplateData;

		@Data
		public static class To {

			private String email;
			private String name;

		}
		
		@Data
		public static class DynamicTemplateData {
			private String customerFirstName;
			private String customerLastName;
			private String customerStreetOne;
			private String customerStreetTwo;
			private String customerCity;
			private String customerState;
			private String customerCountry;
			private String customerZip;
			private String customerEmail;
			private String customerPhoneNumber;
			private String orderDate;
			private String customerCompanyName;
			private String customerCompanyIndustry;
			private String customerRecordUrl;
	        @JsonProperty("CustomerCompanyRecordUrl")
			private String customerCompanyRecordUrl;
			private String validateEmailUrl;
			private String taxExemptionStatusUrl;
			private String requestedDate;
			private String salesRepresentativeFirstName;
			private String salesRepresentativeLastName;
			private String salesRepresentativeEmail;
			private String salesRepresentativePhoneNumber;
			private String salesRepresentativeCalendlyLink;
			private String approvedStates;
			private String orderNumber;
			private String orderProductName;
			private String orderShippingMethod;
			private List<Product> products;
			private List<Map<String,Object>> productlist;
			private String orderSummarySubtotal;
			private String orderSummaryShipping;
			private String orderSummaryHandling;
			private String orderSummaryTax;
			private String orderSummaryDiscount;
			private String orderSummaryTotal;
			private String orderShippingAddressCompanyName;
			private String orderShippingAddressContactFirstName;
			private String orderShippingAddressContactLastName;
			private String orderShippingAddressStreet1;
			private String orderShippingAddressStreet2;
			private String orderShippingAddressCity;
			private String orderShippingAddressState;
			private String orderShippingAddressZip;
			private String orderShippingAddressPhoneNumber;
			private String orderBillingAddressFirstName;
			private String orderBillingAddressLastName;
			private String orderBillingAddressStreet1;
			private String orderBillingAddressStreet2;
			private String orderBillingAddressCity;
			private String orderBillingAddressState;
			private String orderBillingAddressZip;
			private String orderBillingAddressPhoneNumber;
			private String orderPaymentMethod;
			private boolean nonCreditCardPayment;
			private boolean paymentTermsApplication;
			private boolean taxExemptionRequest;
			private boolean missingFreightInformation;
			private boolean moreFreightInformation;
			private String orderMerchantCenterUrl;
			private String orderUrl;
			private String paymentDocumentUrl;
			private String accountDashboardUrl;
			private String b2bHomepageUrl;
			private String paymentTermsStatusUrl;
			private String orderPlaceDate;
			private String nextRefillDate;
			private String subscriptionTotal;
			private String quoteRequestNumber;
			private String quoteStatusUrl;
			private String requestQuoteUrl;
			private String requestQuoteDate;
			private String orderFedExTrackingInfo;
			private String customerCarrierName;
			private String customerCarrierAccountNumber;
			private boolean requireFreightInformtion;
			private boolean requirePaymentInformation;
			private List<String> filterOrder;
			private String emailHeader;
            private String emailBody;
            private String filterAutoRefillsUrl;
            private String firstName;
            private String subTotal;
            private String discount;
            private String tax;
            private String shippingCost;
            private String orderTotal;
            private boolean gift;
            private boolean billingAddress;
            private String cancelledOrderUrl;
            private String shippingDesc;
            private boolean subscription;
            private boolean promo;
            private String requestId;
            private String customerName;
            private String returnLabelUrl;
            private String updatedEmail;
            private String customerPassword;
            private String myAccountUrl;
            private String subscriptionPlanName;
        	private String subscriptionRenewalDate;
        	private String subscriptionDeviceSerial;
        	private String subscriptionPrice;
        	private boolean creditToCC;
        	private String returnTotal;
        	private String taxNote;
		}
	}

}
