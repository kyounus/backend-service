package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The class BillingAddress used to hold the data for billing address. 
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BillingAddress extends Address {

	private String phoneNumber;

	private String country;
	
}
