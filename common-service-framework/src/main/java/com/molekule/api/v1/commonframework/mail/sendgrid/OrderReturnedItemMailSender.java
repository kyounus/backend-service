package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("orderReturnedItemMailSender")
public class OrderReturnedItemMailSender extends TemplateMailRequestHelper  {

	@Value("${orderRefundTemplateId}")
	private String templateId;

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setProducts(sendGridModel.getProducts());
		dynamicTemplateData.setPromo(true);
		dynamicTemplateData.setCreditToCC(sendGridModel.isCreditToCC());
		dynamicTemplateData.setSubTotal(sendGridModel.getOrderSummarySubtotal());
		dynamicTemplateData.setTax(sendGridModel.getOrderSummaryTax());
		dynamicTemplateData.setShippingCost(sendGridModel.getOrderSummaryShipping());
		dynamicTemplateData.setReturnTotal(sendGridModel.getReturnTotal());
		dynamicTemplateData.setTaxNote("Including VAT");
		return dynamicTemplateData;
	}
}
