package com.molekule.api.v1.commonframework.dto.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PAYMENTS;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.checkout.CheckoutSurveyResponse.CheckoutSurvey;
import com.molekule.api.v1.commonframework.model.customerprofile.SubscriptionSurveyResponse.SurveyQuestion;
import com.molekule.api.v1.commonframework.model.registration.AdditionalFreightInfo;
import com.molekule.api.v1.commonframework.model.registration.Address;
import com.molekule.api.v1.commonframework.model.registration.AddressV2;
import com.molekule.api.v1.commonframework.model.registration.CarrierOptionCustomObject;
import com.molekule.api.v1.commonframework.model.registration.MolekuleCarrier;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierRelationShip;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReview;

import lombok.Data;

/**
 * The class Value is used to hold the data for the full registration.
 */
@Data
public class Value {

	private String id;
	private String companyName;
	private String noOfEmployees;
	private String noOfOfficeSpaces;
	private boolean parcelFlag;
	private boolean freightFlag;
	private AddressV2 addresses;
	private String downloadUrl;
	private List<String> employeeIds;
	
	private String defaultPaymentId;
	@JsonProperty(PAYMENTS)
	private List<Payments> payments;
    private Customer[] customers;
    
    // Sales Representative details
    private SalesRepresentativeDto salesRepresentative;
	
    private List<TaxExemptReview> taxExemptReviewList;
    
    private boolean taxExemptFlag = false;
    
    //Company Categories
    
    private CompanyCategories companyCategories;
    
    private List<MolekuleCarrier> shippingMethods;
    
    private CategoryDto companyCategory;
    
    @JsonProperty("survey-questions")
	private List<SurveyQuestion> surveyQuestions;

    
    private String defaultBillingAddressId;
    private String defaultShippingAddressId;
    private String defaultCarrierId;
    private List<ShippingCarrierRelationShip> shippingCarrierRelationShip;
    private List<CarrierOptionCustomObject> carriers;
    
    private boolean myOwnCarrier;
    private List<AdditionalFreightInfo> additionalFreightInfos; 
    
    private Address companyAddress;
    
    private List<String> taxCertificateList;
    
    @JsonProperty("sales-reps")
    private List<SalesRepresentativeDto> salesRepresentativeList;
    
    @JsonProperty("checkout_surveys")
    private List<CheckoutSurvey> checkoutSurveyList;

	public List<String> getEmployeeIds() {
		if(employeeIds != null) {
			return employeeIds;
		}
		return new ArrayList<>();
	}

	public void setEmployeeIds(List<String> employeeIds) {
		this.employeeIds = employeeIds;
	}
	
	public List<TaxExemptReview> getTaxExemptReview() {
		return taxExemptReviewList;
	}

	public void setTaxExemptReview(List<TaxExemptReview> taxExemptReviewList) {
		this.taxExemptReviewList = taxExemptReviewList;
	}

}
