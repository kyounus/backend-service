package com.molekule.api.v1.commonframework.model.sendgrid;

import lombok.Data;

@Data
public class Product {
	private String orderProductName;
	private String orderProductSubCopy;
	private String orderProductDesc;
	private long orderProductQty;
	private String orderProductPrice;
	private String orderProductSubscriptionInfo;
	private String orderImageUrl;
	private boolean subscription;
	private String subscriptionHeader;
	private String subscriptionPrice;
}
