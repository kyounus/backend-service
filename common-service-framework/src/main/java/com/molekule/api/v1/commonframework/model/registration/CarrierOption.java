package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

@Data
public class CarrierOption {
	
	@Data
	public static class ReceivingHours {
		private String from;
		private String to;
		@Override
		public String toString() {
			return "ReceivingHours {from=" + from + ", to=" + to + "}";
		}
	}

	private CarrierType type;
	private String carrierName;
	private String carrierAccountNumber;
	private boolean defaultCarrier;
	private BillingAddresses billingAddress;
	private String billingAddressId;
	private boolean loadingDock;
	private boolean forkLift;
	private boolean deliveryAppointmentRequired;
	private ReceivingHours receivingHours;
	private String instructions;

}
