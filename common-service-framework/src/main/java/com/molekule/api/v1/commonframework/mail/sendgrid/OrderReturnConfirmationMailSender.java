package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("orderReturnConfirmationMailSender")
public class OrderReturnConfirmationMailSender extends TemplateMailRequestHelper {

	@Value("${orderReturnConfirmationTemplateId}")
	private String templateId;
	
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setRequestId(sendGridModel.getReturnNumber());
		dynamicTemplateData.setCustomerName(sendGridModel.getFirstName()+sendGridModel.getLastName());
		dynamicTemplateData.setReturnLabelUrl(sendGridModel.getReturnLabelUrl());
		return dynamicTemplateData;
	}
	
}
