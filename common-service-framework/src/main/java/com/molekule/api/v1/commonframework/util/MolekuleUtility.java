package com.molekule.api.v1.commonframework.util;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COMPANY_CATEGORIES;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;

@Service("molekuleUtility")
public class MolekuleUtility {

	private static Logger logger = LoggerFactory.getLogger(MolekuleUtility.class);

	private MolekuleUtility() {

	}

	/**
	 * parseJsonObject method is used to parse the JsonObject.
	 * 
	 * @param responseData
	 * @return JsonObbject
	 */
	public static JsonObject parseJsonObject(String responseData) {
		return JsonParser.parseString(responseData).getAsJsonObject();
	}

	public static String createUuidValue() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	/**
	 * getCategoryListDto method is used to convert the string json response to
	 * CategoryDto entity
	 * 
	 * @param categoryResponse
	 * 
	 * @return List<CategoryDto> - it contains list of category details.
	 */
	public static List<CategoryDto> getCategoryListDto(String categoryResponse) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(categoryResponse);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		JsonObject companyCategoriesObject = valueObject.has(COMPANY_CATEGORIES)? valueObject.get(COMPANY_CATEGORIES).getAsJsonObject(): new JsonObject();
		String response = companyCategoriesObject.has("companyCategory")? companyCategoriesObject.get("companyCategory").toString():"";
		ObjectMapper mapper = new ObjectMapper();
		List<CategoryDto> categoryDtoList = new ArrayList<>();
		try {
			categoryDtoList = Arrays.asList(mapper.readValue(response, CategoryDto[].class));
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException Occured at getCategoryListDto", e);
		}
		return categoryDtoList;
	}

	/**
	 * getSalesRepresentativeDtoList method is used to convert the string json
	 * response to SalesRespresentativeDto entity
	 * 
	 * @param salesRepResponse
	 * 
	 * @return List<CategoryDto> - it contains list of sales respresentative
	 *         details.
	 */
	public static List<SalesRepresentativeDto> getSalesRepresentativeDtoList(String salesRepResponse) {
		JsonObject jsonObject = MolekuleUtility.parseJsonObject(salesRepResponse);
		JsonObject valueObject = jsonObject.get(VALUE).getAsJsonObject();
		String response = valueObject.has("sales-reps")? valueObject.get("sales-reps").toString():"";
		ObjectMapper mapper = new ObjectMapper();
		List<SalesRepresentativeDto> salesRespresentativeDtoList = new ArrayList<>();
		try {
			salesRespresentativeDtoList = Arrays.asList(mapper.readValue(response, SalesRepresentativeDto[].class));
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException occured when getSalesRepresentativeDtoList", e);
		}
		return salesRespresentativeDtoList;
	}
	
	/**
	 * convertToEncodeUri method is used to convert as encode uri.
	 * @param customerBaseUrl
	 * @param value
	 * @return String
	 */
	public static String convertToEncodeUri(String value) {
		StringBuilder encodeString = new StringBuilder();
		try {
			encodeString = new StringBuilder(URLEncoder.encode(value, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error("UnsupportedEncodingException Occured", e);
		}
		return encodeString.toString();
	}
}
