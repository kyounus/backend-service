package com.molekule.api.v1.commonframework.dto.registration;

/**
 * The class Type is used to hold the id for the custom fields.
 */
public class Type {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Type [id=" + id + "]";
	}
	
	
	
}
