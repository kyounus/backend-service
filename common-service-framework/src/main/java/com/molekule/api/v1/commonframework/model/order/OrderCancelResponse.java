package com.molekule.api.v1.commonframework.model.order;

import lombok.Data;

@Data
public class OrderCancelResponse {
private String orderId;
private boolean cancelled;
}
