package com.molekule.api.v1.commonframework.model.mobileapp;

import java.util.List;

import lombok.Data;
@Data
public class Address {

	private String id;
	private String customer_id;
	private Region region;
	private long region_id;
	private String country_id;
	private List<String> street;
	private String telephone;
	private String postcode;
	private String city;
	private String firstname;
	private String lastname;
	private boolean default_shipping;
	private boolean default_billing;
	private List<CustomAttributes> custom_attributes;
	@Data
	public static class Region{
		private String region_code;
		private String region;
		private long region_id; 
	}
	@Data
	public static class CustomAttributes{
		private String attribute_code;
		private String value;
	}
}
