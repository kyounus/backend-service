package com.molekule.api.v1.commonframework.model.products;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.COUNTRY;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;

@Data
public class Price {

	@JsonProperty("id")
	private String id;

	@JsonProperty(VALUE)
	private TypedMoney value;

	@JsonProperty(COUNTRY)
	private String country;
	
	@JsonProperty("customerGroup")
	private CustomerGroup customerGroup;

	@JsonProperty("tiers")
	private List<PriceTier> tiers;
		
	@JsonProperty("discounted")
	private Discounted discounted;
	
	@Data
	public static class Discounted {
		@JsonProperty(VALUE)
		private TypedMoney value;
		@JsonProperty("discount")
		private Discount discount;
		
		@Data
		public static class Discount {
			@JsonProperty("typeId")
			private String typeId;
			@JsonProperty("id")
			private String id;
		}
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TypedMoney getValue() {
		return value;
	}

	public void setValue(TypedMoney value) {
		this.value = value;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public CustomerGroup getCustomerGroup() {
		return customerGroup;
	}

	public void setCustomerGroup(CustomerGroup customerGroup) {
		this.customerGroup = customerGroup;
	}

	public List<PriceTier> getTiers() {
		return tiers;
	}

	public void setTiers(List<PriceTier> tiers) {
		this.tiers = tiers;
	}

}
