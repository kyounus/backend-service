package com.molekule.api.v1.commonframework.dto.registration;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;

/**
 * The class AccountCustomObject is used to hold the data for full registration.
 */
@Data
public class AccountCustomObject {
	@JsonProperty(CONTAINER)
	private String container;
	
	@JsonProperty("key")
	private String key;
	
	@JsonProperty(VERSION)
	private int version;
	
	@JsonProperty(VALUE)
	private Value value;


	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}

	public String getContainer() {
		return container;
	}
	public void setContainer(String container) {
		this.container = container;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Value getValue() {
		return value;
	}
	public void setValue(Value value) {
		this.value = value;
	}
}
