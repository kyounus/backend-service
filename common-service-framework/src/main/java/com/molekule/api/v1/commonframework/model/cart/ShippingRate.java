package com.molekule.api.v1.commonframework.model.cart;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class ShippingRate {

	@JsonProperty("price")
    private TypedMoney price;

    @JsonProperty("freeAbove")
    private TypedMoney freeAbove;

    @JsonProperty("isMatching")
    private Boolean isMatching;

    @JsonProperty("tiers")
    private List<ShippingRatePriceTier> tiers;

	public TypedMoney getPrice() {
		return price;
	}

	public void setPrice(TypedMoney price) {
		this.price = price;
	}

	public TypedMoney getFreeAbove() {
		return freeAbove;
	}

	public void setFreeAbove(TypedMoney freeAbove) {
		this.freeAbove = freeAbove;
	}

	public Boolean getIsMatching() {
		return isMatching;
	}

	public void setIsMatching(Boolean isMatching) {
		this.isMatching = isMatching;
	}

	public List<ShippingRatePriceTier> getTiers() {
		return tiers;
	}

	public void setTiers(List<ShippingRatePriceTier> tiers) {
		this.tiers = tiers;
	}
    
    
}
