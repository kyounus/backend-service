package com.molekule.api.v1.commonframework.dto.cart;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CartCustomField {
	private Long version;
	private List<CustomAction> actions = null;
	@Data
	public static class CustomAction {
		
		@JsonProperty(ACTION)
		private String actionName;
		private String name;
		@JsonProperty("value")
		private Object valueObject;
		
		@Data
		public static class ProductValue {
			private String typeId;
			private String id;
		}
		
	}
}
