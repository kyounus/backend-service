package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;
@Component("orderPendingMailSender")
public class OrderPendingMailSender extends TemplateMailRequestHelper {
	
	@Value("${orderPendingTemplateId}")
	private String templateId;
	
	@Value("${paymentDocumentUrl}")
	private String paymentDocumentUrl;
	
	@Value("${orderDashboardUrl}")
	private String orderDashboardUrl;
	
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setCustomerFirstName(sendGridModel.getFirstName());
	    dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
	    dynamicTemplateData.setOrderUrl(orderDashboardUrl);
	    dynamicTemplateData.setPaymentDocumentUrl(paymentDocumentUrl);
        dynamicTemplateData.setProducts(sendGridModel.getProducts());
        dynamicTemplateData.setOrderSummarySubtotal(sendGridModel.getOrderSummarySubtotal());
        dynamicTemplateData.setOrderSummaryDiscount(sendGridModel.getOrderSummaryDiscount());
        dynamicTemplateData.setOrderSummaryTax(sendGridModel.getOrderSummaryTax());
        dynamicTemplateData.setOrderSummaryShipping(sendGridModel.getOrderSummaryShipping());
        dynamicTemplateData.setOrderSummaryHandling(sendGridModel.getOrderSummaryHandling());
        dynamicTemplateData.setOrderSummaryTotal(sendGridModel.getOrderSummaryTotal());
        dynamicTemplateData.setNonCreditCardPayment(sendGridModel.isNonCreditCardPayment());
        dynamicTemplateData.setPaymentTermsApplication(sendGridModel.isPaymentTermsApplication());
        dynamicTemplateData.setMoreFreightInformation(sendGridModel.isMoreFreightInformation());
        dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
        dynamicTemplateData.setSalesRepresentativePhoneNumber(getReplacedPhoneNumber(sendGridModel.getSalesRepPhoneNumber()));
        dynamicTemplateData.setSalesRepresentativeCalendlyLink(sendGridModel.getSalesRepCalendyLink());
        dynamicTemplateData.setOrderShippingAddressCompanyName(sendGridModel.getOrderShippingAddressCompanyName());
        dynamicTemplateData.setOrderShippingAddressContactFirstName(sendGridModel.getOrderShippingAddressCompanyName());
        dynamicTemplateData.setOrderShippingAddressContactLastName(sendGridModel.getOrderShippingAddressContactLastName());
        dynamicTemplateData.setOrderShippingAddressStreet1(sendGridModel.getOrderShippingAddressStreet1());
        dynamicTemplateData.setOrderShippingAddressStreet2(sendGridModel.getOrderShippingAddressStreet2());
        dynamicTemplateData.setOrderShippingAddressCity(sendGridModel.getOrderShippingAddressCity());
        dynamicTemplateData.setOrderShippingAddressState(sendGridModel.getOrderShippingAddressState());
        dynamicTemplateData.setOrderShippingAddressZip(sendGridModel.getOrderShippingAddressZip());
        if(sendGridModel.getOrderShippingAddressPhoneNumber()!=null) {
        dynamicTemplateData.setOrderShippingAddressPhoneNumber(getReplacedPhoneNumber(sendGridModel.getOrderShippingAddressPhoneNumber()));
        }
        dynamicTemplateData.setOrderBillingAddressFirstName(sendGridModel.getOrderBillingAddressFirstName());
        dynamicTemplateData.setOrderBillingAddressLastName(sendGridModel.getOrderBillingAddressLastName());
        dynamicTemplateData.setOrderBillingAddressStreet1(sendGridModel.getOrderBillingAddressStreet1());
        dynamicTemplateData.setOrderBillingAddressStreet2(sendGridModel.getOrderBillingAddressStreet2());
        dynamicTemplateData.setOrderBillingAddressCity(sendGridModel.getOrderBillingAddressCity());
        dynamicTemplateData.setOrderBillingAddressState(sendGridModel.getOrderBillingAddressState());
        dynamicTemplateData.setOrderBillingAddressZip(sendGridModel.getOrderBillingAddressZip());
        if(sendGridModel.getOrderBillingAddressPhoneNumber()!=null) {
        dynamicTemplateData.setOrderBillingAddressPhoneNumber(getReplacedPhoneNumber(sendGridModel.getOrderBillingAddressPhoneNumber()));
        }
        dynamicTemplateData.setOrderShippingMethod(sendGridModel.getOrderShippingMethod());
        dynamicTemplateData.setOrderPaymentMethod(sendGridModel.getOrderPaymentMethod());
		return dynamicTemplateData;
	}

	private String getReplacedPhoneNumber(String phoneNumber) {
		return phoneNumber.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
	}
}
