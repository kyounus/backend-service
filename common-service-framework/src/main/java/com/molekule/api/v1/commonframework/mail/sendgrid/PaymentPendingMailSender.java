package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("paymentPendingMailSender")
public class PaymentPendingMailSender extends TemplateMailRequestHelper{

	@Value("${paymentPendingTemplateId}")
	private String templateId;
	
	@Value("${paymentPendingDashBoardURL}")
	private String accountDashboardURL;
	
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	
	@Override
	public String templateId() {
		return templateId;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setAccountDashboardUrl(accountDashboardURL);
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		dynamicTemplateData.setSalesRepresentativeCalendlyLink(sendGridModel.getSalesRepCalendyLink());
		return dynamicTemplateData;
	}
}
