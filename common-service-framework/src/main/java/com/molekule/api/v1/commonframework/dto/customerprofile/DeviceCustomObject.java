package com.molekule.api.v1.commonframework.dto.customerprofile;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DeviceCustomObject {
	@JsonProperty(CONTAINER)
	private String container;
	
	@JsonProperty("key")
	private String key;
	
	@JsonProperty(VERSION)
	private long version;
	
	@JsonProperty(VALUE)
	private Value value;
	
	@Data
	public static class Value {
		private String deviceId;
		private String serialNumber;
		private String sku;
		private String channel;
		private boolean gift;
		private String retailSeller;
		private String customerId;
		private String registraionDate; 
		private String purchaseDate;
		private String comments;
		private String customerNumber;
	}
}
