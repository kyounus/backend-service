package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PaymentTypeServiceBean {

	private Long version;
	private List<Action> actions = null;

	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		private Type type;
		private Fields fields;
		private String name;
		private Object value;
		
		@Data
		public static class Type {
			private String id;
			private String typeId;
		}

		@Data
		public static class Fields {
			private String expMonth;
			private String expYear;
			private String last4;
			private String brand;
			private String charge;
			private String streetCheck;
			private String zipCheck;
			private String cvcCheck;
			private Boolean captured;
			private Boolean refunded;
			private String radarRisk;
			private String paymentView;
			private String customerView;
		}
	}
}
