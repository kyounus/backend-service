package com.molekule.api.v1.commonframework.model.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParcelMeasurements {

	@JsonProperty("heightInMillimeter")
	private Double heightInMillimeter;

	@JsonProperty("lengthInMillimeter")
	private Double lengthInMillimeter;

	@JsonProperty("widthInMillimeter")
	private Double widthInMillimeter;

	@JsonProperty("weightInGram")
	private Double weightInGram;

	public Double getHeightInMillimeter() {
		return heightInMillimeter;
	}

	public void setHeightInMillimeter(Double heightInMillimeter) {
		this.heightInMillimeter = heightInMillimeter;
	}

	public Double getLengthInMillimeter() {
		return lengthInMillimeter;
	}

	public void setLengthInMillimeter(Double lengthInMillimeter) {
		this.lengthInMillimeter = lengthInMillimeter;
	}

	public Double getWidthInMillimeter() {
		return widthInMillimeter;
	}

	public void setWidthInMillimeter(Double widthInMillimeter) {
		this.widthInMillimeter = widthInMillimeter;
	}

	public Double getWeightInGram() {
		return weightInGram;
	}

	public void setWeightInGram(Double weightInGram) {
		this.weightInGram = weightInGram;
	}

}
