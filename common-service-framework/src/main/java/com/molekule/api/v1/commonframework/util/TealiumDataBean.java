package com.molekule.api.v1.commonframework.util;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TealiumDataBean {

	@JsonProperty("tealium_event")
	private String tealiumEvent;
	
	@JsonProperty("event_category")
	private String eventCategory;
	
	@JsonProperty("event_action")
	private String eventAction;
	
	@JsonProperty("event_platform")
	private String eventPlatform;
	
	@JsonProperty("event_label")
	private String eventLabel;
	
	@JsonProperty("customer_email_preference")
	private boolean customerEmailPreference;
	
	@JsonProperty("auto_refill")
	private boolean[] autoRefill;
	
	@JsonProperty("event_value")
	private BigDecimal eventValue;
	
	@JsonProperty("cart_id")
	private String cartId;
	
	@JsonProperty("cc_last_4_digits")
	private String ccLast4Digits;
	
	@JsonProperty("customer_address_1_billing")
	private String customerAddress1Billing;
	
	@JsonProperty("customer_address_1_shipping")
	private String customerAddress1Shipping;
	
	@JsonProperty("customer_address_2_billing")
	private String customerAddress2billing;
	
	@JsonProperty("customer_address_2_shipping")
	private String customerAddress2Shipping;
	
	@JsonProperty("customer_city_billing")
	private String customerCityBilling;
	
	@JsonProperty("customer_city_shipping")
	private String customerCityShipping;
	
	@JsonProperty("customer_country_billing")
	private String customerCountryBilling;
	
	@JsonProperty("customer_country_code_billing")
	private String customerCountryCodeBilling;
	
	@JsonProperty("customer_country_code_shipping")
	private String customerCountryCodeShipping;
	
	@JsonProperty("customer_country_shipping")
	private String customerCountryShipping;
	
	@JsonProperty("customer_email")
	private String customerEmail;
	
	@JsonProperty("customer_first_name_billing")
	private String customerFirstNameBilling;
	
	@JsonProperty("customer_first_name_shipping")
	private String customerFirstNameShipping;
	
	@JsonProperty("customer_id")
	private String customerId;
	
	@JsonProperty("customer_last_name_billing")
	private String customerLastNameBilling;
	
	@JsonProperty("customer_last_name_shipping")
	private String customerLastNameShipping;
	
	@JsonProperty("customer_state_billing")
	private String customerStateBilling;
	
	@JsonProperty("customer_state_shipping")
	private String customerStateShipping;
	
	@JsonProperty("customer_type")
	private String customerType;
	
	@JsonProperty("customer_uid")
	private String customerUid;
	
	@JsonProperty("customer_zip_billing")
	private String customerZipBilling;
	
	@JsonProperty("customer_zip_shipping")
	private String customerZipShipping;
	
	@JsonProperty("email_preferences")
	private boolean emailPreferences;
	
	@JsonProperty("filter_frequency")
	private List<Integer> filterFrequency;
	
	@JsonProperty("filter_next_charge_amount")
	private List<String> filterNextChargeAmount;
	
	@JsonProperty("product_plan_details")
	private String filterPlanPrice;
	
	@JsonProperty("filter_plan_price")
	private List<String> filterPlanPriceList;
	
	@JsonProperty("free_refill_eligible")
	private boolean freeRefillEligible;
	
	@JsonProperty("filter_refill_end_date")
	private String filterRefillEndDate;
	
	@JsonProperty("filter_refill_end_dates")
	private List<String> filterRefillEndDateList;
	
	@JsonProperty("filter_refill_end_date_previous")
	private List<String> filterRefillEndDatePrevious;
	
	@JsonProperty("gift_purchase")
	private boolean giftPurchase;
	
	@JsonProperty("is_business_address")
	private boolean isBusinessAddress;
	
	@JsonProperty("next_shipment_date")
	private List<String> nextShipmentDate;
	
	@JsonProperty("offer_name")
	private String offerName;
	
	@JsonProperty("order_currency_code")
	private String orderCurrencyCode;
	
	@JsonProperty("order_grand_total")
	private BigDecimal orderGrandTotal;
	
	@JsonProperty("order_id")
	private String orderId;
	
	@JsonProperty("order_id_increment")
	private String orderIdIncrement;
	
	@JsonProperty("order_number")
	private String orderNumber;
	
	@JsonProperty("order_promo_amount")
	private BigDecimal orderPromoAmount;
	
	@JsonProperty("order_subtotal_after_promo")
	private BigDecimal orderSubtotalAfterPromo;
	
	@JsonProperty("order_promo_code")
	private String orderPromoCode;
	
	@JsonProperty("order_shipping_amount")
	private BigDecimal orderShippingAmount;
	
	@JsonProperty("order_tax_amount")
	private BigDecimal orderTaxAmount;
	
	@JsonProperty("order_handling_amount")
	private BigDecimal orderHandlingAmount;
	
	@JsonProperty("order_discount")
	private BigDecimal orderDiscount;
	
	@JsonProperty("order_shipping_type")
	private String orderShippingType;
	
	@JsonProperty("order_subtotal")
	private BigDecimal orderSubtotal;
	
	@JsonProperty("page_type")
	private String pageType;
	
	@JsonProperty("page_url")
	private String pageUrl;
	
	@JsonProperty("payment_method")
	private String paymentMethod;
	
	@JsonProperty("payment_on_file")
	private List<Boolean> paymentOnFile;
	
	@JsonProperty("order_payment_type")
	private String orderPaymentType;
	
	@JsonProperty("cart_product_id")
	private List<String> cartProductId;
	
	@JsonProperty("cart_product_list_price")
	private BigDecimal[] cartProductListPrice;
	
	@JsonProperty("cart_product_price")
	private BigDecimal[] cartProductPrice;
	
	@JsonProperty("cart_product_quantity")
	private List<Integer> cartProductQuantity;
	
	@JsonProperty("cart_product_sku")
	private List<String> cartProductSku;
	
	@JsonProperty("cart_total_items")
	private int cartTotalItems;
	
	@JsonProperty("cart_total_value")
	private BigDecimal cartTotalValue;
	
	@JsonProperty("cart_url")
	private List<String> cartUrl;
	
	@JsonProperty("product_brand")
	private List<String> productBrand;
	
	@JsonProperty("product_category")
	private List<String> productCategory;
	
	@JsonProperty("product_discount_amount")
	private BigDecimal[] productDiscountAmount;
	
	@JsonProperty("product_id")
	private List<String> productId;
	
	@JsonProperty("product_image_url")
	private List<String> productImageUrl;
	
	@JsonProperty("product_list_price")
	private BigDecimal[] productListPrice;
	
	@JsonProperty("product_name")
	private List<String> productName;
	
	@JsonProperty("product_price")
	private BigDecimal[] productPrice;
	
	@JsonProperty("product_promo_code")
	private List<String> productPromoCode;
	
	@JsonProperty("product_quantity")
	private List<Integer> productQuantity;
	
	@JsonProperty("product_sku")
	private List<String> productSku;
	
	@JsonProperty("product_review_stars")
	private List<String> productReview;
	
	@JsonProperty("product_in_stock")
	private List<String> productStock;
	
	@JsonProperty("product_renewal_date")
	private String productRenewalDate;
	
	@JsonProperty("session_id")
	private String sessionId;
	
	@JsonProperty("serial_number")
	private List<String> serialNumber;
	
	@JsonProperty("signature_required")
	private boolean signatureRequired;
	
	@JsonProperty("subscription_id")
	private List<String> subscriptionId;

	@JsonProperty("customer_phone")
	private String customerPhone;
	
	@JsonProperty("customer_first_name")
	private String customerFirstName;
	
	@JsonProperty("customer_last_name")
	private String customerLastName;
	
	@JsonProperty("customer_business_name")
	private String customerBusinessName;
	
	@JsonProperty("customer_industry_category")
	private String customerIndustryCategory;
	
	@JsonProperty("customer_marketing_opt_in")
	private String customerMarketingOptIn;
	
	@JsonProperty("customer_tos_opt_in")
	private String customerTosOptIn;
	
	@JsonProperty("customer_refill_opt_in")
	private String customerRefillOptIn;
	
	@JsonProperty("cart_discount")
	private List<String> cartDiscount;
	
	@JsonProperty("ab_test_group")
	private String abTestGroup;
	
	@JsonProperty("order_status")
    private String orderStatus;
   
    @JsonProperty("device_id")
    private String deviceId;
   
    @JsonProperty("device_name")
    private String deviceName;
    
    @JsonProperty("attempt_number")
	private String attemptNumber;

    @JsonProperty("error_message")
    private String errorMessage;
    
    @JsonProperty("gift_email_address")
    private String giftEmailAddress;
}
