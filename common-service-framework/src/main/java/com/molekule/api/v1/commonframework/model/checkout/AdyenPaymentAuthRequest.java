package com.molekule.api.v1.commonframework.model.checkout;

import lombok.Data;
@Data
public class AdyenPaymentAuthRequest {

	private Amount amount;
	private String reference;
	private PaymentMethod paymentMethod;
	private String shopperReference;
	private String shopperInteraction;
	private String recurringProcessingModel;
	private String returnUrl;
	private String merchantAccount;
	@Data
	public static class Amount{
		private String currency;
		private Long value;
	}
	@Data
	public static class PaymentMethod{
		private String storedPaymentMethodId;
	}
}
