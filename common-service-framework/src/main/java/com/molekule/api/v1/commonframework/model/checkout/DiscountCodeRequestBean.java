package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DiscountCodeRequestBean {

	private long version;
	private List<Action> actions;

	@Data
	public static class Action {
		@JsonProperty(ACTION)
		private String actionName;
		private String code;
		private Type type;
		private String name;
		private String value;
		private DiscountCode discountCode;
		private Fields fields;

		@Data
		public static class Fields {
			private String totalDiscountPrice;
		}

		@Data
		public static class Type {
			private String id;
			private String typeId;
		}

		@Data
		public static class DiscountCode {
			private String typeId;
			private String id;
		}

	}

}
