package com.molekule.api.v1.commonframework.model.checkout;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class PaymentIdUpdatePayment {
	
	private long version;
	private List<Action> actions;
	@Data
	public static class Action{
		
		@JsonProperty(ACTION)
		private String actionName;
		
		private Type type;
		private Fields fields;
		@Data
		public  static class Type{
			private String id;
			private String typeId;
		}
		@Data
		public static class Fields{
			private String paymentId;
		}
	}
	

}
