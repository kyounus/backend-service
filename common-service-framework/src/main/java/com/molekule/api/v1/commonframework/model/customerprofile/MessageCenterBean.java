package com.molekule.api.v1.commonframework.model.customerprofile;

import java.util.List;

import lombok.Data;

@Data
public class MessageCenterBean {
	
private List<MessageCenter> messageCenter;
	
	@Data
	public static class MessageCenter implements Comparable<MessageCenter>{
	private String orderId;
	private String orderNumber;
	private Boolean readFlag;
	private List<String> orderState;
	
	@Override
	public int compareTo(MessageCenter messageCenter) {
		return this.getOrderNumber().compareTo( messageCenter.getOrderNumber());
	}
	
	}
}
