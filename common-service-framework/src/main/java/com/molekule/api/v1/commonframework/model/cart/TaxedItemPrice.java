package com.molekule.api.v1.commonframework.model.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

public class TaxedItemPrice {

	@JsonProperty("totalNet")
	private TypedMoney totalNet;

	@JsonProperty("totalGross")
	private TypedMoney totalGross;

	public TypedMoney getTotalNet() {
		return totalNet;
	}

	public void setTotalNet(TypedMoney totalNet) {
		this.totalNet = totalNet;
	}

	public TypedMoney getTotalGross() {
		return totalGross;
	}

	public void setTotalGross(TypedMoney totalGross) {
		this.totalGross = totalGross;
	}

}
