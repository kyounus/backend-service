package com.molekule.api.v1.commonframework.model.customerprofile;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.dto.customerprofile.SubscriptionCustomObject.Value.TransactionObject;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;

import lombok.Data;

@Data
public class AutoRefillsResponseBean {
	private long limit;
	private long offset;
	private long count;
	private long total;
	@JsonProperty("results")
	private List<SubscriptionsObject> subscriptionsObject;
	@Data
	public static class SubscriptionsObject {
		private String subscriptionState;
		private String id;
		private String createdAt;
		private String lastModifiedAt;
		private String cartId;
		private String container;
		private String key;
		private String customerId;
		private List<LineItems> lineItemList;
		private int period;
		private AddressRequestBean shippingAddress;
		private String nextOrderDate;
		private boolean subscription;
		private String totalPrice;
		private String taxedPrice;
		private String paymentMethod;
		private CreditCard creditCard;
		private String customerName;
		private String customerEmail;
		private String channel;
		private String state;
		private String planName;
		private String planDescription;
		//failureReason
		private String status; 
		private String lastOrderDate;
		private String serialNumber;
		private List<TransactionObject> transactions;

		@Data 
		public static class LineItems {
			private String lineItemId;
			private String productId;
			private String name;
			private long quantity;
			private String slug;
			private String variantId;
			private String sku;
			private String price;
			private List<String> promoCodes;
		}
	}
}
