package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;
@Data
public class SubscriptionCustomer {

	private String id;
	private String customer_id;
	private Device device;
	private String next_order;
	
    @Data
    public static class Device{
    	private String serial_number;
    	private String sku;
    	private String customer_id;
    }
    private Payment payment;
    @Data
    public static class Payment{
    	private String status;
    	
    }
    
    private String shipping_address;
    private String status;
    private SubscriptionPlan subscription_plan;
    @Data
    public static class SubscriptionPlan{
    	private String default_promo_ids;
    	private String title;
    	private String short_description;
    	private String long_description;
    	private String more_info;
    	private String frequency_units;
    	private String frequency;
    	private String trigger_sku;
    	private String sort_order;
    	private String duration;
    	private String status;
    	private String visible;
    	private String created_at;
    	private String updated_at;
    	private int number_of_free_shipments;
    	private String device_sku;
    	private boolean payment_required_for_free;
    	private String identifier;
    	private long  plan_price;
    
    }
}
