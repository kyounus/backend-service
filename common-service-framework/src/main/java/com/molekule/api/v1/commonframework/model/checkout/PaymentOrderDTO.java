package com.molekule.api.v1.commonframework.model.checkout;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PaymentOrderDTO {
	@ApiModelProperty(required = true)
	private String cartId;
	@ApiModelProperty(required = true)
	private String setupPaymentIntentId;
	@ApiModelProperty(required = true)
	private String amount;
	@ApiModelProperty(required = true)
	private String currency;
	@ApiModelProperty(required = true)
	private String cardNumber;
	@ApiModelProperty(required = true)
	private String expMonth;
	@ApiModelProperty(required = true)
	private String expYear;
	@ApiModelProperty(required = true)
	private String cvc;
	@ApiModelProperty
	private String customerId;
	@ApiModelProperty
	private String paymentType;
	private AddressRequestBean billingAddress;
	private String shopperReference;
	private String encryptedCardNumber;
    private String encryptedExpiryMonth;
	private String encryptedExpiryYear;
	private String encryptedSecurityCode;
}
