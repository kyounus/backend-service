package com.molekule.api.v1.commonframework.util;

public enum CustomerStatusEnum {
	LEAD,
	REGISTERED,
	ACTIVE
}
