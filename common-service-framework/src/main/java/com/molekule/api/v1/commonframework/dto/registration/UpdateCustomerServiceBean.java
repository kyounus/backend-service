package com.molekule.api.v1.commonframework.dto.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.*;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;

import lombok.Data;

@Data
public class UpdateCustomerServiceBean {

	private Long version;
	private List<Action> actions = null;
	
	@Data
	public static class Action {

		@JsonProperty(ACTION)
		private String actionName;
		@JsonProperty("address")
		private AddressRequestBean address;
		private Type type;
		private Fields fields;
		private String addressId;
		private String email;
		private String firstName;
		private String lastName;
		private String name;
		private Object value;
		
		@Data
		public static class Type {
			private String id;
			private String typeId;
		}
		
		@Data
		public static class Fields {
			private Boolean isBusiness;
		}
	}
}
