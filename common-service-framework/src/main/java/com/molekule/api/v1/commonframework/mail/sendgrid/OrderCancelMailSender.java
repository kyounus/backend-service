package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("orderCancelMailSender")
public class OrderCancelMailSender extends TemplateMailRequestHelper {

	@Value("${orderCancelTemplateId}")
	private String templateId;

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(
			SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderDate(sendGridModel.getOrderDate());
		dynamicTemplateData.setOrderNumber(sendGridModel.getOrderNumber());
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		if(sendGridModel.getSalesRepPhoneNumber() != null)
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		return dynamicTemplateData; 
	}

}
