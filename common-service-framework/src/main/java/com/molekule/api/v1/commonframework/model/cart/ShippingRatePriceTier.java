package com.molekule.api.v1.commonframework.model.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShippingRatePriceTier {

	@JsonProperty("type")
    private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
