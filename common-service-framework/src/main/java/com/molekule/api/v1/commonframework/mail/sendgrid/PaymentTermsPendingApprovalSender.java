package com.molekule.api.v1.commonframework.mail.sendgrid;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;
/**
 * PaymentTermsApprovalSender is a helper class.it will Build SendGrid
 * Dynamic data
 * 
 * @version 1.0
 */

@Component("paymentTermsPendingApprovalSender")
public class PaymentTermsPendingApprovalSender extends TemplateMailRequestHelper {

	@Value("${paymentTermsApprovalTemplateId}")
	private String templateId;
	
	@Value("${paymentTermsCustomerURL}")
	private String customerStatusURL;
	
	@Value("${paymentTermsCustomerCompanyURL}")
	private String customerCompanyStatusURL;

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	@Override
	public String templateId() {
		return templateId;
	}

	@Override
	public SendGridRequestBean.Personalizations getPersonalizations(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations personalizations = new SendGridRequestBean.Personalizations();
		SendGridRequestBean.Personalizations.To to = new SendGridRequestBean.Personalizations.To();
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = getDynamicTemplateData(
				sendGridModel);
		to.setEmail(sendGridModel.getSalesRepEmail());
		to.setName(sendGridModel.getSalesRepFirstName());
		List<SendGridRequestBean.Personalizations.To> toList = new ArrayList<>();
		toList.add(to);
		personalizations.setTo(toList);
		personalizations.setDynamicTemplateData(dynamicTemplateData);
		return personalizations;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel) {
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setOrderDate(sendGridModel.getRequestedDate());
		dynamicTemplateData.setCustomerFirstName(sendGridModel.getFirstName());
		dynamicTemplateData.setCustomerLastName(sendGridModel.getLastName());
		dynamicTemplateData.setCustomerCompanyName(sendGridModel.getCustomerCompanyName());
		dynamicTemplateData.setCustomerCompanyIndustry(sendGridModel.getCompanyIndustry());
		dynamicTemplateData.setCustomerEmail(sendGridModel.getEmail());
		dynamicTemplateData.setCustomerPhoneNumber(sendGridModel.getPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		dynamicTemplateData.setCustomerRecordUrl(customerStatusURL+sendGridModel.getCustomerId()+"/general");
		dynamicTemplateData.setCustomerCompanyRecordUrl(customerCompanyStatusURL+sendGridModel.getAccountId());
		dynamicTemplateData.setSalesRepresentativeFirstName(sendGridModel.getSalesRepFirstName());
		dynamicTemplateData.setSalesRepresentativeLastName(sendGridModel.getSalesRepLastName());
		dynamicTemplateData.setSalesRepresentativeEmail(sendGridModel.getSalesRepEmail());
		dynamicTemplateData.setSalesRepresentativePhoneNumber(sendGridModel.getSalesRepPhoneNumber().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		dynamicTemplateData.setSalesRepresentativeCalendlyLink(sendGridModel.getSalesRepCalendyLink());
		return dynamicTemplateData;
	}
}
