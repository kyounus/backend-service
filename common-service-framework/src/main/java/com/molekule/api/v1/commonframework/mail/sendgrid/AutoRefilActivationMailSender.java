package com.molekule.api.v1.commonframework.mail.sendgrid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@Component("autoRefilActivationMailSender")
public class AutoRefilActivationMailSender extends TemplateMailRequestHelper {

	@Value("${autoRefilActivationTemplateId}")
	private String templateId;
	
	@Override
	public String templateId() {
		return templateId ;
	}
	
	@Override
	public SendGridRequestBean.Personalizations.DynamicTemplateData getDynamicTemplateData(SendGridModel sendGridModel){
		SendGridRequestBean.Personalizations.DynamicTemplateData dynamicTemplateData = new SendGridRequestBean.Personalizations.DynamicTemplateData();
		dynamicTemplateData.setSubscriptionPlanName(sendGridModel.getSubscriptionPlanName());
		dynamicTemplateData.setSubscriptionRenewalDate(sendGridModel.getSubscriptionRenewalDate());
		dynamicTemplateData.setSubscriptionDeviceSerial(sendGridModel.getSubscriptionDeviceSerial());
		dynamicTemplateData.setSubscriptionPrice(sendGridModel.getSubscriptionPrice());
		dynamicTemplateData.setCustomerFirstName(sendGridModel.getFirstName());
		dynamicTemplateData.setCustomerLastName(sendGridModel.getLastName());
		dynamicTemplateData.setCustomerStreetOne(sendGridModel.getOrderShippingAddressStreet1());
		dynamicTemplateData.setCustomerStreetTwo(sendGridModel.getOrderShippingAddressStreet2());
		dynamicTemplateData.setCustomerCity(sendGridModel.getOrderShippingAddressCity());
		dynamicTemplateData.setCustomerState(sendGridModel.getOrderShippingAddressState());
		dynamicTemplateData.setCustomerZip(sendGridModel.getOrderShippingAddressZip());
		dynamicTemplateData.setCustomerCountry(sendGridModel.getOrderShippingAddressCountry());
		dynamicTemplateData.setPromo(true);
		return dynamicTemplateData;
	}

}
