package com.molekule.api.v1.commonframework.dto.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.EMAIL;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIRST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.LAST_NAME;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.PHONE;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * SalesRepresentativeDto class is used to hold the sales represntative details.
 *
 */
@Data
public class SalesRepresentativeDto {

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty(PHONE)
	private String phone;

	@JsonProperty(EMAIL)
	private String email;
	
	@JsonProperty(FIRST_NAME)
    private String firstName;
   
    @JsonProperty(LAST_NAME)
    private String lastName;
   
    @JsonProperty("calendlyLink")
    private String calendlyLink;
}
