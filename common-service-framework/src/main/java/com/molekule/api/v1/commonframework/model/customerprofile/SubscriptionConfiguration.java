package com.molekule.api.v1.commonframework.model.customerprofile;

import lombok.Data;

@Data
public class SubscriptionConfiguration {

	private String blackoutStartTime;
	private String blackoutEndTime;
	private int firstPaymentRetryDays;
	private int secondPaymentRetryDays;
	private int thirdPaymentRetryDays;
	private String shippingMethodName;
	private boolean shippingPriceOveride;
}
