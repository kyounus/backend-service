package com.molekule.api.v1.commonframework.dto.cart;

import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CartRequestBean {

	private String productId;
	private int quantity;
	private String lineItemId;
	private String action;
	private long variantId;
	private String poNumber;
	private boolean subscriptionEnabled;
	private AddressRequestBean shippingAddress;
	private QuoteCustomerDetail quoteCustomerDetail;
	private ShippingMethod shippingMethod;
	private String customerId;
	private String carrierOptionId;
	@ApiModelProperty(example = "FILTER_ONE_TIME or FILTER_AUTO_REFILL")
	private String filterBuyOption;
	private boolean gift;
    private String recipientsEmail;
    private String giftMessage;
    private String variantSKU;
    private String channel;
    private int period;
    private String email;
    private String orderTag;
    private String warrantyComment;
    private String previousOrderNumber;
    private String country;
	@Data
	public static class ShippingMethod {
		private String name;
		private String cost;
	}

	@Data
	public static class QuoteCustomerDetail {
		private String firstName;
		private String lastName;
		private String email;
		private String phone;
		private String comments;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public long getVariantId() {
		return variantId;
	}

	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getLineItemId() {
		return lineItemId;
	}

	public void setLineItemId(String lineItemId) {
		this.lineItemId = lineItemId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public AddressRequestBean getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(AddressRequestBean shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
	
}
