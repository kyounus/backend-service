package com.molekule.api.v1.commonframework.model.products;

import java.util.List;
import java.util.Map;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryReference {

	@JsonProperty("id")
	private String id;

	@JsonProperty(VERSION)
	private Long version;

	@JsonProperty("name")
	private Map<String, String> name;

	@JsonProperty("slug")
	private Map<String, String> slug;

	@JsonProperty("description")
	private Map<String, String> description;

	@JsonProperty("ancestors")
	private List<CategoryReference> ancestors;

	@JsonProperty("parent")
	private CategoryReference parent;

	@JsonProperty("orderHint")
	private String orderHint;

	@JsonProperty("externalId")
	private String externalId;

	@JsonProperty("metaTitle")
	private Map<String, String> metaTitle;

	@JsonProperty("metaDescription")
	private Map<String, String> metaDescription;

	@JsonProperty("metaKeywords")
	private Map<String, String> metaKeywords;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Map<String, String> getName() {
		return name;
	}

	public void setName(Map<String, String> name) {
		this.name = name;
	}

	public Map<String, String> getSlug() {
		return slug;
	}

	public void setSlug(Map<String, String> slug) {
		this.slug = slug;
	}

	public Map<String, String> getDescription() {
		return description;
	}

	public void setDescription(Map<String, String> description) {
		this.description = description;
	}

	public List<CategoryReference> getAncestors() {
		return ancestors;
	}

	public void setAncestors(List<CategoryReference> ancestors) {
		this.ancestors = ancestors;
	}

	public CategoryReference getParent() {
		return parent;
	}

	public void setParent(CategoryReference parent) {
		this.parent = parent;
	}

	public String getOrderHint() {
		return orderHint;
	}

	public void setOrderHint(String orderHint) {
		this.orderHint = orderHint;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Map<String, String> getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(Map<String, String> metaTitle) {
		this.metaTitle = metaTitle;
	}

	public Map<String, String> getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(Map<String, String> metaDescription) {
		this.metaDescription = metaDescription;
	}

	public Map<String, String> getMetaKeywords() {
		return metaKeywords;
	}

	public void setMetaKeywords(Map<String, String> metaKeywords) {
		this.metaKeywords = metaKeywords;
	}
}
