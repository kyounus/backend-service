package com.molekule.api.v1.commonframework.model.registration;

import io.swagger.annotations.ApiModel;

/**
 * The class PaymentTermsPublicResponseBean is used to hold the response of payment option public type.
 */
@ApiModel(description = "Payment Terms on Public Type Api Response Model")
public class PaymentTermsPublicResponseBean {

}
