package com.molekule.api.v1.commonframework.model.products;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CENT_AMOUNT;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CURRENCY_CODE;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TypedMoney {

	@JsonProperty("type")
	private String type;

	@JsonProperty("fractionDigits")
	private Integer fractionDigits;

	@JsonProperty(CENT_AMOUNT)
	private Long centAmount;

	@JsonProperty(CURRENCY_CODE)
	private String currencyCode;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getFractionDigits() {
		return fractionDigits;
	}

	public void setFractionDigits(Integer fractionDigits) {
		this.fractionDigits = fractionDigits;
	}

	public Long getCentAmount() {
		return centAmount;
	}

	public void setCentAmount(Long centAmount) {
		this.centAmount = centAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

}
