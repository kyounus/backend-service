package com.molekule.api.v1.commonframework.util;

public enum CustomerStoreEnum {

	MLK_US("US","USD", "MLK_US"),
	MLK_UK("GB","GBP", "MLK_UK"),
	MLK_EU("DE","EUR", "MLK_EU");
	
	private String countryCode;
	private String currency;
	private String store;
	
	private CustomerStoreEnum(String countryCode, String currency, String store){
        this.countryCode = countryCode;
        this.currency = currency;
        this.store = store;
    }

	public String getCountryCode() {
		return countryCode;
	}

	public String getCurrency() {
		return currency;
	}
	
	public String getStore() {
		return store;
	}
	
}
