package com.molekule.api.v1.commonframework.dto.order;

import lombok.Data;

@Data
public class LineItems {
	private String lineItemId; 
	private Integer quantityToRefund;

}
