package com.molekule.api.v1.commonframework.dto.customerprofile;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOMER;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.NEXT_ORDER_DATE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CONTAINER;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CHANNEL;
@Data
public class SubscriptionCustomObject {
	@JsonProperty(CONTAINER)
	private String container;
	
	@JsonProperty("key")
	private String key;
	
	@JsonProperty(VERSION)
	private long version;
	
	@JsonProperty(VALUE)
	private Value value;
	
	@Data
	public static class Value {
		@JsonProperty("cart")
		private Cart cart;
		
		@JsonProperty(CUSTOMER)
		private Customer customer;
		
		@JsonProperty("product")
		private Product product;

		@JsonProperty(CHANNEL)
		private String channel;
		
		@JsonProperty(NEXT_ORDER_DATE)
		private String nextOrderDate;
		
		@JsonProperty("lastOrderDate")
		private String lastOrderDate;
		
		@JsonProperty("subscription")
		private boolean subscription;
		
		@JsonProperty("state")
		private String state;
		
		@JsonProperty("transactions")
		private List<TransactionObject> transactions;
		
		@JsonProperty("renewalReference")
		private String renewalReference;
		
		@Data
		public static class Cart {
			@JsonProperty("typeId")
			private String typeId;
			@JsonProperty("id")
			private String id;
		}
		
		@Data
		public static class Customer {
			@JsonProperty("typeId")
			private String typeId;
			@JsonProperty("id")
			private String id;
		}
		
		@Data
		public static class TransactionObject {
			@JsonProperty("type")
			private String type;
			@JsonProperty("messages")
			private String message;
		}
		
		@Data
		public static class Product {
			@JsonProperty("name")
			private Name name;
			@JsonProperty("description")
			private Description description;
			
			@Data
			public static class Name {
				@JsonProperty("en-US")
				private String en;
			}
			@Data
			public static class Description {
				@JsonProperty("en-US")
				private String en;
			}
		}
		
	}
}
