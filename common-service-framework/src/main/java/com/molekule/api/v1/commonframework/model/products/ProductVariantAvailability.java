package com.molekule.api.v1.commonframework.model.products;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductVariantAvailability {

	@JsonProperty("isOnStock")
	private Boolean isOnStock;

	@JsonProperty("availableQuantity")
	private Long availableQuantity;

	public Boolean getIsOnStock() {
		return isOnStock;
	}

	public void setIsOnStock(Boolean isOnStock) {
		this.isOnStock = isOnStock;
	}

	public Long getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Long availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

}
