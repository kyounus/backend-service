package com.molekule.api.v1.commonframework.model.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrackingData {

	@JsonProperty("trackingId")
	private String trackingId;

	@JsonProperty("carrier")
	private String carrier;

	@JsonProperty("provider")
	private String provider;

	@JsonProperty("providerTransaction")
	private String providerTransaction;

	@JsonProperty("isReturn")
	private Boolean isReturn;

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getProviderTransaction() {
		return providerTransaction;
	}

	public void setProviderTransaction(String providerTransaction) {
		this.providerTransaction = providerTransaction;
	}

	public Boolean getIsReturn() {
		return isReturn;
	}

	public void setIsReturn(Boolean isReturn) {
		this.isReturn = isReturn;
	}

}
