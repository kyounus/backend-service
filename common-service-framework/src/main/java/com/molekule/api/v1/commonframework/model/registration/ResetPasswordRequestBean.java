package com.molekule.api.v1.commonframework.model.registration;

import lombok.Data;

@Data
public class ResetPasswordRequestBean {
	private String emailId;
	private String confirmationCode;
	private String password;
}
