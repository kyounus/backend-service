package com.molekule.api.v1.commonframework.model.mobileapp;

import lombok.Data;

@Data
public class Subscription {

	private String id;
	private String customer_id;
	private String status;
	private String next_order;
	private String last_order;
	private String created_at;
	private String updated_at;
	private String error_message;
    private Address shipping_address;
    private SubscriptionPlan subscription_plan;
    @Data
    public static class SubscriptionPlan{
    	private String id;
    	private String title;
    	private String short_description;
    	private String long_description;
    	private String more_info;
    	private String frequency_units;
    	private String frequency;
    	private String trigger_sku;
    	private String sort_order;
    	private String duration;
    	private String status;
    	private String visible;
    	private String created_at;
    	private String updated_at;
    	private String default_promo_ids;
    	private int number_of_free_shipments;
    	private String device_sku;
    	private boolean payment_required_for_free;
    	private String identifier;
    	private long  plan_price;
    	
    }
    private Device device;
    @Data
    public static class Device{
    	private String devicemanager_id;
    	private String entity_id;
    	private String serial_number;
    	private String sku;
    	private String purchase_date;
    	private String customer_id;
    	private String created_at;
    	private String updated_at;
    	private String associated_product_name;
    }
    
    private Payment payment;
    @Data
    public static class Payment{
    	private int id;
    	private String stripe_customer_id;
    	private String payment_id;
    	private String billing_address;
    	private String valid;
    	private String payment_code;
    	private String expiration_date;
    	private String created_at;
    	private String updated_at;
    	
    }
    private String promos[];
    private String coupon_codes[];
    private String cancel_reason;
    private String renewal_date;
    private String week_of_string;
    
   }































