package com.molekule.api.v1.commonframework.model.aurora;

import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;

import lombok.Data;

@Data
public class CategoryObjectBean {
	private CategoryDto category;
	private SalesRepresentativeDto salesRep;

}
