package com.molekule.api.v1.commonframework.dto.registration;

/**
 * The class Custom is used to create the custom files.
 */
public class Custom {

	private Type type;
	private Fields fields;
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Fields getFields() {
		return fields;
	}
	public void setFields(Fields fields) {
		this.fields = fields;
	}
	@Override
	public String toString() {
		return "Custom [type=" + type + ", fields=" + fields + "]";
	}
	
}
