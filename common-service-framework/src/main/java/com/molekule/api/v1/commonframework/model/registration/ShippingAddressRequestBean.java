package com.molekule.api.v1.commonframework.model.registration;

/**
 * The class ShippingAddressRequestBean is used to hold the request of shipping address.
 */
public class ShippingAddressRequestBean {
	private String accountId;
	private String customerId;
	private ShippingAddress shippingAddress;
	private boolean preferredShippingAddress;

	private boolean ownCarrier;

	private MolekuleCarrier molekuleCarrier;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public boolean isPreferredShippingAddress() {
		return preferredShippingAddress;
	}

	public void setPreferredShippingAddress(boolean preferredShippingAddress) {
		this.preferredShippingAddress = preferredShippingAddress;
	}

	public MolekuleCarrier getMolekuleCarrier() {
		return molekuleCarrier;
	}

	public void setMolekuleCarrier(MolekuleCarrier molekuleCarrier) {
		this.molekuleCarrier = molekuleCarrier;
	}

	public boolean isOwnCarrier() {
		return ownCarrier;
	}

	public void setOwnCarrier(boolean isOwnCarrier) {
		this.ownCarrier = isOwnCarrier;
	}

}
