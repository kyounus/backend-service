package com.molekule.api.v1.commonframework.model.customerprofile;

import java.util.List;

import com.molekule.api.v1.commonframework.model.checkout.Payment.AmountPlanned;
import com.molekule.api.v1.commonframework.model.checkout.Payment.PaymentMethodInfo;
import com.molekule.api.v1.commonframework.model.checkout.Payment.Transaction;

import lombok.Data;

@Data
public class TermsPayment {

	private AmountPlanned amountPlanned;
	private PaymentMethodInfo paymentMethodInfo;
	private List<Transaction> transactions;

}
