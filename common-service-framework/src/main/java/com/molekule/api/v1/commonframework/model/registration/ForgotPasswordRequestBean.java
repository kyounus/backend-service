package com.molekule.api.v1.commonframework.model.registration;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ForgotPasswordRequestBean {

	private String email;
	
	@ApiModelProperty(example = "forgotPassword or createPassword")
	private String passwordType;
	
}
