package com.molekule.api.v1.commonframework.util;

public enum StoreLocationType {
	MLK_US, MLK_CA
}
