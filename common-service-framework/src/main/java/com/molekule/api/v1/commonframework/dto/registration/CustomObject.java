package com.molekule.api.v1.commonframework.dto.registration;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VERSION;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.FIELDS;
import lombok.Data;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.ACTION;

@Data
public class CustomObject {
	@JsonProperty(VERSION)
	private int version;
	@JsonProperty("actions")
	private List<Actions> actions;
	
	@Data
	public static class Actions {
		@JsonProperty(ACTION)
		private String action;
		@JsonProperty("type")
		private Type type;
		@JsonProperty(FIELDS)
		private Fields fields;
	}
}
