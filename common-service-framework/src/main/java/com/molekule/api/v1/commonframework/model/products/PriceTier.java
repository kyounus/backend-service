package com.molekule.api.v1.commonframework.model.products;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.molekule.api.v1.commonframework.util.MolekuleConstant.VALUE;
public class PriceTier {

	@JsonProperty("minimumQuantity")
	private Long minimumQuantity;

	@JsonProperty(VALUE)
	private TypedMoney value;

	public Long getMinimumQuantity() {
		return minimumQuantity;
	}

	public void setMinimumQuantity(Long minimumQuantity) {
		this.minimumQuantity = minimumQuantity;
	}

	public TypedMoney getValue() {
		return value;
	}

	public void setValue(TypedMoney value) {
		this.value = value;
	}

}
