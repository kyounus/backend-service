package com.molekule.api.v1.commonframework.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * The class ProfileInfo is used to get properties of the application.
 */
@Component
@Profile(value = "dev")
public class ProfileInfo implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(ProfileInfo.class);
	@Autowired
	Environment env;

	@Override
	public void run(String... args) throws Exception {
		String environmentDetails = String.join(", ", env.getActiveProfiles());
		logger.info("Profile Active: {}", environmentDetails);
	}
}
