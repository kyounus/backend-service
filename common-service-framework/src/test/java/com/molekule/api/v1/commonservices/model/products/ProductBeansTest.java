package com.molekule.api.v1.commonservices.model.products;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.products.Attribute;
import com.molekule.api.v1.commonframework.model.products.AttributeDefinition;
import com.molekule.api.v1.commonframework.model.products.CategoryReference;
import com.molekule.api.v1.commonframework.model.products.CustomerGroup;
import com.molekule.api.v1.commonframework.model.products.Price;
import com.molekule.api.v1.commonframework.model.products.PriceTier;
import com.molekule.api.v1.commonframework.model.products.ProductCatalogData;
import com.molekule.api.v1.commonframework.model.products.ProductData;
import com.molekule.api.v1.commonframework.model.products.ProductTypeReference;
import com.molekule.api.v1.commonframework.model.products.ProductVariant;
import com.molekule.api.v1.commonframework.model.products.ProductVariantAvailability;
import com.molekule.api.v1.commonframework.model.products.TypedMoney;

@ContextConfiguration
public class ProductBeansTest {

	@Test
	public void getterAndSetterProductData() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductData.class);
	}

	@Test
	public void getterAndSetterCustomerGroup() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomerGroup.class);
	}

	@Test
	public void getterAndSetterCategoryReference() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CategoryReference.class);
	}

	@Test
	public void getterAndSetterProductCatalogData() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductCatalogData.class);
	}

	@Test
	public void getterAndSetterProductVariant() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductVariant.class);
	}

	@Test
	public void getterAndSetterProductTypeReference() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductTypeReference.class);
	}

	@Test
	public void getterAndSetterPrice() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Price.class);
	}

	@Test
	public void getterAndSetterTypedMoney() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TypedMoney.class);
	}

	@Test
	public void getterAndSetterAttributeDefinition() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AttributeDefinition.class);
	}

	@Test
	public void getterAndSetterAttribute() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Attribute.class);
	}

	@Test
	public void getterAndSetterPriceTier() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PriceTier.class);
	}

	@Test
	public void getterAndSetterProductVariantAvailability() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductVariantAvailability.class);
	}

}