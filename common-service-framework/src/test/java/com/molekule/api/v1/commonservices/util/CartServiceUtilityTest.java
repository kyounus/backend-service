package com.molekule.api.v1.commonservices.util;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean;
import com.molekule.api.v1.commonframework.util.CartServiceUtility;
import com.molekule.api.v1.commonframework.util.CartServiceUtility.CartActions;

public class CartServiceUtilityTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Mock
	CartRequestBean cartRequestBean;

	@Test
	public void addLineItemTest() {
		CartRequestBean cartRequestBean = new CartRequestBean();
		cartRequestBean.setAction(CartActions.ADD_LINE_ITEM.name());
		UpdateCartServiceBean updateCartServiceBean = CartServiceUtility.getUpdateCartServiceBean(cartRequestBean,
				1l, 1l);
		Assert.assertEquals("addLineItem", updateCartServiceBean.getActions().get(0).getActionName());
	}

	@Test
	public void removeLineItemTest() {
		CartRequestBean cartRequestBean = new CartRequestBean();
		cartRequestBean.setAction(CartActions.REMOVE_LINE_ITEM.name());
		UpdateCartServiceBean updateCartServiceBean = CartServiceUtility.getUpdateCartServiceBean(cartRequestBean,
				1l, 1l);
		Assert.assertEquals("removeLineItem", updateCartServiceBean.getActions().get(0).getActionName());
	}

	@Test
	public void changeLineItemQuantityTest() {
		CartRequestBean updateCartRequestBean = new CartRequestBean();
		updateCartRequestBean.setAction(CartActions.CHANGE_LINE_ITEM_QTY.name());
		UpdateCartServiceBean updateCartServiceBean = CartServiceUtility.getUpdateCartServiceBean(updateCartRequestBean,
				1l, 1l);
		Assert.assertEquals("changeLineItemQuantity", updateCartServiceBean.getActions().get(0).getActionName());
	}

}
