package com.molekule.api.v1.commonservices.service.checkout;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;

@ContextConfiguration
public class ServiceCheckoutBeanTest {

	@Test
	public void taxService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CheckoutHelperService.class);
	}

}