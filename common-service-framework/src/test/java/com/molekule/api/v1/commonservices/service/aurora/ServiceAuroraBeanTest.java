package com.molekule.api.v1.commonservices.service.aurora;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.service.aurora.AuroraHelperService;

@ContextConfiguration
public class ServiceAuroraBeanTest {

	@Test
	public void auroraHelperService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AuroraHelperService.class);
	}

}