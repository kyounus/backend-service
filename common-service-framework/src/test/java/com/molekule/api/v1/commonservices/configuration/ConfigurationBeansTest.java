package com.molekule.api.v1.commonservices.configuration;

import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.meanbean.test.BeanTester;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.configuration.CommonFrameworkConfiguration;
import com.molekule.api.v1.commonframework.configuration.ProfileInfo;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("myProfile")
public class ConfigurationBeansTest {

	@InjectMocks
	ProfileInfo profileInfo;

	@Mock
	Environment env;

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Before
	public void init() {
		env = Mockito.mock(Environment.class);
		String profiles[] = { "dev" };

		when(env.getActiveProfiles()).thenReturn(profiles);

	}

	@Mock
	Logger logger;

	//@Test
	public void getterAndSetterAmazonS3Config() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CTEnvProperties.class);
	}

	@Test
	public void getterAndSetterCTEnvProperties() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CommonFrameworkConfiguration.class);
	}

	@Test
	public void getterAndSetterMolekuleBeanConfiguration() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProfileInfo.class);
	}
}