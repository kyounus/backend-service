package com.molekule.api.v1.commonservices.service.cart;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.service.cart.TaxService;

@ContextConfiguration
public class ServiceCartBeanTest {

	@Test
	public void taxService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxService.class);
	}

}