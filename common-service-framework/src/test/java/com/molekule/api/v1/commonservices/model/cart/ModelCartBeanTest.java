package com.molekule.api.v1.commonservices.model.cart;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.cart.AvalaraRequestBean;
import com.molekule.api.v1.commonframework.model.cart.CustomLineItem;
import com.molekule.api.v1.commonframework.model.cart.DeliveryItem;
import com.molekule.api.v1.commonframework.model.cart.DiscountCodeInfo;
import com.molekule.api.v1.commonframework.model.cart.DiscountedLineItemPortion;
import com.molekule.api.v1.commonframework.model.cart.DiscountedLineItemPrice;
import com.molekule.api.v1.commonframework.model.cart.DiscountedLineItemPriceForQuantity;
import com.molekule.api.v1.commonframework.model.cart.ItemState;
import com.molekule.api.v1.commonframework.model.cart.ParcelMeasurements;
import com.molekule.api.v1.commonframework.model.cart.PaymentInfo;
import com.molekule.api.v1.commonframework.model.cart.ShippingInfo;
import com.molekule.api.v1.commonframework.model.cart.ShippingMethodRequest;
import com.molekule.api.v1.commonframework.model.cart.ShippingRate;
import com.molekule.api.v1.commonframework.model.cart.ShippingRateInput;
import com.molekule.api.v1.commonframework.model.cart.ShippingRatePriceTier;
import com.molekule.api.v1.commonframework.model.cart.SubRate;
import com.molekule.api.v1.commonframework.model.cart.TaxPortion;
import com.molekule.api.v1.commonframework.model.cart.TaxRate;
import com.molekule.api.v1.commonframework.model.cart.TaxedItemPrice;
import com.molekule.api.v1.commonframework.model.cart.TaxedPrice;
import com.molekule.api.v1.commonframework.model.cart.TrackingData;

@ContextConfiguration
public class ModelCartBeanTest {

	@Test
	public void avalaraRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AvalaraRequestBean.class);
	}

	@Test
	public void customLineItem() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomLineItem.class);
	}

	@Test
	public void deliveryItem() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DeliveryItem.class);
	}

	@Test
	public void discountCodeInfo() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountCodeInfo.class);
	}

	@Test
	public void discountedLineItemPortion() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountedLineItemPortion.class);
	}

	@Test
	public void discountedLineItemPrice() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountedLineItemPrice.class);
	}

	@Test
	public void discountedLineItemPriceForQuantity() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountedLineItemPriceForQuantity.class);
	}

	@Test
	public void itemState() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ItemState.class);
	}

	@Test
	public void parcelMeasurements() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ParcelMeasurements.class);
	}

	@Test
	public void paymentInfo() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentInfo.class);
	}

	@Test
	public void shippingInfo() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingInfo.class);
	}

	@Test
	public void shippingMethodRequest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingMethodRequest.class);
	}

	@Test
	public void shippingRate() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingRate.class);
	}

	@Test
	public void shippingRateInput() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingRateInput.class);
	}

	@Test
	public void shippingRatePriceTier() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingRatePriceTier.class);
	}

	@Test
	public void subRate() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SubRate.class);
	}

	@Test
	public void taxedItemPrice() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxedItemPrice.class);
	}

	@Test
	public void taxedPrice() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxedPrice.class);
	}

	@Test
	public void taxPortion() throws Exception

	{
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxPortion.class);
	}

	@Test
	public void taxRate() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxRate.class);
	}

	@Test
	public void trackingData() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TrackingData.class);
	}
}