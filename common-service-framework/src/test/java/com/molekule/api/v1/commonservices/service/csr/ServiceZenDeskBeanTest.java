package com.molekule.api.v1.commonservices.service.csr;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;

@ContextConfiguration
public class ServiceZenDeskBeanTest {

	@Test
	public void zendeskHelperService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CsrHelperService.class);
	}

}