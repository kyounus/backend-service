package com.molekule.api.v1.commonservices.dto.registration;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.dto.cart.QuoteCustomerDetailServiceBean.Action;
import com.molekule.api.v1.commonframework.dto.registration.AccessTokenDTO;
import com.molekule.api.v1.commonframework.dto.registration.CategoryDto;
import com.molekule.api.v1.commonframework.dto.registration.CompanyCategories;
import com.molekule.api.v1.commonframework.dto.registration.Custom;
import com.molekule.api.v1.commonframework.dto.registration.CustomObject;
import com.molekule.api.v1.commonframework.dto.registration.Customer;
import com.molekule.api.v1.commonframework.dto.registration.CustomerDTO;
import com.molekule.api.v1.commonframework.dto.registration.CustomerGroupRequestBean;
import com.molekule.api.v1.commonframework.dto.registration.CustomerSignupResponseDTO;
import com.molekule.api.v1.commonframework.dto.registration.Fields;
import com.molekule.api.v1.commonframework.dto.registration.NonProfitDocumentProofDto;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.SalesRepresentativeDto;
import com.molekule.api.v1.commonframework.dto.registration.StoreKeyReference;
import com.molekule.api.v1.commonframework.dto.registration.Type;

@ContextConfiguration
public class DTORegistrationBeanTest {

	@Test
	public void getterAndSetterAccessTokenDTO() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AccessTokenDTO.class);
	}

	@Test
	public void getterAndSetterAction() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Action.class);
	}

	@Test
	public void getterAndSetterCategoryDto() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CategoryDto.class);
	}

	@Test
	public void getterAndSetterCompanyCategories() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CompanyCategories.class);
	}

	@Test
	public void getterAndSetterCustom() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Custom.class);
	}

	@Test
	public void getterAndSetterCustomer() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Customer.class);
	}

	@Test
	public void getterAndSetterCustomerDTO() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomerDTO.class);
	}

	@Test
	public void getterAndSetterCustomerGroupRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomerGroupRequestBean.class);
	}

	@Test
	public void getterAndSetterCustomerSignupResponseDTO() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomerSignupResponseDTO.class);
	}

	@Test
	public void getterAndSetterCustomObject() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomObject.class);
	}

	@Test
	public void getterAndSetterFields() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Fields.class);
	}

	@Test
	public void getterAndSetterNonProfitDocumentProofDto() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(NonProfitDocumentProofDto.class);
	}

	@Test
	public void getterAndSetterPayments() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Payments.class);
	}

	@Test
	public void getterAndSetterSalesRepresentativeDto() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SalesRepresentativeDto.class);
	}

	@Test
	public void getterAndSetterStoreKeyReference() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(StoreKeyReference.class);
	}

	@Test
	public void getterAndSetterType() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Type.class);
	}

}
