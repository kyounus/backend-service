package com.molekule.api.v1.commonservices.mail.sendgrid;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.mail.sendgrid.OrderConfirmMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderPendingMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.OrderPendingSalesRepMailSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentTermsPendingApprovalSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentTermsRequestConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.PaymentTermsSuccessConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.RegistartionConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.RegistrationBySalesRepConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TaxExemptionConfirmationSender;
import com.molekule.api.v1.commonframework.mail.sendgrid.TaxExemptionSuccessConfirmationSender;

@ContextConfiguration
public class MailSendGridBeanTest {

	@Test
	public void getterAndSetterOrderConfirmMailSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderConfirmMailSender.class);
	}

	@Test
	public void getterAndSetterOrderPendingMailSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderPendingMailSender.class);
	}

	@Test
	public void getterAndSetterPaymentTermsRequestConfirmationSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentTermsRequestConfirmationSender.class);
	}

	@Test
	public void getterAndSetterRegistartionConfirmationSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(RegistartionConfirmationSender.class);
	}

	@Test
	public void getterAndSetterRegistrationBySalesRepConfirmationSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(RegistrationBySalesRepConfirmationSender.class);
	}

	@Test
	public void getterAndSetterTaxExemptionConfirmationSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxExemptionConfirmationSender.class);
	}

	@Test
	public void getterAndSetterTaxExemptionSuccessConfirmationSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxExemptionSuccessConfirmationSender.class);
	}

	@Test
	public void getterAndSetterPaymentTermsSuccessConfirmationSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentTermsSuccessConfirmationSender.class);
	}

	@Test
	public void paymentTermsPendingApprovalSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentTermsPendingApprovalSender.class);
	}

	@Test
	public void orderPendingSalesRepMailSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderPendingSalesRepMailSender.class);
	}

}