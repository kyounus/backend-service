package com.molekule.api.v1.commonservices.model.checkout;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.checkout.AddressRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.CTSetBillingAddress;
import com.molekule.api.v1.commonframework.model.checkout.ClearHoldState;
import com.molekule.api.v1.commonframework.model.checkout.ClientSecret;
import com.molekule.api.v1.commonframework.model.checkout.DiscountCodeRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.OrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.OrderRequestBean;
import com.molekule.api.v1.commonframework.model.checkout.Payment;
import com.molekule.api.v1.commonframework.model.checkout.PaymentIntentRequest;
import com.molekule.api.v1.commonframework.model.checkout.PaymentOrderDTO;
import com.molekule.api.v1.commonframework.model.checkout.PaymentRequest;
import com.molekule.api.v1.commonframework.model.checkout.SetTaxAmount;
import com.molekule.api.v1.commonframework.model.checkout.UpdateActionsOnCart;
import com.molekule.api.v1.commonframework.model.checkout.UpdatePayment;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;

@ContextConfiguration
public class CheckoutBeanTest {

	@Test
	public void getterAndSetterOrderSummaryResponseBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdateActionsOnCart.class);
	}

	@Test
	public void getterAndSetterAddPaymentToCartActionBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdateActionsOnCart.Action.class);
	}

	@Test
	public void getterAndSetterAddressRequestBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AddressRequestBean.class);
	}

	@Test
	public void getterAndSetterClearHoldStateTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ClearHoldState.class);
	}

	@Test
	public void getterAndSetterClearHoldStateActionStateTest() throws Exception {
		BeanTester beanTester = new BeanTester();

		beanTester.setIterations(1);
		beanTester.testBean(ClearHoldState.Action.State.class);
	}

	@Test
	public void getterAndSetterClearHoldStateActionTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ClearHoldState.Action.class);
	}

	@Test
	public void getterAndSetterClientSecretTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ClientSecret.class);
	}

	@Test
	public void getterAndSetterCTSetBillingAddressTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CTSetBillingAddress.class);
	}

	@Test
	public void getterAndSetterCTSetBillingAddressActionTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CTSetBillingAddress.Action.class);
	}

	@Test
	public void getterAndSetterCTSetBillingAddressActionAddressTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CTSetBillingAddress.Action.Address.class);
	}

	@Test
	public void getterAndSetterDiscountCodeRequestBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountCodeRequestBean.class);
	}

	@Test
	public void getterAndSetterDiscountCodeRequestActionBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountCodeRequestBean.Action.class);
	}

	@Test
	public void getterAndSetterDiscountCodeBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountCodeRequestBean.Action.DiscountCode.class);
	}

	@Test
	public void getterAndSetterFieldsTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountCodeRequestBean.Action.Fields.class);
	}

	@Test
	public void getterAndSetterDiscountCodeRequestBeanTypeTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(DiscountCodeRequestBean.Action.Type.class);
	}

	@Test
	public void getterAndSetterOrderDTOTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderDTO.class);
	}

	@Test
	public void getterAndSetterOrderRequestBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderRequestBean.class);
	}

	@Test
	public void getterAndSetterPaymentTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Payment.class);
	}

	@Test
	public void getterAndSetterPaymentTransactionTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Payment.Transaction.class);
	}

	@Test
	public void getterAndSetterPaymentTranscationAmountTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Payment.Transaction.Amount.class);
	}

	@Test
	public void getterAndSetterPaymentRequestBeanTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentRequestBean.class);
	}

	@Test
	public void getterAndSetterPaymentOrderDTOTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentOrderDTO.class);
	}

	/*
	 * @Test public void getterAndSetterSetTaxAmountActionTest() throws Exception {
	 * BeanTester beanTester = new BeanTester(); beanTester.setIterations(1);
	 * beanTester.testBean(SetTaxAmount.Action.class); }
	 */

	@Test
	public void getterAndSetterExternalTaxAmountTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SetTaxAmount.Action.ExternalTaxAmount.class);
	}

	@Test
	public void getterAndSetterExternalTotalGrossTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SetTaxAmount.Action.ExternalTotalGross.class);
	}

	@Test
	public void getterAndSetterActionFieldsTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SetTaxAmount.Action.Fields.class);
	}

	@Test
	public void getterAndSetterFieldsTypeTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SetTaxAmount.Action.Type.class);
	}

	@Test
	public void getterAndSetterSetTaxAmountTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SetTaxAmount.class);
	}

	@Test
	public void getterAndSetterUpdatePaymentTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdatePayment.class);
	}

	@Test
	public void getterAndSetterUpdatePaymentActionTest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdatePayment.Action.class);
	}

	@Test
	public void paymentmRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentRequest.class);
	}

	@Test
	public void paymentIntentRequest() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentIntentRequest.class);
	}

}
