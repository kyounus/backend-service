package com.molekule.api.v1.commonservices.service.registration;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;

@ContextConfiguration
public class ServiceRegistrationBeanTest {

	@Test
	public void ctServerHelperService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CtServerHelperService.class);
	}

	@Test
	public void registrationHelperService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(RegistrationHelperService.class);
	}
}