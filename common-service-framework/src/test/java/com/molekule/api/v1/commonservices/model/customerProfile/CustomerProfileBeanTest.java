package com.molekule.api.v1.commonservices.model.customerProfile;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.customerprofile.MessageCenterBean;

@ContextConfiguration
public class CustomerProfileBeanTest {

	@Test
	public void messageCenterBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(MessageCenterBean.class);
	}

}