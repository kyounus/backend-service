package com.molekule.api.v1.commonservices.dto.cart;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.dto.cart.Cart;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartDTO;
import com.molekule.api.v1.commonframework.dto.cart.CreateCartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.QuoteCustomFieldCartServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.QuoteCustomerDetailServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartCustomServiceBean;
import com.molekule.api.v1.commonframework.dto.cart.CartRequestBean;
import com.molekule.api.v1.commonframework.dto.cart.UpdateCartServiceBean;

@ContextConfiguration
public class CartBeansTest {

	@Test
	public void getterAndSetterCart() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Cart.class);
	}

	@Test
	public void getterAndSetterCreateCartDTO() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CreateCartDTO.class);

	}

	@Test
	public void getterAndSetterCreateCustomerGroup() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CreateCartDTO.CustomerGroup.class);
	}

	@Test
	public void getterAndSetterCreateCartRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CreateCartRequestBean.class);
	}

	@Test
	public void getterAndSetterUpdateCartCustomServiceBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdateCartCustomServiceBean.class);
	}

	@Test
	public void getterAndSetterUpdateCartRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CartRequestBean.class);
	}

	@Test
	public void getterAndSetterUpdateCartServiceBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdateCartServiceBean.class);
	}

	@Test
	public void getterAndSetterQuoteCustomerDetailServiceBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(QuoteCustomerDetailServiceBean.class);
	}

	@Test
	public void getterAndSetterQuoteCustomFieldCartServiceBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(QuoteCustomFieldCartServiceBean.class);
	}

}