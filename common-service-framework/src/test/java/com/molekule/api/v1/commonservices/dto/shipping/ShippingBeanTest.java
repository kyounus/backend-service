package com.molekule.api.v1.commonservices.dto.shipping;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.dto.shipping.ShippingTable;

@ContextConfiguration
public class ShippingBeanTest {

	@Test
	public void getterAndSetterAccessTokenDTO() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingTable.class);
	}

	@Test
	public void getterAndSetterShippingTableValue() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingTable.ShippingTableValue.class);
	}

	
}
