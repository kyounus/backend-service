package com.molekule.api.v1.commonservices.model.aurora;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.aurora.AuroraCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.AuroraUpdateCustomerRequestBean;
import com.molekule.api.v1.commonframework.model.aurora.CategoryObjectBean;

@ContextConfiguration
public class ModelAuroraBeanTest {

	@Test
	public void auroraCustomerRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AuroraCustomerRequestBean.class);
	}
	@Test
	public void auroraUpdateCustomerRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AuroraUpdateCustomerRequestBean.class);
	}
	@Test
	public void categoryObjectBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CategoryObjectBean.class);
	}

}