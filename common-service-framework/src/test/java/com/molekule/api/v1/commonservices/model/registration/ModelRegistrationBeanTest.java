package com.molekule.api.v1.commonservices.model.registration;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.registration.AccountCustomResponseBean;
import com.molekule.api.v1.commonframework.model.registration.AdditionalFreightInfo;
import com.molekule.api.v1.commonframework.model.registration.Address;
import com.molekule.api.v1.commonframework.model.registration.AddressV2;
import com.molekule.api.v1.commonframework.model.registration.BankInformation;
import com.molekule.api.v1.commonframework.model.registration.BillingAddress;
import com.molekule.api.v1.commonframework.model.registration.BillingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.BillingAddresses;
import com.molekule.api.v1.commonframework.model.registration.Business;
import com.molekule.api.v1.commonframework.model.registration.BusinessReferences;
import com.molekule.api.v1.commonframework.model.registration.CarrierOption;
import com.molekule.api.v1.commonframework.model.registration.CarrierOptionCustomObject;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoBean;
import com.molekule.api.v1.commonframework.model.registration.CompanyInfoResponseBean;
import com.molekule.api.v1.commonframework.model.registration.CreditCard;
import com.molekule.api.v1.commonframework.model.registration.GovernmentVerification;
import com.molekule.api.v1.commonframework.model.registration.MolekuleCarrier;
import com.molekule.api.v1.commonframework.model.registration.PaymentOptionRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentOptionResponseBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentRequestBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentTermsPublicBean;
import com.molekule.api.v1.commonframework.model.registration.PaymentTermsPublicResponseBean;
import com.molekule.api.v1.commonframework.model.registration.ProductBean;
import com.molekule.api.v1.commonframework.model.registration.ProductResponse;
import com.molekule.api.v1.commonframework.model.registration.ProductResponseBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationBean;
import com.molekule.api.v1.commonframework.model.registration.RegistrationResponseBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddress;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressRequestBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddressResponseBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingAddresses;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierOptionsBean;
import com.molekule.api.v1.commonframework.model.registration.ShippingCarrierRelationShip;
import com.molekule.api.v1.commonframework.model.registration.StockDetails;
import com.molekule.api.v1.commonframework.model.registration.StreetLine;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReview;
import com.molekule.api.v1.commonframework.model.registration.TaxExemptReviewsRequestBean;
import com.molekule.api.v1.commonframework.model.registration.UpdatePaymentRequestBean;

@ContextConfiguration
public class ModelRegistrationBeanTest {

	@Test
	public void getterAndSetterAccountCustomResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AccountCustomResponseBean.class);
	}

	@Test
	public void getterAndSetterAdditionalFreightInfo() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AdditionalFreightInfo.class);
	}

	@Test
	public void getterAndSetterAddress() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Address.class);
	}

	@Test
	public void getterAndSetterAddressV2() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(AddressV2.class);
	}

	@Test
	public void getterAndSetterBankInformation() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(BankInformation.class);
	}

	@Test
	public void getterAndSetterBillingAddress() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(BillingAddress.class);
	}

	@Test
	public void getterAndSetterBillingAddresses() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(BillingAddresses.class);
	}

	@Test
	public void getterAndSetterBillingAddressRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(BillingAddressRequestBean.class);
	}

	@Test
	public void getterAndSetterBusiness() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Business.class);
	}

	@Test
	public void getterAndSetterBusinessReferences() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(BusinessReferences.class);
	}

	@Test
	public void getterAndSetterCarrierOption() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CarrierOption.class);
	}

	@Test
	public void getterAndSetterCarrierOptionCustomObject() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CarrierOptionCustomObject.class);
	}

	@Test
	public void getterAndSetterCompanyInfoBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CompanyInfoBean.class);
	}

	@Test
	public void getterAndSetterCompanyInfoResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CompanyInfoResponseBean.class);
	}

	@Test
	public void getterAndSetterCreditCard() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CreditCard.class);
	}

	@Test
	public void getterAndSetterGovernmentVerification() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(GovernmentVerification.class);
	}

	@Test
	public void getterAndSetterMolekuleCarrier() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(MolekuleCarrier.class);
	}

	@Test
	public void getterAndSetterPaymentOptionRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentOptionRequestBean.class);
	}

	@Test
	public void getterAndSetterPaymentOptionResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentOptionResponseBean.class);
	}

	@Test
	public void getterAndSetterPaymentRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentRequestBean.class);
	}

	@Test
	public void getterAndSetterPaymentTermsPublicBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentTermsPublicBean.class);
	}

	@Test
	public void getterAndSetterPaymentTermsPublicResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(PaymentTermsPublicResponseBean.class);
	}

	@Test
	public void getterAndSetterProductBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductBean.class);
	}

	@Test
	public void getterAndSetterProductResponse() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductResponse.class);
	}

	@Test
	public void getterAndSetterProductResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ProductResponseBean.class);
	}

	@Test
	public void getterAndSetterRegistrationBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(RegistrationBean.class);
	}

	@Test
	public void getterAndSetterRegistrationResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(RegistrationResponseBean.class);
	}

	@Test
	public void getterAndSetterShippingAddress() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingAddress.class);
	}

	@Test
	public void getterAndSetterShippingAddresses() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingAddresses.class);
	}

	@Test
	public void getterAndSetterShippingAddressRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingAddressRequestBean.class);
	}

	@Test
	public void getterAndSetterShippingAddressResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingAddressResponseBean.class);
	}

	@Test
	public void getterAndSetterShippingCarrierOptionsBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingCarrierOptionsBean.class);
	}

	@Test
	public void getterAndSetterShippingCarrierRelationShip() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(ShippingCarrierRelationShip.class);
	}

	@Test
	public void getterAndSetterStockDetails() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(StockDetails.class);
	}

	@Test
	public void getterAndSetterStreetLine() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(StreetLine.class);
	}

	@Test
	public void getterAndSetterTaxExemptReviews() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxExemptReview.class);
	}

	@Test
	public void getterAndSetterTaxExemptReviewsRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(TaxExemptReviewsRequestBean.class);
	}

	@Test
	public void getterAndSetterUpdatePaymentRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(UpdatePaymentRequestBean.class);
	}

}