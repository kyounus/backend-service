package com.molekule.api.v1.commonservices.service.order;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.service.order.OrderHelperService;

@ContextConfiguration
public class ServiceOrderBeanTest {

	@Test
	public void orderHelperService() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderHelperService.class);
	}

}