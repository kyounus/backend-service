package com.molekule.api.v1.commonservices.model.zendesk;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;

@ContextConfiguration
public class ModelZenDesk {

	@Test
	public void getterAndSetterOrderConfirmMailSender() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(CustomerDetailBean.class);
	}

}