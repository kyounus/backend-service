package com.molekule.api.v1.commonservices.model.order;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.order.OrderSummaryResponseBean;

@ContextConfiguration
public class OrderBeanTest {

	@Test
	public void orderSummaryResponseBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderSummaryResponseBean.class);
	}

	@Test
	public void orderSummaryResponseBeanOrderSummary() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(OrderSummaryResponseBean.OrderSummary.class);
	}

}