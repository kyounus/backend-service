package com.molekule.api.v1.commonservices.service.registration;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.CUSTOM_OBJECTS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.dto.registration.AccountCustomObject;
import com.molekule.api.v1.commonframework.dto.registration.AccountDto;
import com.molekule.api.v1.commonframework.dto.registration.Custom;
import com.molekule.api.v1.commonframework.dto.registration.CustomerResponseBean;
import com.molekule.api.v1.commonframework.dto.registration.Fields;
import com.molekule.api.v1.commonframework.dto.registration.Payments;
import com.molekule.api.v1.commonframework.dto.registration.Value;
import com.molekule.api.v1.commonframework.model.checkout.ClearHoldState;
import com.molekule.api.v1.commonframework.service.checkout.CheckoutHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.service.registration.RegistrationHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

@ContextConfiguration
public class RegistrationHelperServiceTest {
	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	RegistrationHelperService registrationHelperService = spy(RegistrationHelperService.class);

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	NetConnectionHelper netConnectionHelper;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	CheckoutHelperService checkoutHelperService;

	@Mock
	ClearHoldState clearHoldState;

	@Mock
	CustomerResponseBean customer = Mockito.mock(CustomerResponseBean.class);

	@Mock
	AccountCustomObject accountCustomObject = mock(AccountCustomObject.class);

	private final String CUSTOMER_ID = "1234";
	private final String ACCOUNT_ID = "5678";
	private final String KEY = "key";
	private final String customerGroupId = "1234567";

	@Test
	public void getCustomerById() {

		BDDMockito.when(netConnectionHelper.sendGet(null, CUSTOMER_ID)).thenReturn(customer);

		assertEquals(null, registrationHelperService.getCustomerById(CUSTOMER_ID));
	}

	@Test
	public void getCTCustomObjectsByAccountIdUrl() {
		assertEquals("null/null/custom-objects?where=id=\"5678\"",
				registrationHelperService.getCTCustomObjectsByAccountIdUrl(ACCOUNT_ID));
	}

	@Test
	public void assignCardObjectToPayments() {
		Value value = Mockito.mock(Value.class);
		Payments payment = mock(Payments.class);
		String paymentToken = "seti_1J0pSK2eZvKYlo2Ct4pfzqWG";
		List<Payments> paymentList = new ArrayList<>();
		payment.setPaymentType("CREDITCARD");
		payment.setPaymentToken(paymentToken);
		paymentList.add(payment);
		value.setPayments(paymentList);

		BDDMockito.when(value.getPayments()).thenReturn(paymentList);
		BDDMockito.when(payment.getPaymentToken()).thenReturn(paymentToken);
		BDDMockito.when(payment.getCustomerId()).thenReturn(CUSTOMER_ID);
		BDDMockito.when(payment.getPaymentType()).thenReturn("CREDITCARD");
		String paymentIntentBaseUrl = "null/seti_1J0pSK2eZvKYlo2Ct4pfzqWG";

		String setupPaymentIntentResponse = "{\"id\":\"seti_1J0pSK2eZvKYlo2Ct4pfzqWG\",\"object\":\"setup_intent\",\"application\":null,\"cancellation_reason\":null,\"client_secret\":\"seti_1J0pSK2eZvKYlo2Ct4pfzqWG_secret_Je7hydKkcZLgSB0cGhuZ54y5FX4itSL\",\"created\":1623337564,\"customer\":\"cus_Je7hncFDSVPWGO\",\"description\":null,\"last_setup_error\":null,\"latest_attempt\":\"setatt_1J0pSL2eZvKYlo2CR9iZLFL1\",\"livemode\":false,\"mandate\":null,\"metadata\":{},\"next_action\":null,\"on_behalf_of\":null,\"payment_method\":\"pm_1J0pSL2eZvKYlo2CfCo5NM8a\",\"payment_method_options\":{\"card\":{\"request_three_d_secure\":\"automatic\"}},\"payment_method_types\":[\"card\"],\"single_use_mandate\":null,\"status\":\"succeeded\",\"usage\":\"off_session\"}";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", paymentIntentBaseUrl))
				.thenReturn(setupPaymentIntentResponse);

		String paymentMethodBaseUrl = "null/pm_1J0pSL2eZvKYlo2CfCo5NM8a";
		String paymentMethodResponse = "{\r\n" + "    \"id\": \"pm_1J0pSL2eZvKYlo2CfCo5NM8a\",\r\n"
				+ "    \"object\": \"payment_method\",\r\n" + "    \"billing_details\": {\r\n"
				+ "        \"address\": {\r\n" + "            \"city\": null,\r\n"
				+ "            \"country\": null,\r\n" + "            \"line1\": null,\r\n"
				+ "            \"line2\": null,\r\n" + "            \"postal_code\": null,\r\n"
				+ "            \"state\": null\r\n" + "        },\r\n" + "        \"email\": null,\r\n"
				+ "        \"name\": null,\r\n" + "        \"phone\": null\r\n" + "    },\r\n" + "    \"card\": {\r\n"
				+ "        \"brand\": \"visa\",\r\n" + "        \"checks\": {\r\n"
				+ "            \"address_line1_check\": null,\r\n"
				+ "            \"address_postal_code_check\": null,\r\n" + "            \"cvc_check\": \"pass\"\r\n"
				+ "        },\r\n" + "        \"country\": \"US\",\r\n" + "        \"exp_month\": 9,\r\n"
				+ "        \"exp_year\": 2025,\r\n" + "        \"fingerprint\": \"tiDP36QdYA6X7km8\",\r\n"
				+ "        \"funding\": \"credit\",\r\n" + "        \"generated_from\": null,\r\n"
				+ "        \"last4\": \"1111\",\r\n" + "        \"networks\": {\r\n"
				+ "            \"available\": [\r\n" + "                \"visa\"\r\n" + "            ],\r\n"
				+ "            \"preferred\": null\r\n" + "        },\r\n" + "        \"three_d_secure_usage\": {\r\n"
				+ "            \"supported\": true\r\n" + "        },\r\n" + "        \"wallet\": null\r\n"
				+ "    },\r\n" + "    \"created\": 1623337565,\r\n" + "    \"customer\": \"cus_Je7hncFDSVPWGO\",\r\n"
				+ "    \"livemode\": false,\r\n" + "    \"metadata\": {},\r\n" + "    \"type\": \"card\"\r\n" + "}";

		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", paymentMethodBaseUrl))
				.thenReturn(paymentMethodResponse);

		assertEquals(paymentList, registrationHelperService.assignCardObjectToPayments(value, CUSTOMER_ID));
	}

	@Test
	public void getAccountKeyById() {

		String accountCustomUrl = "null/null/custom-objects?where=id=\"5678\"";
		Mockito.doReturn(accountCustomUrl).when(registrationHelperService).getCTCustomObjectsByAccountIdUrl(ACCOUNT_ID);

		String accountStringResponse = "{\"limit\":20,\"offset\":0,\"count\":20,\"total\":10396,\"results\":[{\"id\":\"3f65f67c-a45c-4c5d-b4cb-ab6152751d26\",\"version\":21,\"createdAt\":\"2021-03-23T10:46:14.673Z\",\"lastModifiedAt\":\"2021-05-26T08:23:13.730Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"bd577ea6-82f0-4857-adf6-fc8ab315c881\",\"value\":{\"parcelFlag\":false,\"freightFlag\":false,\"noOfEmployees\":\"12\",\"noOfOfficeSpaces\":\"12\",\"employeeIds\":[\"78007a27-34f9-4666-ab4d-c3f4390cd2f9\",\"7a2d72c3-f654-437e-a53b-d69de38de447\",\"196e15b1-df44-4d0b-9d3f-9cd15caf42cd\",\"7cd30584-e971-4550-ac9f-038fd084eb0b\",\"727d7769-53a1-4d81-98fa-e2af302f17f5\",\"bcd6a26d-3368-43e0-b754-b826e514640d\",\"d517a8a6-a547-43ac-ba77-8b63dedef8cb\",\"5efc09a3-3406-40f9-9be3-541cda1c2d00\",\"2a4bb145-ff48-423e-90a5-10298c58f6c1\",\"3db21d19-f237-4c83-aa11-93caaeae2017\",\"92589178-bd35-40b3-9e7f-08e84fae430c\",\"9792bcf3-c798-4590-bb18-28294abf6dcd\",\"8b210905-6f54-47a2-88d8-ca62681bce92\",\"5c316189-46d1-461a-aeef-ae4dac6d77f0\",\"1223f6e1-68e0-4b0f-a62d-73dcd7e5f562\",\"a2a951aa-15f7-4f90-8cea-3051146cfbd7\",\"0c84b3d3-9907-4373-a4c3-eac66ec6c58a\",\"e5cc2b1a-4b59-454a-ab93-f74519617e39\",\"fa29a8f2-44a7-4bb3-a57b-7cd03d95ce3a\"],\"myOwnCarrier\":false,\"taxExemptFlag\":false}}]}";

		Mockito.doReturn(accountStringResponse).when(netConnectionHelper).sendGetWithoutBody(null, accountCustomUrl);
		assertEquals("bd577ea6-82f0-4857-adf6-fc8ab315c881", registrationHelperService.getAccountKeyById(ACCOUNT_ID));
	}

	@Test
	public void getAccountCustomObject() {
		assertEquals(null, registrationHelperService.getAccountCustomObject(KEY));
	}

	@Test
	public void getCustomerGroup() {
		assertEquals(null, registrationHelperService.getCustomerGroup(customerGroupId));
	}

	@Test
	public void getAccountIdWithCustomerId() {
		final String customerResponse = "{\"id\":\"75c0fd82-2068-4f7d-9e43-f3d97e29ad2a\",\"version\":9,\"lastMessageSequenceNumber\":7,\"createdAt\":\"2021-02-25T00:37:01.993Z\",\"lastModifiedAt\":\"2021-05-10T12:02:16.860Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"isPlatformClient\":true,\"user\":{\"typeId\":\"user\",\"id\":\"692b6979-70d8-4720-b446-902e754e3b08\"}},\"email\":\"test@cust.com\",\"firstName\":\"Test\",\"lastName\":\"Customer\",\"middleName\":\"\",\"title\":\"\",\"salutation\":\"\",\"companyName\":\"TestA\",\"password\":\"Dl2aSY3YDWOn0Ky/D8yOd2fJ5bjWYovFpUUtROihqSo=$5+s7C7l+dFw+kUfCEl8DN42ITegxtWkmxin3Z1r0TOU=\",\"addresses\":[{\"id\":\"pBqZ6dWu\",\"firstName\":\"Test\",\"lastName\":\"Customer\",\"streetName\":\"East\",\"streetNumber\":\"5\",\"postalCode\":\"27520\",\"city\":\"City\",\"country\":\"US\",\"email\":\"test@cust.com\"}],\"shippingAddressIds\":[],\"billingAddressIds\":[],\"isEmailVerified\":false,\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"de4181c9-24f4-4877-a4be-8b2e04b3f2e1\"},\"fields\":{\"invoiceEmailAddress\":[\"tejatech@gmail.com\"],\"accountId\":\"ba076565-d5f0-4195-96d7-b310330f4d40\"}},\"stores\":[]}";

		String customerBaseUrl = "null/null/customers/1234/?";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", customerBaseUrl))
				.thenReturn(customerResponse);
		assertEquals("ba076565-d5f0-4195-96d7-b310330f4d40",
				registrationHelperService.getAccountIdWithCustomerId(CUSTOMER_ID));
	}

	// @Test
	public void createEmailDynamicInfo() {
		Mockito.doReturn(customer).when(registrationHelperService).getCustomerById(CUSTOMER_ID);

		Mockito.doReturn(ACCOUNT_ID).when(registrationHelperService).getAccountIdWithCustomerId(CUSTOMER_ID);
		Mockito.doReturn(ACCOUNT_ID).when(registrationHelperService).getCTCustomObjectsByAccountIdUrl(ACCOUNT_ID);
		final String actualResponse = "{ \"container\":\"b2b\", \"key\":\"TestCompany395Sudalai\", \"version\":2, \"results\":[ { \"value\":{ \"id\":null, \"companyName\":\"TestCompany\", \"salesRepresentative\":{ \"id\":\"b779377d-0a92-48c6-a7e7-016da03f5922\", \"name\":\"Pavithra test\", \"phone\":\"123456789\", \"email\":\"test@gmail.com\", \"firstName\":\"Pavithra\", \"lastName\":\"test\", \"calendlyLink\":\"string\" }, \"companyCategory\":{ \"id\":\"5cb62872-fa6e-422e-80ef-dadd35ec3490\", \"name\":\"OtherHealthcare\", \"sales-rep-id\":\"b779377d-0a92-48c6-a7e7-016da03f5922\" } } } ] }";
		BDDMockito.when(netConnectionHelper.sendGetWithoutBody(null, ACCOUNT_ID)).thenReturn(actualResponse);
		Custom custom = mock(Custom.class);
		Fields fields = mock(Fields.class);
		BDDMockito.when(customer.getCustom()).thenReturn(custom);
		BDDMockito.when(customer.getCustom().getFields()).thenReturn(fields);
		registrationHelperService.createEmailDynamicInfo(CUSTOMER_ID);

	}

	@Test
	public void getAccountByCustomerId() {
		AccountCustomObject accountCustomObject = mock(AccountCustomObject.class);
		AccountDto accountDto = mock(AccountDto.class);
		List<AccountCustomObject> resultList = new ArrayList<>();
		resultList.add(accountCustomObject);

		String accountUrl = "null/null" + CUSTOM_OBJECTS + "?where=value(employeeIds=\"" + CUSTOMER_ID + "\")";
		BDDMockito.when(netConnectionHelper.sendGetAccountObject(null, accountUrl)).thenReturn(accountDto);
		BDDMockito.when(accountDto.getResults()).thenReturn(resultList);

		assertEquals(accountCustomObject, registrationHelperService.getAccountByCustomerId(CUSTOMER_ID, null));
	}

	@Test
	public void getAccountByAccountId() {
		String accountUrl = "null/null/custom-objects?where=id=\"5678\"";

		AccountCustomObject accountCustomObject = mock(AccountCustomObject.class);
		AccountDto accountResult = mock(AccountDto.class);
		List<AccountCustomObject> resultList = new ArrayList<>();
		resultList.add(accountCustomObject);

		BDDMockito.when(netConnectionHelper.sendGetAccountObject(null, accountUrl)).thenReturn(accountResult);
		BDDMockito.when(accountResult.getResults()).thenReturn(resultList);

		assertEquals(accountCustomObject, registrationHelperService.getAccountByAccountId(ACCOUNT_ID));
	}

	@Test
	public void processOrder() {
		String holdType = "CreditHold";
		String orderUrl = "null/null/orders?where=customerId=\"1234\"";

		String orderResultResponse = "{\"limit\":20,\"offset\":0,\"count\":4,\"total\":4,\"results\":[{\"type\":\"Order\",\"id\":\"ec723104-4b5c-4db7-8081-384176461186\",\"version\":3,\"lastMessageSequenceNumber\":3,\"createdAt\":\"2021-05-15T17:52:48.294Z\",\"lastModifiedAt\":\"2021-05-15T17:52:52.071Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"orderNumber\":\"15052021175248264\",\"customerId\":\"75c0fd82-2068-4f7d-9e43-f3d97e29ad2a\",\"customerEmail\":\"test@cust.com\",\"customerGroup\":{\"typeId\":\"customer-group\",\"id\":\"7ee4f9ae-9b48-4e4d-914e-7ad3c00ac1c0\"},\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":200,\"fractionDigits\":2},\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":200,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":212,\"fractionDigits\":2},\"taxPortions\":[{\"rate\":0.06,\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":12,\"fractionDigits\":2},\"name\":\"CA STATE TAX\"}]},\"country\":\"US\",\"orderState\":\"Confirmed\",\"syncInfo\":[],\"returnInfo\":[],\"state\":{\"typeId\":\"state\",\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\"},\"taxMode\":\"ExternalAmount\",\"inventoryMode\":\"None\",\"taxRoundingMode\":\"HalfEven\",\"taxCalculationMode\":\"LineItemLevel\",\"origin\":\"Customer\",\"lineItems\":[{\"id\":\"bc2c2c3c-81ad-441b-9a8d-9b1864671fdc\",\"productId\":\"ab858042-d452-4e99-9d78-06afd6ee00b1\",\"name\":{\"en-US\":\"AirSubscription\"},\"productType\":{\"typeId\":\"product-type\",\"id\":\"9160d8e4-9cc1-422f-9b00-1bd688de51c3\",\"version\":17},\"variant\":{\"id\":1,\"sku\":\"MAS-123\",\"prices\":[{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":29900,\"fractionDigits\":2},\"id\":\"72aa3f7f-a649-413e-b0aa-40e1800ac05f\"}],\"images\":[],\"attributes\":[{\"name\":\"purifierReferenceSku\",\"value\":{\"typeId\":\"product\",\"id\":\"27355a9d-f2ab-4c2b-990a-a9486f58d972\"}},{\"name\":\"filterProduct\",\"value\":{\"typeId\":\"product\",\"id\":\"8a4b28c5-a0e3-4afe-a547-c1d4680282ba\"}},{\"name\":\"b2cView\",\"value\":false},{\"name\":\"b2bView\",\"value\":true},{\"name\":\"sellable\",\"value\":true},{\"name\":\"period\",\"value\":6},{\"name\":\"firstDeliveryDays\",\"value\":180},{\"name\":\"StoreID\",\"value\":{\"key\":\"MLK US\",\"label\":\"MLK US\"}}],\"assets\":[]},\"price\":{\"value\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2},\"id\":\"dd717b47-1cce-4c53-a4ef-fcec8041011e\"},\"quantity\":1,\"discountedPricePerQuantity\":[],\"taxRate\":{\"name\":\"myTaxRate\",\"amount\":0.0,\"includedInPrice\":false,\"country\":\"US\",\"subRates\":[]},\"addedAt\":\"2021-05-15T17:51:44.553Z\",\"lastModifiedAt\":\"2021-05-15T17:51:44.553Z\",\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}],\"priceMode\":\"ExternalTotal\",\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2},\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":0,\"fractionDigits\":2}},\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"6d2ca957-ce13-46e5-8d52-3f8267b07263\"},\"fields\":{\"subscriptionEnabled\":true}},\"lineItemMode\":\"Standard\"}],\"customLineItems\":[{\"totalPrice\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":200,\"fractionDigits\":2},\"id\":\"ed385b19-3b8f-44eb-a129-418f84dd8148\",\"name\":{\"en\":\"Handling Cost\"},\"money\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":200,\"fractionDigits\":2},\"slug\":\"handling_cost\",\"quantity\":1,\"discountedPricePerQuantity\":[],\"taxRate\":{\"name\":\"tax\",\"amount\":0.06,\"includedInPrice\":false,\"country\":\"US\",\"subRates\":[]},\"state\":[{\"quantity\":1,\"state\":{\"typeId\":\"state\",\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\"}}],\"taxedPrice\":{\"totalNet\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":200,\"fractionDigits\":2},\"totalGross\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":212,\"fractionDigits\":2}}}],\"transactionFee\":true,\"discountCodes\":[],\"cart\":{\"typeId\":\"cart\",\"id\":\"8180e71b-0f12-4dcd-8930-bf7b05367f07\"},\"custom\":{\"type\":{\"typeId\":\"type\",\"id\":\"937cb0c5-55a8-4c88-af52-3bcb8c53a04c\"},\"fields\":{\"subTotal\":\"0\",\"shippingCost\":\"0\",\"handlingCost\":\"200\",\"totalTax\":\"12\",\"orderTotal\":\"212\",\"holds\":[\"TaxHold\",\"FreightHold\"]}},\"paymentInfo\":{\"payments\":[{\"typeId\":\"payment\",\"id\":\"6d430b3b-3a55-4fea-8ffa-2bc4cc93037a\"}]},\"shippingAddress\":{\"id\":\"33180660-8a32-4a83-b8c6-507d86a90f4a\",\"firstName\":\"string\",\"lastName\":\"string\",\"streetName\":\"2000 Main Street\",\"streetNumber\":\"string\",\"postalCode\":\"92461\",\"city\":\"Ivrine\",\"region\":\"CA\",\"state\":\"CA\",\"country\":\"US\",\"company\":\"string\",\"phone\":\"string\"},\"itemShippingAddresses\":[],\"refusedGifts\":[],\"store\":{\"typeId\":\"store\",\"key\":\"MLK_US\"}}]}";

		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", orderUrl))
				.thenReturn(orderResultResponse);
		AccountCustomObject accountCustomObject = mock(AccountCustomObject.class);
		ResponseEntity<AccountCustomObject> responseEntity = new ResponseEntity<AccountCustomObject>(
				accountCustomObject, HttpStatus.OK);
		BDDMockito.when(checkoutHelperService.getAccountCustomObjectApi(ACCOUNT_ID, null)).thenReturn(responseEntity);
		Value value = mock(Value.class);
		BDDMockito.when(accountCustomObject.getValue()).thenReturn(value);

		String orderStateUrl = "null/null/states?where=key=\"order-processing\"";

		String orderStateResponse = "{\"limit\":20,\"offset\":0,\"count\":13,\"total\":13,\"results\":[{\"id\":\"94260c22-687b-4263-82b1-77e601644ca0\",\"version\":4,\"createdAt\":\"2021-01-08T22:22:23.325Z\",\"lastModifiedAt\":\"2021-05-17T13:12:27.026Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"isPlatformClient\":true},\"key\":\"Initial\",\"type\":\"LineItemState\",\"roles\":[],\"name\":{\"en\":\"Initial\"},\"description\":{\"en\":\"Initial\"},\"builtIn\":true,\"initial\":true},{\"id\":\"6b0ca09d-b6e1-4a0d-9568-30d1c0f2b2f1\",\"version\":10,\"createdAt\":\"2021-04-27T18:30:35.706Z\",\"lastModifiedAt\":\"2021-06-04T09:53:50.703Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-customerHold\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Customer Hold\"},\"description\":{\"en\":\"Hold\"},\"builtIn\":false,\"transitions\":[{\"typeId\":\"state\",\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\"},{\"typeId\":\"state\",\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\"},{\"typeId\":\"state\",\"id\":\"c66111c7-92f3-4145-9d3d-4b6a0c6e00e9\"},{\"typeId\":\"state\",\"id\":\"c4da8c79-a751-429f-bb93-0332e7d8b5dd\"},{\"typeId\":\"state\",\"id\":\"39d91194-ade0-435c-af99-8e81a5b921ed\"}],\"initial\":false},{\"id\":\"1c57f4c5-feb5-4855-85c1-6b76672568c2\",\"version\":1,\"createdAt\":\"2021-05-03T13:35:42.950Z\",\"lastModifiedAt\":\"2021-05-03T13:35:42.950Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"clearHold\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"\"},\"description\":{\"en\":\"\"},\"builtIn\":false,\"initial\":true},{\"id\":\"39d91194-ade0-435c-af99-8e81a5b921ed\",\"version\":3,\"createdAt\":\"2021-05-04T13:33:44.245Z\",\"lastModifiedAt\":\"2021-06-04T09:53:19.946Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-submitted\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Submitted\"},\"description\":{\"en\":\"Submitted\"},\"builtIn\":false,\"transitions\":[{\"typeId\":\"state\",\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\"},{\"typeId\":\"state\",\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\"},{\"typeId\":\"state\",\"id\":\"6b0ca09d-b6e1-4a0d-9568-30d1c0f2b2f1\"},{\"typeId\":\"state\",\"id\":\"c66111c7-92f3-4145-9d3d-4b6a0c6e00e9\"},{\"typeId\":\"state\",\"id\":\"c4da8c79-a751-429f-bb93-0332e7d8b5dd\"}],\"initial\":true},{\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\",\"version\":4,\"createdAt\":\"2021-05-04T13:34:52.825Z\",\"lastModifiedAt\":\"2021-06-04T09:52:33.743Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-processing\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Processing\"},\"description\":{\"en\":\"Processing\"},\"builtIn\":false,\"transitions\":[{\"typeId\":\"state\",\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\"},{\"typeId\":\"state\",\"id\":\"6b0ca09d-b6e1-4a0d-9568-30d1c0f2b2f1\"},{\"typeId\":\"state\",\"id\":\"c66111c7-92f3-4145-9d3d-4b6a0c6e00e9\"},{\"typeId\":\"state\",\"id\":\"c4da8c79-a751-429f-bb93-0332e7d8b5dd\"},{\"typeId\":\"state\",\"id\":\"39d91194-ade0-435c-af99-8e81a5b921ed\"}],\"initial\":false},{\"id\":\"0b3d7d45-e4fc-485d-b5c0-b26e3e5ffe2a\",\"version\":3,\"createdAt\":\"2021-05-04T13:35:46.375Z\",\"lastModifiedAt\":\"2021-05-08T13:47:59.479Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-accepted\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Accepted\"},\"description\":{\"en\":\"Processing\"},\"builtIn\":false,\"transitions\":[{\"typeId\":\"state\",\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\"},{\"typeId\":\"state\",\"id\":\"c4da8c79-a751-429f-bb93-0332e7d8b5dd\"}],\"initial\":false},{\"id\":\"c4da8c79-a751-429f-bb93-0332e7d8b5dd\",\"version\":5,\"createdAt\":\"2021-05-04T13:36:26.555Z\",\"lastModifiedAt\":\"2021-06-04T09:51:58.646Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-backordered\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Backordered\"},\"description\":{\"en\":\"Processing\"},\"builtIn\":false,\"transitions\":[{\"typeId\":\"state\",\"id\":\"e6628cf6-0a2f-4f00-a127-d53cd0e3b3ae\"},{\"typeId\":\"state\",\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\"},{\"typeId\":\"state\",\"id\":\"6b0ca09d-b6e1-4a0d-9568-30d1c0f2b2f1\"},{\"typeId\":\"state\",\"id\":\"c66111c7-92f3-4145-9d3d-4b6a0c6e00e9\"},{\"typeId\":\"state\",\"id\":\"39d91194-ade0-435c-af99-8e81a5b921ed\"}],\"initial\":false},{\"id\":\"aa6982ab-0a9a-4d7e-9442-f99541f6816b\",\"version\":2,\"createdAt\":\"2021-05-04T13:36:57.490Z\",\"lastModifiedAt\":\"2021-05-08T13:50:45.307Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-shipped\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Shipped\"},\"description\":{\"en\":\"Shipped\"},\"builtIn\":false,\"transitions\":[],\"initial\":false},{\"id\":\"c66111c7-92f3-4145-9d3d-4b6a0c6e00e9\",\"version\":2,\"createdAt\":\"2021-05-04T13:37:38.182Z\",\"lastModifiedAt\":\"2021-05-08T13:51:11.216Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"order-cancelled\",\"type\":\"OrderState\",\"roles\":[],\"name\":{\"en\":\"Cancelled\"},\"description\":{\"en\":\"Cancelled\"},\"builtIn\":false,\"transitions\":[],\"initial\":false},{\"id\":\"df51412c-aa16-4c96-9ca9-81586ef5e798\",\"version\":2,\"createdAt\":\"2021-05-12T07:44:49.762Z\",\"lastModifiedAt\":\"2021-05-14T17:19:40.714Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"lineitem-shipped\",\"type\":\"LineItemState\",\"roles\":[],\"name\":{\"en\":\"Shipped\"},\"description\":{\"en\":\"Shipped\"},\"builtIn\":false,\"initial\":false},{\"id\":\"73646ef4-b175-47d0-9e96-ed948eda8d2e\",\"version\":2,\"createdAt\":\"2021-05-12T07:48:18.717Z\",\"lastModifiedAt\":\"2021-05-14T17:19:07.442Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"lineitem-released\",\"type\":\"LineItemState\",\"roles\":[],\"name\":{\"en\":\"Released\"},\"description\":{\"en\":\"Released\"},\"builtIn\":false,\"initial\":false},{\"id\":\"6d742b04-d211-4de3-a8cc-4cda3d6b838a\",\"version\":2,\"createdAt\":\"2021-05-12T07:50:38.536Z\",\"lastModifiedAt\":\"2021-05-14T17:18:30.474Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"lineitem-backordered\",\"type\":\"LineItemState\",\"roles\":[],\"name\":{\"en\":\"Backordered\"},\"description\":{\"en\":\"Backordered\"},\"builtIn\":false,\"initial\":false},{\"id\":\"7b812370-3975-4174-9936-55629a969c03\",\"version\":2,\"createdAt\":\"2021-05-12T07:51:15.462Z\",\"lastModifiedAt\":\"2021-05-14T17:17:59.923Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"key\":\"lineitem-accepted\",\"type\":\"LineItemState\",\"roles\":[],\"name\":{\"en\":\"Accepted\"},\"description\":{\"en\":\"Accepted\"},\"builtIn\":false,\"initial\":false}]}";

		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", orderStateUrl))
				.thenReturn(orderStateResponse);

		String paymentUrl = "null/null/payments/6d430b3b-3a55-4fea-8ffa-2bc4cc93037a";
		String paymentResponse = "{\"id\":\"6d430b3b-3a55-4fea-8ffa-2bc4cc93037a\",\"version\":1,\"lastMessageSequenceNumber\":1,\"createdAt\":\"2021-05-15T17:52:48.033Z\",\"lastModifiedAt\":\"2021-05-15T17:52:48.033Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"interfaceId\":\"pi_1IrRfP2eZvKYlo2C7ymzD96D\",\"amountPlanned\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":212,\"fractionDigits\":2},\"paymentMethodInfo\":{\"paymentInterface\":\"STRIPE\",\"method\":\"CREDIT_CARD\",\"name\":{\"en\":\"Credit Card\"}},\"paymentStatus\":{},\"transactions\":[{\"id\":\"c3a23abf-92a2-48ed-9e87-4affbe953fe0\",\"type\":\"Charge\",\"amount\":{\"type\":\"centPrecision\",\"currencyCode\":\"USD\",\"centAmount\":212,\"fractionDigits\":2},\"state\":\"Pending\"}],\"interfaceInteractions\":[]}";

		BDDMockito.when(netConnectionHelper.sendGetWithoutBody("Bearer null", paymentUrl)).thenReturn(paymentResponse);

		registrationHelperService.processOrder(CUSTOMER_ID, holdType, ACCOUNT_ID);
	}

	@Test
	public void mappingStringResponseToCustomObject() {
		String response = "{\"id\":\"3f65f67c-a45c-4c5d-b4cb-ab6152751d26\",\"version\":21,\"createdAt\":\"2021-03-23T10:46:14.673Z\",\"lastModifiedAt\":\"2021-05-26T08:23:13.730Z\",\"lastModifiedBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"createdBy\":{\"clientId\":\"KWv8cEitMtB_AZYcnMRckN3l\",\"isPlatformClient\":false},\"container\":\"b2b\",\"key\":\"bd577ea6-82f0-4857-adf6-fc8ab315c881\",\"value\":{\"parcelFlag\":false,\"freightFlag\":false,\"noOfEmployees\":\"12\",\"noOfOfficeSpaces\":\"12\",\"employeeIds\":[\"78007a27-34f9-4666-ab4d-c3f4390cd2f9\",\"7a2d72c3-f654-437e-a53b-d69de38de447\",\"196e15b1-df44-4d0b-9d3f-9cd15caf42cd\",\"7cd30584-e971-4550-ac9f-038fd084eb0b\",\"727d7769-53a1-4d81-98fa-e2af302f17f5\",\"bcd6a26d-3368-43e0-b754-b826e514640d\",\"d517a8a6-a547-43ac-ba77-8b63dedef8cb\",\"5efc09a3-3406-40f9-9be3-541cda1c2d00\",\"2a4bb145-ff48-423e-90a5-10298c58f6c1\",\"3db21d19-f237-4c83-aa11-93caaeae2017\",\"92589178-bd35-40b3-9e7f-08e84fae430c\",\"9792bcf3-c798-4590-bb18-28294abf6dcd\",\"8b210905-6f54-47a2-88d8-ca62681bce92\",\"5c316189-46d1-461a-aeef-ae4dac6d77f0\",\"1223f6e1-68e0-4b0f-a62d-73dcd7e5f562\",\"a2a951aa-15f7-4f90-8cea-3051146cfbd7\",\"0c84b3d3-9907-4373-a4c3-eac66ec6c58a\",\"e5cc2b1a-4b59-454a-ab93-f74519617e39\",\"fa29a8f2-44a7-4bb3-a57b-7cd03d95ce3a\"],\"myOwnCarrier\":false,\"taxExemptFlag\":false}}";
		registrationHelperService.mappingStringResponseToCustomObject(response);

	}

}
