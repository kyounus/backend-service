package com.molekule.api.v1.commonservices.model.sendgrid;

import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.springframework.test.context.ContextConfiguration;

import com.molekule.api.v1.commonframework.model.sendgrid.Product;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridModel;
import com.molekule.api.v1.commonframework.model.sendgrid.SendGridRequestBean;

@ContextConfiguration
public class ModelSendGridBeanTest {

	@Test
	public void product() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(Product.class);
	}

	@Test
	public void sendGridModel() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SendGridModel.class);
	}

	@Test
	public void sendGridRequestBean() throws Exception {
		BeanTester beanTester = new BeanTester();
		beanTester.setIterations(1);
		beanTester.testBean(SendGridRequestBean.class);
	}
}