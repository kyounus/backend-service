package com.molekule.api.v1.csr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;
import com.molekule.api.v1.csr.service.CsrService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * ZendeskApiController is used to write registration related api.
 * 
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1")
public class CsrController {
	
	@Autowired
	@Qualifier("csrService")
	CsrService csrService;
	
	/**
	 * getListOfAccounts method is used to get the list of accounts.
	 * @param email
	 * @param limit
	 * @return String - it contain all accounts related values.
	 */
	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Customer Details")
	public CustomerDetailBean getCustomerByEmail(
			@ApiParam(value = "The email is", required = true) @RequestParam String email,
			@ApiParam(value = "The limit is", required = false) @RequestParam(defaultValue = "5" ) String limit) {
		return csrService.getCustomerData(null,email,Integer.parseInt(limit));
	}
	
	/**
	 * getOrderByOrderId method is used to display the Order Data by orderId.
	 * @param orderId
	 * 
	 * @return String - it contain order data.
	 */
	@GetMapping(value = "/orders/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Order Details")
	public String getOrderByOrderId(@ApiParam(value = "The Order id", required = true) @PathVariable("orderId") String orderId) {
		return csrService.getOrderByOrderId(orderId);
	}

}
