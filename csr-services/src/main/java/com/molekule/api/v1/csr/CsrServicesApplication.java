package com.molekule.api.v1.csr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrServicesApplication.class, args);
	}

}
