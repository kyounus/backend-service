package com.molekule.api.v1.csr.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CSRFController {
	@GetMapping(value = "/csrf", produces = "application/json;charset=UTF-8")
	public ResponseEntity<CsrfToken> getToken(final HttpServletRequest request) {
		return ResponseEntity.ok().body(new HttpSessionCsrfTokenRepository().generateToken(request));
	}
}