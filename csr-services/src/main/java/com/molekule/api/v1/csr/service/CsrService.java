package com.molekule.api.v1.csr.service;

import static com.molekule.api.v1.commonframework.util.MolekuleConstant.BEARER;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.model.csr.CustomerDetailBean;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;
/**
 * The class ZendeskService is used to write the api calls for the Zendesk Api.
 * 
 * @version 1.0
 */
@Service("csrService")
public class CsrService {

	Logger logger = LoggerFactory.getLogger(CsrService.class);
	@Autowired
	CTEnvProperties ctEnvProperties;

	@Autowired
	@Qualifier("ctServerHelperService")
	CtServerHelperService ctServerHelperService;
	
	@Autowired
	@Qualifier("netConnectionHelper")
	NetConnectionHelper netConnectionHelper;
	
	@Autowired
	@Qualifier("csrHelperService")
	CsrHelperService csrHelperService;
	
	/**
	 * getOrderByOrderId method is used to display the Order Data by orderId.
	 * @param orderId
	 * 
	 * @return String - it contain order data.
	 */
	public String getOrderByOrderId(String orderId) {
		String ordersBaseUrl = ctEnvProperties.getHost() + "/" + ctEnvProperties.getProjectKey() + "/orders/" + orderId
				+"?expand=paymentInfo.payments[*].typeId";
		String token = new StringBuilder().append(BEARER).append(ctServerHelperService.getAccessToken()).toString();
		return netConnectionHelper.sendGetWithoutBody(token, ordersBaseUrl);
	}

	public CustomerDetailBean getCustomerData(String customerId ,String email,int limit) {
		return csrHelperService.getCustomerData(customerId, email, limit, null);
	}


}
