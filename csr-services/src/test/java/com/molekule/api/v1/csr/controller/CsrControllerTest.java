package com.molekule.api.v1.csr.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.molekule.api.v1.csr.service.CsrService;

public class CsrControllerTest {

	@InjectMocks
	CsrController csrController;

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Mock
	CsrService csrService;

	@Test
	public void getCustomerByEmail() {

		Assert.assertNull(csrController.getCustomerByEmail("testEmail", "1"));
	}

	@Test
	public void getOrderByOrderId() {
		Assert.assertNull(csrController.getOrderByOrderId("testOrderID"));
	}

}