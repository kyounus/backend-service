package com.molekule.api.v1.csr.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.molekule.api.v1.commonframework.configuration.CTEnvProperties;
import com.molekule.api.v1.commonframework.service.csr.CsrHelperService;
import com.molekule.api.v1.commonframework.service.registration.CtServerHelperService;
import com.molekule.api.v1.commonframework.util.NetConnectionHelper;

public class CsrServiceTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CsrService csrService;

	@Mock
	Logger logger = LoggerFactory.getLogger(CsrService.class);

	@Mock
	CTEnvProperties ctEnvProperties;

	@Mock
	CtServerHelperService ctServerHelperService;

	@Mock
	NetConnectionHelper netConnectionHelper;

	@Mock
	CsrHelperService csrHelperService;

	@Test
	public void getOrderByOrderId() {
		Assert.assertNull(csrService.getOrderByOrderId("testOrderID"));
	}

	@Test
	public void getCustomerData() {
		Assert.assertNull(csrService.getCustomerData("testCustomerId", "testEmail", 1));
	}

}