package com.molekule.api.v1.csr.configuration;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class CsrConfigurationTest {

	AutoCloseable closeable;

	@Before
	public void openMocks() {
		closeable = MockitoAnnotations.openMocks(this);

	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@InjectMocks
	CsrConfiguration csrConfiguration;

	@Test
	public void getDocket() {
		Assert.assertNotNull(csrConfiguration.getDocket());
	}

	@Test
	public void crosConfigure() {
		Assert.assertNotNull(csrConfiguration.crosConfigure());
	}

	@Test
	public void getApiInfo() {
		Assert.assertNotNull(csrConfiguration.getApiInfo());
	}

	@Test
	public void cTEnvProperties() {
		Assert.assertNotNull(csrConfiguration.cTEnvProperties());

	}

	@Test
	public void netConnectionHelper() {
		Assert.assertNotNull(csrConfiguration.netConnectionHelper());

	}

	@Test
	public void ctServerHelperService() {
		Assert.assertNotNull(csrConfiguration.ctServerHelperService());
	}

	@Test
	public void registrationHelperService() {
		Assert.assertNotNull(csrConfiguration.registrationHelperService());

	}

	@Test
	public void checkoutHelperService() {
		Assert.assertNotNull(csrConfiguration.checkoutHelperService());

	}

	@Test
	public void orderHelperService() {
		Assert.assertNotNull(csrConfiguration.orderHelperService());

	}

	@Test
	public void cartHelperService() {
		Assert.assertNotNull(csrConfiguration.cartHelperService());
	}

	@Test
	public void taxService() {
		Assert.assertNotNull(csrConfiguration.taxService());

	}

	@Test
	public void handlingCostHelperService() {
		Assert.assertNotNull(csrConfiguration.handlingCostHelperService());

	}

	@Test
	public void parcelHandlingCostCalculationServieImpl() {
		Assert.assertNotNull(csrConfiguration.parcelHandlingCostCalculationServieImpl());
	}

	@Test
	public void freightHandlingCostCalculationServiceImpl() {
		Assert.assertNotNull(csrConfiguration.freightHandlingCostCalculationServiceImpl());

	}

	@Test
	public void csrHelperService() {
		Assert.assertNotNull(csrConfiguration.csrHelperService());

	}

	@Test
	public void tealiumHelperService() {
		Assert.assertNotNull(csrConfiguration.tealiumHelperService());

	}

	@Test
	public void quoteRequestConfirmationMailSender() {
		Assert.assertNotNull(csrConfiguration.quoteRequestConfirmationMailSender());
	}

	@Test
	public void quoteRequestPendingApprovalMailSender() {
		Assert.assertNotNull(csrConfiguration.quoteRequestPendingApprovalMailSender());
	}

	@Test
	public void paymentReceivedConfirmationSender() {
		Assert.assertNotNull(csrConfiguration.paymentReceivedConfirmationSender());
	}

}